package com.imps.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;
import com.imps.api.IImpsApi;
import com.imps.entity.AddBeneficiary;
import com.imps.model.ImpsRequest;
import com.imps.model.ImpsResponse;
import com.imps.model.error.ImpsError;
import com.imps.validation.ImpsValidation;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.model.request.BeneficiaryDTO;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.repositories.AddBeneficiaryRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.util.StartUpUtil;

@Controller
@RequestMapping("Api/v1/User/Android/en/User/IMPS")
public class ImpsFundTransfer {

	private final UserSessionRepository userSessionRepository;
	private final IImpsApi iImpsApi;
	private final MServiceRepository serviceRepository;
	private final MCommissionRepository mCommissionRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository mmCardRepository;
	private final AddBeneficiaryRepository addBeneficiaryRepository;

	public ImpsFundTransfer(UserSessionRepository userSessionRepository, IImpsApi iImpsApi,
			MServiceRepository serviceRepository, MCommissionRepository mCommissionRepository,
			IMatchMoveApi matchMoveApi, MMCardRepository mmCardRepository,
			AddBeneficiaryRepository addBeneficiaryRepository) {
		this.userSessionRepository = userSessionRepository;
		this.iImpsApi = iImpsApi;
		this.serviceRepository = serviceRepository;
		this.mCommissionRepository = mCommissionRepository;
		this.matchMoveApi = matchMoveApi;
		this.mmCardRepository = mmCardRepository;
		this.addBeneficiaryRepository = addBeneficiaryRepository;
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processP2ATransfer(@RequestBody ImpsRequest dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request, ModelMap modelMap,
			HttpSession session) throws SAXException, IOException, ParserConfigurationException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto.getSessionId(), hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			request.setAttribute("sessionId", sessionId);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				ImpsResponse response = new ImpsResponse();
				try {
					ImpsError error = ImpsValidation.checkP2AError(dto);
					if (error.isValid()) {
						if (sessionId != null && sessionId.length() != 0) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								request.getSession().setAttribute("sessionId", sessionId);
								MMCards card = mmCardRepository.getCardByUserAndStatus(user, Status.Active + "");
								MService service = serviceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
								MCommission commission = mCommissionRepository.findCommissionByService(service);
								double transactionAmount = Double.valueOf(dto.getAmount());
								double finalTransactionAmount = transactionAmount;
								double netCommissionValue = 0.0;
								if (dto.isSaveBeneficiary()) {
									AddBeneficiary isPresent = addBeneficiaryRepository
											.findByAccountNoAndUser(dto.getAccountNo(), user);
									if (isPresent == null) {
										AddBeneficiary bene = new AddBeneficiary();
										bene.setName(dto.getName());
										bene.setAccountNo(dto.getAccountNo());
										bene.setIfscNo(dto.getIfscNo());
										bene.setBankName(dto.getBankName());
										bene.setUser(user);
										addBeneficiaryRepository.save(bene);
									}
								}
								if (commission != null) {
									netCommissionValue = CommonUtil.commissionEarned(commission,
											Double.valueOf(dto.getAmount()));
									finalTransactionAmount = netCommissionValue + transactionAmount;
									BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
									BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
									finalTransactionAmount = roundOff.doubleValue();
								}
								ResponseDTO walletResponse = matchMoveApi.getBalanceExp(userSession.getUser());
								if (walletResponse.getBalance() >= finalTransactionAmount) {
									dto.setSessionId(sessionId);
									dto.setContactNo(userSession.getUser().getUsername());
									response = iImpsApi.initiateP2ATransfer(dto);
									if (response.isSuccess()) {
//							deduct money from card
										UserKycResponse walletRespo = matchMoveApi.debitFromCard(
												userSession.getUser().getUserDetail().getEmail(),
												userSession.getUser().getUsername(), card.getCardId(),
												finalTransactionAmount + "", "Transfer funds");
										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletRespo.getCode())) {

											dto.setTransactionRefNo(response.getTransactionRefNo());
											dto.setRemName(user.getUserDetail().getFirstName());
											dto.setRemContactNo(user.getUsername());
											response = iImpsApi.processP2ATransfer(dto);
											if (response.isSuccess()) {
												response.setContactNo(user.getUsername());
												response.setTransactionRefNo(dto.getTransactionRefNo());
												response.setAmount(dto.getAmount());
												response.setSessionId(dto.getSessionId());
												response.setLoadStatus(Status.Done + "");
												response = iImpsApi.responseP2ATransfer(response);
												modelMap.put("Message", response.getMessage());
												result.setCode(ResponseStatus.SUCCESS.getValue());
												result.setMessage(response.getMessage());
											} else {
//								Load Money to card
												WalletResponse walletRespons = matchMoveApi
														.initiateLoadFundsToMMWalletTopup(userSession.getUser(),
																finalTransactionAmount + "");
												String authRefNo = null;
												if (walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
													authRefNo = walletRespons.getAuthRetrivalNo();
													response.setRetrivalRefNo(authRefNo);
													response.setLoadStatus(Status.Success + "");
													response.setContactNo(user.getUsername());
													response.setSessionId(dto.getSessionId());
													response.setTransactionRefNo(dto.getTransactionRefNo());
													response.setAmount(dto.getAmount());
													dto.setMessage(response.getMessage());
													response = iImpsApi.responseP2ATransfer(response);
												} else {
													response.setRetrivalRefNo(authRefNo);
													response.setLoadStatus(Status.Pending + "");
													response.setContactNo(user.getUsername());
													response.setSessionId(dto.getSessionId());
													response.setTransactionRefNo(dto.getTransactionRefNo());
													response.setAmount(dto.getAmount());
													dto.setMessage(response.getMessage());
													response = iImpsApi.responseP2ATransfer(response);
												}
												modelMap.put("Message", dto.getMessage());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage(dto.getMessage());
											}
										} else {
											modelMap.put("Message", walletRespo.getMessage());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setMessage(walletRespo.getMessage());
										}
									} else {
										modelMap.put("Message", response.getMessage());
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage(response.getMessage());
									}
								} else {
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Insufficient balance,your current balance is Rs."
											+ walletResponse.getBalance());
								}

							} else {
								modelMap.put("Message", "Invalid Session Id,Please login and try again later");
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Invalid Session Id,Please login and try again later");
							}
							/*
							 * }else{ modelMap.put("Message",
							 * "Unauthorized User,Please login and try again later"); result.setCode(ResponseStatus.FAILURE.getValue());
							 * result.setMessage("Unauthorized User,Please login and try again later"); }
							 */
						} else {
							modelMap.put("Message", "Invalid Session Id,Please login and try again later");
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Unauthorized User,Please login and try again later");
						}
					} else {
						modelMap.put("Message", error.getMessage());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(error.getMessage());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Service Unavailable,PLease try Again later");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage("Unauthorized User,Please login and try again later");
			}
		} else {
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage("Invalid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessWeb", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processP2ATransferWeb(@RequestBody ImpsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request, ModelMap modelMap,
			HttpSession session) throws SAXException, IOException, ParserConfigurationException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto.getSessionId(), hash);
		if (isValidHash) {
			String sessionId = session.getId();
			request.setAttribute("sessionId", sessionId);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				ImpsResponse response = new ImpsResponse();
				try {
					ImpsError error = ImpsValidation.checkP2AError(dto);
					if (error.isValid()) {
						if (sessionId != null && sessionId.length() != 0) {
//				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
//				if (authority != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								request.getSession().setAttribute("sessionId", sessionId);
								MMCards card = mmCardRepository.getCardByUserAndStatus(user, Status.Active + "");
								MService service = serviceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
								MCommission commission = mCommissionRepository.findCommissionByService(service);
								double transactionAmount = Double.valueOf(dto.getAmount());
								double finalTransactionAmount = transactionAmount;
								double netCommissionValue = 0.0;
								if (dto.isSaveBeneficiary()) {
									AddBeneficiary isPresent = addBeneficiaryRepository
											.findByAccountNoAndUser(dto.getAccountNo(), user);
									if (isPresent == null) {
										AddBeneficiary bene = new AddBeneficiary();
										bene.setName(dto.getName());
										bene.setAccountNo(dto.getAccountNo());
										bene.setIfscNo(dto.getIfscNo());
										bene.setBankName(dto.getBankName());
										bene.setUser(user);
										addBeneficiaryRepository.save(bene);
									}
								}
								if (commission != null) {
									netCommissionValue = CommonUtil.commissionEarned(commission,
											Double.valueOf(dto.getAmount()));
									finalTransactionAmount = netCommissionValue + transactionAmount;
									BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
									BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
									finalTransactionAmount = roundOff.doubleValue();
								}
								ResponseDTO walletResponse = matchMoveApi.getBalanceExp(userSession.getUser());
								if (walletResponse.getBalance() >= finalTransactionAmount) {
									dto.setSessionId(sessionId);
									dto.setContactNo(userSession.getUser().getUsername());
									response = iImpsApi.initiateP2ATransfer(dto);
									if (response.isSuccess()) {
//							deduct money from card
										UserKycResponse walletRespo = matchMoveApi.debitFromCard(
												userSession.getUser().getUserDetail().getEmail(),
												userSession.getUser().getUsername(), card.getCardId(),
												finalTransactionAmount + "", "Transafer funds");
										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletRespo.getCode())) {

											dto.setTransactionRefNo(response.getTransactionRefNo());
											dto.setRemName(user.getUserDetail().getFirstName());
											dto.setRemContactNo(user.getUsername());
											response = iImpsApi.processP2ATransfer(dto);
											if (response.isSuccess()) {
												response.setContactNo(user.getUsername());
												response.setTransactionRefNo(dto.getTransactionRefNo());
												response.setAmount(dto.getAmount());
												response.setSessionId(dto.getSessionId());
												response.setLoadStatus(Status.Done + "");
												response = iImpsApi.responseP2ATransfer(response);
												modelMap.put("Message", response.getMessage());
												result.setCode(ResponseStatus.SUCCESS.getValue());
												result.setMessage(response.getMessage());
											} else {
//								Load Money to card
												WalletResponse walletRespons = matchMoveApi
														.initiateLoadFundsToMMWalletTopup(userSession.getUser(),
																finalTransactionAmount + "");
												String authRefNo = null;
												if (walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
													authRefNo = walletRespons.getAuthRetrivalNo();
													response.setRetrivalRefNo(authRefNo);
													response.setLoadStatus(Status.Success + "");
													response.setContactNo(user.getUsername());
													response.setSessionId(dto.getSessionId());
													response.setTransactionRefNo(dto.getTransactionRefNo());
													response.setAmount(dto.getAmount());
													dto.setMessage(response.getMessage());
													response = iImpsApi.responseP2ATransfer(response);
												} else {
													response.setRetrivalRefNo(authRefNo);
													response.setLoadStatus(Status.Pending + "");
													response.setContactNo(user.getUsername());
													response.setSessionId(dto.getSessionId());
													response.setTransactionRefNo(dto.getTransactionRefNo());
													response.setAmount(dto.getAmount());
													dto.setMessage(response.getMessage());
													response = iImpsApi.responseP2ATransfer(response);
												}
												modelMap.put("Message", dto.getMessage());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage(dto.getMessage());
											}
										} else {
											modelMap.put("Message", walletRespo.getMessage());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setMessage(walletRespo.getMessage());
										}
									} else {
										modelMap.put("Message", response.getMessage());
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage(response.getMessage());
									}
								} else {
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Insufficient balance,your current balance is Rs."
											+ walletResponse.getBalance());
								}

							} else {
								modelMap.put("Message", "Invalid Session Id,Please login and try again later");
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Invalid Session Id,Please login and try again later");
							}
							/*
							 * }else{ modelMap.put("Message",
							 * "Unauthorized User,Please login and try again later"); result.setCode(ResponseStatus.FAILURE.getValue());
							 * result.setMessage("Unauthorized User,Please login and try again later"); }
							 */
						} else {
							modelMap.put("Message", "Invalid Session Id,Please login and try again later");
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Unauthorized User,Please login and try again later");
						}
					} else {
						modelMap.put("Message", error.getMessage());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(error.getMessage());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Service Unavailable,PLease try Again later");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage("Unauthorized User,Please login and try again later");
			}
		} else {
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage("Invalid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/fetchBeneficiary", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> fetchBeneficiary(@RequestBody ImpsRequest dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request, ModelMap modelMap,
			HttpSession session) throws SAXException, IOException, ParserConfigurationException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto.getSessionId(), hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			request.setAttribute("sessionId", sessionId);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				try {
					if (user != null) {
						List<AddBeneficiary> listBeneficiary = addBeneficiaryRepository.findByUser(user);
						List<BeneficiaryDTO> blist = new ArrayList<>();
						if (listBeneficiary.size() > 0) {
							for (AddBeneficiary beneficiary : listBeneficiary) {
								BeneficiaryDTO be = new BeneficiaryDTO();
								be.setAccountNo(beneficiary.getAccountNo());
								be.setIfscNo(beneficiary.getIfscNo());
								be.setName(beneficiary.getName());
								be.setId(beneficiary.getId() + "");
								be.setBankname(beneficiary.getBankName());
								blist.add(be);
							}
							result.setBeneficiary(blist);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Beneficiaries List.");
						} else {
							result.setBeneficiary(blist);
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(" No Beneficiaries available yet.");
						}
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage("Failed,PLease try Again later");
					}

				} catch (Exception e) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Service Unavailable,PLease try Again later");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage("Unauthorized User,Please login and try again later");
			}
		} else {
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage("Invalid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

}
