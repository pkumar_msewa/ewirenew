package com.imps.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ImpsConstant {

//	test
	public static final String URL = "http://203.199.32.92:7474/imps-web-bc/api/transaction/bc/IBCFli00044/p2a?";
	public static final String PASSCODE = "3f4dbbdad1a241bca994339c0d3f3efd";
	public static final String BC_ID = "IBCFli00044";
	
//	LIVE
//	public static final String PASSCODE = "f5c1af53445e4c4fb00bf0a1dd121b78";
//	public static final String URL = "https://103.87.40.11:7474/imps-web-bc/api/transaction/bc/IBCMse00336/p2a?";	
//	public static final String BC_ID = "IBCMse00336";
	public static final String RETAILER_CODE = "CashierCards";
	public static final String TRANSACTION_DATE = new Date()+"";
	private static SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddhhmmss");
	
	public static String transactionDate(){
		Date date= new Date();
		String aa=sdf.format(date);
		return aa;
	}
	
	/*public static void main(String args[]){
		Date date= new Date();
		String aa=sdf.format(date);
		System.err.println(aa);
	}*/
	
}
