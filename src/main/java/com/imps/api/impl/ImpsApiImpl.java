package com.imps.api.impl;

import java.io.StringReader;
import java.math.BigDecimal;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.NodeList;

import com.imps.api.IImpsApi;
import com.imps.model.ImpsRequest;
import com.imps.model.ImpsResponse;
import com.imps.util.ImpsConstant;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.model.ResponseStatus;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.util.CommonUtil;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.LoadMoneyValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

public class ImpsApiImpl implements IImpsApi {
	private final MServiceRepository serviceRepository;
	private final MCommissionRepository mCommissionRepository;
	private final MUserRespository mUserRespository;
	private final ITransactionApi transactionApi;
	private final LoadMoneyValidation loadMoneyValidation;
	public ImpsApiImpl(MServiceRepository serviceRepository,MCommissionRepository mCommissionRepository,MUserRespository mUserRespository,
			ITransactionApi transactionApi,LoadMoneyValidation loadMoneyValidation) {

	this.serviceRepository=serviceRepository;
	this.mCommissionRepository=mCommissionRepository;
	this.mUserRespository=mUserRespository;
	this.transactionApi=transactionApi;
	this.loadMoneyValidation=loadMoneyValidation;
	}

	@Override
	public ImpsResponse initiateP2ATransfer(ImpsRequest ebsRequest) {
		ImpsResponse resp= new ImpsResponse();
		try { 
			MUser user=mUserRespository.findByUsername(ebsRequest.getContactNo());
			MService service = serviceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
			double amount=Double.valueOf(ebsRequest.getAmount());
			MCommission commission = mCommissionRepository.findCommissionByService(service);
			double transactionAmount=Double.valueOf(ebsRequest.getAmount());
			double finalTransactionAmount=transactionAmount;
			double netCommissionValue=0.0;
			if(commission!=null){
				netCommissionValue=CommonUtil.commissionEarned(commission,Double.valueOf(ebsRequest.getAmount()));
				finalTransactionAmount=netCommissionValue+transactionAmount;
				BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
				BigDecimal roundOff= a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				finalTransactionAmount=roundOff.doubleValue();
			}
			TransactionError transactionError = loadMoneyValidation.validateP2PFundTransaction(ebsRequest.getAmount(), user.getUsername(), service, commission, netCommissionValue);
			if (transactionError.isValid()) {
				String transactionRefNo=String.valueOf(System.currentTimeMillis());	
				ResponseDTO result=transactionApi.initiateP2PFundTransfer(amount, "Payment of P2A Fund Transfer", service,transactionRefNo, user, StartUpUtil.IMPS, commission, netCommissionValue,ebsRequest);;
					if(ResponseStatus.SUCCESS.getValue().equals(result.getCode())) {
						resp.setTransactionRefNo(transactionRefNo);
						resp.setSuccess(true);
					}else {
						resp.setSuccess(false);
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F04");
			resp.setMessage("Service Unavailable");
		}
		return resp;
	}
	@Override
	public ImpsResponse processP2ATransfer(ImpsRequest dto) {
		ImpsResponse response= new ImpsResponse();
		if(dto.getDescription() == null){
			dto.setDescription("NA");
		}
		try {
		Client client=Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource=client.resource(ImpsConstant.URL).queryParam("BeneAccNo", dto.getAccountNo()).queryParam("BeneIFSC", dto.getIfscNo())
				.queryParam("TranRefNo", dto.getTransactionRefNo()).queryParam("PaymentRef", dto.getDescription()).queryParam("RemName", dto.getRemName())
				.queryParam("RemMobile", dto.getRemContactNo()).queryParam("RetailerCode", ImpsConstant.RETAILER_CODE).queryParam("PassCode", ImpsConstant.PASSCODE).queryParam("TransactionDate", ImpsConstant.transactionDate()).queryParam("Amount", dto.getAmount());
		System.err.println(ImpsConstant.transactionDate());
		System.err.println("requset"+webResource);
		ClientResponse respons=webResource.accept("application/xml").type("application/x-www-form-urlencoded").post(ClientResponse.class);
		String strResponse = respons.getEntity(String.class);
		
	        DocumentBuilderFactory dbf =DocumentBuilderFactory.newInstance();
	            DocumentBuilder db = dbf.newDocumentBuilder();
	            InputSource is = new InputSource();
	            is.setCharacterStream(new StringReader(strResponse));
	            Document doc = db.parse(is);
	            NodeList nodes = doc.getElementsByTagName("ImpsResponse");
	            for (int i = 0; i < nodes.getLength(); i++) {
	               Element element = (Element) nodes.item(i);
	               NodeList name = element.getElementsByTagName("ActCode");
	               Element line = (Element) name.item(0);
	               response.setCode(getCharacterDataFromElement(line));
	               NodeList title = element.getElementsByTagName("Response");
	               line = (Element) title.item(0);
	               response.setMessage(getCharacterDataFromElement(line));
	               NodeList BankRRN = element.getElementsByTagName("BankRRN");
	               line = (Element) BankRRN.item(0);
	               response.setRetrivalRefNo(getCharacterDataFromElement(line));
	               NodeList BeneName = element.getElementsByTagName("BeneName");
	               line = (Element) BeneName.item(0);
	               response.setBeneficiaryName(getCharacterDataFromElement(line));
	               NodeList TranRefNo = element.getElementsByTagName("TranRefNo");
	               line = (Element) TranRefNo.item(0);
	               response.setTransactionRefNo(getCharacterDataFromElement(line));
	            }
		
		if(response.getCode().equalsIgnoreCase("0")){
			response.setStatus(true);
			response.setSuccess(true);
		}else{
			response.setStatus(false);
			response.setSuccess(false);
		}} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(false);
			response.setSuccess(false);
		}
		return response;
	}
	
	@Override
	public ImpsResponse responseP2ATransfer(ImpsResponse dto) {
		ImpsResponse respo= new ImpsResponse();
		try{
			MUser user=mUserRespository.findByUsername(dto.getContactNo());
			MService service = serviceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
			MCommission commission =  mCommissionRepository.findCommissionByService(service);
			double netCommissionValue = CommonUtil.commissionEarned(commission,Double.parseDouble(dto.getAmount()));
			if (dto.isStatus()) {
				transactionApi.successImpsPayment(dto.getTransactionRefNo(), commission, netCommissionValue, user, dto);
				respo.setCode(ResponseStatus.SUCCESS.getValue());
				respo.setMessage("Payment Succesful.");
				respo.setTransactionRefNo(dto.getTransactionRefNo());
				return respo;
			}else{
				transactionApi.failedImpsPayment(dto.getTransactionRefNo(),dto.getRetrivalRefNo(),dto.getLoadStatus());
				respo.setCode(ResponseStatus.FAILURE.getValue());
				respo.setMessage("Payment Failed, Please try again later");
				return respo;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			respo.setCode(ResponseStatus.FAILURE.getValue());
			respo.setMessage(ResponseStatus.FAILURE.getKey());
		}
		return respo;
	}
	
	  public static String getCharacterDataFromElement(Element e) {
		    Node child = e.getFirstChild();
		    if (child instanceof CharacterData) {
		       CharacterData cd = (CharacterData) child;
		       return cd.getData();
		    }
		    return "NA";
		  }

}
