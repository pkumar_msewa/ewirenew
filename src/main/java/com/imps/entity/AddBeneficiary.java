package com.imps.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.msscard.entity.AbstractEntity;
import com.msscard.entity.MUser;

@Entity
public class AddBeneficiary extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountNo;
	private String ifscNo;
	private String name;
	private String bankName;
	@ManyToOne
	private MUser user;
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscNo() {
		return ifscNo;
	}
	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
}
