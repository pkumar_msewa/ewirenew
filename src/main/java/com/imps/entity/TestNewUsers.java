package com.imps.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.msscard.entity.AbstractEntity;

@Table(name = "TestNewUsers")
@Entity
public class TestNewUsers extends AbstractEntity<Long> {

	private static final long serialVersionUID = -8614124864176053020L;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column(unique = true, nullable = false)
	private String mobile;

	@Column(unique = true, nullable = false)
	private String email;

	private String dob;

	private boolean userCreationStatus = false;

	private boolean walletCreationStatus = false;

	private boolean cardCreationStatus = false;

	private String idNumber;

	private String idType;

	private String failedReason;

	private boolean failedStatus;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public boolean isUserCreationStatus() {
		return userCreationStatus;
	}

	public void setUserCreationStatus(boolean userCreationStatus) {
		this.userCreationStatus = userCreationStatus;
	}

	public boolean isWalletCreationStatus() {
		return walletCreationStatus;
	}

	public void setWalletCreationStatus(boolean walletCreationStatus) {
		this.walletCreationStatus = walletCreationStatus;
	}

	public boolean isCardCreationStatus() {
		return cardCreationStatus;
	}

	public void setCardCreationStatus(boolean cardCreationStatus) {
		this.cardCreationStatus = cardCreationStatus;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	public boolean isFailedStatus() {
		return failedStatus;
	}

	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}

}