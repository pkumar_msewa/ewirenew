package com.cashiercard.bus.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.enums.Device;
import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.ITravelBusApi;
import com.msscard.app.model.request.BusGetTxnIdRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.request.CommonRequest;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Travel/Bus")
public class TravelBusController {
	private final ITravelBusApi iTravelBusApi;
	
	public  TravelBusController(ITravelBusApi iTravelBusApi) {
		this.iTravelBusApi= iTravelBusApi;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/SaveSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusSaveSeatDetailsResponse> saveSeatDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BusSaveSeatDetailsRequest dto) {
		BusSaveSeatDetailsResponse result = new BusSaveSeatDetailsResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					result = iTravelBusApi.saveSeatDetails(dto, result);
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<BusSaveSeatDetailsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetTransactionIdUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getTxnIdUpdated(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BusGetTxnIdRequest dto) {
		CommonResponse result = new CommonResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					result = iTravelBusApi.getTxnIdUpdated(dto, result);
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetAllTicketsByUser", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getAllTicketsByUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody CommonRequest dto) {
		CommonResponse result = new CommonResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					result = iTravelBusApi.getAllBookTickets(dto.getSessionId(), result);
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	

}
