package com.msscards.metadatas;

public class URLMetadatas {

	static final String MMCREATE_USER = "MatchMove/CreateNewUser";
	static final String MMCREATE_CARD = "MatchMove/CreateCard";

	static final String MMCARD_FETCH = "MatchMove/getCardDetails";

	static final String BASE_URL = "http://66.207.206.54:8089/MDEX/Api/v1/Client/Android/en/";

	static final String CLIENT_KEY = "CD8376D49B7104547D62E2E63FACD498A56637F";
	static final String CLIENT_TOKEN = "MTUwNzAxNjQwMjA3NjM1NjAwMDAwMDAwMDAwMTE=";

	public static final String BUCKET_NAME = "cashiercardsimages";
	public static final String BUCKET_NAME_TEST = "cashiercardtest";
	public static final String CSV_BUCKET_NAME = "corporatelive";

	public static final String AWS_SECRET_ACCESS_KEY = "j5uDVcFoYEqHxn0yK8+x2jjLAed1j4ovF8adyz6X";
	public static final String AWS_ACCESS_KEY_ID = "AKIAJXXGFRZLER36WUSQ";

	public static final String PATH = "/usr/local/tomcat/webapps/ROOT/resources/excelSheets/";
	public static final String EXCEL_PATH = "/usr/local/tomcat/webapps/ROOT/resources/external_files/";

	// LIVE
	public static final String IOS_CERT = "/usr/local/tomcat/webapps/ROOT/resources/extra/Cashier_p12PushCert.p12";

	public static final String EWIRE_MASTER_CSV_KEY_PATH_BULKREGISTER_CORPORATE = "BLKREGISTER/123456/";
	public static final String EWIRE_MASTER_CSV_KEY_PATH_BULKKYC_CORPORATE = "BLKKYCUPLOAD/123456/";
	public static final String EWIRE_MASTER_CSV_KEY_PATH_BULKCARDLOAD_CORPORATE = "BLKCARDLOAD/123456/";
	public static final String EWIRE_CSV_KEY_PATH_BULKCARDASSIGN_GROUP = "BLKCARDASSIGNGROUP/123456/";
	

	public static String getMmcreateUser() {
		return MMCREATE_USER;
	}

	public static String getMmcreateCard() {
		return MMCREATE_CARD;
	}

	public static String getBaseUrl() {
		return BASE_URL;
	}

	public static String getClientKey() {
		return CLIENT_KEY;
	}

	public static String getClientToken() {
		return CLIENT_TOKEN;
	}

	public static String getMmcardFetch() {
		return MMCARD_FETCH;
	}

}
