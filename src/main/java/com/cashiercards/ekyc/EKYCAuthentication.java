package com.cashiercards.ekyc;

import java.io.UnsupportedEncodingException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class EKYCAuthentication {

	private static final String secret_key="785cecaea7333fc9698b96eec13707c7";
//	private static final String license_key="cAukq2IEYyGx2+/7e1AVtONq+LmOOw7ewy77mumXW1Br1Wlo0ADMuhBKEw89NN/QL54LNToTPwKG71KhkIXHLS64Y7WfbEfP3hR4/5oO1LAyJODdIiIAQW4G0tsZWStxX+wyX5S0ULGCuS4A2XI8+kvbNFnhBaqil/A2oyBom0k=";
	private static final String URL_EKYC="https://auacon.infotelconnect.com/EkycAuth.aspx";
	
	public static void  testEKYC() throws UnsupportedEncodingException{
		String trnId=String.valueOf(System.currentTimeMillis());
	String authData="E|"+trnId+"|663750486979|http://localhost:8030/FailureAU|http://localhost:8030/SuccessAU|||||||||||";
	byte[] iv = new byte[128/8];
	System.err.println(new String(iv,"UTF-8"));
String encValue = null;
	encValue = EKYCUtil.encrypt(secret_key,new String(iv), authData);
System.err.println(encValue);
	Client client=Client.create();
	WebResource clientResponse=client.resource(URL_EKYC);
	ClientResponse resp=clientResponse.post(ClientResponse.class, encValue);
	String strResp=resp.getEntity(String.class);
	System.err.println(strResp);
	
	}	
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		testEKYC();
	}
}
