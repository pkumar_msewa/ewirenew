package com.cashiercards.ekyc;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public class EKYCUtil {

		/**
		 * DECRYPT RESPONSE
		 * */
	
		public static String decrypt(String key, String initVector, String encrypted) {
		try {
		IvParameterSpec iv = new IvParameterSpec(
		initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"),
		"AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted.getBytes()));
		return new String(original);
		 }
		catch (Exception ex)
		 {
		ex.printStackTrace();
		 }
		return null;
}
		
		
		/**
		 * ENCRYPT REQUEST
		 * */
		
		public static String encrypt(String key, String initVector, String value) {
			try {
			IvParameterSpec iv = new IvParameterSpec(
			initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"),
			"AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(value.getBytes());
			String encryptedString = new String(Base64.encodeBase64(encrypted));
			return encryptedString;
			 }
			catch (Exception ex)
			 {
			ex.printStackTrace();
			 }
			 return null;
			}
		
		
			}

