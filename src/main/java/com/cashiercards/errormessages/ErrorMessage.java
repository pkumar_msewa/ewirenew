package com.cashiercards.errormessages;

public class ErrorMessage {
	
	public static final String SERVICE_UNAVAILABLE_ERROR = "Service unavailable";
	
	public static final String DEVICE_MSG = "Not a valid device";
	
	public static final String AUTHORITY_MSG = "Unauthorised User";

	public static final String TRY_AGAIN_MSG = "Please try again later.";
	
	public static final String FAILED_MSG = "Failed, Unauthorized user.";
	
	public static final String SESSION_LOGOUT_ERROR = "Session expired. Please, login and try again.";
	
	public static final String DONATEE = "Donatee Added Successfully";
	
	public static final String DONATEE_EXIST = "Donatee already Exist";
	
	public static final String CARDS = "Please generate your virtual card";
	
	public static final String PHYSICAL_CARD = "Physical card is already active";
	
	public static final String PHYSICAL_CARD_BLOCKED = "Physical card is already blocked";
	
	public static final String VIRTUAL_CARD_BLOCKED = "Virtual card is already blocked";

}
