
package com.cashiercards.giftcards.api.impl;

import org.json.JSONException;
import org.json.JSONObject;

import com.cashiercards.giftcards.WoohooUtil;
import com.cashiercards.giftcards.api.WoohooApi;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class WoohooApiImpl implements WoohooApi{

	@Override
	public String fetchList() {
	
		Client client = Client.create();
		WebResource webResource	= client.resource(WoohooUtil.PRODUCT_LIST_URL);
		ClientResponse clientResponse=webResource.accept("application/json").type("application/json").get(ClientResponse.class);
		String strResponse=clientResponse.getEntity(String.class);
		if(strResponse!=null){
			try {
				JSONObject jsonObject=new JSONObject(strResponse);
				if(jsonObject.getBoolean("success")){
					return strResponse;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
		
		
	}

	
}
