package com.cashiercards.giftcards.response;

import com.msscard.model.ResponseStatus;

public class GiftCardListResponse {

	private String code;
	
	private String message;
	
	private String response;

	public String getCode() {
		return code;
	}

	public void setCode(ResponseStatus code) {
		this.code = code.getValue();
		this.message=code.getKey();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	
}
