package com.cashiercards.fcm;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.fcm.constants.FCMConstants;
import com.msscard.app.model.request.FCMRequestDTO;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.repositories.UserSessionRepository;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Controller
@RequestMapping("/")
public class FCMController {

	private final UserSessionRepository userSessionRepository;

	
	
	public FCMController(UserSessionRepository userSessionRepository) {
		super();
		this.userSessionRepository = userSessionRepository;
	}



	@RequestMapping(value = "/Notification", method = RequestMethod.POST, produces = {
			   MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
			 ResponseEntity<String> sendNotificationToParticularUser(@RequestBody FCMRequestDTO fcmRequest) {
			    try {
			     System.err.println("inside send notification");
			 	UserSession userSession = userSessionRepository.findByActiveSessionId(fcmRequest.getSessionId());
				if(userSession!=null){
				MUser user=userSession.getUser();
				fcmRequest.setGcmId(user.getGcmId());
			    Client client = Client.create();
			    client.addFilter(new LoggingFilter(System.out));
			   WebResource webResource = client.resource(FCMConstants.SEND_NOTIFICATION);
			    ClientResponse clientResponse = webResource.accept("application/json").type("application/json").header("Authorization","key="+FCMConstants.APP_KEY).post(ClientResponse.class,fcmRequest.setToJson());
			    String strResponse = clientResponse.getEntity(String.class);
			    System.err.println("response of gcm ::" + strResponse);
			   if (clientResponse.getStatus() == 200) {
			    String response = strResponse;
			    return new ResponseEntity<String>(response, HttpStatus.OK);
			   }
			  }
			    }catch (Exception e) {
			   e.printStackTrace();
			   return new ResponseEntity<String>("", HttpStatus.OK);
			  }
			    return new ResponseEntity<String>("", HttpStatus.OK);
			 }
	
}
