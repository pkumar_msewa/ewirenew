package com.cashiercards.cron;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.MediaType;

import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkRegisterGroup;
import com.msscard.app.api.ILoadMoneyApi;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.AddressResponseDTO;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.BulkKYCCorporate;
import com.msscard.entity.BulkLoadMoney;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.CorporateFileCatalogue;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.GroupSms;
import com.msscard.entity.LoadWalletByVARequest;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionType;
import com.msscard.model.UserType;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.RegisterError;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.BulkCardAssignmentGroupRepository;
import com.msscard.repositories.BulkKYCCorporateRepository;
import com.msscard.repositories.BulkLoadMoneyRepository;
import com.msscard.repositories.BulkRegisterGroupRepository;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.CorporateFileCatalogueRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.LoadWalletByVARequestRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.MailTemplate;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.CommonValidation;
import com.msscard.validation.LoadMoneyValidation;
import com.msscard.validation.RegisterValidation;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class AutoReverse {
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository cardRepository;
	private final MUserRespository userRespository;
	private final ISMSSenderApi senderApi;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final CorporateFileCatalogueRepository corporateFileCatalogueRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final IUserApi userApi;
	private final RegisterValidation registerValidation;
	private final ITransactionApi transactionApi;
	private final MTransactionRepository transactionRepository;
	private final BulkLoadMoneyRepository bulkLoadMoneyRepository;
	private final IMdexApi mdexApi;
	private final MServiceRepository mServiceRepository;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MKycRepository mKycRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final GroupSmsRepository groupSmsRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final IMailSenderApi mailSenderApi;
	private final BulkKYCCorporateRepository bulkKYCCorporateRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final LoadWalletByVARequestRepository loadWalletByVARequestRepository;
	private final ILoadMoneyApi loadMoneyApi;
	private final GroupBulkKycRepository groupBulkKycRepository;
	private final BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository;
	private final BulkRegisterGroupRepository bulkRegisterGroupRepository;

	public AutoReverse(SendMoneyDetailsRepository sendMoneyDetailsRepository, IMatchMoveApi matchMoveApi,
			MMCardRepository cardRepository, MUserRespository userRespository, ISMSSenderApi senderApi,
			CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			CorporateFileCatalogueRepository corporateFileCatalogueRepository,
			BulkRegisterRepository bulkRegisterRepository, IUserApi userApi, RegisterValidation registerValidation,
			ITransactionApi transactionApi, MTransactionRepository transactionRepository,
			BulkLoadMoneyRepository bulkLoadMoneyRepository, IMdexApi mdexApi, MServiceRepository mServiceRepository,
			LoadMoneyValidation loadMoneyValidation, MKycRepository mKycRepository,
			MPQAccountTypeRepository mPQAccountTypeRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			GroupSmsRepository groupSmsRepository, GroupFileCatalogueRepository groupFileCatalogueRepository,
			IMailSenderApi mailSenderApi, BulkKYCCorporateRepository bulkKYCCorporateRepository,
			MTransactionRepository mTransactionRepository, MatchMoveWalletRepository matchMoveWalletRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository,
			LoadWalletByVARequestRepository loadWalletByVARequestRepository, ILoadMoneyApi loadMoneyApi,
			GroupBulkKycRepository groupBulkKycRepository,
			BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository,
			BulkRegisterGroupRepository bulkRegisterGroupRepository) {
		this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		this.matchMoveApi = matchMoveApi;
		this.cardRepository = cardRepository;
		this.userRespository = userRespository;
		this.senderApi = senderApi;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.corporateFileCatalogueRepository = corporateFileCatalogueRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.userApi = userApi;
		this.registerValidation = registerValidation;
		this.transactionApi = transactionApi;
		this.transactionRepository = transactionRepository;
		this.bulkLoadMoneyRepository = bulkLoadMoneyRepository;
		this.mdexApi = mdexApi;
		this.mServiceRepository = mServiceRepository;
		this.loadMoneyValidation = loadMoneyValidation;
		this.mKycRepository = mKycRepository;
		this.mPQAccountTypeRepository = mPQAccountTypeRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.groupSmsRepository = groupSmsRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.mailSenderApi = mailSenderApi;
		this.bulkKYCCorporateRepository = bulkKYCCorporateRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.loadWalletByVARequestRepository = loadWalletByVARequestRepository;
		this.loadMoneyApi = loadMoneyApi;
		this.groupBulkKycRepository = groupBulkKycRepository;
		this.bulkCardAssignmentGroupRepository = bulkCardAssignmentGroupRepository;
		this.bulkRegisterGroupRepository = bulkRegisterGroupRepository;
	}

	public void checkForMoneyTransfer() {
		System.err.println("in cron tab");
		List<SendMoneyDetails> sendDetails = sendMoneyDetailsRepository.getTransactionWithPendingStatus();
		if (sendDetails != null) {
			for (SendMoneyDetails sendMoneyDetails : sendDetails) {
				String refNo = sendMoneyDetails.getTransactionId();
				String recipeintNo = sendMoneyDetails.getRecipientNo();
				String amount = sendMoneyDetails.getAmount();
				MUser sender = sendMoneyDetails.getUser();
				MUser recipientUser = userRespository.findByUsername(recipeintNo);

				WalletResponse resp = matchMoveApi.acknowledgeFundTransfer(amount, refNo, recipeintNo);
				if (resp != null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
					MMCards vCard = cardRepository.getVirtualCardsByCardUser(recipientUser);
					MMCards pCard = cardRepository.getPhysicalCardByUser(recipientUser);
					WalletResponse tranferResponse = new WalletResponse();
					if (pCard != null && Status.Active.getValue().equalsIgnoreCase(pCard.getStatus())) {
						tranferResponse = matchMoveApi.transferFundsToMMCard(recipientUser, amount, pCard.getCardId());
					} else if (Status.Active.getValue().equalsIgnoreCase(vCard.getStatus())) {
						tranferResponse = matchMoveApi.transferFundsToMMCard(recipientUser, amount, vCard.getCardId());
					}

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(tranferResponse.getCode())) {
						SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
						trxDetails.setStatus(ResponseStatus.SUCCESS.getValue());
						trxDetails.setChecked(true);
						sendMoneyDetailsRepository.save(trxDetails);

						senderApi.sendUserSMSFund(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS, sender, amount,
								recipientUser);
						senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS_RECEIVER,
								recipientUser, amount + " from " + sender.getUsername());
						mailSenderApi.sendFundTransfer("Fund Transfer Successful", MailTemplate.FUND_TRANSFER, sender,
								recipientUser, sendMoneyDetails);
					}
				} else {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setChecked(true);
					sendMoneyDetailsRepository.save(trxDetails);
				}
			}
		}
	}

	public void recheckTransactions() {
		System.err.println("2nd cron running....");
		List<SendMoneyDetails> sendMoneyDetails = sendMoneyDetailsRepository.getTransactionWithFailedStatus();
		if (sendMoneyDetails != null) {
			for (SendMoneyDetails sendMoneyDetails2 : sendMoneyDetails) {
				String refNo = sendMoneyDetails2.getTransactionId();
				String recipeintNo = sendMoneyDetails2.getRecipientNo();
				String amount = sendMoneyDetails2.getAmount();
				MUser sender = sendMoneyDetails2.getUser();
				MUser recipientUser = userRespository.findByUsername(recipeintNo);

				WalletResponse resp = matchMoveApi.acknowledgeFundTransfer(amount, refNo, recipeintNo);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
					MMCards vCard = cardRepository.getVirtualCardsByCardUser(recipientUser);
					MMCards pCard = cardRepository.getPhysicalCardByUser(recipientUser);
					WalletResponse tranferResponse = new WalletResponse();

					if (pCard != null && Status.Active.getValue().equalsIgnoreCase(pCard.getStatus())) {
						tranferResponse = matchMoveApi.transferFundsToMMCard(recipientUser, amount, pCard.getCardId());
					} else if (Status.Active.getValue().equalsIgnoreCase(vCard.getStatus())) {
						tranferResponse = matchMoveApi.transferFundsToMMCard(recipientUser, amount, vCard.getCardId());
					}

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(tranferResponse.getCode())) {
						SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
						trxDetails.setStatus(ResponseStatus.SUCCESS.getValue());
						trxDetails.setChecked(true);
						sendMoneyDetailsRepository.save(trxDetails);

						senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS, sender, amount);
						senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SENDMONEY_SUCCESS_RECEIVER,
								recipientUser, amount + " from " + sender.getUsername());
					}
				} else {
					SendMoneyDetails trxDetails = sendMoneyDetailsRepository.getByTransactionRefNo(refNo);
					trxDetails.setChecked(true);
					trxDetails.setError(resp.getMessage());
					trxDetails.setStatus("Failed");
					sendMoneyDetailsRepository.save(trxDetails);

					WalletResponse refund = matchMoveApi.cancelFundTransfer(recipientUser, amount, refNo);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(refund.getCode())) {
						MMCards vCard = cardRepository.getVirtualCardsByCardUser(sender);
						MMCards pCard = cardRepository.getPhysicalCardByUser(sender);
						WalletResponse refundtranferResponse = new WalletResponse();

						if (pCard != null && Status.Active.getValue().equalsIgnoreCase(pCard.getStatus())) {
							refundtranferResponse = matchMoveApi.transferFundsToMMCard(sender, amount,
									pCard.getCardId());
						} else if (Status.Active.getValue().equalsIgnoreCase(vCard.getStatus())) {
							refundtranferResponse = matchMoveApi.transferFundsToMMCard(sender, amount,
									vCard.getCardId());
						}

						if (refundtranferResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REFUND_SENDMONEY, sender, amount);
						}
					}
				}
			}
		}
	}

	public void bulkSms() {
		System.err.println("bulk SMS");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkSms();
		System.err.println("This is file size::::::::" + fileList.size());
		if (fileList != null) {
			for (GroupFileCatalogue corporateFileCatalogue : fileList) {
				System.err.println(corporateFileCatalogue);
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKSMS")) {
							System.err.println("hiii");
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;

							try {
								System.err.println("This is file Path::::::____________:" + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadSms(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											System.err.println("Length of Array>>>>>>>>>>>>" + jArray.length());
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												String mobile = jObject.getString("mobile");
												String message = jObject.getString("message");
												userApi.sendSingleGroupSMS(mobile, message);
											}
										}
										corporateFileCatalogue.setSchedulerStatus(true);
										groupFileCatalogueRepository.save(corporateFileCatalogue);
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

	public void sendgroupnotification() {
		try {
			System.err.println("in group sms cron");
			List<GroupSms> groupSmsSingle = groupSmsRepository.getAllSingleNumbers(false, true,
					Status.Inactive.getValue());
			if (groupSmsSingle != null) {
				for (GroupSms gs : groupSmsSingle) {
					if (gs.getStatus().equals(Status.Inactive.getValue())) {
						userApi.sendSingleGroupSMS(gs.getMobile(), gs.getText());
						gs.setCronStatus(true);
						gs.setStatus(Status.Active.getValue());
						groupSmsRepository.save(gs);
					}
				}
			}
			List<GroupSms> groupSms = groupSmsRepository.getAllGroupNumbers(false, false, Status.Inactive.getValue());
			if (groupSms != null) {
				for (GroupSms g : groupSms) {
					if (g.getStatus().equals(Status.Inactive.getValue())) {
						userApi.sendSingleGroupSMS(g.getMobile(), g.getText());
						g.setCronStatus(true);
						g.setStatus(Status.Active.getValue());
						groupSmsRepository.save(g);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkIpayStatusnew() {
		Calendar cal = Calendar.getInstance();
		Date present = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			List<MTransaction> checkStatus = transactionRepository.getAllSuspiciousCustom(sdf.format(present));
			for (MTransaction mTransaction : checkStatus) {
				MdexTransactionRequestDTO req = new MdexTransactionRequestDTO();
				String trx = mTransaction.getTransactionRefNo();
				String[] splittedtrx = trx.split("D");
				req.setTransactionId(splittedtrx[0]);
				MdexTransactionResponseDTO statusresp = mdexApi.checkStatus(req);
				if (statusresp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					mTransaction.setSuspicious(false);
					mTransaction.setMdexTransactionStatus(Status.Success.getValue());
					transactionRepository.save(mTransaction);
				} else if (statusresp.getCode().equalsIgnoreCase(ResponseStatus.FAILURE.getValue())) {
					MUser user = userRespository.findByAccountDetails(mTransaction.getAccount());
					String activeCardId = null;
					List<MMCards> cardList = cardRepository.getCardsListByUser(user);
					if (cardList != null && cardList.size() > 0) {
						for (MMCards mmCards : cardList) {
							if (mmCards.isHasPhysicalCard()) {
								if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
									activeCardId = mmCards.getCardId();
								}
							} else {
								if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
									activeCardId = mmCards.getCardId();
								}
							}
						}
						WalletResponse walletResp = matchMoveApi.reversalToWalletForRechargeFailure(user,
								String.valueOf(mTransaction.getAmount()), mTransaction.getService().getCode(),
								mTransaction.getTransactionRefNo(), mTransaction.getDescription());
						if (walletResp != null) {
							String code = walletResp.getCode();
							if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(user,
										String.valueOf(mTransaction.getAmount()), activeCardId);
								if (tranferCard != null
										&& tranferCard.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									mTransaction.setStatus(Status.Refunded);
									String authRefNo = walletResp.getAuthRetrivalNo();
									mTransaction.setRetrivalReferenceNo(authRefNo);
									mTransaction.setSuspicious(false);
									mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
									mTransaction.setSuspicious(false);
								} else {
									mTransaction.setStatus(Status.Refund_Failed);
									mTransaction.setSuspicious(false);
									mTransaction.setRemarks(tranferCard.getMessage());
									mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
								}
							} else {
								mTransaction.setStatus(Status.Refund_Failed);
								mTransaction.setSuspicious(false);
								mTransaction.setRemarks(walletResp.getMessage());
								mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
							}
						} else {
							mTransaction.setStatus(Status.Refund_Failed);
							mTransaction.setSuspicious(false);
							mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
							mTransaction.setRemarks(statusresp.getMessage());
						}
						transactionRepository.save(mTransaction);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bulkKYCUpload() {
		List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkRegister();
		if (fileList != null) {
			for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
				System.err.println(corporateFileCatalogue);
				if (corporateFileCatalogue.isFileRejectionStatus() == false) {
					if (corporateFileCatalogue.isReviewStatus() == true) {
						if (corporateFileCatalogue.getCategoryType().equalsIgnoreCase("BLKKYCUPLOAD")) {
							System.err.println("hiii");
							String absPath = corporateFileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;

							try {
								System.err.println(
										"This is file Path::::::::::::______________________________:" + filePath);
								JSONObject fileJsonObject = CSVReader.readCsvBulkKycUpload(filePath);
								System.err.println(fileJsonObject);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											System.err.println(
													"Length of Array>>>>>>>>>>>>>>>>>>>>>>>>" + jArray.length());
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												MPQAccountType accountTypeKYC = mPQAccountTypeRepository
														.findByCode("KYC");
												MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository
														.findByCode("NONKYC");

												String mobile = jObject.getString("mobile");
												String address1 = jObject.getString("address1");
												String address2 = jObject.getString("address2");
												String pinCode = jObject.getString("pinCode");
												String documentUrl = jObject.getString("documentUrl");
												String documentUrl1 = jObject.getString("documentUrl1");
												String idType = jObject.getString("idType");
												String idNumber = jObject.getString("idNumber");
												System.err.println("hey m here");
												MUser user = userApi.findByUserName(mobile);
												if (user != null) {
													MKycDetail kycDetail = mKycRepository.findByUser(user);
													if (kycDetail == null) {
														kycDetail = new MKycDetail();
														kycDetail.setAddress1(address1);
														kycDetail.setAddress2(address2);
														kycDetail.setIdImage(documentUrl);
														kycDetail.setIdImage1(documentUrl1);
														kycDetail.setIdType(idType);
														kycDetail.setIdNumber(idNumber);
														kycDetail.setPinCode(pinCode);
														kycDetail.setUser(user);
														kycDetail.setUserType(UserType.User);
														AddressResponseDTO addResp = CommonUtil.getAddress(pinCode);
														if (addResp.getCode()
																.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
															kycDetail.setCity(addResp.getCity());
															kycDetail.setState(addResp.getState());
															kycDetail.setCountry(addResp.getCountry());
															UserKycResponse userKyc = matchMoveApi
																	.kycUserMMCorporate(kycDetail);
															if (userKyc.getCode().equalsIgnoreCase(
																	ResponseStatus.SUCCESS.getValue())) {
																UserKycResponse userIdDetails = matchMoveApi
																		.setIdDetailsCorporate(kycDetail);
																if (userIdDetails.getCode().equalsIgnoreCase(
																		ResponseStatus.SUCCESS.getValue())) {
																	UserKycResponse image1Upload = matchMoveApi
																			.setImagesForKyc1(kycDetail);
																	if (image1Upload.getCode().equalsIgnoreCase(
																			ResponseStatus.SUCCESS.getValue())) {
																		UserKycResponse imageUpload2 = matchMoveApi
																				.setImagesForKyc2(kycDetail);
																		if (imageUpload2.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse approvalResp = matchMoveApi
																					.tempConfirmKycCorporate(kycDetail);
																			if (approvalResp.getCode().equalsIgnoreCase(
																					ResponseStatus.SUCCESS
																							.getValue())) {
																				MPQAccountDetails accDetails = user
																						.getAccountDetail();
																				if (accDetails != null) {
																					MPQAccountType accountType = mPQAccountTypeRepository
																							.findByCode("KYC");
																					accDetails.setAccountType(
																							accountType);
																					mPQAccountDetailRepository
																							.save(accDetails);
																					kycDetail.setAccountType(
																							accountType);
																					kycDetail.setRejectionStatus(false);
																					mKycRepository.save(kycDetail);

																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							"No Account found");
																					mKycRepository.save(kycDetail);

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						approvalResp.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					imageUpload2.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				image1Upload.getMessage());
																		mKycRepository.save(kycDetail);
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			userIdDetails.getMessage());
																	mKycRepository.save(kycDetail);
																}
															} else {
																kycDetail.setAccountType(accountTypeNONKYC);
																kycDetail.setRejectionStatus(true);
																kycDetail.setRejectionReason(userKyc.getMessage());
																mKycRepository.save(kycDetail);
															}

														} else {
															kycDetail.setCity("Bangalore");
															kycDetail.setState("Karnataka");
															kycDetail.setCountry("India");

															UserKycResponse userKyc = matchMoveApi
																	.kycUserMMCorporate(kycDetail);
															if (userKyc.getCode().equalsIgnoreCase(
																	ResponseStatus.SUCCESS.getValue())) {
																UserKycResponse userIdDetails = matchMoveApi
																		.setIdDetailsCorporate(kycDetail);
																if (userIdDetails.getCode().equalsIgnoreCase(
																		ResponseStatus.SUCCESS.getValue())) {
																	UserKycResponse image1Upload = matchMoveApi
																			.setImagesForKyc1(kycDetail);
																	if (image1Upload.getCode().equalsIgnoreCase(
																			ResponseStatus.SUCCESS.getValue())) {
																		UserKycResponse imageUpload2 = matchMoveApi
																				.setImagesForKyc2(kycDetail);
																		if (imageUpload2.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse approvalResp = matchMoveApi
																					.tempConfirmKycCorporate(kycDetail);
																			if (approvalResp.getCode().equalsIgnoreCase(
																					ResponseStatus.SUCCESS
																							.getValue())) {
																				MPQAccountDetails accDetails = user
																						.getAccountDetail();
																				if (accDetails != null) {
																					MPQAccountType accountType = mPQAccountTypeRepository
																							.findByCode("KYC");
																					accDetails.setAccountType(
																							accountType);
																					mPQAccountDetailRepository
																							.save(accDetails);
																					kycDetail.setAccountType(
																							accountType);
																					kycDetail.setRejectionStatus(false);
																					mKycRepository.save(kycDetail);

																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							"No Account found");
																					mKycRepository.save(kycDetail);

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						approvalResp.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					imageUpload2.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				image1Upload.getMessage());
																		mKycRepository.save(kycDetail);
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			userIdDetails.getMessage());
																	mKycRepository.save(kycDetail);
																}
															} else {
																kycDetail.setAccountType(accountTypeNONKYC);
																kycDetail.setRejectionStatus(true);
																kycDetail.setRejectionReason(userKyc.getMessage());
																mKycRepository.save(kycDetail);
															}

														}
													} else {
														MPQAccountDetails accountDetails = kycDetail.getUser()
																.getAccountDetail();
														if (accountDetails != null) {
															MPQAccountType accountType = accountDetails
																	.getAccountType();
															if (accountType.getCode().equalsIgnoreCase("NONKYC")) {
																mKycRepository.delete(kycDetail);
																MKycDetail kycDetails = new MKycDetail();
																kycDetails.setAddress1(address1);
																kycDetails.setAddress2(address2);
																kycDetails.setIdImage(documentUrl);
																kycDetails.setIdImage1(documentUrl);
																kycDetails.setIdType(idType);
																kycDetails.setIdNumber(idNumber);
																kycDetails.setPinCode(pinCode);
																kycDetail.setUser(user);
																kycDetail.setUserType(UserType.User);
																AddressResponseDTO addResp = CommonUtil
																		.getAddress(pinCode);
																if (addResp.getCode().equalsIgnoreCase(
																		ResponseStatus.SUCCESS.getValue())) {
																	kycDetails.setCity(addResp.getCity());
																	kycDetails.setState(addResp.getState());
																	kycDetails.setCountry(addResp.getCountry());
																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase(
																			ResponseStatus.SUCCESS.getValue())) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode().equalsIgnoreCase(
																					ResponseStatus.SUCCESS
																							.getValue())) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase(
																								ResponseStatus.SUCCESS
																										.getValue())) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase(
																									ResponseStatus.SUCCESS
																											.getValue())) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							accDetails.setAccountType(
																									accountTypeKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountTypeKYC);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);
																	}

																} else {
																	kycDetails.setCity("Bangalore");
																	kycDetails.setState("Karnataka");
																	kycDetails.setCountry("India");

																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (userKyc.getCode().equalsIgnoreCase(
																			ResponseStatus.SUCCESS.getValue())) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (userIdDetails.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (image1Upload.getCode().equalsIgnoreCase(
																					ResponseStatus.SUCCESS
																							.getValue())) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (imageUpload2.getCode()
																						.equalsIgnoreCase(
																								ResponseStatus.SUCCESS
																										.getValue())) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (approvalResp.getCode()
																							.equalsIgnoreCase(
																									ResponseStatus.SUCCESS
																											.getValue())) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							accDetails.setAccountType(
																									accountTypeKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountTypeKYC);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);
																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);
																	}
																}
															} else {
																MPQAccountDetails accDetails = user.getAccountDetail();
																UserKycResponse kycResponse = matchMoveApi
																		.checkKYCStatus(user);
																if (!("Approved"
																		.equalsIgnoreCase(kycResponse.getStatus())
																		|| ResponseStatus.SUCCESS.getValue()
																				.equalsIgnoreCase(
																						kycResponse.getCode()))) {
																	mKycRepository.delete(kycDetail);
																	MKycDetail kycDetails = new MKycDetail();
																	kycDetails.setAddress1(address1);
																	kycDetails.setAddress2(address2);
																	kycDetails.setIdImage(documentUrl);
																	kycDetails.setIdImage1(documentUrl);
																	kycDetails.setIdType(idType);
																	kycDetails.setIdNumber(idNumber);
																	kycDetails.setPinCode(pinCode);
																	kycDetail.setUser(user);
																	kycDetail.setUserType(UserType.User);
																	AddressResponseDTO addResp = CommonUtil
																			.getAddress(pinCode);
																	if (addResp.getCode().equalsIgnoreCase(
																			ResponseStatus.SUCCESS.getValue())) {
																		kycDetails.setCity(addResp.getCity());
																		kycDetails.setState(addResp.getState());
																		kycDetails.setCountry(addResp.getCountry());
																		UserKycResponse userKyc = matchMoveApi
																				.kycUserMMCorporate(kycDetail);
																		if (userKyc.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse userIdDetails = matchMoveApi
																					.setIdDetailsCorporate(kycDetail);
																			if (userIdDetails.getCode()
																					.equalsIgnoreCase(
																							ResponseStatus.SUCCESS
																									.getValue())) {
																				UserKycResponse image1Upload = matchMoveApi
																						.setImagesForKyc1(kycDetail);
																				if (image1Upload.getCode()
																						.equalsIgnoreCase(
																								ResponseStatus.SUCCESS
																										.getValue())) {
																					UserKycResponse imageUpload2 = matchMoveApi
																							.setImagesForKyc2(
																									kycDetail);
																					if (imageUpload2.getCode()
																							.equalsIgnoreCase(
																									ResponseStatus.SUCCESS
																											.getValue())) {
																						UserKycResponse approvalResp = matchMoveApi
																								.tempConfirmKycCorporate(
																										kycDetail);
																						if (approvalResp.getCode()
																								.equalsIgnoreCase(
																										ResponseStatus.SUCCESS
																												.getValue())) {
																							if (accDetails != null) {
																								accDetails
																										.setAccountType(
																												accountTypeKYC);
																								mPQAccountDetailRepository
																										.save(accDetails);
																								kycDetail
																										.setAccountType(
																												accountTypeKYC);
																								kycDetail
																										.setRejectionStatus(
																												false);
																								mKycRepository.save(
																										kycDetail);
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												"No Account found");
																								mKycRepository.save(
																										kycDetail);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											approvalResp
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);
																							accDetails.setAccountType(
																									accountTypeNONKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								imageUpload2
																										.getMessage());
																						mKycRepository.save(kycDetail);
																						accDetails.setAccountType(
																								accountTypeNONKYC);
																						mPQAccountDetailRepository
																								.save(accDetails);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							image1Upload.getMessage());
																					mKycRepository.save(kycDetail);
																					accDetails.setAccountType(
																							accountTypeNONKYC);
																					mPQAccountDetailRepository
																							.save(accDetails);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userIdDetails.getMessage());
																				mKycRepository.save(kycDetail);
																				accDetails.setAccountType(
																						accountTypeNONKYC);
																				mPQAccountDetailRepository
																						.save(accDetails);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userKyc.getMessage());
																			mKycRepository.save(kycDetail);
																			accDetails
																					.setAccountType(accountTypeNONKYC);
																			mPQAccountDetailRepository.save(accDetails);
																		}
																	} else {
																		kycDetails.setCity("Bangalore");
																		kycDetails.setState("Karnataka");
																		kycDetails.setCountry("India");

																		UserKycResponse userKyc = matchMoveApi
																				.kycUserMMCorporate(kycDetail);
																		if (userKyc.getCode().equalsIgnoreCase(
																				ResponseStatus.SUCCESS.getValue())) {
																			UserKycResponse userIdDetails = matchMoveApi
																					.setIdDetailsCorporate(kycDetail);
																			if (userIdDetails.getCode()
																					.equalsIgnoreCase(
																							ResponseStatus.SUCCESS
																									.getValue())) {
																				UserKycResponse image1Upload = matchMoveApi
																						.setImagesForKyc1(kycDetail);
																				if (image1Upload.getCode()
																						.equalsIgnoreCase(
																								ResponseStatus.SUCCESS
																										.getValue())) {
																					UserKycResponse imageUpload2 = matchMoveApi
																							.setImagesForKyc2(
																									kycDetail);
																					if (imageUpload2.getCode()
																							.equalsIgnoreCase(
																									ResponseStatus.SUCCESS
																											.getValue())) {
																						UserKycResponse approvalResp = matchMoveApi
																								.tempConfirmKycCorporate(
																										kycDetail);
																						if (approvalResp.getCode()
																								.equalsIgnoreCase(
																										ResponseStatus.SUCCESS
																												.getValue())) {
																							if (accDetails != null) {
																								accDetails
																										.setAccountType(
																												accountTypeKYC);
																								mPQAccountDetailRepository
																										.save(accDetails);
																								kycDetail
																										.setAccountType(
																												accountTypeKYC);
																								kycDetail
																										.setRejectionStatus(
																												false);
																								mKycRepository.save(
																										kycDetail);
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												"No Account found");
																								mKycRepository.save(
																										kycDetail);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											approvalResp
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);
																							accDetails.setAccountType(
																									accountTypeNONKYC);
																							mPQAccountDetailRepository
																									.save(accDetails);
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								imageUpload2
																										.getMessage());
																						mKycRepository.save(kycDetail);
																						accDetails.setAccountType(
																								accountTypeNONKYC);
																						mPQAccountDetailRepository
																								.save(accDetails);
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							image1Upload.getMessage());
																					mKycRepository.save(kycDetail);
																					accDetails.setAccountType(
																							accountTypeNONKYC);
																					mPQAccountDetailRepository
																							.save(accDetails);
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userIdDetails.getMessage());
																				mKycRepository.save(kycDetail);
																				accDetails.setAccountType(
																						accountTypeNONKYC);
																				mPQAccountDetailRepository
																						.save(accDetails);
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userKyc.getMessage());
																			mKycRepository.save(kycDetail);
																			accDetails
																					.setAccountType(accountTypeNONKYC);
																			mPQAccountDetailRepository.save(accDetails);
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				corporateFileCatalogue.setSchedulerStatus(true);
				corporateFileCatalogueRepository.save(corporateFileCatalogue);
			}
		}
	}

	public void checkIpayStatus() {
		List<MTransaction> checkStatus = transactionRepository.getAllSuspicious();
		for (MTransaction mTransaction : checkStatus) {
			MdexTransactionRequestDTO req = new MdexTransactionRequestDTO();
			String trx = mTransaction.getTransactionRefNo();
			String[] splittedtrx = trx.split("D");
			req.setTransactionId(splittedtrx[0]);
			MdexTransactionResponseDTO statusresp = mdexApi.checkStatus(req);
			if (statusresp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
				mTransaction.setSuspicious(false);
				mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
				transactionRepository.save(mTransaction);

			} else if (statusresp.getCode().equalsIgnoreCase(ResponseStatus.FAILURE.getValue())) {
				MTransaction creditTransaction = transactionRepository.getByUpiId(mTransaction.getTransactionRefNo());
				JSONObject payload = new JSONObject();
				try {
					payload.put("token", RazorPayConstants.UPI_TOKEN);
					payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
					payload.put("transactionId", creditTransaction.getRetrivalReferenceNo());

					Client client = Client.create();
					WebResource webResource = client.resource("https://mycashier.in/UPI/RefundMoney");
					ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
							.post(ClientResponse.class, payload);
					String stringResponse = clientResponse.getEntity(String.class);
					JSONObject respObj = new JSONObject(stringResponse);
					if (respObj != null) {
						String code = respObj.getString("code");
						if (clientResponse.getStatus() == 200
								&& ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							creditTransaction.setStatus(Status.Refunded);
						}
					}
					creditTransaction.setSuspicious(false);

					transactionRepository.save(creditTransaction);
					mTransaction.setMdexTransactionStatus(Status.Failed.getValue());
					mTransaction.setSuspicious(false);
					mTransaction.setStatus(Status.Failed);
					transactionRepository.save(mTransaction);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// New code By SHuvam

	public void saveRegistrationDataInBulkRegisterByCorporate() {
		List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkRegister();
		if (fileList != null && fileList.size() > 0) {
			System.out.println("no of Uploaded Files:" + fileList.size());

			for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
				if (!corporateFileCatalogue.isFileRejectionStatus()) {
					if (corporateFileCatalogue.isReviewStatus()) {
						if ("BLKREGISTER".equalsIgnoreCase(corporateFileCatalogue.getCategoryType())) {
							String s3Path = corporateFileCatalogue.getS3Path();
							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadRegister(s3Path);
								if (fileJsonObject != null && fileJsonObject.length() > 0) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												try {
													RegisterDTO registerDTO = new RegisterDTO();
													registerDTO.setFirstName(jObject.getString("firstName"));
													registerDTO.setLastName(jObject.getString("lastName"));
													registerDTO.setUserType(UserType.User);

													String day = jObject.getString("dob(dd)");
													String month = jObject.getString("dob(MM)");
													String year = jObject.getString("dob(yyyy)");
													String dob = day + "/" + month + "/" + year;
													System.out.println("Date formate before parsing " + dob);
													try {
														if (dob != null) {
															registerDTO.setDateOfBirth(CommonUtil.DATEFORMATTER
																	.format(CommonUtil.DATEFORMATTER3.parse(dob)));
														}
													} catch (Exception e) {
														System.out
																.println("Invalid date format" + dob + e.getMessage());
													}
													registerDTO.setEmail(jObject.getString("email"));
													registerDTO.setContactNo(jObject.getString("mobile"));
													registerDTO.setUsername(jObject.getString("mobile"));
													registerDTO.setIdNo(jObject.getString("idNumber"));
													registerDTO.setIdType(jObject.getString("idType"));
													registerDTO.setPassword(jObject.getString("mobile"));

													CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
															.getByCorporateId(corporateFileCatalogue.getCorporate());

													RegisterError regErr = registerValidation
															.validateCorporateUserRecheck(registerDTO);

													if (regErr.isValid()) {
														BulkRegister bulkRegister = new BulkRegister();

														bulkRegister.setFirstName(jObject.getString("firstName"));
														bulkRegister.setLastName(jObject.getString("lastName"));
														bulkRegister.setEmail(jObject.getString("email"));
														bulkRegister.setMobile(jObject.getString("mobile"));
														bulkRegister.setDob(CommonUtil.DATEFORMATTER
																.format(CommonUtil.DATEFORMATTER3.parse(dob)));
														bulkRegister.setProxyNo(CommonUtil
																.getProxyNumber(jObject.getString("proxyNo")));
														bulkRegister.setIdNumber(jObject.getString("idNumber"));
														bulkRegister.setIdType(jObject.getString("idType"));
														bulkRegister.setAgentDetails(corpDetails);

														bulkRegister.setFailedReason(null);
														bulkRegister.setCardCreationStatus(false);
														bulkRegister.setWalletCreationStatus(false);
														bulkRegister.setFailedStatus(false);

														try {
															bulkRegisterRepository.save(bulkRegister);
														} catch (Exception e) {
															e.printStackTrace();
														}
													} else {
														System.out.println(regErr.getMessage());
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										}
										corporateFileCatalogue.setSchedulerStatus(true);
									}
								} else {
									corporateFileCatalogue.setSchedulerStatus(true);
								}

								corporateFileCatalogueRepository.save(corporateFileCatalogue);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

	public void bulkUserRegisterAtMMByCorporate() {
		List<BulkRegister> userList = bulkRegisterRepository.getListOfUserForUserCreation(false, false);
		System.out.println("Cron is Working");
		if (userList != null && userList.size() > 0) {
			for (BulkRegister bulk : userList) {
				try {
					RegisterDTO registerDTO = new RegisterDTO();
					registerDTO.setFirstName(bulk.getFirstName());
					registerDTO.setLastName(bulk.getLastName());
					registerDTO.setUserType(UserType.User);

					String dob = bulk.getDob();
					try {
						if (dob != null) {
							registerDTO.setDateOfBirth(
									CommonUtil.DATEFORMATTER.format(CommonUtil.DATEFORMATTER.parse(dob)));
						}
					} catch (Exception e) {
						System.out.println("Invalid date format" + dob + e.getMessage());
					}

					registerDTO.setEmail(bulk.getEmail());
					registerDTO.setContactNo(bulk.getMobile());
					registerDTO.setUsername(bulk.getMobile());
					registerDTO.setIdNo(bulk.getIdNumber());
					registerDTO.setIdType(bulk.getIdType());

					MUser usermob = userApi.saveCorpUser(registerDTO);

					if (usermob != null) {
						ResponseDTO userResponse = matchMoveApi.corpCreateUserOnMM(usermob);

						if (userResponse.isUserCreated()) {
							bulk.setUserCreationStatus(true);
							bulk.setFailedStatus(false);
						} else {
							bulk.setFailedReason(userResponse.getMessage());
							bulk.setFailedStatus(true);
						}
						bulk.setUser(usermob);
						bulk.setLastModified(
								CommonUtil.DATEFORMATTER1.parse(CommonUtil.DATEFORMATTER1.format(new Date())));
						bulkRegisterRepository.save(bulk);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			System.err.println("bulk list is blank");
		}
	}

	public void bulkWalletCreateAtMMByCorporate() {
		List<BulkRegister> userList = bulkRegisterRepository.getListOfUserForWalletCreation(true, false, false);
		if (userList != null && userList.size() > 0) {

			for (BulkRegister bulk : userList) {
				try {
					ResponseDTO walletResponse = matchMoveApi.corpCreateWalletOnMM(bulk.getUser());

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
						bulk.setWalletCreationStatus(true);
						bulk.setFailedStatus(false);
					} else {
						bulk.setWalletCreationStatus(false);
						bulk.setFailedReason(walletResponse.getMessage());
						bulk.setFailedStatus(true);
					}
					bulkRegisterRepository.save(bulk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void bulkPhysicalCardCreateAtMMByCorporate() {
		List<BulkRegister> userList = bulkRegisterRepository.getListOfUserForPhysicalCardCreation(true, true, false,
				false);
		if (userList != null && userList.size() > 0) {

			for (BulkRegister bulk : userList) {
				try {
					WalletResponse cardResponse = matchMoveApi.assignPhysicalCard(bulk.getUser(), bulk.getProxyNo());
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(cardResponse.getCode())) {
						ResponseDTO resp = matchMoveApi.activationPhysicalCard(cardResponse.getActivationCode(),
								bulk.getUser());
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
							PhysicalCardDetails card = physicalCardDetailRepository.findByUser(bulk.getUser());
							if (card != null) {
								card.setStatus(Status.Active);
								physicalCardDetailRepository.save(card);
							}
						}
						bulk.setCardCreationStatus(true);
						bulk.setFailedStatus(false);
					} else {
						bulk.setFailedReason(cardResponse.getMessage());
						bulk.setFailedStatus(true);
					}
					bulkRegisterRepository.save(bulk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void saveKYCDataInBulkKYCCorporateByCorporate() {
		List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkKYCTransfer();
		if (fileList != null && fileList.size() > 0) {
			System.out.println("no of Uploaded Files:" + fileList.size());

			for (CorporateFileCatalogue corporateFileCatalogue : fileList) {
				if (!corporateFileCatalogue.isFileRejectionStatus()) {
					if (corporateFileCatalogue.isReviewStatus()) {
						if ("BLKKYCUPLOAD".equalsIgnoreCase(corporateFileCatalogue.getCategoryType())) {
							String s3Path = corporateFileCatalogue.getS3Path();
							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkKycUpload(s3Path);
								if (fileJsonObject != null && fileJsonObject.length() > 0) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject loadObject = jArray.getJSONObject(i);

											if (loadObject != null) {
												String mobile = loadObject.getString("mobile");

												MUser user = userRespository.findByUsername(mobile);
												if (user != null) {
													String email = user.getUserDetail().getEmail();

													BulkKYCCorporate bkcMobile = bulkKYCCorporateRepository
															.findByMobile(mobile);
													if (bkcMobile == null) {
														BulkKYCCorporate bkcEmail = bulkKYCCorporateRepository
																.findByEmail(email);
														if (bkcEmail == null) {
															String address1 = loadObject.getString("address1");
															String address2 = loadObject.getString("address2");
															String day = loadObject.getString("dob(dd)");
															String month = loadObject.getString("dob(MM)");
															String year = loadObject.getString("dob(yyyy)");
															String dob = year + "-" + month + "-" + day;

															System.out.println("Date formate before parsing " + dob);

															String pinCode = loadObject.getString("pinCode");
															String docUrl = loadObject.getString("documentUrl");
															String docUrl1 = loadObject.getString("documentUrl1");
															String idType = loadObject.getString("idType");
															String idNumber = loadObject.getString("idNumber");

															BulkKYCCorporate bulkKYCCorporate = new BulkKYCCorporate();

															try {
																if (dob != null) {
																	bulkKYCCorporate.setDob(CommonUtil.DATEFORMATTER
																			.format(CommonUtil.DATEFORMATTER
																					.parse(dob)));
																}
															} catch (Exception e) {
																System.out.println(
																		"Invalid date format" + dob + e.getMessage());
															}

															bulkKYCCorporate.setAddress1(address1);
															bulkKYCCorporate.setAddress2(address2);
															bulkKYCCorporate.setMobile(mobile);
															bulkKYCCorporate.setEmail(email);
															bulkKYCCorporate.setPinCode(pinCode);
															bulkKYCCorporate.setDocUrl(docUrl);
															bulkKYCCorporate.setDocUrl1(docUrl1);
															bulkKYCCorporate.setIdType(idType);
															bulkKYCCorporate.setIdNumber(idNumber);

															bulkKYCCorporate.setUser(user);
															bulkKYCCorporate.setKycStatus(false);
															bulkKYCCorporate.setFailedStatus(false);

															CorporateAgentDetails corporateAgentDetails = corporateAgentDetailsRepository
																	.getByCorporateId(
																			corporateFileCatalogue.getCorporate());
															bulkKYCCorporate.setAgentDetails(corporateAgentDetails);

															if (corporateFileCatalogue.getPartnerDetails() != null) {
																bulkKYCCorporate.setPartnerDetails(
																		corporateFileCatalogue.getPartnerDetails());
															}
															bulkKYCCorporateRepository.save(bulkKYCCorporate);
														} else {
															System.out.println("Email already exist.");
														}
													} else {
														System.out.println("Mobile already exist.");
													}
												} else {
													System.out.println("User not found: " + mobile);
												}
											}
										}
										corporateFileCatalogue.setSchedulerStatus(true);
									}
								} else {
									corporateFileCatalogue.setSchedulerStatus(true);
								}

								corporateFileCatalogueRepository.save(corporateFileCatalogue);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else {
						System.out.println("Approval is pending from Admin.");
					}
				} else {
					System.out.println("File Rejection Status is true.");
				}
			}
		}
	}

	public void bulkKYCToCorporateByCorporate() {
		List<BulkKYCCorporate> userList = bulkKYCCorporateRepository.getListOfUserForKYC(false, false);
		if (userList != null && userList.size() > 0) {

			for (BulkKYCCorporate bulk : userList) {
				try {
					if (bulk != null) {
						MPQAccountType accountTypeKYC = mPQAccountTypeRepository.findByCode("KYC");
						MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository.findByCode("NONKYC");

						String address1 = bulk.getAddress1().trim();
						String address2 = bulk.getAddress2().trim();
						String pinCode = bulk.getPinCode().trim();
						String documentUrl = bulk.getDocUrl().trim();
						String documentUrl1 = bulk.getDocUrl1().trim();
						String idType = bulk.getIdType().trim();
						String idNumber = bulk.getIdNumber().trim();

						MUser user = bulk.getUser();
						if (user != null) {
							MKycDetail kycDetail = mKycRepository.findByUser(user);
							if (kycDetail == null) {
								kycDetail = new MKycDetail();
								kycDetail.setAddress1(address1);
								kycDetail.setAddress2(address2);
								kycDetail.setIdImage(documentUrl);
								kycDetail.setIdImage1(documentUrl1);
								kycDetail.setIdType(idType);
								kycDetail.setIdNumber(idNumber);
								kycDetail.setPinCode(pinCode);
								kycDetail.setUser(user);
								kycDetail.setUserType(UserType.User);
								AddressResponseDTO addResp = CommonUtil.getAddress(pinCode);

								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(addResp.getCode())) {
									kycDetail.setCity(addResp.getCity());
									kycDetail.setState(addResp.getState());
									kycDetail.setCountry(addResp.getCountry());
								} else {
									kycDetail.setCity("Bangalore");
									kycDetail.setState("Karnataka");
									kycDetail.setCountry("India");
								}

								UserKycResponse userKyc = matchMoveApi.kycUserMMCorporate(kycDetail);
								System.out.println("USERKYCRESPONSE1: " + userKyc.toString());

								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKyc.getCode())) {
									UserKycResponse userIdDetails = matchMoveApi.setIdDetailsCorporate(kycDetail);
									System.out.println("USERKYCRESPONSE2: " + userIdDetails.toString());

									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userIdDetails.getCode())) {
										UserKycResponse image1Upload = matchMoveApi.setImagesForKyc1(kycDetail);
										System.out.println("USERKYCRESPONSE3: " + image1Upload.toString());

										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(image1Upload.getCode())) {
											UserKycResponse imageUpload2 = matchMoveApi.setImagesForKyc2(kycDetail);
											System.out.println("USERKYCRESPONSE4: " + imageUpload2.toString());

											if (ResponseStatus.SUCCESS.getValue()
													.equalsIgnoreCase(imageUpload2.getCode())) {
												UserKycResponse approvalResp = matchMoveApi
														.tempConfirmKycCorporate(kycDetail);
												System.out.println("USERKYCRESPONSE5: " + approvalResp.toString());

												if (ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(approvalResp.getCode())) {
													MPQAccountDetails accDetails = user.getAccountDetail();
													if (accDetails != null) {
														MPQAccountType accountType = mPQAccountTypeRepository
																.findByCode("KYC");
														accDetails.setAccountType(accountType);
														mPQAccountDetailRepository.save(accDetails);

														kycDetail.setAccountType(accountType);
														kycDetail.setRejectionStatus(false);
														mKycRepository.save(kycDetail);

														bulk.setKycStatus(true);
														bulk.setFailedStatus(false);
													} else {
														kycDetail.setAccountType(accountTypeNONKYC);
														kycDetail.setRejectionStatus(true);
														kycDetail.setRejectionReason("No Account found");
														mKycRepository.save(kycDetail);

														bulk.setKycStatus(false);
														bulk.setFailedStatus(true);
														bulk.setFailedReason("No Account found");
													}
												} else {
													kycDetail.setAccountType(accountTypeNONKYC);
													kycDetail.setRejectionStatus(true);
													kycDetail.setRejectionReason(approvalResp.getMessage());
													mKycRepository.save(kycDetail);

													bulk.setKycStatus(false);
													bulk.setFailedStatus(true);
													bulk.setFailedReason(approvalResp.getMessage());
												}
											} else {
												kycDetail.setAccountType(accountTypeNONKYC);
												kycDetail.setRejectionStatus(true);
												kycDetail.setRejectionReason(imageUpload2.getMessage());
												mKycRepository.save(kycDetail);

												bulk.setKycStatus(false);
												bulk.setFailedStatus(true);
												bulk.setFailedReason(imageUpload2.getMessage());
											}
										} else {
											kycDetail.setAccountType(accountTypeNONKYC);
											kycDetail.setRejectionStatus(true);
											kycDetail.setRejectionReason(image1Upload.getMessage());
											mKycRepository.save(kycDetail);

											bulk.setKycStatus(false);
											bulk.setFailedStatus(true);
											bulk.setFailedReason(image1Upload.getMessage());
										}
									} else {
										kycDetail.setAccountType(accountTypeNONKYC);
										kycDetail.setRejectionStatus(true);
										kycDetail.setRejectionReason(userIdDetails.getMessage());
										mKycRepository.save(kycDetail);

										bulk.setKycStatus(false);
										bulk.setFailedStatus(true);
										bulk.setFailedReason(userIdDetails.getMessage());
									}
								} else {
									kycDetail.setAccountType(accountTypeNONKYC);
									kycDetail.setRejectionStatus(true);
									kycDetail.setRejectionReason(userKyc.getMessage());
									mKycRepository.save(kycDetail);

									bulk.setKycStatus(false);
									bulk.setFailedStatus(true);
									bulk.setFailedReason(userKyc.getMessage());
								}
							} else {
								MPQAccountDetails accountDetails = kycDetail.getUser().getAccountDetail();
								if (accountDetails != null) {
									MPQAccountType accountType = accountDetails.getAccountType();

									if ("NONKYC".equalsIgnoreCase(accountType.getCode())) {
										mKycRepository.delete(kycDetail);

										MKycDetail kycDetails = new MKycDetail();
										kycDetails.setAddress1(address1);
										kycDetails.setAddress2(address2);
										kycDetails.setIdImage(documentUrl);
										kycDetails.setIdImage1(documentUrl);
										kycDetails.setIdType(idType);
										kycDetails.setIdNumber(idNumber);
										kycDetails.setPinCode(pinCode);
										kycDetails.setUser(user);
										kycDetails.setUserType(UserType.User);
										AddressResponseDTO addResp = CommonUtil.getAddress(pinCode);

										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(addResp.getCode())) {
											kycDetails.setCity(addResp.getCity());
											kycDetails.setState(addResp.getState());
											kycDetails.setCountry(addResp.getCountry());
										} else {
											kycDetails.setCity("Bangalore");
											kycDetails.setState("Karnataka");
											kycDetails.setCountry("India");
										}

										UserKycResponse userKyc = matchMoveApi.kycUserMMCorporate(kycDetails);
										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKyc.getCode())) {
											UserKycResponse userIdDetails = matchMoveApi
													.setIdDetailsCorporate(kycDetails);
											if (ResponseStatus.SUCCESS.getValue()
													.equalsIgnoreCase(userIdDetails.getCode())) {
												UserKycResponse image1Upload = matchMoveApi
														.setImagesForKyc1(kycDetails);
												if (ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(image1Upload.getCode())) {
													UserKycResponse imageUpload2 = matchMoveApi
															.setImagesForKyc2(kycDetails);
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(imageUpload2.getCode())) {
														UserKycResponse approvalResp = matchMoveApi
																.tempConfirmKycCorporate(kycDetails);
														if (ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(approvalResp.getCode())) {
															MPQAccountDetails accDetails = user.getAccountDetail();
															if (accDetails != null) {
																accDetails.setAccountType(accountTypeKYC);
																mPQAccountDetailRepository.save(accDetails);

																kycDetails.setAccountType(accountTypeKYC);
																kycDetails.setRejectionStatus(false);
																mKycRepository.save(kycDetails);

																bulk.setKycStatus(true);
																bulk.setFailedStatus(false);
															} else {
																kycDetails.setAccountType(accountTypeNONKYC);
																kycDetails.setRejectionStatus(true);
																kycDetails.setRejectionReason("No Account found");
																mKycRepository.save(kycDetails);

																bulk.setKycStatus(false);
																bulk.setFailedStatus(true);
																bulk.setFailedReason("No Account found");
															}
														} else {
															kycDetails.setAccountType(accountTypeNONKYC);
															kycDetails.setRejectionStatus(true);
															kycDetails.setRejectionReason(approvalResp.getMessage());
															mKycRepository.save(kycDetails);

															bulk.setKycStatus(false);
															bulk.setFailedStatus(true);
															bulk.setFailedReason(approvalResp.getMessage());
														}
													} else {
														kycDetails.setAccountType(accountTypeNONKYC);
														kycDetails.setRejectionStatus(true);
														kycDetails.setRejectionReason(imageUpload2.getMessage());
														mKycRepository.save(kycDetails);

														bulk.setKycStatus(false);
														bulk.setFailedStatus(true);
														bulk.setFailedReason(imageUpload2.getMessage());
													}
												} else {
													kycDetails.setAccountType(accountTypeNONKYC);
													kycDetails.setRejectionStatus(true);
													kycDetails.setRejectionReason(image1Upload.getMessage());
													mKycRepository.save(kycDetails);

													bulk.setKycStatus(false);
													bulk.setFailedStatus(true);
													bulk.setFailedReason(image1Upload.getMessage());
												}
											} else {
												kycDetails.setAccountType(accountTypeNONKYC);
												kycDetails.setRejectionStatus(true);
												kycDetails.setRejectionReason(userIdDetails.getMessage());
												mKycRepository.save(kycDetails);

												bulk.setKycStatus(false);
												bulk.setFailedStatus(true);
												bulk.setFailedReason(userIdDetails.getMessage());
											}
										} else {
											kycDetails.setAccountType(accountTypeNONKYC);
											kycDetails.setRejectionStatus(true);
											kycDetails.setRejectionReason(userKyc.getMessage());
											mKycRepository.save(kycDetails);

											bulk.setKycStatus(false);
											bulk.setFailedStatus(true);
											bulk.setFailedReason(userKyc.getMessage());
										}
									} else {
										MPQAccountDetails accDetails = user.getAccountDetail();
										UserKycResponse kycResponse = matchMoveApi.checkKYCStatus(user);

										if (!("Approved".equalsIgnoreCase(kycResponse.getStatus())
												|| ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(kycResponse.getCode()))) {
											mKycRepository.delete(kycDetail);

											MKycDetail kycDetails = new MKycDetail();
											kycDetails.setAddress1(address1);
											kycDetails.setAddress2(address2);
											kycDetails.setIdImage(documentUrl);
											kycDetails.setIdImage1(documentUrl);
											kycDetails.setIdType(idType);
											kycDetails.setIdNumber(idNumber);
											kycDetails.setPinCode(pinCode);
											kycDetails.setUser(user);
											kycDetails.setUserType(UserType.User);

											AddressResponseDTO addResp = CommonUtil.getAddress(pinCode);

											if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(addResp.getCode())) {
												kycDetails.setCity(addResp.getCity());
												kycDetails.setState(addResp.getState());
												kycDetails.setCountry(addResp.getCountry());
											} else {
												kycDetails.setCity("Bangalore");
												kycDetails.setState("Karnataka");
												kycDetails.setCountry("India");
											}

											UserKycResponse userKyc = matchMoveApi.kycUserMMCorporate(kycDetails);
											if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKyc.getCode())) {
												UserKycResponse userIdDetails = matchMoveApi
														.setIdDetailsCorporate(kycDetails);
												if (ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(userIdDetails.getCode())) {
													UserKycResponse image1Upload = matchMoveApi
															.setImagesForKyc1(kycDetails);
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(image1Upload.getCode())) {
														UserKycResponse imageUpload2 = matchMoveApi
																.setImagesForKyc2(kycDetails);
														if (ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(imageUpload2.getCode())) {
															UserKycResponse approvalResp = matchMoveApi
																	.tempConfirmKycCorporate(kycDetails);
															if (ResponseStatus.SUCCESS.getValue()
																	.equalsIgnoreCase(approvalResp.getCode())) {
																if (accDetails != null) {
																	accDetails.setAccountType(accountTypeKYC);
																	mPQAccountDetailRepository.save(accDetails);

																	kycDetails.setAccountType(accountTypeKYC);
																	kycDetails.setRejectionStatus(false);
																	mKycRepository.save(kycDetails);

																	bulk.setKycStatus(true);
																	bulk.setFailedStatus(false);
																} else {
																	kycDetails.setAccountType(accountTypeNONKYC);
																	kycDetails.setRejectionStatus(true);
																	kycDetails.setRejectionReason("No Account found");
																	mKycRepository.save(kycDetails);

																	bulk.setKycStatus(false);
																	bulk.setFailedStatus(true);
																	bulk.setFailedReason("No Account found");
																}
															} else {
																kycDetails.setAccountType(accountTypeNONKYC);
																kycDetails.setRejectionStatus(true);
																kycDetails
																		.setRejectionReason(approvalResp.getMessage());
																mKycRepository.save(kycDetails);

																accDetails.setAccountType(accountTypeNONKYC);
																mPQAccountDetailRepository.save(accDetails);

																bulk.setKycStatus(false);
																bulk.setFailedStatus(true);
																bulk.setFailedReason(approvalResp.getMessage());
															}
														} else {
															kycDetails.setAccountType(accountTypeNONKYC);
															kycDetails.setRejectionStatus(true);
															kycDetails.setRejectionReason(imageUpload2.getMessage());
															mKycRepository.save(kycDetails);

															accDetails.setAccountType(accountTypeNONKYC);
															mPQAccountDetailRepository.save(accDetails);

															bulk.setKycStatus(false);
															bulk.setFailedStatus(true);
															bulk.setFailedReason(imageUpload2.getMessage());
														}
													} else {
														kycDetails.setAccountType(accountTypeNONKYC);
														kycDetails.setRejectionStatus(true);
														kycDetails.setRejectionReason(image1Upload.getMessage());
														mKycRepository.save(kycDetails);

														accDetails.setAccountType(accountTypeNONKYC);
														mPQAccountDetailRepository.save(accDetails);

														bulk.setKycStatus(false);
														bulk.setFailedStatus(true);
														bulk.setFailedReason(image1Upload.getMessage());
													}
												} else {
													kycDetails.setAccountType(accountTypeNONKYC);
													kycDetails.setRejectionStatus(true);
													kycDetails.setRejectionReason(userIdDetails.getMessage());
													mKycRepository.save(kycDetails);

													accDetails.setAccountType(accountTypeNONKYC);
													mPQAccountDetailRepository.save(accDetails);

													bulk.setKycStatus(false);
													bulk.setFailedStatus(true);
													bulk.setFailedReason(userIdDetails.getMessage());
												}
											} else {
												kycDetails.setAccountType(accountTypeNONKYC);
												kycDetails.setRejectionStatus(true);
												kycDetails.setRejectionReason(userKyc.getMessage());
												mKycRepository.save(kycDetails);

												accDetails.setAccountType(accountTypeNONKYC);
												mPQAccountDetailRepository.save(accDetails);

												bulk.setKycStatus(false);
												bulk.setFailedStatus(true);
												bulk.setFailedReason(userKyc.getMessage());
											}
										} else {
											bulk.setFailedReason(kycResponse.getMessage());
											bulk.setFailedStatus(true);
										}
									}
								} else {
									bulk.setFailedReason("User Account is not available.");
									bulk.setFailedStatus(true);
								}
							}
						} else {
							bulk.setFailedReason("User is not available.");
							bulk.setFailedStatus(true);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					bulk.setFailedReason(e.getMessage());
					bulk.setFailedStatus(true);
				}
				bulkKYCCorporateRepository.save(bulk);
			}
		}
	}

	public void saveBulkWalletLoadInBulkLoadMoneyByCorporate() {
		System.err.println("Corporate Bulk Fund Transfer Running >>>>");

		try {
			List<CorporateFileCatalogue> fileList = corporateFileCatalogueRepository.getListOfBulkFundTransfer();
			if (fileList != null && fileList.size() > 0) {
				for (CorporateFileCatalogue corporateFileCatalogue : fileList) {

					if (corporateFileCatalogue.isFileRejectionStatus() == false) {
						if (corporateFileCatalogue.isReviewStatus() == true
								&& corporateFileCatalogue.isTransactionStatus() == false) {
							if ("BLKCARDLOAD".equalsIgnoreCase(corporateFileCatalogue.getCategoryType())) {

								String s3Path = corporateFileCatalogue.getS3Path();

								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadLoadBalance(s3Path);
								if (fileJsonObject != null) {

									JSONArray filejArray = fileJsonObject.getJSONArray("LoadMoneyList");
									if (filejArray != null) {

										for (int i = 0; i < filejArray.length(); i++) {
											JSONObject loadObject = filejArray.getJSONObject(i);
											if (loadObject != null) {

												String mobile = loadObject.getString("mobile");
												String amount = loadObject.getString("amount");
												String email = loadObject.getString("email");
												String dateOfTransaction = loadObject.getString("dateOfTransaction");

												MUser agent = corporateFileCatalogue.getCorporate();

												BulkLoadMoney bulkLoadMoney = new BulkLoadMoney();
												bulkLoadMoney.setMobile(mobile);
												bulkLoadMoney.setAmount(amount);
												bulkLoadMoney.setEmail(email);
												bulkLoadMoney.setDateOfTransaction(dateOfTransaction);
												bulkLoadMoney.setWalletLoadStatus(false);
												bulkLoadMoney.setCardLoadStatus(false);
												bulkLoadMoney.setFailedStatus(false);

												bulkLoadMoney.setCorporate(agent);

												if (corporateFileCatalogue.getPartnerDetails() != null) {
													bulkLoadMoney.setPartnerDetails(
															corporateFileCatalogue.getPartnerDetails());
												}
												bulkLoadMoneyRepository.save(bulkLoadMoney);
											}
										}
									}
								}
							}
							corporateFileCatalogue.setSchedulerStatus(true);
							corporateFileCatalogueRepository.save(corporateFileCatalogue);
						} else {
							System.out.println("Review Status: " + corporateFileCatalogue.isReviewStatus()
									+ " Transaction Status: " + corporateFileCatalogue.isTransactionStatus()
									+ ". Please update the status.");
						}
					} else {
						System.out.println("File Rejection Status is TRUE. Set as FALSE.");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadWalletToMMByCorporate() {
		System.err.println("Corporate Load Wallet >>>>");

		try {
			List<BulkLoadMoney> walletLoadList = bulkLoadMoneyRepository.loadWalletAndCard(false, false, false);
			if (walletLoadList != null && walletLoadList.size() > 0) {
				for (BulkLoadMoney bulkLoadMoney : walletLoadList) {

					String mobile = bulkLoadMoney.getMobile();
					String amount = bulkLoadMoney.getAmount();

					MUser agent = bulkLoadMoney.getCorporate();

					if (agent.getAccountDetail().getBalance() >= Long.valueOf(amount)) {
						MUser cardUser = userApi.findByUserName(mobile.trim());

						if (cardUser != null) {
							MService service = mServiceRepository.findServiceByCode(StartUpUtil.LOADCARD_CORPORATE);
							TransactionError transactionError = loadMoneyValidation.validateLoadCardTransaction(amount,
									mobile.trim(), service);

							if (transactionError.isValid()) {
								RequestDTO cardRequest = new RequestDTO();
								cardRequest.setAmount(amount);
								cardRequest.setContactNo(mobile);

								ResponseDTO resp = transactionApi.initiateLoadCardTransactionCorporate(cardRequest,
										agent, service, cardUser);

								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {

									WalletResponse walletRespon = matchMoveApi.initiateLoadFundsToMMWalletCorp(cardUser,
											cardRequest.getAmount());

									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletRespon.getCode())) {
										transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
												Status.Success.getValue(), walletRespon.getAuthRetrivalNo());

										bulkLoadMoney.setWalletLoadStatus(true);
										bulkLoadMoney.setFailedStatus(false);
										bulkLoadMoneyRepository.save(bulkLoadMoney);

										MTransaction loadCard = new MTransaction();

										String TransactionRefNo = System.currentTimeMillis() + "";
										loadCard.setAmount(Double.parseDouble(amount));
										loadCard.setAccount(agent.getAccountDetail());
										loadCard.setStatus(Status.Initiated);
										loadCard.setTransactionRefNo(TransactionRefNo);
										loadCard.setTransactionType(TransactionType.CORPORATE_LOAD);
										MService service1 = mServiceRepository.findByName("BRCL");
										if (service1 != null) {
											loadCard.setService(service1);

										}
										mTransactionRepository.save(loadCard);

										MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
										userData.add("amount", amount);
										userData.add("message", "return funds");

										MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(agent);
										if (mmw != null) {
											MMCards card = cardRepository
													.getCardByWalletId(Long.parseLong(mmw.getWalletId()));
											if (card != null) {
												Client client = Client.create();
												client.addFilter(new LoggingFilter(System.out));
												WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL
														+ "/users/wallets/cards/" + card.getCardId() + "/funds");
												ClientResponse resp1 = resource
														.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
														.header("Authorization", MatchMoveUtil.getBasicAuthorization())
														.header("X-Auth-User-id", mmw.getMmUserId())
														.delete(ClientResponse.class, userData);
												String strResponse = resp1.getEntity(String.class);
												JSONObject transaction = new JSONObject(strResponse);

												System.out.println("transfer from card to wallet::::" + transaction);
												if (transaction != null && transaction.has("code")) {
													String code = transaction.getString("code");
													if (!CommonValidation.isNull(code)
															&& !code.equalsIgnoreCase("200")) {
														String description = transaction.getString("description");
														resp.setMessage(description);
														resp.setCode(ResponseStatus.FAILURE.getValue());
													} else {
														MTransaction load = mTransactionRepository
																.findByTransactionRefNo(TransactionRefNo);
														if (load != null) {
															load.setRetrivalReferenceNo(transaction.getString("id"));
															load.setStatus(Status.Success);
															mTransactionRepository.save(load);
															resp.setStatus(ResponseStatus.SUCCESS.getKey());
															resp.setCode(ResponseStatus.SUCCESS.getValue());
															resp.setMessage("Transferred Successfully");

														} else {
															resp.setStatus(ResponseStatus.FAILURE.getKey());
															resp.setCode(ResponseStatus.FAILURE.getValue());
															resp.setMessage("Transferred Failed");

														}

													}
												} else {
													resp.setStatus(ResponseStatus.FAILURE.getKey());
													resp.setCode(ResponseStatus.SUCCESS.getValue());
													resp.setMessage("Transferred Failed");

												}

											} else {
												resp.setStatus(ResponseStatus.FAILURE.getKey());
												resp.setCode(ResponseStatus.FAILURE.getValue());
												resp.setMessage("Transferred Failed");

											}
										}
									} else {
										transactionApi.updateLoadCardByCorporate(resp.getTxnId(),
												Status.Failed.getValue(), "");

										bulkLoadMoney.setWalletLoadStatus(false);
										bulkLoadMoney.setFailedStatus(true);
										bulkLoadMoney.setFailedReason(walletRespon.getMessage());
										bulkLoadMoneyRepository.save(bulkLoadMoney);
									}
								} else {
									transactionApi.updateLoadCardByCorporate(resp.getTxnId(), Status.Failed.getValue(),
											"");
									bulkLoadMoney.setWalletLoadStatus(false);
									bulkLoadMoney.setFailedStatus(true);
									bulkLoadMoney.setFailedReason(resp.getMessage());
									bulkLoadMoneyRepository.save(bulkLoadMoney);
								}
							} else {
								System.out.println(transactionError.toString());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadWalletToCardMMByCorporate() {
		try {
			List<BulkLoadMoney> walletLoadList = bulkLoadMoneyRepository.loadWalletAndCard(true, false, false);
			if (walletLoadList != null && walletLoadList.size() > 0) {
				for (BulkLoadMoney bulkLoadMoney : walletLoadList) {

					String mobile = bulkLoadMoney.getMobile();
					String amount = bulkLoadMoney.getAmount();

					MUser cardUser = userApi.findByUserName(mobile.trim());

					if (cardUser != null) {
						RequestDTO cardRequest = new RequestDTO();
						cardRequest.setAmount(amount);

						MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
						userData.add("amount", amount);
						userData.add("message", "return funds");

						MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(cardUser);
						if (mmw != null) {
							MMCards card = cardRepository.getPhysicalCardByWalletId(mmw.getId(), true);
							if (card != null) {
								Client client = Client.create();
								client.addFilter(new LoggingFilter(System.out));
								WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL
										+ "/users/wallets/cards/" + card.getCardId() + "/funds");
								ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
										.header("Authorization", MatchMoveUtil.getBasicAuthorization())
										.header("X-Auth-User-id", mmw.getMmUserId())
										.post(ClientResponse.class, userData);
								String strResponse = resp1.getEntity(String.class);
								JSONObject transaction = new JSONObject(strResponse);

								System.out.println("transfer from card to wallet::::" + transaction);

								if (resp1.getStatus() != 200) {
									bulkLoadMoney.setFailedStatus(true);
									bulkLoadMoney.setFailedReason(transaction.getString("description"));
									bulkLoadMoneyRepository.save(bulkLoadMoney);
								} else {
									bulkLoadMoney.setCardLoadStatus(true);
									bulkLoadMoney.setFailedStatus(false);
									bulkLoadMoneyRepository.save(bulkLoadMoney);
								}
							} else {
								bulkLoadMoney.setFailedStatus(true);
								bulkLoadMoney.setFailedReason("Card not found.");
								bulkLoadMoneyRepository.save(bulkLoadMoney);
							}
						} else {
							bulkLoadMoney.setFailedStatus(true);
							bulkLoadMoney.setFailedReason("Wallet not found.");
							bulkLoadMoneyRepository.save(bulkLoadMoney);
						}
					} else {
						bulkLoadMoney.setFailedStatus(true);
						bulkLoadMoney.setFailedReason("User not found");
						bulkLoadMoneyRepository.save(bulkLoadMoney);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveRegistrationDataInBulkRegisterGroupByGroup() {
		System.err.println("bulk register for failed group is running MM>>>>>>>>>>>");

		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkRegister();
		if (fileList != null && fileList.size() > 0) {
			for (GroupFileCatalogue groupFileCatalogue : fileList) {
				if (groupFileCatalogue.isFileRejectionStatus() == false) {
					if (groupFileCatalogue.isReviewStatus() == true) {
						if ("BLKREGISTER".equalsIgnoreCase(groupFileCatalogue.getCategoryType())) {

							try {
								String s3Path = groupFileCatalogue.getS3Path();

								JSONObject fileJsonObject = CSVReader.readCsvBulkUploadRegisterGroup(s3Path);
								if (fileJsonObject != null && fileJsonObject.length() > 0) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");

									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												try {
													RegisterDTO registerDTO = new RegisterDTO();
													registerDTO.setFirstName(jObject.getString("firstName"));
													registerDTO.setMiddleName(jObject.getString("middleName"));
													registerDTO.setLastName(jObject.getString("lastName"));
													registerDTO.setUserType(UserType.User);

													String day = jObject.getString("dob(dd)");
													String month = jObject.getString("dob(MM)");
													String year = jObject.getString("dob(yyyy)");
													String dob = year + "-" + month + "-" + day;
													try {
														if (dob != null) {
															registerDTO.setDateOfBirth(CommonUtil.DATEFORMATTER
																	.format(CommonUtil.DATEFORMATTER.parse(dob)));
														}
													} catch (Exception e) {
														System.out
																.println("Invalid date format" + dob + e.getMessage());
													}

													registerDTO.setEmail(jObject.getString("email"));
													registerDTO.setContactNo(jObject.getString("mobile"));
													registerDTO.setUsername(jObject.getString("mobile"));
													registerDTO.setIdNo(jObject.getString("idNumber"));
													registerDTO.setIdType(jObject.getString("idType"));

													registerDTO.setPassword("123456");
													registerDTO.setGroup(
															groupFileCatalogue.getUser().getGroupDetails().getEmail());
													registerDTO.setRemark(
															groupFileCatalogue.getUser().getGroupDetails().getDname());

													RegisterError regErr = registerValidation
															.validateCorporateUserRecheck(registerDTO);

													if (regErr.isValid()) {
														BulkRegisterGroup bulkRegister = new BulkRegisterGroup();

														bulkRegister.setFirstName(jObject.getString("firstName"));
														bulkRegister.setLastName(jObject.getString("lastName"));
														bulkRegister.setEmail(jObject.getString("email"));
														bulkRegister.setMobile(jObject.getString("mobile"));
														bulkRegister.setDob(CommonUtil.DATEFORMATTER
																.format(CommonUtil.DATEFORMATTER.parse(dob)));

														bulkRegister.setIdNumber(jObject.getString("idNumber"));
														bulkRegister.setIdtype(jObject.getString("idType"));

														bulkRegister.setFailedReason(null);
														bulkRegister.setUserCreationStatus(false);
														bulkRegister.setWalletCreationStatus(false);
														bulkRegister.setFailedStatus(false);
														bulkRegister.setGroupdetails(
																groupFileCatalogue.getUser().getGroupDetails());
														try {
															bulkRegisterGroupRepository.save(bulkRegister);
														} catch (Exception e) {
															e.printStackTrace();
														}
													} else {
														System.out.println(regErr.getMessage());
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										}
										groupFileCatalogue.setSchedulerStatus(true);
									}
								} else {
									groupFileCatalogue.setSchedulerStatus(true);
								}
								groupFileCatalogueRepository.save(groupFileCatalogue);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

	public void bulkUserRegisterAtMMByGroup() {
		List<BulkRegisterGroup> userList = bulkRegisterGroupRepository.getListOfUserForUserCreation(false, false);
		if (userList != null && userList.size() > 0) {

			for (BulkRegisterGroup bulk : userList) {
				try {
					RegisterDTO registerDTO = new RegisterDTO();
					registerDTO.setFirstName(bulk.getFirstName());
					registerDTO.setLastName(bulk.getLastName());
					registerDTO.setUserType(UserType.User);

					String dob = bulk.getDob();
					try {
						if (dob != null) {
							if (dob.contains("/")) {
								registerDTO.setDateOfBirth(
										CommonUtil.DATEFORMATTER.format(CommonUtil.DATEFORMATTER3.parse(dob)));
							} else {
								registerDTO.setDateOfBirth(
										CommonUtil.DATEFORMATTER.format(CommonUtil.DATEFORMATTER.parse(dob)));
							}
						}
					} catch (Exception e) {
						System.out.println("Invalid date format" + dob + e.getMessage());
					}

					registerDTO.setEmail(bulk.getEmail());
					registerDTO.setContactNo(bulk.getMobile());
					registerDTO.setUsername(bulk.getMobile());
					registerDTO.setIdNo(bulk.getIdNumber());
					registerDTO.setIdType(bulk.getIdtype());
					registerDTO.setGroup(bulk.getGroupdetails().getEmail());

					MUser usermob = userApi.saveGroupUser(registerDTO);

					if (usermob != null) {
						ResponseDTO userResponse = matchMoveApi.corpCreateUserOnMM(usermob);

						if (userResponse.isUserCreated()) {
							bulk.setUserCreationStatus(true);
							bulk.setFailedStatus(false);
						}
					}

					bulk.setUser(usermob);
					bulk.setLastModified(CommonUtil.DATEFORMATTER1.parse(CommonUtil.DATEFORMATTER1.format(new Date())));
					bulkRegisterGroupRepository.save(bulk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void bulkWalletCreateAtMMByGroup() {
		List<BulkRegisterGroup> userList = bulkRegisterGroupRepository.getListOfUserForWalletCreation(true, false,
				false);
		if (userList != null && userList.size() > 0) {

			for (BulkRegisterGroup bulk : userList) {
				try {
					ResponseDTO walletResponse = matchMoveApi.corpCreateWalletOnMM(bulk.getUser());

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
						bulk.setWalletCreationStatus(true);
						bulk.setFailedStatus(false);
					} else {
						bulk.setWalletCreationStatus(false);
						bulk.setFailedReason(walletResponse.getMessage());
						bulk.setFailedStatus(true);
					}

					bulkRegisterGroupRepository.save(bulk);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void assignCardToUsersByGroup() {
		List<BulkCardAssignmentGroup> bulkCardAssignmentGroupList = bulkCardAssignmentGroupRepository
				.getListOfUserForCardAssign(Status.Processing, Status.Processing, false);

		if (bulkCardAssignmentGroupList != null && bulkCardAssignmentGroupList.size() > 0) {
			for (BulkCardAssignmentGroup bulkCardAssignmentGroup : bulkCardAssignmentGroupList) {
				try {
					CommonError error = registerValidation
							.validateCardCreation(bulkCardAssignmentGroup.getMobileNumber());
					if (error != null) {
						if (error.isValid()) {
							WalletResponse walletResponse = matchMoveApi.assignPhysicalCardFromGroup(
									bulkCardAssignmentGroup.getMobileNumber(),
									bulkCardAssignmentGroup.getProxyNumber());
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
								bulkCardAssignmentGroup.setCreationStatus(Status.Success);
								bulkCardAssignmentGroup.setActivationStatus(Status.Active);
								bulkCardAssignmentGroup.setActivationCode(walletResponse.getActivationCode());
							} else if ("A00".equalsIgnoreCase(walletResponse.getCode())) {
								bulkCardAssignmentGroup.setRemarks(walletResponse.getMessage());
								bulkCardAssignmentGroup.setCreationStatus(Status.Success);
								bulkCardAssignmentGroup.setActivationStatus(Status.Inactive);
								bulkCardAssignmentGroup.setFailedStatus(true);
								bulkCardAssignmentGroup.setActivationCode(walletResponse.getActivationCode());
							} else {
								bulkCardAssignmentGroup.setCreationStatus(Status.Failed);
								bulkCardAssignmentGroup.setActivationStatus(Status.Inactive);
								bulkCardAssignmentGroup.setRemarks(walletResponse.getMessage());
								bulkCardAssignmentGroup.setFailedStatus(true);
							}
						} else {
							bulkCardAssignmentGroup.setCreationStatus(Status.Failed);
							bulkCardAssignmentGroup.setActivationStatus(Status.Inactive);
							bulkCardAssignmentGroup.setRemarks(error.getMessage());
							bulkCardAssignmentGroup.setFailedStatus(true);
						}
					} else {
						bulkCardAssignmentGroup.setCreationStatus(Status.Failed);
						bulkCardAssignmentGroup.setActivationStatus(Status.Inactive);
						bulkCardAssignmentGroup.setRemarks("Fatal Error");
						bulkCardAssignmentGroup.setFailedStatus(true);
					}
					bulkCardAssignmentGroupRepository.save(bulkCardAssignmentGroup);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void saveCardAssignDataInBulkCardAssignmentGroupByGroup() {
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getBulkCardAssignmentGroup();
		if (fileList != null) {
			for (GroupFileCatalogue groupFileCatalogue : fileList) {
				if (groupFileCatalogue.isFileRejectionStatus() == false) {
					if (groupFileCatalogue.isReviewStatus() == true) {
						if ("BLKCARDASSIGNGROUP".equalsIgnoreCase(groupFileCatalogue.getCategoryType())) {
							try {
								String s3Path = groupFileCatalogue.getS3Path();
								JSONObject fileJsonObject = CSVReader.readCsvBulkCardAssignmentGroup(s3Path);

								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("CardList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												BulkCardAssignmentGroup bulkCardAssignment = new BulkCardAssignmentGroup();
												bulkCardAssignment.setMobileNumber(jObject.getString("mobile"));
												bulkCardAssignment.setProxyNumber(jObject.getString("proxyNumber"));
												bulkCardAssignment.setGroupDetails(groupFileCatalogue.getUser());
												bulkCardAssignment.setFileName(s3Path);
												bulkCardAssignment.setCreationStatus(Status.Processing);
												bulkCardAssignment.setActivationStatus(Status.Processing);
												bulkCardAssignmentGroupRepository.save(bulkCardAssignment);
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				groupFileCatalogue.setSchedulerStatus(true);
				groupFileCatalogueRepository.save(groupFileCatalogue);
			}
		}
	}

	public void SaveGroupBulkKYCDataByGroup() {
		System.out.println("bulk kyc Group running>>>>>");
		List<GroupFileCatalogue> fileList = groupFileCatalogueRepository.getListOfBulkKyc();
		if (fileList != null && fileList.size() > 0) {
			for (GroupFileCatalogue fileCatalogue : fileList) {
				if (fileCatalogue.isFileRejectionStatus() == false) {
					if (fileCatalogue.isReviewStatus() == true) {
						if (fileCatalogue.getCategoryType().equalsIgnoreCase("BLKKYCUPLOAD")) {
							String absPath = fileCatalogue.getAbsPath();
							String filePath = StartUpUtil.CSV_FILE + absPath;
							try {
								JSONObject fileJsonObject = CSVReader.readCsvBulkKycUpload(filePath);
								if (fileJsonObject != null) {
									JSONArray jArray = fileJsonObject.getJSONArray("UserList");
									if (jArray != null) {
										for (int i = 0; i < jArray.length(); i++) {
											JSONObject jObject = jArray.getJSONObject(i);
											if (jObject != null) {
												MPQAccountType accountTypeKYC = mPQAccountTypeRepository
														.findByCode(StartUpUtil.KYC);
												MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository
														.findByCode(StartUpUtil.NON_KYC);

												String mobile = jObject.getString("mobile");
												String email = jObject.getString("email");
												String day = jObject.getString("dob(dd)");
												String month = jObject.getString("dob(MM)");
												String year = jObject.getString("dob(yyyy)");
												String address1 = jObject.getString("address1");
												String address2 = jObject.getString("address2");
												String idType = jObject.getString("idType");
												String idNumber = jObject.getString("idNumber");
												String pinCode = jObject.getString("pinCode");
												String documentUrl = jObject.getString("documentUrl");
												String documentUrl1 = jObject.getString("documentUrl1");

												System.out.println("documenturl:: " + documentUrl);

												RegisterDTO reg = new RegisterDTO();
												reg.setContactNo(mobile);
												reg.setEmail(email);
												String dob = year + "-" + month + "-" + day;
												reg.setDateOfBirth(dob);
												if (address1.length() >= 35) {
													reg.setAddress(address1.replaceAll("[^a-zA-Z0-9]", "").trim()
															.substring(0, 35));
												} else {
													reg.setAddress(address1.replaceAll("[^a-zA-Z0-9]", "").trim());
												}
												if (address2.length() >= 35) {
													reg.setServices(address2.replaceAll("[^a-zA-Z0-9]", "").trim()
															.substring(0, 35));
												} else {
													reg.setServices(address2.replaceAll("[^a-zA-Z0-9]", "").trim());
												}
												reg.setIdType(idType);
												reg.setIdNo(idNumber);
												reg.setFileName(pinCode);
												reg.setAadharImagePath1(documentUrl);
												reg.setAadharImagePath2(documentUrl1);
												ResponseDTO res = registerValidation.addKycData(reg, fileCatalogue);
												if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(res.getCode())) {
													RegisterError regErr = registerValidation.validateKyc(reg,
															fileCatalogue, false);
													if (regErr.isValid()) {
														MUser user = userApi.findByUserName(mobile);
														if (user != null) {
															MKycDetail kycDetail = mKycRepository.findByUser(user);
															if (kycDetail == null) {
																kycDetail = new MKycDetail();
																if (address1.length() >= 35) {
																	kycDetail.setAddress1(
																			address1.replaceAll("[^a-zA-Z0-9]", "")
																					.trim().substring(0, 35));
																} else {
																	kycDetail.setAddress1(address1
																			.replaceAll("[^a-zA-Z0-9]", "").trim());
																}
																if (address2.length() >= 35) {
																	kycDetail.setAddress2(
																			address2.replaceAll("[^a-zA-Z0-9]", "")
																					.trim().substring(0, 35));
																} else {
																	kycDetail.setAddress2(address2
																			.replaceAll("[^a-zA-Z0-9]", "").trim());
																}
																kycDetail.setIdImage(documentUrl);
																kycDetail.setIdImage1(documentUrl1);
																kycDetail.setIdType(idType);
																kycDetail.setIdNumber(idNumber);
																kycDetail.setPinCode(pinCode);
																kycDetail.setUser(user);
																kycDetail.setUserType(UserType.User);
																AddressResponseDTO addResp = CommonUtil
																		.getAddress(pinCode);
																if (ResponseStatus.SUCCESS.getValue()
																		.equalsIgnoreCase(addResp.getCode())) {
																	kycDetail.setCity(addResp.getCity());
																	kycDetail.setState(addResp.getState());
																	kycDetail.setCountry(addResp.getCountry());
																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (ResponseStatus.SUCCESS.getValue()
																			.equalsIgnoreCase(userKyc.getCode())) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (ResponseStatus.SUCCESS.getValue()
																				.equalsIgnoreCase(
																						userIdDetails.getCode())) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (ResponseStatus.SUCCESS.getValue()
																					.equalsIgnoreCase(
																							image1Upload.getCode())) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (ResponseStatus.SUCCESS.getValue()
																						.equalsIgnoreCase(imageUpload2
																								.getCode())) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (ResponseStatus.SUCCESS
																							.getValue()
																							.equalsIgnoreCase(
																									approvalResp
																											.getCode())) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							MPQAccountType accountType = mPQAccountTypeRepository
																									.findByCode(
																											StartUpUtil.KYC);
																							accDetails.setAccountType(
																									accountType);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountType);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason("NA");
																								gbr.setStatus(
																										Status.Success);
																								gbr.setSuccessStatus(
																										true);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										"No Account found");
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									approvalResp
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(imageUpload2
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							image1Upload.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);

																			GroupBulkKyc gbr = groupBulkKycRepository
																					.getUserRowByContact(mobile,
																							fileCatalogue, false,
																							Status.Initiated);
																			if (gbr != null) {
																				gbr.setFailReason(
																						userIdDetails.getMessage());
																				gbr.setStatus(Status.Failed);
																				gbr.setSuccessStatus(false);
																				groupBulkKycRepository.save(gbr);
																			}

																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason(userKyc.getMessage());
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																} else {
																	kycDetail.setCity("Bangalore");
																	kycDetail.setState("Karnataka");
																	kycDetail.setCountry("India");

																	UserKycResponse userKyc = matchMoveApi
																			.kycUserMMCorporate(kycDetail);
																	if (ResponseStatus.SUCCESS.getValue()
																			.equalsIgnoreCase(userKyc.getCode())) {
																		UserKycResponse userIdDetails = matchMoveApi
																				.setIdDetailsCorporate(kycDetail);
																		if (ResponseStatus.SUCCESS.getValue()
																				.equalsIgnoreCase(
																						userIdDetails.getCode())) {
																			UserKycResponse image1Upload = matchMoveApi
																					.setImagesForKyc1(kycDetail);
																			if (ResponseStatus.SUCCESS.getValue()
																					.equalsIgnoreCase(
																							image1Upload.getCode())) {
																				UserKycResponse imageUpload2 = matchMoveApi
																						.setImagesForKyc2(kycDetail);
																				if (ResponseStatus.SUCCESS.getValue()
																						.equalsIgnoreCase(imageUpload2
																								.getCode())) {
																					UserKycResponse approvalResp = matchMoveApi
																							.tempConfirmKycCorporate(
																									kycDetail);
																					if (ResponseStatus.SUCCESS
																							.getValue()
																							.equalsIgnoreCase(
																									approvalResp
																											.getCode())) {
																						MPQAccountDetails accDetails = user
																								.getAccountDetail();
																						if (accDetails != null) {
																							MPQAccountType accountType = mPQAccountTypeRepository
																									.findByCode(
																											StartUpUtil.KYC);
																							accDetails.setAccountType(
																									accountType);
																							mPQAccountDetailRepository
																									.save(accDetails);
																							kycDetail.setAccountType(
																									accountType);
																							kycDetail
																									.setRejectionStatus(
																											false);
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason("NA");
																								gbr.setStatus(
																										Status.Success);
																								gbr.setSuccessStatus(
																										true);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											"No Account found");
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										"No Account found");
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								approvalResp
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									approvalResp
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							imageUpload2.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(imageUpload2
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}
																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						image1Upload.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							image1Upload.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setAccountType(accountTypeNONKYC);
																			kycDetail.setRejectionStatus(true);
																			kycDetail.setRejectionReason(
																					userIdDetails.getMessage());
																			mKycRepository.save(kycDetail);

																			GroupBulkKyc gbr = groupBulkKycRepository
																					.getUserRowByContact(mobile,
																							fileCatalogue, false,
																							Status.Initiated);
																			if (gbr != null) {
																				gbr.setFailReason(
																						userIdDetails.getMessage());
																				gbr.setStatus(Status.Failed);
																				gbr.setSuccessStatus(false);
																				groupBulkKycRepository.save(gbr);
																			}

																		}
																	} else {
																		kycDetail.setAccountType(accountTypeNONKYC);
																		kycDetail.setRejectionStatus(true);
																		kycDetail.setRejectionReason(
																				userKyc.getMessage());
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason(userKyc.getMessage());
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																}
															} else {
																MPQAccountDetails accountDetails = kycDetail.getUser()
																		.getAccountDetail();
																if (accountDetails != null) {
																	MPQAccountType accountType = accountDetails
																			.getAccountType();
																	if (accountType.getCode()
																			.equalsIgnoreCase(StartUpUtil.NON_KYC)) {
																		mKycRepository.delete(kycDetail);
																		MKycDetail kycDetails = new MKycDetail();
																		kycDetails.setAddress1(address1);
																		kycDetails.setAddress2(address2);
																		kycDetails.setIdImage(documentUrl);
																		kycDetails.setIdImage1(documentUrl);
																		kycDetails.setIdType(idType);
																		kycDetails.setIdNumber(idNumber);
																		kycDetails.setPinCode(pinCode);
																		kycDetail.setUser(user);
																		kycDetail.setUserType(UserType.User);
																		AddressResponseDTO addResp = CommonUtil
																				.getAddress(pinCode);
																		if (addResp.getCode().equalsIgnoreCase("S00")) {
																			kycDetails.setCity(addResp.getCity());
																			kycDetails.setState(addResp.getState());
																			kycDetails.setCountry(addResp.getCountry());
																			UserKycResponse userKyc = matchMoveApi
																					.kycUserMMCorporate(kycDetail);
																			if (userKyc.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse userIdDetails = matchMoveApi
																						.setIdDetailsCorporate(
																								kycDetail);
																				if (userIdDetails.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse image1Upload = matchMoveApi
																							.setImagesForKyc1(
																									kycDetail);
																					if (image1Upload.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse imageUpload2 = matchMoveApi
																								.setImagesForKyc2(
																										kycDetail);
																						if (imageUpload2.getCode()
																								.equalsIgnoreCase(
																										"S00")) {
																							UserKycResponse approvalResp = matchMoveApi
																									.tempConfirmKycCorporate(
																											kycDetail);
																							if (approvalResp.getCode()
																									.equalsIgnoreCase(
																											"S00")) {
																								MPQAccountDetails accDetails = user
																										.getAccountDetail();
																								if (accDetails != null) {
																									// MPQAccountType
																									// accountType=mPQAccountTypeRepository.findByCode(StartUpUtil.KYC);
																									accDetails
																											.setAccountType(
																													accountTypeKYC);
																									mPQAccountDetailRepository
																											.save(accDetails);
																									kycDetail
																											.setAccountType(
																													accountTypeKYC);
																									kycDetail
																											.setRejectionStatus(
																													false);
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"NA");
																										gbr.setStatus(
																												Status.Success);
																										gbr.setSuccessStatus(
																												true);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								} else {
																									kycDetail
																											.setAccountType(
																													accountTypeNONKYC);
																									kycDetail
																											.setRejectionStatus(
																													true);
																									kycDetail
																											.setRejectionReason(
																													"No Account found");
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"No Account found");
																										gbr.setStatus(
																												Status.Failed);
																										gbr.setSuccessStatus(
																												false);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								}
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												approvalResp
																														.getMessage());
																								mKycRepository.save(
																										kycDetail);

																								GroupBulkKyc gbr = groupBulkKycRepository
																										.getUserRowByContact(
																												mobile,
																												fileCatalogue,
																												false,
																												Status.Initiated);
																								if (gbr != null) {
																									gbr.setFailReason(
																											approvalResp
																													.getMessage());
																									gbr.setStatus(
																											Status.Failed);
																									gbr.setSuccessStatus(
																											false);
																									groupBulkKycRepository
																											.save(gbr);
																								}
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											imageUpload2
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										imageUpload2
																												.getMessage());
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								image1Upload
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									image1Upload
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							userIdDetails.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(userIdDetails
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userKyc.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							userKyc.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		} else {
																			kycDetail.setCity("Bangalore");
																			kycDetail.setState("Karnataka");
																			kycDetail.setCountry("India");

																			UserKycResponse userKyc = matchMoveApi
																					.kycUserMMCorporate(kycDetail);
																			if (userKyc.getCode()
																					.equalsIgnoreCase("S00")) {
																				UserKycResponse userIdDetails = matchMoveApi
																						.setIdDetailsCorporate(
																								kycDetail);
																				if (userIdDetails.getCode()
																						.equalsIgnoreCase("S00")) {
																					UserKycResponse image1Upload = matchMoveApi
																							.setImagesForKyc1(
																									kycDetail);
																					if (image1Upload.getCode()
																							.equalsIgnoreCase("S00")) {
																						UserKycResponse imageUpload2 = matchMoveApi
																								.setImagesForKyc2(
																										kycDetail);
																						if (imageUpload2.getCode()
																								.equalsIgnoreCase(
																										"S00")) {
																							UserKycResponse approvalResp = matchMoveApi
																									.tempConfirmKycCorporate(
																											kycDetail);
																							if (approvalResp.getCode()
																									.equalsIgnoreCase(
																											"S00")) {
																								MPQAccountDetails accDetails = user
																										.getAccountDetail();
																								if (accDetails != null) {
																									MPQAccountType accType = mPQAccountTypeRepository
																											.findByCode(
																													StartUpUtil.KYC);
																									accDetails
																											.setAccountType(
																													accType);
																									mPQAccountDetailRepository
																											.save(accDetails);
																									kycDetail
																											.setAccountType(
																													accType);
																									kycDetail
																											.setRejectionStatus(
																													false);
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"NA");
																										gbr.setStatus(
																												Status.Success);
																										gbr.setSuccessStatus(
																												true);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								} else {
																									kycDetail
																											.setAccountType(
																													accountTypeNONKYC);
																									kycDetail
																											.setRejectionStatus(
																													true);
																									kycDetail
																											.setRejectionReason(
																													"No Account found");
																									mKycRepository.save(
																											kycDetail);

																									GroupBulkKyc gbr = groupBulkKycRepository
																											.getUserRowByContact(
																													mobile,
																													fileCatalogue,
																													false,
																													Status.Initiated);
																									if (gbr != null) {
																										gbr.setFailReason(
																												"No Account found");
																										gbr.setStatus(
																												Status.Failed);
																										gbr.setSuccessStatus(
																												false);
																										groupBulkKycRepository
																												.save(gbr);
																									}
																								}
																							} else {
																								kycDetail
																										.setAccountType(
																												accountTypeNONKYC);
																								kycDetail
																										.setRejectionStatus(
																												true);
																								kycDetail
																										.setRejectionReason(
																												approvalResp
																														.getMessage());
																								mKycRepository.save(
																										kycDetail);

																								GroupBulkKyc gbr = groupBulkKycRepository
																										.getUserRowByContact(
																												mobile,
																												fileCatalogue,
																												false,
																												Status.Initiated);
																								if (gbr != null) {
																									gbr.setFailReason(
																											approvalResp
																													.getMessage());
																									gbr.setStatus(
																											Status.Failed);
																									gbr.setSuccessStatus(
																											false);
																									groupBulkKycRepository
																											.save(gbr);
																								}
																							}
																						} else {
																							kycDetail.setAccountType(
																									accountTypeNONKYC);
																							kycDetail
																									.setRejectionStatus(
																											true);
																							kycDetail
																									.setRejectionReason(
																											imageUpload2
																													.getMessage());
																							mKycRepository
																									.save(kycDetail);

																							GroupBulkKyc gbr = groupBulkKycRepository
																									.getUserRowByContact(
																											mobile,
																											fileCatalogue,
																											false,
																											Status.Initiated);
																							if (gbr != null) {
																								gbr.setFailReason(
																										imageUpload2
																												.getMessage());
																								gbr.setStatus(
																										Status.Failed);
																								gbr.setSuccessStatus(
																										false);
																								groupBulkKycRepository
																										.save(gbr);
																							}
																						}
																					} else {
																						kycDetail.setAccountType(
																								accountTypeNONKYC);
																						kycDetail.setRejectionStatus(
																								true);
																						kycDetail.setRejectionReason(
																								image1Upload
																										.getMessage());
																						mKycRepository.save(kycDetail);

																						GroupBulkKyc gbr = groupBulkKycRepository
																								.getUserRowByContact(
																										mobile,
																										fileCatalogue,
																										false,
																										Status.Initiated);
																						if (gbr != null) {
																							gbr.setFailReason(
																									image1Upload
																											.getMessage());
																							gbr.setStatus(
																									Status.Failed);
																							gbr.setSuccessStatus(false);
																							groupBulkKycRepository
																									.save(gbr);
																						}
																					}
																				} else {
																					kycDetail.setAccountType(
																							accountTypeNONKYC);
																					kycDetail.setRejectionStatus(true);
																					kycDetail.setRejectionReason(
																							userIdDetails.getMessage());
																					mKycRepository.save(kycDetail);

																					GroupBulkKyc gbr = groupBulkKycRepository
																							.getUserRowByContact(mobile,
																									fileCatalogue,
																									false,
																									Status.Initiated);
																					if (gbr != null) {
																						gbr.setFailReason(userIdDetails
																								.getMessage());
																						gbr.setStatus(Status.Failed);
																						gbr.setSuccessStatus(false);
																						groupBulkKycRepository
																								.save(gbr);
																					}

																				}
																			} else {
																				kycDetail.setAccountType(
																						accountTypeNONKYC);
																				kycDetail.setRejectionStatus(true);
																				kycDetail.setRejectionReason(
																						userKyc.getMessage());
																				mKycRepository.save(kycDetail);

																				GroupBulkKyc gbr = groupBulkKycRepository
																						.getUserRowByContact(mobile,
																								fileCatalogue, false,
																								Status.Initiated);
																				if (gbr != null) {
																					gbr.setFailReason(
																							userKyc.getMessage());
																					gbr.setStatus(Status.Failed);
																					gbr.setSuccessStatus(false);
																					groupBulkKycRepository.save(gbr);
																				}
																			}
																		}
																	} else {
																		kycDetail.setAccountType(accountType);
																		kycDetail.setRejectionStatus(true);
																		kycDetail
																				.setRejectionReason("Already KYC user");
																		mKycRepository.save(kycDetail);

																		GroupBulkKyc gbr = groupBulkKycRepository
																				.getUserRowByContact(mobile,
																						fileCatalogue, false,
																						Status.Initiated);
																		if (gbr != null) {
																			gbr.setFailReason("Already KYC user");
																			gbr.setStatus(Status.Failed);
																			gbr.setSuccessStatus(false);
																			groupBulkKycRepository.save(gbr);
																		}
																	}
																} else {
																	kycDetail.setAccountType(accountTypeNONKYC);
																	kycDetail.setRejectionStatus(true);
																	kycDetail.setRejectionReason(
																			"Account Details does not exist");
																	mKycRepository.save(kycDetail);

																	GroupBulkKyc gbr = groupBulkKycRepository
																			.getUserRowByContact(mobile, fileCatalogue,
																					false, Status.Initiated);
																	if (gbr != null) {
																		gbr.setFailReason(
																				"Account Details does not exist");
																		gbr.setStatus(Status.Failed);
																		gbr.setSuccessStatus(false);
																		groupBulkKycRepository.save(gbr);
																	}
																}
															}
														} else {
															GroupBulkKyc gbr = groupBulkKycRepository
																	.getUserRowByContact(mobile, fileCatalogue, false,
																			Status.Initiated);
															if (gbr != null) {
																gbr.setFailReason("user not found");
																gbr.setStatus(Status.Failed);
																gbr.setSuccessStatus(false);
																groupBulkKycRepository.save(gbr);
															}
														}
													}
												}
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				fileCatalogue.setSchedulerStatus(true);
				groupFileCatalogueRepository.save(fileCatalogue);
			}
		}

	}

	public void kycStatusCheck() {
		List<MUser> userList = userRespository.getKYCUserListForKYCCheck();
		if (userList != null && userList.size() > 0) {
			for (int i = 0; i < userList.size(); i++) {
				MUser user = userList.get(i);
				UserKycResponse resp = matchMoveApi.checkKYCStatus(user);
				if (ResponseStatus.SUCCESS.getValue().equals(resp.getCode())) {
					if ("rejected".equalsIgnoreCase(resp.getStatus())) {
						MKycDetail detail = mKycRepository.findByUser(user);
						if (detail != null) {
							detail.setRejectionStatus(true);
							detail.setRejectionReason("Rejected on matchmove end");
							detail.setLastModified(new Date());
							mKycRepository.save(detail);
						}

						MPQAccountDetails account = user.getAccountDetail();
						MPQAccountType type = account.getAccountType();
						if (StartUpUtil.KYC.equals(type.getCode())) {
							MPQAccountType mat = mPQAccountTypeRepository.findByCode(StartUpUtil.NON_KYC);
							account.setAccountType(mat);
							mPQAccountDetailRepository.save(account);
						}
					}
				}
			}
		}
	}

	public void loadWalletByVA() {
		try {

			List<LoadWalletByVARequest> list = loadWalletByVARequestRepository.getPendingList(Status.Pending, false);
			if (list != null && list.size() > 0) {
				for (LoadWalletByVARequest lw : list) {
					if (Status.Success.getValue().equalsIgnoreCase(lw.getMmStatus())
							&& StartUpUtil.PRODUCTCODE_VA.equalsIgnoreCase(lw.getProductCode())) {
						CorporateAgentDetails mmw = corporateAgentDetailsRepository
								.getByVirtualAccount(lw.getVirtualAccountNumber());
						if (mmw != null) {
							MUser user = mmw.getCorporate();
							if (user != null) {
								MService service = mServiceRepository
										.findServiceByCode(RazorPayConstants.VA_LOAD_CARD_SERVICE_CODE);

								if (service != null && Status.Active.equals(service.getStatus())) {
									MTransaction transaction = null;
									if (lw.getTransactionRefNo() != null) {
										transaction = transactionRepository
												.findByTransactionRefNo(lw.getTransactionRefNo());
										if (transaction == null) {
											transaction = loadMoneyApi.initiateTransaction(user.getUsername(),
													Double.parseDouble(lw.getAmount()),
													RazorPayConstants.VA_LOAD_CARD_SERVICE_CODE);
										}
									} else {
										transaction = loadMoneyApi.initiateTransaction(user.getUsername(),
												Double.parseDouble(lw.getAmount()),
												RazorPayConstants.VA_LOAD_CARD_SERVICE_CODE);
									}

									if (transaction != null && !Status.Success.equals(transaction.getStatus())) {
										MPQAccountDetails pq = user.getAccountDetail();
										pq.setBalance(pq.getBalance() + transaction.getAmount());
										mPQAccountDetailRepository.save(pq);

										transaction.setRetrivalReferenceNo(lw.getMmId());
										transaction.setCardLoadStatus(Status.Success.getValue());
										transaction.setStatus(Status.Success);
										transaction.setCurrentBalance(pq.getBalance());
										transactionRepository.save(transaction);

										// send success sms
										senderApi.sendTransactionSMS(SMSAccount.PAYQWIK_OTP,
												SMSTemplete.LOADMONEY_SUCCESS, user, null, transaction);

										lw.setFailedStatus(false);
										lw.setStatus(Status.Success);
										lw.setTransactionRefNo(transaction.getTransactionRefNo());
									} else {
										lw.setFailedReason("Transaction is already Success");
										lw.setFailedStatus(true);
									}
								} else {
									lw.setFailedReason("Service is down.");
									lw.setFailedStatus(true);
								}
							} else {
								lw.setFailedReason("User not found");
								lw.setFailedStatus(true);
							}
						} else {
							lw.setFailedReason("Corporate not found");
							lw.setFailedStatus(true);
						}
					} else {
						lw.setFailedReason("Status is not success or Product Code mismatch");
						lw.setFailedStatus(true);
					}
					loadWalletByVARequestRepository.save(lw);
				}
			} else {
				System.out.println("List size is Zero.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}