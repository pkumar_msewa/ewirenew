package com.cashiercards.paramerterization;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.msscard.entity.AbstractEntity;

@Entity
@Table(name="DataConfig")
public class DataConfig extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cardFees;
	private String minimumCardBalance;
	private String activePromoCode1;
	private String activePromoCode2;
	private String activePromoCode3;
	private String sendMoneyMinimum;
	private String sendMoneyMaximum;
	private String phyCorpStatus;
	private double virtualCardLoadDailyLimit;
	private double physicalCardLoadDailyLimit;
	private double virtualCardLoadMonthlyLimit;
	private double physicalCardLoadMonthlyLimit;
	private String cardBaseFare;
	private boolean allowRegistration;
	private String upiLoadMin;
	
	
		
	public String getUpiLoadMin() {
		return upiLoadMin;
	}
	public void setUpiLoadMin(String upiLoadMin) {
		this.upiLoadMin = upiLoadMin;
	}
	public boolean isAllowRegistration() {
		return allowRegistration;
	}
	public void setAllowRegistration(boolean allowRegistration) {
		this.allowRegistration = allowRegistration;
	}
	public String getCardBaseFare() {
		return cardBaseFare;
	}
	public void setCardBaseFare(String cardBaseFare) {
		this.cardBaseFare = cardBaseFare;
	}
	public double getVirtualCardLoadDailyLimit() {
		return virtualCardLoadDailyLimit;
	}
	public void setVirtualCardLoadDailyLimit(double virtualCardLoadDailyLimit) {
		this.virtualCardLoadDailyLimit = virtualCardLoadDailyLimit;
	}
	public double getPhysicalCardLoadDailyLimit() {
		return physicalCardLoadDailyLimit;
	}
	public void setPhysicalCardLoadDailyLimit(double physicalCardLoadDailyLimit) {
		this.physicalCardLoadDailyLimit = physicalCardLoadDailyLimit;
	}
	public double getVirtualCardLoadMonthlyLimit() {
		return virtualCardLoadMonthlyLimit;
	}
	public void setVirtualCardLoadMonthlyLimit(double virtualCardLoadMonthlyLimit) {
		this.virtualCardLoadMonthlyLimit = virtualCardLoadMonthlyLimit;
	}
	public double getPhysicalCardLoadMonthlyLimit() {
		return physicalCardLoadMonthlyLimit;
	}
	public void setPhysicalCardLoadMonthlyLimit(double physicalCardLoadMonthlyLimit) {
		this.physicalCardLoadMonthlyLimit = physicalCardLoadMonthlyLimit;
	}
	public String getPhyCorpStatus() {
		return phyCorpStatus;
	}
	public void setPhyCorpStatus(String phyCorpStatus) {
		this.phyCorpStatus = phyCorpStatus;
	}
	public String getSendMoneyMinimum() {
		return sendMoneyMinimum;
	}
	public void setSendMoneyMinimum(String sendMoneyMinimum) {
		this.sendMoneyMinimum = sendMoneyMinimum;
	}
	public String getSendMoneyMaximum() {
		return sendMoneyMaximum;
	}
	public void setSendMoneyMaximum(String sendMoneyMaximum) {
		this.sendMoneyMaximum = sendMoneyMaximum;
	}
	public String getCardFees() {
		return cardFees;
	}
	public void setCardFees(String cardFees) {
		this.cardFees = cardFees;
	}
	public String getMinimumCardBalance() {
		return minimumCardBalance;
	}
	public void setMinimumCardBalance(String minimumCardBalance) {
		this.minimumCardBalance = minimumCardBalance;
	}
	public String getActivePromoCode1() {
		return activePromoCode1;
	}
	public void setActivePromoCode1(String activePromoCode1) {
		this.activePromoCode1 = activePromoCode1;
	}
	public String getActivePromoCode2() {
		return activePromoCode2;
	}
	public void setActivePromoCode2(String activePromoCode2) {
		this.activePromoCode2 = activePromoCode2;
	}
	public String getActivePromoCode3() {
		return activePromoCode3;
	}
	public void setActivePromoCode3(String activePromoCode3) {
		this.activePromoCode3 = activePromoCode3;
	}
	
	
	
}
