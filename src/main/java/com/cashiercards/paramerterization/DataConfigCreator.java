package com.cashiercards.paramerterization;

import com.msscard.repositories.DataConfigRepository;

public class DataConfigCreator {

	private DataConfigRepository dataConfigRepository;
	
	public DataConfigCreator(DataConfigRepository dataConfigRepository) {
		super();
		this.dataConfigRepository = dataConfigRepository;
	}

	public void create(){
		
		DataConfig config=new DataConfig();
		config.setCardFees("10");
		config.setMinimumCardBalance("10");
		config.setSendMoneyMaximum("1000");
		config.setSendMoneyMinimum("1");
		dataConfigRepository.save(config);
	}
}
