package com.razorpay.entities;

import org.json.JSONException;

public class AddonClient extends ApiClient {

  AddonClient(String auth) {
    super(auth);
  }

  // To create an Addon, use the createAddon method of SubscriptionClient
  public Addon fetch(String id) throws RazorpayException, JSONException {
    return get(String.format(Constants.ADDON_GET, id), null);
  }

  public void delete(String id) throws RazorpayException, JSONException {
    delete(String.format(Constants.ADDON_DELETE, id), null);
  }
}
