package com.razorpay.constants;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidKeyException;
import javax.crypto.Mac;
import com.msscard.app.model.request.PGTransaction;
import com.msscard.util.StartUpUtil;

public class RazorPayConstants {

	public static final String KEY_ID = getKeyId();
	public static final String KEY_SECRET = getKeySecret();

	public static final String UPI_URL = getUPIUrl();
	public static final String UPI_STATUS = getUPIStatus();
	public static final String UPI_MERCHANT_ID = getUPIMerchantId();
	public static final String UPI_TOKEN = getUPIToken();

	// Razorpay LIVE
	private static final String KEY_ID_LIVE = "rzp_live_1PSfjQPHqWf0WC";
	private static final String KEY_SECRET_LIVE = "x9prAI3gv5jcPceXAOOHjfrd";

	// Razorpay TEST
	private static final String KEY_ID_TEST = "rzp_test_FbHAUNTWtCOWEo";
	private static final String KEY_SECRET_TEST = "tfN2vNOuGwJkxw32Al26IbtU";

	// UPI LIVE
	private static final String UPI_URL_LIVE = "https://msspay.in/ws/api/authValidate";
	private static final String UPI_STATUS_LIVE = "https://mycashier.in/ws/api/UpiStatus";
	private static final String UPI_MERCHANT_ID_LIVE = "48";
	private static final String UPI_TOKEN_LIVE = "745DDD4F44AE2BF18E800554BD3F9663";

	// UPI TEST
	private static final String UPI_URL_TEST = "http://106.51.8.246/cashier/ws/api/authValidate";
	private static final String UPI_STATUS_TEST = "http://106.51.8.246/cashier/ws/api/UpiStatus";
	private static final String UPI_MERCHANT_ID_TEST = "52";
	private static final String UPI_TOKEN_TEST = "BDCC05A3053BBA47366DBB0F78161E67";

	public static final String LOGO_PATH = "resources/images/logo1.png";
	public static final String SERVICE_CODE = "LMS";
	public static final String PROMO_SERVICES = "PROMO";
	public static final String UPI_SERVICE = "UPS";

	public static final String VA_LOAD_CARD_SERVICE_CODE = "VAL";

	private static String getKeyId() {
		if (StartUpUtil.PRODUCTION) {
			return KEY_ID_LIVE;
		} else {
			return KEY_ID_TEST;
		}
	}

	private static String getKeySecret() {
		if (StartUpUtil.PRODUCTION) {
			return KEY_SECRET_LIVE;
		} else {
			return KEY_SECRET_TEST;
		}
	}

	private static String getUPIUrl() {
		if (StartUpUtil.PRODUCTION) {
			return UPI_URL_LIVE;
		} else {
			return UPI_URL_TEST;
		}
	}

	private static String getUPIStatus() {
		if (StartUpUtil.PRODUCTION) {
			return UPI_STATUS_LIVE;
		} else {
			return UPI_STATUS_TEST;
		}
	}

	private static String getUPIMerchantId() {
		if (StartUpUtil.PRODUCTION) {
			return UPI_MERCHANT_ID_LIVE;
		} else {
			return UPI_MERCHANT_ID_TEST;
		}
	}

	private static String getUPIToken() {
		if (StartUpUtil.PRODUCTION) {
			return UPI_TOKEN_LIVE;
		} else {
			return UPI_TOKEN_TEST;
		}
	}

	/**
	 * PAYPHI LIVE
	 */

	public static final String SALE_URL = "https://secure-ptg.payphi.com/pg/api/sale?v=2";
	public static final String STATUS_CHECK = "https://secure-ptg.payphi.com/pg/api/command?v=2";
	public static final String MERCHANT_ID = "P_30016";
	public static final String CURRENCY_CODE = "356";
	public static final String PAY_TYPE = "0";
	public static final String RETURN_URL = "https://liveewire.com/Api/v1/User/Android/en/LoadMoney/PGHandler";
	public static final String TRANSACTION_TYPE = "SALE";
	public static final String TRANSACTION_KEY_TEST = "f1fc4e1d22ae4c20a188dc0dcc1f297f";

	public static String hmacDigest(String msg, String keyString) {
		String digest = null;
		try {
			SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA256");
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(key);
			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
			StringBuffer hash = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
	}

	public static String composeMessage(PGTransaction message) {

		String msg = message.getAmount() + message.getCurrencyCode() + message.getCustomerEmailId()
				+ message.getCustomerMobileNo() + message.getMerchantId() + message.getMerchantTxnNo()
				+ message.getPayType() + message.getReturnURL() + message.getTransactionType() + message.getTxnDate();

		return hmacDigest(msg, TRANSACTION_KEY_TEST);

	}

	public static String composeMessageStatusCheck(String merchantID, String merchantTxnNo, String originalTxnNo,
			String transactionType) {
		String msg = merchantID + merchantTxnNo + originalTxnNo + transactionType;
		return hmacDigest(msg, TRANSACTION_KEY_TEST);
	}

}