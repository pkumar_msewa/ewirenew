package com.razorpay.constants;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

import com.msscard.util.StartUpUtil;

public class MdexConstants {

	public static final String MDEX_DOMAIN = getMDEXDomain();
	public static final String MDEX_KEY = getMDEXKey();
	public static final String MDEX_TOKEN = getMDEXToken();
	public static final String MDEX_CLIENTIP = getClientIP();

	// MDEX LIVE
	private static final String MDEX_DOMAIN_LIVE = "https://mdex.in";
	private static final String MDEX_KEY_LIVE = "41982394FCC73DBBC9F49CAF59D3E61D365755D6";
	private static final String MDEX_TOKEN_LIVE = "MTUyOTEzMzAzMzgxODM1NjAwMDAwMDAwMDAwMjA=";
	private static final String MDEX_CLIENTIP_LIVE = "52.15.165.153";

	// MDEX TEST
	private static final String MDEX_DOMAIN_TEST = "http://106.51.8.246/mdex";
	private static final String MDEX_KEY_TEST = "86A2143BC1E75F26D8419BF88E49B3D58DCA2B36";
	private static final String MDEX_TOKEN_TEST = "MTUyNDY1ODY5NjQxNjM1NjAwMDAwMDAwMDAwMjA=";
	private static final String MDEX_CLIENTIP_TEST = "106.51.8.246";

	public static final String MDEX_URL = MDEX_DOMAIN + "/ws/api/v1/Client/en/";
	public static final String MDEX_SERVICES = MDEX_DOMAIN + "/api/recharge/services/";

	public static final String MDEX_TRAVEL_URL = MDEX_DOMAIN + "/Api/Client/Website/en";
	public static final String TELCO_OPERATOR = MDEX_DOMAIN + "/api/recharge/gettelco";
	public static final String TELCO_PLANS = MDEX_DOMAIN + "/api/recharge/getplans";
	public static final String DUE_AMOUNT = MDEX_DOMAIN + "/api/recharge/dueamount";
	public static final String MDEX_BROWSE_PLANS = MDEX_DOMAIN + "/api/recharge/browseplans";
	public static final String MDEX_MNPLOOKUP = MDEX_DOMAIN + "/api/recharge/mnplookup";

	public static final String FINGOOLE_REG = MDEX_DOMAIN + "/Api/v1/Client/en/insurance/RegisterUser";
	public static final String FINGOOLE_BATCH = MDEX_DOMAIN + "/Api/v1/Client/en/insurance/GetBatchNo";
	public static final String FINGOOLE_PROCESS = MDEX_DOMAIN + "/Api/v1/Client/en/insurance/ProcessTxn";
	public static final String FINGOOLE_SERVICES = MDEX_DOMAIN + "/api/recharge/services/FING";

	public static final String BUS_GET_ALL_AVAILABLE_TRIPS = "/Bus/GetAllAvailableTrips";
	public static final String BUS_GET_SEAT_DETAILS = "/Bus/GetSeatDetails";
	public static final String BUS_GET_TXN_ID_UPDATED = "/Bus/GetTransactionIdUpdated";
	public static final String BUS_GET_BOOK_TICKET_UPDATED = "/Bus/BookTicketUpdated";
	public static final String BUS_GET_ISCANCELABLE_TICKET = "/Bus/IsCancellable";
	public static final String BUS_GET_CANCEL_TICKET = "/Bus/CancelTicket";

	public static String getMdexBasicAuthorization() {
		String result = "Basic ";
		String credentials = MDEX_KEY + ":" + MDEX_TOKEN;
		String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
		result = result + encodedCredentials;
		return result;
	}

	private static String getMDEXDomain() {
		if (StartUpUtil.PRODUCTION) {
			return MDEX_DOMAIN_LIVE;
		} else {
			return MDEX_DOMAIN_TEST;
		}
	}

	private static String getMDEXKey() {
		if (StartUpUtil.PRODUCTION) {
			return MDEX_KEY_LIVE;
		} else {
			return MDEX_KEY_TEST;
		}
	}

	private static String getMDEXToken() {
		if (StartUpUtil.PRODUCTION) {
			return MDEX_TOKEN_LIVE;
		} else {
			return MDEX_TOKEN_TEST;
		}
	}

	private static String getClientIP() {
		if (StartUpUtil.PRODUCTION) {
			return MDEX_CLIENTIP_LIVE;
		} else {
			return MDEX_CLIENTIP_TEST;
		}
	}
}