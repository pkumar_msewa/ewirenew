package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.KYCPending;
import com.msscard.model.Status;

public interface KYCPendingRepository extends CrudRepository<KYCPending, Long>,
		PagingAndSortingRepository<KYCPending, Long>, JpaSpecificationExecutor<KYCPending> {

	@Query("SELECT kp.mobile FROM KYCPending kp WHERE kp.status = ?1")
	List<String> getList(Status status);

}