package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.PromoCodeRequest;

public interface PromoCodeRequestRepository extends CrudRepository<PromoCodeRequest, Long>, PagingAndSortingRepository<PromoCodeRequest, Long>, JpaSpecificationExecutor<PromoCodeRequest>
{

	@Query("select r from PromoCodeRequest r")
	List<PromoCodeRequest> findAllRequest();

	@Query("select r from PromoCodeRequest r where r.promocode=?1 and r.user=?2")
	PromoCodeRequest findByPromoCodeAndUSer(String promoCode, MUser usr);
	
	@Query("select r from PromoCodeRequest r where r.status='16'")
	List<PromoCodeRequest> findPromoCodeWhichAreNotRedeemed();

}
