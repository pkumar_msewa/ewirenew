package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.msscard.entity.MUserDetails;

public interface MUserDetailRepository
		extends CrudRepository<MUserDetails, Long>, JpaSpecificationExecutor<MUserDetails> {

	@Query("select u from MUserDetails u where u.email=?1")
	List<MUserDetails> checkMail(String mail);

	@Query("select u from MUserDetails u where u.contactNo=?1")
	List<MUserDetails> checkContactNo(String contactNo);

	/*
	 * @Query("select COUNT(u) from MUserDetails u where u.gender=?1") Long
	 * countUsersByGender(Gender gender);
	 */
	@Modifying
	@Transactional
	@Query("update MUserDetails u set u.image=?1 where u.contactNo=?2")
	int updateUserImage(String url, String username);

	/*
	 * @Modifying
	 * 
	 * @Transactional
	 * 
	 * @Query("update MUserDetails u set u.address=?1, u.firstName=?2, u.lastName=?3, u.email=?4, u.gender=?6 where u.contactNo=?5"
	 * ) int updateUserDetail(String address, String firstName, String lastName,
	 * String email, String username,Gender gender);
	 */
	@Modifying
	@Transactional
	@Query("update MUserDetails u set  u.firstName=?1  where u.contactNo=?2")
	int updateUserName(String firstName, String username);

	/*
	 * @Modifying
	 * 
	 * @Transactional
	 * 
	 * @Query("update MUserDetails u set u.address=?1, u.firstName=?2, u.lastName=?3 , u.gender=?5 where u.contactNo=?4"
	 * ) int updateUserDetailOnly(String address, String firstName, String lastName,
	 * String username,Gender gender);
	 */

	@Modifying
	@Transactional
	@Query("update MUserDetails u set u.mpin=?1 where u.contactNo=?2")
	int updateUserMPIN(String mpin, String username);

	@Modifying
	@Transactional
	@Query("update MUserDetails u set u.mpin=null where u.contactNo=?1")
	int deleteUserMPIN(String username);

	@Modifying
	@Transactional
	@Query("update MUserDetails c set c.email=?1 where c.id =?2")
	int updateChangeEmail(String email, long id);

	@Query("select u from MUserDetails u where u.contactNo=?1")
	MUserDetails findByContactNo(String contactNo);

	@Query("select u from MUserDetails u where u.id=?1")
	MUserDetails findUserById(long id);

	@Query("select u from MUserDetails u where u.dateOfBirth is not null and MONTH(u.dateOfBirth)=?1 AND DAY(u.dateOfBirth)=?2")
	List<MUserDetails> findUserBdayDate(int month, int day);

	@Query("select u from MUserDetails u where u.contactNo=?1")
	List<MUserDetails> getAllMerchants(String username);

	@Query("select u from MUserDetails u where u.firstName=?1")
	List<MUserDetails> getByFirstName(String firstName);

	@Query("SELECT md FROM MUserDetails md WHERE md.contactNo IN (ky)")
	List<MUserDetails> getListByMobile(List<String> ky);

}