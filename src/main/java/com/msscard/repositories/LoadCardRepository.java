package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.LoadCard;
import com.msscard.entity.MUser;
import com.msscard.model.Role;

public interface LoadCardRepository extends CrudRepository<LoadCard, Long>, PagingAndSortingRepository<LoadCard, Long>, 
	JpaSpecificationExecutor<LoadCard> {
	
	@Query("select l from LoadCard l where l.agent=?1 ORDER BY l.created DESC")
	List<LoadCard> getLoadcardDataBasedOnAgent(MUser agent);
	
	@Query("select l from LoadCard l where l.role=?1 ORDER BY l.created DESC")
	List<LoadCard> getDetailsBasedOnUserType(Role role);
	
	@Query("select sum(l.amount) from LoadCard l where l.agent=?1")
	double getTotalAmountOfAgent(MUser agent);
	
	@Query("select l from LoadCard l where l.agent=?1 AND l.created BETWEEN ?2 AND ?3")
	List<LoadCard> getLoadcardDataBasedOnAgentAndDate(MUser agent,Date startDate,Date endDate);
	

}
