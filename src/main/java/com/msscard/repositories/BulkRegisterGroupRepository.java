package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkRegisterGroup;
import com.msscard.entity.MUser;

public interface BulkRegisterGroupRepository extends CrudRepository<BulkRegisterGroup, Long>, PagingAndSortingRepository<BulkRegisterGroup, Long>, JpaSpecificationExecutor<BulkRegisterGroup>{

	@Query("select b from BulkRegisterGroup b where b.user=?1")
	BulkRegisterGroup getByUser(MUser user);

	@Query("SELECT b FROM BulkRegisterGroup b WHERE b.userCreationStatus = ?1 AND b.walletCreationStatus = ?2 AND b.failedStatus = ?3")
	List<BulkRegisterGroup> getListOfUserForWalletCreation(boolean b, boolean c, boolean d);

	@Query("SELECT b FROM BulkRegisterGroup b WHERE b.userCreationStatus = ?1 AND b.failedStatus = ?2")
	List<BulkRegisterGroup> getListOfUserForUserCreation(boolean b, boolean c);
		
}
