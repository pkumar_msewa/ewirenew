package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.ApiVersion;

public interface ApiVersionRepository extends CrudRepository<ApiVersion,Long>,JpaSpecificationExecutor<ApiVersion>,PagingAndSortingRepository<ApiVersion,Long>{

	@Query("select a from ApiVersion a where a.name=?1 AND a.apiStatus=?2")
	ApiVersion getByNameStatus(String name,String apiStatus);

	@Query("select a from ApiVersion a")
	List<ApiVersion> findAllVersions();

}
