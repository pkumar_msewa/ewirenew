package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MBulkRegister;

public interface MBulkRegisterRepository extends CrudRepository<MBulkRegister, Long>, PagingAndSortingRepository<MBulkRegister, Long>, JpaSpecificationExecutor<MBulkRegister> {

	
	@Query("select u from MBulkRegister u where u.contactNo=?1")
	MBulkRegister findByContactno(String contactNo);

	@Query("select u from MBulkRegister u order by created desc")
	List<MBulkRegister> findLast10Register();

}
