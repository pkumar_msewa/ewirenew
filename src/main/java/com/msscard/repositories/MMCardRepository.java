package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;

public interface MMCardRepository extends CrudRepository<MMCards, Long>, PagingAndSortingRepository<MMCards, Long>,
		JpaSpecificationExecutor<MMCards> {

	@Query("select c from MMCards c where c.cardId=?1 and c.hasPhysicalCard=0")
	MMCards getVirtualCardByCardId(String cardId);

	@Query("select c from MMCards c where c.wallet.user=?1 and c.hasPhysicalCard=0")
	MMCards getVirtualCardsByCardUser(MUser user);

	@Query("select c from MMCards c where c.hasPhysicalCard=0")
	List<MMCards> fetchAllVirtualCards();

	@Query("select c from MMCards c where c.wallet.user=?1 and c.hasPhysicalCard=1")
	MMCards getPhysicalCardByUser(MUser user);

	@Query("select count(c) from MMCards c where c.wallet.user=?1")
	Long getCardsByUser(MUser user);

	@Query("select c from MMCards c where c.cardId=?1 and c.hasPhysicalCard=1")
	MMCards getPhysicalCardByCardId(String cardId);

	@Query("select c from MMCards c where c.cardId=?1")
	MMCards getCardByCardId(String cardId);

	@Query("select c from MMCards c where c.cardId=?1 and c.hasPhysicalCard=1")
	MMCards getCardByCardIdPhysical(String cardId);

	@Query("select c from MMCards c where c.cardId=?1 and c.hasPhysicalCard=0")
	MMCards getCardByCardIdVirtual(String cardId);

	@Query("select count(c) from MMCards c")
	long findTotalCards();

	@Query("select c from MMCards c order by c.created desc")
	List<MMCards> findAllCards();

	@Query("select c from MMCards c where c.hasPhysicalCard=0 order by c.created desc")
	List<MMCards> findAllVirtualCards();

	@Query("select c from MMCards c where c.hasPhysicalCard=1 order by c.created desc")
	List<MMCards> findAllPhysicalCards();

	@Query("select c from MMCards c where c.wallet.user=?1")
	MMCards findCardByUser(MUser usera);

	@Query("select c from MMCards c where c.wallet.user=?1 and c.status=?2")
	MMCards getCardByUserAndStatus(MUser user, String string);

	@Query("select c from MMCards c where c.wallet.user=?1 and c.blocked=?2")
	MMCards getCardByUserAndBlockStatus(MUser user, boolean flag);

	@Query("select count(c) from MMCards c where c.hasPhysicalCard=1")
	long findTotalPhysicalCards();

	@Query("select c from MMCards c where DATE(c.created) BETWEEN ?1 AND ?2 AND c.hasPhysicalCard=?3 order by created desc")
	List<MMCards> getCardListByDate(Date fromDate, Date toDate, boolean flag);

	@Query("select c from MMCards c where c.wallet.user=?1 order by created desc")
	List<MMCards> findCardByUserId(MUser usera);

	@Query("select c from MMCards c where DATE(c.created) BETWEEN ?1 AND ?2")
	Page<MMCards> findAllByFiltered(Pageable page, Date from, Date to);

	@Query("select c from MMCards c where c.hasPhysicalCard=?1 and DATE(c.created) BETWEEN ?2 AND ?3 and c.status=?4")
	Page<MMCards> getAllCardsByPage(Pageable page, boolean flag, Date from, Date to, String status);

	@Query("select c from MMCards c where c.wallet.user=?1 AND  c.status=?2")
	MMCards getCardDetailsByUserAndStatus(MUser user, String status);

	@Query("select count(c) from MMCards c where c.hasPhysicalCard='1' and c.status='Active'")
	Long getCountPhysicalCards();

	@Query("select count(c) from MMCards c where c.hasPhysicalCard='0' and c.status='Active'")
	Long getCountVirtualCards();

	@Query("select count(c) from MMCards c where c.hasPhysicalCard=0 and c.created BETWEEN ?1 AND ?2")
	long findVirtualCards(Date from, Date to);

	@Query("select c from MMCards c where c.hasPhysicalCard=?1 and DATE(c.created) BETWEEN ?2 AND ?3 and c.status=?4")
	List<MMCards> getAllCards(boolean flag, Date from, Date to, String status);

	@Query("select c from MMCards c where c.hasPhysicalCard=?1 and DATE(c.created) BETWEEN ?2 AND ?3")
	List<MMCards> getAllCards(boolean flag, Date from, Date to);

	@Query("select c from MMCards c where c.status='Inactive'")
	List<MMCards> getBlockedCards();

	@Query("select c from MMCards c where c.wallet.user=?1 AND  c.status=?2 AND c.hasPhysicalCard=?3")
	MMCards getCardDetailsByUserAndStatus(MUser user, String status, boolean hasPhysicalCard);

	@Query("select c from MMCards c where c.wallet.user=?1")
	List<MMCards> getCardsListByUser(MUser user);

	@Query("select c from MMCards c where c.hasPhysicalCard=?1 and c.blocked=?2")
	List<MMCards> getPhysicalCards(boolean ph, boolean active);

	@Query("select c from MMCards c where c.wallet.id=?1")
	MMCards getCardByWalletId(long walletId);

	@Query("select c from MMCards c where c.wallet.id = ?1 AND c.hasPhysicalCard = ?2")
	MMCards getPhysicalCardByWalletId(long walletId, boolean b);

	@Query("SELECT c FROM MMCards c WHERE c.hasPhysicalCard = ?1 AND c.status = ?2 AND c.wallet = ?3")
	MMCards getInactivePhysical(boolean b, String value, MatchMoveWallet mmw);
}
