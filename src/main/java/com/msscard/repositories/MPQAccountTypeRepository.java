package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MPQAccountType;

public interface MPQAccountTypeRepository extends CrudRepository<MPQAccountType, Long>, JpaSpecificationExecutor<MPQAccountType> {

	@Query("select u from MPQAccountType u")
	List<MPQAccountType> findAll();

	@Query("select u from MPQAccountType u where u.code=?1")
	MPQAccountType findByCode(String code);


}
