package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.model.Status;

public interface BulkCardAssignmentGroupRepository extends CrudRepository<BulkCardAssignmentGroup, Long>,
JpaSpecificationExecutor<BulkCardAssignmentGroup>, PagingAndSortingRepository<BulkCardAssignmentGroup, Long> {

	@Query("SELECT g FROM BulkCardAssignmentGroup g WHERE g.creationStatus=?1 AND g.groupDetails.groupDetails.email=?2")
	Page<BulkCardAssignmentGroup> getBulkCardFailList(Pageable pageable, Status fail, String email);

	@Query("SELECT g FROM BulkCardAssignmentGroup g WHERE DATE(g.created) BETWEEN ?1 AND ?2 AND g.creationStatus=?3 AND g.groupDetails.groupDetails.email=?4")
	Page<BulkCardAssignmentGroup> getBulkCardFailListPost(Pageable pageable, Date from, Date to, Status fail,
			String email);

	@Query("SELECT b FROM BulkCardAssignmentGroup b WHERE b.activationStatus='Inactive' AND b.creationStatus='Success'")
	List<BulkCardAssignmentGroup> getBulkCardAssignmentWithStatusInactive();

	@Query("SELECT b FROM BulkCardAssignmentGroup b WHERE b.activationStatus = ?1 AND b.creationStatus = ?2 AND b.failedStatus = ?3")
	List<BulkCardAssignmentGroup> getListOfUserForCardAssign(Status status, Status status1, boolean d);
	
}
