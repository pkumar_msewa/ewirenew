package com.msscard.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.TwoMinBlock;

public interface TwoMinBlockRepository  extends CrudRepository<TwoMinBlock, Long>, PagingAndSortingRepository<TwoMinBlock, Long>, JpaSpecificationExecutor<TwoMinBlock> {
	
	@Query("select u.user from TwoMinBlock u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.block=?3 AND u.user.authority=?4")
	Page<MUser> findAllBlockUser(Pageable pageable, Date from, Date to, boolean block, String authority);
	
	@Query("select t from TwoMinBlock t where t.user=?1")
	TwoMinBlock getBlockedUser(MUser u);
}
