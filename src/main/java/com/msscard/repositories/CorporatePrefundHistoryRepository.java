package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.CorporatePrefundHistory;
import com.msscard.entity.MUser;

public interface CorporatePrefundHistoryRepository extends CrudRepository<CorporatePrefundHistory, Long>,JpaSpecificationExecutor<CorporatePrefundHistory>{

	@Query("select c from CorporatePrefundHistory c where c.corporate=?1")
	List<CorporatePrefundHistory> getByCorporate(MUser corporate);
	
	@Query("select c from CorporatePrefundHistory c ORDER BY c.id DESC")
	List<CorporatePrefundHistory> findAllRequests();

	@Query("select c from CorporatePrefundHistory c where c.corporate=?3 and DATE(c.created) BETWEEN ?1 AND ?2")
	List<CorporatePrefundHistory> getByCorporateByDate(Date fromDate, Date toDate, MUser user);
}
