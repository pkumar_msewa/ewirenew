package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.MUser;

public interface CorporateAgentDetailsRepository extends CrudRepository<CorporateAgentDetails, Long>,
		JpaSpecificationExecutor<CorporateAgentDetails>, PagingAndSortingRepository<CorporateAgentDetails, Long> {

	@Query("select c from CorporateAgentDetails c where c.corporate=?1")
	CorporateAgentDetails getByCorporateId(MUser corporate);

	@Query("SELECT c FROM CorporateAgentDetails c WHERE c.virtualAccountNumber = ?1")
	CorporateAgentDetails getByVirtualAccount(String virtualAccountNumber);
}