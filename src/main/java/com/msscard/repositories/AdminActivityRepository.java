package com.msscard.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.AdminActivity;

public interface AdminActivityRepository extends CrudRepository<AdminActivity, Long>, PagingAndSortingRepository<AdminActivity, Long>, JpaSpecificationExecutor<AdminActivity> {
	
	@Query("select t from AdminActivity t where DATE(t.created) BETWEEN ?1 AND ?2")
	Page<AdminActivity> getActivities(Pageable pageable, Date from, Date to);
}
