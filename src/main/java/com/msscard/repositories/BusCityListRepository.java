package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BusCityList;

public interface BusCityListRepository extends CrudRepository<BusCityList, Long>, PagingAndSortingRepository<BusCityList, Long>, JpaSpecificationExecutor<BusCityList> {
	
	@Query("select b from BusCityList b")
	List<BusCityList> getAllCityList();
	
	@Query("select b from BusCityList b where b.cityId=?1")
	BusCityList getcity(long citYId);
}
