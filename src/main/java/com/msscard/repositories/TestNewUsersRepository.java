package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.imps.entity.TestNewUsers;

public interface TestNewUsersRepository extends CrudRepository<TestNewUsers, Long>,
		JpaSpecificationExecutor<TestNewUsers>, PagingAndSortingRepository<TestNewUsers, Long> {

	@Query("SELECT tn FROM TestNewUsers tn WHERE tn.userCreationStatus = 0 AND tn.walletCreationStatus = 0 AND tn.cardCreationStatus = 0 AND tn.failedStatus = 0")
	List<TestNewUsers> getData();

	@Query("SELECT tn FROM TestNewUsers tn WHERE tn.userCreationStatus = 1 AND tn.walletCreationStatus = 1 AND tn.cardCreationStatus = 1 AND tn.failedStatus = 0")
	List<TestNewUsers> getSuccessData();

}