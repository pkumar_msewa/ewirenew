package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.PromoCode;

public interface PromoCodeRepository extends CrudRepository<PromoCode, Long>, PagingAndSortingRepository<PromoCode, Long>, JpaSpecificationExecutor<PromoCode>
{

	@Query("select p from PromoCode p where p.promoCode=?1")
	PromoCode fetchByPromoCode(String promoCode);

	@Query("select p from PromoCode p")
	List<PromoCode> findAllPromoCode();
	

}
