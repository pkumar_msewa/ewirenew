package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.GroupOtpMobile;


public interface GroupOtpRepository extends CrudRepository<GroupOtpMobile, String>{

	@Query("select c.mobileNumber from GroupOtpMobile c where c.groupName=?1")
	List<String> getMobileGroup(String group);
	
	@Query("select c from GroupOtpMobile c where c.mobileNumber=?1")
	List<GroupOtpMobile> getGroupDetails(String mobile);
}
