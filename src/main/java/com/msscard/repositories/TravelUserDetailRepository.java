package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.BusTicket;
import com.msscard.entity.TravellerDetails;
import com.msscard.model.Status;

public interface TravelUserDetailRepository extends CrudRepository<TravellerDetails,Long>,JpaSpecificationExecutor<TravellerDetails>{
	
	@Query("select t from TravellerDetails t where t.busTicketId.status!=?1")
	List<TravellerDetails> getTicketDetails(Status status);
	
	@Query("select t from TravellerDetails t where t.busTicketId=?1")
	List<TravellerDetails> getTicketByBusId(BusTicket busTicket);
	
	@Query("select t.seatNo from TravellerDetails t where t.busTicketId=?1")
	List<String> getSeatNosByBusId(BusTicket busTicket);

}
