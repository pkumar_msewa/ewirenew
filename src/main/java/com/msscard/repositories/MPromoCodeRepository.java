package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.PromoCode;

public interface MPromoCodeRepository extends CrudRepository<PromoCode, Long>, JpaSpecificationExecutor<PromoCode>{

	
}
