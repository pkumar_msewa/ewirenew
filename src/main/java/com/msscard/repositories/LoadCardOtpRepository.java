package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.LoadCardOtp;

public interface LoadCardOtpRepository extends CrudRepository<LoadCardOtp, Long>, PagingAndSortingRepository<LoadCardOtp, Long>, JpaSpecificationExecutor<LoadCardOtp> {
	
	@Query("select c from LoadCardOtp c")
	List<LoadCardOtp> getLoadCardList();
}
