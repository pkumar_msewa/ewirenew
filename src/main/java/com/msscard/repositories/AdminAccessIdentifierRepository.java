package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.AdminAccessIdentifier;
import com.msscard.entity.MUser;

public interface AdminAccessIdentifierRepository  extends CrudRepository<AdminAccessIdentifier, Long>, PagingAndSortingRepository<AdminAccessIdentifier, Long>, JpaSpecificationExecutor<AdminAccessIdentifier>  {
		
	@Query("select c from AdminAccessIdentifier c where c.user=?1")
	AdminAccessIdentifier getallaccess(MUser user);
}
