package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkRegister;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.MUser;
import com.msscard.entity.PartnerDetails;

public interface BulkRegisterRepository extends CrudRepository<BulkRegister, Long>,
		JpaSpecificationExecutor<BulkRegister>, PagingAndSortingRepository<BulkRegister, Long> {

	@Query("select b from BulkRegister b where b.agentDetails=?1")
	List<BulkRegister> getRegisteredUsers(CorporateAgentDetails corporateDetails);

	@Query("select b from BulkRegister b where b.agentDetails.id=?1 and DATE(b.created) BETWEEN ?2 AND ?3")
	List<BulkRegister> getRegisteredUsersByDate(Long id, Date fromDate, Date toDate);

	@Query("select b from BulkRegister b where b.partnerDetails=?3 and DATE(b.created) BETWEEN ?1 AND ?2")
	List<BulkRegister> getRegisteredUserPartner(Date fromDate, Date toDate, PartnerDetails agentDetails);

	@Query("select b from BulkRegister b where b.user=?1")
	BulkRegister getByUser(MUser user);

	@Query("select b from BulkRegister b where b.cardCreationStatus=0 and b.agentDetails=?1")
	List<BulkRegister> getFailedUserList(CorporateAgentDetails details);

	@Query("select b from BulkRegister b where b.user=?1")
	BulkRegister getUser(MUser user);

	@Query("select b from BulkRegister b where b.partnerDetails=?1")
	List<BulkRegister> getPartnerUsers(PartnerDetails details);

	@Query("select b from BulkRegister b where b.agentDetails=6")
	List<BulkRegister> getRegisteredUsers();

	@Query("SELECT b FROM BulkRegister b WHERE b.userCreationStatus = ?1 AND b.failedStatus = ?2")
	List<BulkRegister> getListOfUserForUserCreation(boolean b, boolean c);

	@Query("SELECT b FROM BulkRegister b WHERE b.userCreationStatus = ?1 AND b.walletCreationStatus = ?2 AND b.failedStatus = ?3")
	List<BulkRegister> getListOfUserForWalletCreation(boolean b, boolean c, boolean d);

	@Query("SELECT b FROM BulkRegister b WHERE b.userCreationStatus = ?1 AND b.walletCreationStatus = ?2 AND b.cardCreationStatus = ?3  AND b.failedStatus = ?4 AND b.proxyNo IS NOT NULL")
	List<BulkRegister> getListOfUserForPhysicalCardCreation(boolean b, boolean c, boolean d, boolean e);

}
