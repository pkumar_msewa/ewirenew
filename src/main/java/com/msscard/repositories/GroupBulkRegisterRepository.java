package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.GroupBulkRegister;

public interface GroupBulkRegisterRepository  extends CrudRepository<GroupBulkRegister,Long> ,PagingAndSortingRepository<GroupBulkRegister,Long>,JpaSpecificationExecutor<GroupBulkRegister>{

	@Query("SELECT g FROM GroupBulkRegister g WHERE g.successStatus =?1 AND g.groupDetail.email=?2")
	Page<GroupBulkRegister> getBulkRegisterFailList(boolean fail, String email, Pageable pageable);

	@Query("SELECT g FROM GroupBulkRegister g WHERE DATE(g.created) >= ?1 AND DATE(g.created) <= ?2 AND g.successStatus = ?3 AND g.groupDetail.email = ?4")
	Page<GroupBulkRegister> getBulkRegisterFailListPost(Pageable pageable, Date from, Date to, boolean fail,
			String email);

		@Query("SELECT g FROM GroupBulkRegister g WHERE g.successStatus = ?1 AND g.mobile = ?2 AND g.groupDetail.email = ?3")
	List<GroupBulkRegister> getsingleuserfail(boolean fail, String contact, String email);
}
