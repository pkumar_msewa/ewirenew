package com.msscard.repositories;


import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.MCommission;
import com.msscard.entity.MService;

public interface MCommissionRepository extends CrudRepository<MCommission, Long>, JpaSpecificationExecutor<MCommission> {

	@Query("select u from MCommission u where u.service=?1")
	MCommission findCommissionByService(MService service);
}
