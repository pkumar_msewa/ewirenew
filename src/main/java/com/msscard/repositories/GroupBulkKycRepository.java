package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.model.Status;

public interface GroupBulkKycRepository extends CrudRepository<GroupBulkKyc, Long>,
PagingAndSortingRepository<GroupBulkKyc, Long>, JpaSpecificationExecutor<GroupBulkKyc> {

	@Query("SELECT g FROM GroupBulkKyc g WHERE g.mobile = ?1 AND g.groupFileCatalogue = ?2 AND g.successStatus = ?3 AND g.status = ?4")
	GroupBulkKyc getUserRowByContact(String contact, GroupFileCatalogue groupFiletCatalogue, boolean status,
			Status state);

	@Query("SELECT g FROM GroupBulkKyc g WHERE g.successStatus = ?1 AND g.groupDetail.email = ?2")
	Page<GroupBulkKyc> getBulkKycFailList(Pageable pageable, boolean fail, String email);

	@Query("SELECT g FROM GroupBulkKyc g WHERE DATE(g.created) >= ?1 AND DATE(g.created) <= ?2 AND g.successStatus = ?3 AND g.groupDetail.email = ?4")
	Page<GroupBulkKyc> getBulkKycFailListPost(Pageable pageable, Date from, Date to, boolean fail, String email);

	@Query("SELECT g FROM GroupBulkKyc g WHERE g.successStatus = ?1 AND g.mobile = ?2 AND g.groupDetail.email = ?3")
	List<GroupBulkKyc> getsingleuserfail(boolean fail, String contact, String email);
	
}
