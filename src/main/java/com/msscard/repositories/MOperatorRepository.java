package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MOperator;

public interface MOperatorRepository extends CrudRepository<MOperator, Long>, JpaSpecificationExecutor<MOperator> {

	@Query("select u from MOperator u where u.name=?1")
	MOperator findOperatorByName(String name);


	
}
