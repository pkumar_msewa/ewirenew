package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BalanceReport;

public interface BalanceReportRepository extends CrudRepository<BalanceReport, Long>, PagingAndSortingRepository<BalanceReport, Long>, JpaSpecificationExecutor<BalanceReport> {

	
}
