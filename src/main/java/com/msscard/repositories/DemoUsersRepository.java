package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.DemoUsers;

public interface DemoUsersRepository extends CrudRepository<DemoUsers, Long>,
JpaSpecificationExecutor<DemoUsers>{

}
