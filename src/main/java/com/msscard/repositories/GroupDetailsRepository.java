package com.msscard.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.GroupDetails;

public interface GroupDetailsRepository extends CrudRepository<GroupDetails,Long> ,PagingAndSortingRepository<GroupDetails,Long>,JpaSpecificationExecutor<GroupDetails>{

	@Query("select g from GroupDetails g where g.contactNo=?1")
	Page<GroupDetails> getGroupDetails(Pageable pageable, String contact);
	
	@Query("select g from GroupDetails g where g.email=?1")
	GroupDetails getGroupDetailsByEmail(String email);
	
	@Query("Select g from GroupDetails g where g.contactNo=?1")
	GroupDetails getGroupDetailsByContact(String mobile);
	
	@Query("select g from GroupDetails g")
	Page<GroupDetails> getGroupName(Pageable pageable);
	
	@Query("select g from GroupDetails g where g.groupName!='None'")
	Page<GroupDetails> getGroupNameAdmin(Pageable pageable);
	
	@Query("select g from GroupDetails g where g.groupName=?1")
	GroupDetails getGroupDetailsByName(String name);
	
	@Query("select g from GroupDetails g where g.groupName!='None'")
	Page<GroupDetails> getGroupListGet(Pageable pageable);
	
	@Query("select g from GroupDetails g where DATE(g.created) >= ?1 AND DATE(g.created) <= ?2 and g.groupName!='None'")
	Page<GroupDetails> getGroupListPost(Pageable pageable, Date from, Date to);
	
	@Query("select g from GroupDetails g where g.contactNo=?1")
	Page<GroupDetails> getGroupListPost(Pageable pageable, String contactNo);
}
