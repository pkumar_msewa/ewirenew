package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.MKycDetail;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.model.UserType;

public interface MKycRepository extends CrudRepository<MKycDetail, Long>, PagingAndSortingRepository<MKycDetail, Long>,
		JpaSpecificationExecutor<MKycDetail> {

	@Query("select u from MKycDetail u where u.userType=?1")
	List<MKycDetail> findCorporateAgent(UserType corporate);

	@Query("select u from MKycDetail u where u.userType=?1 and u.accountType=?2 ORDER BY u.created DESC ")
	List<MKycDetail> findKycUser(UserType user, MPQAccountType accountType);

	@Query("select u from MKycDetail u where u.id=?1")
	MKycDetail findById(long id);

	@Query("select u from MKycDetail u where u.user=?1")
	MKycDetail findByUser(MUser user);

	@Query("select u from MKycDetail u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 and u.accountType=?3 and u.rejectionStatus=0 ORDER BY u.created DESC ")
	Page<MKycDetail> findAllKYCUserList(Pageable pageable, Date fromDate, Date toDate, MPQAccountType accountType);

	@Query("select u from MKycDetail u where u.user=?1 and u.accountType=?2")
	MKycDetail findByUserAndAccount(MUser us, MPQAccountType accountType);

	@Query("select u from MKycDetail u where u.user=?1 and u.accountType=?2 and u.rejectionStatus=?3")
	MKycDetail findByUserAndAccountUpdated(MUser us, MPQAccountType accountType, boolean status);

	@Query("select u from MKycDetail u where u.userType=?1 ORDER BY u.created DESC")
	List<MKycDetail> findByUserType(UserType userType);

	@Query("select u from MKycDetail u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 and u.accountType=?3 and u.rejectionStatus=1 ORDER BY u.created DESC ")
	Page<MKycDetail> findAllKYCUserListRejected(Pageable pageable, Date fromDate, Date toDate,
			MPQAccountType accountType);

	@Query("select u from MKycDetail u where u.userType='1' and u.accountType=?1 and u.rejectionStatus=0 ORDER BY u.created DESC ")
	Page<MKycDetail> findAllKYCUserList(Pageable pageable, MPQAccountType accountType);

	@Query("select u from MKycDetail u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 and u.rejectionStatus=?3 and u.user.accountDetail.accountType.code='NONKYC'")
	Page<MKycDetail> findUpdatedUserList(Pageable pageable, Date fromDate, Date toDate, boolean status);

	@Query("select u from MKycDetail u where u.rejectionStatus=?1")
	List<MKycDetail> findAllKyc(boolean status);

	@Query("SELECT u from MKycDetail u where u.user.id in (?1)")
	List<MKycDetail> getListByMobile(List<Long> userList);
}
