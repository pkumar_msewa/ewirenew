package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.OtpMobile;

public interface OtpRepository extends CrudRepository<OtpMobile, String>{
	
	@Query("select c.mobile from OtpMobile c")
	List<String> getMobileNumbers();
}
