package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.CorporateFileCatalogue;

public interface CorporateFileCatalogueRepository
		extends CrudRepository<CorporateFileCatalogue, Long>, JpaSpecificationExecutor<CorporateFileCatalogue> {

	@Query("select c from CorporateFileCatalogue c ORDER BY c.id desc")
	List<CorporateFileCatalogue> getUnapprovedList();

	@Query("select c from CorporateFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKREGISTER' and c.schedulerStatus=0")
	List<CorporateFileCatalogue> getListOfBulkRegister();

	@Query("select c from CorporateFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKCARDLOAD' and c.schedulerStatus=0")
	List<CorporateFileCatalogue> getListOfBulkFundTransfer();

	@Query("select c from CorporateFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKKYCUPLOAD' and c.schedulerStatus=0")
	List<CorporateFileCatalogue> getListOfBulkKYCTransfer();
	
	@Query("select c from CorporateFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKMINKYCUPLOAD' and c.schedulerStatus=0")
	List<CorporateFileCatalogue> getListOfBulkminKYCTransfer();
}
