package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.imps.entity.AddBeneficiary;
import com.msscard.entity.MUser;


public interface AddBeneficiaryRepository extends CrudRepository<AddBeneficiary, Long>, PagingAndSortingRepository<AddBeneficiary, Long>, JpaSpecificationExecutor<AddBeneficiary>
{

	@Query("select b from AddBeneficiary b where b.accountNo=?1 AND b.user=?2")
	AddBeneficiary findByAccountNoAndUser(String accountNo,MUser user);

	@Query("select b from AddBeneficiary b where b.user=?1")
	List<AddBeneficiary> findByUser(MUser user);

}
