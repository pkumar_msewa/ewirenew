package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MServiceType;

public interface MServiceTypeRepository extends
CrudRepository<MServiceType, Long>,
JpaSpecificationExecutor<MServiceType> {

@Query("select u from MServiceType u where u.name=?1")
MServiceType findServiceTypeByName(String name);


	
}
