package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.MAgentAssignPhysicalCard;
import com.msscard.entity.MUser;


public interface MAgentAssignPhysicalCardRepository extends CrudRepository<MAgentAssignPhysicalCard,Long>, 
	JpaSpecificationExecutor<MAgentAssignPhysicalCard> , PagingAndSortingRepository<MAgentAssignPhysicalCard,Long> {
	
	@Query("select p from MAgentAssignPhysicalCard p where p.agent=?1 ORDER BY p.created DESC")
	List<MAgentAssignPhysicalCard> getPhysicalCardAssignList(MUser agent);
	
	@Query("select count(p) from MAgentAssignPhysicalCard p where p.agent=?1")
	long getTotalPhysicalCardCount(MUser agent);
	
	@Query("select p from MAgentAssignPhysicalCard p where p.agent=?1 AND p.created BETWEEN ?2 AND ?3")
	List<MAgentAssignPhysicalCard> getPhysicalCardAssignListBasedOnDate(MUser agent,Date startDate,Date endDate);

}
