package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.Banks;

public interface BankRepository extends CrudRepository<Banks,Long>, JpaSpecificationExecutor<Banks> ,
PagingAndSortingRepository<Banks,Long> {
	
	@Query("select b.name from Banks b")
	List<String> getBankName();
	
	@Query("select b from Banks b where b.name=?1")
	Banks getBankDetailsBasedOnName(String name);

}
