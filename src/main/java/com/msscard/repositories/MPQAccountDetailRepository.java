package com.msscard.repositories;

import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;

public interface MPQAccountDetailRepository extends CrudRepository<MPQAccountDetails, Long>,PagingAndSortingRepository<MPQAccountDetails, Long>, JpaSpecificationExecutor<MPQAccountDetails> {

@Query("select u from MPQAccountDetails u")
List<MPQAccountDetails> findAll();

/*@Query("select u from MPQAccountDetails u where u.vBankAccount=?1")
MPQAccountDetails findUsingVBankAccount(VBankAccountDetail vBankAccountDetail);
*/
@Query("select u from MPQAccountDetails u where u.id=?1")
MPQAccountDetails findAccount(long id);

@Query("select SUM(u.balance) from MPQAccountDetails u")
double getTotalBalance();

@Query("select u.balance from MPQAccountDetails u where u.id = ?1")
double getUserWalletBalance(long id);

@Modifying
@Transactional
@Query("update MPQAccountDetails c set c.balance=?1 where c.id =?2")
int updateUserBalance(double balance, long accountDetailId);

@Query("select u from MPQAccountDetails u")
Page<MPQAccountDetails> findAll(Pageable page);


@Modifying
@Transactional
@Query("update MPQAccountDetails c set c.points=?1 where c.id =?2")
int updateUserPoints(long points, long id);


@Modifying
@Transactional
@Query("update MPQAccountDetails c set c.balance=?1 where c.id =?2")
int updateUserBalance(String authority, long id);

@Modifying
@Transactional
@Query("update MPQAccountDetails a set a.points= a.points+?1 where a.accountNumber=?2")
int addUserPoints(long points, long accountNumber);

@Query("select u.balance from MPQAccountDetails u where u.id=?1")
double getBalanceByUserAccount(long accountDetailId);

@Query("select u.balance from MPQAccountDetails u where DATE(u.created)<=?1 and (accountType=?2 or accountType=?3)")
List<MPQAccountDetails> getAllAccountTillDate(Date date,MPQAccountType accType,MPQAccountTypeRepository accountType);

@Query("select COUNT(u) from MPQAccountDetails u where DATE(u.created)=?1 and (accountType=?2 or accountType=?3)")
long getTotalCreated(Date date,MPQAccountType accType,MPQAccountType accountType);




}
