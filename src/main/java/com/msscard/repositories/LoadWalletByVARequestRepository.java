package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.LoadWalletByVARequest;
import com.msscard.model.Status;

public interface LoadWalletByVARequestRepository extends CrudRepository<LoadWalletByVARequest, Long>,
		PagingAndSortingRepository<LoadWalletByVARequest, Long>, JpaSpecificationExecutor<LoadWalletByVARequest> {

	@Query("SELECT lw FROM LoadWalletByVARequest lw WHERE lw.status = ?1 AND lw.failedStatus = ?2")
	List<LoadWalletByVARequest> getPendingList(Status status, boolean b);

}