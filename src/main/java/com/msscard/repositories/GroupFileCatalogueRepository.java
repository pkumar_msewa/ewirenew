package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.MUser;

public interface GroupFileCatalogueRepository extends CrudRepository<GroupFileCatalogue, Long>,JpaSpecificationExecutor<GroupFileCatalogue>{
	
	@Query("select c from GroupFileCatalogue c")
	List<GroupFileCatalogue> getUnapprovedList();
	
	@Query("select c from GroupFileCatalogue c where c.categoryType='BLKREGISTER'")
	List<GroupFileCatalogue> getUnapprovedListGroup();
	
	@Query("select c from GroupFileCatalogue c where c.categoryType='BLKSMS'")
	List<GroupFileCatalogue> getUnapprovedListSms();
	
	@Query("select c from GroupFileCatalogue c where c.categoryType='BLKSMS' and c.user=?1 and c.reviewStatus=?2")
	List<GroupFileCatalogue> getUnapprovedListSmsOFGroup(MUser user , boolean status);
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKREGISTER' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkRegister();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKSMS' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkSms();
	
	@Query("select c from GroupFileCatalogue c where c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKCARDLOAD' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkFundTransfer();

	@Query("SELECT c FROM GroupFileCatalogue c WHERE c.reviewStatus=1 and c.fileRejectionStatus=0 and c.categoryType='BLKKYCUPLOAD' and c.schedulerStatus=0")
	List<GroupFileCatalogue> getListOfBulkKyc();

	@Query("SELECT c FROM GroupFileCatalogue c WHERE c.reviewStatus=1 AND c.fileRejectionStatus=0 AND c.categoryType='BLKCARDASSIGNGROUP' AND c.schedulerStatus=0")
	List<GroupFileCatalogue> getBulkCardAssignmentGroup();
}
