package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.model.Status;

public interface PhysicalCardDetailRepository
		extends CrudRepository<PhysicalCardDetails, Long>, JpaSpecificationExecutor<PhysicalCardDetails> {

	@Query("select w from PhysicalCardDetails w where w.wallet=?1")
	PhysicalCardDetails findByWallet(MatchMoveWallet matchMoveWallet);

	@Query("select w from PhysicalCardDetails w where w.cards=?1")
	PhysicalCardDetails findByCard(MMCards cards);

	@Query("select w from PhysicalCardDetails w where w.status=?1")
	List<PhysicalCardDetails> findPhysicalCardRequest(Status requested);

	@Query("select w from PhysicalCardDetails w where w.proxyNumber=?1")
	PhysicalCardDetails findCardByProxyNo(String proxy_number);

	@Query("select w from PhysicalCardDetails w where w.wallet.user=?1")
	PhysicalCardDetails findByUser(MUser user);

	@Query("select w from PhysicalCardDetails w order by w.created asc and w.status='Requested'")
	List<PhysicalCardDetails> findPhysicalCardRequest();

	@Query("select w from PhysicalCardDetails w where w.status=?1")
	List<PhysicalCardDetails> findPhysicalCardRequestList(Status requested);

	@Query("select w from PhysicalCardDetails w")
	List<PhysicalCardDetails> findAllPhysicalCardRequest(Pageable pageable);

	@Query("select w from PhysicalCardDetails w where w.id=?1")
	PhysicalCardDetails findById(long logId);

	@Query("select w from PhysicalCardDetails w where DATE(w.created) BETWEEN ?1 AND ?2")
	Page<PhysicalCardDetails> findAllPhysicalCardRequestUserList(Pageable pageable, Date fromDate, Date toDate);

	@Query("select w from PhysicalCardDetails w where DATE(w.created) BETWEEN ?1 AND ?2 and w.status=?3")
	Page<PhysicalCardDetails> findPhysicalCardRequestList(Pageable pageable, Date fromDate, Date toDate,
			Status requested);
	
	@Query("select count(w) from PhysicalCardDetails w where w.status=?1 and DATE(w.created) BETWEEN ?2 AND ?3")
	Long getCountPhysicalCardRequestPending(Status status,Date from, Date to);
	
	@Query("select count(w) from PhysicalCardDetails w where w.status=?1 and DATE(w.created) BETWEEN ?2 AND ?3")
	Long getCountPhysicalCardActive(Status status,Date from, Date to);
	
	@Query("select count(w) from PhysicalCardDetails w where w.status='Requested'")
	Long getCountPhysicalCardRequestPending();


}
