package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkLoadMoney;

public interface BulkLoadMoneyRepository extends CrudRepository<BulkLoadMoney, Long>,
		JpaSpecificationExecutor<BulkLoadMoney>, PagingAndSortingRepository<BulkLoadMoney, Long> {

	@Query("SELECT b FROM BulkLoadMoney b WHERE b.walletLoadStatus = ?1 AND b.failedStatus = ?2 AND b.cardLoadStatus = ?3")
	List<BulkLoadMoney> loadWalletAndCard(boolean b, boolean c, boolean d);

}