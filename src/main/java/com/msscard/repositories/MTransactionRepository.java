package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.msscard.entity.MOperator;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.entity.MTransaction;
import com.msscard.model.Status;
import com.msscard.model.TransactionType;

public interface MTransactionRepository extends CrudRepository<MTransaction, Long>,
PagingAndSortingRepository<MTransaction, Long>, JpaSpecificationExecutor<MTransaction> {
	
/*@Query("select u from MTransaction u where u.transactionType=?1 and u.account NOT IN (?2) ORDER BY u.created DESC")
Page<MTransaction> findTodaysTransactions(Pageable page,TransactionType transactionType,List<AccountDetail> accounts);
*/

@Query("select u from MTransaction u where u.service in (?1) and u.created BETWEEN ?2 AND ?3  and u.transactionType=?4 and u.status=?5  ORDER BY u.created DESC")
List<MTransaction> getTransactionBetween(List<MService> services,Date startDate,Date endDate,TransactionType transactionType,Status status);

@Query("select u from MTransaction u where  u.created BETWEEN ?1 AND ?2  and u.transactionType=?3 and u.transactionRefNo=?4 and status='Success' ORDER BY u.created DESC")
List<MTransaction> getTransactionBetweenDateAndRefNo(Date startDate,Date endDate,TransactionType transactionType,String transactionRefNo);

@Query("select u from MTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2")
Page<MTransaction> findMerchantTransactions(Pageable page,MService service,TransactionType transactionType);

@Query("select u from MTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 and debit=false")
Page<MTransaction> findSuccessMerchantTransactions(Pageable page,MService service,TransactionType transactionType);

@Query("select u from MTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 and  u.created BETWEEN ?3 AND ?4")
Page<MTransaction> findMerchantTransactionsFiltered(Pageable page,MService service,TransactionType transactionType,Date startDate,Date endDate);

@Query("select sum(u.amount) from MTransaction u where u.transactionType=?1 and u.account = ?2 and u.status='Success' and u.debit=?3")
double  calculateTotalCrDb(TransactionType transactionType,MPQAccountDetails account,boolean debit);

@Query("select u from MTransaction u where u.transactionType=?1  and u.created BETWEEN ?3 AND ?4 and u.account NOT IN  (?2) ORDER BY u.created DESC")
List<MTransaction> getTransactionBetween(TransactionType transactionType,List<MPQAccountDetails> accounts,Date startDate,Date endDate);

@Query("select u from MTransaction u where u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4)")
List<MTransaction> getTransactionBetween(TransactionType transactionType,Date startDate,Date endDate,List<MService> service);

@Query("select u from MTransaction u where u.debit=true AND u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4)")
List<MTransaction> getTransactionBetweenTopup(TransactionType transactionType,Date startDate,Date endDate,List<MService> service);

@Query("select u from MTransaction u where u.transactionRefNo=?1")
MTransaction findByTransactionRefNo(String transactionRefNo);

@Query("select u from MTransaction u where u.retrivalReferenceNo=?1")
MTransaction findByTransactionRetivalReferenceNo(String retrivalReferenceNo);

@Query("select u from MTransaction u where u.retrivalReferenceNo=?1")
List<MTransaction> findListByTransactionRetivalReferenceNo(String retrivalReferenceNo);


/*@Query("select u from TPTransaction u where u.transactionRefNo=?1")
TPTransaction findTransactionRefNo(String transactionRefNo);
*/

@Query("select u from MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.status=?3 and u.service=?4")
List<MTransaction> findByTranRefNoAndStatus(Date date,Date to,Status Status,MService service);

@Query("select u from MTransaction u where u.transactionRefNo=?1 and u.status=?2")
MTransaction findByTransactionRefNoAndStatus(String transactionRefNo, Status status);

@Query("select COUNT(u) from MTransaction u where u.status=?1 AND u.transactionType = ?2")
Long countTotalTransactionsByStatus(Status status,TransactionType transactionType);

@Query("select COUNT(u) from MTransaction u where DATE(u.created) = ?1 AND status = ?2 AND u.transactionType = ?3")
Long getDailyTransactionCount(Date date,Status status,TransactionType transactionType);

@Query("select SUM(u) from MTransaction u where DATE(u.created) = ?1 AND status = ?2 AND u.transactionType = ?3")
Double getDailyTransactionAmount(Date date,Status status,TransactionType transactionType);

@Modifying
@Transactional
@Query("update MTransaction c set c.status=?1, c.currentBalance=?2 where c.transactionRefNo =?3")
int updateTransaction(Status status, double currentBalance, String transactionRefNo);

@Modifying
@Transactional
@Query("update MTransaction c set c.favourite=?2 where c.transactionRefNo =?1")
int updateFavouriteTransaction(String transactionRefNo,boolean favourite);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 ")
List<MTransaction> getMonthlyTransaction(int year, int month, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.service IN ( ?1 , ?2 ) AND u.status = ?3")
List<MTransaction> getLoadMoneyTransactions(MService ebs,MService vnet,Status status);

@Query("SELECT u FROM MTransaction u where u.transactionType = ?1")
List<MTransaction> getTotalDefaultTransactions(TransactionType transactionType);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.debit=?1 AND u.status=?2 ")
Double getValidAmount(boolean debit,Status status);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND u.transactionType IN(0,4) ")
Double getValidAmountByService(MService service);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service IN(select p.id from MService p where p.serviceType=?1) AND u.status='Success' AND u.debit=true AND u.transactionType=0 ")
Double getValidAmountByTopUpService(MServiceType serviceType);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND u.transactionType=0")
Double getValidAmountByBankService(MService service);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND transactionType=2 ")
Double getValidAmountByBankServiceCommission(MService service);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
Double getValidAmountByServiceForDate(MService service,Date date);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
Double getValidTopupAmountForDate(MPQAccountDetails account,Date date);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4")
List<MTransaction> getDailyTransaction(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.service=?1")
List<MTransaction> findTransactionByService(MService service);

@Query("SELECT u FROM MTransaction u where u.created BETWEEN ?1 AND ?2  AND u.debit=?3 AND u.transactionType=?4 AND u.status=?5  and u.service IN(?6) ORDER BY u.created DESC")
List<MTransaction> findTransactionByServiceAndDate(Date from, Date to, boolean debit, TransactionType transactionType, Status status, List<MService> services);

@Query("SELECT u FROM MTransaction u where u.created BETWEEN ?1 AND ?2  AND u.debit=?3 AND u.transactionType=?4 AND u.status=?5 and u.account=71")
List<MTransaction> findCommissionTransactionByServiceAndDate(Date from, Date to, boolean debit, TransactionType transactionType, Status status);

@Query("SELECT u FROM MTransaction u where u.service=?1 AND u.debit=?2")
List<MTransaction> findTransactionByServiceAndDebit(MService service,boolean debit);

@Query("SELECT u FROM MTransaction u where u.service=?1 AND u.status=?2")
List<MTransaction> findTransactionByServiceAndStatus(MService service, Status status);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status = 'Success'")
Double getMonthlyTransactionTotal(int year, int month, MPQAccountDetails account);

@Query("SELECT COUNT(u) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.service=5 AND u.transactionType=0 AND u.debit=true AND u.status='Success' ")
long getMonthlyTransactionTotalCountBank(int year, int month,MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.debit = ?4 AND u.status = 'Success'")
Double getMonthlyTotalByCreditOrDebit(int year, int month, MPQAccountDetails account,boolean debit);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.service = ?4 AND u.status = 'Success'")
Double getMonthlyTransactionTotalByServiceType(int year, int month, MPQAccountDetails account, MService service);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status = 'Success'")
Double getDailyTransactionTotal(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?4 AND u.status = 'Success'")
Double getDailyTotalByCreditOrDebit(int year, int month, int day, MPQAccountDetails account,boolean debit);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?5")
Double getDailyDebitTransactionTotal(int year, int month, int day, MPQAccountDetails account, boolean debit);

@Query("SELECT u FROM MTransaction u where  MONTH(created) = ?1")
List<MTransaction> getMonthlyTransaction(int month);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
List<MTransaction> getDailyTransactionBetween(Date from, Date to);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
List<MTransaction> getTransactionBetween(Date from, Date to);


@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3")
List<MTransaction> getDailyTransactionBetweenForAccount(Date from, Date to, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where DATE(u.created) = ?1")
List<MTransaction> getDailyTransactionByDate(Date from);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) = ?1")
Long countDailyTransactionByDate(Date from);

@Query("SELECT SUM(u.amount) FROM MTransaction u where DATE(u.created) = ?1")
Double countDailyTransactionAmountByDate(Date from);

@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1")
Date getTimeStampOfLastTransaction(MPQAccountDetails account);

@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1 AND t.status=?2")
Date getTimeStampOfLastTransactionByStatus(MPQAccountDetails account,Status status);


@Query("SELECT u FROM MTransaction u where DATE(u.created) = ?1 AND u.account=?2")
List<MTransaction> getDailyTransactionByDateForAccount(Date from, MPQAccountDetails account);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) = ?1 AND u.account=?2")
Long countDailyTransactionByDateForAccount(Date from, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.account= ?1 order by u.created")
List<MTransaction> getTotalTransactions(MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.account= ?1 AND u.status='Reversed' ORDER BY u.id DESC ")
List<MTransaction> getReverseTransactionsOfUser(MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.account= ?1 AND u.status='Reversed' AND u.created=(select MAX(f.created) from MTransaction f where f.account=?1)")
MTransaction getLastReverseTransactionsOfUser(MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u")
List<MTransaction> getTotalTransactions();

@Query("select u from MTransaction u")
Page<MTransaction> findAll(Pageable page);


@Query("select u from MTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2")
Page<MTransaction> findAllByFiltered(Pageable page,Date from,Date to);

@Query("select u from MTransaction u where u.account = ?1")
Page<MTransaction> findAllByAccount(Pageable page,MPQAccountDetails account);

@Query("select u from MTransaction u where u.account=?1")
Page<MTransaction> findAllTransactionByUser(Pageable page, MPQAccountDetails account);


@Query("select u from MTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2 and u.account=?3")
Page<MTransaction> findAllTransactionByUserFilter(Pageable page,Date StartDate,Date endDate, MPQAccountDetails account);

@Query("select u from MTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2 and u.account=?3")
Page<MTransaction> findAllTransactionByUserFilterPagewise(Pageable page,Date StartDate,Date endDate,MPQAccountDetails account);

@Query("select u from MTransaction u where u.account=?1")
List<MTransaction> findAllTransactionByMerchant(MPQAccountDetails account);

@Query("select u from MTransaction u where u.service=?1 and debit=true and u.transactionType=0")
Page<MTransaction> findAllTransactionByMerchantService(MService service ,Pageable page);


@Query("select u from MTransaction u where u.account=?1 AND u.status=?2 AND u.transactionRefNo LIKE '%D' ")
List<MTransaction> findAllTransactionByUserAndStatus(MPQAccountDetails account,Status status);

@Transactional
@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3 AND u.amount=?4")
List<MTransaction> findTransactionByAccount(Date from, Date to, MPQAccountDetails account, double amount);

@Transactional
@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3 AND u.amount>=?4")
List<MTransaction> findTransactionByAccountAndAmount(Date from, Date to, MPQAccountDetails account, double amount);

@Query("SELECT u FROM MTransaction u where u.account=?1")
List<MTransaction> findTransactionByAccount(MPQAccountDetails account);

//@Transactional
//@Query("SELECT t FROM MTransaction t LEFT JOIN t.service s WHERE s.code=?1 AND t.account=?2")
//List<MTransaction> findTransactionDateByAccountAndService(String serviceType, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?5")
List<MTransaction> getDailyDebitTransaction(int year, int month, int day, MPQAccountDetails account, boolean debit);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true")
List<MTransaction> getMonthlyDebitTransaction(int year, int month, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=true OR u.debit=false")
List<MTransaction> getDailyCreditAndDebitTransation(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true OR u.debit=false")
List<MTransaction> getMonthlyCreditAndDebitTransation(int year, int month, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=false")
List<MTransaction> getDailyCreditTransation(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=false")
List<MTransaction> getMonthlyCreditTransation(int year, int month, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit=true")
Double getDailyDebitTransactionTotalAmount(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit=true AND u.transactionType=0 AND u.service=5 AND u.status='Success'")
Double getDailyDebitTransactionTotalAmountBank(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true")
Double getMonthlyDebitTransactionTotalAmount(int year, int month, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true AND u.transactionType=0 AND u.service=5")
Double getMonthlyDebitTransactionTotalAmountBank(int year, int month, MPQAccountDetails account);
@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=true OR u.debit=false")
Double getDailyCreditAndDebitTransationTotalAmount(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true OR u.debit=false")
Double getMonthlyCreditAndDebitTransationTotalAmount(int year, int month, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=false")
Double getDailyCreditTransationTotalAmount(int year, int month, int day, MPQAccountDetails account);

@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=false")
Double getMonthlyCreditTransationTotalAmount(int year, int month,MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.account= ?1 AND u.status in('Success','Booked') order by u.created asc")
List<MTransaction> getTotalSuccessTransactions(MPQAccountDetails account);
//
@Query("select t from MTransaction t where t.service=?1 AND (t.transactionRefNo LIKE '%C' OR t.transactionRefNo LIKE '%D')")
List<MTransaction> findAllTransactionForMerchant(MService service);

@Query("select t from MTransaction t where t.created BETWEEN ?1 AND ?2 AND transactionType=0 and account_id!=3 and account_id!=72)")
List<MTransaction> findTransactionsByDate(Date from,Date to);

@Query("select t from MTransaction t where t.service=162 AND t.transactionType=0 AND t.account!=125119 ORDER BY t.created desc")
List<MTransaction> findVisaMerchantTransactions();

@Query("select t from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.service=162 AND t.transactionType=0 AND t.account!=125119 ORDER BY t.created desc")
List<MTransaction> findVisaMerchantTransactionsFilter(Date from,Date to);

// Agent

@Query("select u from MService u where u.code=?1")
MService findBypqservicetype(String pqservicecode);


@Query("select t from MTransaction t where t.service=?1 AND status='Initiated'")
List<MTransaction> getAgentRequestLoadMoney(MService service);

@Query("select t from MTransaction t where t.service=135 AND t.transactionType=0 AND t.transactionRefNo=?1")
MTransaction getStatusByTransactionRefNo(String transactionRefNo);


@Query("SELECT SUM(u.amount) FROM MTransaction u where u.account = ?1 AND u.debit=true")
Double getAllTransactionDebitByAgent(MPQAccountDetails account);

@Query("select count(t),sum(t.amount),t.service from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 and t.debit=true and t.status='Success' GROUP BY t.service")
List<Object[]> getAllTransactionsByService(Date from,Date to);

@Query("select count(t),sum(t.amount),t.service from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3 and t.service NOT IN(?4)   AND t.transactionType=0 and t.debit=true and t.status='Success' GROUP BY t.service")
List<Object[]> getAllTransactionsByServiceOfUser(Date from,Date to,MPQAccountDetails account,List<MService> service);

@Query("select count(t),sum(t.amount),t.service from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3 and t.service IN(?4)   AND t.transactionType=0 and t.debit=true and t.status='Success'")
List<Object[]> getAllTopUpByServiceOfUser(Date from,Date to,MPQAccountDetails account,List<MService> service);

@Query("select count(t),sum(t.amount),t.service from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3  AND t.transactionType IN(0,4) AND t.status='Success' AND t.debit=false GROUP BY t.service")
List<Object[]> getCreditTransactionsOfUser(Date from,Date to,MPQAccountDetails account);

@Query("select t from MTransaction t where t.created between ?1 and ?2  AND status='Success' AND t.account=?3 order by t.created asc")
List<MTransaction> getUserTransaction(Date from,Date to,MPQAccountDetails account);

@Query("select  SUM(t.amount) from MTransaction t where t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?1 AND t.debit=true")
Double getFilteredUserDebitTransaction(MPQAccountDetails account);

@Query("select SUM(t.amount) from MTransaction t where t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?1 AND t.debit=false")
Double getFilteredUserCreditTransaction(MPQAccountDetails account);

//API3
@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.debit=true")
Double getTotalDebit(Date to,Date from,MPQAccountDetails account);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.debit=false")
Double getTotalcredit(Date to,Date from,MPQAccountDetails account);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.service=?4 AND t.debit=false")
Double getLoadMoneyCredit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.service=?4 AND t.debit=false")
double getSendMoneyCredit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
Double getSendMoneydebit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
Double getTopupAndBillPaymentDebit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
Double getBankTransferdebit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 ")
Double getServiceDebit(Date to,Date from,MPQAccountDetails account,MService service);

@Query("select t from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Success' AND t.service=?3")
List<MTransaction> getUserListAnalytics(Date from,Date to,MService service);

/*	@Query("select t from MTransaction t LEFT JOIN t.MPQAccountDetails q ON t.account=q.id LEFT JOIN User u ON t.account_id=u.accountDetail ")
*/	
@Query("select count(t),sum(t.amount),t.service from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Success' AND t.service IN(112,1,2) AND t.debit=false GROUP BY t.service")
List<Object[]> getCreditTransactions(Date from,Date to);


@Query("select SUM(t.amount) from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service NOT IN (4,5,171,162,163,201,211,231,241,421,531,135) AND t.debit=true")
Double getTopupAndBillPayment(Date to,Date from,MPQAccountDetails account);

//for sending sms for transaction reversed(topups and billpayment)

@Query("select t from MTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Reversed'")
List<MTransaction> getInitiatedTransaction(Date toDate,Date fromDate);

@Query("select u from MTransaction u where u.status='Success' and u.account=?1 and u.transactionType=0 and u.currentBalance >=0 and u.created BETWEEN ?2 AND ?3")
List<MTransaction> findMonthlyUserTransaction(MPQAccountDetails account,Date startDate,Date endDate);

@Query("select sum(u.amount) from MTransaction u where u.transactionType IN(0) AND u.status='Success' AND u.debit=false AND u.account=?1 and u.created BETWEEN ?2 AND ?3 ")
Double findAllCreditBalance(MPQAccountDetails detail,Date startDate,Date endDate);

@Query("select sum(u.amount) from MTransaction u where u.transactionType=0 AND u.status='Success' AND u.debit=true AND u.account=?1 and u.created BETWEEN ?2 AND ?3 ")
Double findAllDebitBalance(MPQAccountDetails detail,Date startDate,Date endDate);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
Long getDailyTransactionCountBetween(Date from, Date to);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3")
Long getDailyTransactionCountBetweenForAccount(Date from, Date to, MPQAccountDetails account);

@Query("select t from MTransaction t where t.service=?1 AND t.account=?2")
List<MTransaction> findByService(MService service,MPQAccountDetails accountDetail);

@Query("select t from MTransaction t where  t.account=?1 and t.service IN (select p.id from MService p where p.serviceType=6) and t.status='Success' and t.debit=true and t.transactionType=0 and timestampdiff(day,date(t.created),current_date) between 25 and 28")
List<MTransaction> findAllBillPaymentTransaction(MPQAccountDetails accountDetail);

@Query("select t from MTransaction t where t.service=?1 AND t.transactionType=0 order by t.created desc")
List<MTransaction> findByServiceFlight(MService service);

@Query("select u from MTransaction u where u.service=?1  and u.transactionType=2")
Page<MTransaction> findAllCommissionByMerchantServicePage(MService service,Pageable pageable);

@Query("select u from MTransaction u where u.service=?1  and u.transactionType=2 order by u.created desc")
List<MTransaction> findAllCommissionByMerchantService(MService service);

//@Query("select t from MTransaction t where  AND t.account=?1 and t.service IN (select p.id from MService p where p.serviceType=6) and t.status='Success' and t.debit=true and t.transactionType=0 and timestampdiff(day,date(t.created),t.date=?1) between  25 and 28")
//List<MTransaction> findAllBillPaymentTransaction(MPQAccountDetails accountDetail,Date date);

/*Graph*/

@Query("select count(t) from MTransaction t where t.created BETWEEN ?1 AND ?2  AND status='Success'")
long findCountByMonth(Date parse, Date parse2);

@Query("select count(t) from MTransaction t where t.created BETWEEN ?1 AND ?2 AND t.account=?3 AND status='Success'")
long findCountByMonthUser(Date parse, Date parse2,MPQAccountDetails account);

/*Admin Panel Report*/

@Query("SELECT DATE(t.created) FROM MTransaction t where t.transactionType='0' AND t.status='Success'")
List<Date> getBeginDateOfAllTxns();

@Query("SELECT distinct u.account FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.status='Success'")
List<MPQAccountDetails> getTotalAccountByFromAndToDate(Date from, Date to);


@Query("SELECT u FROM MTransaction u where u.status='Success' AND u.account =?1 AND DATE(u.created) BETWEEN ?2 AND ?3")
List<MTransaction> getTotalTxnsByStatusAndAccountId(MPQAccountDetails account, Date from, Date to);

/*Admin Panel Report*/

@Query("SELECT distinct u.account FROM MTransaction u where DATE(u.created) =?1 AND u.status='Success' order by u.created desc")
List<MPQAccountDetails> getTotalAccountByFromDate(Date from);

@Query("SELECT u FROM MTransaction u where u.status='Success' AND u.account =?1")
List<MTransaction> getTotalTxnByStatusAndAccountId(MPQAccountDetails account);

@Query("SELECT u FROM MTransaction u where u.account =?1")
List<MTransaction> getTotalTxnByAccount(MPQAccountDetails account);

@Query("SELECT DATE(t.created) FROM MTransaction t where t.debit=false AND t.status='Success'")
List<Date> getLastDateOfLoadMoney();

@Query("SELECT distinct u.account FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.status='Success'")
List<MPQAccountDetails> getTotalTxnsAccount(Date from, Date to);

@Query("SELECT SUM(u.amount) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
double getTotalSMUCreditByDate(Date date, MService smu);

@Query("SELECT SUM(u.amount) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
double getTotalSMRCreditByDate(Date date, MService smr);

@Query("SELECT SUM(u.amount) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
double getTotalSMRDebitByDate(Date date, MService smr);

@Query("SELECT SUM(u.amount) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
double getTotalSMUDebitByDate(Date date, MService smu);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
long getCountSMUCreditByDate(Date date, MService smu);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
long getCountSMRCreditByDate(Date date, MService smr);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
long getCountSMUDebitByDate(Date date, MService smu);

@Query("SELECT count(u) FROM MTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
long getCountSMRDebitByDate(Date date,MService smr);


@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
double getValidAmountByService(MService service, Date date);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false")
double getValidAmountByServiceForDate(MService service);

@Query("SELECT COUNT(u) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
Long getValidCountByServiceForDate(MService service,Date date);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=true AND DATE(u.created)=?2")
double getValidDebitAmountByService(MService service, Date date);

@Query("SELECT COUNT(u) FROM MTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
Long getCountValidTopupAmountForDate(MPQAccountDetails account,Date date);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
double getTotalBankAmountForDate(MPQAccountDetails account, Date date);

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.status='Success' AND u.debit=false AND DATE(u.created)=?1 AND u.service = ?2")
double getTotalAmountPromoCodes(Date date, MService service);

/*@Query("SELECT SUM(u.amount) FROM MTransaction u where u.status='Success' AND u.refundStatus=true AND DATE(u.created)=?1")
double getTotalAmountMerchantRefunds(Date date);*/

@Query("SELECT COUNT(u) FROM MTransaction u where u.status='Success' AND u.debit=false AND DATE(u.created)=?1 AND u.service = ?2")
Long getCountPromoCodes(Date date, MService service);

/*@Query("SELECT COUNT(u) FROM MTransaction u where u.status='Success' AND u.refundStatus=true AND DATE(u.created)=?1")
Long getCountMerchantRefunds(Date date);*/

@Query("SELECT SUM(u.amount) FROM MTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
double getTotalMerchatAmountForDate(MPQAccountDetails account, Date date);

@Query("SELECT count(*) FROM MTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
long getCountMerchatAmountForDate(MPQAccountDetails account, Date date);

@Query("SELECT u FROM MTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)>=?2")
List<MTransaction> getValidAmountByServiceForMIS(MService service,Date from);

@Transactional
@Query("SELECT t FROM MTransaction t LEFT JOIN t.service s WHERE DATE(t.created)>=?1 and DATE(t.created)<=?2 and t.status='Success' and (s.code=?3 or s.code=?4)")
List<MTransaction> getallLoadMoney(Date date,Date date2,String service1,String service2);

@Query("select sum(amount) from MTransaction t where t.debit=1 and t.status='Success' and t.transactionType=0")
Double getTotalDebitTransaction();

@Query("select t from MTransaction t where t.authReferenceNo=?1")
MTransaction getTransactionByAuthRefNo(String authRefNo);

@Query("select count(t) from MTransaction t where t.transactionRefNo like'%D' and t.status=?1 and t.account IN (?2) ")
long getTransactionCountForAdmin(Status success, List<MPQAccountDetails> list);

@Query("select sum(t.amount) from MTransaction t where t.transactionRefNo like'%D' and t.status=?1 and t.account IN (?2) ")
long getTransactionAmountForAdmin(Status success, List<MPQAccountDetails> findAllAccountByUser);

@Query("select sum(t.amount) from MTransaction t where t.transactionRefNo like'%C' and t.status=?1 and t.account IN (?2) ")
Double getCreditTransactionAmountForAdmin(Status success, List<MPQAccountDetails> findAllAccountByUser);



@Query("select t from MTransaction t")
List<MTransaction> findRecentTransactions(Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?3")
List<MTransaction> findCardTransactions(Date date, Date date2, MService service);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.account=?3")
List<MTransaction> findCorporateTransactions(Date startDate, Date endDate,MPQAccountDetails account);


@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1 AND t.status=?2")
Date getLastTranasactionTimeStampByStatus(MPQAccountDetails accountDetail, Status reversed);

@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1")
Date getLastTranasactionTimeStamp(MPQAccountDetails accountDetail);

@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1 AND t.debit=?2")
Date getLastTranasactionTimeStampBus(MPQAccountDetails accountDetail, boolean debit);

@Query("SELECT SUM(t.amount) from MTransaction t where t.service=?1")
long getTotalLoadMoney(MService service);

@Query("SELECT SUM(t.amount) from MTransaction t where t.service IN(1,3) and t.status='Success' AND t.transactionType='0'")
Double getTotalLoadMoney();

/*@Query("select t from MTransaction t where t.account=?1 AND t.service='3' AND t.status='Pending'")
List<MTransaction> findByServiceAccount(MPQAccountDetails account);
*/
@Query("SELECT MAX(u.amount) from MTransaction u where DATE(u.created) BETWEEN ?2 AND ?3 AND u.transactionType='0' AND u.status='Success' AND u.account=?1")
Double findByCreatedAndUser(MPQAccountDetails mpqAccountDetails, Date startDate, Date endDate);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=1 AND u.status='Success' order by u.created desc")
List<MTransaction> getLoadTransactions(Date date, Date date2, Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=1 order by u.created desc")
List<MTransaction> getAllLoadTransactions(Date from, Date to, Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=3 AND u.status='Success' order by u.created desc")
List<MTransaction> getLoadTransactionsUPI(Date date, Date date2, Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=3 order by u.created desc")
List<MTransaction> getAllLoadTransactionsUPI(Date from, Date to, Pageable pageable);

@Query("SELECT t FROM MTransaction t where t.account=?1 and t.transactionType='0' AND t.service=?2 order by t.created desc")
List<MTransaction> getByUserAndService(MPQAccountDetails mpqAccountDetails, MService service);

@Query("SELECT SUM(t.amount) from MTransaction t where t.account = ?1 AND t.service IN(1,3) AND t.transactionType='0' AND t.status='Success'")
Double findTotalLoadMoneyByUser(MPQAccountDetails accountDetail);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=1 AND u.status=?3 order by u.created desc")
List<MTransaction> getLoadTransactions(Date from, Date to, Status failed, Pageable pageable);


@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=2 order by u.created desc")
List<MTransaction> getAllPromoTransactions(Date from, Date to, Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=2 AND u.status=?3 order by u.created desc")
List<MTransaction> getPromosTransaction(Date from, Date to, Status failed, Pageable pageable);


@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=2 AND u.status='Success' order by u.created desc")
List<MTransaction> getLoadPromoTransactions(Date date, Date date2, Pageable pageable);

@Query("Select u FROM MTransaction u where u.isMdexTransaction='1' AND u.isSuspicious='1' AND u.debit='1' AND u.status='Success'")
List<MTransaction> getAllSuspicious();

@Query("Select u FROM MTransaction u where u.isMdexTransaction='1' AND u.isSuspicious='1' AND u.debit='1' AND u.status='Success' AND DATE(u.created)=?1")
List<MTransaction> getAllSuspiciousCustom(String present);

@Query("Select u FROM MTransaction u where u.isMdexTransaction='1' AND u.debit='1' order by created DESC")
List<MTransaction> getAllMdexTransaction();

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.service.serviceType=?3 AND u.isMdexTransaction='1' AND u.debit='1' order by u.created desc")
List<MTransaction> getAllMdexTransactionList(Date from, Date to,MServiceType serviceType, Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.isMdexTransaction='1' AND u.debit='1' AND u.status='Success' order by u.created desc")
List<MTransaction> getMdexTransaction(Date date, Date date2, Pageable pageable);
@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?3  order by u.created desc")
List<MTransaction> getAllImpsTransactions(Date oneMonthBack, Date present, Pageable pageable, MService service);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?3 AND u.status=?4 order by u.created desc")
List<MTransaction> getAllImpsTransactionsByStatus(Date from, Date to, Pageable pageable, MService service,Status success);

@Query("Select sum(u.commissionEarned) FROM MTransaction u where u.service=?1")
Double findTotalImpsCommission(MService service);

@Query("select u from MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' and u.service IN(1,2) AND u.status='Success' order by u.created desc")
List<MTransaction> getDataForAdminLineGraph(Date date1,Date date2);

@Query("SELECT count(mt) FROM MTransaction mt where service_id=?1 and status=?2 and mt.created between ?3 and ?4")
Long getAllPaymentGateway( int service_id ,Status status,Date from,Date to);

@Query("SELECT count(mt) FROM MTransaction mt where service_id=?1 and status=?2 and mt.created between ?3 and ?4")
Long getAllUpiPayment(int service_id,Status status,Date from,Date to);

@Query("SELECT mt FROM MTransaction mt where mt.service.operator=?1 and mt.account=?2 order by mt.created desc")
Page<MTransaction> getAllDetails(Pageable pageable,MOperator operator,MPQAccountDetails accountDetails);

@Query("SELECT m FROM MTransaction m where m.upiId=?1")
MTransaction getByUpiId(String upiId);

@Query("SELECT sum(mt.amount) FROM MTransaction mt where service_id=?1 and status=?2 and mt.created between ?3 and ?4")
Double getAllUpiPaymentSum(int service_id,Status status,Date from,Date to);

@Query("SELECT sum(mt.amount) FROM MTransaction mt where service_id=?1 and status=?2 and mt.created between ?3 and ?4")
Double getAllUpiRefundSum(int service_id,Status status,Date from,Date to);

@Query("SELECT sum(mt.amount) FROM MTransaction mt where service_id=?1 and status=?2 and mt.created between ?3 and ?4")
Double getAllPaymentGatewaySum( int service_id ,Status status,Date from,Date to);

@Query("select sum(m.amount) as totalAmount,monthname(m.created) as month from MTransaction m where m.status=?1 and m.debit=0 and m.service=?2 group by month(m.created)")
List<Object[]> getAllCreditTransactionsMonthWise(Status status,MService service);

@Query("select monthname(m.created) as month from MTransaction m where m.status=?1 and m.debit=0 and m.service=?2 group by month(m.created)")
List<String> getMonthsForBarGraph(Status status,MService service);

@Query("select sum(m.amount) as month from MTransaction m where m.status=?1 and m.debit=0 and m.service=?2 group by month(m.created)")
List<Double> getTransactionAmountMonthWise(Status status,MService service);


@Query("SELECT sum(mt.amount) FROM MTransaction mt where mt.service.operator=?1 and mt.status=?2 and mt.created between ?3 and ?4")
Double getAllPaymentsByMdex(MOperator operator,Status status,Date from,Date to);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.service.serviceType=?3 AND u.status=?4 AND u.debit='1' order by u.created desc")
List<MTransaction> getAllMdexTransactionListByServiceType(Date from, Date to, MServiceType serviceType,Status status,Pageable pageable);

@Query("SELECT u FROM MTransaction u where u.upiId=?1")
MTransaction getAllUpiTransaction(String upi);

@Query("SELECT u FROM MTransaction u where u.status='Initiated' AND u.created=?1")
MTransaction getAllUpiInitiatedTransactions(String date);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.account=?3 AND u.status=?4 order by u.created desc")
List<MTransaction> getAllSuccessLoadTransactions(Date from, Date to, MPQAccountDetails mpqAccountDetails, Status status);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=1 AND u.status=?3 order by u.created desc")
List<MTransaction> getSuccessLoadTransactions(Date from, Date to, Status failed);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?3 order by u.created desc")
List<MTransaction> getAllCashBackTransactions(Date from, Date to,MService service,Pageable pageable);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?3 order by u.created desc")
List<MTransaction> getAllServiceType(Date from, Date to, MService service);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' order by u.created desc")
List<MTransaction>  getAllServices(Date from,Date to);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.account=?3 order by u.created desc")
List<MTransaction> getAllLoadTransactions(Date from, Date to, MPQAccountDetails accountDetail);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?5 AND u.account=?3 AND u.status=?4 order by u.created desc")
List<MTransaction> getSuccessLoadTransactions(Date from, Date to, MPQAccountDetails accountDetail, Status status,MService service);

@Query("SELECT u FROM MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.service=?4 AND u.account=?3  order by u.created desc")
List<MTransaction> getAllServiceTransactions(Date from, Date to, MPQAccountDetails accountDetail, MService service);

@Query("select u from MTransaction u where u.transactionType=15 and u.account=?1 and u.status in('Failed','Success') order by created desc")
List<MTransaction> getDriveUTransaction(MPQAccountDetails account);

@Query("select u from MTransaction u where u.transactionType=15 and u.account=?1 and u.status='Success' order by created desc")
List<MTransaction> getSuccessTransaction(MPQAccountDetails account);

@Query("select u from MTransaction u where u.transactionType=15 and u.account=?1 and u.status='Failed' order by created desc")
List<MTransaction> getDriveUTransactionFailed(MPQAccountDetails account);

@Query("select u from MTransaction u where u.account IN (?1) order by created desc")
List<MTransaction> getTransactionByAccounts(List<MPQAccountDetails> accounts);


@Query("SELECT SUM(u.amount) FROM MTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=true")
double getDailyDebitTransationTotalAmount(int year, int month, int day, MPQAccountDetails account);

@Query("select u from MTransaction u where DATE(u.created)=?1 and u.status='Initiated' and u.service='7' and u.transactionType='0'")
List<MTransaction> triggerTransfer(Date from);

@Query("select u from MTransaction u where u.service=?1 and u.debit=?2 and u.account=?3 and u.status='Success'")
List<MTransaction> getTransactionListFingoole(MService service, boolean debit, MPQAccountDetails acccount);

@Query("select u from MTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 and u.service=?3 and u.debit=?4 and u.status=?5 and u.cardLoadStatus='Pending'")
List<MTransaction> getTransactionAutoRefund(Date from, Date to, MService service, boolean debit, Status status);

@Query("select u from MTransaction u where u.authReferenceNo=?1")
MTransaction findByRetreivalReferenceNo(String rettreival);

@Query("SELECT MAX(t.created) FROM MTransaction t where t.account=?1 AND t.debit=?2")
Date getTimeStampOfLastTransaction(MPQAccountDetails account, boolean res);

@Query("SELECT t FROM MTransaction t where t.created=?1 AND t.account=?2 AND t.debit=?3")
MTransaction getLastTransactionDetails(Date from, MPQAccountDetails account, boolean res);



}
