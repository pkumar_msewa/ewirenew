package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.FCMLogs;

public interface FCMLogsRepository extends CrudRepository<FCMLogs, Long>,
JpaSpecificationExecutor<FCMLogs>{

}
