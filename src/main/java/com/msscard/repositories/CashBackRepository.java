package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.msscard.entity.CashBack;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;

public interface CashBackRepository extends CrudRepository<CashBack, Long>, PagingAndSortingRepository<CashBack, Long>,
JpaSpecificationExecutor<CashBack>{

	@Query("select c from CashBack c where c.transaction=?1")
	CashBack getCashBackTrasactionsByTran(MTransaction transaction);
	
	@Query("select c from CashBack c where c.user=?1")
	List<CashBack> getCashBackByUser(MUser user);
	
}
