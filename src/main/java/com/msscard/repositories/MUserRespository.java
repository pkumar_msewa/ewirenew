package com.msscard.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.msscard.entity.GroupDetails;
import com.msscard.entity.MLocationDetails;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.model.Status;
import com.msscard.model.UserType;

public interface MUserRespository
		extends CrudRepository<MUser, Long>, PagingAndSortingRepository<MUser, Long>, JpaSpecificationExecutor<MUser> {

	@Query("select u from MUser u where u.accountDetail IN (?1)")
	List<MUser> getUsersByAccount(List<MPQAccountDetails> accounts);

	@Query("select u from MUser u where  u.userType IN (?1)")
	List<MUser> getByUserType(List<UserType> userType);

	@Query("select u from MUser u where  u.userType IN (?1)")
	MUser getUserType(UserType userType);

	@Query("select u.userDetail.email from MUser u where u.mobileStatus='Active' AND u.userType=?1")
	List<String> getMailsOfActiveUser(UserType MUser);

	@Query("select u from MUser u where u.username=?1")
	MUser checkForLogin(String username);

	@Query("select u.accountDetail from MUser u where u.authority=?1")
	List<MPQAccountDetails> getAccountsByAuthority(String authority);

	@Query("select u from MUser u where u.accountDetail=?1")
	MUser getByAccount(MPQAccountDetails account);

	@Query("select u from MUser u where u.userType=?1 ORDER BY u.created DESC ")
	Page<MUser> getTodaysUser(Pageable page, UserType userType);

	/*
	 * @Query("select u from MUser u where u.userType=?1 and u.MUserDetails.gender=?2 ORDER BY u.created DESC"
	 * ) Page<MUser> getTodaysUser(Pageable page,UserType userType,Gender gender);
	 */
	@Query("select u from MUser u where u.userType=?1 and u.accountDetail.accountType=?2 ORDER BY u.created DESC ")
	Page<MUser> getTodaysUser(Pageable page, UserType userType, MPQAccountType accountType);

	@Query("select u from MUser u where u.userType=?1 and u.authority=?2 ORDER BY u.created DESC ")
	Page<MUser> getTodaysUser(Pageable page, UserType userType, String authority);

	@Query("select u from MUser u where u.userType=?1 and u.mobileStatus=?2 ORDER BY u.created DESC ")
	Page<MUser> getTodaysUser(Pageable page, UserType userType, Status status);

	@Query("select u from MUser u where u.userType=?1 and u.created BETWEEN ?2 AND ?3")
	List<MUser> getUserBetween(UserType userType, Date startDate, Date endDate);

	@Query("select u from MUser u where u.userType=?1 and u.authority=?4 and u.created BETWEEN ?2 AND ?3")
	List<MUser> getUserBetween(UserType userType, Date startDate, Date endDate, String authority);

	@Query("select u from MUser u where u.userType=?1 and u.mobileStatus=?4 and u.created BETWEEN ?2 AND ?3")
	List<MUser> getUserBetween(UserType userType, Date startDate, Date endDate, Status status);

	@Query("select u from MUser u where u.userType=?1 and u.accountDetail.accountType=?4 and u.created BETWEEN ?2 AND ?3")
	List<MUser> getUserBetween(UserType userType, Date startDate, Date endDate, MPQAccountType accountType);

	
	@Query("select u from MUser u where u.userDetail.email=?1")
	List<MUser> getByMail(String email);

	@Query("select u from MUser u where u.username=?1")
	MUser findByUsername(String username);

	@Query("select u from MUser u where u.username=?1 and u.authority='ROLE_GROUP,ROLE_AUTHENTICATED'")
	MUser findByUsernameAndAuthority(String username);

	@Query("select u from MUser u where u.username=?1")
	List<MUser> findByMobileNo(String username);

	@Query("select u.gcmId from MUser u where u.userType=?1 AND u.mobileStatus='Active' AND u.gcmId IS NOT NULL")
	Page<String> getGCMIds(Pageable pageable, UserType userType);

	@Query("select u from MUser u where u.userType IN (?1,?2)")
	List<MUser> getAllBlockedAdmins(UserType admin, UserType superAdmin);

	@Query("select u from MUser u where u.userType = ?1 and u.emailStatus='Active' and u.mobileStatus='Active'")
	List<MUser> getAllActiveUsers(UserType MUser);

	@Query("select u from MUser u where u.username=?1 and u.mobileStatus=?2")
	MUser findByUsernameAndStatus(String username, Status status);

	@Query("select u from MUser u where u.username=?1 and u.mobileStatus=?2 and u.emailStatus=?3")
	MUser findByUsernameAndMobileStatusAndEmailStatus(String username, Status mobileStatus, Status emailStatus);

	@Query("select u from MUser u where u.userType='1' order by created desc")
	List<MUser> findAllUser();

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_BLOCKED'")
	List<MUser> findAllBlockedUsers();

	@Query("select u from MUser u where u.authority LIKE '%ROLE_USER%' and u.userType='1' and u.mobileStatus='Active'")
	Page<MUser> findAuthenticateUser(Pageable page);

	@Query("select u from MUser u where u.authority in('ROLE_USER,ROLE_AUTHENTICATED','ROLE_USER,ROLE_LOCKED') and u.userType='1' and u.mobileStatus='Active' and Date(u.created) BETWEEN date(?1) AND date(?2)")
	List<MUser> findAuthenticateUsers(Date startDate, Date endDate);

	@Query("select u from MUser u where u.authority in('ROLE_USER,ROLE_AUTHENTICATED','ROLE_USER,ROLE_LOCKED') and u.userType='1' and u.mobileStatus='Active' and u.created <=?1")
	List<MUser> findAuthenticateUserss(Date endDate);

	@Query("select count(u) from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'")
	long findAllAuthenticateUser();

	@Query("select u from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'")
	List<MUser> findAllAuthenticateUserList();

	@Query("select u from MUser u where u.emailToken=?1")
	MUser findByEmailToken(String key);

	@Query("select u from MUser u where u.emailToken=?1 and u.emailStatus=?2")
	MUser findByEmailTokenAndStatus(String key, Status active);

	@Query("select u from MUser u where u.emailToken=?1 and u.emailStatus=?2 and u.mobileStatus=?3")
	MUser findByEmailTokenAndEmailStatusAndMobileStatus(String key, Status emailStatus, Status mobileStatus);

	@Query("select u from MUser u where u.mobileToken=?1 and u.mobileStatus=?2")
	MUser findByOTPAndStatus(String key, Status active);

	@Query("select u from MUser u where u.mobileToken=?1 and u.username=?2 and u.mobileStatus=?3")
	MUser findByMobileTokenAndStatus(String key, String mobile, Status active);

	@Query("select u from MUser u where u.mobileToken=?1 and u.username=?2 and u.mobileStatus=?3")
	MUser findByMobileTokenAndStatusForDirectPayment(String key, String mobile, Status active);

	@Query("select u from MUser u where u.emailToken=?1 and u.username=?2")
	MUser findByEmailTokenAndUsername(String key, String username);

	@Query("select u from MUser u where u.mobileToken=?1 and u.username=?2")
	MUser findByMobileTokenAndUsername(String key, String username);

	@Query("select u from MUser u where u.userDetail=?1")
	MUser findByUserDetails(MUserDetails MUserDetails);

	@Query("select u from MUser u where u.accountDetail=?1")
	MUser findByAccountDetails(MPQAccountDetails accountDetail);

	@Query("select p from MUser p")
	public List<MUser> findWithPageable(Pageable pageable);

	@Query("select p from MUser p where p.userType=9 ORDER BY p.id desc")
	public List<MUser> findAllMerchants();

	@Query("select p from MUser p")
	List<MUser> findWithVerifiedUsersPageable(Pageable pageable);

	@Query("select COUNT(p) from MUser p where p.userType= ?1")
	Long getTotalUsersCount(UserType userType);

	@Query("select count(u) from MUser u")
	Long getUserCount();

	@Modifying
	@Transactional
	@Query("update MUser c set c.authority=?1 where c.id =?2")
	int updateUserAuthority(String authority, long id);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	Page<MUser> findAll(Pageable page);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' and DATE(u.created) BETWEEN ?1 AND ?2 ")
	Page<MUser> findAllByDate(Pageable page, Date fromDate, Date toDate);

	// select * from payqwikdb.MUser where authority='ROLE_USER,ROLE_AUTHENTICATED'
	// AND emailStatus='Active' AND mobileStatus='Active';
	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.emailStatus='Active' AND u.mobileStatus='Active'")
	Page<MUser> getActiveUsers(Pageable pageable);

	// @Query("select u from MUser u where u.mobileStatus='Inactive' ")
	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.emailStatus='Inactive' OR u.mobileStatus='Inactive'")
	Page<MUser> getInactiveUsers(Pageable pageable);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_BLOCKED'")
	Page<MUser> getBlockedUsers(Pageable pageable);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_LOCKED'")
	Page<MUser> getLockedUsers(Pageable pageable);

	@Query("select u from MUser u where u.id=?1")
	MUser findUserByUserId(long id);

	@Query("select u from MUser u where u.userType=?1")
	List<MUser> getTotalUsers(UserType userType);

	@Modifying
	@Transactional
	@Query("update MUser u set u.gcmId=?1 where u.username=?2")
	int updateGCMID(String gcmId, String username);

	@Query("select u from MUser u where u.created=?2")
	List<MUser> getUsersByDate(Date created);

	@Query("select t from MUser t where t.accountDetail=?1")
	MUser getUserFromAccountId(MPQAccountDetails accountDetail);

	@Query("select u from MUser u where u.username LIKE ?1 ")
	List<MUser> getPossibilitiesByMobile(String mobile);

	@Query("select u.username from MUser u where u.userType=?1 and u.mobileStatus='Active' ")
	List<String> getAllActiveUsername(UserType userType);

	@Query("select u.username from MUser u where u.authority='ROLE_GROUP,ROLE_AUTHENTICATED' and u.groupDetails=?1")
	List<String> getGroupUser(GroupDetails group);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.groupDetails=?1 and u.userDetail.groupStatus='Active'")
	List<MUser> getGroupUsersSms(GroupDetails g);

	@Query("select t from MUser t where t.created BETWEEN ?1 AND ?2 and t.authority='ROLE_USER,ROLE_AUTHENTICATED' ORDER BY t.created DESC")
	List<MUser> findUsersByDate(Date from, Date to);

	/*
	 * @Query("select u from AgentDetail u where u.panCardNo=?1") AgentDetail
	 * findByAgentPannumber(String Agent);
	 */
	@Query("select u from MUser u where u.authority='ROLE_LOCKED,ROLE_AUTHENTICATED' AND u.userType=3  ORDER BY u.created DESC")
	List<MUser> findAllLockedAccounts();

	@Query("select u from MUser u where u.authority='ROLE_AGENT,ROLE_AUTHENTICATED' AND u.userType=5 ORDER BY u.created DESC")
	List<MUser> findAllAgent();

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.userDetail=?1 ORDER BY u.created DESC")
	List<MUser> findUserByUserDetails(MUserDetails MUserDetails);

	@Query("select SUM(u.accountDetail.balance) from MUser u where u.userType=1 ORDER BY u.created DESC")
	Double findUsersWalletBalance();

	@Query("select u from MUser u where u.userDetail IN(select ud.id from MUserDetails ud where ud.location=?1) and u.created BETWEEN ?2 AND ?3 ")
	List<MUser> findUserByLocationCode(MLocationDetails locationDetails, Date StartDate, Date endDate);

	@Query("select u from MUser u where u.authority='ROLE_MERCHANT,ROLE_AUTHENTICATED' AND u.userType=2")
	List<MUser> findAllMerchantList();

	@Query("select u from MUser u where u.androidDeviceID=?1 and u.isDeviceLocked=?2")
	MUser findByAndroidDeviceId(String deviceID, boolean isDeviceLocked);

	/* Graph */

	@Query("select count(u) from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'and u.created BETWEEN ?1 AND ?2")
	long findUserCountByMonth(Date parse, Date parse2);

	/* Admin Panel Backend Report */

	@Query("SELECT DATE(u.created) FROM MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	List<Date> getBeginDateOfUsers();

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND DATE(u.created) BETWEEN ?1 AND ?2")
	Page<MUser> findAllByDate(Date from, Date to, Pageable page);

	@Query("select distinct(s.user) from UserSession s WHERE s.expired=false ")
	List<MUser> getOnlineUsers();

	@Query("select u from MUser u where u.created BETWEEN ?1 AND ?2 and   u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.emailStatus='Active' AND u.mobileStatus='Active' ORDER BY u.created DESC")
	List<MUser> getActiveUsers(Date from, Date to);

	@Query("select u from MUser u where u.created BETWEEN ?1 AND ?2 and   u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND (u.emailStatus='Inactive' OR u.mobileStatus='Inactive') ORDER BY u.created DESC")
	List<MUser> getInActiveUsers(Date from, Date to);

	@Query("select u from MUser u where u.created BETWEEN ?1 AND ?2 and   u.authority='ROLE_USER,ROLE_LOCKED' ORDER BY u.created DESC")
	List<MUser> getLockedUsers(Date from, Date to);

	@Query("select u from MUser u where u.created BETWEEN ?1 AND ?2 and  u.authority='ROLE_USER,ROLE_BLOCKED' ORDER BY u.created DESC")
	List<MUser> getBlockedUsers(Date from, Date to);

	@Query("select u.accountDetail from MUser u where u.userType='1' Order by u.username ASC")
	List<MPQAccountDetails> findAllAccountByUser();

	@Query("select u from MUser u where u.authority='ROLE_CORPORATE_AGENT,ROLE_AUTHENTICATED' AND u.userType=9 ORDER BY u.created DESC")
	List<MUser> findCorporateAgent();

	@Query("select count(u) from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1")
	long findAllActiveUser();

	@Query("select u from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1")
	List<MUser> findAllActiveUserList();

	@Query("select u from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1 and u.accountDetail.accountType.code='KYC'")
	List<MUser> findAllKYCUserList();

	@Query("select u from MUser u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1 and u.accountDetail.accountType.code='NONKYC'")
	List<MUser> findAllNONKYCUserList();

	@Query("select u from MUser u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 order by created desc")
	Page<MUser> findAllUserList(Pageable pageable, Date date, Date date2);

	@Query("select u from MUser u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1 order by created desc")
	Page<MUser> findAllActiveUserList(Pageable pageable, Date fromDate, Date toDate);

	@Query("select u from MUser u where u.userType='1' and DATE(u.created) BETWEEN ?1 AND ?2 and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active' and u.isFullyFilled=1 and u.accountDetail.accountType.code='KYC' order by created desc")
	Page<MUser> findAllKYCUserList(Pageable pageable, Date fromDate, Date toDate);

	@Query("select u from MUser u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.username=?1 and u.mobileStatus=?2")
	MUser getUserBasedOnUserNameAndAuthority(String userName, Status status);

	@Query("select u from MUser u where u.username=?1 AND u.authority='ROLE_AGENT,ROLE_AUTHENTICATED'")
	MUser getAgentBasedOnUserName(String userName);

	/* FOR EMAIL CRON */
	@Query("SELECT count(mu) FROM MUser mu where mu.mobileStatus=?1 AND isFullyFilled=?2 AND  mu.created BETWEEN ?3 and ?4")
	Long getAllUser(Status mobileStatus, boolean isFullyFilled, Date from, Date to);

	@Query("select m from MUser m where m.username=?1")
	List<MUser> getBymobileNumber(String number);

	@Query("select m from MUser m where m.userDetail.email=?1")
	List<MUser> getByEmail(String email);

	@Query("select m from MUser m where m.userDetail.firstName=?1")
	List<MUser> getFirstName(String firstName);

	@Query("select m.gcmId from MUser m where m.gcmId is not null")
	List<String> getAllGcmIds();

	@Query("select m.gcmId from MUser m where m.username=?1")
	List<String> getGcmByUsername(String username);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.groupDetails.groupName!='None' AND u.userDetail.groupStatus= ?3 OR u.userDetail.groupChange=?4")
	Page<MUser> findAllUserByGroupStatusByContactAdmin(Pageable pageable, Date from, Date to, String status,
			boolean change);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.groupDetails.groupName!='None' AND u.userType='1' AND u.groupDetails.contactNo=?3 and u.userDetail.groupStatus= ?4 OR u.userDetail.groupChange=?5")
	Page<MUser> findAllUserByGroupStatusByContact(Pageable pageable, Date from, Date to, String contact, String status,
			boolean change);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.userDetail.groupStatus= ?3 AND u.groupDetails.groupName!='None' OR u.userDetail.groupChange=?4")
	Page<MUser> findAllUserByGroupStatus(Pageable pageable, Date from, Date to, String status, boolean change);

	@Query("SELECT u from MUser u WHERE u.userType='1' AND u.userDetail.groupStatus= ?1 AND u.groupDetails.groupName!='None' OR u.userDetail.groupChange=?2")
	Page<MUser> findAllUserByGroupStatusNoDate(Pageable pageable, String status, boolean change);

	@Query("select u from MUser u where DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 and u.groupDetails =?3 and u.userType='1'")
	Page<MUser> findAllUserBasedOnGroup(Pageable pageable, Date from, Date to, GroupDetails gd);

	@Query("select u from MUser u where u.groupDetails.email=?1 and u.mobileStatus=?2")
	MUser findByGroupUsernameAndStatus(String username, Status status);

	@Query("select u from MUser u where u.authority='ROLE_GROUP,ROLE_AUTHENTICATED' AND u.username=?1 and u.mobileStatus=?2")
	MUser getUserBasedOnUserNameAndAuthorityGroup(String userName, Status status);

	@Query("select u from MUser u where u.mobileStatus=?1 and u.userType=?2")
	List<MUser> getListOfUsers(Status status, UserType userType);

	@Query("select u from MUser u where u.username=?1")
	Page<MUser> getListUserbyContact(Pageable pageable, String username);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userDetail.groupStatus= ?3")
	Page<MUser> findAllUserByGroupStatusDeleted(Pageable pageable, Date from, Date to, String status);

	@Query("SELECT COUNT(u) from MUser u WHERE u.userType='1' AND u.userDetail.groupStatus= ?1 AND u.groupDetails.contactNo=?2 OR u.userDetail.groupChange=?3")
	long findAllUserByGroupStatusByCount(String status, String contact, boolean count);

	@Query("SELECT COUNT(u) from MUser u WHERE u.userDetail.groupStatus= ?1 AND u.groupDetails.contactNo=?2 AND u.userType='1'")
	long findAllUserByGroupStatusCount(String status, String contact);

	@Query("SELECT COUNT(u) from MUser u WHERE u.userDetail.groupStatus= ?1 AND u.userDetail.previousGroupContact=?2 AND u.userType='1'")
	long findAllUserByCountReject(String status, String contact);

	@Query("SELECT u from MUser u WHERE u.groupDetails.groupName!='None' and u.userDetail.groupStatus= ?1 OR u.userDetail.groupChange=?2")
	Page<MUser> findAllUserByGroupStatusByContactAdminNoDate(Pageable pageable, String status, boolean change);

	@Query("SELECT u from MUser u WHERE u.userDetail.groupStatus= ?1 AND u.groupDetails.contactNo=?2")
	Page<MUser> findAllUserByGroupStatusByContactNoDate(Pageable pageable, String status, String contact);

	@Query("select u from MUser u where u.userType='1' AND u.groupDetails.groupName!='None' AND u.userDetail.groupStatus= ?1 OR u.userDetail.groupChange=?2")
	Page<MUser> getAllRequestListGroup(Pageable pageable, String status, boolean change);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.userDetail.groupStatus= ?3")
	Page<MUser> acceptedGroupStatus(Pageable pageable, Date from, Date to, String status);

	@Query("SELECT u from MUser u WHERE u.userType='1' AND u.userDetail.groupStatus= ?1")
	Page<MUser> acceptedGroupStatusGet(Pageable pageable, String status);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.userDetail.groupStatus= ?3")
	Page<MUser> rejectedGroupStatus(Pageable pageable, Date from, Date to, String status);

	@Query("SELECT u from MUser u WHERE u.userType='1' AND u.userDetail.groupStatus= ?1")
	Page<MUser> rejectedGroupStatusGet(Pageable pageable, String status);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.userDetail.previousGroupContact=?3 AND u.userDetail.groupStatus= ?4")
	Page<MUser> rejectedGroupByContactStatus(Pageable pageable, Date from, Date to, String contact, String status);

	@Query("SELECT u from MUser u WHERE u.userDetail.previousGroupContact=?1 AND u.userDetail.groupStatus= ?2 AND u.userType='1'")
	Page<MUser> rejectedGroupByContactStatusGet(Pageable pageable, String contact, String status);

	@Query("SELECT u from MUser u WHERE u.userType='1' AND u.groupDetails.contactNo=?1 and u.userDetail.groupStatus= ?2")
	Page<MUser> acceptedGroupStatusByContact(Pageable pageable, String contact, String status);

	@Query("SELECT u from MUser u WHERE DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.userType='1' AND u.groupDetails.contactNo=?3 and u.userDetail.groupStatus= ?4")
	Page<MUser> acceptedGroupStatusByContactPost(Pageable pageable, Date from, Date to, String contact, String status);

	@Query("SELECT u from MUser u WHERE u.userType='1' AND u.groupDetails.groupName!='None' AND u.groupDetails.contactNo=?1 AND u.userDetail.groupStatus= ?2 OR u.userDetail.groupChange=?3")
	Page<MUser> requestUserGroup(Pageable pageable, String contact, String status, boolean change);

	@Query("select u from MUser u where u.userDetail.groupStatus=?1 AND u.username=?2")
	List<MUser> getSingleRequest(String status, String username);

	@Query("SELECT u.id from MUser u where u.username in (?1)")
	List<Long> getListByMobile(List<String> ky);

	@Query("SELECT m FROM MUser m WHERE m.authority = 'ROLE_USER,ROLE_AUTHENTICATED' AND m.accountDetail.accountType.id = '1' ")
	List<MUser> getKYCUserListForKYCCheck();

	@Query("select u from MUser u where u.groupDetails.groupName!='None' AND u.groupDetails.email = ?1 AND u.userDetail.groupChange = ?2 AND DATE(u.created) >= ?3 AND DATE(u.created) <= ?4")
	Page<MUser> getChangeList(Pageable pageable, String email, boolean success, Date date, Date date2);

	@Query("select u from MUser u where DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 and u.groupDetails.groupName!='None' AND u.groupDetails.email=?3 AND u.userDetail.groupChange=?4")
	Page<MUser> getChangeListPost(Pageable pageable, Date from, Date to, String email, boolean success);
}
