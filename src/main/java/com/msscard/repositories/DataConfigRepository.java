package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cashiercards.paramerterization.DataConfig;


public interface DataConfigRepository extends CrudRepository<DataConfig, Long>,
JpaSpecificationExecutor<DataConfig>{

	@Query("select t from DataConfig t")
	DataConfig findDatas();
	
}
