package com.msscard.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.Donation;

public interface DonationRepository extends CrudRepository<Donation, Long>, PagingAndSortingRepository<Donation, Long>, JpaSpecificationExecutor<Donation>{

	@Query("select d from Donation d")
	Page<Donation> getDonationDetailsDate(Pageable pageable);
	
	@Query("select d from Donation d where d.created BETWEEN ?1 AND ?2")
	Page<Donation> getDonationDetailsDatePost(Pageable pageable, Date from, Date to);
	
	@Query("select d from Donation d")
	Page<Donation> getDonationDetails(Pageable pageable);
	
	@Query("select d from Donation d where d.contactNo=?1")
	Donation getDonation(String contactNo);
}
