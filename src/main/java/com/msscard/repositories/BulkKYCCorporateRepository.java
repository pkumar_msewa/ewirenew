package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.BulkKYCCorporate;

public interface BulkKYCCorporateRepository extends CrudRepository<BulkKYCCorporate, Long>,
		PagingAndSortingRepository<BulkKYCCorporate, Long>, JpaSpecificationExecutor<BulkKYCCorporate> {

	@Query("SELECT b FROM BulkKYCCorporate b WHERE b.mobile = ?1")
	BulkKYCCorporate findByMobile(String mobile);

	@Query("SELECT b FROM BulkKYCCorporate b WHERE b.email = ?1")
	BulkKYCCorporate findByEmail(String email);

	@Query("SELECT b FROM BulkKYCCorporate b WHERE b.kycStatus = ?1 AND b.failedStatus = ?2")
	List<BulkKYCCorporate> getListOfUserForKYC(boolean b, boolean c);

	@Query("SELECT b FROM BulkKYCCorporate b WHERE b.kycStatus = ?1")
	List<BulkKYCCorporate> getKYCList(boolean b);

}