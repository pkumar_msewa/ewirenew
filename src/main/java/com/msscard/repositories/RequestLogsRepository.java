package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import com.msscard.entity.RequestLogs;

public interface RequestLogsRepository extends CrudRepository<RequestLogs, Long>, JpaSpecificationExecutor<RequestLogs>{

	
}
