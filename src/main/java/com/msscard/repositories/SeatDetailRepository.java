package com.msscard.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.msscard.entity.TravelSeatDetail;

public interface SeatDetailRepository extends CrudRepository<TravelSeatDetail, Long> ,
PagingAndSortingRepository<TravelSeatDetail, Long>,JpaSpecificationExecutor<TravelSeatDetail>{

}
