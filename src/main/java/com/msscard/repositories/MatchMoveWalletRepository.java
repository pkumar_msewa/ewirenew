package com.msscard.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;

public interface MatchMoveWalletRepository
		extends CrudRepository<MatchMoveWallet, Long>, JpaSpecificationExecutor<MatchMoveWallet> {

	@Query("select u from MatchMoveWallet u where user=?1")
	MatchMoveWallet findByUser(MUser user);

	@Query("select u from MatchMoveWallet u where u.walletId is not null")
	List<MatchMoveWallet> getListOfWallet();

}