package com.msscard.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.msscard.model.MMCardsDTO;
import com.msscards.metadatas.URLMetadatas;

public class ExcelWriter{
	@SuppressWarnings("unused")
	public static String makeExcelSheet(List<MMCardsDTO> cards,String filename) {

	
	 //filename = ("Card_Records"+System.currentTimeMillis()+".xls");
		try {
			
			String path= System.getProperty("user.dir");  
			FileOutputStream fileOut = new FileOutputStream(URLMetadatas.PATH +filename );
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("Worksheet");
			HSSFRow row = worksheet.createRow((short) 0);
			HSSFCell cellA1 = row.createCell(0);
			cellA1.setCellValue("Date");
			HSSFCellStyle styleOfCell = workbook.createCellStyle();
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellA1.setCellStyle(styleOfCell);
			
			HSSFCell cellB1 = row.createCell(1);
			cellB1.setCellValue("First Name");
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellB1.setCellStyle(styleOfCell);
			
			HSSFCell cellC1 = row.createCell(2);
			cellC1 .setCellValue("Last Name");
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellC1 .setCellStyle(styleOfCell);
			
			HSSFCell cellD1 = row.createCell(3);
			cellD1.setCellValue("Email ID");
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellD1.setCellStyle(styleOfCell);

			HSSFCell cellE1 = row.createCell(4);
			cellE1.setCellValue("Contact NO");
			styleOfCell = workbook.createCellStyle();
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellE1.setCellStyle(styleOfCell);
			
			HSSFCell cellF1 = row.createCell(5);
			cellF1.setCellValue("Card Number");
			styleOfCell = workbook.createCellStyle();
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			 cellF1.setCellStyle(styleOfCell);
			
			HSSFCell cellG1 = row.createCell(6);
			cellG1.setCellValue("Balance");
			styleOfCell = workbook.createCellStyle();
			styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
			styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
			cellG1.setCellStyle(styleOfCell);
			
			
			
		
			
			for (int i = 0; i < cards.size(); i++) {
				HSSFRow rowNew = worksheet.createRow((short) i+1);
				rowNew.createCell(0).setCellValue(cards.get(i).getDate());
				rowNew.createCell(1).setCellValue(cards.get(i).getFirstName());
				rowNew.createCell(2).setCellValue(cards.get(i).getLastName());
				rowNew.createCell(3).setCellValue(cards.get(i).getEmail());
				rowNew.createCell(4).setCellValue(cards.get(i).getContactNO());
				rowNew.createCell(5).setCellValue(cards.get(i).getCardNumber());
				rowNew.createCell(6).setCellValue(cards.get(i).getBalance());
				
			}
			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}

}
