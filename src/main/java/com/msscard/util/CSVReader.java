package com.msscard.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.csvreader.CsvReader;
import com.msscards.metadatas.URLMetadatas;

public class CSVReader {

	public static final String DESCRIPTION_BULKREGISTER = "File for Bulk Register";
	public static final String DESCRIPTION_BULKSMS = "File for Bulk SMS";
	public static final String DESCRIPTION_BULKCARDLOAD = "File for Bulk Card Load";
	public static final String DESCRIPTION_BULKKYC = "File for Bulk KYC Load";
	public static final String BULK_CARD_ASSIGN = "File for Bulk Card Assign in group";

	public static JSONObject readCsvBulkUploadRegister(String fileName) throws Exception {
		JSONObject userList = new JSONObject();
		String url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKREGISTER_CORPORATE + fileName;
		S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
		InputStream objectData = object.getObjectContent();
		CsvReader users = new CsvReader(new InputStreamReader(objectData));

		JSONArray jsonArray = new JSONArray();
		if (users.readHeaders()) {
			while (users.readRecord()) {
				System.err.println("m reading");
				String fname = users.get("firstName(Mandatory)");
				String lname = users.get("lastName(Mandatory)");
				String mobile = users.get("mobile");
				String email = users.get("email");
				String day = users.get("dob(dd)");
				String month = users.get("dob(MM)");
				String year = users.get("dob(yyyy)");
				String idType = users.get("idType(aadhaar|drivers_id|voters_id|passport)");
				String idNumber = users.get("idNumber");
				String proxyNo = users.get("proxyNo");
				System.err.println(fname);
				if (!lname.isEmpty() && !fname.isEmpty() && !mobile.isEmpty() && !email.isEmpty()
						&& !proxyNo.isEmpty()) {
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("firstName", fname);
					jsonObject.put("lastName", lname);
					jsonObject.put("mobile", mobile);
					jsonObject.put("email", email);
					jsonObject.put("dob(dd)", day);
					jsonObject.put("dob(MM)", month);
					jsonObject.put("dob(yyyy)", year);
					jsonObject.put("idType", idType);
					jsonObject.put("idNumber", idNumber);
					jsonObject.put("proxyNo", proxyNo);

					jsonArray.put(jsonObject);
				}
			}
			userList.put("UserList", jsonArray);
		}
		users.close();

		return userList;

	}

	public static JSONObject readCsvBulkUploadRegisterGroup(String absPath) throws Exception {
		JSONObject userList = new JSONObject();

		CsvReader users = new CsvReader(absPath);
		JSONArray jsonArray = new JSONArray();
		if (users.readHeaders()) {
			while (users.readRecord()) {
				System.err.println("m reading");
				String fname = users.get("firstName");
				String lname = users.get("lastName");
				String mobile = users.get("mobile");
				String email = users.get("email");
				String day = users.get("dob(dd)");
				String month = users.get("dob(MM)");
				String year = users.get("dob(yyyy)");
				String idType = users.get("idType");
				String idNumber = users.get("idNumber");

				System.err.println(fname);
				if (!lname.isEmpty() && !fname.isEmpty() && !mobile.isEmpty() && !email.isEmpty()) {
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("firstName", fname);
					jsonObject.put("lastName", lname);
					jsonObject.put("mobile", mobile);
					jsonObject.put("email", email);
					jsonObject.put("dob(dd)", day);
					jsonObject.put("dob(MM)", month);
					jsonObject.put("dob(yyyy)", year);
					jsonObject.put("idType", idType);
					jsonObject.put("idNumber", idNumber);

					jsonArray.put(jsonObject);
				}
				// perform program logic here
			}
			userList.put("UserList", jsonArray);
		}
		users.close();

		return userList;

	}

	public static JSONObject readCsvBulkUploadSms(String absPath) throws Exception {
		JSONObject userList = new JSONObject();

		CsvReader users = new CsvReader(absPath);
		JSONArray jsonArray = new JSONArray();
		if (users.readHeaders()) {
			while (users.readRecord()) {
				System.err.println("m reading");
				String mobile = users.get("mobile");
				String message = users.get("message");
				if (!mobile.isEmpty()) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("mobile", mobile);
					jsonObject.put("message", message);
					jsonArray.put(jsonObject);
				}
			}
			userList.put("UserList", jsonArray);
		}
		users.close();
		return userList;
	}

	public static JSONObject readCsvBulkUploadLoadBalance(String fileName) {
		JSONObject userList = new JSONObject();
		try {
			String url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKCARDLOAD_CORPORATE + fileName;
			S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
			InputStream objectData = object.getObjectContent();
			CsvReader users = new CsvReader(new InputStreamReader(objectData));
			JSONArray jsonArray = new JSONArray();

			// CsvReader users = new CsvReader(fileName);
			if (users.readHeaders()) {
				while (users.readRecord()) {
					System.err.println("m reading");
					String amount = users.get("amount");
					String mobile = users.get("mobile");
					String email = users.get("email");
					String dateOfTransaction = users.get("dateOfTransaction(yyyy-MM-dd)");
					if (!amount.isEmpty() && !email.isEmpty() && !mobile.isEmpty() && !email.isEmpty()
							&& !mobile.isEmpty() && !dateOfTransaction.isEmpty()) {
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("dateOfTransaction", dateOfTransaction);
						jsonObject.put("mobile", mobile);
						jsonObject.put("email", email);
						jsonObject.put("amount", amount);
						jsonArray.put(jsonObject);
					}
					// perform program logic here
				}
				userList.put("LoadMoneyList", jsonArray);
			}
			users.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;

	}

	public static JSONObject readCsvBulkKycUpload(String fileName) throws Exception {
		JSONObject userList = new JSONObject();

		String url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKKYC_CORPORATE + fileName;
		S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
		InputStream objectData = object.getObjectContent();
		CsvReader users = new CsvReader(new InputStreamReader(objectData));

		JSONArray jsonArray = new JSONArray();
		if (users.readHeaders()) {
			while (users.readRecord()) {
				String mobile = users.get("mobile");
				String email = users.get("email");
				String address1 = users.get("address1");
				String address2 = users.get("address2");
				String day = users.get("dob(dd)");
				String month = users.get("dob(MM)");
				String year = users.get("dob(yyyy)");
				String pincode = users.get("pinCode");
				String documentUrl = users.get("documentUrl");
				String documentUrl1 = users.get("documentUrl1");
				String idType = users.get("idType(aadhaar|pan|drivers_id|voters_id|passport)");
				String idNumber = users.get("idNumber");
				if (!mobile.isEmpty() && !email.isEmpty() && !address1.isEmpty() && !address2.isEmpty()
						&& !day.isEmpty() && !month.isEmpty() && !year.isEmpty()&& !pincode.isEmpty() && !documentUrl.isEmpty() && !idType.isEmpty()
						&& !idNumber.isEmpty() && !documentUrl1.isEmpty()) {
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("mobile", mobile);
					jsonObject.put("email", email);
					jsonObject.put("dob(dd)", day);
					jsonObject.put("dob(MM)", month);
					jsonObject.put("dob(yyyy)", year);
					jsonObject.put("address1", address1);
					jsonObject.put("address2", address2);
					jsonObject.put("idType", idType);
					jsonObject.put("idNumber", idNumber);
					jsonObject.put("pinCode", pincode);
					jsonObject.put("documentUrl", documentUrl);
					jsonObject.put("documentUrl1", documentUrl1);
					jsonArray.put(jsonObject);
				}
				// perform program logic here
			}
			userList.put("UserList", jsonArray);
		}
		users.close();

		return userList;
	}
	
	
	public static JSONObject readCsvBulkminKycUpload(String fileName) throws Exception {
		JSONObject userList = new JSONObject();

		String url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKKYC_CORPORATE + fileName;
		S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
		InputStream objectData = object.getObjectContent();
		CsvReader users = new CsvReader(new InputStreamReader(objectData));

		JSONArray jsonArray = new JSONArray();
		if (users.readHeaders()) {
			while (users.readRecord()) {
				String mobile = users.get("mobile");
				String mmUserId = users.get("mmUserId");
				String idType = users.get("idType");
				String idNumber = users.get("idNumber");
				if (!mobile.isEmpty() &&!idType.isEmpty()
						&& !idNumber.isEmpty()) {
					JSONObject jsonObject = new JSONObject();
					
					jsonObject.put("mmUserId", mmUserId);
					jsonObject.put("mobile", mobile);
					jsonObject.put("idType", idType);
					jsonObject.put("idNumber", idNumber);
					jsonArray.put(jsonObject);
				}
				// perform program logic here
			}
			userList.put("UserList", jsonArray);
		}
		users.close();

		return userList;
	}

	public static void downloadFile(String fileName, HttpServletResponse response) {
		OutputStream outputStream = null;
		InputStream objectData = null;
		try {
			String url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKREGISTER_CORPORATE + fileName + ".csv";

			if (fileName != null && fileName.contains("BLKCARDLOAD")) {
				url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKCARDLOAD_CORPORATE + fileName + ".csv";
			} else if (fileName != null && fileName.contains("BLKKYCUPLOAD")) {
				url = URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKKYC_CORPORATE + fileName + ".csv";
			}

			S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
			objectData = object.getObjectContent();
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".csv\"");
			outputStream = response.getOutputStream();
			byte[] buffer = new byte[8 * 1024];
			int bytesRead;
			while ((bytesRead = objectData.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (objectData != null && outputStream != null) {
					outputStream.close();
					objectData.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static JSONObject readCsvBulkCardAssignmentGroup(String fileName) throws Exception {
		JSONObject userList = new JSONObject();

		try {
			String url = URLMetadatas.EWIRE_CSV_KEY_PATH_BULKCARDASSIGN_GROUP + fileName;
			S3Object object = CommonUtil.client.getObject(new GetObjectRequest(URLMetadatas.CSV_BUCKET_NAME, url));
			InputStream objectData = object.getObjectContent();
			CsvReader users = new CsvReader(new InputStreamReader(objectData));
			JSONArray jsonArray = new JSONArray();

			if (users.readHeaders()) {
				while (users.readRecord()) {
					String mobile = users.get("mobile");
					String proxNo = users.get("proxyNumber");

					if (!mobile.isEmpty() && !proxNo.isEmpty()) {
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("mobile", mobile);
						jsonObject.put("proxyNumber", proxNo);

						jsonArray.put(jsonObject);
					}
				}
				userList.put("CardList", jsonArray);
			}
			users.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;

	}

}