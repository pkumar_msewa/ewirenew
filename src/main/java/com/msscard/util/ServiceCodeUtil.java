package com.msscard.util;

public class ServiceCodeUtil {
	
	public static final String SENDMONEY_REGISTEREDUSER = "SMR";

	public static final String SENDMONEY_UNREGISTEREDUSER = "SMU";

	public static final String CASHBACK_REGISTEREDUSER = "SCSR";

	public static final String CASHBACK_UNREGISTEREDUSER = "SCSU";

	public static final String LOADMONEY = "LMC";
	
	public static final String RAZORPAY_SERVICE = "LMS";

	public static final String COMMISSIONTYPE_POST = "POST";

	public static final String COMMISSIONTYPE_PRE = "PRE";

	public static final String WOOHOO_GIFTCARD = "WOOHO";

	public static final String REFERANDEARN = "RNE";

	public static final String BUS = "EMTB";

	public static final String FLIGHT = "EMTF";

	public static final String WALLET_TO_CARDTRANSFER = "WTCT";

}
