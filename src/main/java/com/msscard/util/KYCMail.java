package com.msscard.util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.msscard.entity.EmailLog;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.model.Status;
import com.msscard.repositories.EmailLogRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserRespository;

public class KYCMail implements  MessageSourceAware  {
	private VelocityEngine velocityEngine;
	private MUserRespository mUserRespository;
	private EmailLogRepository emailLogRepository;
	private MMCardRepository mMCardRepository;
	public static final String MAIL_TEMPLATE = "com/msscard/mail/template/";
	public KYCMail(VelocityEngine velocityEngine,MUserRespository mUserRespository,EmailLogRepository emailLogRepository,MMCardRepository mMCardRepository) {
		this.velocityEngine=velocityEngine;
		this.mUserRespository=mUserRespository;
		this.emailLogRepository=emailLogRepository;
		this.mMCardRepository=mMCardRepository;
	}

	public void sendKYCEmail() throws MailSendException {
		 String subject="EKYC Upgradation";
		 String mailTemplate="KYCUpgrade.vm";
		 System.err.println("Inside kyc mail cron");
		try {
		List<MUser> userList=mUserRespository.findAllNONKYCUserList();
		if(userList!=null){
			for (MUser user : userList) {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("tpImage", MailConstants.Path+"/resources/images/tp.png");
							model.put("btImage", MailConstants.Path+"/resources/images/bt.png");
							String email = user.getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MAIL_TEMPLATE + mailTemplate,model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		}}
		} catch (Exception e) {
			e.printStackTrace();
//			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}

}
	
	public void sendOffersMail() throws MailSendException {
		 String subject="CASHIER OFFERS";
		 String mailTemplate="Offers.vm";
		 System.err.println("Inside kyc mail cron");
		try {
		List<MMCards> userList=mMCardRepository.findAllCards();
		if(userList!=null){
			for (MMCards card : userList) {
				MUser user=card.getWallet().getUser();

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("tpImage", MailConstants.Path+"/resources/assets-newui/img/landing/promo.png");
							model.put("btImage", MailConstants.Path+"/resources/User/NewUi/img/logo.svg");
							String email = card.getWallet().getUser().getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MAIL_TEMPLATE + mailTemplate,model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		}}
		} catch (Exception e) {
			e.printStackTrace();
//			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}

}

	
	public static void sendEmail(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Transport.send(message, userId, password);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveLog(String subject, String mailTemplate, MUser user, String sender, Status status) {
		EmailLog email = new EmailLog();
		email.setDestination(user.getUserDetail().getEmail());
		email.setExcutionTime(new Date());
		email.setMailTemplate(mailTemplate);
		email.setStatus(status);
		email.setSender(sender);
		emailLogRepository.save(email);
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}