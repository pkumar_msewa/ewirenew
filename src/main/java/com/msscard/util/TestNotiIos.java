package com.msscard.util;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;

import javapns.*;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

public class TestNotiIos {

	    public static void main(String[] args) {
	        BasicConfigurator.configure();
	        try {
	            PushNotificationPayload payload = PushNotificationPayload.complex();
	            payload.addAlert("tree orange tree orange tree orange tree orangetree orangetree orange");
	            payload.addBadge(1);
	            payload.addSound("default");
	            payload.addCustomDictionary("imageUrl", "https://bit.ly/2NEL5cJ");
	            payload.addCustomDictionary("body", "This is the test message This is the test message This is the test message.........jbsjhdc");
	            System.out.println(payload.toString());
	            System.err.println(payload.getPayloadSize());
	            List < PushedNotification > NOTIFICATIONS = Push.payload(payload, "D:/MSS Cards/madmovecards-web/WebContent/resources/extra/Cashier_p12PushCert.p12", "",true, "a1f7750b5b09ddd9bbb978f4acfdf2e6a740852d4440f4474eb2e93a6db1cf23");
	            for (PushedNotification NOTIFICATION: NOTIFICATIONS) {
	                if (NOTIFICATION.isSuccessful()) {
	                    System.out.println("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
	                        NOTIFICATION.getDevice().getToken());
	                } else {
	                    Exception THEPROBLEM = NOTIFICATION.getException();
	                    THEPROBLEM.printStackTrace();
	                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
	                    if (THEERRORRESPONSE != null) {
	                        System.out.println(THEERRORRESPONSE.getMessage());
	                    }
	                }
	            }
	        } catch (CommunicationException e) {
	            e.printStackTrace();
	        } catch (KeystoreException e) {
	            e.printStackTrace();
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } catch (Exception e) {
				e.printStackTrace();
			}
	    
	    }
}
