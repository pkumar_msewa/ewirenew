package com.msscard.util;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import com.msscards.metadatas.URLMetadatas;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javapns.notification.PushNotificationPayload;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.msscard.app.model.response.AddressResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.model.SendNotificationDTO;

public class CommonUtil {

	/**
	 * Random Number Generator for MObile OTP return 6 digit random number
	 * 
	 * @return
	 */
	private static final double RADIUS_OF_EARTH = 6371.0D;
	public static String ITUNES_LINK = "https://goo.gl/Ou5ZRr";
	public static String PLAY_STORE_LINK = "http://goo.gl/07Kf4u";
	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat DATEFORMATTER = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat DATEFORMATTER1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat DATEFORMATTER3 = new SimpleDateFormat("dd/MM/yy");
	public static final String startTime = " 00:00:00";
	public static final String endTime = " 23:59:59";
	public static final String RandomString = "ARNAB_LAYEK!#%&*01234567890";

	public final static int PAGE = 0;
	public final static int SIZE = 100000;
	public final static String ADMIN_SESSION_ID = "adminSessionId";
	public final static String USER_SESSION_ID = "userSessionId";
	public final static String AGENT_SESSION_ID = "agentSessionId";
	public final static String SUPER_ADMIN_SESSION_ID = "superAdminSessionId";
	public final static String GROUP_SESSION_ID = "groupSessionId";
	public final static String GROUP_ADD_MSG = "groupAddMsg";
	public final static String ALL = "All";
	public final static int PROXYLENGTH = 12;

	public static String generateSixDigitNumericString() {
		int number = (int) (Math.random() * 1000000);
		String formattedNumber = String.format("%06d", number);
		return formattedNumber;
	}

	public static String[] getDefaultDateRanger() {
		String range[] = new String[2];
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_WEEK, -6);
		Date today7 = cal.getTime();
		range[0] = formatter.format(today7);
		range[1] = formatter.format(today);
		return range;
	}
	
	public static Date getTodayDate() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}

	/**
	 * random n digit number generator
	 * 
	 * @param n
	 * @return
	 */

	public static String generateNDigitNumericString(long n) {
		double mul = Math.pow(10, n);
		long result = (long) (Math.random() * mul);
//		long result = 12;
		String number = String.format("%0" + n + "d", result);
//		System.err.println(number);
		return number;
	}

	//
//	public static void main(String... args){
//		generateSixDigitNumericString();
//		generateNDigitNumericString(12);
//	}

	public static String generateNineDigitNumericString() {
		return "" + (int) (Math.random() * 1000000000);
	}

	public static void sleep(long timeInMS) {
		try {
			Thread.sleep(timeInMS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static double calculateDistance(double lat1, double long1, double lat2, double long2) {
		double distance = 0.0D;
		double lat1InRad = Math.toRadians(lat1);
		double lat2InRad = Math.toRadians(lat2);
		double diffInLat = Math.toRadians(lat2 - lat1);
		double diffInLong = Math.toRadians(long2 - long1);
		double a = Math.pow(Math.sin(diffInLat / 2.0D), 2.0D)
				+ Math.cos(lat1InRad) * Math.cos(lat2InRad) * Math.pow(Math.sin(diffInLong / 2.0D), 2.0D);
		distance = RADIUS_OF_EARTH * Math.atan2(Math.sqrt(a), Math.sqrt(1.0D - a));
		return distance;
	}

	public static String[] getDefaultDateRange() {
		String range[] = new String[2];
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -30);
		Date today30 = cal.getTime();
		range[0] = formatter.format(today30)+CommonUtil.endTime;
		range[1] = formatter.format(today)+CommonUtil.startTime;
		System.out.println(range[0] + ":" + range[1]);
		return range;
	}

//		AWS UPLOAD IMAGE

	public static final AWSCredentials credentials = new BasicAWSCredentials(URLMetadatas.AWS_ACCESS_KEY_ID,
			URLMetadatas.AWS_SECRET_ACCESS_KEY);
	public static final AmazonS3 client = new AmazonS3Client(credentials);
	public static final String CSRF_TOKEN = "user_csrf_token";

	public static String uploadImage(MultipartFile file, String contact, String image) {
		Bucket bucketName = getBucket(URLMetadatas.BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(URLMetadatas.BUCKET_NAME);
		}

		String fileKey = contact + "/" + generateFileKey(file, contact, image);
		client.putObject(new PutObjectRequest(URLMetadatas.BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.BUCKET_NAME, fileKey);
	}

	// Upload file

	public static String uploadCsv(MultipartFile file, String merchantId, String fileType) {
		Bucket bucketName = getBucket(URLMetadatas.CSV_BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(URLMetadatas.CSV_BUCKET_NAME);
		}

		String fileKey = fileType + "/" + merchantId + "/" + generateFileKeyForCsv(file, merchantId, fileType);
		client.putObject(new PutObjectRequest(URLMetadatas.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.CSV_BUCKET_NAME, fileKey);
	}

	// upload corporate logo

	public static String uploadCorporateLogo(MultipartFile file, String corporateName, String fileType) {
		Bucket bucketName = getBucket(URLMetadatas.CSV_BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(URLMetadatas.CSV_BUCKET_NAME);
		}

		String fileKey = fileType + "/" + corporateName + "/" + generateFileKeyForCsv(file, corporateName, fileType);
		client.putObject(new PutObjectRequest(URLMetadatas.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.CSV_BUCKET_NAME, fileKey);
	}

	private static Bucket getBucket(String bucketName) {
		Bucket result = null;
		List<Bucket> buckets = client.listBuckets();
		for (Bucket b : buckets) {
			if (b.getName().equalsIgnoreCase(bucketName))
				result = b;
		}
		return result;
	}

	private static String generateFileKey(MultipartFile file, String contact, String image) {
		String[] extension = file.getContentType().split("\\/");

		String systemMillis = String.valueOf(System.currentTimeMillis());
		return contact + "_" + image + systemMillis + "." + extension[1];
	}

	private static File convertFromMultipart(MultipartFile file) {
		File convertedFile = new File(file.getOriginalFilename());
		try {
			file.transferTo(convertedFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertedFile;
	}

	// generate file key for csv

	private static String generateFileKeyForCsv(MultipartFile file, String contact, String image) {
		String[] extension = file.getContentType().split("\\/");

		String systemMillis = String.valueOf(System.currentTimeMillis());
		return contact + "_" + image + systemMillis + "." + extension[1];
	}

	public static String[] getDateRange(String daterange) {
		String range[] = null;
		if (daterange != null) {
			System.err.println(daterange);
			try {
				range = daterange.split("-");
				range[0] = formatter.format(new Date(Long.parseLong(range[0].trim())));
				range[1] = formatter.format(new Date(Long.parseLong(range[1].trim())));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return range;
	}

	public static String uploadImageForUser(MultipartFile file, String contact, String image) {
		if (file.getSize() > 0) {
			Bucket bucketName = getBucket(URLMetadatas.BUCKET_NAME);
			if (bucketName == null) {
				client.createBucket(URLMetadatas.BUCKET_NAME);
			}

			String fileKey = contact + "/" + generateFileKey(file, contact, image);
			client.putObject(new PutObjectRequest(URLMetadatas.BUCKET_NAME, fileKey, convertFromMultipart(file))
					.withCannedAcl(CannedAccessControlList.PublicRead));
			return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.BUCKET_NAME, fileKey);
		}
		return null;
	}

	public static String generateRandomString(String randomString) {
		if (randomString != null && !randomString.isEmpty()) {
			Random rand = new Random();
			StringBuilder res = new StringBuilder();
			for (int i = 0; i < 6; i++) {
				int randIndex = rand.nextInt(randomString.length());
				res.append(randomString.charAt(randIndex));
			}
			return res.toString();
		}
		return null;
	}

	public static void main(String[] args) {
		/*
		 * File file = new File("‪c:\\users\\suchita\\Desktop\\card.png"); DiskFileItem
		 * fileItem = new DiskFileItem("file", "text/plain", false, file.getName(),
		 * (int) file.length() , file.getParentFile()); try {
		 * fileItem.getOutputStream(); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } MultipartFile multipartFile = new
		 * CommonsMultipartFile(fileItem); uploadImage(multipartFile, "8895093779",
		 * "Card");
		 */

		String aToZ = "ARNAB_LAYEK!@#$%&*01234567890";
		String randomStr = generateRandomString(aToZ);
		System.out.println("value>>> " + randomStr);

	}

	public static double commissionEarned(MCommission comm, double amount) {
		double finalComm = 0.0;
		if (comm.isFixed() == true) {
			finalComm = comm.getValue();
		} else {
			finalComm = (amount * comm.getValue()) / 100;
		}
		BigDecimal a = new BigDecimal(String.valueOf(finalComm));
		BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		// System.out.println(roundOff);
		finalComm = roundOff.doubleValue();
		return finalComm;
	}

	// FOR EMAIL CRON TO GET CARD REPORT SUMMARY
	public static Date getPreviousStartDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date date1 = cal.getTime();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

		String d = formatter2.format(date1);
		d = d + " 00:00:00";

		try {
			date1 = formatter1.parse(d);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return date1;
	}

	public static Date getPreviousToDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date date1 = cal.getTime();
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

		String d = formatter2.format(date1);
		d = d + " 23:59:59";

		try {
			date1 = formatter1.parse(d);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return date1;

	}

	public static Date getStartMonthDate() {
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, -1);
		aCalendar.set(Calendar.DATE, 1);
		Calendar cal = setTimeToBeginningOfDay(aCalendar);
		Date firstDateOfPreviousMonth = cal.getTime();
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return firstDateOfPreviousMonth;

	}

	private static Calendar setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	public static Date getMonthToDate() {
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, -1);
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Calendar cal1 = setTimeToEndofDay(aCalendar);
		Date lastDateOfPreviousMonth = cal1.getTime();
		return lastDateOfPreviousMonth;

	}

	private static Calendar setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar;
	}

	// Upload pdf
	public static String uploadPdf(MultipartFile file, String merchantId, String fileType) {
		Bucket bucketName = getBucket(URLMetadatas.CSV_BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(URLMetadatas.CSV_BUCKET_NAME);
		}

		String fileKey = fileType + "/" + merchantId + "/" + generateFileKeyForCsv(file, merchantId, fileType);
		client.putObject(new PutObjectRequest(URLMetadatas.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.CSV_BUCKET_NAME, fileKey);
	}

	/**
	 * UPLOAD FCM
	 */

	public static String uploadFcmImage(MultipartFile file, String merchantId, String fileType) {
		Bucket bucketName = getBucket(URLMetadatas.CSV_BUCKET_NAME);
		if (bucketName == null) {
			client.createBucket(URLMetadatas.CSV_BUCKET_NAME);
		}

		String fileKey = fileType + "/" + merchantId + "/" + generateFileKeyForCsv(file, merchantId, fileType);
		client.putObject(new PutObjectRequest(URLMetadatas.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
				.withCannedAcl(CannedAccessControlList.PublicRead));
		return String.format("https://%s.s3.amazonaws.com/%s", URLMetadatas.CSV_BUCKET_NAME, fileKey);
	}

	// Calculate Payload

	public static boolean calculatePayload(SendNotificationDTO dto) {
		PushNotificationPayload payload = PushNotificationPayload.complex();
		boolean valid = true;
		try {
			payload.addAlert(dto.getTitle());
			payload.addBadge(1);
			payload.addSound("default");
			payload.addCustomDictionary("imageUrl", dto.getShortenedimageUrl());
			payload.addCustomDictionary("body", dto.getMessage());
			System.out.println(payload.toString());
			System.err.println(payload.getPayloadSize());
			if (payload.getPayloadSize() < 256) {
				valid = true;
			} else {
				valid = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return valid;
	}

	public static AddressResponseDTO getAddress(String pincode) {

		AddressResponseDTO resp = new AddressResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource("http://postalpincode.in/api/pincode/" + pincode);
		ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
				.get(ClientResponse.class);
		String strResp = clientResponse.getEntity(String.class);
		try {
			JSONObject respObj = new JSONObject(strResp);
			JSONArray jArray = respObj.getJSONArray("PostOffice");
			if (jArray != null) {
				JSONObject postOfficeObject = jArray.getJSONObject(0);
				String circle = postOfficeObject.getString("Circle");
				String state = postOfficeObject.getString("State");
				String country = postOfficeObject.getString("Country");
				resp.setCity(circle);
				resp.setState(state);
				resp.setCountry(country);
				resp.setCode("S00");
				return resp;
			}
		} catch (org.codehaus.jettison.json.JSONException e) {
			e.printStackTrace();
			resp.setCode("F00");
			return resp;
		}
		resp.setCode("F00");
		return resp;
	}

	public static String generateRoleCode(String role) {

		return RandomStringUtils.random(role.length(), true, false);
	}

	public static String base64Decode(String s) {
		return StringUtils.newStringUtf8(Base64.decodeBase64(s));
	}

	private static String generateFileKey1(MultipartFile file, String contact, String image) {
		String systemMillis = String.valueOf(System.currentTimeMillis());
		return contact + "_" + image + systemMillis + ".csv";
	}

	public static String uploadCsv1(MultipartFile file, String id, String uploadFileType, String path) {
		String fileName = "";
		try {
			fileName = generateFileKey1(file, id, uploadFileType);
			String fileKey = path + fileName;

			client.putObject(URLMetadatas.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		return fileName;

	}

	public static String getProxyNumber(String proxyNo) {
		if (proxyNo != null) {
			StringBuffer zeros = new StringBuffer();
			for (int i = 0; i < CommonUtil.PROXYLENGTH - proxyNo.length(); i++) {
				zeros.append("0");
			}
			return zeros + proxyNo;
		}
		return null;
	}

}
