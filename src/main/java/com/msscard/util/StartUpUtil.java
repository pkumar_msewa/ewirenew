package com.msscard.util;

public class StartUpUtil {

	public static final boolean PRODUCTION = false;

	public static final String CSV_FILE = getCSVFile();

	// Local Server
	private static final String CSV_FILE_UAT = "/home/msewa-user/Ewire_Rupay/ewirenew/WebContent/resources/register/";

	// LIVE EWIRE
	private static final String CSV_FILE_LIVE = "/usr/local/tomcat/webapps/ROOT/resources/register/";

	public static final String KYC = "KYC";
	public static final String SUPER_KYC = "SUPERKYC";
	public static final String NON_KYC = "NONKYC";

	public static final String SUPER_ADMIN = "superadmin@vpayqwik.com";
	public static final String ADMIN = "admin@cashier.com";
	public static final String MMCARD_USER = "matchmove@msspayments.com";

	// For Bus Booking
	public static final String BUS_BOOKING = "easeMyTripBus@msewa.com";
	public static final String BUS_SERVICECODE = "EMTB";

	// IMPS
	public static final String IMPS_SERVICECODE = "IMPS";
	public static final String IMPS = "imps@cashier.com";

	public static final String LMS_SERVICECODE = "LMS";

	public static final String LOADCARD_CORPORATE = "BRCSL";

	private static String getCSVFile() {
		if (PRODUCTION) {
			return CSV_FILE_LIVE;
		} else {
			return CSV_FILE_UAT;
		}
	}

	public static final String PRODUCTCODE_VA = "inewire";

}