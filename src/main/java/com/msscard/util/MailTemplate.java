package com.msscard.util;

public class MailTemplate {
	
	public static String TICKETMAIL_TEMPLATE = "summary.vm";
	
	public static String BILL_PAYMENT = "billpayment.vm";
	
	public static String CHANGE_PASSWORD = "changepassword.vm";
	
	public static String FUND_TRANSFER = "fundtransfer.vm";
	
	public static String LOAD_MONEY = "loadmoney.vm";
	
	public static String PREFUND_REQUEST = "prefundAlert.vm";
	
	public static String MOBILE_RECHARGE = "mobilerecharge.vm";
	
	public static String REGISTRATION_SUCCESS = "registrationsuccess.vm";
	
	public static String SUMMARY_REPORTS = "summaryreport.vm";
	
	public static String BUS_PAYMENT_SUCCESS = "bussuccess.vm";
}
