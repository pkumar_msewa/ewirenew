package com.msscard.util;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class JSONParserUtil {

	public static String getString(JSONObject object, String key) {
		String value = null;
		if(checkKey(object,key)) {
			try {
				value = (String) object.getString(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return value;
	}

	public static JSONArray getArray(JSONObject object, String key) {
		JSONArray value = null;

		try {
			if (checkKey(object, key)) {
				value =  object.getJSONArray(key);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}
	public static long getLong(JSONObject object, String key) {
		long value = 0;
		try {
			value = object.getLong(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}


	public static int getInt(JSONObject object, String key) {
		int value = 0;
		try {
			value = (int) object.getInt(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public static boolean checkKey(JSONObject object, String key) {
		return object.has(key);
	}
	
	public static String convertToJSON(Object obj) {
		String json = "";
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		 try {
			json = ow.writeValueAsString(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static boolean getBoolean(JSONObject object, String key) {
		boolean value = false;

		try {
			if (checkKey(object, key)) {
				value = (boolean) object.getBoolean(key);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}

	public static JSONObject getObject(JSONObject object, String key) {
		JSONObject value = null;

		try {
			if (checkKey(object, key)) {
				value =  object.getJSONObject(key);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}

}
