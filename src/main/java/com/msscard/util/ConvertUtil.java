package com.msscard.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.msscard.entity.MLocationDetails;

import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;

import com.msscards.session.UserSessionDTO;

public class ConvertUtil {

	public static UserDTO convertUser(MUser u) {
		System.err.println("converting to user");
		UserDTO dto = new UserDTO();
		dto.setUserId(String.valueOf(u.getId()));
		dto.setAddress((u.getUserDetail().getAddress() == null) ? "" : u.getUserDetail().getAddress());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setFirstName(u.getUserDetail().getFirstName());
		dto.setMiddleName((u.getUserDetail().getMiddleName() == null) ? "" : u.getUserDetail().getMiddleName());
		dto.setLastName(u.getUserDetail().getLastName());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setMobileStatus(u.getMobileStatus());
		dto.setEmailStatus(u.getEmailStatus());
		dto.setUserType(u.getUserType());
		dto.setUsername(u.getUsername());
		dto.setAuthority(u.getAuthority());
		dto.setDateOfBirth("" + u.getUserDetail().getDateOfBirth());
		dto.setMpin((u.getUserDetail().getMpin() == null) ? "" : u.getUserDetail().getMpin());
		dto.setImage((u.getUserDetail().getImage() == null) ? "" : u.getUserDetail().getImage());
		dto.setEncodedImage((u.getUserDetail().getImageContent() != null)
				? Base64.getEncoder().encodeToString(u.getUserDetail().getImageContent())
				: "");
		dto.setMpinPresent((u.getUserDetail().getMpin() == null) ? false : true);
		dto.setGcmId((u.getGcmId() == null) ? "" : u.getGcmId());
		dto.setDeviceLocked(u.isDeviceLocked());

		MLocationDetails location = u.getUserDetail().getLocation();
		try {
			if (u.getGroupDetails() == null || u.getGroupDetails().getGroupName() == null) {
				dto.setGroupName("NONE");
			} else {
				if (Status.Active.getValue().equals(u.getUserDetail().getGroupStatus())) {
					dto.setGroupName(u.getGroupDetails().getDname());
				} else {
					dto.setGroupName("NONE");
				}
				dto.setImage(u.getGroupDetails() != null ? u.getGroupDetails().getImage() : "");
				dto.setEncodedImage(u.getGroupDetails().getImageContent() != null
						? Base64.getEncoder().encodeToString(u.getGroupDetails().getImageContent())
						: "");
			}
		} catch (Exception e) {
			dto.setGroupName("NONE");
			e.printStackTrace();
		}

		if (location != null) {
			dto.setPinCode(location.getPinCode());
			dto.setCircleName(location.getCircleName());
			dto.setLocality(location.getLocality());
			dto.setDistrictName(location.getDistrictName());
		}
		return dto;
	}

	public static List<UserSessionDTO> convertSessionList(List<UserSession> userSession) {
		List<UserSessionDTO> dtoList = new ArrayList<>();
		for (UserSession session : userSession) {
			dtoList.add(convertSession(session));
		}
		return dtoList;
	}

	public static UserSessionDTO convertSession(UserSession session) {
		SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
		UserSessionDTO dto = new UserSessionDTO();
		dto.setUsername(session.getUser().getUsername());
		dto.setId(session.getId());
		dto.setSessionId(session.getSessionId());
		dto.setUserId(session.getUser().getId());
		dto.setUserType(session.getUser().getUserType());
		dto.setLastRequest(time.format(session.getLastRequest()));
		return dto;
	}
}
