package com.msscard.util;

import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

public class SecurityUtil {

	protected final static Logger logger = LoggerFactory.getLogger(SecurityUtil.class);
	private static final String KEY = "AFRNDUCNBANKUPON";
	private static final String HASH_KEY = generateKey("VIBHANSHU_VYAS");
	private static final String ALGORITHM = "AES";
	private static final String MODE = "CBC";
	private static final String PADDING = "PKCS5Padding";

	private static final String SEPERATOR = "/";
	// C1B4246A70E8C22CE85CA472AB90A3DF
	private static final String PARTNER_CONSTANT_KEY = "VIJAYA_BANK_AFRNDUCNBANKUPON";

	public static String getPartnerConstantKey() {
		return PARTNER_CONSTANT_KEY;
	}
	public static String getKey() {
		return KEY;
	}
	public static void main(String[] args) {
		try {
			String decryptedData = decryptData("HUvnQ9498qHDuxx5G693RcoRhfD3vCk4DFno3UwhgzTpx09RA/SJz3ntkl4+xr0xxdvudVNkKG7i5gY+MXIxqrnjaMMBXSm5qE+hzUuu00IHb3zGMlETS29r0VlV31kN+gyhjmj/X/uWCumhUEV4mSRRkdss8M91ZWTF/VS4xqm0RBr5rwJsohQxTLKiILH2");
			System.err.println(decryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String encryptString(String rawText) {
		SecretKeySpec keySpec = getKeySpec();
		String encryptedData = null;
		try {
			Cipher cipher = getCipherInstance();
			IvParameterSpec ivParameterSpec = getIvParameterSpec();
			cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivParameterSpec);
			byte[] realCipher = cipher.doFinal(rawText.getBytes());
			encryptedData = encodeBase64(realCipher);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		return encryptedData;
	}



	public static String encryptJSON(JSONObject json) {
		SecretKeySpec keySpec = getKeySpec();
		String encryptedData = null;
		try {
			Cipher cipher = getCipherInstance();
			IvParameterSpec ivParameterSpec = getIvParameterSpec();
			cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivParameterSpec);
			String data = convertJSON(json);
			byte[] realCipher = cipher.doFinal(data.getBytes());
			encryptedData = encodeBase64(realCipher);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		return encryptedData;
	}

	public static String decryptData(String data){
		String json = null;
		SecretKeySpec keySpec = getKeySpec();
		try {
			Cipher cipher = getCipherInstance();
			IvParameterSpec ivParameterSpec = getIvParameterSpec();
			cipher.init(Cipher.DECRYPT_MODE,keySpec,ivParameterSpec);
			byte []cipherText = decodeBase64(data);
			byte []rawBytes = cipher.doFinal(cipherText);
			json  = new String(rawBytes);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}

	public static String encodeBase64(byte[] data){
		return Base64.getEncoder().encodeToString(data);
	}

	public static byte[] decodeBase64(String encodedData) throws IOException {
		return Base64.getDecoder().decode(encodedData);
	}
	public static IvParameterSpec getIvParameterSpec() {
		byte []bytesForIv = new byte[16];
		IvParameterSpec ivParameterSpec = new IvParameterSpec(bytesForIv);
		return ivParameterSpec;
	}
	public static Cipher getCipherInstance() throws NoSuchPaddingException, NoSuchAlgorithmException {
		return Cipher.getInstance(ALGORITHM+SEPERATOR+MODE+SEPERATOR+PADDING);
	}

	public static JSONObject convertString(String encryptedData){
		JSONObject json = null;
		try {
			json = new JSONObject(encryptedData);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	public static SecretKeySpec getKeySpec(){
		byte []workingKeyBytes = KEY.getBytes();
		SecretKeySpec keySpec = new SecretKeySpec(workingKeyBytes,ALGORITHM);
		return keySpec;
	}
	public static String convertJSON(JSONObject json) {
		return String.valueOf(json);
	}


	public static String generateKey(String str) {

		String hashKey = "";
		try {
			hashKey = md5(str);
		} catch (Exception e) {
			logger.info("Key Not Generated");
		}
		return hashKey;

	}

	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

	public static String sha1(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("SHA-1");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

	public static String sha512(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("SHA-512");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

	public static boolean isHashMatches(Object obj, String hash) {
		boolean isValid = true;
		ObjectWriter ow = new ObjectMapper().writer();
		try {
			String json = ow.writeValueAsString(obj) + HASH_KEY;
			String encryptedHash = "";
			try {
				encryptedHash = md5(json);
			} catch (Exception e) {
			}
			if (encryptedHash.equals(hash)) {
				isValid = true;
			} else {
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return isValid;
	}
	
	public static boolean isApiKeys(String key){
		boolean isValid=false;
		if(key.equals(KEY)){
			isValid=true;
		}
		return isValid;
		
	}
	public static Object getHash(String string) {
		try {
			   return SecurityUtil.md5(string + HASH_KEY);
			  } catch (Exception e) {
			   return "";
			  }
	}
	
	public static String getHashBus(String string) {
		try {
			   return SecurityUtil.md5(string + HASH_KEY);
			  } catch (Exception e) {
			   return "";
			  }
	}
}