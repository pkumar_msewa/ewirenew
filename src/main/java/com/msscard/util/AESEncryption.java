package com.msscard.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jettison.json.JSONObject;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class AESEncryption {

		private static String algorithm = "AES";
		private static byte[] keyValue = new byte[] 				
				{ 'E', 'W', 'C', 'A', 'w', 'i', 's', 'h', 'R', 'e', 'E', 'R', 'S', 'K', 'e', 'y' };

		/**
		 * It is used to Performs Encryption
		 * 
		 * @param plainText
		 * @return
		 * @throws Exception
		 */
		public static String encrypt(String plainText) throws Exception {
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(algorithm);
			chiper.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = chiper.doFinal(plainText.getBytes());
			String encryptedValue = new BASE64Encoder().encode(encVal);
			return encryptedValue;
		}

		/**
		 * It is used to Perform Decryption
		 * @param encrypted
		 * @return
		 * @throws Exception
		 */
		public static String decrypt(String encrypted) throws Exception {
			String encryptedText = StringEscapeUtils.unescapeJava(encrypted);
			byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
			System.out.println(decordedValue.length);
			// generate key
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(algorithm);
			chiper.init(Cipher.DECRYPT_MODE, key);
			byte[] decValue = chiper.doFinal(decordedValue);
			String decryptedValue = new String(decValue);
			System.err.println("Decrypted request : "+decryptedValue);
			return decryptedValue;
		}

		/**
		 *  Performs decryption generateKey() is used to generate a secret key for AES algorithm
		 *  
		 * @return
		 * @throws Exception
		 */
		private static Key generateKey() throws Exception {
			Key key = new SecretKeySpec(keyValue, algorithm);
			return key;
		}

		// performs encryption & decryption
		public static void main(String[] args) throws Exception {
			JSONObject payload = new JSONObject();
			
//			{"topupType":"Prepaid","serviceProvider":"TGPIN","mobileNo":"8867396207",
//				"amount":"10","area":"KN","sessionId":"B77C6179E580F244464FF2D0B265F3FC"}
			
//			payload.put("sessionId", "401F5096A8F091400D9F90D4A096009D");
//			payload.put("topupType", "Prepaid");
//			payload.put("serviceProvider", "TGPIN");
//			payload.put("mobileNo", "8867396207");
//			payload.put("amount", "10");
			payload.put("area", true);
			String encryptedText = AESEncryption.encrypt(payload.toString());
//			String encryptedText = "0LaDdekySUJqrZIVtCyybZOI26SKhHOI+TdMzGnJA0bjYRspdMr2STdXwz+Zu2lvKZ9iCGf/jUBoCRDTEPZ/5iTJlR/kQVjWx4Q2cO24WEicE6GZQ1hVkb+JE4yW+FmZkqMcVme+Ruo/uzlrxk4TxaCnfDu6DhItFJrmya2mP2cV+hBZC39IDxtyJj3yCmecx6UmebJcFFdh7zMlko0r5w==";
			
//			String encryptedText = AESEncryption.encrypt("10-942AA657EC185CBF26F555A4BB9B1A31");
			String decryptedText = AESEncryption.decrypt(encryptedText);
			System.out.println("Encrypted Text : " + encryptedText);
			System.out.println("Decrypted Text : " + decryptedText);
			
			JSONObject obj = new JSONObject(decryptedText);
			
			if(obj.has("area")){
//				System.err.println("Area : : " + obj.getBoolean("area"));
				System.err.println("Area : : " + obj.getString("area"));
			}
			
//			String ss[] = decryptedText.split("-");
//			System.err.println("Amount " + ss[0]);
//			System.err.println("SessionId " + ss[1]);
		}	
}
