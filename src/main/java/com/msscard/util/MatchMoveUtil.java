package com.msscard.util;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

public class MatchMoveUtil {

	public static final String MATCHMOVE_HOST_URL = getHostUrl();
	public static final String MATCHMOVE_CONSUMER_KEY = getConsumerKey();
	public static final String MATCHMOVE_CONSUMER_SECRET = getConsumerSecret();

	// MM LIVE
	private static final String MATCHMOVE_HOST_URL_LIVE = "https://api.mmvpay.com/inmss/v1";
	private static final String MATCHMOVE_CONSUMER_KEY_LIVE = "rvzP7VjFzNdF5FSxb7kAmSs40r3aqGYa";
	private static final String MATCHMOVE_CONSUMER_SECRET_LIVE = "PYpVBmMaB7mho9eXBFSXA2YaIDmthAjs0OlSwUNdawkxXrjmB3UfLlMqDK1eh160";

	// MM TEST
	private static final String MATCHMOVE_HOST_URL_TEST = "https://beta-api.mmvpay.com/inmss/v1";
	private static final String MATCHMOVE_CONSUMER_KEY_TEST = "UUbXdlrTzjlsPUdezEPvKtKGmKODz5nX";
	private static final String MATCHMOVE_CONSUMER_SECRET_TEST = "FRSxXp9S3e6atfVPed1iQ7iONHidkPNbU96wJBTnl1LcyfblN1xU29cK9LpT53rE";

	public static final String MATCHMOVE_CARDTYPE_VIRUAL = "msewamssmccard";
	public static final String MATCHMOVE_CARDTYPE_PHYSICAL = "msewamssmcpcard";
	public static final String MATCHMOVE_PHYSICALCARD_FEE = "200";
	public static final String MAXIMUM_SEND_MONEY = "1000";
	public static final String MINIMUM_SEND_MONEY = "10";

	private static String getHostUrl() {
		if (StartUpUtil.PRODUCTION) {
			return MATCHMOVE_HOST_URL_LIVE;
		} else {
			return MATCHMOVE_HOST_URL_TEST;
		}
	}

	private static String getConsumerKey() {
		if (StartUpUtil.PRODUCTION) {
			return MATCHMOVE_CONSUMER_KEY_LIVE;
		} else {
			return MATCHMOVE_CONSUMER_KEY_TEST;
		}
	}

	private static String getConsumerSecret() {
		if (StartUpUtil.PRODUCTION) {
			return MATCHMOVE_CONSUMER_SECRET_LIVE;
		} else {
			return MATCHMOVE_CONSUMER_SECRET_TEST;
		}
	}

	public static String getBasicAuthorization() {
		String result = "Basic ";
		String credentials = getConsumerKey() + ":" + getConsumerSecret();
		String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
		result = result + encodedCredentials;
		return result;
	}

}