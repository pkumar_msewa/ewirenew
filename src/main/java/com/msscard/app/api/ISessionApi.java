package com.msscard.app.api;

import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscards.session.UserDetailsWrapper;
import com.msscards.session.UserSessionDTO;

public interface ISessionApi {

	void registerNewSession(String sessionId, UserDetailsWrapper principal);

	void removeSession(String tokenKey);

	UserSession getUserSession(String sessionId);
	
	UserSession getActiveUserSession(String sessionId);

	boolean getUserOnlineStatus(MUser user);

	void refreshSession(String sessionId);

	List<UserSession> getAllUserSession(long userId, boolean includeExpiredSessions);

	void expireSession(String sessionId);

	boolean checkActiveSession(MUser user);

	long countActiveSessions();

	Page<MUser> findOnlineUsers(Pageable page);

	List<MUser> findOnlineUsers();

	List<MUser> findOnlineUsersBetween(Date start,Date from);

	Page<UserSession> findActiveSessions(Pageable page);

	void registerNewSession(String sessionId, MUser principal);

	List<UserSessionDTO> getAllActiveUser();

	void clearAllSessionForUser(MUser user);

}
