package com.msscard.app.api;


import java.util.List;

import org.springframework.mail.MailSendException;

import com.msscard.app.model.request.TicketDetailsDTO;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.Status;
import com.msscard.model.SummaryDto;

public interface IMailSenderApi {
	
	void dailyUpdates(String subject, String mailTemplate, List<SummaryDto> obj1,String heading,String fileName);

	void sendBusTicketMail(String subject, String mailTemplate, MUser user, MTransaction transaction,
			TicketDetailsDTO additionalInfo) throws MailSendException;

	void saveLog(String subject, String mailTemplate, MUser user, String sender, Status status);

	void sendBillPayment(String subject, String mailTemplate, MUser user, MTransaction transaction);

	void sendChangePassword(String subject, String mailTemplate, MUser user) throws MailSendException;

	void sendFundTransfer(String subject, String mailTemplate, MUser user, MUser receipient,
			SendMoneyDetails sendMoneyDetails) throws MailSendException;

	void sendLoadMoney(String subject, String mailTemplate, MUser user, MTransaction senderTransaction)
			throws MailSendException;

	void sendMobileRecharge(String subject, String mailTemplate, MUser user, MTransaction senderTransaction)
			throws MailSendException;

	void sendRegistrationSuccess(String subject, String mailTemplate, MUser user) throws MailSendException;

	void sendPrefundAlert(String subject, String mailTemplate, MUser user, String amount,String clientName ,String transactionRefNo)
			throws MailSendException;

}
