package com.msscard.app.api;

import javax.servlet.http.HttpServletRequest;

import com.msscard.model.Role;

public interface IAuthenticationApi {

	String getAuthorityFromSession(String session , Role role);

	String getAuthorityFromSessionRequest(String sessionId, HttpServletRequest request);

//	UserDetailsResponse getUserDetailsFromSession(String session);
}
