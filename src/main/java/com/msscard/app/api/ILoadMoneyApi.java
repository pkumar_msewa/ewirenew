package com.msscard.app.api;

import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.TransactionRedirectResponse;
import com.msscard.app.model.response.TransactionVerificationResponse;
import com.msscard.app.model.response.UPIResponseDTO;
import com.msscard.entity.MTransaction;

public interface ILoadMoneyApi {

	MTransaction initiateTransaction(String username, double amount, String upiService);

	TransactionVerificationResponse verifyRazorPay(TransactionInitiateRequest dto);

	TransactionRedirectResponse redirectResponse(TransactionInitiateRequest initiateRequest);

	UPIResponseDTO checkUPIStatus(String transactionId, String merchantId);

	MTransaction initiateUPITransaction(String username, double parseDouble, String upiService, boolean b);

	
}
