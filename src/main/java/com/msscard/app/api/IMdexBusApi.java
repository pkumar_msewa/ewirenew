package com.msscard.app.api;

import com.msscard.app.model.request.BusAvailableTripListRequest;
import com.msscard.app.model.request.BusBookTicketRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.MDEXBusAppResponse;
import com.msscard.app.model.response.TopupAppResponse;
import com.msscard.entity.MUser;
import com.msscard.model.BusSeatDetailRequest;
import com.msscard.model.GetSeatDetailsResp;
import com.msscard.model.TransactionIdDTO;

public interface IMdexBusApi {

	CommonResponse getTransactionId(TransactionIdDTO dto);
	
	CommonResponse saveDetailsInDB(TransactionIdDTO request, CommonResponse result, MUser user);

	MDEXBusAppResponse bookTicket(BusBookTicketRequest dto);

	TopupAppResponse initiatePayment(BusBookTicketRequest dto, MUser user);

	CommonResponse confirmBusPayment(MDEXBusAppResponse dto, BusBookTicketRequest res1, MUser user);

	CommonResponse getAllBookTickets(String sessionId, MUser u, CommonResponse result);

	CommonResponse getAllCityList(CommonResponse result);

	CommonResponse setCityList();

	GetSeatDetailsResp getBusSeatDetails(BusSeatDetailRequest req);

	BusSaveSeatDetailsResponse saveSeatDetails(BusSaveSeatDetailsRequest dto, BusSaveSeatDetailsResponse result,
			MUser user);

	CommonResponse cancelBookedTicket(IsCancellableReq dto);

	CommonResponse getAvailableTripsForBus(BusAvailableTripListRequest request, CommonResponse resp);

	ResponseDTO getSingleTicketTravellerDetails(String emtTxnId);

	ResponseDTO isCancellable(IsCancellableReq dto);

	void setUpi();

	CommonResponse cancellInitiRequest(String transactionrefno);

	

	

	
}
