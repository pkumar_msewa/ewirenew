package com.msscard.app.api;

import com.msscard.app.model.request.TicketDetailsDTO;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;

public interface ISMSSenderApi {

	void sendUserSMS(SMSAccount smsAccount, String smsTemplate, MUser user,String additionalInfo);

	void sendAnotherUserSMS(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo);

	void sendTransactionSMS(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			MTransaction transaction);
	
	void sendAgentForgetPasswordSMS(SMSAccount smsAccount, String smsTemplate, MUser user,String additionalInfo);

	void sendUserSMSCashback(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			double amount);

	void sendTransactional(SMSAccount smsAccount, String destination, String smsMessage);

	void sendGroupSMS(SMSAccount smsAccount, String smsTemplate, GroupDetails gd, MUser user, String additionalInfo);

	void sendGroupCreateSMS(SMSAccount smsAccount, String smsTemplate, GroupDetails gd, String additionalInfo);

	void sendUserSMSForAdmin(SMSAccount smsAccount, String smsTemplate, String additionalInfo, String otp);

	void sendUserSMSDonation(SMSAccount smsAccount, String smsTemplate, String additionalInfo);

	void sendUserGroupSms(String template, String user);

	void sendUserSMSFund(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			MUser receipient);

	void sendUserSMSTopup(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo, String amount);

	void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, MUser user, MTransaction transaction,
			TicketDetailsDTO additionalInfo);

}
