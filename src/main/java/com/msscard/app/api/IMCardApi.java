package com.msscard.app.api;

import com.msscard.app.model.request.MMCardFetchRequest;
import com.msscard.app.model.response.MMCardFetchResponse;
import com.msscard.entity.MUser;

public interface IMCardApi {

	MMCardFetchResponse generateCard(MUser user);
	MMCardFetchResponse fetchCardDetails(MMCardFetchRequest dto);
	
}
