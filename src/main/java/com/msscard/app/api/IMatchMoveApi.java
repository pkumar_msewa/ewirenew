package com.msscard.app.api;

import java.util.Date;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;

public interface IMatchMoveApi {

	ResponseDTO createUserOnMM(MUser request);

	WalletResponse createWallet(MatchMoveCreateCardRequest request);

	WalletResponse assignVirtualCard(MUser mUser);

	WalletResponse inquireCard(MatchMoveCreateCardRequest request);

	ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request);

	WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request);

	ResponseDTO deActivateCards(MatchMoveCreateCardRequest request);

	ResponseDTO reActivateCard(MatchMoveCreateCardRequest request);

	WalletResponse assignPhysicalCard(MUser mUser, String proxy_number);

	ResponseDTO activationPhysicalCard(String activationCode, MUser user);

	WalletResponse transferFundsToMMCard(MUser mUser, String amount, String cardId);

	WalletResponse confirmFundsToMMWallet(MUser mUser, String amount, String authRetrievalNo);

	UserKycResponse confirmKyc(UserKycRequest request);

	UserKycResponse setImagesForKyc(UserKycRequest request);

	UserKycResponse setIdDetails(UserKycRequest request);

	UserKycResponse kycUserMM(UserKycRequest request);

	double getBalance(MUser request);

	UserKycResponse tempConfirmKyc(MUser user);

	UserKycResponse tempSetImagesForKyc(MUser user);

	UserKycResponse tempSetIdDetails(MUser user);

	UserKycResponse tempKycUserMM(MUser user);

	UserKycResponse debitFromMMWalletToCard(String email, String mobile, String cardId, String amount);

	UserKycResponse tempKycStatusApproval(MUser user);

	UserKycResponse getTransactions(MUser userRequest);

	UserKycResponse debitFromCard(String email, String mobile, String cardId, String amount, String message);

	MMCards findCardByCardId(String cardId);

	UserKycResponse resetPins(MUser userRequest);

	MMCards findCardByUserAndStatus(MUser user);

	UserKycResponse getUsers(String email, String mobile);

	UserKycResponse getConsumers();

	WalletResponse walletToWalletTransfer(String amount, MUser user, String cardId, MUser recipient);

	WalletResponse acknowledgeFundTransfer(String amount, String transactionId, String recipient);

	WalletResponse transferFundFromCardToWallet(String amount, MUser user, String cardId, MUser reciever);

	UserKycResponse getTransactions(MUser userRequest, String page);

	WalletResponse cancelFundTransfer(MUser recipient, String amount, String id);

	ResponseDTO activateMMPhysicalCard(String cardId, String activationCode);

	double myBalance(MUser userMob);

	ResponseDTO getBalanceExp(MUser request);

	UserKycResponse deductFromCardConfirmation(MUser user, String amount, String message);

	WalletResponse getFundTransfer(MUser recipient);

	UserKycResponse getTransactionsCustom(MUser userRequest, String page);

	UserKycResponse getTransactionsCustomAdmin(MUser userRequest);

	UserKycResponse updateEmail(MUser userRequest, String email);

	UserKycResponse checkKYCStatus(MUser userRequest);

	UserKycResponse kycUserMMCorporate(MKycDetail request);

	UserKycResponse setIdDetailsCorporate(MKycDetail request);

	UserKycResponse setImagesForKyc2(MKycDetail request);

	UserKycResponse setImagesForKyc1(MKycDetail request);

	UserKycResponse tempConfirmKycCorporate(MKycDetail detail);

	UserKycResponse setPIN(MUser detail, String pin);

	WalletResponse fetchWalletMMOnLogin(MatchMoveCreateCardRequest request, MUser u);

	Double getBalanceApp(MUser request);

	WalletResponse transferFundFromCardToWalletDonation(String amount, MUser user, String cardId, String recipient);

	WalletResponse fetchWalletMMOld(MatchMoveCreateCardRequest request);

	UserKycResponse debitFromCardInHouse(MUser user, String cardId, String amount, String message);

	Double getBalanceCus(MUser request);

	ResponseDTO reActivateCardCorp(MatchMoveCreateCardRequest request);

	UserKycResponse debitFromCardCorp(String email, String mobile, String cardId, String amount);

	WalletResponse transferFundsToMMCardTemp(MUser mUser, String amount, String cardId);

	UserKycResponse getUsersOath(String email, String mobile);

	UserKycResponse debitFromCardFingoole(MUser user, String cardId, String amount, String message);

	Double getBalanceAppWallet(MUser request);

	UserKycResponse debitFromWalletToPool(MatchMoveWallet wallet, String amount, String message);

	WalletResponse initiateLoadFundsToMMWalletUpi(MUser mUser, String amount, String transactionRefNo, String upiid);

	UserKycResponse setPINConfirmReset(MUser detail, String pin, Date dob);

	WalletResponse moveToCardFromWallet(MUser mUser, String amount, String cardId);

	WalletResponse initiateLoadFundsToMMWalletCorp(MUser mUser, String amount);

	WalletResponse refundToWalletFromCustomAdmin(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String ipAdrr);

	WalletResponse initiateLoadFundsToMMWalletPG(MUser mUser, String amount, String transactionRefNo, String upiid);

	WalletResponse reversalToWalletForRechargeFailure(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String desc);

	WalletResponse initiateLoadFundsToMMWalletTopup(MUser mUser, String amount);

	WalletResponse initiateLoadFundsToMMWalletRazor(MUser mUser, String amount);

	ResponseDTO corpCreateUserOnMM(MUser usermob);

	ResponseDTO corpCreateWalletOnMM(MUser user);

	WalletResponse refundDebitTxns(String txnId, MUser user);

	WalletResponse assignPhysicalCardFromGroup(String mobile, String proxy_number);
}