package com.msscard.app.api;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.imps.model.ImpsRequest;
import com.imps.model.ImpsResponse;
import com.msscard.app.model.request.AllTransactionRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.PGStatusCheckDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.CashBackDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.Status;
import com.msscard.model.TransactionListDTO;

public interface ITransactionApi {

	long getTransactionCountForAdmin();

	Double getTransactionAmountForAdmin();

	List<MTransaction> findTransactions(AllTransactionRequest transRequest);

	MTransaction initiatePhyCardTransaction(TransactionalRequest request);

	void successLoadMoney(String transactionRefNo, String paymentId);

	void failedLoadMoney(String transactionRefNo);

	double getMonthlyCreditTransationTotalAmount(MPQAccountDetails account);

	double getDailyCreditTransationTotalAmount(MPQAccountDetails account);

	void doPromocodeTransaction(MUser use, String loadAmount);

	void doPromocodeFailedTransaction(MUser use, String loadAmount);

	MTransaction failedLoadMoneyUPI(String transactionRefNo);

	MTransaction successLoadMoneyUPI(String transactionRefNo, String paymentId, String payerVA, String payerName,
			String payerMobile);

	Double getTransactionAmount();

	void doPrefundTransaction(MUser user, String loadAmount);

	MTransaction initiateMdexTransactionCredit(TransactionInitiateRequest request);

	MTransaction initiateMdexTransactionDebit(TransactionInitiateRequest request);

	MTransaction initiateUPITransaction(TransactionalRequest request);

	ResponseDTO initiateLoadCardTransaction(RequestDTO cardRequest, MUser agent, MService service);

	void updateLoadCardByCorporate(String txnId, String status);

	MTransaction dobulkcreditTransaction(MUser user, String loadAmount);

	MTransaction dobulkDebitTransaction(MUser corporate, String loadAmount);

	ResponseDTO initiateP2PFundTransfer(double amount, String string, MService service, String transactionRefNo,
			MUser user, String imps, MCommission commission, double netCommissionValue, ImpsRequest ebsRequest);

	void successImpsPayment(String transactionRefNo, MCommission commission, double netCommissionValue, MUser user,
			ImpsResponse dto);

	void failedImpsPayment(String transactionRefNo, String string, String string2);

	void updateTransactionStatus(String transactionId, String referenceNo, Status success, String string);

	Map<Double, String> getCreditTransactionForBarGraphMonthWise(MService service);

	MTransaction cashBackTransaction(CashBackDTO request);

	MTransaction getTransactionByRefNo(String string);

	ResponseDTO initiateLoadCardTransactionCorporate(RequestDTO request, MUser agent, MService service);

	List<TransactionListDTO> getTransactionByAccounts(List<MTransaction> loadMoneyTxn, MUser mUser);

	double getDailyDebitTransationTotalAmount(MPQAccountDetails mpqAccountDetails);

	MTransaction initiateFingooleTransaction(TransactionalRequest request);

	MTransaction initiateMdexTransactionDebitnew(TransactionInitiateRequest request);

	MTransaction dobulkDebitTransactionCorp(MUser corporate, String loadAmount);

	MTransaction dobulkDebitTransactionCorpFailed(MUser corporate, String loadAmount);

	MTransaction initiateMdexTransactionDebitnewBus(TransactionInitiateRequest request);

	CommonResponse cancelBusTicket(String transactionRefNo, double refundAmount, String ticketPnr,
			double cancellationCharges);

	MTransaction getTransactionByRetReferNo(String paymentId);

	void failedBusPayment(String transactionRefNo);

	Date getLastTranasactionTimeStamp(MPQAccountDetails account, boolean res);

	Date getLastTransactionTimeStampByStatus(MPQAccountDetails account, Status status);

	MTransaction getLastTransactionDetails(Date from, MPQAccountDetails account, boolean res);

	PGStatusCheckDTO checkPGStatus(String originalTxnNo, String retreivalRefNo);

	MTransaction successLoadMoneyCustom(String transactionRefNo, String paymentId);

	void failedLoadMoneyCustom(String transactionRefNo, String retreivalRefNo);

	ResponseDTO initiateLoadCardTransactionCorporate(RequestDTO cardRequest, MUser agent, MService service,
			MUser cardUser);

	void updateLoadCardByCorporate(String txnId, String value, String authRetrivalNo);

}
