package com.msscard.app.api;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;

import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.LoginResponse;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.BusTicket;
import com.msscard.entity.Donation;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.PartnerDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PromoCode;
import com.msscard.entity.PromoCodeRequest;
import com.msscard.entity.VersionLogs;
import com.msscard.model.ApiVersionDTO;
import com.msscard.model.DonationDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.MPinChangeDTO;
import com.msscard.model.MpinDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;

public interface IUserApi {

	void saveUser(RegisterDTO dto) throws Exception;

	MUser findByUserName(String name);

	void changePasswordRequest(MUser u);

	boolean checkMobileToken(String key, String mobileNumber);

	boolean resendMobileTokenNew(String username);
	
	UserDTO getUserById(Long id);

	int updateGcmId(String gcmId, String username);

	void requestNewLoginDeviceForSuperAdmin(MUser user);

	void requestNewLoginDevice(MUser user);

	boolean isValidLoginToken(String otp, MUser user);

	void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	int getMpinAttempts();

	int getLoginAttempts();

	void updateUserAuthority(String authority, long id);

	String handleLoginFailure(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	long getMCardCount();

	List<MMCards> findAllVirtualCards();


	List<MTransaction> findAllCardTransactioons(DateDTO dto);

	MUser findByUserAccount(MPQAccountDetails account);

	MMCards findCardByUser(MUser usera);

	boolean changePassword(ChangePasswordRequest dto);

	boolean changeProfilePassword(ChangePasswordRequest dto);

	void saveCustomUser(RegisterDTO dto) throws Exception;
	
	void saveFullDetails(RegisterDTO dto) throws Exception;

	List<MKycDetail> findAllCorporateAgent();

	List<MTransaction> findSingleCorporateTransactioons(DateDTO dto);

	ResponseDTO saveBulkUser(RegisterDTO dto);

	List<MBulkRegister> findLast10Register();

	void upgradeAccount(UpgradeAccountDTO dto);

	List<MKycDetail> findAllKycRequest();

	void updateKycRequest(UpgradeAccountDTO dto);

	double monthlyLoadMoneyTransactionTotal(MUser senderUser);

	List<MUser> findAllUser();

	List<PhysicalCardDetails> findAllPhysicalCardRequest();

	long gettotalPhysicalCard();

	long getTotalActiveUser();

	long getTotalLoadMoney();

	ResponseDTO savePromoCode(PromoCodeDTO dto);

	List<PromoCode> fetchPromoCodes();

	List<MService> fetchServices();

	List<PromoCodeRequest> fetchPromoCodeRequest();

	boolean saveUserFromAdmin(RegisterDTO dto);

	List<MMCards> findAllPhysicalCards();

	List<MMCards> findAllCards();

	List<VersionLogs> findAllVersion();

	ResponseDTO updateVersionStatus(String id, String status);

	List<MUser> findAllActiveUser();

	ResponseDTO Addversion(ApiVersionDTO dto);

	List<PhysicalCardDetails> findAllPhysicalCardRequestList(String string);

	ResponseDTO updatePhysicalCardStatus(String id, String status, String string);

	List<MMCards> getListByDateCards(Date fromDate, Date toDate, boolean flag);

	Page<MMCards> findCardByPage(PagingDTO dto, boolean flag);

	List<MUser> findAllKYCUser();
	boolean setNewMpin(MpinDTO dto);

	Page<MUser> findAllUser(PagingDTO page);

	Page<MUser> findAllActiveUser(PagingDTO page);

	Page<MUser> findAllKYCUser(PagingDTO page);

	Page<MKycDetail> findAllKycRequest(PagingDTO page);

	Page<PhysicalCardDetails> findAllPhysicalCardRequestUser(PagingDTO page);

	Page<PhysicalCardDetails> findAllPhysicalCardRequestOnStatusList(PagingDTO page);

	MKycDetail findKYCREquestByUser(MUser us);

	List<PromoCodeRequest> fetchPromoCodeRequestWithStatus();

	Page<MKycDetail> findAllKycRequestRejected(PagingDTO dto);

	void saveUserCorpUser(RegisterDTO dto) throws Exception;

	List<BulkRegister> findAllCorporateUser(String id);

	boolean saveUserFromCorporate(RegisterDTO dto);

	boolean saveCorporatePartner(RegisterDTO dto);

	boolean changeCurrentMpin(MPinChangeDTO dto);

	String handleMpinFailure(HttpServletRequest request, String loginUsername);

	boolean deleteCurrentMpin(String username);

	boolean changePasswordCorporate(MatchMoveCreateCardRequest dto);

	ResponseDTO updateKycRequestCorp(UpgradeAccountDTO dto);

	ResponseDTO upgradeAccountCorp(UpgradeAccountDTO dto);

	List<GroupDetails> getGroupDetails();

	List<MUser> groupAddRequestByContact(UserDataRequestDTO dto);

	LoginResponse loginGroup(LoginDTO login, HttpServletRequest request, HttpServletResponse response);

	CommonResponse createGroup(GroupDetailDTO dto, CommonResponse result);

	List<MUser> groupAddRequest(UserDataRequestDTO dto);

	List<GroupDetails> getGroupDetailsAdmin();

	List<Donation> getDonationDetails();

	CommonResponse acceptGroupAddRequest(String contactNo, CommonResponse result);

	Page<MUser> findUserBasedOnGroup(PagingDTO dto);

	void sendBulkSMS();

	void sendSingleSMS(String mobile);

	List<MUser> groupRejectedUserByContact(UserDataRequestDTO dto);

	List<MUser> groupAcceptedUserByContact(UserDataRequestDTO dto);

	List<MUser> groupAcceptedUser(UserDataRequestDTO dto);

	List<MUser> groupRejectedUser(UserDataRequestDTO dto);

	List<Donation> addDonationList(UserDataRequestDTO dto);

	ResponseDTO saveDonatee(GroupDetailDTO dto);

	List<DonationDTO> donationListBasedOnGroup(UserDataRequestDTO dto);

	boolean resendMobileTokenDeviceBinding(String username);

	boolean resendMobileTokenDeviceBindingGroup(String username);

	MMCards findByUserWallet(MUser u);
	String inactivePartener(MUser partner);

	MUser getPartnerById(long parseLong);

	List<MPQAccountDetails> getUserAccounts(PartnerDetails partner);

	PartnerDetails getPartnerDetails(long parseLong);

	MMCards findByUserWalletPhysical(MUser u);

	MMCards findByUserWalletVirtual(MUser u);

	void sendGroupSMS(String groupName, String text);

	void sendSingleGroupSMS(String mobile, String text);

	Page<MKycDetail> findUpdatedKycList(PagingDTO dto);

	MKycDetail findKYCRequestByUserUpdated(MUser us);

	GroupDetails getGroupDetails(String groupName);

	List<BusTicket> getBusDetailsForAdminByDate(Date from, Date to);

	boolean changePasswordAdmin(MatchMoveCreateCardRequest dto);

	MKycDetail findKYCRequestByUserUpdatedRejectedKyc(MUser us);

	Page<MUser> findAllUserBasedOnBlock(PagingDTO dto);

	List<MUser> getSingleUserGroupRequest(String contact);

	CommonResponse deleteUserFromGroup(PagingDTO page, CommonResponse result);

	List<MUser> getDeletedUser(UserDataRequestDTO dto);

	ResponseDTO changeUserGroup(PagingDTO dto);

	ResponseDTO loadCardOtp();

	List<GroupDetails> getGroupListGet(PagingDTO dto);

	List<GroupDetails> getSingleGroupList(PagingDTO dto);

	List<GroupDetails> getGroupListPost(PagingDTO dto);

	CommonResponse rejectGroupRequest(PagingDTO dto, CommonResponse result);

	List<MUser> groupRejectedUserPost(UserDataRequestDTO dto);

	List<MUser> groupAcceptedUserPost(UserDataRequestDTO dto);

	List<MUser> groupRejectedUserByContactPost(UserDataRequestDTO dto);

	List<MUser> groupAddRequestByContactGet(UserDataRequestDTO dto);

	List<MUser> acceptedUserByContactPostGroup(UserDataRequestDTO dto);

	long getCountGroupRequest(String contact);

	long getCountGroupAccept(String contact);

	long getCountGroupReject(String contact);

	List<MUser> getRequestUser(String contact);

	List<MUser> getAcceptedUser(String contact);

	List<MUser> getRejectedUser(String contact);

	List<MUser> addGroupRequestGet(UserDataRequestDTO dto);

	List<MUser> addGroupRequestpost(UserDataRequestDTO dto);

	List<Donation> addDonationListPost(UserDataRequestDTO dto);

	MUser saveCorpUser(RegisterDTO registerDTO);

	List<MUser> changeRequestListGet(UserDataRequestDTO requestDTO);

	List<MUser> changeRequestListPost(UserDataRequestDTO requestDTO);

	List<GroupBulkRegister> getgroupBulkRegisterFailList(String email);

	List<GroupBulkRegister> getgroupBulkRegisterFailListPost(UserDataRequestDTO dto);

	List<BulkCardAssignmentGroup> getgroupBulkCarFailList(String email);

	List<BulkCardAssignmentGroup> getgroupBulkCarFailListPost(UserDataRequestDTO dto);

	List<GroupBulkKyc> getgroupBulkKycFailList(String email);

	List<GroupBulkKyc> getgroupBulkLKycFailListPost(UserDataRequestDTO dto);

	List<GroupBulkRegister> getsingleuserfailbulkreg(String mobileNoId, String email);

	MUser saveGroupUser(RegisterDTO registerDTO);

}
