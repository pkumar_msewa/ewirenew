package com.msscard.app.api;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MUser;

public interface CorporateMatchMoveApi {

	ResponseDTO createUserOnMM(MUser request);

	WalletResponse createWallet(MatchMoveCreateCardRequest request);

	WalletResponse assignVirtualCard(MUser mUser);

	UserKycResponse getUsers(String email, String mobile);

	ResponseDTO activateMMPhysicalCard(String cardId, String activationCode);

	ResponseDTO activationPhysicalCard(String activationCode, MUser user);

}
