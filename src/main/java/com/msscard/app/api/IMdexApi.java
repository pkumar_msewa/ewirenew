package com.msscard.app.api;

import com.msscard.app.model.request.BusGetTxnIdRequest;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.OperatorCircleResponse;
import com.msscard.app.model.request.TopupPlansList;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.CancelTicketResp;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.FingooleResponse;
import com.msscard.app.model.response.MDEXBusAppDTO;
import com.msscard.app.model.response.MDEXBusAppResponse;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.model.FingooleRegisterDTO;

public interface IMdexApi {
	
	MdexTransactionResponseDTO doPrepaid(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO doPostpaid(MdexTransactionRequestDTO request);
    
	MdexTransactionResponseDTO doInsurance(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO dodTH(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO doElectricity(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO doLandLine(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO doGas(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO doDataCard(MdexTransactionRequestDTO request);
	
	MdexTransactionResponseDTO checkStatus(MdexTransactionRequestDTO request);
	
	//MdexTransactionRequestDTO  doBusTickets(MdexTransactionResponseDTO request);
	
	//CommonResponse getAvailableTripsForBus(BusAvailableTripListRequest dto, CommonResponse result);

	//CommonResponse getBusSeatDetails(BusSeatDetailsRequest req, CommonResponse result);

	CommonResponse saveBusSeatDetails(BusGetTxnIdRequest dto, CommonResponse result);

    MDEXBusAppResponse processBusPayment(MDEXBusAppDTO mdexBusAppDTO);
    
    CancelTicketResp cancelBusTicket(IsCancellableReq dto);

	MdexTransactionResponseDTO doBusPayment(MdexTransactionRequestDTO request);

	FingooleResponse fingooleRegister(FingooleRegisterDTO request);

	FingooleResponse batchNumber(FingooleRegisterDTO request);

	FingooleResponse getServices();

	FingooleResponse fingooleService(FingooleRegisterDTO request);

	OperatorCircleResponse getOperatorAndCircles(TransactionInitiateRequest req);

	TopupPlansList getTelcoBasedOnMobile(TransactionInitiateRequest req);

	TopupPlansList getPlans(MdexTransactionRequestDTO req);

	TopupPlansList dueAmount(MdexTransactionRequestDTO req);

	TopupPlansList getMnpLookUp(TransactionInitiateRequest req);

	TopupPlansList getBrowsePlans(TransactionInitiateRequest req);
}
