package com.msscard.app.api.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexBusApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.model.request.BusAvailableTripListRequest;
import com.msscard.app.model.request.BusBookTicketRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.request.BusTicketDetailsDTO;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.TicketDetailsDTO;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CancelTicketResp;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.MDEXBusAppResponse;
import com.msscard.app.model.response.TopupAppResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.BusCityList;
import com.msscard.entity.BusResponseOfMdex;
import com.msscard.entity.BusTicket;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.TravellerDetails;
import com.msscard.entity.UpiPay;
import com.msscard.model.AvailableTripsDTO;
import com.msscard.model.BoardingPointsDTO;
import com.msscard.model.BusSaveSeatDTO;
import com.msscard.model.BusSeatDetailRequest;
import com.msscard.model.BusTicketResp;
import com.msscard.model.DroppingPointsDTO;
import com.msscard.model.GetAllAvailableTripsDTO;
import com.msscard.model.GetSeatDetailsResp;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionIdDTO;
import com.msscard.model.error.CommonError;
import com.msscard.repositories.BusCityListRepository;
import com.msscard.repositories.BusJsonRepository;
import com.msscard.repositories.BusTicketRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.TravelUserDetailRepository;
import com.msscard.repositories.UpiPayRepository;
import com.msscard.util.MailTemplate;
import com.msscard.util.SecurityUtil;
import com.msscard.util.ServiceCodeUtil;
import com.msscard.validation.TransactionTypeValidation;
import com.razorpay.constants.MdexConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class MdexBusApiImpl implements IMdexBusApi{
	
	private final BusJsonRepository busJsonRepository;
	private final BusTicketRepository busTicketRepository;
	private final TravelUserDetailRepository travelUserDetailRepository;
	private final MTransactionRepository mTransactionRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository mMCardRepository;
	private final MServiceRepository mServiceRepository;
	private final TransactionTypeValidation transactionTypeValidation;
	private final ITransactionApi transactionApi;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final BusCityListRepository busCityListRepository;
	private final UpiPayRepository upiPayRepository;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;
	
	public final SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
	public final SimpleDateFormat formater1 = new SimpleDateFormat("dd-MM-yyyy");
	
	public MdexBusApiImpl(BusJsonRepository busJsonRepository, BusTicketRepository busTicketRepository,
			TravelUserDetailRepository travelUserDetailRepository, MTransactionRepository mTransactionRepository,
			IMatchMoveApi matchMoveApi, MMCardRepository mMCardRepository, MServiceRepository mServiceRepository,
			TransactionTypeValidation transactionTypeValidation, ITransactionApi transactionApi,
			MPQAccountDetailRepository mPQAccountDetailRepository, BusCityListRepository busCityListRepository,
			UpiPayRepository upiPayRepository, IMailSenderApi mailSenderApi, ISMSSenderApi smsSenderApi) {
		this.busJsonRepository = busJsonRepository;
		this.busTicketRepository = busTicketRepository;
		this.travelUserDetailRepository = travelUserDetailRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.matchMoveApi = matchMoveApi;
		this.mMCardRepository = mMCardRepository;
		this.mServiceRepository = mServiceRepository;
		this.transactionTypeValidation = transactionTypeValidation;
		this.transactionApi = transactionApi;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.busCityListRepository = busCityListRepository;
		this.upiPayRepository = upiPayRepository;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
	}
	
	@Override
	public CommonResponse getAllCityList(CommonResponse result) {
		try {
			List<BusCityList> busCityList = busCityListRepository.getAllCityList();

			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Get All City List");
			result.setSuccess(true);
			result.setDetails(busCityList);	
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;				
	}
	
	@Override
	public void setUpi() {
		UpiPay pay = new UpiPay();
		pay.setUsername("udkrtnfg");
		pay.setPassword("30869142");
		pay.setUpikeys("UPILOADE");
		upiPayRepository.save(pay);
	}
	
	@Override
	public CommonResponse setCityList() {
		CommonResponse resp = new CommonResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource("https://mdex.msewa.com/api/travel/bus/citylist");
			ClientResponse response = webResource.header("Authorization", MdexConstants.getMdexBasicAuthorization()).get(ClientResponse.class);
			if(response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					System.err.println(strResponse);
					JSONObject jobj = new JSONObject(strResponse);
					if(jobj != null) {
						JSONArray array = jobj.getJSONArray("details");	
						for(int i = 0; i < array.length(); i++) {
							JSONObject ob = array.getJSONObject(i);
							long id = ob.getLong("cityId");
							BusCityList city = busCityListRepository.getcity(id);
							if(city == null) {
								city =	new BusCityList();
								city.setCityId(ob.getLong("cityId"));
								city.setCityName(ob.getString("cityName"));
								busCityListRepository.save(city);
							}
						}
						resp.setCode(ResponseStatus.SUCCESS.getValue());
					}					
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	/*@Override
	public CommonResponse getBusSeatDetails(BusSeatDetailRequest req, CommonResponse result) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CashierCards");

			payload.put("busId", req.getBusId());
			payload.put("seater", req.isSeater());
			payload.put("sleeper", req.isSleeper());
			payload.put("engineId", req.getEngineId());
			payload.put("journeyDate", req.getJourneyDate());
			payload.put("bpId", req.getBpId());
			payload.put("dpId", req.getDpId());
			payload.put("bpdpLayout", req.isBpdpLayout());
			payload.put("routeid", req.getRouteid());
			payload.put("searchReq", req.getSearchReq());

			String hash = SecurityUtil.getHashBus(payload.toString());
			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_SEAT_DETAILS);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", hash)
					.post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("Get Seat Details");
					result.setSuccess(true);
					
					JSONObject jobj = new JSONObject(strResponse);
					
					
					result.setDetails(strResponse);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}*/
	
	@Override
	public GetSeatDetailsResp getBusSeatDetails(BusSeatDetailRequest req) {

		GetSeatDetailsResp resp = new GetSeatDetailsResp();
	 try {
		
		JSONObject payload = new JSONObject();
		payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
		payload.put("cllientKey",MdexConstants.MDEX_KEY);
		payload.put("clientToken",MdexConstants.MDEX_TOKEN);
		payload.put("clientApiName", "CashierCards");

		payload.put("busId", req.getBusId());
		payload.put("seater", req.isSeater());
		payload.put("sleeper", req.isSleeper());
		payload.put("engineId", req.getEngineId());
		payload.put("journeyDate", req.getJourneyDate());
		payload.put("bpId", req.getBpId());
		payload.put("dpId", req.getDpId());
		payload.put("bpdpLayout", req.isBpdpLayout());
		payload.put("routeid", req.getRouteid());
		payload.put("searchReq", req.getSearchReq());

		String hash = SecurityUtil.getHashBus(payload.toString());
		Client client = Client.create();
		WebResource webResource = client
				.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_SEAT_DETAILS);
		client.addFilter(new LoggingFilter(System.out));
		ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
				.type(MediaType.APPLICATION_JSON_VALUE).header("hash", hash)
				.post(ClientResponse.class, payload);

		if (response.getStatus() != 200) {
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
		} else {
			String strResponse = response.getEntity(String.class);
			if (strResponse != null) {
			    ObjectMapper mapper = new ObjectMapper();				
				resp=mapper.readValue(strResponse, GetSeatDetailsResp.class);
			} else {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}
		return resp;
	}
	
	@Override
	public BusSaveSeatDetailsResponse saveSeatDetails(BusSaveSeatDetailsRequest dto,
			BusSaveSeatDetailsResponse result, MUser user) {
		try {
			dto.setSeatDetailsId(System.currentTimeMillis() + "");
			result = saveUserSeat(dto, user, result);
		} catch(Exception e) {
			e.printStackTrace();
		}		
		return result;
	}
	
	private BusSaveSeatDetailsResponse saveUserSeat(BusSaveSeatDetailsRequest dto, MUser user,
			BusSaveSeatDetailsResponse result) {
		try {
			BusTicket booking = null;
			if (dto.getTripId() != null && !dto.getTripId().isEmpty() && !"null".equalsIgnoreCase(dto.getTripId())) {
				booking = busTicketRepository.getTicketBytripId(dto.getTripId());
			} else {
				booking = new BusTicket();
				booking.setTripId(dto.getSeatDetailsId());
				booking = saveCommonDetails(booking, dto, user);
			}

			if (booking == null) {
				booking = new BusTicket();
				booking = saveCommonDetails(booking, dto, user);
			}

			booking.setUserMobile(dto.getUserMobile());
			booking.setUserEmail(dto.getUserEmail());
			booking.setSeatHoldId(dto.getSeatHoldId());
			booking.setEmtTxnId(dto.getEmtTxnId());
			booking.setEmtTransactionScreenId(dto.getEmtTransactionScreenId());
			booking.setPriceRecheckTotalFare(String.valueOf(dto.getTotalFare()));
			busTicketRepository.save(booking);

			if (dto.getTravellers() != null) {
				for (int i = 0; i < dto.getTravellers().size(); i++) {
					TravellerDetails travellerDetails = new TravellerDetails();
					travellerDetails.setfName(dto.getTravellers().get(i).getfName());
					travellerDetails.setlName(dto.getTravellers().get(i).getlName());
					travellerDetails.setAge(dto.getTravellers().get(i).getAge());
					travellerDetails.setGender(dto.getTravellers().get(i).getGender());
					travellerDetails.setSeatNo(dto.getTravellers().get(i).getSeatNo());
					travellerDetails.setSeatType(dto.getTravellers().get(i).getSeatType());
					travellerDetails.setFare(dto.getTravellers().get(i).getFare());
					travellerDetails.setBusTicketId(booking);
					travelUserDetailRepository.save(travellerDetails);
				}
			}

			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Seat Details save");
			result.setSuccess(true);
			result.setTripId(dto.getSeatDetailsId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Seat details not saved");
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public CommonResponse getAvailableTripsForBus(BusAvailableTripListRequest request, CommonResponse resp) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CashierCards");
			payload.put("sourceId", request.getSourceId());
			payload.put("destinationId", request.getDestinationId());
			payload.put("date", request.getDate());

			String hash = SecurityUtil.getHashBus(payload.toString());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_TRAVEL_URL+MdexConstants.BUS_GET_ALL_AVAILABLE_TRIPS);
			ClientResponse response = webResource.accept("application/json").type("application/json").header("hash", hash).post(ClientResponse.class, payload.toString());
			
			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {

					System.err.println(strResponse);
					
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("Get All Available Trips");
					resp.setSuccess(true);		
					JSONObject jobj = new JSONObject(strResponse);
					
					GetAllAvailableTripsDTO respo = new GetAllAvailableTripsDTO();
					respo.setCode(jobj.getString("code"));
					respo.setStatus(jobj.getString("status"));
					respo.setMessage(jobj.getString("message"));
					ObjectMapper objectMapper = new ObjectMapper();
					
					String jsonArray = jobj.getJSONArray("droppingPoints").toString();						
					ArrayList<DroppingPointsDTO> drop = objectMapper.readValue(jsonArray, new TypeReference<ArrayList<DroppingPointsDTO>>(){});
					respo.setDroppingPoints(drop);
					
					respo.setMaxPrice(jobj.getString("maxPrice"));
					respo.setSource(jobj.getString("source"));
					respo.setSourceId(jobj.getString("sourceId"));
					respo.setDestination(jobj.getString("destination"));
					respo.setDestinationId(jobj.getString("destinationId"));
					respo.setMinPrice(jobj.getString("minPrice"));
					respo.setTotalTrips(jobj.getString("totalTrips"));
					
					
					String array2 = jobj.getJSONArray("bdPoints").toString();
					ArrayList<BoardingPointsDTO> board = objectMapper.readValue(array2, new TypeReference<ArrayList<BoardingPointsDTO>>(){});
					respo.setBdPoints(board);
					
					
					String array3 = jobj.getJSONArray("availableTripsDTOs").toString();
					ArrayList<AvailableTripsDTO> available = objectMapper.readValue(array3, new TypeReference<ArrayList<AvailableTripsDTO>>(){});
					respo.setAvailableTripsDTOs(available);
					
					resp.setDetails(strResponse);
					resp.setDetails2(respo);
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}	
	
	
	@Override
	public CommonResponse getTransactionId(TransactionIdDTO request) {
		CommonResponse result = new CommonResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_TXN_ID_UPDATED);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", SecurityUtil.getHashBus(request.toString()))
					.post(ClientResponse.class, request.getJson());

			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("Success");
					result.setSuccess(true);
					result.setDetails(strResponse);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public TopupAppResponse initiatePayment(BusBookTicketRequest dto, MUser user) {
		TopupAppResponse result = new TopupAppResponse();
		CommonError er = new CommonError();
		try {
			MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.BUS);
			double balance = matchMoveApi.getBalanceApp(user);
			er = transactionTypeValidation.validateCommissionCharges(service, er, balance, dto.getAmount());
			if(er.isValid()) {				
				TransactionInitiateRequest req = new TransactionInitiateRequest();			
				req.setServiceCode(ServiceCodeUtil.BUS);
				req.setAmount(String.valueOf(dto.getAmount()));
				req.setUser(user);
				req.setAndroid(true);
				MTransaction transaction = transactionApi.initiateMdexTransactionDebitnewBus(req);
				if(transaction != null) {
					String activeCardId=null;
					List<MMCards> cardList=mMCardRepository.getCardsListByUser(user);
					if(cardList!=null && cardList.size()>0){
					for (MMCards mmCards : cardList) {
						if(mmCards.isHasPhysicalCard()){
							if(Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus()) || !mmCards.isBlocked()){
								activeCardId=mmCards.getCardId();
							}
						} else {
							if(Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())  || !mmCards.isBlocked()){
								activeCardId=mmCards.getCardId();
							}
						}
					}
					}else{
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage("No active card found");
						return result;
					}
					UserKycResponse resp = matchMoveApi.debitFromCard(user.getUserDetail().getEmail(), user.getUsername(), activeCardId, transaction.getAmount()+"", "bus ticket book");
					if(resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {						
					
						BusResponseOfMdex mdexresp = busJsonRepository.getTxnBySeatHoldId(dto.getSeatHoldId());
						if (mdexresp != null) {
							mdexresp.setmTransaction(transaction);
							busJsonRepository.save(mdexresp);
						}
		
						BusTicket busTicket = busTicketRepository.getTicketBySeatHoldId(dto.getSeatHoldId());
						if (busTicket != null) {
							busTicket.setTransaction(transaction);
							busTicket.setPriceRecheckTotalFare(dto.getAmount() + "");
							busTicketRepository.save(busTicket);
						}
		
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setCode(ResponseStatus.SUCCESS.getValue());				
						result.setSuccess(true);
						result.setTransactionId(transaction.getTransactionRefNo());
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(resp.getMessage());
						result.setSuccess(false);
						result.setTransactionId(transaction.getTransactionRefNo());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Transaction not initiated");
					result.setSuccess(false);
				}
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(er.getMessage());
						result.setSuccess(false);
					}
				} catch(Exception e) {
					e.printStackTrace();
					result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
				}
				return result;
			}
	
		
	@Override
	public MDEXBusAppResponse bookTicket(BusBookTicketRequest request) {
		MDEXBusAppResponse resp = new MDEXBusAppResponse();
		try {						
			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "Ewire");

			payload.put("pQTxnId", "");
			payload.put("transactionId", request.getTransactionId());
			payload.put("seatHoldId", request.getSeatHoldId());
			payload.put("amount", request.getAmount());

			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_BOOK_TICKET_UPDATED);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", SecurityUtil.getHashBus(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = (String) jobj.get("message");
			System.err.println("book ticket initial response::--> "+strResponse);
			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					System.err.println("in bookticket function mdex bus");
				final boolean isTicket = jobj.getBoolean("ticket");
				final String ticketPnr = jobj.getString("ticketPnr");
				final String operatorPnr = jobj.getString("operatorPnr");

				if (isTicket) {
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage(message);
					resp.setSuccess(true);
					resp.setTicketPnr(ticketPnr);
					resp.setOperatorPnr(operatorPnr);
					resp.setTransactionId("");
					resp.setTicket(isTicket);
					resp.setMdexResponse(strResponse);
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(message);
					resp.setSuccess(false);
				}
			} else {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
				resp.setSuccess(false);
			}
		}
				
	
		} catch(Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}
	
	@Override
	public CommonResponse saveDetailsInDB(TransactionIdDTO request, CommonResponse result, MUser user) {
		BusResponseOfMdex mdexresp = new BusResponseOfMdex();
		try {
			JSONObject getTxnIdResp = new JSONObject(result.getDetails().toString());
			mdexresp.setGetTxnIdResp(getTxnIdResp.getString("error"));
			mdexresp.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			mdexresp.setEmtTxnId(getTxnIdResp.getString("transactionid"));
			busJsonRepository.save(mdexresp);

			BusSaveSeatDetailsRequest req = new BusSaveSeatDetailsRequest();
			req.setEmtTransactionScreenId(getTxnIdResp.getString("emtTransactionScreenId"));
			req.setEmtTxnId(getTxnIdResp.getString("transactionid"));
			req.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			req.setTotalFare(getTxnIdResp.getDouble("totalAmount"));
			req.setTravellers(request.getTravellers());
			req.setTripId(request.getTripId());
			req.setUserEmail(request.getEmailId());
			req.setUserMobile(request.getMobileNo());

			BusSaveSeatDetailsResponse response = new BusSaveSeatDetailsResponse();
			response = saveSeat(req, user, response);

			BusSaveSeatDTO resp = new BusSaveSeatDTO();
			resp.setError(getTxnIdResp.getString("error"));
			resp.setTransactionCreated(getTxnIdResp.getBoolean("transactionCreated"));
			resp.setTransactionid(getTxnIdResp.getString("transactionid"));
			resp.setValidFor(getTxnIdResp.getString("validFor"));
			resp.setTransactionid(getTxnIdResp.getString("transactionid"));
			resp.setPriceRecheckAmt(getTxnIdResp.getString("totalAmount"));
			resp.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			resp.setFareBreak(getTxnIdResp.getString("fareBreak"));
			resp.setPriceRecheck(getTxnIdResp.getBoolean("fareRecheck"));
			resp.setEmtTransactionScreenId(getTxnIdResp.getString("emtTransactionScreenId"));
			result.setDetails(resp);

			if (getTxnIdResp.getBoolean("transactionCreated") == true) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Transaction Initiated");
				result.setSuccess(true);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());

				String error = getTxnIdResp.getString("error");
				if (error != null) {
					if (!"null".equalsIgnoreCase(error)) {
						result.setMessage(error);
					} else {
						result.setMessage("Ticket not booked. Please contact customer care");
					}
				} else {
					result.setMessage("Ticket not booked. Please contact customer care");
				}
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}
	
	private BusSaveSeatDetailsResponse saveSeat(BusSaveSeatDetailsRequest dto, MUser user,
			BusSaveSeatDetailsResponse result) {
		try {
			BusTicket booking = null;
			if (dto.getTripId() != null && !dto.getTripId().isEmpty() && !"null".equalsIgnoreCase(dto.getTripId())) {
				booking = busTicketRepository.getTicketBytripId(dto.getTripId());
			} else {
				booking = new BusTicket();
				booking.setTripId(dto.getSeatDetailsId());
				booking = saveCommonDetails(booking, dto, user);
			}

			if (booking == null) {
				booking = new BusTicket();
				booking = saveCommonDetails(booking, dto, user);
			}

			booking.setUserMobile(dto.getUserMobile());
			booking.setUserEmail(dto.getUserEmail());
			booking.setSeatHoldId(dto.getSeatHoldId());
			booking.setEmtTxnId(dto.getEmtTxnId());
			booking.setEmtTransactionScreenId(dto.getEmtTransactionScreenId());
			booking.setPriceRecheckTotalFare(String.valueOf(dto.getTotalFare()));
			busTicketRepository.save(booking);

			if (dto.getTravellers() != null) {
				for (int i = 0; i < dto.getTravellers().size(); i++) {
					TravellerDetails travellerDetails = new TravellerDetails();
					travellerDetails.setfName(dto.getTravellers().get(i).getfName());
					travellerDetails.setlName(dto.getTravellers().get(i).getlName());
					travellerDetails.setAge(dto.getTravellers().get(i).getAge());
					travellerDetails.setGender(dto.getTravellers().get(i).getGender());
					travellerDetails.setSeatNo(dto.getTravellers().get(i).getSeatNo());
					travellerDetails.setSeatType(dto.getTravellers().get(i).getSeatType());
					travellerDetails.setFare(dto.getTravellers().get(i).getFare());
					travellerDetails.setBusTicketId(booking);
					travelUserDetailRepository.save(travellerDetails);
				}
			}

			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Seat Details save");
			result.setSuccess(true);
			result.setTripId(dto.getSeatDetailsId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Seat details not saved");
			result.setSuccess(false);
		}
		return result;
	}
	
	private BusTicket saveCommonDetails(BusTicket booking, BusSaveSeatDetailsRequest dto, MUser user) {
		booking.setBusId(dto.getBusId());
		booking.setTotalFare(dto.getTotalFare());
		booking.setTransaction(null);
		booking.setStatus(Status.Initiated);
		booking.setArrTime(dto.getArrTime());
		booking.setBusOperator(dto.getTravelName());
		booking.setSource(dto.getSource());
		booking.setDestination(dto.getDestination());

		if (dto.getJourneyDate() != null) {
			try {
				Date d = formater.parse(dto.getJourneyDate().substring(0, 10));
				dto.setJourneyDate(formater1.format(d));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		booking.setJourneyDate(dto.getJourneyDate());
		booking.setDeptTime(dto.getDepTime());
		booking.setBoardTime(dto.getBoardTime());
		booking.setBoardingAddress(dto.getBoardLocation());
		booking.setBoardingId(dto.getBoardId());
		return booking;
	}
	
	@Override
	public CommonResponse confirmBusPayment(MDEXBusAppResponse dto, BusBookTicketRequest res1, MUser user) {
		CommonResponse result = new CommonResponse();
		try {
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(dto.getCode()) && dto.isTicket()) {
				result = successBusPayment(dto, res1, result, user);
			} else {
				failedRechargeBillPayment(dto.getTransactionId(), dto.getMessage(), user);
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void failedRechargeBillPayment(String transactionId, String errorMessage, MUser user) {
		MTransaction senderTransaction = mTransactionRepository.findByTransactionRefNo(transactionId + "D");
		if (senderTransaction != null) {
			
			double senderCurrentBalance = 0;

			if (Status.Initiated.equals(senderTransaction.getStatus())) {
				List<MTransaction> lastTransList = mTransactionRepository.getTotalSuccessTransactions(user.getAccountDetail());
				MTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance = lastTrans.getCurrentBalance();
				}

				senderTransaction.setCurrentBalance(
						BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				senderTransaction.setStatus(Status.Failed);
				senderTransaction.setDebit(true);				
				senderTransaction.setRetrivalReferenceNo(errorMessage);
				senderTransaction.setLastModified(new Date());

				MPQAccountDetails senderAccount = user.getAccountDetail();
				senderAccount.setBalance(
						BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				mPQAccountDetailRepository.save(senderAccount);

				mTransactionRepository.save(senderTransaction);
			}
		}
	}
	
	public CommonResponse successBusPayment(MDEXBusAppResponse dto, BusBookTicketRequest res1,
			CommonResponse result, MUser user) {
		BusResponseOfMdex response = busJsonRepository.getTxnBySeatHoldId(res1.getSeatHoldId());
		MTransaction transaction = mTransactionRepository.findByTransactionRefNoAndStatus(response.getmTransaction().getTransactionRefNo(),
				Status.Initiated);
		if (transaction != null) {
			BusResponseOfMdex mdexresp = busJsonRepository.getTxnBySeatHoldId(res1.getSeatHoldId());
			if (mdexresp != null) {
				mdexresp.setBookingResp(dto.getMdexResponse());
				mdexresp.setmTransaction(transaction);
				busJsonRepository.save(mdexresp);
				BusTicket busTicket = busTicketRepository.getTicketBySeatHoldId(res1.getSeatHoldId());
				if(busTicket != null) {
					busTicket.setTransaction(transaction);					
					busTicketRepository.save(busTicket);
				}				
			}

			CommonResponse resp = successBusPayment(transaction.getTransactionRefNo(), dto.getTicketPnr(), user);
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
				BusTicket ticket = busTicketRepository.getByTransaction(transaction);
				if (ticket != null) {
					ticket.setTicketPnr(dto.getTicketPnr());
					ticket.setOperatorPnr(dto.getOperatorPnr());
					ticket.setStatus(Status.Booked);
					busTicketRepository.save(ticket);

					List<TravellerDetails> travellerList = travelUserDetailRepository.getTicketByBusId(ticket);

					TicketDetailsDTO additionalInfo = new TicketDetailsDTO();
					additionalInfo.setUserMobile(ticket.getUserMobile());
					additionalInfo.setTicketPnr(ticket.getTicketPnr());
					additionalInfo.setEmtTxnId(ticket.getEmtTxnId());
					additionalInfo.setArrTime(ticket.getArrTime());
					additionalInfo.setDeptTime(ticket.getDeptTime());
					additionalInfo.setBoardingAddress(ticket.getBoardingAddress());
					additionalInfo.setBusOperator(ticket.getBusOperator());
					additionalInfo.setDestination(ticket.getDestination());
					additionalInfo.setJourneyDate(ticket.getJourneyDate());
					additionalInfo.setOperatorPnr(ticket.getOperatorPnr());
					additionalInfo.setSource(ticket.getSource());
					additionalInfo.setTotalFare(Double.valueOf(ticket.getPriceRecheckTotalFare()));
					additionalInfo.setUserEmail(ticket.getUserEmail());
					additionalInfo.setBoardTime(ticket.getBoardTime());

					additionalInfo.setTravellerList(travellerList);

					mailSenderApi.sendBusTicketMail("EWire Bus Ticket Booking", MailTemplate.BUS_PAYMENT_SUCCESS,
							user, transaction, additionalInfo);
					
					smsSenderApi.sendBusTicketSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.BUSTICKET_SUCCESS,
							user, transaction, additionalInfo);
					
					BusTicketDetailsDTO bTDdto = new BusTicketDetailsDTO();
					bTDdto.setTicket(dto.isTicket());
					bTDdto.setTicketPnr(dto.getTicketPnr());
					bTDdto.setOperatorPnr(dto.getOperatorPnr());
					bTDdto.setMessage(dto.getMessage());
					
					result.setStatus(resp.getStatus());
					result.setCode(resp.getCode());
					result.setMessage(resp.getMessage());
					result.setSuccess(resp.isSuccess());
					result.setDetails(bTDdto);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Order Details not found");
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(resp.getMessage());
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Transaction  not saved,Try again later");
			result.setSuccess(false);
		}
		return result;
	}
	
	private CommonResponse successBusPayment(String transactionRefNo, String ticketPnr, MUser user) {
		CommonResponse result = new CommonResponse();
		MTransaction senderTransaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo);
		if (senderTransaction != null) {
			String retrieval = System.currentTimeMillis() + "";
			if (Status.Initiated.equals(senderTransaction.getStatus())) {
				
					double balance3 = matchMoveApi.getBalanceApp(user);	
					senderTransaction.setStatus(Status.Success);
					senderTransaction.setSuspicious(false);
					senderTransaction.setCardLoadStatus(Status.Success.getValue());
					senderTransaction.setRetrivalReferenceNo(ticketPnr+"_"+retrieval);					
					senderTransaction.setAuthReferenceNo(ticketPnr+"+"+retrieval);					
					senderTransaction.setLastModified(new Date());	
					senderTransaction.setCurrentBalance(balance3);
					mTransactionRepository.save(senderTransaction);
					
					MPQAccountDetails account = user.getAccountDetail();
					account.setBalance(balance3);
					mPQAccountDetailRepository.save(account);
				
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("Transaction successful.");
					result.setSuccess(true);

			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public CommonResponse getAllBookTickets(String sessionId, MUser user, CommonResponse result) {
		try {
			List<BusTicketResp> allTickets = getAllTickets(user);
			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Book Ticket List");
			result.setDetails(allTickets);
			result.setSuccess(true);			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<BusTicketResp> getAllTickets(MUser user) {
		List<BusTicketResp> list = new ArrayList<BusTicketResp>();
		try {
			MPQAccountDetails accountDetail = user.getAccountDetail();
			List<BusTicket> busTickets = busTicketRepository.getByStatusAndAccount(Status.Initiated, accountDetail);
			for (int i = 0; i < busTickets.size(); i++) {
				BusTicketResp resp = new BusTicketResp();
				BusTicket busTicketDB = busTickets.get(i);
				List<TravellerDetails> travellerDetails2 = travelUserDetailRepository.getTicketByBusId(busTicketDB);
				resp.setBusTicket(busTicketDB);
				resp.setTravellerDetails(travellerDetails2);
				list.add(resp);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public CommonResponse cancelBookedTicket(IsCancellableReq dto) {
		CommonResponse result = new CommonResponse();
		CancelTicketResp resp = cancelBusTicket(dto);

		if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
			System.err.println("txnRefNo "+dto.getvPqTxnId());
			String txnRefNo = dto.getvPqTxnId();
			String a[] = txnRefNo.split("D");
			System.err.println("txnRefNo 1 "+a[0]);
			result = cancelBookedTicket(a[0] + "", resp.getRefundAmount(), resp.getPnrNo(),
					resp.getCancellationCharges());
			result.setDetails(resp);
		} else {
			result.setStatus(resp.getStatus());
			result.setCode(resp.getCode());
			result.setMessage(resp.getMessage());
		}
		return result;
	}
	
	public CommonResponse cancelBookedTicket(String transactionRefNo, double refundedAmt, String pnr, double charges) {
		CommonResponse result = new CommonResponse();
		try {
			System.err.println("transactionRefNo :: "+transactionRefNo);
			CommonResponse response = transactionApi.cancelBusTicket(transactionRefNo, refundedAmt, pnr, charges);

			if (response != null && response.isSuccess()) {		
				System.err.println("success transactionapi ");
				MTransaction transaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo + "D");
				MTransaction newTransaction = (MTransaction) response.getDetails();

				BusTicket busTicket = busTicketRepository.getByTransaction(transaction);
				busTicket.setStatus(Status.Cancelled);
				busTicket.setTransaction(newTransaction);
				busTicket.setLastModified(new Date());
				busTicketRepository.save(busTicket);

				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Bus Ticket Cancel Successfully");
			} else {
				System.err.println("failure transactionapi ");
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Ticket cancel successful. But unable to refund amount");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Ticket cancel successful. But unable to refund amount");
		}
		return result;
	}
	
	public CancelTicketResp cancelBusTicket(IsCancellableReq dto) {
		CancelTicketResp result = new CancelTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CashierCards");
			payload.put("bookId", dto.getBookId());
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			payload.put("canceltype", "1");

			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_CANCEL_TICKET);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", payload.toString())
					.post(ClientResponse.class, payload);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("cancel ticket response :: "+strResponse);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(jObj.getString("code"))) {

						final boolean isCancelRequested = jObj.getBoolean("cancelRequested");
						final double refundAmount = jObj.getDouble("refundAmount");
						final double cancellationCharges = jObj.getDouble("cancellationCharges");
						final boolean cancelStatus = jObj.getBoolean("cancelStatus");
						final String pnrNo = jObj.getString("pnrNo");
						final String operatorPnr = jObj.getString("operatorPnr");
						final String remarks = jObj.getString("remarks");
						
						System.err.println("isCancelRequested "+isCancelRequested);
						System.err.println("refundAmount "+refundAmount);
						System.err.println("cancellationCharges "+cancellationCharges);
						System.err.println("cancelStatus "+cancelStatus);
						System.err.println("pnrNo "+pnrNo);
						System.err.println("operatorPnr "+operatorPnr);
						System.err.println("remarks "+remarks);
						
						if (isCancelRequested) {
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
						}
						result.setMessage(jObj.getString("message"));
						result.setCancelRequested(isCancelRequested);
						result.setRefundAmount(refundAmount);
						result.setCancellationCharges(cancellationCharges);
						result.setCancelStatus(cancelStatus);
						result.setPnrNo(pnrNo);
						result.setOperatorPnr(operatorPnr);
						result.setRemarks(remarks);
						System.err.println("message "+result.getMessage());
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(jObj.getString("message"));
						System.err.println("message "+result.getMessage());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage("Not getting proper response from Mdex");
					System.err.println("message "+result.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public ResponseDTO getSingleTicketTravellerDetails(String emtTxnId) {
		ResponseDTO resp = new ResponseDTO();

		try {
			BusTicket busTicket = busTicketRepository.getTicketByTxnId(emtTxnId);
			List<TravellerDetails> travellerDetails = travelUserDetailRepository.getTicketByBusId(busTicket);
			List<String> seatNo = travelUserDetailRepository.getSeatNosByBusId(busTicket);

			resp.setExtraInfo(busTicket);
			resp.setSeatNo(seatNo);
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(travellerDetails);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());
		} catch (Exception e) {

		}
		return resp;
	}
	
	@Override
	public ResponseDTO isCancellable(IsCancellableReq dto) {

		ResponseDTO resp = new ResponseDTO();
		CancelTicketResp result = new CancelTicketResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", MdexConstants.MDEX_CLIENTIP);
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CashierCards");

			payload.put("bookId", dto.getBookId());
			payload.put("canceltype", "1");
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());

			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_ISCANCELABLE_TICKET);
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", payload.toString())
					.post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {

					JSONObject jObj = new JSONObject(strResponse);

					final boolean isCancellable = jObj.getBoolean("cancellable");
					final double refundAmount = jObj.getDouble("refundAmount");
					final double cancellationCharges = jObj.getDouble("cancellationCharges");
					final String seatNo = jObj.getString("seatNo");
					System.err.println("isCancellable "+isCancellable);
					System.err.println("refundAmount "+refundAmount);
					System.err.println("cancellationCharges "+cancellationCharges);
					System.err.println("seatNo "+seatNo);
					if (isCancellable) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						System.err.println("success is cancellable");
					} else {
						System.err.println("failure is cancellable");
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					result.setMessage("Get Cancellation Details");
					result.setCancellable(isCancellable);
					result.setCancellationCharges(cancellationCharges);
					result.setRefundAmount(refundAmount);
					result.setSeatNumber(seatNo);
					resp.setDetails(result);
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
		}
		return resp;
	}
	
	@Override
	public CommonResponse cancellInitiRequest(String transactionrefno) {
		CommonResponse response = new CommonResponse();
		try {			
			transactionApi.failedBusPayment(transactionrefno);
			response.setCode(ResponseStatus.SUCCESS.getValue());
			response.setStatus(ResponseStatus.SUCCESS.getKey());
			response.setMessage("cancellation successfull");
			response.setSuccess(true);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return response;
		
	}

}
