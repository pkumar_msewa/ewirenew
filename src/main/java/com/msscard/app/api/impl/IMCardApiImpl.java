package com.msscard.app.api.impl;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.msscard.app.api.IMCardApi;
import com.msscard.app.model.request.MMCardFetchRequest;
import com.msscard.app.model.response.MMCardFetchResponse;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.model.ResponseStatus;
import com.msscard.repositories.MMCardRepository;
import com.msscard.util.SecurityUtil;
import com.msscards.metadatas.URLMetadatas;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class IMCardApiImpl implements IMCardApi{

	private final MMCardRepository mMCardRepository;
	
	public IMCardApiImpl(com.msscard.repositories.MMCardRepository mMCardRepository) {
		super();
		this.mMCardRepository = mMCardRepository;
	}

	@Override
	public MMCardFetchResponse generateCard(MUser user) {
		MMCardFetchResponse cardDetails=new MMCardFetchResponse();	

	try {
	MMCards card=mMCardRepository.getVirtualCardsByCardUser(user);
	if(card==null){
		MMCardFetchResponse userResp=createUser(user);
		if(userResp!=null){
			if(userResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
				JSONObject payload=new JSONObject();
				payload.put("cllientKey", URLMetadatas.getClientKey());
				payload.put("clientToken", URLMetadatas.getClientToken());
				payload.put("email", user.getUserDetail().getEmail());
				payload.put("password", SecurityUtil.md5(user.getUsername()));
				Client client=Client.create();
				WebResource webResource=client.resource(URLMetadatas.getBaseUrl()+URLMetadatas.getMmcreateCard());
				ClientResponse clientResponse=webResource.accept("application/json").type("application/json").post(ClientResponse.class,payload);
				String strResponse=clientResponse.getEntity(String.class);
				System.err.println(strResponse);
				if(clientResponse.getStatus()==200){
					if(strResponse!=null){
						JSONObject jsonObject=new JSONObject(strResponse);
						String code=jsonObject.getString("code");
						if(code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
						String walletnumber=jsonObject.getString("walletNumber");
						String expiryDate=jsonObject.getString("expiryDate");
						String cardId=jsonObject.getString("cardId");
						String cvv=jsonObject.getString("cvv");
						MMCards cards=new MMCards();
						cards.setCardId(cardId);
						cards.setHasPhysicalCard(false);
						cards.setStatus("Active");
						mMCardRepository.save(cards);
						cardDetails.setCardNo(walletnumber);
						cardDetails.setCvv(cvv);
						cardDetails.setCardId(cardId);
						cardDetails.setCode(code);
						cardDetails.setExpiryDate(expiryDate);
						cardDetails.setMessage("card created...");
						}
						else{
							cardDetails.setCode(ResponseStatus.FAILURE.getValue());
							cardDetails.setMessage("Failure creating card");
						}
					}else{
						cardDetails.setCode(ResponseStatus.FAILURE.getValue());
						cardDetails.setMessage("Error in getting response");
					}
				}else{
					cardDetails.setCode(ResponseStatus.FAILURE.getValue());
					cardDetails.setMessage("Error response::"+clientResponse.getStatus());
				}
				
			}else{
				cardDetails.setCode(ResponseStatus.FAILURE.getValue());
				cardDetails.setMessage("Error creating user on MM");
			}
		}else{
			cardDetails.setCode(ResponseStatus.FAILURE.getValue());
			cardDetails.setMessage("Error while creating User on MM");
		}
	}else{
		cardDetails.setCode(ResponseStatus.FAILURE.getValue());
		cardDetails.setMessage("User is already registered with a virtual card");
	}
	} catch (Exception e) {
		e.printStackTrace();
		cardDetails.setCode(ResponseStatus.FAILURE.getValue());
		cardDetails.setMessage("Exception Occurred");
	}
		return cardDetails;
	}

	@Override
	public MMCardFetchResponse fetchCardDetails(MMCardFetchRequest dto) {
		MMCardFetchResponse cardResponse=new MMCardFetchResponse();
		try {
		JSONObject payload=new JSONObject();
		payload.put("email", dto.getEmail());
		payload.put("password", SecurityUtil.md5(dto.getMobileNo()));
		payload.put("cardId", dto.getCardId());
		payload.put("cllientKey",URLMetadatas.getClientKey());
		payload.put("clientToken", URLMetadatas.getClientToken());
		System.err.println("Requeststs"+payload.toString());
		Client client=Client.create();
		WebResource webResource=client.resource(URLMetadatas.getBaseUrl()+URLMetadatas.getMmcardFetch());
		ClientResponse clientResponse=webResource.accept("application/json").type("application/json").post(ClientResponse.class,payload);
		String strResponse=clientResponse.getEntity(String.class);
		System.err.println("Response"+strResponse);
		if(clientResponse.getStatus()==200){
			if(strResponse!=null){
				JSONObject jsonObject=new JSONObject(strResponse);
				String code=jsonObject.getString("code");
				if(code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
				String cardId=jsonObject.getString("cardId");
				String accountHolder=jsonObject.getString("holderName");
				String cardNumber=jsonObject.getString("walletNumber");
				String expiryDate=jsonObject.getString("expiryDate");
				String cvv=jsonObject.getString("cvv");
				cardResponse.setCardId(cardId);
				cardResponse.setCardNo(cardNumber);
				cardResponse.setCode(code);
				cardResponse.setCvv(cvv);
				cardResponse.setExpiryDate(expiryDate);
				cardResponse.setMessage("Card details fetched..");
				cardResponse.setCardHolder(accountHolder);
			}else{
				cardResponse.setCode(ResponseStatus.FAILURE.getValue());
				cardResponse.setMessage("Error fetching card");
			}
			}else{
				cardResponse.setCode(ResponseStatus.FAILURE.getValue());
				cardResponse.setMessage("No Response received from server");
			}
		}else{
			cardResponse.setCode(ResponseStatus.FAILURE.getValue());
			cardResponse.setMessage("Error while connection server::"+clientResponse.getStatus());
		}
		} catch (Exception e) {
			e.printStackTrace();
			cardResponse.setCode(ResponseStatus.FAILURE.getValue());
			cardResponse.setMessage("Opps!!Exception Occurred");
		}
		return cardResponse;
	}

	public MMCardFetchResponse createUser(MUser user){
		MMCardFetchResponse cardResp=new MMCardFetchResponse();
		JSONObject payload=new JSONObject();
		try {
			payload.put("email", user.getUserDetail().getEmail());
			payload.put("password", SecurityUtil.md5(user.getUsername()));
			payload.put("firstName", user.getUserDetail().getFirstName());
			payload.put("lastName", user.getUserDetail().getLastName());
			payload.put("mobileCountryCode", "91");
			payload.put("mobile", user.getUsername());
			payload.put("preferredName",
					user.getUserDetail().getFirstName() + " " + user.getUserDetail().getLastName());
			payload.put("cllientKey", URLMetadatas.getClientKey());
			payload.put("clientToken", URLMetadatas.getClientToken());
			Client client = Client.create();
			WebResource webResource = client.resource(URLMetadatas.getBaseUrl() + URLMetadatas.getMmcreateUser());
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = clientResponse.getEntity(String.class);
			System.err.println(strResponse);
			if (clientResponse.getStatus() == 200) {
				if (strResponse != null) {
					JSONObject jobject = new JSONObject(strResponse);
					String code = jobject.getString("code");
					if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						cardResp.setCode(code);
					} else {
						cardResp.setCode(code);
					}
				} else {
					cardResp.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				cardResp.setCode(ResponseStatus.FAILURE.getValue());
			}
		} catch (JSONException e) {
		e.printStackTrace();
		cardResp.setCode(ResponseStatus.FAILURE.getValue());
	} catch (Exception e) {
		e.printStackTrace();
		cardResp.setCode(ResponseStatus.FAILURE.getValue());
	}
		return cardResp;
	}
}
