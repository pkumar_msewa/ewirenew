package com.msscard.app.api.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.msscard.app.api.IEmailApi;
import com.msscard.entity.MOperator;
import com.msscard.model.Status;
import com.msscard.model.SummaryDto;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.util.MailTemplate;
import com.msscards.metadatas.URLMetadatas;

public class EmailApi implements IEmailApi{
	private MailSenderApi mailSenderApi;
	private PhysicalCardDetailRepository physicalCardDetailRepository;
	private MTransactionRepository mTransactionRepository;
	private MUserRespository mUserRepository;
	private MMCardRepository mmCardRepository;
	private MOperatorRepository mOperatorRepository;
	private SendMoneyDetailsRepository sendMoneyDetailsRepository;
	
	public EmailApi(MailSenderApi mailSenderApi,PhysicalCardDetailRepository physicalCardDetailRepository,MTransactionRepository mTransactionRepository,
			MUserRespository mUserRepository,MMCardRepository mmCardRepository,MOperatorRepository mOperatorRepository,SendMoneyDetailsRepository sendMoneyDetailsRepository){
		this.mailSenderApi=mailSenderApi;
		this.physicalCardDetailRepository=physicalCardDetailRepository;
		this.mTransactionRepository=mTransactionRepository;
		this.mUserRepository=mUserRepository;
		this.mmCardRepository=mmCardRepository;
		this.mOperatorRepository=mOperatorRepository;
		this.sendMoneyDetailsRepository=sendMoneyDetailsRepository;
	}

@SuppressWarnings("unused")
public void getSummary(Date startDate, Date endDate,String subject,String heading,String fileName) throws Exception {
		
		System.out.println("start and end date: "+startDate+":"+endDate);
		
		String mailTemplate = MailTemplate.TICKETMAIL_TEMPLATE;
		List<SummaryDto> tlist = new ArrayList<SummaryDto>();
		
		SummaryDto dto = new SummaryDto();
		  //TO GET DEATILS ABOUT LOADMONEY
		
		   Long getUpiPayemnt= mTransactionRepository.getAllUpiPayment(3, Status.Success,startDate,endDate);
		   if(getUpiPayemnt!=null){
			   dto.setUpiLoadMoney(getUpiPayemnt);
		   }else{
			   dto.setUpiLoadMoney(0L);
		   }
		   
		   Long getPaymentGateway= mTransactionRepository.getAllPaymentGateway(1, Status.Success,startDate,endDate);
		   if(getPaymentGateway!=null){
			   dto.setPaymentGatewayLoadMoney(getPaymentGateway);
		   }
		   else{
			   dto.setPaymentGatewayLoadMoney(0L);
		   }
		   
		  //TO GET ALL ACTIVE USERS
		   Long getAllUser= mUserRepository.getAllUser(Status.Active, true,startDate,endDate);
		   if(getAllUser!=null){
			   dto.setNewUser(getAllUser);
		   }else{
			   dto.setNewUser(0L);
		   }
		   
		   
		
		    //TO GET DEATILS OF ALL PHYSICAL CARDS
			Long activePhysicalCards = physicalCardDetailRepository.getCountPhysicalCardActive(Status.Active, startDate, endDate);
			if ( activePhysicalCards!= null) {
				dto.setPhysicalCardActive(activePhysicalCards);
			} else {
				dto.setPhysicalCardActive(0L);
			}
			Long requestedPhysicalCards= physicalCardDetailRepository.getCountPhysicalCardRequestPending(Status.Requested, startDate, endDate);
			if(requestedPhysicalCards!=null){
				dto.setPhysicalCardRequested(requestedPhysicalCards);
			}else{
				dto.setPhysicalCardRequested(0L);
			}
		   //TO GET ALL VIRTUAL CARDS
			Long getAllVirtualCards= mmCardRepository.findVirtualCards(startDate,endDate);
			if(getAllVirtualCards!=null){
				dto.setTotalVirtualCard(getAllVirtualCards);
			}
			else{
				dto.setTotalVirtualCard(0L);
			}
			tlist.add(dto);
			
			//TO GET  AMOUNT OF CARDS
			Double upiPayemntSum= mTransactionRepository.getAllUpiPaymentSum(3, Status.Success,startDate,endDate);
			   if(upiPayemntSum!=null){
				   BigDecimal b = new BigDecimal(upiPayemntSum).setScale(2,BigDecimal.ROUND_HALF_UP);
				   dto.setUpiLoadMoneySum(b.doubleValue());
			   }else{
				   dto.setUpiLoadMoneySum(0.0);
			   }
			   
			   Double paymentGatewaySum= mTransactionRepository.getAllPaymentGatewaySum(1, Status.Success,startDate,endDate);
			   if(paymentGatewaySum!=null){
				   BigDecimal b = new BigDecimal(paymentGatewaySum).setScale(2,BigDecimal.ROUND_HALF_UP);
				   dto.setPayementGatewayLoadMoneySum(b.doubleValue());
			   }
			   else{
				   dto.setPayementGatewayLoadMoneySum(0.0);
			   }
			   
			   Double upiRefundSum= mTransactionRepository.getAllUpiRefundSum(3, Status.Refunded, startDate,endDate);
			   if(upiRefundSum!=null){
				   BigDecimal b = new BigDecimal(upiRefundSum).setScale(2,BigDecimal.ROUND_HALF_UP);
				   dto.setUpiRefundSum(b.doubleValue());
			   }
			   else{
				   dto.setUpiRefundSum(0.0);
			   }
			   //TO GET ALL BILL PAYMENTS AND RECHARGE TRANSACTIONS
			   MOperator operatorName= mOperatorRepository.findOperatorByName("MDEX");
			   Double billPayments= mTransactionRepository.getAllPaymentsByMdex(operatorName, Status.Success, startDate,endDate);
			   if(billPayments!=null){
				   BigDecimal b = new BigDecimal(billPayments).setScale(2,BigDecimal.ROUND_HALF_UP);
				   dto.setPaymentByMdex(b.doubleValue());
			   }
			   else{
				   dto.setPaymentByMdex(0.0);
			   }
			   
			   //GET SEND MONEY DETAILS
			   Double sendMoney= sendMoneyDetailsRepository.getAllSendMoneyDetails(startDate,endDate);
			   if(sendMoney!=null){
				   BigDecimal b = new BigDecimal(sendMoney).setScale(2,BigDecimal.ROUND_HALF_UP);
				   dto.setSendMoneyDetails(b.doubleValue());
			   }
			   else{
				   dto.setSendMoneyDetails(0.0);
			   }
			   
		
		fileName= makeExcelSheet(tlist);
	  mailSenderApi.dailyUpdates(subject, mailTemplate,tlist,heading,fileName);
	}

//For excel sheet
private String makeExcelSheet(List<SummaryDto> dto) {

Calendar cal = Calendar.getInstance();

cal.add(Calendar.DATE, -1);

Date date1 = cal.getTime();

String pattern = "yyyyMMdd";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
String date = simpleDateFormat.format(date1);

String filename = ("Cashier Card Summary "+date+".xls");

try {
	String path= System.getProperty("user.dir");  
    System.out.println(path);
	FileOutputStream fileOut = new FileOutputStream(URLMetadatas.EXCEL_PATH + filename);
	HSSFWorkbook workbook = new HSSFWorkbook();
	HSSFSheet worksheet = workbook.createSheet("Worksheet");
	HSSFRow row = worksheet.createRow((short) 0);
	HSSFCell cellA1 = row.createCell(0);
	cellA1.setCellValue("New User");
	HSSFCellStyle styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellA1.setCellStyle(styleOfCell);

	HSSFCell cellB1 = row.createCell(1);
	cellB1.setCellValue("Total Virtual Cards");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellB1.setCellStyle(styleOfCell);
	
	HSSFCell cellC1 = row.createCell(2);
	cellC1.setCellValue("Active Physical Cards");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellC1.setCellStyle(styleOfCell);
	

	HSSFCell cellE1 = row.createCell(3);
	cellE1.setCellValue("Requested Physical Cards");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellE1.setCellStyle(styleOfCell);
	

	HSSFCell cellF1 = row.createCell(4);
	cellF1.setCellValue("No Of Payment Gateway Load Money");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellF1.setCellStyle(styleOfCell);
	

	HSSFCell cellG1 = row.createCell(5);
	cellG1.setCellValue("No Of UPI Load Money");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellG1.setCellStyle(styleOfCell);
	

	HSSFCell cellH1 = row.createCell(6);
	cellH1.setCellValue("Payment Gateway Load Money");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellH1.setCellStyle(styleOfCell);
	
	HSSFCell cellI1 = row.createCell(7);
	cellI1.setCellValue("UPI Load Money");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellI1.setCellStyle(styleOfCell);
	
	HSSFCell cellJ1 = row.createCell(8);
	cellJ1.setCellValue("UPI Refund");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellJ1.setCellStyle(styleOfCell);
	
	HSSFCell cellK1 = row.createCell(9);
	cellK1.setCellValue("Bill Payments And Recharge Transactions");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellK1.setCellStyle(styleOfCell);
	
	HSSFCell cellp1 = row.createCell(10);
	cellp1.setCellValue("Total Send Money Details");
	styleOfCell = workbook.createCellStyle();
	styleOfCell.setFillForegroundColor(HSSFColor.AQUA.index);
	styleOfCell.setFillPattern(HSSFCellStyle.BORDER_THIN);
	cellp1.setCellStyle(styleOfCell);
	
	
	for (int i = 0; i < dto.size(); i++) {
		HSSFRow rowNew = worksheet.createRow((short) i+1);
		rowNew.createCell(0).setCellValue(dto.get(i).getNewUser());
		rowNew.createCell(1).setCellValue(dto.get(i).getTotalVirtualCard());
		rowNew.createCell(2).setCellValue(dto.get(i).getPhysicalCardActive());
		rowNew.createCell(3).setCellValue(dto.get(i).getPhysicalCardRequested());
		rowNew.createCell(4).setCellValue(dto.get(i).getPaymentGatewayLoadMoney());
		rowNew.createCell(5).setCellValue(dto.get(i).getUpiLoadMoney());
		rowNew.createCell(6).setCellValue(dto.get(i).getPayementGatewayLoadMoneySum());
		rowNew.createCell(7).setCellValue(dto.get(i).getUpiLoadMoneySum());
		rowNew.createCell(8).setCellValue(dto.get(i).getUpiRefundSum());
		rowNew.createCell(9).setCellValue(dto.get(i).getPaymentByMdex());
		rowNew.createCell(10).setCellValue(dto.get(i).getSendMoneyDetails());
		
	}
	workbook.write(fileOut);
	fileOut.flush();
	fileOut.close();
} catch (FileNotFoundException e) {
	e.printStackTrace();
} catch (IOException e) {
	e.printStackTrace();
}
return filename;
}


}


