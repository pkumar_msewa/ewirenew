package com.msscard.app.api.impl;

import com.msscard.app.api.CorporateMatchMoveApi;
import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;
import org.springframework.http.MediaType;

import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CorporateMatchMoveApiImpl implements CorporateMatchMoveApi {

	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mmCardRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MUserRespository userRepository;

	public CorporateMatchMoveApiImpl(MatchMoveWalletRepository matchMoveWalletRepository,
			MMCardRepository mmCardRepository, PhysicalCardDetailRepository physicalCardDetailRepository,
			MUserRespository userRepository) {
		super();
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mmCardRepository = mmCardRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.userRepository = userRepository;
	}

	@Override
	public ResponseDTO createUserOnMM(MUser request) {
		ResponseDTO resp = new ResponseDTO();

		String fName = request.getUserDetail().getFirstName();
		String lName = request.getUserDetail().getLastName();

		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
		userData.add("email", request.getUserDetail().getEmail());
		try {
			userData.add("password", SecurityUtil.md5(request.getUserDetail().getContactNo()));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		userData.add("first_name", fName);
		userData.add("last_name", lName);
		userData.add("mobile_country_code", "91");
		userData.add("mobile", request.getUserDetail().getContactNo());

		if ((fName.length() + lName.length() + 1) > 25) {
			userData.add("preferred_name", fName);
		} else {
			userData.add("preferred_name", fName + " " + lName);
		}

		JSONObject user = null;
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(request.getUserDetail().getEmail());
				cardRequest.setPassword(SecurityUtil.md5(request.getUserDetail().getContactNo()));
				cardRequest.setIdNo(user.getString("id"));
				WalletResponse walletResponse = createWallet(cardRequest);
				System.err.println("wallet response is::" + walletResponse);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
					MatchMoveWallet moveWallet = new MatchMoveWallet();
					moveWallet.setWalletId(walletResponse.getWalletId());
					moveWallet.setWalletNumber(walletResponse.getWalletNumber());
					moveWallet.setUser(request);
					moveWallet.setMmUserId(user.getString("id"));
					matchMoveWalletRepository.save(moveWallet);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("wallet created successfully");
					return resp;
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Creation of wallet failed....");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse createWallet(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", request.getIdNo()).post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				resp.setAvailableAmt(user.getJSONObject("funds").getJSONObject("available").getString("amount"));
				resp.setCard_status(user.getJSONObject("status").getBoolean("is_active"));
				resp.setExpiryDate(date.getString("expiry"));
				resp.setIssueDate(date.getString("issued"));
				resp.setWithholdingAmt(user.getJSONObject("funds").getJSONObject("withholding").getString("amount"));
				resp.setWalletNumber(user.getString("number"));
				resp.setWalletId(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getValue());
			return resp;
		}
	}

	@Override
	public WalletResponse assignVirtualCard(MUser mUser) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());

				userId = userResp.getMmUserId();
			}

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
					.post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {

				JSONObject date = user.getJSONObject("date");
				String walletId = user.getString("id");

				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json")
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);

				@SuppressWarnings("unused")
				String cvv = null;
				System.err.println("cvv request::::" + cvvRequest);
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					wal.setIssueDate(date.getString("issued"));
					MMCards cards = mmCardRepository.getVirtualCardsByCardUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(false);
						virtualCard.setStatus(Status.Active.getValue());
						virtualCard.setWallet(wal);
						MMCards cd = mmCardRepository.save(virtualCard);
						System.err.println("card saved" + cd.getId());

					} else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("You are already assigned with a card.Please Contact Admin for queries");
						return resp;
					}
					matchMoveWalletRepository.save(wal);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("card successfully assigned...");
					return resp;

				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("user wallet not found.");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public ResponseDTO activationPhysicalCard(String activationCode, MUser user) {

		ResponseDTO respDTO = new ResponseDTO();
		MMCards card = mmCardRepository.getPhysicalCardByUser(user);
		PhysicalCardDetails physicalCardDetails = physicalCardDetailRepository.findByCard(card);
		if (physicalCardDetails.getActivationCode().equalsIgnoreCase(activationCode)) {
			ResponseDTO resp = activateMMPhysicalCard(card.getCardId(), activationCode);
			respDTO.setCode(resp.getCode());
			respDTO.setMessage(resp.getMessage());
			return respDTO;
		} else {
			respDTO.setCode(ResponseStatus.FAILURE.getValue());
			respDTO.setMessage("Card Activation failed...Please contact Customer Care");
			return respDTO;
		}
	}

	@Override
	public ResponseDTO activateMMPhysicalCard(String cardId, String activationCode) {
		ResponseDTO result = new ResponseDTO();
		try {
			MMCards card = mmCardRepository.getCardByCardId(cardId);
			if (card != null) {
				MultivaluedMap<String, String> multiMap = new MultivaluedMapImpl();
				multiMap.add("activation_code", activationCode);

				Client client = Client.create();
				WebResource webResource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
				ClientResponse clientResponse = webResource
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", card.getWallet().getMmUserId())
						.header("Content-Type", "application/x-www-form-urlencoded")
						.put(ClientResponse.class, multiMap);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200 && json != null) {
					card.setStatus(Status.Active.getValue());
					mmCardRepository.save(card);
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("Card Activated Successfully");
					return result;
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Activation failed");
					return result;
				}
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Operation Failed due to some exception...");
			return result;
		}
		result.setCode(ResponseStatus.FAILURE.getValue());
		result.setMessage("Card Activation failed");
		return result;

	}

	@Override
	public UserKycResponse getUsers(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MUser muser = userRepository.findByUsername(mobile);
			if (muser != null && muser.isFullyFilled()
					&& Status.Active.getValue().equalsIgnoreCase(muser.getMobileStatus().getValue())) {
				MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(muser);
				if (wallet != null && wallet.getMmUserId() != null && !wallet.getMmUserId().isEmpty()) {
					Client client = Client.create();
					WebResource webResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
					ClientResponse clientResponse = webResource
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-ID", wallet.getMmUserId())
							.header("Content-Type", "application/x-www-form-urlencoded").get(ClientResponse.class);
					String strResponse = clientResponse.getEntity(String.class);
					JSONObject user = new JSONObject(strResponse);
					if (clientResponse.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(user.getString("description"));
						return resp;
					} else {
						resp.setDetails(user.getString("id"));
						resp.setMessage("user details fetched.");
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("user wallet not found.");
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user record not found.");
				return resp;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

}
