package com.msscard.app.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.MediaType;

import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.model.request.BrowsePLansList;
import com.msscard.app.model.request.BusGetTxnIdRequest;
import com.msscard.app.model.request.DueAmountDTO;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.request.MdexPlanPrepaidDetail;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.OperatorCircleResponse;
import com.msscard.app.model.request.PlanPrepaidResponse;
import com.msscard.app.model.request.TopupPlansList;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.CancelTicketResp;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.FingooleResponse;
import com.msscard.app.model.response.MDEXBusAppDTO;
import com.msscard.app.model.response.MDEXBusAppResponse;
import com.msscard.app.model.response.MdexCircleResponse;
import com.msscard.app.model.response.MdexOperatorResponseDTO;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.FingooleServices;
import com.msscard.entity.MMCards;
import com.msscard.entity.MUser;
import com.msscard.model.FingooleRegisterDTO;
import com.msscard.model.FingooleResponseDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.repositories.FingooleServicesRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.util.SecurityUtil;
import com.razorpay.constants.MdexConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class MdexApiImpl implements IMdexApi {

	private final ITransactionApi transactionApi;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository cardRepository;
	private final MUserRespository mUserRespository;
	private final FingooleServicesRepository fingooleServicesRepository;

	public MdexApiImpl(ITransactionApi transactionApi, IMatchMoveApi matchMoveApi, MMCardRepository cardRepository,
			MUserRespository mUserRespository, FingooleServicesRepository fingooleServicesRepository) {
		this.transactionApi = transactionApi;
		this.matchMoveApi = matchMoveApi;
		this.cardRepository = cardRepository;
		this.mUserRespository = mUserRespository;
		this.fingooleServicesRepository = fingooleServicesRepository;
	}

	@Override
	public OperatorCircleResponse getOperatorAndCircles(TransactionInitiateRequest req) {
		OperatorCircleResponse opresponse = new OperatorCircleResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_SERVICES + req.getTopUpType());

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization()).get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			System.err.println("getopcircle response:: " + strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			System.err.println("code and message:: " + code + " " + message);
			if (response.getStatus() != 200) {
				opresponse.setStatus(ResponseStatus.FAILURE.getKey());
				opresponse.setCode(ResponseStatus.FAILURE.getValue());
				opresponse.setMessage(message);
			} else {
				String status = (String) jobj.get("status");
				JSONArray array = jobj.getJSONArray("details");
				System.err.println("status:: " + status);
				List<MdexOperatorResponseDTO> data = new ArrayList<MdexOperatorResponseDTO>();
				for (int i = 0; i < array.length(); i++) {
					JSONObject ob = array.getJSONObject(i);
					MdexOperatorResponseDTO resp = new MdexOperatorResponseDTO();
					String opcode = ob.getString("code");
					String name = ob.getString("name");
					String description = ob.getString("description");
					String serviceTypeCode = ob.getString("serviceTypeCode");
					double minAmount = ob.getDouble("minAmount");
					double maxAmount = ob.getDouble("maxAmount");
					boolean fixed = ob.getBoolean("fixed");
					double value = ob.getDouble("value");
					String type = ob.getString("type");
					String serviceLogo = ob.getString("serviceLogo");
					resp.setCode(opcode);
					resp.setDescription(description);
					resp.setName(name);
					resp.setFixed(fixed);
					resp.setMaxAmount(maxAmount);
					resp.setMinAmount(minAmount);
					resp.setServiceLogo(serviceLogo);
					resp.setServiceType(serviceTypeCode);
					resp.setServiceTypeCode(serviceTypeCode);
					resp.setType(type);
					resp.setValue(value);
					data.add(resp);
				}
				List<MdexCircleResponse> arr = new ArrayList<MdexCircleResponse>();
				if (!(req.getTopUpType().equalsIgnoreCase("landline") || req.getTopUpType().equalsIgnoreCase("gas")
						|| req.getTopUpType().equalsIgnoreCase("insurance")
						|| req.getTopUpType().equalsIgnoreCase("elec")
						|| req.getTopUpType().equalsIgnoreCase("dthbill"))) {
					JSONArray arrays = jobj.getJSONArray("circles");
					if (arrays != null) {
						for (int i = 0; i < arrays.length(); i++) {
							JSONObject object = arrays.getJSONObject(i);
							MdexCircleResponse mcr = new MdexCircleResponse();
							long id = object.getLong("id");
							long created = object.getLong("created");
							long lastModified = object.getLong("lastModified");
							String name = object.getString("name");
							String codem = object.getString("code");
							String sequenceCode = object.getString("sequenceCode");
							boolean news = object.getBoolean("new");
							mcr.setId(id);
							mcr.setCreated(created);
							mcr.setLastModified(lastModified);
							mcr.setName(name);
							mcr.setCode(codem);
							mcr.setSequenceCode(sequenceCode);
							mcr.setNews(news);
							arr.add(mcr);
						}
						opresponse.setCircles(arr);
					} else {
						opresponse.setCircles(null);
					}
				} else {
					opresponse.setCircles(null);
				}
				opresponse.setCode(code);
				opresponse.setStatus(status);
				opresponse.setMessage(message);
				opresponse.setDetails(data);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return opresponse;
	}

	@Override
	public TopupPlansList getTelcoBasedOnMobile(TransactionInitiateRequest req) {
		TopupPlansList list = new TopupPlansList();
		try {
			JSONObject payload = new JSONObject();
			payload.put("mobileNumber", req.getMobileNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.TELCO_OPERATOR);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization())
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			System.err.println("getopcircle response:: " + strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				list.setStatus(ResponseStatus.FAILURE.getKey());
				list.setCode(ResponseStatus.FAILURE.getValue());
				list.setMessage(message);
			} else {
				String status = (String) jobj.get("status");
				String circle = jobj.getString("circles");
				JSONObject detail = jobj.getJSONObject("details");
				String serviceName = detail.getString("serviceName");
				String serviceCode = detail.getString("serviceCode");
				String circleName = detail.getString("circleName");
				String circleCode = detail.getString("circleCode");
				String code1 = detail.getString("code");

				MdexPlanPrepaidDetail detailPlan = new MdexPlanPrepaidDetail();

				detailPlan.setCircleCode(circleCode);
				detailPlan.setCircleName(circleName);
				detailPlan.setCode(code1);
				detailPlan.setServiceCode(serviceCode);
				detailPlan.setServiceName(serviceName);
				detailPlan.setPlans(null);
				list.setCircles(circle);
				list.setCode(code);
				list.setMessage(message);
				list.setStatus(status);
				list.setDetails(detailPlan);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public TopupPlansList getPlans(MdexTransactionRequestDTO req) {
		TopupPlansList list = new TopupPlansList();
		try {
			JSONObject payload = new JSONObject();
			payload.put("circleCode", req.getArea());
			payload.put("serviceProvider", req.getServiceProvider());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.TELCO_PLANS);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization())
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			System.err.println("getopcircle response:: " + strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				list.setStatus(ResponseStatus.FAILURE.getKey());
				list.setCode(ResponseStatus.FAILURE.getValue());
				list.setMessage(message);
			} else {
				String status = (String) jobj.get("status");
				String circle = jobj.getString("circles");
				JSONObject detail = jobj.getJSONObject("details");
				String serviceName = detail.getString("serviceName");
				String serviceCode = detail.getString("serviceCode");
				String circleName = detail.getString("circleName");
				String circleCode = detail.getString("circleCode");
				String code1 = detail.getString("code");
				JSONArray array = detail.getJSONArray("plans");
				MdexPlanPrepaidDetail detailPlan = new MdexPlanPrepaidDetail();
				if (array != null) {
					List<PlanPrepaidResponse> planList = new ArrayList<PlanPrepaidResponse>();
					for (int i = 0; i < array.length(); i++) {
						JSONObject ob = array.getJSONObject(i);
						PlanPrepaidResponse plan = new PlanPrepaidResponse();
						plan.setAmount(ob.getString("amount"));
						plan.setDescription(ob.getString("description"));
						plan.setOperatorCode(ob.getString("operatorCode"));
						plan.setPlanName(ob.getString("planName"));
						plan.setPlanType(ob.getString("planType"));
						plan.setSmsDaakCode(ob.getString("smsDaakCode"));
						plan.setState(ob.getString("state"));
						plan.setValidity(ob.getString("validity"));
						planList.add(plan);
					}
					detailPlan.setPlans(planList);
				} else {
					detailPlan.setPlans(null);
				}
				detailPlan.setCircleCode(circleCode);
				detailPlan.setCircleName(circleName);
				detailPlan.setCode(code1);
				detailPlan.setServiceCode(serviceCode);
				detailPlan.setServiceName(serviceName);
				list.setCircles(circle);
				list.setCode(code);
				list.setMessage(message);
				list.setStatus(status);
				list.setDetails(detailPlan);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public TopupPlansList dueAmount(MdexTransactionRequestDTO req) {
		TopupPlansList list = new TopupPlansList();
		try {
			JSONObject payload = new JSONObject();
			payload.put("accountNumber", req.getAccountNumber());
			payload.put("serviceProvider", req.getServiceProvider());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.DUE_AMOUNT);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization())
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			System.err.println("getopcircle response:: " + strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				list.setStatus(ResponseStatus.FAILURE.getKey());
				list.setCode(ResponseStatus.FAILURE.getValue());
				list.setMessage(message);
			} else {
				String status = jobj.getString("status");
				list.setCode(code);
				list.setMessage(message);
				if (!status.equalsIgnoreCase("Failure")) {
					if (jobj.has("details")) {
						JSONObject detail = jobj.getJSONObject("details");
						if (detail != null) {
							if (detail.has("data")) {
								if (detail.get("data") != null) {
									JSONObject dataObject = detail.getJSONObject("data");

									if (dataObject != null) {

										DueAmountDTO detailPlan = new DueAmountDTO();

										detailPlan.setDueAmount(detail.getString("dueAmount"));
										detailPlan.setDueDate(dataObject.getString("duedate"));
										detailPlan.setCustomerName(dataObject.getString("customername"));
										detailPlan.setBillNumber(dataObject.getString("billnumber"));
										detailPlan.setBillDate(dataObject.getString("billdate"));
										detailPlan.setBillPeriod(dataObject.getString("billperiod"));
										detailPlan.setReferenceNumber(dataObject.getString("referenceNumber"));
										list.setCode(code);
										list.setMessage(message);
										list.setStatus(status);
										list.setDetails(detailPlan);
									}
								}
							}
						}
					}
				} else {
					list.setCode(code);
					list.setMessage(message);
					return list;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public MdexTransactionResponseDTO doPrepaid(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("mobileNo", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("area", request.getArea());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Prepaid");

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("MDEX Response:::" + strResponse);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(ResponseStatus.FAILURE.getValue());

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						final String operatorId = jobj.getString("operatorId");
						resp.setReferenceNo(referenceNo);
						resp.setTransactionId(transactionId);
						resp.setOperatorId(operatorId);
						request.setTransactionId(transactionId);
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(statusResp.getCode())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.SUCCESS.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.FAILURE.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Failed, Status.Pending + "");

						}
						// Success
					} else {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						resp.setReferenceNo(referenceNo);
						resp.setTransactionId(transactionId);
						resp.setSuccess(false);
						resp.setCode(code);
						resp.setStatus(ResponseStatus.FAILURE.getValue());
						resp.setMessage(message);
						resp.setResponse(strResponse);

						transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo, Status.Failed,
								Status.Pending + "");
					}

				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}
		return resp;
	}

	@Override
	public MdexTransactionResponseDTO doPostpaid(MdexTransactionRequestDTO request) {

		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("mobileNo", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("additionalInfo", request.getAdditionalInfo());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Postpaid");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(ResponseStatus.FAILURE.getValue());
				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");

			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						final String operatorId = jobj.getString("operatorId");
						resp.setReferenceNo(referenceNo);
						resp.setTransactionId(transactionId);
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.SUCCESS.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.FAILURE.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					} else {
						resp.setSuccess(false);
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Service unavailable");
						resp.setStatus(ResponseStatus.FAILURE.getValue());
						transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
								Status.Pending + "");

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}
		return resp;

	}

	@Override
	public MdexTransactionResponseDTO doInsurance(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("amount", request.getAmount());
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("transactionId", request.getTransactionId());
			payload.put("policyNumber", request.getPolicyNumber());
			payload.put("policyDate", request.getPolicyDate());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Datacard");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(ResponseStatus.FAILURE.getValue());

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						final String operatorId = jobj.getString("operatorId");
						resp.setReferenceNo(referenceNo);
						resp.setTransactionId(transactionId);
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.SUCCESS.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(ResponseStatus.FAILURE.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							MUser user = mUserRespository.findByUsername(request.getSenderUser());
							MMCards card = cardRepository.getCardByUserAndStatus(user, Status.Active + "");

							WalletResponse walletResponse = matchMoveApi.refundDebitTxns(transactionId1, user);
							String authRefNo = null;
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
								authRefNo = walletResponse.getAuthRetrivalNo();
								WalletResponse cardTransferResposne = matchMoveApi.transferFundsToMMCard(user,
										request.getAmount(), card.getCardId());
								transactionApi.updateTransactionStatus(request.getTransactionId(), authRefNo,
										Status.Failed, cardTransferResposne.getStatus());
							} else {
								transactionApi.updateTransactionStatus(request.getTransactionId(), authRefNo,
										Status.Failed, Status.Pending + "");
							}
						}

					} else {
						resp.setSuccess(false);
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Service unavailable");
						resp.setStatus(ResponseStatus.FAILURE.getValue());

						transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
								Status.Pending + "");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}

		return resp;

	}

	@Override
	public MdexTransactionResponseDTO dodTH(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("dthNo", request.getDthNumber());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("additionalInfo", request.getAdditionalInfo());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/DTH");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus("FAILED");

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							final String operatorId = jobj.getString("operatorId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId);
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(statusResp.getCode());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(Status.Failed.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					}

				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed.getValue());

					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed.getValue());

			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}

		return resp;

	}

	@Override
	public MdexTransactionResponseDTO doElectricity(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("accountNumber", request.getAccountNumber());
			payload.put("billingUnit", request.getBillingUnit());
			payload.put("processingCycle", request.getProcessingCycle());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("additionalInfo", request.getAdditionalInfo());
			payload.put("cityName", request.getCityName());
			payload.put("cycleNumber", request.getCycleNumber());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Electricity");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(Status.Failed.getValue());

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						final String operatorId = jobj.getString("operatorId");
						resp.setReferenceNo(referenceNo);
						resp.setTransactionId(transactionId);
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(statusResp.getCode())) {
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(Status.Success.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(Status.Failed.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					}

				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed.getValue());

					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed.getValue());

			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;

		}
		return resp;

	}

	@Override
	public MdexTransactionResponseDTO doLandLine(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("accountNumber", request.getAccountNumber());
			payload.put("landlineNumber", request.getLandlineNumber());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("additionalInfo", request.getAdditionalInfo());
			payload.put("stdCode", request.getStdCode());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Landline");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus("FAILED");

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(statusResp.getCode())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							final String operatorId = jobj.getString("operatorId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId);
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(Status.Success.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(Status.Failed.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed.getValue());
					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed.getValue());
			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}
		return resp;

	}

	@Override
	public MdexTransactionResponseDTO doGas(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("accountNumber", request.getAccountNumber());
			payload.put("amount", request.getAmount());
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("transactionId", request.getTransactionId());
			payload.put("billGroupNumber", request.getBillGroupNumber());
			payload.put("referenceNumber", request.getReferenceNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Gas");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(Status.Failed.getValue());

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							final String operatorId = jobj.getString("operatorId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId);
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(Status.Success.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(Status.Failed.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					}

				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed.getValue());

					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed.getValue());

			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}

		return resp;
	}

	@Override
	public MdexTransactionResponseDTO doDataCard(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		String referenceNo = null;
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("mobileNo", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Datacard");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(Status.Failed.getValue());

				transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
						Status.Pending + "");
			} else {
				if (strResponse != null && jobj != null) {
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)
							|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						referenceNo = jobj.getString("referenceNo");
						final String transactionId = jobj.getString("transactionId");
						MdexTransactionResponseDTO statusResp = checkStatus(request);
						if (statusResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())
								|| ResponseStatus.PENDING.getValue().equalsIgnoreCase(statusResp.getCode())) {
							final String operatorId = jobj.getString("operatorId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId);
							resp.setOperatorId(operatorId);
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setStatus(Status.Success.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);
							// Success
							transactionApi.updateTransactionStatus(request.getTransactionId(), referenceNo,
									Status.Success, Status.Done + "");
						} else {
							referenceNo = jobj.getString("referenceNo");
							final String transactionId1 = jobj.getString("transactionId");
							resp.setReferenceNo(referenceNo);
							resp.setTransactionId(transactionId1);
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(Status.Failed.getValue());
							resp.setMessage(message);
							resp.setResponse(strResponse);

							transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
									Status.Pending + "");
						}
					}

				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed.getValue());

					transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
							Status.Pending + "");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed.getValue());

			transactionApi.updateTransactionStatus(request.getTransactionId(), null, Status.Failed,
					Status.Pending + "");
			return resp;
		}

		return resp;

	}

	@Override
	public MdexTransactionResponseDTO checkStatus(MdexTransactionRequestDTO request) {
		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("transactionId", request.getTransactionId());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_URL + "ipay/Status");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("key", MdexConstants.MDEX_KEY).header("token", MdexConstants.MDEX_TOKEN)
					.header("hash", SecurityUtil.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(Status.Failed.getValue());
			} else {
				if (strResponse != null && jobj != null) {
					if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						resp.setSuccess(true);
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					} else if (ResponseStatus.PENDING.getValue().equalsIgnoreCase(code)) {
						resp.setCode(ResponseStatus.PENDING.getValue());
						resp.setSuccess(false);
						return resp;
					}

					else {
						resp.setSuccess(false);
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Service unavailable");
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						return resp;

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			return resp;

		}
		return resp;

	}

	// FOR BUS API
	@Override
	public MDEXBusAppResponse processBusPayment(MDEXBusAppDTO request) {

		MDEXBusAppResponse resp = new MDEXBusAppResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CahierCard");
			payload.put("pQTxnId", "");
			payload.put("transactionId", request.getTransactionId());
			payload.put("seatHoldId", request.getSeatHoldId());
			payload.put("amount", request.getAmount());

			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.MDEX_TRAVEL_URL);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", SecurityUtil.getHash(payload.toString()))
					.post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = (String) jobj.get("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					final boolean isTicket = jobj.getBoolean("ticket");
					final String ticketPnr = jobj.getString("ticketPnr");
					final String operatorPnr = jobj.getString("operatorPnr");

					if (isTicket) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage(message);
						resp.setSuccess(true);
						resp.setTicketPnr(ticketPnr);
						resp.setOperatorPnr(operatorPnr);
						resp.setTransactionId("");
						resp.setTicket(isTicket);
						resp.setMdexResponse(strResponse);
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(message);
						resp.setSuccess(false);
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public CommonResponse saveBusSeatDetails(BusGetTxnIdRequest request, CommonResponse result) {

		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_TXN_ID_UPDATED);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", SecurityUtil.getHash(request.toString()))
					.post(ClientResponse.class, request.getJson());

			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("Bus details saved.");
					result.setSuccess(true);
					result.setDetails(strResponse);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public CancelTicketResp cancelBusTicket(IsCancellableReq dto) {
		CancelTicketResp result = new CancelTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("cllientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "Cashiercard");
			payload.put("bookId", dto.getBookId());
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			payload.put("canceltype", "1");

			Client client = Client.create();
			WebResource webResource = client
					.resource(MdexConstants.MDEX_TRAVEL_URL + MdexConstants.BUS_GET_CANCEL_TICKET);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", payload.toString())
					.post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(jObj.getString("code"))) {

						final boolean isCancelRequested = jObj.getBoolean("cancelRequested");
						final double refundAmount = jObj.getDouble("refundAmount");
						final double cancellationCharges = jObj.getDouble("cancellationCharges");
						final boolean cancelStatus = jObj.getBoolean("cancelStatus");
						final String pnrNo = jObj.getString("pnrNo");
						final String operatorPnr = jObj.getString("operatorPnr");
						final String remarks = jObj.getString("remarks");
						if (isCancelRequested) {
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
						}
						result.setMessage(jObj.getString("message"));
						result.setCancelRequested(isCancelRequested);
						result.setRefundAmount(refundAmount);
						result.setCancellationCharges(cancellationCharges);
						result.setCancelStatus(cancelStatus);
						result.setPnrNo(pnrNo);
						result.setOperatorPnr(operatorPnr);
						result.setRemarks(remarks);
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(jObj.getString("message"));
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage("Not getting proper response from Mdex");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public MdexTransactionResponseDTO doBusPayment(MdexTransactionRequestDTO request) {

		MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientKey", MdexConstants.MDEX_KEY);
			payload.put("clientToken", MdexConstants.MDEX_TOKEN);
			payload.put("clientApiName", "CashierCard");

			payload.put("mpQTxnId", "");
			payload.put("transactionId", request.getTransactionId());
			payload.put("seatHoldId", request.getSeatHoldId());
			payload.put("amount", request.getAmount());

			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.MDEX_TRAVEL_URL);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("hash", SecurityUtil.getHash(payload.toString()))
					.post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = (String) jobj.get("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					final boolean isTicket = jobj.getBoolean("ticket");
					final String ticketPnr = jobj.getString("ticketPnr");
					final String operatorPnr = jobj.getString("operatorPnr");

					if (isTicket) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage(message);
						resp.setSuccess(true);
						resp.setTicketPnr(ticketPnr);
						resp.setOperatorPnr(operatorPnr);
						resp.setTransactionId("");
						resp.setTicket(isTicket);
						resp.setMdexResponse(strResponse);
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(message);
						resp.setSuccess(false);
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public FingooleResponse getServices() {
		FingooleResponse resp = new FingooleResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.FINGOOLE_SERVICES);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE)
					.header("Authorization", MdexConstants.getMdexBasicAuthorization()).get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = (String) jobj.get("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					String status = jobj.getString("status");
					String code = jobj.getString("code");
					resp.setCode(code);
					resp.setStatus(status);
					resp.setServiceList(jobj.toString());
					JSONArray array = jobj.getJSONArray("details");
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = array.getJSONObject(i);
						FingooleServices ser = fingooleServicesRepository.getServiceBasedOnCode(obj.getString("code"));
						if (ser == null) {
							FingooleServices fs = new FingooleServices();
							fs.setCode(obj.getString("code"));
							fs.setDescription(obj.getString("description"));
							fs.setName(obj.getString("name"));
							fs.setMaxAmount(1000);
							fs.setMinAmount(1);
							fs.setValue(0);
							fingooleServicesRepository.save(fs);
						}
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public FingooleResponse batchNumber(FingooleRegisterDTO request) {
		FingooleResponse resp = new FingooleResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.FINGOOLE_BATCH);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("key", MdexConstants.MDEX_KEY)
					.header("token", MdexConstants.MDEX_TOKEN).get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = (String) jobj.get("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					String status = jobj.getString("status");
					String code = jobj.getString("code");
					String batch = jobj.getJSONObject("details").getString("batchNo");
					resp.setCode(code);
					resp.setStatus(status);
					resp.setBatch(batch);
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public FingooleResponse fingooleRegister(FingooleRegisterDTO request) {

		FingooleResponse resp = new FingooleResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("name", request.getName());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("noOfDays", request.getNoOfDays());
			payload.put("email", request.getEmail());
			payload.put("batchNumber", request.getBatchNumber());
			payload.put("serviceProvider", request.getServiceProvider());

			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.FINGOOLE_REG);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("key", MdexConstants.MDEX_KEY)
					.header("token", MdexConstants.MDEX_TOKEN).post(ClientResponse.class, payload.toString());

			String strResponse = response.getEntity(String.class);
			System.err.println("mdex fingoole reg response:: " + strResponse);
			JSONObject jobj = new JSONObject(strResponse);
			final String message = jobj.getString("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					String status = jobj.getString("status");
					String code = jobj.getString("code");
					String batch = jobj.getString("batchNo");
					resp.setCode(code);
					resp.setStatus(status);
					resp.setBatch(batch);
					resp.setMessage(message);
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public FingooleResponse fingooleService(FingooleRegisterDTO request) {

		FingooleResponse resp = new FingooleResponse();
		try {
			org.json.JSONObject pay = new org.json.JSONObject();

			pay.put("batchNo", request.getBatchNumber());
			pay.put("transactionId", request.getTransactionRefNo());

			System.err.println("payload process:: " + pay.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(MdexConstants.FINGOOLE_PROCESS);
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE)
					.type(MediaType.APPLICATION_JSON_VALUE).header("key", MdexConstants.MDEX_KEY)
					.header("token", MdexConstants.MDEX_TOKEN).post(ClientResponse.class, pay.toString());

			String strResponse = response.getEntity(String.class);
			System.err.println("mdex fingoole service response:: " + strResponse);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String message = jobj.getString("message");

			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					String status = jobj.getString("status");
					String code = jobj.getString("code");
					String batch = jobj.getString("batchNo");
					ArrayList<FingooleResponseDTO> customer = new ArrayList<FingooleResponseDTO>();
					org.json.JSONArray array = jobj.getJSONArray("details");
					if (array != null) {
						for (int i = 0; i < array.length(); i++) {
							org.json.JSONObject ob = array.getJSONObject(i);
							FingooleResponseDTO fr = new FingooleResponseDTO();
							fr.setCoino(ob.getString("coino"));
							fr.setCoiurl(ob.getString("coiurl"));
							fr.setCustomerName(ob.getString("customerName"));
							fr.setMobileNo(ob.getString("mobileNo"));
							fr.setRefNo(ob.getString("refNo"));
							customer.add(fr);
						}
						resp.setCustomerTransactionList(customer);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setBatch(batch);

				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			resp.setSuccess(false);
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public TopupPlansList getMnpLookUp(TransactionInitiateRequest req) {

		TopupPlansList resp = new TopupPlansList();
		try {

			JSONObject payload = new JSONObject();

			payload.put("accountNumber", req.getAccountNumber());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MdexConstants.MDEX_MNPLOOKUP);

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization())
					.post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);

			System.err.println("MNP response:: " + strResponse);

			final String code = (String) jobj.get("code");
			final String status = (String) jobj.get("status");
			final String message = (String) jobj.get("message");

			System.err.println("code and message:: " + code + " " + message);
			@SuppressWarnings("rawtypes")
			Map map = new HashMap();
			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(message);
			} else {
				String circle = jobj.getJSONObject("details").getString("circle");
				String operator = jobj.getJSONObject("details").getString("operator");

				map.put("circle", circle);
				map.put("operator", operator);
				resp.setDetails(map);
			}

			resp.setCode(code);
			resp.setStatus(status);
			resp.setMessage(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public TopupPlansList getBrowsePlans(TransactionInitiateRequest req) {

		TopupPlansList list = new TopupPlansList();
		try {

			JSONObject payload = new JSONObject();
			payload.put("rechargeType", req.getRechargeType());
			payload.put("serviceProvider", req.getServiceProvider());
			payload.put("circleCode", req.getCircleCode());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));

			WebResource webResource = client.resource(MdexConstants.MDEX_BROWSE_PLANS);

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", MdexConstants.getMdexBasicAuthorization())
					.post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);

			JSONObject jobj = new JSONObject(strResponse);

			System.err.println("Plans response:: " + strResponse);
			final String code = (String) jobj.get("code");
			final String message = (String) jobj.get("message");
			if (response.getStatus() != 200) {
				list.setStatus(ResponseStatus.FAILURE.getKey());
				list.setCode(ResponseStatus.FAILURE.getValue());
				list.setMessage(message);
			} else {
				String status = (String) jobj.get("status");
				JSONArray detail = jobj.getJSONArray("details");
				if (detail != null) {
					List<BrowsePLansList> planList = new ArrayList<BrowsePLansList>();
					for (int i = 0; i < detail.length(); i++) {
						JSONObject ob = detail.getJSONObject(i);
						BrowsePLansList plan = new BrowsePLansList();
						plan.setRecharge_amount(ob.getString("recharge_amount"));
						plan.setRecharge_long_desc(ob.getString("recharge_long_desc"));
						plan.setRecharge_short_desc(ob.getString("recharge_short_desc"));
						plan.setRecharge_talktime(ob.getString("recharge_talktime"));
						plan.setRecharge_type(ob.getString("recharge_type"));
						plan.setRecharge_validity(ob.getString("recharge_validity"));
						planList.add(plan);
					}
					list.setDetails(planList);
				} else {
					list.setDetails(null);
				}
				list.setCode(code);
				list.setStatus(status);
				list.setMessage(message);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
