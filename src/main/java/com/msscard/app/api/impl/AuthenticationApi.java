package com.msscard.app.api.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.ISessionLogApi;
import com.msscard.app.api.IUserApi;
import com.msscard.entity.UserSession;
import com.msscard.model.Role;
import com.msscard.model.UserDTO;
import com.msscard.repositories.UserSessionRepository;

public class AuthenticationApi implements IAuthenticationApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final ISessionLogApi sessionLogApi;

	public AuthenticationApi(IUserApi userApi, UserSessionRepository userSessionRepository, ISessionLogApi sessionLogApi) {
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.sessionLogApi = sessionLogApi;
	}

	@Override
	public String getAuthorityFromSession(String session , Role role) {
		
		UserSession userSession = userSessionRepository.findByActiveSessionId(session);
		if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				return user.getAuthority();
			} 
		return null;
	}
	
	@Override
	public String getAuthorityFromSessionRequest(String sessionId, HttpServletRequest request) {
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			boolean requestValid = sessionLogApi.checkRequest(sessionId, request);
			UserDTO user = null;
			if (requestValid) {
				user = userApi.getUserById(userSession.getUser().getId());
				return user.getAuthority();
			}
		}
		return null;
	}
	
	/*@Override
	public UserDetailsResponse getUserDetailsFromSession(String session) {
		UserDetailsResponse resp = new UserDetailsResponse();
		try {
			JSONObject details = null;
			JSONObject user = null;
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", session);
			WebResource webResource = client
					.resource(UrlMetadatas.getAuthority(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String output = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			} else {
				JSONObject jobj = new JSONObject(output);
				if (jobj != null) {
					resp.setSuccess(true);
					details = (JSONObject) jobj.get("details");
					if(details != null){
						user = (JSONObject) details.get("user");
						if(user != null){
							resp.setAuthority(user.getString("authority"));
							resp.setEmail(user.getString("email"));
							resp.setContactNo(user.getString("contactNo"));
							resp.setFirstName(user.getString("firstName"));
							resp.setLastName(user.getString("lastName"));
							resp.setImage(user.getString("image"));
							resp.setResponse(details.toString());
							return resp;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
*/
}
