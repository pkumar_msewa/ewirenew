package com.msscard.app.api.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.CustomUserTransactions;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionType;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.CommonUtil;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.util.TrippleDSEncryption;
import com.msscard.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class MatchMoveApiImpl implements IMatchMoveApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mmCardRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final ISMSSenderApi senderApi;
	private final MUserRespository userRepository;
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public MatchMoveApiImpl(MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository mmCardRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, ISMSSenderApi senderApi,
			MUserRespository userRespository, SendMoneyDetailsRepository sendMoneyDetailsRepository,
			MTransactionRepository mTransactionRepository, MServiceRepository mServiceRepository) {
		super();
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mmCardRepository = mmCardRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.senderApi = senderApi;
		this.userRepository = userRespository;
		this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mServiceRepository = mServiceRepository;
	}

	@Override
	public ResponseDTO createUserOnMM(MUser request) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", request.getUserDetail().getEmail());
			userData.add("password", SecurityUtil.md5(request.getUsername()));
			userData.add("first_name", request.getUserDetail().getFirstName());
			userData.add("last_name", request.getUserDetail().getLastName());
			userData.add("mobile_country_code", "91");
			userData.add("mobile", request.getUsername());
			userData.add("preferred_name",
					request.getUserDetail().getFirstName() + " " + request.getUserDetail().getLastName());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				String id = user.getString("id");

				setComplianceDetails(request, id);

				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(request.getUserDetail().getEmail());
				cardRequest.setPassword(SecurityUtil.md5(request.getUsername()));
				cardRequest.setIdNo(id);
				WalletResponse walletResponse = createWallet(cardRequest);
				System.err.println("wallet response is::" + walletResponse);
				if (walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					MatchMoveWallet moveWallet = new MatchMoveWallet();
					moveWallet.setWalletId(walletResponse.getWalletId());
					moveWallet.setWalletNumber(walletResponse.getWalletNumber());
					moveWallet.setUser(request);
					moveWallet.setMmUserId(id);
					matchMoveWalletRepository.save(moveWallet);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("wallet created successfully");
					return resp;
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Creation of wallet failed....");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse createWallet(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", request.getIdNo()).post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				resp.setAvailableAmt(user.getJSONObject("funds").getJSONObject("available").getString("amount"));
				resp.setCard_status(user.getJSONObject("status").getBoolean("is_active"));
				resp.setExpiryDate(date.getString("expiry"));
				resp.setIssueDate(date.getString("issued"));
				resp.setWithholdingAmt(user.getJSONObject("funds").getJSONObject("withholding").getString("amount"));
				resp.setWalletNumber(user.getString("number"));
				resp.setWalletId(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getValue());
			return resp;
		}
	}

	@Override
	public WalletResponse assignVirtualCard(MUser mUser) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				userId = userResp.getMmUserId();
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
					.post(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				String walletId = user.getString("id");

				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + walletId + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json")
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);

				@SuppressWarnings("unused")
				String cvv = null;
				System.err.println("cvv request::::" + cvvRequest);
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				MatchMoveWallet wal = matchMoveWalletRepository.findByUser(mUser);
				if (wal != null) {
					wal.setIssueDate(date.getString("issued"));
					MMCards cards = mmCardRepository.getVirtualCardsByCardUser(mUser);
					if (cards == null) {
						MMCards virtualCard = new MMCards();
						virtualCard.setCardId(walletId);
						virtualCard.setHasPhysicalCard(false);
						virtualCard.setStatus(Status.Active.getValue());
						virtualCard.setWallet(wal);
						MMCards cd = mmCardRepository.save(virtualCard);
						System.err.println("card saved" + cd.getId());
					} else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("You are already assigned with a card.Please Contact Admin for queries");
						return resp;
					}
					matchMoveWalletRepository.save(wal);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("card successfully assigned...");
					return resp;

				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Due to some internal issue card cannot be issued.Please contact customer care");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse inquireCard(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		try {
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
			ClientResponse resp1 = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {

				JSONObject date = user.getJSONObject("date");
				System.err.println("walletId:::" + user.getString("id"));

				Client client1 = Client.create();
				client1.addFilter(new LoggingFilter(System.out));
				WebResource resource1 = client1.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ user.getString("id") + "/securities/tokens");
				ClientResponse resp2 = resource1.accept("application/json")
						.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
						.get(ClientResponse.class);
				String strResponse1 = resp2.getEntity(String.class);
				JSONObject cvvRequest = new JSONObject(strResponse1);
				System.err.println("cvvRequest:::::::" + cvvRequest);
				String cvv = null;
				if (cvvRequest.has("value")) {
					cvv = cvvRequest.getString("value");
				} else {
					cvv = "xxx";
				}
				resp.setWalletNumber(user.getString("number"));
				resp.setExpiryDate(date.getString("expiry"));
				resp.setIssueDate(date.getString("issued"));
				resp.setCardId(user.getString("id"));
				resp.setCvv(cvv);
				resp.setHolderName(user.getJSONObject("holder").getString("name"));
				resp.setStatus(user.getJSONObject("status").getString("text"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("card successfully assigned...");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * FETCH USER FROM MATCHMOVE
	 */
	@Override
	public ResponseDTO fetchMMUser(MatchMoveCreateCardRequest request) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MUser mUser = userRepository.findByUsername(request.getUsername());
			if (mUser == null) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User Not found");
				return resp;
			}
			String userId = null;
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("fetch mm user response:::" + user);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("User fetched successfully");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * FETCH WALLET ON MM
	 */
	@Override
	public WalletResponse fetchWalletMM(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		try {
			String userId = null;
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				if (userResp != null && userResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					userId = userResp.getMmUserId();
					System.err.println(userId + "this is userId");
				}
			}

			System.err.println("UserID:::" + userId);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				String expiry = date.getString("expiry");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				String walletId = user.getString("id");
				String walletnumber = user.getString("number");
				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(expiry);
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(walletnumber);
				resp.setWalletId(walletId);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse fetchWalletMMOld(MatchMoveCreateCardRequest request) {
		WalletResponse resp = new WalletResponse();
		try {
			String userId = null;
			MUser mUser = userRepository.findByUsername(request.getUsername());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(mUser.getUserDetail().getEmail(), mUser.getUsername());
				if (userResp != null && userResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					userId = userResp.getMmUserId();
					System.err.println(userId + "this is userId");
				}
			}

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				String issue = date.getString("issued");
				String withholdingamt = user.getJSONObject("funds").getJSONObject("withholding").getString("amount");
				String availabeamt = user.getJSONObject("funds").getJSONObject("available").getString("amount");
				boolean cardstatus = user.getJSONObject("status").getBoolean("is_active");
				resp.setAvailableAmt(availabeamt);
				resp.setCard_status(cardstatus);
				resp.setExpiryDate(date.getString("expiry"));
				resp.setIssueDate(issue);
				resp.setWithholdingAmt(withholdingamt);
				resp.setWalletNumber(user.getString("number"));
				resp.setWalletId(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	// fetch wallet on login
	@Override
	public WalletResponse fetchWalletMMOnLogin(MatchMoveCreateCardRequest request, MUser u) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();

			} else {
				UserKycResponse userResp = getUsers(request.getEmail(), request.getUsername());
				userId = userResp.getMmUserId();
			}

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp1 = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", userId).get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				JSONObject date = user.getJSONObject("date");
				resp.setAvailableAmt(user.getJSONObject("funds").getJSONObject("available").getString("amount"));
				resp.setCard_status(user.getJSONObject("status").getBoolean("is_active"));
				resp.setExpiryDate(date.getString("expiry"));
				resp.setIssueDate(date.getString("issued"));
				resp.setWithholdingAmt(user.getJSONObject("funds").getJSONObject("withholding").getString("amount"));
				resp.setWalletNumber(user.getString("number"));
				resp.setWalletId(user.getString("id"));

				MatchMoveWallet mmWallet = new MatchMoveWallet();
				mmWallet.setIssueDate(date.getString("issued"));
				mmWallet.setWalletId(user.getString("id"));
				mmWallet.setWalletNumber(user.getString("number"));
				mmWallet.setUser(u);
				matchMoveWalletRepository.save(mmWallet);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * DEACTIVATE CARD
	 */

	@Override
	public ResponseDTO deActivateCards(MatchMoveCreateCardRequest request) {
		ResponseDTO resp = new ResponseDTO();
		JSONObject user = null;
		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());
			if (request.getRequestType().equalsIgnoreCase("suspend")) {
				MatchMoveWallet mmw = card.getWallet();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				} else {
					card.setBlocked(true);
					mmCardRepository.save(card);
					resp.setDetails(user.toString());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setCardDetails(user.getString("id"));
					resp.setStatus(user.getString("status"));
					resp.setMessage("User card Deactivated");
					return resp;
				}
			} else {
				MultivaluedMap<String, String> data = new MultivaluedMapImpl();
				data.add("type", request.getRequestType().toLowerCase());
				MatchMoveWallet mmw = card.getWallet();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + request.getCardId());
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, data);
				String strResponse = resp1.getEntity(String.class);
				user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				} else {
					card.setBlocked(true);
					mmCardRepository.save(card);
					resp.setDetails(user.toString());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setCardDetails(user.getString("id"));
					resp.setStatus(user.getString("status"));
					resp.setMessage("User card Deactivated");
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * REACTIVATE CARD
	 */

	@Override
	public ResponseDTO reActivateCard(MatchMoveCreateCardRequest request) {
		ResponseDTO result = new ResponseDTO();
		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());
			String cardType = null;
			MultivaluedMap<String, String> data = new MultivaluedMapImpl();
			data.add("id", request.getCardId());
			MMCards cards = mmCardRepository.getCardByCardId(request.getCardId());
			if (cards.isHasPhysicalCard()) {
				cardType = MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL;
			} else {
				cardType = MatchMoveUtil.MATCHMOVE_CARDTYPE_VIRUAL;
			}
			MatchMoveWallet mmw = cards.getWallet();

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardType);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, data);
			String strResponse = resp1.getEntity(String.class);
			JSONObject json = new JSONObject(strResponse);
			System.err.println("reactivate card response:::::" + json);
			if (resp1.getStatus() != 200) {
				result.setStatus(json.getString("status"));
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Card unblocked");
			} else {
				result.setStatus(json.getString("status"));
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Card unblocked");
				card.setStatus(Status.Active.getValue());
				card.setBlocked(false);
				mmCardRepository.save(card);
				return result;
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Operation Failed due to some exception...");
		}
		return result;
	}

	@Override
	public ResponseDTO reActivateCardCorp(MatchMoveCreateCardRequest request) {
		ResponseDTO result = new ResponseDTO();
		try {
			MMCards card = mmCardRepository.getCardByCardId(request.getCardId());
			MultivaluedMap<String, String> data = new MultivaluedMapImpl();
			data.add("id", request.getCardId());

			MMCards cards = mmCardRepository.getCardByCardId(request.getCardId());
			if (cards != null) {
				MatchMoveWallet mmw = cards.getWallet();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, data);
				String strResponse = resp1.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				System.err.println("reactivate card response:::::" + json);
				if (json != null) {
					result.setStatus(json.getString("status"));
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("User unblocked");
					card.setStatus(Status.Active.getValue());
					card.setBlocked(false);
					mmCardRepository.save(card);
					return result;
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Operation Failed..");
				return result;
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Operation Failed due to some exception...");
		}
		return result;
	}

	@Override
	public ResponseDTO activationPhysicalCard(String activationCode, MUser user) {
		ResponseDTO respDTO = new ResponseDTO();
		MMCards card = mmCardRepository.getPhysicalCardByUser(user);
		PhysicalCardDetails physicalCardDetails = physicalCardDetailRepository.findByCard(card);
		if (physicalCardDetails.getActivationCode().equalsIgnoreCase(activationCode)) {
			ResponseDTO resp = activateMMPhysicalCard(card.getCardId(), activationCode);
			respDTO.setCode(resp.getCode());
			respDTO.setMessage(resp.getMessage());
			return respDTO;
		} else {
			respDTO.setCode(ResponseStatus.FAILURE.getValue());
			respDTO.setMessage("Card Activation failed...Please contact Customer Care");
			return respDTO;
		}
	}

	@SuppressWarnings("unused")
	@Override
	public WalletResponse assignPhysicalCardFromGroup(String mobile, String proxy_number) {
		WalletResponse resp = new WalletResponse();

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY0000" + proxy_number);

			MUser mUser = userRepository.findByUsername(mobile);

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
					+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);

			if (resp1.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				return resp;
			} else {
				String walletId = user.getString("id");
				String activationCode = user.getString("activation_code");
				resp.setActivationCode(activationCode);

				if (mmw != null) {
					MMCards card = mmCardRepository.getPhysicalCardByUser(mUser);
					if (card == null) {
						card = new MMCards();
						card.setCardId(walletId);
						card.setHasPhysicalCard(true);
						card.setWallet(mmw);
						card.setActivationCode(activationCode);
						ResponseDTO activationResponse = activateMMPhysicalCard(walletId, activationCode);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(activationResponse.getCode())) {
							card.setStatus("Active");
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setMessage("Card Activated");
						} else {
							card.setStatus(Status.Inactive.getValue());
							resp.setCode("A00");
							resp.setMessage("Activation Failed.");
						}
						mmCardRepository.save(card);
						return resp;
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Physical card already exist for this user");
						return resp;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		resp.setStatus(ResponseStatus.FAILURE.getKey());
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	@Override
	public ResponseDTO activateMMPhysicalCard(String cardId, String activationCode) {
		ResponseDTO result = new ResponseDTO();
		try {
			MMCards card = mmCardRepository.getCardByCardId(cardId);
			MultivaluedMap<String, String> data = new MultivaluedMapImpl();
			data.add("activation_code", activationCode);

			MatchMoveWallet mmw = card.getWallet();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class, data);
			String strResponse = resp1.getEntity(String.class);
			JSONObject json = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				card.setStatus(Status.Active.getValue());
				mmCardRepository.save(card);
				result.setStatus(json.getString("status"));
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Card Activated Successfully");
				return result;
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Activation failed");
				return result;
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Operation Failed due to some exception...");
			return result;
		}
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletRazor(MUser mUser, String amount) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PG");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(user.getString("confirm"));
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * corporate load to wallet used in bulk load
	 */

	@Override
	public WalletResponse initiateLoadFundsToMMWalletCorp(MUser mUser, String amount) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "CORP_LOAD");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(user.getString("confirm"));
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletTopup(MUser mUser, String amount) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "TOPUP_LOAD");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				String authRetrievalId = user.getString("id");
				String walletId = user.getString("wallet_id");
				String status = user.getString("confirm");
				resp.setWalletId(walletId);
				resp.setAuthRetrivalNo(authRetrievalId);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(status);
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletUpi(MUser mUser, String amount, String transactionRefNo,
			String upiid) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "upi");
			details.put("payment_ref", transactionRefNo);
			details.put("status", "Success");
			details.put("description", upiid);
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			System.err.println("wallet fund initiate load::" + user);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(user.getString("confirm"));
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * CONFIRM FUNDS TO WALLET
	 */

	@Override
	public WalletResponse confirmFundsToMMWallet(MUser mUser, String amount, String authRetrievalNo) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + authRetrievalNo);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				System.err.println("confirm funds wallet load::" + response);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					JSONArray transactions = response.getJSONArray("transactions");
					if (transactions != null) {
						JSONObject jobject = transactions.getJSONObject(0);
						if (jobject != null) {
							String status = jobject.getString("status");
							resp.setStatus(status);
							if (Status.Success.getValue().equalsIgnoreCase(status)) {
								resp.setCode(ResponseStatus.SUCCESS.getValue());
								resp.setMessage("Load Confirmed");
								return resp;
							} else {
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setMessage("Load confirmation failed");
								return resp;
							}
						} else {
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Load confirmation failed");
							return resp;
						}
					} else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Load confirmation failed");
						return resp;
					}
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Load confirmation failed");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * TRANSFER FUNDS TO CARD
	 */

	@Override
	public WalletResponse transferFundsToMMCard(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				System.err.println("Transfer funds to card::" + user);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("Tranfered sucessfully");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user record not found");
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
		}
		return resp;
	}

	@Override
	public WalletResponse transferFundsToMMCardTemp(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));

				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);

				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setMessage("Tranfered sucessfully");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user record not found.");
				resp.setStatus(ResponseStatus.FAILURE.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	/**
	 * KYC PROCESS
	 */
	@Override
	public UserKycResponse kycUserMM(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

			userData.add("address_1", request.getAddress1());
			userData.add("address_2", request.getAddress2());
			userData.add("city", request.getCity());
			userData.add("state", request.getState());
			userData.add("country", request.getCountry());
			userData.add("zipcode", request.getPinCode());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/addresses/residential");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject address = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(address.getString("description"));
				return resp;
			} else {
				resp.setMessage("residential address saved");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse setIdDetails(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			if (mmw != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("title", "Mr");
				userData.add("id_type", request.getId_type());
				userData.add("id_number", request.getId_number());
				userData.add("country_of_issue", request.getCountry());
				userData.add("birthday", sdf.format(request.getUser().getUserDetail().getDateOfBirth()));
				userData.add("gender", "male");

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				}
			} else {
				resp.setMessage("error while saving address details");
				resp.setCode(ResponseStatus.FAILURE.getKey());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse setImagesForKyc(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			byte[] array = recoverImageFromUrl(request.getFilePath());

			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("data", encodedfile.trim());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getKey());
				resp.setMessage("user record not found");
				return resp;
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	public byte[] recoverImageFromUrl(String urlText) throws Exception {
		URL url = new URL(urlText);
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		try (InputStream inputStream = url.openStream()) {
			int n = 0;
			byte[] buffer = new byte[1024];
			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}
		}
		return output.toByteArray();
	}

	@Override
	public UserKycResponse confirmKyc(UserKycRequest request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request.getUser());
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					resp.setMessage("Kyc confirmed");
					resp.setCode(ResponseStatus.SUCCESS.getKey());
					return resp;
				}
			} else {
				resp.setMessage("Kyc confirmed");
				resp.setCode(ResponseStatus.FAILURE.getKey());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * BALANCE API
	 */
	@Override
	public double getBalance(MUser request) {
		String cardId = null;
		double balance = 0;
		ClientResponse resp1 = null;

		try {
			MMCards physicalCard = mmCardRepository.getPhysicalCardByUser(request);
			if (physicalCard != null) {
				if (Status.Active.getValue().equalsIgnoreCase(physicalCard.getStatus())) {
					cardId = physicalCard.getCardId();
				}
			}

			if (cardId == null) {
				MMCards virtualCard = mmCardRepository.getVirtualCardsByCardUser(request);
				if (virtualCard != null) {
					if (Status.Active.getValue().equalsIgnoreCase(virtualCard.getStatus())) {
						cardId = virtualCard.getCardId();
					}
				}
			}

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);

			if (mmw.getMmUserId() != null) {
				resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
			} else {
				resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
			}
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				return 0.0;
			}
			if (resp1.getStatus() != 200) {
				return balance;
			} else {
				JSONObject funds = user.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return balance;
		}
		return balance;
	}

	// getbalanceapp

	@Override
	public Double getBalanceApp(MUser request) {
		String cardId = null;
		double balance = 0;
		String mmUserId = null;

		try {

			MMCards physicalCard = mmCardRepository.getPhysicalCardByUser(request);
			if (physicalCard != null) {
				if (Status.Active.getValue().equalsIgnoreCase(physicalCard.getStatus())) {
					cardId = physicalCard.getCardId();
				}
			}

			if (cardId == null) {
				MMCards virtualCard = mmCardRepository.getVirtualCardsByCardUser(request);
				if (virtualCard != null) {
					if (Status.Active.getValue().equalsIgnoreCase(virtualCard.getStatus())) {
						cardId = virtualCard.getCardId();
					}
				}
			}

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw == null) {
				UserKycResponse kycResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (kycResponse != null) {
					mmUserId = kycResponse.getMmUserId();
				}
			} else {
				mmUserId = mmw.getMmUserId();
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
			ClientResponse resp = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", mmUserId)
					.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			if (resp.getStatus() != 200) {
				return balance;
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return balance;
		}
		return balance;
	}

	@Override

	public Double getBalanceAppWallet(MUser request) {
		double balance = 0;
		String mmUserId = null;
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw == null) {
				UserKycResponse kycResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (kycResponse != null) {
					mmUserId = kycResponse.getMmUserId();
				}
			} else {
				mmUserId = mmw.getMmUserId();
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
			ClientResponse resp = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmUserId).get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			if (resp.getStatus() != 200) {
				return balance;
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return balance;
		}
		return balance;
	}

	@Override
	public Double getBalanceCus(MUser request) {
		String cardId = null;
		String mmUserId = null;
		double balance = 0;
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(request);
			if (cards != null) {
				cardId = cards.getCardId();
			}
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(request);
			if (mmw != null && mmw.getMmUserId() != null) {
				mmUserId = mmw.getMmUserId();
			} else {
				UserKycResponse userResponse = getUsers(request.getUserDetail().getEmail(), request.getUsername());
				if (userResponse != null
						&& userResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					mmUserId = userResponse.getMmUserId();
				}
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(SMSConstant.GETBALANCE + cardId);
			ClientResponse resp = resource.accept("application/json")
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", mmUserId)
					.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);

			if (resp.getStatus() != 200) {
				return balance;
			} else {
				JSONObject funds = response.getJSONObject("funds");
				if (funds != null) {
					balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return balance;
		}
		return balance;
	}

	@Override
	public ResponseDTO getBalanceExp(MUser request) {
		ResponseDTO dto = new ResponseDTO();
		String cardId = null;
		ClientResponse resp1 = null;
		double balance = 0.0;
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(request);
			if (cards != null) {
				if (Status.Active.getValue().equalsIgnoreCase(cards.getStatus())) {
					cardId = cards.getCardId();
				}
			}
			MMCards phyCard = mmCardRepository.getVirtualCardsByCardUser(request);
			if (phyCard != null) {
				if (Status.Active.getValue().equalsIgnoreCase(phyCard.getStatus())) {
					cardId = phyCard.getCardId();
				}
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
			if (cards.getWallet().getMmUserId() != null) {
				resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", cards.getWallet().getMmUserId()).get(ClientResponse.class);
			} else {
				resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", phyCard.getWallet().getMmUserId()).get(ClientResponse.class);
			}
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				dto.setCode(ResponseStatus.FAILURE.getValue());
				dto.setBalance(balance);
				return dto;
			} else {
				JSONObject funds = user.getJSONObject("funds");
				balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
				dto.setCode(ResponseStatus.SUCCESS.getValue());
				dto.setBalance(balance);
				return dto;
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setBalance(balance);
			dto.setCode(ResponseStatus.FAILURE.getValue());
			return dto;
		}
	}

	/**
	 * MY BALANCE
	 * 
	 */
	@Override
	public double myBalance(MUser userMob) {
		String cardId = null;
		double balance = 0;
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(userMob);
			if (cards != null) {
				if (Status.Active.getValue().equalsIgnoreCase(cards.getStatus())) {
					cardId = cards.getCardId();
				}
			}
			MMCards phyCard = mmCardRepository.getVirtualCardsByCardUser(userMob);
			if (phyCard != null) {
				if (Status.Active.getValue().equalsIgnoreCase(phyCard.getStatus())) {
					cardId = phyCard.getCardId();
				}
			}
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(userMob);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId);
				ClientResponse resp = resource.accept("application/json")
						.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse = resp.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp.getStatus() != 200) {
					return balance;
				} else {
					JSONObject funds = response.getJSONObject("funds");
					if (funds != null) {
						balance = Double.parseDouble(funds.getJSONObject("available").getString("amount"));
					}
				}
			} else {
				return balance;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return balance;
	}

	@Override
	public UserKycResponse tempKycUserMM(MUser user) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("address_1", "1/1,DCN homes,Kormangala");
			userData.add("address_2", "6th block");
			userData.add("city", "Bangalore");
			userData.add("state", "Karnataka");
			userData.add("country", "India");
			userData.add("zipcode", "560095");
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/addresses/residential");
				ClientResponse resp1 = resource.accept("application/json")
						.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject address = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(address.getString("description"));
					return resp;
				} else {
					resp.setMessage("residential address saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("User record not found.");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse tempSetIdDetails(MUser user) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			Date dob = user.getUserDetail().getDateOfBirth();
			String date = sdf.format(dob);
			userData.add("title", "Mr");
			userData.add("id_type", "voters_id");
			userData.add("id_number", "RPZ" + user.getUserDetail().getContactNo());
			userData.add("country_of_issue", "India");
			userData.add("birthday", date);
			userData.add("gender", "male");

			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
				ClientResponse resp1 = resource.accept("application/json")
						.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(json.getString("description"));
					return resp;
				} else {
					resp.setMessage("Id Details saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user record not found");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse getUsersOath(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MUser u = userRepository.findByUsername(mobile);
			if (u == null) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User not found");
				return resp;
			}
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(u);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
				ClientResponse res = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse = res.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				if (res.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				} else {
					logger.info("mmuserid :: " + user.getString("id"));
					resp.setMmUserId(user.getString("id"));
					resp.setDetails(user.toString());
					resp.setMessage("Id Details saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user record not found");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse getUsers(String email, String mobile) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MUser user = userRepository.findByUsername(mobile);
			if (user != null) {
				MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
				if (wallet != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
					ClientResponse res = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", wallet.getMmUserId()).get(ClientResponse.class);
					String strResponse = res.getEntity(String.class);
					JSONObject json = new JSONObject(strResponse);
					if (res.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(json.getString("description"));
						return resp;
					} else {
						resp.setMmUserId(json.getString("id"));
						resp.setDetails(user.toString());
						resp.setMessage("Id Details saved");
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					}
				} else {
					resp.setMessage("user wallet not found.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				resp.setMessage("User record not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse getConsumers() {
		UserKycResponse resp = new UserKycResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/oauth/consumer");
			ClientResponse res = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.get(ClientResponse.class);
			String strResponse = res.getEntity(String.class);
			JSONObject json = new JSONObject(strResponse);
			if (res.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(json.getString("description"));
				return resp;
			} else {
				resp.setPrefundingBalance(json.getJSONObject("consumer").getString("fund_floating_balance"));
				resp.setDetails(json.toString());
				resp.setMessage("Id Details saved");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse tempSetImagesForKyc(MUser user) {
		UserKycResponse resp = new UserKycResponse();
		try {
			String files = "/usr/local/tomcat/webapps/ROOT/WEB-INF/startup/easemytrip.png";
			byte[] array = Files.readAllBytes(new File(files).toPath());
			String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("data", encodedfile.trim());

			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse res = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = res.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				if (res.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(json.getString("description"));
					return resp;
				} else {
					resp.setMessage("Images saved saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse tempConfirmKyc(MUser user) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse res = resource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class);
				String strResponse = res.getEntity(String.class);
				JSONObject json = new JSONObject(strResponse);
				if (res.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(json.getString("description"));
					return resp;
				} else {
					resp.setMessage("Images saved saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse tempKycStatusApproval(MUser user) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("status", "approved");

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/statuses");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					resp.setStatus(ResponseStatus.FAILURE.getValue());
					return resp;
				} else {
					resp.setMessage("Kyc confirmed");
					resp.setStatus(ResponseStatus.SUCCESS.getValue());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found.");
				resp.setStatus(ResponseStatus.FAILURE.getValue());
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setStatus(ResponseStatus.FAILURE.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse debitFromMMWalletToCard(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");

			MUser user = userRepository.findByUsername(mobile);
			if (user != null) {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(response.getString("description"));
						return resp;
					} else {
						resp.setMessage("Funds transferred to wallet");
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					}
				} else {
					resp.setMessage("user wallet not found.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			} else {
				resp.setMessage("user record not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	/**
	 * DEBIT TO PREFUND ACCOUNT
	 */

	@Override
	public UserKycResponse debitFromCard(String email, String mobile, String cardId, String amount, String message) {
		UserKycResponse resp = new UserKycResponse();

		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");
			MUser user = userRepository.findByUsername(mobile);
			if (user != null) {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					System.err.println("debit from card to prefund:::" + response);
					if (resp1.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(response.getString("description"));
						return resp;
					} else {
						UserKycResponse debitStatus = deductFromCardConfirmation(user, amount, message);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(debitStatus.getDebitStatus())) {
							resp.setMessage("Funds transferred to wallet");
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setIndicator(response.getString("id"));
							return resp;
						} else {
							resp.setMessage("transaction failed please try again later");
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setStatus(ResponseStatus.FAILURE.getValue());
						}
					}
				} else {
					resp.setMessage("user wallet not found.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			} else {
				resp.setMessage("user record not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardFingoole(MUser user, String cardId, String amount, String message) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);
				userData.add("message", "return funds");
				userData.add("to_wallet", Boolean.toString(true));
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
				ClientResponse clientResponse = webResources
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.delete(ClientResponse.class, userData);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);

				System.err.println("debit from card to wallet:::" + response);
				if (clientResponse.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					resp = deductFromCardConfirmation(user, amount, message);
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User record not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardCorp(String email, String mobile, String cardId, String amount) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");
			userData.add("to_wallet", Boolean.toString(true));

			MUser user = userRepository.findByUsername(mobile);
			if (user != null) {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(response.getString("description"));
						return resp;
					} else {
						UserKycResponse debitStatus = deductFromCardConfirmation(user, amount, "Corporate Debit");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(debitStatus.getDebitStatus())) {
							resp.setMessage("Funds transferred to wallet");
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							return resp;
						} else {
							resp.setMessage("transaction failed please try after some time.");
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setStatus(ResponseStatus.FAILURE.getValue());
						}
					}
				} else {
					resp.setMessage("user wallet not found.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}

			} else {
				resp.setMessage("user record not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse debitFromCardInHouse(MUser user, String cardId, String amount, String message) {
		UserKycResponse resp = new UserKycResponse();

		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);
				userData.add("message", "return funds");
				userData.add("to_wallet", Boolean.toString(true));
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
				ClientResponse clientResponse = webResources
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.delete(ClientResponse.class, userData);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);

				System.err.println("debit from card to wallet:::" + response);
				if (clientResponse.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					resp = deductFromCardConfirmation(user, amount, message);
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User record not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse deductFromCardConfirmation(MUser user, String amount, String message) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("email", user.getUserDetail().getEmail());
			userData.add("message", message);
			JSONObject details = new JSONObject();
			details.put("pp", message);
			userData.add("details", details.toString());

			Client client1 = Client.create();
			client1.addFilter(new LoggingFilter(System.out));
			WebResource webResources1 = client1.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse clientResponse1 = webResources1.header("Content Type", MediaType.APPLICATION_FORM_URLENCODED)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.delete(ClientResponse.class, userData);
			String strResponse1 = clientResponse1.getEntity(String.class);
			JSONObject transaction1 = new JSONObject(strResponse1);
			System.err.println("deduct confirmation:::" + transaction1);
			if (clientResponse1.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(transaction1.getString("description"));
			} else {
				MultivaluedMap<String, String> deductconfrm = new MultivaluedMapImpl();
				deductconfrm.add("ids", transaction1.getString("id"));
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/oauth/consumer/funds");
				ClientResponse clientResponse = webResources
						.header("Content Type", MediaType.APPLICATION_FORM_URLENCODED)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.delete(ClientResponse.class, deductconfrm);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject transaction = new JSONObject(strResponse);
				System.err.println("deduct confirmation:::" + transaction);
				if (clientResponse.getStatus() != 200) {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(transaction.getString("description"));
				} else {
					JSONArray jsonArray = transaction.getJSONArray("transactions");
					if (jsonArray != null) {
						JSONObject jobj = jsonArray.getJSONObject(0);
						if (jobj != null && jobj.has("status")) {
							String status = jobj.getString("status");
							if (status != null && !"".equalsIgnoreCase(status)) {
								if (Status.Success.getValue().equalsIgnoreCase(status)) {
									resp.setStatus(ResponseStatus.SUCCESS.getKey());
									resp.setCode(ResponseStatus.SUCCESS.getValue());
									resp.setMessage("Funds transferred to wallet");
								}
							} else {
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setMessage("Status is blank");
							}
						} else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Status not found");
						}
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Response not found");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactions(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		JSONObject transaction = null;
		ClientResponse clientResponse = null;
		try {
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + vircardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", virCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + phycardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", phyCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(transaction.getString("description"));
			} else {
				if (transaction != null) {
					resp.setCardTransactions(transaction.toString());
					resp.setMessage("TransactionList fetched");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	/**
	 * RESETTING PINS
	 */

	@Override
	public UserKycResponse resetPins(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		String cardId = null;
		try {
			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				if (Status.Active.getValue().equalsIgnoreCase(phyCard.getStatus())) {
					cardId = phyCard.getCardId();
				}
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResources = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/pins/reset");
			ClientResponse clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.get(ClientResponse.class);
			String strResponse = clientResponse.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(response.getString("description"));
				return resp;
			} else {
				resp.setDetails(response.toString());
				resp.setMessage("Your 4-digit secure PIN has been sent to your registered Email Id");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;

			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public MMCards findCardByCardId(String cardId) {
		return mmCardRepository.getCardByCardId(cardId);
	}

	@Override
	public MMCards findCardByUserAndStatus(MUser user) {
		return mmCardRepository.getCardByUserAndStatus(user, Status.Active.getValue());
	}

	@Override
	public WalletResponse transferFundFromCardToWallet(String amount, MUser user, String cardId, MUser reciever) {
		WalletResponse resp = new WalletResponse();
		try {
			if (user != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);
				userData.add("message", "return funds");
				userData.add("to_wallet", Boolean.toString(true));

				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject transaction = new JSONObject(strResponse);

					System.out.println("transfer from card to wallet::::" + transaction);
					if (resp1.getStatus() != 200) {
						resp.setMessage(transaction.getString("description"));
						resp.setCode(ResponseStatus.FAILURE.getValue());
						return resp;
					}
					if (transaction != null) {
						return walletToWalletTransfer(amount, user, cardId, reciever);
					}
				} else {
					resp.setMessage("user wallet not found.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				resp.setMessage("user record not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("exception occured while fund transfering. please try after some time");
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public WalletResponse transferFundFromCardToWalletDonation(String amount, MUser user, String cardId,
			String recipient) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("amount", amount);
			userData.add("message", "return funds");
			userData.add("to_wallet", Boolean.toString(true));

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
			if (mmw != null && Status.Active.equals(user.getMobileStatus())) {
				MUser recipientUser = userRepository.findByUsername(recipient);
				if (recipientUser != null && Status.Active.equals(recipientUser.getMobileStatus())) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject transaction = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setMessage(transaction.getString("description"));
						resp.setCode(ResponseStatus.FAILURE.getValue());
						return resp;
					} else {
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						return walletToWalletTransfer(amount, user, cardId, recipientUser);
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setMessage("Reciever not found.");
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setMessage("user wallet not found.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setStatus(ResponseStatus.FAILURE.getKey());
			resp.setMessage("please try afer some time.");
		}
		return resp;
	}

	@Override
	public WalletResponse walletToWalletTransfer(String amount, MUser user, String cardId, MUser reciever) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", reciever.getUserDetail().getEmail());
			userData.add("amount", amount);

			JSONObject details = new JSONObject();
			details.put("pp", "fund transfer");
			userData.add("details", details.toString());

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject transaction = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setMessage(transaction.getString("description"));
					resp.setCode(ResponseStatus.FAILURE.getValue());
					return resp;
				} else {
					resp.setAuthRetrivalNo(transaction.getString("id"));

					SendMoneyDetails sendMoney = new SendMoneyDetails();
					sendMoney.setRecipientNo(reciever.getUsername());
					sendMoney.setAmount(amount);
					sendMoney.setChecked(false);
					sendMoney.setStatus(Status.Pending.getValue());
					sendMoney.setTransactionId(transaction.getString("id"));
					sendMoney.setUser(user);
					sendMoneyDetailsRepository.save(sendMoney);

					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage(
							"Transaction Successfull.Amount will be reflected in Recipient's Card in 5 minutes");
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user wallet not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Operation failed please contact customer care");
			return resp;
		}
	}

	@Override
	public WalletResponse acknowledgeFundTransfer(String amount, String transactionId, String recipient) {
		WalletResponse resp = new WalletResponse();
		try {
			MUser recipientUser = userRepository.findByUsername(recipient);
			if (recipientUser != null && Status.Active.equals(recipientUser.getMobileStatus())) {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipientUser);
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + transactionId);
					ClientResponse resp1 = resource.accept("application/json")
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).put(ClientResponse.class);
					String strResponse = resp1.getEntity(String.class);
					JSONObject transaction = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setMessage(transaction.getString("description"));
						resp.setCode(ResponseStatus.FAILURE.getValue());
						return resp;
					} else {
						resp.setMessage("Fund Transfer completed");
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					}
				} else {
					resp.setMessage("user wallet not found or invalid user.");
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				resp.setMessage("user record not found or invalid user.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("exception occured while fund transfering please try after some time");
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactions(MUser userRequest, String page) {
		UserKycResponse resp = new UserKycResponse();
		ClientResponse clientResponse = null;
		JSONObject transaction = null;
		try {
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + vircardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", virCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}
			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + phycardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", phyCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(transaction.getString("description"));
			} else {
				resp.setCardTransactions(transaction.toString());
				resp.setMessage("TransactionList fetched");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}

		return resp;
	}

	@Override
	public WalletResponse cancelFundTransfer(MUser recipient, String amount, String id) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", recipient.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PPI");
			userData.add("details", details.toString());
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipient);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/" + id);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject transaction = new JSONObject(strResponse);

				if (resp1.getStatus() != 200) {
					resp.setMessage(transaction.getString("description"));
					resp.setCode(ResponseStatus.FAILURE.getValue());
					return resp;
				} else {
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setDetails(transaction.toString());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found or invalid user.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("exception occured while fund transfering please try after some time.");
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public WalletResponse getFundTransfer(MUser recipient) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(recipient);
			if (mmw != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds/transfers");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).get(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				JSONObject transaction = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setMessage(transaction.getString("description"));
					resp.setCode(ResponseStatus.FAILURE.getValue());
					return resp;
				} else {
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setDetails(transaction.toString());
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user wallet not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("exception occured please try after some time.");
		}
		return resp;
	}

	@Override
	public UserKycResponse getTransactionsCustom(MUser userRequest, String page) {
		UserKycResponse resp = new UserKycResponse();
		JSONObject transaction = null;
		ClientResponse clientResponse = null;
		try {
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + vircardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", virCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}

			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + phycardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", phyCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				transaction = new JSONObject(strResponse);
			}
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(transaction.getString("description"));

			} else {
				if (transaction != null) {
					JSONArray transactionArray = transaction.getJSONArray("transactions");
					List<CustomUserTransactions> transListPhy = new ArrayList<>();
					if (transactionArray != null) {
						for (int i = 0; i < transactionArray.length(); i++) {
							CustomUserTransactions trRespPhy = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespPhy.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespPhy.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespPhy.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespPhy.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespPhy.setMessage(transactionObject.getString("description"));
									} else {
										trRespPhy.setMessage("ATM withdrawal");
									}
								} else {
									trRespPhy.setMessage("ATM withdrawal");
								}
							}
							trRespPhy.setAmount(amount);
							trRespPhy.setIndicator(indicator);
							trRespPhy.setDate(date);
							trRespPhy.setStatus(status);
							transListPhy.add(trRespPhy);
						}
					}
					resp.setCustomPhysicalTransaction(transListPhy);
					resp.setCardTransactions(transaction.toString());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
				}
			}
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(transaction.getString("description"));

			} else {
				List<CustomUserTransactions> transListVir = new ArrayList<>();
				if (transaction != null) {
					JSONArray transactionArray = transaction.getJSONArray("transactions");
					if (transactionArray != null) {
						for (int i = 0; i < transactionArray.length(); i++) {
							CustomUserTransactions trRespVir = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							JSONObject detailsJson = transactionObject.getJSONObject("details");

							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespVir.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespVir.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespVir.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespVir.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespVir.setMessage(transactionObject.getString("description"));
									} else {
										trRespVir.setMessage("ATM withdrawal");
									}
								} else {
									trRespVir.setMessage("ATM withdrawal");
								}
							}
							trRespVir.setAmount(amount);
							trRespVir.setIndicator(indicator);
							trRespVir.setDate(date);
							trRespVir.setStatus(status);
							transListVir.add(trRespVir);
						}
					}
					resp.setCardTransactions(transaction.toString());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setCustomVirtualTransaction(transListVir);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	// Admin user trax

	@Override
	public UserKycResponse getTransactionsCustomAdmin(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		ClientResponse clientResponse = null;
		try {
			MMCards virCard = mmCardRepository.getVirtualCardsByCardUser(userRequest);
			if (virCard != null) {
				String vircardId = virCard.getCardId();

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + vircardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", virCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject responseVir = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(responseVir.getString("description"));

				} else {
					List<CustomUserTransactions> transListVir = new ArrayList<>();

					if (responseVir != null) {
						JSONArray transactionArray = responseVir.getJSONArray("transactions");
						if (transactionArray != null) {

							for (int i = 0; i < transactionArray.length(); i++) {

								CustomUserTransactions trRespVir = new CustomUserTransactions();
								JSONObject transactionObject = transactionArray.getJSONObject(i);
								String amount = transactionObject.getString("amount");
								String date = transactionObject.getString("date");
								String indicator = transactionObject.getString("indicator");
								String status = transactionObject.getString("status");

								JSONObject detailsJson = transactionObject.getJSONObject("details");

								String type = transactionObject.getString("type");
								if (detailsJson != null) {
									if (type != null) {
										if (type.trim().equalsIgnoreCase("Purchase")) {
											String merchantName = detailsJson.getString("merchantname");
											trRespVir.setMessage(merchantName);

										} else if (type.trim().equalsIgnoreCase("Load")) {
											trRespVir.setMessage("Load money of Rs " + amount);
										} else if (type.trim().equalsIgnoreCase("Unload")) {
											trRespVir.setMessage("Send Money of Rs " + amount);
										} else if (type.trim().equalsIgnoreCase("Get Card")) {
											trRespVir.setMessage("Card Fees");
										} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
											trRespVir.setMessage(transactionObject.getString("description"));
										} else {
											trRespVir.setMessage("ATM withdrawal");
										}
									} else {
										trRespVir.setMessage("ATM withdrawal");
									}
								}
								trRespVir.setAmount(amount);
								trRespVir.setIndicator(indicator);
								trRespVir.setDate(date);
								trRespVir.setStatus(status);
								transListVir.add(trRespVir);
							}
						}
						resp.setCardTransactions(responseVir.toString());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setCustomVirtualTransaction(transListVir);
					}
				}
			}
			MMCards phyCard = mmCardRepository.getPhysicalCardByUser(userRequest);
			if (phyCard != null) {
				String phycardId = phyCard.getCardId();
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResources = client.resource(
						MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + phycardId + "/transactions");
				clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", phyCard.getWallet().getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject responsePhy = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(responsePhy.getString("description"));
				} else {
					JSONArray transactionArray = responsePhy.getJSONArray("transactions");
					List<CustomUserTransactions> transListPhy = new ArrayList<>();
					if (transactionArray != null) {
						for (int i = 0; i < transactionArray.length(); i++) {
							CustomUserTransactions trRespPhy = new CustomUserTransactions();
							JSONObject transactionObject = transactionArray.getJSONObject(i);
							String amount = transactionObject.getString("amount");
							String date = transactionObject.getString("date");
							String indicator = transactionObject.getString("indicator");
							String status = transactionObject.getString("status");
							JSONObject detailsJson = transactionObject.getJSONObject("details");
							String type = transactionObject.getString("type");
							if (detailsJson != null) {
								if (type != null) {
									if (type.trim().equalsIgnoreCase("Purchase")) {
										String merchantName = detailsJson.getString("merchantname");
										trRespPhy.setMessage(merchantName);

									} else if (type.trim().equalsIgnoreCase("Load")) {
										trRespPhy.setMessage("Load money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Unload")) {
										trRespPhy.setMessage("Send Money of Rs " + amount);
									} else if (type.trim().equalsIgnoreCase("Get Card")) {
										trRespPhy.setMessage("Card Fees");
									} else if (type.trim().equalsIgnoreCase("Money Transfer Debit")) {
										trRespPhy.setMessage(transactionObject.getString("description"));
									} else {
										trRespPhy.setMessage("ATM withdrawal");
									}
								} else {
									trRespPhy.setMessage("ATM withdrawal");
								}
							}
							trRespPhy.setAmount(amount);
							trRespPhy.setIndicator(indicator);
							trRespPhy.setDate(date);
							trRespPhy.setStatus(status);
							transListPhy.add(trRespPhy);
						}
					}
					resp.setCustomPhysicalTransaction(transListPhy);
					resp.setCardTransactions(responsePhy.toString());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse updateEmail(MUser userRequest, String email) {
		UserKycResponse resp = new UserKycResponse();
		try {

			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", email);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResources = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse clientResponse = webResources.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.put(ClientResponse.class, userData);
			String strResponse = clientResponse.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			if (clientResponse.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(response.getString("description"));
				return resp;
			} else {
				resp.setDetails(response.toString());
				resp.setMessage("user email updated");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse checkKYCStatus(MUser userRequest) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(userRequest);
			String userId = null;
			if (mmw != null && mmw.getMmUserId() != null) {
				userId = mmw.getMmUserId();
			} else {
				UserKycResponse userResp = getUsers(userRequest.getUserDetail().getEmail(), userRequest.getUsername());
				userId = userResp.getMmUserId();
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization()).header("X-Auth-User-Id", userId)
					.get(ClientResponse.class);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(response.getString("description"));
				return resp;
			} else {
				resp.setDetails(response.toString());
				resp.setStatus(response.getString("status"));
				resp.setMessage("KYC status fetched.");
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * CORPORATE KYC
	 */

	@Override
	public UserKycResponse kycUserMMCorporate(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("address_1", request.getAddress1());
			userData.add("address_2", request.getAddress2());
			userData.add("city", request.getCity());
			userData.add("state", request.getState());
			userData.add("country", request.getCountry());
			userData.add("zipcode", request.getPinCode());
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(request.getUser());
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/addresses/residential");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject address = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(address.getString("description"));
					return resp;
				} else {
					resp.setMessage("residential address saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse setIdDetailsCorporate(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(request.getUser());
			if (wallet != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("title", "Mr");
				userData.add("id_type", request.getIdType());
				userData.add("id_number", request.getIdNumber());
				userData.add("country_of_issue", request.getCountry());
				userData.add("birthday", sdf.format(request.getUser().getUserDetail().getDateOfBirth()));
				userData.add("gender", "male");

				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				} else {
					resp.setMessage("Id Details saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("user wallet not found");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse setImagesForKyc1(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(request.getUser());
			if (wallet != null) {
				byte[] array = recoverImageFromUrl(request.getIdImage());

				String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("data", encodedfile.trim());
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);

				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					resp.setMessage("Images saved saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("User wallet not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse setImagesForKyc2(MKycDetail request) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(request.getUser());
			if (wallet != null) {
				byte[] array = recoverImageFromUrl(request.getIdImage());

				String encodedfile = new String(Base64.encodeBase64(array), "UTF-8");
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("data", encodedfile.trim());
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					resp.setMessage("Images saved saved");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setMessage("User wallet not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse tempConfirmKycCorporate(MKycDetail detail) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(detail.getUser());
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", wallet.getMmUserId()).put(ClientResponse.class);
				String strResponse = resp1.getEntity(String.class);
				JSONObject response = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(response.getString("description"));
					return resp;
				} else {
					UserKycResponse approvalResp = tempKycStatusApproval(detail.getUser());
					if (approvalResp.getStatus().equals(ResponseStatus.SUCCESS.getValue())) {
						resp.setMessage("Kyc confirmed");
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						return resp;
					}
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User wallet not found.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse setPIN(MUser detail, String pin) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(detail);
			if (cards != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

				String encodedBlock = TrippleDSEncryption.get3desPinBlock(pin, "91" + detail.getUsername());
				userData.add("pinblock", encodedBlock.toUpperCase());

				MatchMoveWallet mmw = cards.getWallet();
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client.resource(
							MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards.getCardId() + "/pins");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(response.getString("description"));
						return resp;
					}
				} else {
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("Pin Reset successful");
					return resp;
				}
			} else {
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("User wallet not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
		return resp;
	}

	@Override
	public UserKycResponse setPINConfirmReset(MUser detail, String pin, Date dob) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MMCards cards = mmCardRepository.getPhysicalCardByUser(detail);
			if (cards != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				String encodedBlock = TrippleDSEncryption.get3desPinBlock(pin, "91" + detail.getUsername());
				userData.add("pinblock", encodedBlock);
				userData.add("birthday", sdf.format(dob));

				MatchMoveWallet mmw = cards.getWallet();
				if (mmw != null) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource resource = client.resource(
							MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cards.getCardId() + "/pins");
					ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
					String strResponse = resp1.getEntity(String.class);
					JSONObject response = new JSONObject(strResponse);
					if (resp1.getStatus() != 200) {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(response.getString("description"));
						return resp;
					} else {
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Pin Reset Confirmation");
						return resp;
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("user wallet not found.");
					return resp;
				}

			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("user cards not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public UserKycResponse debitFromWalletToPool(MatchMoveWallet wallet, String amount, String message) {
		UserKycResponse resp = new UserKycResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", wallet.getUser().getUserDetail().getEmail());
			userData.add("amount", amount);
			userData.add("message", "Deduct funds");
			JSONObject details = new JSONObject();
			details.put("pp", "CORP_DEBIT");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("Content-Type", "application/x-www-form-urlencoded").delete(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject response = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(response.getString("description"));
				return resp;
			} else {
				UserKycResponse debitStatus = deductFromCardConfirmation(wallet.getUser(), amount, message);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(debitStatus.getDebitStatus())) {
					resp.setMessage("Funds transferred to wallet");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setIndicator(response.getString("id"));
					return resp;
				} else {
					resp.setMessage("Funds transfer failed");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse moveToCardFromWallet(MUser mUser, String amount, String cardId) {
		WalletResponse resp = new WalletResponse();
		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null) {
				MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
				userData.add("amount", amount);
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));

				WebResource resource = client
						.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/" + cardId + "/funds");
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);

				JSONObject user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					return resp;
				} else {
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setMessage("Tranfered sucessfully");
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			} else {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setMessage("User wallet not found.");
				resp.setCode(ResponseStatus.FAILURE.getValue());
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	/**
	 * Custom method for doing refunds to wallet
	 * 
	 * @param mUser
	 * @param amount
	 * @return
	 */

	@Override
	public WalletResponse refundToWalletFromCustomAdmin(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String ipAdrr) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "REFUND");
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(user.getString("confirm"));
				MService service = mServiceRepository.findServiceByCode(serviceCode);
				String transactionRefNo = System.currentTimeMillis() + "C";
				MTransaction transaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo);
				if (transaction == null) {
					transaction = new MTransaction();
					transaction.setAmount(Double.parseDouble(amount));
					transaction.setTransactionRefNo(transactionRefNo);
					transaction.setRetrivalReferenceNo(user.getString("id"));
					transaction.setTransactionType(TransactionType.REFUND);
					transaction.setDescription("Refund of Rs " + amount + " from Admin");
					transaction.setCardLoadStatus("Pending");
					transaction.setCurrentBalance(mUser.getAccountDetail().getBalance());
					if (service != null) {
						transaction.setService(service);
					}
					transaction.setAuthReferenceNo(trxIdOfOriginalTrx);
					transaction.setRemarks(ipAdrr);
					transaction.setAccount(mUser.getAccountDetail());
					mTransactionRepository.save(transaction);
				}
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse initiateLoadFundsToMMWalletPG(MUser mUser, String amount, String transactionRefNo,
			String upiid) {
		WalletResponse resp = new WalletResponse();
		Security.addProvider(new BouncyCastleProvider());
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "PG");
			details.put("payment_ref", transactionRefNo);
			details.put("status", "Success");
			details.put("description", upiid);
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Load Initiated");
				resp.setStatus(user.getString("confirm"));
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse reversalToWalletForRechargeFailure(MUser mUser, String amount, String serviceCode,
			String trxIdOfOriginalTrx, String desc) {
		WalletResponse resp = new WalletResponse();
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("email", mUser.getUserDetail().getEmail());
			userData.add("amount", amount);
			JSONObject details = new JSONObject();
			details.put("pp", "REVERSAL_BILLPAY");
			details.put("payment_ref", trxIdOfOriginalTrx);
			details.put("description", desc);
			userData.add("details", details.toString());

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/funds");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				return resp;
			} else {
				resp.setWalletId(user.getString("wallet_id"));
				resp.setAuthRetrivalNo(user.getString("id"));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Reversal");
				resp.setStatus(user.getString("confirm"));
				MService service = mServiceRepository.findServiceByCode(serviceCode);
				String transactionRefNo = System.currentTimeMillis() + "C";
				MTransaction transaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo);
				if (transaction == null) {
					transaction = new MTransaction();
					transaction.setAmount(Double.parseDouble(amount));
					transaction.setTransactionRefNo(transactionRefNo);
					transaction.setRetrivalReferenceNo(user.getString("id"));
					transaction.setTransactionType(TransactionType.REVERSAL);
					transaction.setStatus(Status.Success);
					transaction.setDescription(
							"Reversal of Rs " + amount + " for bill_pay & recharges for " + trxIdOfOriginalTrx);
					transaction.setCardLoadStatus(Status.Success.getValue());
					transaction.setCurrentBalance(mUser.getAccountDetail().getBalance());
					if (service != null) {
						transaction.setService(service);
					}
					transaction.setAuthReferenceNo(trxIdOfOriginalTrx);
					transaction.setAccount(mUser.getAccountDetail());
					mTransactionRepository.save(transaction);
				}
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public ResponseDTO corpCreateUserOnMM(MUser request) {
		ResponseDTO resp = new ResponseDTO();
		MultivaluedMap<String, String> userData = new MultivaluedMapImpl();

		userData.add("email", request.getUserDetail().getEmail());
		try {
			userData.add("password", SecurityUtil.md5(request.getUsername()));
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		String fName = request.getUserDetail().getFirstName();
		String lName = request.getUserDetail().getLastName();

		userData.add("first_name", fName);
		userData.add("last_name", lName);
		userData.add("mobile_country_code", "91");
		userData.add("mobile", request.getUsername());
		if (request.getUserDetail().getMiddleName() != null) {
			userData.add("middle_name", request.getUserDetail().getMiddleName());
		}

		if (lName != null) {
			if (fName.length() + lName.length() + 1 > 25) {
				userData.add("preferred_name", fName);
			} else {
				userData.add("preferred_name", fName + " " + lName);
			}
		} else {
			userData.add("preferred_name", fName);
		}

		try {

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.post(ClientResponse.class, userData);
			String strResponse = resp1.getEntity(String.class);
			JSONObject user = new JSONObject(strResponse);
			if (resp1.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(user.getString("description"));
				return resp;
			} else {
				resp.setUserCreated(true);
				resp.setInfo(user.getString("id"));
				request.setFullyFilled(true);
				userRepository.save(request);
				MatchMoveWallet moveWallet = new MatchMoveWallet();
				moveWallet.setMmUserId(user.getString("id"));
				moveWallet.setUser(request);
				moveWallet = matchMoveWalletRepository.save(moveWallet);

				resp.setStatus(ResponseStatus.SUCCESS.getKey());
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("User created successfully");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public ResponseDTO corpCreateWalletOnMM(MUser request) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MatchMoveWallet moveWallet = matchMoveWalletRepository.findByUser(request);
			if (moveWallet != null) {
				String mmUserId = moveWallet.getMmUserId();
				setComplianceDetails(request, mmUserId);

				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(request.getUserDetail().getEmail());
				cardRequest.setPassword(SecurityUtil.md5(request.getUsername()));
				cardRequest.setIdNo(mmUserId);
				WalletResponse walletResponse = createWallet(cardRequest);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
					moveWallet.setWalletId(walletResponse.getWalletId());
					moveWallet.setWalletNumber(walletResponse.getWalletNumber());
					matchMoveWalletRepository.save(moveWallet);
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("wallet created successfully");
					return resp;
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(walletResponse.getMessage());
					return resp;
				}
			} else {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User wallet not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage("exception occured while creating corporate Wallet.");
			return resp;
		}
	}

	@Override
	public WalletResponse assignPhysicalCard(MUser mUser, String proxy_number) {
		WalletResponse resp = new WalletResponse();
		try {
			if (proxy_number != null && proxy_number.length() != 12) {
				proxy_number = CommonUtil.getProxyNumber(proxy_number);
			}
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("assoc_number", "PY" + proxy_number);

			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(mUser);
			if (mmw != null && mmw.getMmUserId() != null && !mmw.getMmUserId().isEmpty()) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets/cards/"
						+ MatchMoveUtil.MATCHMOVE_CARDTYPE_PHYSICAL);
				ClientResponse resp1 = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-Id", mmw.getMmUserId()).post(ClientResponse.class, userData);
				String strResponse = resp1.getEntity(String.class);
				JSONObject user = new JSONObject(strResponse);
				if (resp1.getStatus() != 200) {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(user.getString("description"));
					return resp;
				} else {
					MMCards cards = mmCardRepository.getPhysicalCardByUser(mUser);
					if (cards == null) {
						cards = new MMCards();
						cards.setCardId(user.getString("id"));
						cards.setHasPhysicalCard(true);
						cards.setStatus(Status.Inactive.getValue());
						cards.setWallet(mmw);
						cards.setActivationCode(user.getString("activation_code"));
						cards = mmCardRepository.save(cards);
						PhysicalCardDetails phyDetails = physicalCardDetailRepository.findByWallet(mmw);
						if (phyDetails == null) {
							phyDetails = new PhysicalCardDetails();
							phyDetails.setStatus(Status.Received);
							phyDetails.setCards(cards);
							phyDetails.setWallet(mmw);
							phyDetails.setProxyNumber(proxy_number);
							phyDetails.setActivationCode(user.getString("activation_code"));
							phyDetails.setCards(cards);
							phyDetails = physicalCardDetailRepository.save(phyDetails);
							resp.setActivationCode(user.getString("activation_code"));
							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());
							resp.setMessage("Card successfully assigned");
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE, mUser,
									user.getString("activation_code"));
							return resp;
						} else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Request already saved.");
							return resp;
						}
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Physical card is already assigned.");
						return resp;
					}
				}
			} else {
				resp.setStatus(ResponseStatus.FAILURE.getKey());
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("User wallet not found.");
				return resp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
			return resp;
		}
	}

	@Override
	public WalletResponse refundDebitTxns(String txnId, MUser user) {
		WalletResponse resp = new WalletResponse();

		try {
			MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client
					.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/oauth/consumer/wallet/transaction/" + txnId);
			ClientResponse clientResponse = resource.type(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.header("Authorization", MatchMoveUtil.getBasicAuthorization())
					.header("X-Auth-User-Id", mmw.getMmUserId()).delete(ClientResponse.class);

			String strResponse = clientResponse.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);

			if (jobj != null && jobj.length() > 0) {
				JSONObject txn = jobj.getJSONObject("transaction");
				if (txn != null && txn.length() > 0) {
					JSONObject reversal = txn.getJSONObject("reversal");
					if (reversal != null && reversal.length() > 0) {
						String id = reversal.getString("id");
						resp.setAuthRetrivalNo(id);
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Reverse done.");
						return resp;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.EXCEPTION_OCCURED.getKey());
			resp.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			resp.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
		}

		resp.setStatus(ResponseStatus.FAILURE.getKey());
		resp.setCode(ResponseStatus.FAILURE.getValue());
		resp.setMessage("Ohh!! snap, please try again later.");
		return resp;
	}

	private String setComplianceDetails(MUser user, String mmUserId) {
		try {
			MultivaluedMap<String, String> userData = new MultivaluedMapImpl();
			userData.add("title", "Mr");
			userData.add("id_type", user.getUserDetail().getIdType());
			userData.add("id_number", user.getUserDetail().getIdNo());
			userData.add("country_of_issue", "India");
			userData.add("gender", "male");

			try {
				userData.add("birthday", CommonUtil.DATEFORMATTER.format(user.getUserDetail().getDateOfBirth()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			String authorization = SMSConstant.getBasicAuthorization();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users");
			ClientResponse response = resource.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
					.header("X-Auth-User-Id", mmUserId).header("Authorization", authorization)
					.put(ClientResponse.class, userData);
			String strResp = response.getEntity(String.class);

			JSONObject mmResp = new JSONObject(strResp);

			if (strResp != null) {
				mmResp = new JSONObject(strResp);
				if (mmResp != null && mmResp.has("code")) {
					String code = mmResp.getString("code");
					if (!CommonValidation.isNull(code) && !"200".equalsIgnoreCase(code)) {
						return ResponseStatus.SUCCESS.getValue();
					}
				} else {
					return ResponseStatus.SUCCESS.getValue();
				}
			}
			return ResponseStatus.FAILURE.getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}