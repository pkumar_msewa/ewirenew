package com.msscard.app.api.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.msscard.app.api.ISessionApi;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.ConvertUtil;
import com.msscards.session.UserDetailsWrapper;
import com.msscards.session.UserSessionDTO;


@Transactional
public class SessionApi implements ISessionApi, InitializingBean, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unused")
	private MessageSource messageSource;
	private final UserSessionRepository userSessionRepository;
	private final MUserRespository userRepository;
	private final int sessionExpirtyTime;

	public SessionApi(UserSessionRepository userSessionRepository,
			MUserRespository userRepository, int sessionExpirtyTime) {
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
		this.sessionExpirtyTime = sessionExpirtyTime;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public void registerNewSession(String sessionId,
			UserDetailsWrapper principal) {
		UserSession userSession = userSessionRepository
				.findBySessionId(sessionId);
		if (userSession == null) {
			userSession = new UserSession();
			userSession.setSessionId(sessionId);
		}
		userSession.setUser(userRepository.findByUsername(principal
				.getUsername()));
		userSession.setLastRequest(new Date());
		userSessionRepository.save(userSession);
	}

	@Override
	public void removeSession(String tokenKey) {
		UserSession userSession = userSessionRepository
				.findBySessionId(tokenKey);
		if (userSession != null) {
			userSessionRepository.delete(userSession);
		}
	}

	@Override
	public boolean checkActiveSession(MUser user) {
		boolean valid = true;
		System.err.println("inside check active session");
		if (user != null) {
			System.err.println("user is not null");
			List<UserSession> session = userSessionRepository.findByActiveUserSession(user);
			if (session != null) {
				if(session.size()>0){
					UserSession sess=(UserSession)session.get(0);
				System.err.println("session is not null");
				if (!sess.isExpired()) {
					System.err.println("session is not expired");
					sess.setExpired(true);
					userSessionRepository.save(session);
				
				} 
			} 
		}
		}
		return valid;
	}

	@Override
	public UserSession getUserSession(String sessionId) {
		return userSessionRepository.findBySessionId(sessionId);
	}
	
	@Override
	public UserSession getActiveUserSession(String sessionId) {
		return userSessionRepository.findByActiveSessionId(sessionId);
	}

	@Override
	public boolean getUserOnlineStatus(MUser user) {
        boolean online = false;
        try {
            online =  !(userSessionRepository.findOnlineStatus(user));
        }catch(NullPointerException ex){

        }
        return online;
	}

	@Override
	public void refreshSession(String sessionId) {
		userSessionRepository.refreshSession(sessionId);
	}

	@Override
	public List<UserSession> getAllUserSession(long userId,
			boolean includeExpiredSessions) {
		if (includeExpiredSessions) {
			return userSessionRepository
					.getUserSessionsIncludingExpired(userId);
		}
		return userSessionRepository.getUserSessions(userId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MINUTE, -sessionExpirtyTime);
		userSessionRepository.deleteExpiredSessions(c.getTime());
	}

	@Override
	public void expireSession(String sessionId) {
		UserSession session = getUserSession(sessionId);
		session.setExpired(true);
		userSessionRepository.save(session);
	}

	@Override
	public long countActiveSessions() {
		return userSessionRepository.countActiveSessions();
	}

	@Override
	public Page<MUser> findOnlineUsers(Pageable page) {
		return userSessionRepository.findActiveUsers(page);
	}

	@Override
	public List<MUser> findOnlineUsers() {

		return null;
	}

	@Override
	public List<MUser> findOnlineUsersBetween(Date start, Date from) {
		return null;
	}

	@Override
	public Page<UserSession> findActiveSessions(Pageable page) {
		return userSessionRepository.findActiveSessions(page);
	}

	@Override
	public void registerNewSession(String sessionId, MUser principal) {
		UserSession userSession = userSessionRepository
				.findBySessionId(sessionId);
		if (userSession == null) {
			userSession = new UserSession();
			userSession.setSessionId(sessionId);
		}
		userSession.setUser(userRepository.findByUsername(principal
				.getUsername()));
		userSession.setLastRequest(new Date());
		userSessionRepository.save(userSession);
	}

	@Override
	public List<UserSessionDTO> getAllActiveUser() {
		List<UserSession> userSession = userSessionRepository.findActiveUser();
		return ConvertUtil.convertSessionList(userSession);
	}
	
	@Override
	public void clearAllSessionForUser(MUser user) {
		List<UserSession> userSessions = userSessionRepository.getUserSessions(user.getId());
		for (UserSession userSession : userSessions) {
			expireSession(userSession.getSessionId());
		}
	}
}

