package com.msscard.app.api.impl;

import org.json.JSONObject;

import com.msscard.app.api.ILoadMoneyApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi; 
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.response.TransactionRedirectResponse;
import com.msscard.app.model.response.TransactionVerificationResponse;
import com.msscard.app.model.response.UPIResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MServiceRepository;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class LoadMoneyApiImpl implements ILoadMoneyApi {

	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final MServiceRepository serviceRepository;
	private final MCommissionRepository mCommissionRepository;

	public LoadMoneyApiImpl(ITransactionApi transactionApi, IUserApi userApi, MServiceRepository serviceRepository,
			MCommissionRepository mCommissionRepository) {
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.serviceRepository = serviceRepository;
		this.mCommissionRepository = mCommissionRepository;
	}

	@Override
	public MTransaction initiateTransaction(String username, double amount, String servic) {
		try {
			MUser user = userApi.findByUserName(username);
			MService service = serviceRepository.findServiceByCode(servic);
			if(service!=null && Status.Active.equals(service.getStatus())) {
				MCommission commission = mCommissionRepository.findCommissionByService(service);
				TransactionalRequest transactionRequest = new TransactionalRequest();
				transactionRequest.setAmount(amount);
				transactionRequest.setDescription(service.getDescription());
				transactionRequest.setService(service);
				transactionRequest.setUser(user);
				if (commission != null) {
					transactionRequest.setCommission(commission);
				}
				return transactionApi.initiateUPITransaction(transactionRequest);
			}else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public MTransaction initiateUPITransaction(String username, double amount, String servic, boolean andriod) {
		try {
			MUser user = userApi.findByUserName(username);
			MService service = serviceRepository.findServiceByCode(servic);
			MCommission commission = mCommissionRepository.findCommissionByService(service);
			TransactionalRequest transactionRequest = new TransactionalRequest();
			transactionRequest.setAmount(amount);
			transactionRequest.setDescription(service.getDescription());
			transactionRequest.setService(service);
			transactionRequest.setAndroid(andriod);
			transactionRequest.setUser(user);
			if (commission != null) {
				transactionRequest.setCommission(commission);
			}
			return transactionApi.initiatePhyCardTransaction(transactionRequest);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public TransactionRedirectResponse redirectResponse(TransactionInitiateRequest initiateRequest) {
		TransactionRedirectResponse redirectResponse = new TransactionRedirectResponse();
		try {
			if (initiateRequest.getStatus().equalsIgnoreCase("Captured")) {
				TransactionVerificationResponse verificationResponse = verifyRazorPay(initiateRequest);
				if (verificationResponse.isCaptured()) {
					transactionApi.successLoadMoney(initiateRequest.getTransactionRefNo(),
							initiateRequest.getPaymentId());
					redirectResponse.setSuccess(true);
					return redirectResponse;
				} else {
					transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
					redirectResponse.setSuccess(false);
					return redirectResponse;
				}
			} else {
				transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
				redirectResponse.setSuccess(false);
				return redirectResponse;
			}
		} catch (Exception e) {
			e.printStackTrace();
			transactionApi.failedLoadMoney(initiateRequest.getTransactionRefNo());
			redirectResponse.setSuccess(false);
			return redirectResponse;
		}
	}

	@Override
	public TransactionVerificationResponse verifyRazorPay(TransactionInitiateRequest dto) {
		TransactionVerificationResponse resp = new TransactionVerificationResponse();
		try {
			double amt = Double.valueOf(dto.getAmount());
			RazorpayClient razorpayClient = new RazorpayClient(RazorPayConstants.KEY_ID, RazorPayConstants.KEY_SECRET);
			org.json.JSONObject options = new org.json.JSONObject();
			options.put("amount", amt);
			Payment strResponse = razorpayClient.Payments.capture(dto.getPaymentId(), options);
			JSONObject jObj = new JSONObject(strResponse.toString());
			final String status = jObj.getString("status");
			if (status.equalsIgnoreCase("captured")) {
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
				resp.setResponse("00");
			} else {
				resp.setMessage(jObj.getString("description"));
				resp.setResponse("11");
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCaptured(false);
			resp.setResponse("11");
		}
		return resp;
	}

	@Override
	public UPIResponseDTO checkUPIStatus(String transactionId, String merchantId) {
		UPIResponseDTO upiResponse = new UPIResponseDTO();
		org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
		try {
			payload.put("merchantId", RazorPayConstants.UPI_MERCHANT_ID);
			payload.put("merchantRefNo", transactionId);
			Client client = Client.create();

			WebResource webResource = client.resource(RazorPayConstants.UPI_STATUS);
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = clientResponse.getEntity(String.class);
			System.err.println(strResponse);
			if (strResponse != null) {
				org.codehaus.jettison.json.JSONObject jObject = new org.codehaus.jettison.json.JSONObject(strResponse);
				if (jObject != null && clientResponse.getStatus() == 200) {
					upiResponse.setAmount(jObject.getString("amount"));
					upiResponse.setCode(jObject.getString("code"));
					upiResponse.setMerchantRefNo(jObject.getString("txnRefId"));
					upiResponse.setPaymentId(jObject.getString("txnId"));
					upiResponse.setStatus(jObject.getString("status"));

					return upiResponse;
				} else {
					upiResponse.setCode(ResponseStatus.FAILURE.getValue());
					upiResponse.setStatus(ResponseStatus.FAILURE.getKey());
					return upiResponse;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			upiResponse.setCode(ResponseStatus.FAILURE.getValue());
			upiResponse.setStatus(ResponseStatus.FAILURE.getKey());
			return upiResponse;
		}
		upiResponse.setCode(ResponseStatus.FAILURE.getValue());
		upiResponse.setStatus(ResponseStatus.FAILURE.getKey());
		return upiResponse;
	}
}