package com.msscard.app.api.impl;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;

import com.msscard.app.api.FCMSenderApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.FCMDetails;
import com.msscard.entity.MUser;
import com.msscard.model.FCMResponseDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.repositories.MUserRespository;
import com.msscards.metadatas.URLMetadatas;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

public class FCMSenderApiImpl implements FCMSenderApi {

	private final MUserRespository userRepository;

	public FCMSenderApiImpl(MUserRespository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public FCMResponseDTO sendNotification(FCMDetails dto) {
		FCMResponseDTO fcmResp = new FCMResponseDTO();
		try {
			if (!dto.isForIos()) {
				if (dto.isAllUsers()) {
					List<String> gcmIdsList = userRepository.getAllGcmIds();
					if (gcmIdsList != null && !gcmIdsList.isEmpty()) {
						JSONObject jObject = new JSONObject();
						JSONObject dataObject = new JSONObject();
						JSONArray ids = new JSONArray();
						for (String string : gcmIdsList) {
							ids.put(string);
						}
						dataObject.put("title", dto.getTitle());
						dataObject.put("message", dto.getMessage());
						if (dto.isHasImage()) {
							dataObject.put("image", dto.getImagePath());
						}
						jObject.put("registration_ids", ids);
						jObject.put("data", dataObject);
						Client client = Client.create();
						WebResource webResource = client.resource("https://fcm.googleapis.com/fcm/send");
						client.addFilter(new LoggingFilter(System.out));
						ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
								.header("Authorization", "key=AIzaSyBXsuXqEioksmRn5fBnbiUjAaBXzV5qZzw")
								.post(ClientResponse.class, jObject);
						String stringResponse = clientResponse.getEntity(String.class);
						System.err.println("fcm response:: --> " + stringResponse);
						if (stringResponse != null) {
							JSONObject jResponse = new JSONObject(stringResponse);
							int success = jResponse.getInt("success");
							if (success == 1) {
								fcmResp.setSuccess(true);
							} else {
								fcmResp.setSuccess(false);
							}
						}
					}
				} else {
					if (dto.isSingleUser()) {
						List<String> user = userRepository.getGcmByUsername(dto.getUsername());
						if (user != null) {
							JSONObject jObject = new JSONObject();
							JSONObject dataObject = new JSONObject();
							JSONArray ids = new JSONArray();
							dataObject.put("title", dto.getTitle());
							dataObject.put("message", dto.getMessage());
							if (dto.isHasImage()) {
								dataObject.put("image", dto.getImagePath());
							}
							for (String string : user) {
								ids.put(string);
							}
							jObject.put("registration_ids", ids);
							jObject.put("data", dataObject);
							System.err.println(jObject.toString());
							Client client = Client.create();
							WebResource webResource = client.resource("https://fcm.googleapis.com/fcm/send");
							ClientResponse response = webResource.accept("application/json").type("application/json")
									.header("Authorization", "key=AIzaSyBXsuXqEioksmRn5fBnbiUjAaBXzV5qZzw")
									.post(ClientResponse.class, jObject);
							String stringResponse = response.getEntity(String.class);
							System.err.println("single user fcm response::--> " + stringResponse);

							if (stringResponse != null) {
								JSONObject jResponse = new JSONObject(stringResponse);
								int success = jResponse.getInt("success");
								if (success == 1) {
									fcmResp.setSuccess(true);
								} else {
									fcmResp.setSuccess(false);
								}
							}
						}
					}
				}
			} else {
				List<String> gcmIdsList = userRepository.getAllGcmIds();
				if (gcmIdsList != null && !gcmIdsList.isEmpty()) {
					for (String string : gcmIdsList) {
						if (string != null && string != "") {
							if (dto.isAllUsers()) {
								@SuppressWarnings("unused")
								ResponseDTO respDto = IOSNotificationSender(dto.getTitle(), dto.getMessage(),
										dto.getImagePath(), string);

							}
						}
					}
					if (dto.isSingleUser()) {

						MUser user = userRepository.findByUsername(dto.getUsername());
						ResponseDTO respDto = IOSNotificationSender(dto.getTitle(), dto.getMessage(),
								dto.getImagePath(), user.getGcmId());
						if (respDto.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							fcmResp.setSuccess(true);
							return fcmResp;
						} else {
							fcmResp.setSuccess(false);
							return fcmResp;
						}

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			fcmResp.setSuccess(false);
			return fcmResp;
		}
		fcmResp.setSuccess(true);
		return fcmResp;
	}

	public static ResponseDTO IOSNotificationSender(String message, String body, String imageUrl, String user) {
		ResponseDTO resp = new ResponseDTO();
		BasicConfigurator.configure();
		try {
			PushNotificationPayload payload = PushNotificationPayload.complex();
			payload.addAlert(message);
			payload.addBadge(1);
			payload.addSound("default");
			payload.addCustomDictionary("imageUrl", imageUrl);
			payload.addCustomDictionary("body", body);
			System.out.println(payload.toString());
			System.err.println(payload.getPayloadSize());
			if (payload.getPayloadSize() < 256) {
				List<PushedNotification> NOTIFICATIONS = Push.payload(payload, URLMetadatas.IOS_CERT, "", true, user);
				for (PushedNotification NOTIFICATION : NOTIFICATIONS) {
					if (NOTIFICATION.isSuccessful()) {
						/* APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT */
						System.out.println(
								"PUSH NOTIFICATION SENT SUCCESSFULLY TO: " + NOTIFICATION.getDevice().getToken());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Notification Sent Successfully");
						return resp;
					} else {
						Exception THEPROBLEM = NOTIFICATION.getException();
						THEPROBLEM.printStackTrace();
						ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();

						if (THEERRORRESPONSE != null) {
							System.out.println(THEERRORRESPONSE.getMessage());
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage(THEERRORRESPONSE.getMessage());
							return resp;
						}
					}
				}
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Size of Payload shouldn't be more than 256 bytes");
				return resp;
			}
		} catch (CommunicationException e) {
			e.printStackTrace();
		} catch (KeystoreException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;

	}

}
