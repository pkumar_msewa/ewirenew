package com.msscard.app.api.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.cashiercards.errormessages.ErrorMessage;
import com.imps.entity.ImpsP2ADetail;
import com.imps.model.ImpsRequest;
import com.imps.model.ImpsResponse;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.AllTransactionRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.PGStatus;
import com.msscard.app.model.response.PGStatusCheckDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.model.CashBackDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionListDTO;
import com.msscard.model.TransactionType;
import com.msscard.repositories.ImpsP2ADetailRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.util.CommonUtil;
import com.msscard.util.MailTemplate;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.TransactionTypeValidation;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class TransactionApi implements ITransactionApi {

	private final MTransactionRepository transactionRepository;
	private final MPQAccountDetailRepository accountDetailRepository;
	private final MUserRespository userRepository;
	private final IUserApi userApi;
	private final ISMSSenderApi senderApi;
	private final MServiceRepository mServiceRepository;
	private final MCommissionRepository mCommissionRepository;
	private final ImpsP2ADetailRepository impsP2ADetailRepository;
	private final MUserRespository mUserRespository;
	private final TransactionTypeValidation transactionTypeValidation;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository mMCardRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final IMailSenderApi mailSenderApi;

	public TransactionApi(MTransactionRepository transactionRepository,
			MPQAccountDetailRepository accountDetailRepository, MUserRespository userRepository, IUserApi userApi,
			ISMSSenderApi senderApi, MServiceRepository mServiceRepository, MCommissionRepository mCommissionRepository,
			ImpsP2ADetailRepository impsP2ADetailRepository, MUserRespository mUserRespository,
			TransactionTypeValidation transactionTypeValidation, IMatchMoveApi matchMoveApi,
			MMCardRepository mMCardRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			IMailSenderApi mailSenderApi) {
		this.transactionRepository = transactionRepository;
		this.accountDetailRepository = accountDetailRepository;
		this.userRepository = userRepository;
		this.userApi = userApi;
		this.senderApi = senderApi;
		this.mServiceRepository = mServiceRepository;
		this.mCommissionRepository = mCommissionRepository;
		this.impsP2ADetailRepository = impsP2ADetailRepository;
		this.mUserRespository = mUserRespository;
		this.transactionTypeValidation = transactionTypeValidation;
		this.matchMoveApi = matchMoveApi;
		this.mMCardRepository = mMCardRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.mailSenderApi = mailSenderApi;
	}

	@Override
	public long getTransactionCountForAdmin() {
		long totalTxn = 0;
		try {
			totalTxn = transactionRepository.getTransactionCountForAdmin(Status.Success,
					userRepository.findAllAccountByUser());
		} catch (Exception e) {
			e.printStackTrace();
			return totalTxn;
		}
		return totalTxn;
	}

	@Override
	public Double getTransactionAmountForAdmin() {
		Double totalTxnAmount = 0.0;
		try {
			totalTxnAmount = transactionRepository.getCreditTransactionAmountForAdmin(Status.Success,
					userRepository.findAllAccountByUser());
		} catch (Exception e) {
			e.printStackTrace();
			return totalTxnAmount;
		}
		return totalTxnAmount;
	}

	@Override
	public Double getTransactionAmount() {
		if (transactionRepository.getTotalLoadMoney() == null) {
			return 0.0;
		} else {
			return transactionRepository.getTotalLoadMoney();
		}

	}

	@Override
	public Date getLastTranasactionTimeStamp(MPQAccountDetails account, boolean res) {
		return transactionRepository.getTimeStampOfLastTransaction(account, res);
	}

	@Override
	public MTransaction getLastTransactionDetails(Date from, MPQAccountDetails account, boolean res) {
		return transactionRepository.getLastTransactionDetails(from, account, res);
	}

	@Override
	public Date getLastTransactionTimeStampByStatus(MPQAccountDetails account, Status status) {
		return transactionRepository.getTimeStampOfLastTransactionByStatus(account, status);
	}

	@Override
	public List<MTransaction> findTransactions(AllTransactionRequest dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return transactionRepository.findRecentTransactions(pageable);
	}

	@Override
	public MTransaction initiatePhyCardTransaction(TransactionalRequest request) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double transactionAmount = request.getAmount();
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			double finalTransactionAmount = transactionAmount;
			if (request.getCommission() != null) {
				double comm = CommonUtil.commissionEarned(request.getCommission(), transactionAmount);
				finalTransactionAmount = comm + transactionAmount;
				BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
				BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				finalTransactionAmount = roundOff.doubleValue();
			}
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(request.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(request.getService());
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setCardLoadStatus("Pending");
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			if (request.getCommission() != null) {
				senderTransaction
						.setCommissionEarned(CommonUtil.commissionEarned(request.getCommission(), transactionAmount));
			}
			transactionExists = transactionRepository.save(senderTransaction);
		}

		return transactionExists;
	}

	@Override
	public MTransaction initiateUPITransaction(TransactionalRequest request) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double transactionAmount = request.getAmount();
			double finalTransactionAmount = request.getAmount();
			if (request.getCommission() != null) {
				double comm = CommonUtil.commissionEarned(request.getCommission(), transactionAmount);
				finalTransactionAmount = comm + transactionAmount;
				BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
				BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				finalTransactionAmount = roundOff.doubleValue();
			}
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			if (request.getCommission() != null) {
				senderTransaction
						.setCommissionEarned(CommonUtil.commissionEarned(request.getCommission(), transactionAmount));
			}
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(request.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(request.getService());
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setFavourite(false);
			senderTransaction.setCardLoadStatus(Status.Pending.getValue());
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			transactionExists = transactionRepository.save(senderTransaction);
		}

		return transactionExists;
	}

	/* FOR INITIATE MDEX TRANSACTION DEBIT */
	@Override
	public MTransaction initiateMdexTransactionDebit(TransactionInitiateRequest request) {
		MService service = mServiceRepository.findServiceByCode(request.getServiceCode());
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double finalTransactionAmount = Double.parseDouble(request.getAmount());
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(service.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setFavourite(false);
			senderTransaction.setCardLoadStatus("Pending");
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			transactionExists = transactionRepository.save(senderTransaction);
		}

		return transactionExists;
	}

	@Override
	public MTransaction initiateMdexTransactionDebitnew(TransactionInitiateRequest request) {
		MService service = mServiceRepository.findServiceByCode(request.getServiceCode());
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
		if (service != null && Status.Active.getValue().equalsIgnoreCase(service.getStatus().getValue())) {
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double finalTransactionAmount = Double.parseDouble(request.getAmount());
				double senderUserBalance = request.getUser().getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance;
				senderTransaction.setAmount(finalTransactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + request.getAmount());
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "D");
				senderTransaction.setDebit(true);
				senderTransaction.setAndriod(request.isAndroid());
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus(Status.Pending.getValue());
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(request.getUser().getAccountDetail());
				senderTransaction.setStatus(Status.Initiated);
				senderTransaction.setMdexTransaction(true);
				transactionExists = transactionRepository.save(senderTransaction);
			}
		} else {
			return null;
		}
		return transactionExists;
	}

	@Override
	public MTransaction initiateMdexTransactionDebitnewBus(TransactionInitiateRequest request) {
		MService service = mServiceRepository.findServiceByCode(request.getServiceCode());
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
		MCommission senderCommission = transactionTypeValidation.findCommissionByService(service);
		double netCommissionValue = transactionTypeValidation.getCommissionValue(senderCommission,
				Double.parseDouble(request.getAmount()));
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double finalTransactionAmount = Double.parseDouble(request.getAmount()) + netCommissionValue;
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance - finalTransactionAmount;
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(service.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setFavourite(false);
			senderTransaction.setMdexTransaction(true);
			senderTransaction.setCardLoadStatus("Pending");
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setMdexTransaction(true);
			transactionExists = transactionRepository.save(senderTransaction);
		}

		return transactionExists;
	}

	// fail bus
	@Override
	public void failedBusPayment(String transactionRefNo) {

		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			double netTransactionAmount = senderTransaction.getAmount();
			MPQAccountDetails account = senderTransaction.getAccount();
			double senderUserBalance = senderTransaction.getAccount().getBalance();
			double senderCurrentBalance = senderUserBalance + senderTransaction.getAmount();

			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				account.setBalance(senderCurrentBalance);
				mPQAccountDetailRepository.save(account);
				transactionRepository.save(senderTransaction);
			}
		}
	}

	// mdex CREDIT

	@Override
	public MTransaction initiateMdexTransactionCredit(TransactionInitiateRequest request) {
		MService service = mServiceRepository.findServiceByCode("UPS");
		String transactionRefNo = String.valueOf(System.currentTimeMillis());

		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double finalTransactionAmount = Double.parseDouble(request.getAmount());
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + finalTransactionAmount;
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(service.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setFavourite(false);
			senderTransaction.setCardLoadStatus("Pending");
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setAndriod(true);
			transactionExists = transactionRepository.save(senderTransaction);
		}

		return transactionExists;
	}

	/* END OF INITIATE MDEX TRANSACTION */

	@Override
	public MTransaction getTransactionByRefNo(String string) {
		return transactionRepository.findByTransactionRefNo(string);
	}

	@Override
	public MTransaction getTransactionByRetReferNo(String paymentId) {
		return transactionRepository.findByRetreivalReferenceNo(paymentId);
	}

	@Override
	public void successLoadMoney(String transactionRefNo, String paymentId) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			MUser sender = userApi.findByUserAccount(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(paymentId);
			senderTransaction = transactionRepository.save(senderTransaction);
			MPQAccountDetails senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			accountDetailRepository.save(senderAccount);
			senderApi.sendTransactionSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.LOADMONEY_SUCCESS, sender, null,
					senderTransaction);
		}
	}

	@Override
	public void failedLoadMoney(String transactionRefNo) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public MTransaction successLoadMoneyUPI(String transactionRefNo, String paymentId, String payerVA, String payerName,
			String payerMobile) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			MUser sender = userApi.findByUserAccount(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(paymentId);
			senderTransaction.setRemarks(payerName + "|" + payerMobile);
			senderTransaction = transactionRepository.save(senderTransaction);
			MPQAccountDetails senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			accountDetailRepository.save(senderAccount);
			senderApi.sendTransactionSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.LOADMONEY_SUCCESS, sender, null,
					senderTransaction);
			mailSenderApi.sendLoadMoney("Load Money Successful", MailTemplate.LOAD_MONEY, sender, senderTransaction);
		}
		return senderTransaction;
	}

	@Override
	public MTransaction failedLoadMoneyUPI(String transactionRefNo) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if (senderTransaction != null) {
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Failed);
				transactionRepository.save(senderTransaction);
			}
		}
		return senderTransaction;
	}

	@Override
	public double getMonthlyCreditTransationTotalAmount(MPQAccountDetails account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try {
			amount = transactionRepository.getMonthlyCreditTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		} catch (NullPointerException e) {
			return amount;
		}
		return amount;
	}

	@Override
	public double getDailyCreditTransationTotalAmount(MPQAccountDetails account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try {
			amount = transactionRepository.getDailyCreditTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		} catch (NullPointerException e) {
			return amount;
		}
		return amount;
	}

	@Override
	public double getDailyDebitTransationTotalAmount(MPQAccountDetails account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try {
			amount = transactionRepository.getDailyDebitTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		} catch (NullPointerException e) {
			return amount;
		}
		return amount;
	}

	@Override
	public void doPromocodeTransaction(MUser user, String loadAmount) {
		try {
			String transactionRefNo = String.valueOf(System.currentTimeMillis());
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
			MService service = mServiceRepository.findServiceByCode(RazorPayConstants.PROMO_SERVICES);
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = user.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance + transactionAmount;
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus(Status.Success + "");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(user.getAccountDetail());
				senderTransaction.setStatus(Status.Success);
				transactionExists = transactionRepository.save(senderTransaction);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doPromocodeFailedTransaction(MUser user, String loadAmount) {
		try {
			String transactionRefNo = String.valueOf(System.currentTimeMillis());
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
			MService service = mServiceRepository.findServiceByCode(RazorPayConstants.PROMO_SERVICES);
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = user.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance + transactionAmount;
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus(Status.Failed + "");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(user.getAccountDetail());
				senderTransaction.setStatus(Status.Failed);
				transactionExists = transactionRepository.save(senderTransaction);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void doPrefundTransaction(MUser user, String loadAmount) {
		try {
			long transactionRefNo = System.currentTimeMillis();
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
			MService service = mServiceRepository.findServiceByCode("PREFC");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = user.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance + transactionAmount;
				MPQAccountDetails mAccount = user.getAccountDetail();
				mAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(mAccount);
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(user.getAccountDetail());
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setTransactionType(TransactionType.CORPORATE_PREFUND);
				transactionExists = transactionRepository.save(senderTransaction);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public MTransaction dobulkDebitTransaction(MUser corporate, String loadAmount) {
		long transactionRefNo = System.currentTimeMillis();

		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");

		try {
			MService service = mServiceRepository.findServiceByCode("BRCL");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = corporate.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;
				MPQAccountDetails mAccount = corporate.getAccountDetail();
				mAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(mAccount);
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(true);
				senderTransaction.setFavourite(false);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(corporate.getAccountDetail());
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setTransactionType(TransactionType.DEFAULT);
				transactionExists = transactionRepository.save(senderTransaction);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionExists;
	}

	@Override
	public MTransaction dobulkcreditTransaction(MUser user, String loadAmount) {
		long transactionRefNo = System.currentTimeMillis();
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		try {

			MService service = mServiceRepository.findServiceByCode("BRCL");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = user.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance + transactionAmount;
				MPQAccountDetails mAccount = user.getAccountDetail();
				mAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(mAccount);
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus("Success");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(user.getAccountDetail());
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setTransactionType(TransactionType.DEFAULT);
				transactionExists = transactionRepository.save(senderTransaction);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionExists;
	}

	@Override
	public MTransaction dobulkDebitTransactionCorp(MUser corporate, String loadAmount) {
		long transactionRefNo = System.currentTimeMillis();

		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");

		try {
			MService service = mServiceRepository.findServiceByCode("BRCL");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = corporate.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;
				MPQAccountDetails mAccount = corporate.getAccountDetail();
				mAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(mAccount);
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "D");
				senderTransaction.setDebit(true);
				senderTransaction.setFavourite(false);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(corporate.getAccountDetail());
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setTransactionType(TransactionType.CORPORATE_DEBIT);
				transactionExists = transactionRepository.save(senderTransaction);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionExists;
	}

	@Override
	public MTransaction dobulkDebitTransactionCorpFailed(MUser corporate, String loadAmount) {
		long transactionRefNo = System.currentTimeMillis();

		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");

		try {
			MService service = mServiceRepository.findServiceByCode("BRCL");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Long.valueOf(loadAmount);
				double senderUserBalance = corporate.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;
				MPQAccountDetails mAccount = corporate.getAccountDetail();
				mAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(mAccount);
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(service.getDescription() + " of Rs." + loadAmount);
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "D");
				senderTransaction.setDebit(true);
				senderTransaction.setFavourite(false);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(corporate.getAccountDetail());
				senderTransaction.setStatus(Status.Failed);
				senderTransaction.setTransactionType(TransactionType.CORPORATE_DEBIT);
				transactionExists = transactionRepository.save(senderTransaction);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionExists;
	}

	public static void main(String[] args) {
		BigDecimal a = new BigDecimal(String.valueOf(1.015));
		BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		System.out.println(roundOff);
	}

	@Override
	public ResponseDTO initiateLoadCardTransaction(RequestDTO request, MUser agent, MService service) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MCommission commission = mCommissionRepository.findCommissionByService(service);
			String transactionRefNo = String.valueOf(System.currentTimeMillis());
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Double.valueOf(request.getAmount());
				double senderUserBalance = agent.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;
				if (commission != null) {
					senderTransaction.setCommissionEarned(CommonUtil.commissionEarned(commission, transactionAmount));
				}
				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(
						service.getDescription() + " of Rs." + request.getAmount() + " to" + request.getContactNo());
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "D");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus("Pending");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(agent.getAccountDetail());
				senderTransaction.setTransactionType(TransactionType.CORPORATE_LOAD);
				senderTransaction.setStatus(Status.Initiated);
				transactionExists = transactionRepository.save(senderTransaction);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setTxnId(transactionRefNo);
				resp.setDate(transactionExists.getCreated() + "");
			}

			MUser reciever = userApi.findByUserName(request.getContactNo());
			MTransaction transactionReciever = getTransactionByRefNo(transactionRefNo + "C");
			if (transactionReciever == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Double.valueOf(request.getAmount());
				double senderUserBalance = reciever.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance;
				double finalTransactionAmount = transactionAmount;
				senderTransaction.setAmount(finalTransactionAmount);
				senderTransaction
						.setDescription(" Recieved amount of Rs." + request.getAmount() + "from" + agent.getUsername());
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus("Pending");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(reciever.getAccountDetail());
				senderTransaction.setStatus(Status.Initiated);
				senderTransaction.setCommissionEarned(CommonUtil.commissionEarned(commission, transactionAmount));
				transactionExists = transactionRepository.save(senderTransaction);
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Transaction failed, please try again later.");
		}
		return resp;
	}

	@Override

	public ResponseDTO initiateLoadCardTransactionCorporate(RequestDTO request, MUser agent, MService service) {
		ResponseDTO resp = new ResponseDTO();
		String transactionRefNo = String.valueOf(System.currentTimeMillis());

		try {
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
			if (transactionExists == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Double.valueOf(request.getAmount());
				double senderUserBalance = agent.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;

				senderTransaction.setAmount(transactionAmount);
				senderTransaction.setDescription(
						service.getDescription() + " of Rs." + request.getAmount() + " to" + request.getContactNo());
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "D");
				senderTransaction.setDebit(true);
				senderTransaction.setFavourite(false);
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(agent.getAccountDetail());
				senderTransaction.setTransactionType(TransactionType.CORPORATE_LOAD);
				senderTransaction.setStatus(Status.Initiated);
				transactionExists = transactionRepository.save(senderTransaction);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setTxnId(transactionRefNo);
			}

			MUser reciever = userApi.findByUserName(request.getContactNo());
			MTransaction transactionReciever = getTransactionByRefNo(transactionRefNo + "C");
			if (transactionReciever == null) {
				MTransaction senderTransaction = new MTransaction();
				double transactionAmount = Double.valueOf(request.getAmount());
				double senderUserBalance = reciever.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance;
				double finalTransactionAmount = transactionAmount;
				senderTransaction.setAmount(finalTransactionAmount);
				senderTransaction.setDescription(
						" Recieved amount of Rs." + request.getAmount() + " from " + agent.getUsername());
				senderTransaction.setService(service);
				senderTransaction.setTransactionRefNo(transactionRefNo + "C");
				senderTransaction.setDebit(false);
				senderTransaction.setFavourite(false);
				senderTransaction.setCardLoadStatus("Pending");
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setAccount(reciever.getAccountDetail());
				senderTransaction.setStatus(Status.Initiated);
				transactionExists = transactionRepository.save(senderTransaction);
				resp.setCode("S00");
				resp.setMessage("Transaction Success");
				resp.setTxnId(transactionRefNo);

			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setTxnId(transactionRefNo);

			resp.setMessage("Transaction failed, please try again later.");
		}
		return resp;
	}

	@Override
	public void updateLoadCardByCorporate(String txnId, String status) {
		MTransaction debittrans = getTransactionByRefNo(txnId + "D");
		MTransaction crtrans = getTransactionByRefNo(txnId + "C");
		System.err.println("The status received" + status);
		if (status.equalsIgnoreCase("Success")) {
			MPQAccountDetails account = debittrans.getAccount();
			double bal = account.getBalance() - debittrans.getAmount();
			account.setBalance(bal);
			accountDetailRepository.save(account);
			debittrans.setStatus(Status.Success);
			transactionRepository.save(debittrans);
			crtrans.setStatus(Status.Success);
			transactionRepository.save(crtrans);
			System.err.println("Account Updated");
		} else {
			debittrans.setStatus(Status.Failed);
			transactionRepository.save(debittrans);
		}
	}

	@Override
	public ResponseDTO initiateP2PFundTransfer(double amount, String string, MService service, String transactionRefNo,
			MUser sender, String imps, MCommission commission, double netCommissionValue, ImpsRequest ebsRequest) {
		ResponseDTO result = new ResponseDTO();
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		netTransactionAmount = netTransactionAmount + netCommissionValue;
		MTransaction senderTransaction = new MTransaction();
		MTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			MPQAccountDetails senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(service.getDescription());
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			accountDetailRepository.save(senderAccountDetail);
			MTransaction senderTx = transactionRepository.save(senderTransaction);

			ImpsP2ADetail cardDetails = new ImpsP2ADetail();
			cardDetails.setAccountNo(ebsRequest.getAccountNo());
			cardDetails.setAmount(amount + "");
			cardDetails.setDescription(ebsRequest.getDescription());
			cardDetails.setIfscCode(ebsRequest.getIfscNo());
			cardDetails.setTransactionId(senderTx);
			cardDetails.setStatus("Initiated");
			impsP2ADetailRepository.save(cardDetails);
		}
		result.setMessage("P2A Fund Transfer Initiated Succesfull.");
		result.setCode(ResponseStatus.SUCCESS.getValue());
		return result;

	}

	@Override
	public void successImpsPayment(String transactionRefNo, MCommission commission, double netCommissionValue,
			MUser user, ImpsResponse dto) {

		double netTransactionAmount = 0;
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			netTransactionAmount = senderTransaction.getAmount();
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setRetrivalReferenceNo(dto.getRetrivalRefNo());
				transactionRepository.save(senderTransaction);
				ImpsP2ADetail cardDetails = impsP2ADetailRepository.findByTransaction(senderTransaction);
				if (cardDetails != null) {
					cardDetails.setName(dto.getBeneficiaryName());
					cardDetails.setRetrivalRefNo(dto.getRetrivalRefNo());
					cardDetails.setStatus("Completed");
					impsP2ADetailRepository.save(cardDetails);
				}
			}
		}
		MUser reciever = mUserRespository.findByUsername(StartUpUtil.IMPS);
		if (reciever != null) {
			MPQAccountDetails receiverAccount = reciever.getAccountDetail();
			double amountExceptCommision = netTransactionAmount - senderTransaction.getCommissionEarned();
			double receiverCurrentBalance = receiverAccount.getBalance() + amountExceptCommision;
			receiverAccount.setBalance(receiverCurrentBalance);
			accountDetailRepository.save(receiverAccount);
		}
	}

	@Override
	public void failedImpsPayment(String transactionRefNo, String reference, String loadStatus) {

		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			MUser sender = userApi.findByUserAccount(senderTransaction.getAccount());
			double netTransactionAmount = senderTransaction.getAmount();
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<MTransaction> lastTransList = transactionRepository
						.getTotalSuccessTransactions(senderTransaction.getAccount());
				MTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance = lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderTransaction.setCardLoadStatus(loadStatus);
				MPQAccountDetails senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				accountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
			}
		}
		ImpsP2ADetail cardDetails = impsP2ADetailRepository.findByTransaction(senderTransaction);
		if (cardDetails != null) {
			cardDetails.setStatus(ResponseStatus.FAILURE.getKey());
			impsP2ADetailRepository.save(cardDetails);
		}
	}

	@Override
	public void updateTransactionStatus(String transactionId, String reference, Status success, String cardStatus) {

		MTransaction transactionExists = getTransactionByRefNo(transactionId + "D");

		if (transactionExists != null
				&& Status.Initiated.getValue().equalsIgnoreCase(transactionExists.getStatus().getValue())) {
			MUser user = userRepository.findByAccountDetails(transactionExists.getAccount());
			transactionExists.setStatus(success);
			transactionExists.setAuthReferenceNo(reference);
			transactionRepository.save(transactionExists);
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(success.getValue())) {
				senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.MDEX_SUCCESS, user, transactionId);
			} else {
				senderApi.sendUserSMSTopup(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.MDEX_FAILURE, user,
						transactionId, String.valueOf(transactionExists.getAmount()));
			}
		}
	}

	@Override
	public Map<Double, String> getCreditTransactionForBarGraphMonthWise(MService service) {
		List<Object[]> transactionMonthly = transactionRepository.getAllCreditTransactionsMonthWise(Status.Success,
				service);
		Map<Double, String> mappingData = new HashMap<>();
		if (transactionMonthly != null) {
			for (Object[] objects : transactionMonthly) {
				Double amount = (Double) objects[0];
				String month = (String) objects[1];
				mappingData.put(amount, month);
			}
		}
		return mappingData;
	}

	@Override
	public MTransaction cashBackTransaction(CashBackDTO request) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MService service = mServiceRepository.findServiceByCode("CASH");
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double transactionAmount = Double.parseDouble(request.getAmount());
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription("Cashback" + " of Rs." + request.getAmount());
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Success);
			transactionExists = transactionRepository.save(senderTransaction);
		}
		return transactionExists;
	}

	@Override
	public List<TransactionListDTO> getTransactionByAccounts(List<MTransaction> loadMoneyTxn, MUser user) {
		if (loadMoneyTxn != null && !loadMoneyTxn.isEmpty()) {
			try {
				List<TransactionListDTO> transactionList = new ArrayList<>();
				for (MTransaction mTransaction : loadMoneyTxn) {
					TransactionListDTO listDTO = new TransactionListDTO();
					listDTO.setUsername(user.getUsername());
					listDTO.setContactNo(user.getUserDetail().getContactNo());
					listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
					listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
					listDTO.setCreated(CommonUtil.formatter.format(mTransaction.getCreated()));
					listDTO.setStatus(mTransaction.getStatus().getValue());
					listDTO.setAuthRefNo(
							mTransaction.getAuthReferenceNo() != null ? mTransaction.getAuthReferenceNo() : "NA");
					listDTO.setRetrivalRefNo(
							mTransaction.getRetrivalReferenceNo() != null ? mTransaction.getRetrivalReferenceNo()
									: "NA");
					listDTO.setCardLoadStatus("NA");
					listDTO.setError(mTransaction.getDescription());
					transactionList.add(listDTO);
				}
				return transactionList;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	// fingoole transaction
	@Override
	public MTransaction initiateFingooleTransaction(TransactionalRequest request) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
		if (transactionExists == null) {
			MTransaction senderTransaction = new MTransaction();
			double transactionAmount = request.getAmount();
			double senderUserBalance = request.getUser().getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			double finalTransactionAmount = transactionAmount;
			if (request.getCommission() != null) {
				double comm = CommonUtil.commissionEarned(request.getCommission(), transactionAmount);
				finalTransactionAmount = comm + transactionAmount;
				BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
				BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				finalTransactionAmount = roundOff.doubleValue();
			}
			senderTransaction.setAmount(finalTransactionAmount);
			senderTransaction.setDescription(request.getDescription() + " of Rs." + request.getAmount());
			senderTransaction.setService(request.getService());
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setFavourite(false);
			senderTransaction.setMdexTransaction(true);
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setCardLoadStatus("Pending");
			senderTransaction.setAndriod(request.isAndroid());
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(request.getUser().getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setBatch(request.getBatch());
			if (request.getCommission() != null) {
				senderTransaction
						.setCommissionEarned(CommonUtil.commissionEarned(request.getCommission(), transactionAmount));
			}
			transactionExists = transactionRepository.save(senderTransaction);
		}
		return transactionExists;
	}

	@Override
	public CommonResponse cancelBusTicket(String transactionRefNo, double refundAmount, String ticketPnr,
			double cancellationCharges) {
		CommonResponse result = new CommonResponse();
		try {
			MTransaction senderTransaction = transactionRepository.findByTransactionRefNo(transactionRefNo + "D");
			System.err.println("senderTransaction " + senderTransaction.getDescription());
			String txnRefno = System.currentTimeMillis() + "";
			String creditTxnRefNo = txnRefno + "C";
			String debitTxnRefNo = txnRefno + "D";

			MTransaction creditPQTxn = new MTransaction();
			MTransaction debitPQTxn = new MTransaction();

			if (senderTransaction != null) {
				System.err.println("senderTransaction not null");
				MUser sender = userRepository.findByAccountDetails(senderTransaction.getAccount());
				MPQAccountDetails senderAccountDetail = sender.getAccountDetail();

				double senderNewBal = senderAccountDetail.getBalance() + refundAmount;
				System.err.println("balance " + senderNewBal);
				if (Status.Success.equals(senderTransaction.getStatus())) {
					UserKycResponse resp = matchMoveApi.getConsumers();
					if (resp.getCode().equalsIgnoreCase("S00")) {

						String prefundingBalance = resp.getPrefundingBalance();
						double doublePrefundingBalance = Double.parseDouble(prefundingBalance);
						double actTransactonAmt = refundAmount;
						String activeCardId = null;
						System.err.println("prefund balance " + sender);
						if (doublePrefundingBalance >= actTransactonAmt) {
							WalletResponse walletResponse = matchMoveApi.initiateLoadFundsToMMWalletUpi(sender,
									String.valueOf(actTransactonAmt), transactionRefNo, txnRefno);
							if (walletResponse.getCode().equalsIgnoreCase("S00")) {
								String authRefNo = walletResponse.getAuthRetrivalNo();
								senderTransaction.setAuthReferenceNo(authRefNo);
								senderTransaction.setCardLoadStatus("Pending");
								transactionRepository.save(senderTransaction);
								List<MMCards> cardList = mMCardRepository.getCardsListByUser(sender);
								if (cardList != null && cardList.size() > 0) {
									for (MMCards mmCards : cardList) {
										if (mmCards.isHasPhysicalCard()) {
											if (mmCards.getStatus().equalsIgnoreCase("Active")
													|| !mmCards.isBlocked()) {
												activeCardId = mmCards.getCardId();
											}
										} else {
											if (mmCards.getStatus().equalsIgnoreCase("Active")
													|| !mmCards.isBlocked()) {
												activeCardId = mmCards.getCardId();
											}
										}
									}
									WalletResponse cardTransferResposne = matchMoveApi.transferFundsToMMCard(sender,
											String.valueOf(actTransactonAmt), activeCardId);
									if (cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
										System.err.println("card load ");
										senderTransaction.setCardLoadStatus("Success");
										transactionRepository.save(senderTransaction);
										result.setCode(ResponseStatus.SUCCESS.getValue());
										result.setSuccess(true);
										result.setMessage("Transaction Success");
										result.setStatus("Success");
									} else {
										System.err.println("card load pending");
										senderTransaction.setCardLoadStatus("Pending");
										senderTransaction.setRemarks("CardEr::" + cardTransferResposne);
										transactionRepository.save(senderTransaction);
										result.setCode("S00");
										result.setSuccess(false);
										result.setMessage("Transaction Successful.But card not loaded");
									}
								} else {
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setSuccess(false);
									return result;
								}

								String retrieval = System.currentTimeMillis() + "";

								senderAccountDetail.setBalance(BigDecimal.valueOf(senderNewBal)
										.setScale(2, RoundingMode.HALF_UP).doubleValue());
								mPQAccountDetailRepository.save(senderAccountDetail);

								creditPQTxn.setDescription(
										"Bus cancelled for transaction id " + senderTransaction.getTransactionRefNo());
								creditPQTxn.setCurrentBalance(BigDecimal.valueOf(senderNewBal)
										.setScale(2, RoundingMode.HALF_UP).doubleValue());
								creditPQTxn.setService(senderTransaction.getService());
								creditPQTxn.setAmount(BigDecimal.valueOf(refundAmount).setScale(2, RoundingMode.HALF_UP)
										.doubleValue());

								creditPQTxn.setTransactionRefNo(creditTxnRefNo);
								creditPQTxn.setStatus(Status.Success);
								creditPQTxn.setMdexTransaction(true);
								creditPQTxn.setTransactionType(TransactionType.BUS);
								creditPQTxn.setRetrivalReferenceNo(ticketPnr + "C" + retrieval);

								creditPQTxn.setAuthReferenceNo(resp.getIndicator());
								creditPQTxn.setAccount(senderAccountDetail);
								creditPQTxn.setDebit(false);
								transactionRepository.save(creditPQTxn);

								debitPQTxn.setDescription("Bus cancellation charge for transaction id "
										+ senderTransaction.getTransactionRefNo());
								debitPQTxn.setMdexTransaction(true);
								debitPQTxn.setCurrentBalance(BigDecimal.valueOf(senderNewBal)
										.setScale(2, RoundingMode.HALF_UP).doubleValue());
								debitPQTxn.setService(senderTransaction.getService());
								debitPQTxn.setAmount(BigDecimal.valueOf(cancellationCharges)
										.setScale(2, RoundingMode.HALF_UP).doubleValue());
								debitPQTxn.setTransactionRefNo(debitTxnRefNo);
								debitPQTxn.setStatus(Status.Success);
								debitPQTxn.setTransactionType(TransactionType.BUS);
								debitPQTxn.setRetrivalReferenceNo(ticketPnr + "D" + retrieval);
								debitPQTxn.setAccount(senderAccountDetail);
								debitPQTxn.setDebit(true);
								transactionRepository.save(debitPQTxn);

								senderTransaction.setStatus(Status.Cancelled);

								transactionRepository.save(senderTransaction);

								result.setStatus(ResponseStatus.SUCCESS.getKey());
								result.setCode(ResponseStatus.SUCCESS.getValue());
								result.setMessage("Transaction successful.");
								result.setSuccess(true);
								result.setDetails(creditPQTxn);
								System.err.println("success transaction");
							} else {
								System.err.println("failure transaction");
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage(resp.getMessage());
								result.setSuccess(false);
							}
						} else {
							System.err.println("no prefund failure");
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("no prefund");
							result.setSuccess(false);
						}
					} else {
						System.err.println("Currently we are facing some issues");
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage("Currently we are facing some issues.Please try after sometime");
						result.setSuccess(false);
					}
				} else {
					System.err.println("try again 2nd last");
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("try again last");
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public PGStatusCheckDTO checkPGStatus(String originalTxnNo, String retreivalRefNo) {
		PGStatusCheckDTO statusDto = new PGStatusCheckDTO();
		String transactionType = "STATUS";
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		MultivaluedMap<String, String> multiMap = new MultivaluedMapImpl();
		multiMap.add("merchantID", RazorPayConstants.MERCHANT_ID);
		multiMap.add("merchantTxnNo", originalTxnNo);
		multiMap.add("originalTxnNo", retreivalRefNo);
		multiMap.add("transactionType", transactionType);
		multiMap.add("secureHash", RazorPayConstants.composeMessageStatusCheck(RazorPayConstants.MERCHANT_ID,
				originalTxnNo, retreivalRefNo, transactionType));
		ClientResponse clientResponse = client.resource(RazorPayConstants.STATUS_CHECK)
				.header("Content-Type", "application/x-www-form-urlencoded").post(ClientResponse.class, multiMap);
		String strResponse = clientResponse.getEntity(String.class);
		System.err.println("status response>>>>>>>>>>" + strResponse);
		try {
			JSONObject statusCheck = new JSONObject(strResponse);
			if (clientResponse.getStatus() == 200) {
				if (statusCheck != null) {
					statusDto.setResponseCode(statusCheck.getString("txnResponseCode"));
					statusDto.setRespDescription(statusCheck.getString("respDescription"));
					statusDto.setPgStatus(PGStatus.valueOf(statusCheck.getString("txnStatus")));
					statusDto.setSecureHash(statusCheck.getString("secureHash"));
					statusDto.setTxnAuthID(statusCheck.getString("txnAuthID"));
					statusDto.setRespDescription(statusCheck.getString("txnID"));
					statusDto.setTxnID(statusCheck.getString("txnID"));
					statusDto.setAmount(statusCheck.getString("amount"));
					return statusDto;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			statusDto.setResponseCode(ResponseStatus.FAILURE.getValue());
			return statusDto;
		}
		statusDto.setResponseCode(ResponseStatus.FAILURE.getValue());
		return statusDto;

	}

	@Override
	public MTransaction successLoadMoneyCustom(String transactionRefNo, String paymentId) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			MUser sender = userApi.findByUserAccount(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(paymentId);
			senderTransaction = transactionRepository.save(senderTransaction);
			MPQAccountDetails senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			accountDetailRepository.save(senderAccount);
			senderApi.sendTransactionSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.LOADMONEY_SUCCESS, sender, null,
					senderTransaction);
		}
		return senderTransaction;
	}

	@Override
	public void failedLoadMoneyCustom(String transactionRefNo, String retreivalRefNo) {
		MTransaction senderTransaction = getTransactionByRefNo(transactionRefNo);
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			senderTransaction.setStatus(Status.Failed);
			senderTransaction.setRetrivalReferenceNo(retreivalRefNo);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public ResponseDTO initiateLoadCardTransactionCorporate(RequestDTO cardRequest, MUser agent, MService service,
			MUser cardUser) {
		ResponseDTO resp = new ResponseDTO();
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		try {
			MTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "D");
			if (transactionExists == null) {
				transactionExists = new MTransaction();
				MPQAccountDetails accountDetails = agent.getAccountDetail();
				
				double transactionAmount = Double.valueOf(cardRequest.getAmount());
				double senderUserBalance = accountDetails.getBalance();
				double senderCurrentBalance = senderUserBalance - transactionAmount;
				
				accountDetails.setBalance(senderCurrentBalance);
				accountDetails = mPQAccountDetailRepository.save(accountDetails);

				transactionExists.setAmount(transactionAmount);
				transactionExists.setDescription(service.getDescription() + " of Rs. " + cardRequest.getAmount()
						+ " to " + cardRequest.getContactNo());
				transactionExists.setService(service);
				transactionExists.setTransactionRefNo(transactionRefNo + "D");
				transactionExists.setDebit(true);
				transactionExists.setFavourite(false);
				transactionExists.setCurrentBalance(senderCurrentBalance);
				transactionExists.setAccount(accountDetails);
				transactionExists.setTransactionType(TransactionType.CORPORATE_LOAD);
				transactionExists.setStatus(Status.Initiated);
				transactionExists = transactionRepository.save(transactionExists);

				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setTxnId(transactionRefNo);
			}

			MTransaction transactionReciever = getTransactionByRefNo(transactionRefNo + "C");
			if (transactionReciever == null) {
				transactionReciever = new MTransaction();
				
				MPQAccountDetails accountDetails = cardUser.getAccountDetail();

				double transactionAmount = Double.valueOf(cardRequest.getAmount());
				double receiverUserBalance = accountDetails.getBalance();
				double senderCurrentBalance = receiverUserBalance + transactionAmount;
				
				accountDetails.setBalance(senderCurrentBalance);
				accountDetails = mPQAccountDetailRepository.save(accountDetails);
				transactionReciever.setAmount(transactionAmount);
				transactionReciever.setDescription(
						" Received amount of Rs. " + cardRequest.getAmount() + " from " + agent.getUsername());
				transactionReciever.setService(service);
				transactionReciever.setTransactionRefNo(transactionRefNo + "C");
				transactionReciever.setDebit(false);
				transactionReciever.setFavourite(false);
				transactionReciever.setCardLoadStatus("Pending");
				transactionReciever.setCurrentBalance(senderCurrentBalance);
				transactionReciever.setAccount(accountDetails);
				transactionReciever.setStatus(Status.Initiated);
				transactionExists = transactionRepository.save(transactionReciever);

				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Transaction Success");
				resp.setTxnId(transactionRefNo);
			}
		} catch (Exception e) {
			e.printStackTrace();

			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Transaction failed, please try again later.");
			resp.setTxnId(transactionRefNo);
		}
		return resp;
	}

	@Override
	public void updateLoadCardByCorporate(String txnId, String value, String authRetrivalNo) {
		MTransaction debitTransaction = getTransactionByRefNo(txnId + "D");
		MTransaction creditTransaction = getTransactionByRefNo(txnId + "C");

		if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(value)) {
			MPQAccountDetails account = debitTransaction.getAccount();
			double bal = account.getBalance() - (debitTransaction.getAmount());
			account.setBalance(bal);
			accountDetailRepository.save(account);

			debitTransaction.setStatus(Status.Success);
			transactionRepository.save(debitTransaction);

			creditTransaction.setStatus(Status.Success);
			if (authRetrivalNo != null && !"".equalsIgnoreCase(authRetrivalNo)) {
				creditTransaction.setRetrivalReferenceNo(authRetrivalNo);
			}
			transactionRepository.save(creditTransaction);
		} else {
			debitTransaction.setStatus(Status.Failed);
			transactionRepository.save(debitTransaction);

			creditTransaction.setStatus(Status.Failed);
			creditTransaction.setCommissionEarned(0);
			transactionRepository.save(creditTransaction);
		}
	}

}
