package com.msscard.app.api.impl;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cashiercards.paramerterization.DataConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.msscard.app.api.IAgentApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.AssignPhysicalCardAgentDTO;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.response.PrefundRequestDTO;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.app.sms.util.SMSUtil;
import com.msscard.entity.Banks;
import com.msscard.entity.LoadCard;
import com.msscard.entity.MAgentAssignPhysicalCard;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserAgent;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PrefundRequest;
import com.msscard.model.AgentPrefundDTO;
import com.msscard.model.AgentTransactionListDTO;
import com.msscard.model.ForgetPasswordRequestDTO;
import com.msscard.model.LoadCardList;
import com.msscard.model.MAgentDTO;
import com.msscard.model.MUserDTO;
import com.msscard.model.PhysicalCardRequestListDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.Status;
import com.msscard.model.UserLoadCardDTO;
import com.msscard.model.UserType;
import com.msscard.model.ValidTransactionDTO;
import com.msscard.model.error.CommonError;
import com.msscard.repositories.BankRepository;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.LoadCardRepository;
import com.msscard.repositories.MAgentAssignPhysicalCardRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserAgentRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.PrefundRequestRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.validation.CommonValidation;

public class AgentApiImpl implements IAgentApi {

	private final static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final PasswordEncoder passwordEncoder;
	private final MUserRespository mUserRespository;
	private final MPQAccountTypeRepository mPQAccountTypeRespository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final MKycRepository mKycRepository;
	private final ISMSSenderApi smsSenderApi;
	private final MUserAgentRepository mUserAgentRepository;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private final MMCardRepository cardRepository;
	private final LoadCardRepository loadCardRepository;
	private final MTransactionRepository mTransactionRepository;
	private final PrefundRequestRepository prefundRequestRepository;
	private final static String PROCESSING = "Processing";
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MAgentAssignPhysicalCardRepository mAgentAssignPhysicalCardRepository;
	private final DataConfigRepository dataConfigRepository;
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final BankRepository bankRepository;

	public AgentApiImpl(PasswordEncoder passwordEncoder , MUserRespository mUserRespository , 
			MPQAccountTypeRepository mPQAccountTypeRespository , MUserDetailRepository mUserDetailRepository
			, MPQAccountDetailRepository mPQAccountDetailRepository , MKycRepository mKycRepository , ISMSSenderApi smsSenderApi 
			, MUserAgentRepository mUserAgentRepository , MMCardRepository cardRepository , LoadCardRepository loadCardRepository 
			, MTransactionRepository mTransactionRepository , PrefundRequestRepository prefundRequestRepository , PhysicalCardDetailRepository physicalCardDetailRepository
			, IMatchMoveApi matchMoveApi , MAgentAssignPhysicalCardRepository mAgentAssignPhysicalCardRepository 
			, DataConfigRepository dataConfigRepository , IUserApi userApi , ITransactionApi transactionApi , BankRepository bankRepository) {
		this.passwordEncoder = passwordEncoder;
		this.mUserRespository = mUserRespository;
		this.mPQAccountTypeRespository = mPQAccountTypeRespository;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.mKycRepository = mKycRepository;
		this.smsSenderApi = smsSenderApi;
		this.mUserAgentRepository = mUserAgentRepository;
		this.cardRepository = cardRepository;
		this.loadCardRepository = loadCardRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.prefundRequestRepository = prefundRequestRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.matchMoveApi = matchMoveApi;
		this.mAgentAssignPhysicalCardRepository = mAgentAssignPhysicalCardRepository; 
		this.dataConfigRepository = dataConfigRepository;
		this.userApi=userApi;
		this.transactionApi = transactionApi;
		this.bankRepository = bankRepository;
	}


	@Override
	public void saveAgent(RegisterDTO dto) throws Exception {
		try {
			if (dto != null) {
				MUser mUser = new MUser();
				mUser.setAuthority(Authorities.AGENT + "," + Authorities.AUTHENTICATED);
				mUser.setPassword(passwordEncoder.encode(dto.getPassword()));
				mUser.setMobileStatus(Status.Active);
				mUser.setEmailStatus(Status.Active);
				mUser.setUsername(dto.getUsername().toLowerCase());
				mUser.setUserType(dto.getUserType());
				mUser.setMobileToken(null);
				mUser.setEmailToken("E" + System.currentTimeMillis());
				mUser.setFullyFilled(true);


				MUserDetails mUserDetails = new MUserDetails();
				if (dto.getAndroidDeviceID() != null) {
					logger.info(dto.getAndroidDeviceID());
					mUser.setAndroidDeviceID(dto.getAndroidDeviceID());
					MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
					if (androidUser != null) {
						mUser.setDeviceLocked(false);
					} else {
						mUser.setDeviceLocked(true);
					}
				}
				mUserDetails.setFirstName(dto.getFirstName());
				mUserDetails.setEmail(dto.getEmail());
				mUserDetails.setContactNo(dto.getContactNo());

				MPQAccountDetails mPQAccountDetail = new MPQAccountDetails();
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				if (nonKYCAccountType != null) {
					mPQAccountDetail.setBalance(0);
					mPQAccountDetail.setBranchCode(dto.getBranchCode());
					mPQAccountDetail.setAccountType(nonKYCAccountType);
					mUser.setAccountDetail(mPQAccountDetail);
				} else {
					nonKYCAccountType = new MPQAccountType();
					nonKYCAccountType.setCode("SUPERKYC");
					nonKYCAccountType.setBalanceLimit(5000000);
					nonKYCAccountType.setDailyLimit(5000000);
					nonKYCAccountType.setDescription("Verified Account");
					nonKYCAccountType.setMonthlyLimit(5000000);
					nonKYCAccountType.setName("KYC - MERCHANT");
					nonKYCAccountType.setTransactionLimit(10000000);
					mPQAccountDetail.setBalance(0);
					mPQAccountDetail.setBranchCode(dto.getBranchCode());
					mPQAccountDetail.setAccountType(nonKYCAccountType);
					mUser.setAccountDetail(mPQAccountDetail);
				}
				MKycDetail kyc = new MKycDetail();
				MUser tempUser = mUserRespository.findByUsernameAndStatus(mUser.getUsername(), Status.Inactive);
				if (tempUser == null) {
					mUserDetailRepository.save(mUserDetails);
					mPQAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + mUserDetails.getId());
					mPQAccountDetailRepository.save(mPQAccountDetail);
				} else {
					mUserRespository.delete(tempUser);
					mUserDetailRepository.delete(tempUser.getUserDetail());
					mUser.setAccountDetail(tempUser.getAccountDetail());
				}

				kyc.setAadharImage(dto.getAadharImagePath1());
				kyc.setAadharImage1(dto.getAadharImagePath2());
				kyc.setPancardImage(dto.getPanCardImagePath());
				kyc.setAccountNo(dto.getBankAccountNo());
				kyc.setIfscCode(dto.getIfscCode());
				kyc.setUserType(UserType.Agent);
				kyc.setAccountType(nonKYCAccountType);
				mPQAccountDetailRepository.save(mPQAccountDetail);
				mUser.setUserDetail(mUserDetails);
				mUserRespository.save(mUser);
				kyc.setUser(mUser);

				mKycRepository.save(kyc);
				smsSenderApi.sendAgentForgetPasswordSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.AGENT_CREATION_SUCCESS, mUser, dto.getPassword());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}


	@Override
	public List<MAgentDTO> getAllAgentDetail() throws Exception {
		List<MAgentDTO> agentDTOs = new ArrayList<>();
		try {
			List<MKycDetail> kycDetails = mKycRepository.findByUserType(UserType.Agent);
			if (kycDetails != null && !kycDetails.isEmpty()) {
				agentDTOs = getAgentList(kycDetails);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return agentDTOs;
	}

	private List<MAgentDTO> getAgentList(List<MKycDetail> kycDetails) {
		List<MAgentDTO> mAgentDTOs = new ArrayList<>();
		if (kycDetails != null && !kycDetails.isEmpty()) {
			for (MKycDetail kycdetail : kycDetails) {
				MAgentDTO dto = new MAgentDTO();
				dto.setName(kycdetail.getUser().getUserDetail().getFirstName());
				dto.setEmail(kycdetail.getUser().getUserDetail().getEmail());
				dto.setAccountNo(kycdetail.getAccountNo());
				dto.setIfscCode(kycdetail.getIfscCode());
				dto.setAadharCardImageFront(kycdetail.getAadharImage());
				dto.setAadharCardImageBack(kycdetail.getAadharImage1());
				dto.setPanCardImage(kycdetail.getPancardImage());
				dto.setContactNo(kycdetail.getUser().getUserDetail().getContactNo());
				mAgentDTOs.add(dto);
			}
		}
		return mAgentDTOs;
	}


	@Override
	public MUser saveUserByAgent(RegisterDTO dto) throws Exception {
		MUser mUser = null;
		try {
			if (dto != null) {
				String tempPwd = CommonUtil.generateRandomString(CommonUtil.RandomString);
				logger.info("temp password "+tempPwd);
				mUser = new MUser();
				mUser.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				mUser.setPassword(passwordEncoder.encode(tempPwd));
				mUser.setMobileStatus(Status.Active);
				mUser.setEmailStatus(Status.Active);
				mUser.setUsername(dto.getContactNo());
				mUser.setUserType(dto.getUserType());
				mUser.setMobileToken(CommonUtil.generateSixDigitNumericString());
				mUser.setEmailToken("E" + System.currentTimeMillis());
				mUser.setFullyFilled(true);
				mUser.setUsername(dto.getContactNo());


				MUserDetails mUserDetails = new MUserDetails();
				mUserDetails.setFirstName(dto.getFirstName());
				mUserDetails.setLastName(dto.getLastName());
				mUserDetails.setEmail(dto.getEmail());
				mUserDetails.setContactNo(dto.getContactNo());
				mUserDetails.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));

				MPQAccountDetails mPQAccountDetail = new MPQAccountDetails();
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				if (nonKYCAccountType != null) {
					mPQAccountDetail.setBalance(0);
					mPQAccountDetail.setBranchCode(dto.getBranchCode());
					mPQAccountDetail.setAccountType(nonKYCAccountType);
					mUser.setAccountDetail(mPQAccountDetail);
				} else {
					nonKYCAccountType = new MPQAccountType();
					nonKYCAccountType.setCode("NONKYC");
					nonKYCAccountType.setBalanceLimit(10000);
					nonKYCAccountType.setDailyLimit(10000);
					nonKYCAccountType.setDescription("Unverified Account");
					nonKYCAccountType.setMonthlyLimit(10000);
					nonKYCAccountType.setName("Non - KYC");
					nonKYCAccountType.setTransactionLimit(5);
					mPQAccountDetail.setBalance(0);
					mPQAccountDetail.setBranchCode(dto.getBranchCode());
					mPQAccountDetail.setAccountType(nonKYCAccountType);
					mUser.setAccountDetail(mPQAccountDetail);
				}

				MKycDetail kyc = new MKycDetail();
				MUserAgent mUserAgent = new MUserAgent();
				MUser tempUser = mUserRespository.findByUsernameAndStatus(mUser.getUsername(), Status.Inactive);
				if (tempUser == null) {
					mUserDetailRepository.save(mUserDetails);
					mPQAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + mUserDetails.getId());
					mPQAccountDetailRepository.save(mPQAccountDetail);
				} else {
					mUserRespository.delete(tempUser);
					mUserDetailRepository.delete(tempUser.getUserDetail());
					mUser.setAccountDetail(tempUser.getAccountDetail());
				}
				mPQAccountDetailRepository.save(mPQAccountDetail);
				mUser.setUserDetail(mUserDetails);
				
				if (dto.getAadharImagePath1() != null && !dto.getAadharImagePath1().isEmpty() || 
						dto.getPanCardImagePath() != null && !dto.getPanCardImagePath().isEmpty() || 
						dto.getAadharImagePath2() != null && !dto.getAadharImagePath2().isEmpty()) {
					kyc.setAadharImage(dto.getAadharImagePath1());
					kyc.setAadharImage1(dto.getAadharImagePath2());
					kyc.setPancardImage(dto.getPanCardImagePath());
					kyc.setUserType(UserType.User);
					kyc.setAccountType(nonKYCAccountType);
					kyc.setUser(mUser);
					mKycRepository.save(kyc);
					mUserAgent.setKycDetail(kyc);
				}
				mUserRespository.save(mUser);
				mUserAgent.setAgent(dto.getmUser());
				mUserAgent.setUser(mUser);
				mUserAgentRepository.save(mUserAgent);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.USER_TEMP_PWD, mUser, tempPwd);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mUser;

	}


	@Override
	public List<MUserDTO> getAllUser(MUser user , MUserDTO dto) throws Exception {
		List<MUserDTO> mUserDTOs = new ArrayList<>();
		List<MUserAgent> mUserAgents = null;
		try {
			if (dto != null) {
				if (dto.getFromDate() != null && dto.getToDate() != null) {
					Date fromDate = CommonUtil.dateFormate.parse(dto.getFromDate() + CommonUtil.startTime);
					Date toDate = CommonUtil.dateFormate.parse(dto.getToDate() + CommonUtil.endTime);
					mUserAgents = mUserAgentRepository.getUserBasedOnDate(user, fromDate, toDate);
				}
			} else {
				mUserAgents = mUserAgentRepository.getAllUserBasedOnAgent(user);
			}
			if (mUserAgents != null && !mUserAgents.isEmpty()) {
				mUserDTOs = getAllUserDetails(mUserAgents);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mUserDTOs;
	}

	private List<MUserDTO> getAllUserDetails(List<MUserAgent> mUserAgents) throws Exception {
		List<MUserDTO> mUserDTOs = new ArrayList<>();
		try {
			if (mUserAgents != null && !mUserAgents.isEmpty()) {
				for (MUserAgent mUserAgent : mUserAgents) {
					MUserDTO userDTO = new MUserDTO();
					String firstName = mUserAgent.getUser().getUserDetail().getFirstName();
					String lastName = "";
					if (mUserAgent.getUser().getUserDetail().getLastName() != null && !mUserAgent.getUser().getUserDetail().getLastName().isEmpty()) {
						lastName = mUserAgent.getUser().getUserDetail().getLastName();
					}
					userDTO.setName(firstName + " " + lastName);
					userDTO.setFirstName(firstName);
					userDTO.setLastName(lastName);
					userDTO.setEmail(mUserAgent.getUser().getUserDetail().getEmail());
					userDTO.setContactNo(mUserAgent.getUser().getUserDetail().getContactNo());
					userDTO.setDob(mUserAgent.getUser().getUserDetail().getDateOfBirth());
					MKycDetail mKycDetail = mUserAgent.getKycDetail();
					if (mKycDetail != null) {
						userDTO.setAadharCardImageFront(mKycDetail.getAadharImage());
						userDTO.setAadharCardImageBack(mKycDetail.getAadharImage1());
						userDTO.setPanCardImage(mKycDetail.getPancardImage());
						//userDTO.setKYC(true);
					}
					String accountType = mUserAgent.getUser().getAccountDetail().getAccountType().getName();
					userDTO.setUserType(accountType);
					mUserDTOs.add(userDTO);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return mUserDTOs;

	}


	@Override
	public ValidTransactionDTO validTransaction(UserLoadCardDTO dto) throws Exception {
		ValidTransactionDTO validTransactionDTO = new ValidTransactionDTO();
		try {
			double balance = dto.getUser().getAccountDetail().getBalance();
			double userRequestBlc = dto.getAmount();
			if (balance >= userRequestBlc) {
				validTransactionDTO.setSuccess(true);
				validTransactionDTO.setMessage("Valid transaction");
			} else {
				validTransactionDTO.setSuccess(false);
				validTransactionDTO.setMessage("Sorry agent does not have sufficient balance");
			}

			MUser senderUser = userApi.findByUserName(dto.getContactNo());
			if (senderUser != null) {
				double transactionAmount = dto.getAmount();
				double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
				double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
				double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
				double totalDailyCredit=transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());

				if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly, transactionAmount)) {
					validTransactionDTO.setSuccess(false);
					validTransactionDTO.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
							+ " and you've already credited total of Rs." + totalCreditMonthly);
				}

				if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit, transactionAmount)) {
					validTransactionDTO.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
							+ " and you've already credited total of Rs." + totalDailyCredit);
					validTransactionDTO.setSuccess(false);
				}

			}


		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return validTransactionDTO;
	}


	@Override
	public MMCards existPhysicalCard(MUser user) throws Exception {
		MMCards mmCards = new MMCards();
		try {
			if (user != null) {
				mmCards = cardRepository.getPhysicalCardByUser(user);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mmCards;
	}


	@Override
	public MMCards existVirtualCard(MUser user) throws Exception {
		MMCards mmCards = new MMCards();
		try {
			if (user != null) {
				mmCards = cardRepository.getVirtualCardsByCardUser(user);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mmCards;
	}


	@Override
	public boolean checkUser(MUser agent, MUser mUser) throws Exception {
		boolean valid = false;
		try {
			List<MUserAgent> mUserAgents = mUserAgentRepository.getAllUserBasedOnAgent(agent);
			if (mUserAgents != null && !mUserAgents.isEmpty()) {
				for (MUserAgent mUserAgent : mUserAgents) {
					if ((mUserAgent.getUser().getId()+"").equals(mUser.getId()+"")) {
						valid = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return valid;
	}


	@Override
	public MMCards existCard(MUser user) throws Exception {
		MMCards mmCards = new MMCards();
		try {
			if (user != null) {
				mmCards = cardRepository.getCardDetailsByUserAndStatus(user, Status.Active.getValue());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mmCards;
	}


	@Override
	public void loadCardFromAgent(UserLoadCardDTO dto , MMCards mmCards , MUser user) throws Exception {
		try {
			if (dto != null ) {
				LoadCard loadCard = new LoadCard();
				if (dto.getTransactionrefNo() != null && !dto.getTransactionrefNo().isEmpty()) {
					loadCard.setReferenceNo(dto.getTransactionrefNo());
				}
				loadCard.setAmount(dto.getAmount());
				loadCard.setComment(dto.getComment());
				loadCard.setDirect(true);
				loadCard.setPhysicalCard(mmCards.isHasPhysicalCard());
				loadCard.setUser(user);
				loadCard.setRole(Role.AGENT);
				loadCard.setAgent(dto.getUser());
				loadCardRepository.save(loadCard);
				settleTransaction(dto, user);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}

	private void settleTransaction(UserLoadCardDTO dto , MUser user) throws Exception {
		if (dto != null && user != null) {
			String transactionRefNo=String.valueOf(System.currentTimeMillis());
			MTransaction mTransaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo + "C");
			if (mTransaction == null) {
				mTransaction = new MTransaction();
				double transactionAmount = dto.getAmount();
				double userBalance = user.getAccountDetail().getBalance();
				double userUpdatedBlc = transactionAmount + userBalance;
				mTransaction.setAmount(transactionAmount);
				mTransaction.setCurrentBalance(userUpdatedBlc);
				mTransaction.setStatus(Status.Success);
				mTransaction.setDescription("Load Money to card of Rs. " +dto.getAmount() + " By Agent");
				mTransaction.setDebit(false);
				mTransaction.setAccount(user.getAccountDetail());
				mTransaction.setTransactionRefNo(transactionRefNo + "C");
				mTransaction.setCardLoadStatus("Success");
				mTransactionRepository.save(mTransaction);
				MPQAccountDetails userAccountDetails = user.getAccountDetail();
				if (userAccountDetails != null) {
					userAccountDetails.setBalance(userUpdatedBlc);
					mPQAccountDetailRepository.save(userAccountDetails);
				}
			}
			MTransaction mAgentTransaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo + "D");
			if (mAgentTransaction == null) {
				mAgentTransaction = new MTransaction();
				double transactionAmount = dto.getAmount();
				double agentBalance = dto.getUser().getAccountDetail().getBalance();
				double agentUpdatedBlc = agentBalance - transactionAmount;
				mAgentTransaction.setAmount(transactionAmount);
				mAgentTransaction.setCurrentBalance(agentUpdatedBlc);
				mAgentTransaction.setStatus(Status.Success);
				mAgentTransaction.setDescription("Load Money to card of Rs. " +dto.getAmount() + " By Agent");
				mAgentTransaction.setDebit(true);
				mAgentTransaction.setAccount(dto.getUser().getAccountDetail());
				mAgentTransaction.setTransactionRefNo(transactionRefNo + "D");
				mAgentTransaction.setCardLoadStatus("Success");
				mTransactionRepository.save(mAgentTransaction);
				MPQAccountDetails agentAccountDetails = dto.getUser().getAccountDetail();
				if (agentAccountDetails != null) {
					agentAccountDetails.setBalance(agentUpdatedBlc);
					mPQAccountDetailRepository.save(agentAccountDetails);
				}
			} 
		}

	}


	@Override
	public MTransaction findTransactionBasedOnTransactionRefNo(String transactionRefNo) throws Exception {
		MTransaction mTransaction = new MTransaction();
		try {
			if (transactionRefNo != null && !transactionRefNo.isEmpty()) {
				mTransaction = mTransactionRepository.findByTransactionRefNo(transactionRefNo + "C");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mTransaction;
	}


	@Override
	public void agentPrefundRequest(AgentPrefundDTO dto) throws Exception {
		try {
			if (dto != null) {
				Banks banks = bankRepository.getBankDetailsBasedOnName(dto.getToBank());
				if (banks != null) {
					Banks banks1 = bankRepository.getBankDetailsBasedOnName(dto.getFromBank());
					if (banks1 != null) {
						PrefundRequest pRequest = new PrefundRequest();
						pRequest.setAmount(dto.getAmount());
						pRequest.setSenderAccountNo(dto.getAccountNo());
						pRequest.setSenderBankName(dto.getFromBank());
						pRequest.setReceiverAccountNo(dto.getReceiverAccountNo());
						pRequest.setSenderReferenceNo(dto.getReferenceNo());
						pRequest.setReceiverBankName(dto.getToBank());
						if (dto.getRemarks() != null && !dto.getRemarks().isEmpty()) {
							pRequest.setRemarks(dto.getRemarks());
						}
						pRequest.setUser(dto.getUser());
						pRequest.setStatus(Status.Processing);
						prefundRequestRepository.save(pRequest);
					} else {
						throw new Exception("Please enter valid from bank name.");
					}
				} else {
					throw new Exception("Please enter valid to bank name.");
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}


	@Override
	public List<AgentPrefundDTO> getAgentPrefundDetails() throws Exception {
		List<AgentPrefundDTO> agentPrefundDTOs = new ArrayList<>();
		try {
			Iterable<PrefundRequest> prefundRequest = prefundRequestRepository.findAll();
			for (PrefundRequest rPrefundRequest : prefundRequest) {
				AgentPrefundDTO dto = new AgentPrefundDTO();
				dto.setAmount(rPrefundRequest.getAmount());
				dto.setAccountNo(rPrefundRequest.getSenderAccountNo());
				dto.setFromBank(rPrefundRequest.getSenderBankName());
				dto.setReferenceNo(rPrefundRequest.getSenderReferenceNo());
				dto.setToBank(rPrefundRequest.getReceiverBankName());
				dto.setRemarks(rPrefundRequest.getRemarks());
				dto.setStatus(rPrefundRequest.getStatus().getValue());
				dto.setDate(rPrefundRequest.getCreated());
				dto.setReceiverAccountNo(rPrefundRequest.getReceiverAccountNo());
				agentPrefundDTOs.add(dto);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return agentPrefundDTOs;
	}


	@Override
	public PrefundRequestDTO updateAgentPrefundStatus(AgentPrefundDTO prefundDTO) throws Exception {
		PrefundRequestDTO prefundRequestDTO = new PrefundRequestDTO();
		try {
			if (prefundDTO != null) {
				String value = prefundDTO.getValue();
				if(value != null && !value.isEmpty()) {
					PrefundRequest prefundRequest = prefundRequestRepository.getPrefundRequestBasedOnReferenceNo(prefundDTO.getReferenceNo());
					if (prefundRequest != null) {
						if (prefundRequest.getStatus().getValue().equalsIgnoreCase(PROCESSING)) {
							if (Status.Success.getValue().equalsIgnoreCase(value)) {
								prefundRequest.setStatus(Status.Success);
								MPQAccountDetails mpqAccountDetails = prefundRequest.getUser().getAccountDetail();
								if (mpqAccountDetails != null) {
									double amount = prefundRequest.getAmount();
									double agentBalance = mpqAccountDetails.getBalance();
									double updatedAgentBalance = amount + agentBalance;
									mpqAccountDetails.setBalance(updatedAgentBalance);
									mPQAccountDetailRepository.save(mpqAccountDetails);
								}
								prefundRequestDTO.setStatus(Status.Success.getValue());

							} else if (Status.Cancelled.getValue().equalsIgnoreCase(value)) {
								prefundRequest.setStatus(Status.Cancelled);
								prefundRequestDTO.setStatus(Status.Cancelled.getValue());
							}
							prefundRequestRepository.save(prefundRequest);
							prefundRequestDTO.setCode(ResponseStatus.SUCCESS.getValue());
						}
					}
				}
			}
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return prefundRequestDTO;
	}




	@Override
	public MUser checkUser(String userName) throws Exception {
		MUser mUser = new MUser();
		if (userName != null && !userName.isEmpty()) {
			mUser = mUserRespository.getUserBasedOnUserNameAndAuthority(userName, Status.Active);
			if (mUser != null) {
				return mUser;
			} 
		} else {
			throw new Exception("Please enter your user name");
		}
		return null;
	}


	@Override
	public void updateCardDetails(MUser user) throws Exception {
		PhysicalCardDetails physicalCardDetails = new PhysicalCardDetails();
		if (user != null) {
			physicalCardDetails = physicalCardDetailRepository.findByUser(user);
			if (physicalCardDetails != null) {
				physicalCardDetails.setStatus(Status.Received);
				physicalCardDetailRepository.save(physicalCardDetails);
			}
		}
	}


	@Override
	public void updatePhysicalCard(MUser mUser) throws Exception {
		try {
			PhysicalCardDetails card=physicalCardDetailRepository.findByUser(mUser);
			if (card != null) {
				card.setStatus(Status.Active);
				physicalCardDetailRepository.save(card);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}


	@Override
	public void transferMoneyToCard(MUser mUser) throws Exception {
		try {
			MMCards cards=cardRepository.getVirtualCardsByCardUser(mUser);
			if(cards!=null) {
				if(Status.Active.getValue().equalsIgnoreCase(cards.getStatus())){
					String cardId=cards.getCardId();
					double balance= matchMoveApi.getBalance(mUser);
					if(balance > 0){
						matchMoveApi.debitFromMMWalletToCard(mUser.getUserDetail().getEmail(), mUser.getUsername(), cardId, String.valueOf(balance));
						MatchMoveCreateCardRequest cardReq=new MatchMoveCreateCardRequest();
						cardReq.setCardId(cardId);
						cardReq.setRequestType("suspend");
						matchMoveApi.deActivateCards(cardReq);
						MMCards phyCard=cardRepository.getPhysicalCardByUser(mUser);
						if(phyCard!=null && Status.Active.getValue().equalsIgnoreCase(phyCard.getStatus())){
								matchMoveApi.transferFundsToMMCard(mUser, String.valueOf(balance), phyCard.getCardId());
						}
					} else {
						MatchMoveCreateCardRequest cardReq=new MatchMoveCreateCardRequest();
						cardReq.setCardId(cardId);
						cardReq.setRequestType("suspend");
						matchMoveApi.deActivateCards(cardReq);
					}
				}
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.ACTIVATE_CARD, mUser, null);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}


	@Override
	public void saveAgentAssignPhysicalCard(MUser agent, MUser mUser , AssignPhysicalCardAgentDTO dto) throws Exception {
		try {
			String kitNo = dto.getKitNo();
			MAgentAssignPhysicalCard mAgentAssignPhysicalCard = new MAgentAssignPhysicalCard();
			DataConfig dataConfig = dataConfigRepository.findDatas();
			double cardFees = 0.0;
			if (dataConfig != null) {
				cardFees = Double.parseDouble(dataConfig.getCardFees());
			}
			double updatedBlc = agent.getAccountDetail().getBalance() - cardFees;
			mAgentAssignPhysicalCard.setAmount(cardFees);
			mAgentAssignPhysicalCard.setKitNo(kitNo);
			mAgentAssignPhysicalCard.setAgent(agent);
			mAgentAssignPhysicalCard.setUser(mUser);
			mAgentAssignPhysicalCardRepository.save(mAgentAssignPhysicalCard);
			MPQAccountDetails mpAccountDetails = agent.getAccountDetail();
			if (mpAccountDetails != null) {
				mpAccountDetails.setBalance(updatedBlc);
				mPQAccountDetailRepository.save(mpAccountDetails);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}


	@Override
	public List<LoadCardList> getLoadCardList(MUser agent , MUserDTO dto) throws Exception {
		List<LoadCardList> lCardLists = new ArrayList<>();
		List<LoadCard> loadCards = null;
		try {
			if (agent != null) {
				if (dto != null) {
					if (dto.getFromDate() != null && dto.getToDate() != null) {
						Date fromDate = CommonUtil.dateFormate.parse(dto.getFromDate() + CommonUtil.startTime);
						Date toDate = CommonUtil.dateFormate.parse(dto.getToDate() + CommonUtil.endTime);
						loadCards = loadCardRepository.getLoadcardDataBasedOnAgentAndDate(agent, fromDate, toDate);
					}
				} else {
					loadCards = loadCardRepository.getLoadcardDataBasedOnAgent(agent);
				}
				if (loadCards != null && !loadCards.isEmpty()) {
					for (LoadCard loadCard : loadCards) {
						LoadCardList loadCardList = new LoadCardList();
						loadCardList.setAmount(loadCard.getAmount());
						loadCardList.setDate(loadCard.getCreated());
						loadCardList.setEmail(loadCard.getUser().getUserDetail().getEmail());
						loadCardList.setContactNo(loadCard.getUser().getUserDetail().getContactNo());
						if (loadCard.isPhysicalCard()) {
							loadCardList.setPhysicalCard("Yes");
						} else {
							loadCardList.setPhysicalCard("No");
						}
						String firstName = loadCard.getUser().getUserDetail().getFirstName();
						String lastName = null;
						if (loadCard .getUser().getUserDetail().getLastName() != null && !loadCard .getUser().getUserDetail().getLastName().isEmpty()) {
							lastName = loadCard .getUser().getUserDetail().getLastName();
						}
						loadCardList.setName(firstName + " " + lastName);
						loadCardList.setUserType(loadCard.getUser().getAccountDetail().getAccountType().getName());
						lCardLists.add(loadCardList);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return lCardLists;
	}


	@Override
	public List<PhysicalCardRequestListDTO> getPhysicalCardAssignList(MUser agent , MUserDTO dto) throws Exception {
		List<PhysicalCardRequestListDTO> lCardRequestListDTOs = new ArrayList<>();
		List<MAgentAssignPhysicalCard> mAgentAssignPhysicalCards = null;
		try {
			if (dto != null) {
				if (dto.getFromDate() != null && dto.getToDate() != null) {
					Date fromDate = CommonUtil.dateFormate.parse(dto.getFromDate() + CommonUtil.startTime);
					Date toDate = CommonUtil.dateFormate.parse(dto.getToDate() + CommonUtil.endTime);
					mAgentAssignPhysicalCards = mAgentAssignPhysicalCardRepository.getPhysicalCardAssignListBasedOnDate(agent, toDate, fromDate);
				}
			} else {
				mAgentAssignPhysicalCards = mAgentAssignPhysicalCardRepository.getPhysicalCardAssignList(agent);
			}
			if (mAgentAssignPhysicalCards != null && !mAgentAssignPhysicalCards.isEmpty()) {
				for (MAgentAssignPhysicalCard mAgentAssignPhysicalCard : mAgentAssignPhysicalCards) {
					PhysicalCardRequestListDTO physicalCardRequestListDTO = new PhysicalCardRequestListDTO();
					physicalCardRequestListDTO.setCardCost(mAgentAssignPhysicalCard.getAmount());
					physicalCardRequestListDTO.setContactNo(mAgentAssignPhysicalCard.getUser().getUserDetail().getContactNo());
					physicalCardRequestListDTO.setDate(mAgentAssignPhysicalCard.getCreated());
					physicalCardRequestListDTO.setEmail(mAgentAssignPhysicalCard.getUser().getUserDetail().getEmail());
					physicalCardRequestListDTO.setKitNo(mAgentAssignPhysicalCard.getKitNo());
					String firstName = mAgentAssignPhysicalCard.getUser().getUserDetail().getFirstName();
					String lastName = "";
					if (mAgentAssignPhysicalCard.getUser().getUserDetail().getLastName() != null && !mAgentAssignPhysicalCard.getUser().getUserDetail().getLastName().isEmpty()) {
						lastName = mAgentAssignPhysicalCard.getUser().getUserDetail().getLastName();
					}
					physicalCardRequestListDTO.setName(firstName + " " + lastName);
					physicalCardRequestListDTO.setUserType(mAgentAssignPhysicalCard.getUser().getAccountDetail().getAccountType().getName());
					lCardRequestListDTOs.add(physicalCardRequestListDTO);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return lCardRequestListDTOs;
	}


	@Override
	public long userCountBasedOnAgent(MUser agent) throws Exception {
		long userCount = 0;
		try {
			if (agent != null) {
				userCount = mUserAgentRepository.getCountOfRegisterUserBasedOnAgent(agent);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return userCount;
	}


	@Override
	public void changePassword(ChangePasswordRequest dto) throws Exception {
		try {
			if (dto != null) {
				MUser mUser = mUserRespository.findByUsername(dto.getUsername());
				if (mUser != null) {
					if (passwordEncoder.matches(dto.getPassword(), mUser.getPassword())) {
						mUser.setPassword(passwordEncoder.encode(dto.getNewPassword()));
						mUserRespository.save(mUser);
					} else {
						throw new Exception("Current password does not match");
					}
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}


	@Override
	public ForgetPasswordRequestDTO otpForForgetPassword(MUser mUser) throws Exception {
		ForgetPasswordRequestDTO resp = new ForgetPasswordRequestDTO();
		try {
			if (mUser != null) {
				String otp = CommonUtil.generateSixDigitNumericString();
				mUser.setMobileToken(otp);
				mUserRespository.save(mUser);
				smsSenderApi.sendAgentForgetPasswordSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.FORGET_PASSWORD_OTP, mUser, otp);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return resp;
	}


	@Override
	public ForgetPasswordRequestDTO forgetPasswordRequest(MUser mUser , ChangePasswordRequest dto) throws Exception {
		ForgetPasswordRequestDTO resp = new ForgetPasswordRequestDTO();
		try {
			if (mUser != null) {
				if (mUser.getMobileToken().equalsIgnoreCase(dto.getKey())) {
					mUser.setMobileToken(null);
					String encryptedPassword = passwordEncoder.encode(dto.getNewPassword());
					mUser.setPassword(encryptedPassword);
					mUserRespository.save(mUser);
					smsSenderApi.sendAgentForgetPasswordSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.FORGET_PASSWORD_SUCCESS, mUser, null);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage(ResponseStatus.SUCCESS.getKey());
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Please enter correct OTP.");
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return resp;
	}


	@Override
	public MUser checkAgentExist(String userName) throws Exception {
		MUser mUser = null;
		try {
			if (userName != null && !userName.isEmpty()) {
				mUser = mUserRespository.getAgentBasedOnUserName(userName);
				if (mUser != null) {
					return mUser;
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return mUser;
	}


	@Override
	public List<AgentTransactionListDTO> getAgentTransactionList() throws Exception {
		List<AgentTransactionListDTO> agentTransactionListDTOs = new ArrayList<>();
		try {
			List<LoadCard> loadCards = loadCardRepository.getDetailsBasedOnUserType(Role.AGENT);
			for (LoadCard loadCard : loadCards) {
				AgentTransactionListDTO agentTransactionListDTO = new AgentTransactionListDTO();
				BeanUtils.copyProperties(loadCard, agentTransactionListDTO);
				agentTransactionListDTOs.add(agentTransactionListDTO);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return agentTransactionListDTOs;
	}


	@Override
	public double countTotalTransferredAmount(MUser agent) throws Exception {
		double count = 0;
		try {
			if (agent != null) {
				count = loadCardRepository.getTotalAmountOfAgent(agent);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return count;
	}


	@Override
	public long countPhysicalCards(MUser agent) throws Exception {
		long countPhysicalCard = 0;
		try {
			if (agent != null) {
				countPhysicalCard = mAgentAssignPhysicalCardRepository.getTotalPhysicalCardCount(agent);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return countPhysicalCard;
	}


	@Override
	public CommonError checkUserKYC(MUser mUser) throws Exception {
		CommonError error = new CommonError();
		error.setValid(true);
		try {
			MUserAgent mUserAgent = mUserAgentRepository.getUserBasedOnUser(mUser);
			MPQAccountType mpqAccountType = mUser.getAccountDetail().getAccountType();
			if (mpqAccountType != null) {
				if (mUserAgent != null) {
					MKycDetail mKycDetail = mUserAgent.getKycDetail();
					if (mKycDetail != null) {
						if (mpqAccountType.getCode().equalsIgnoreCase("NONKYC")) {
							error.setValid(false);
							error.setMessage("Please wait to get your KYC approved.");
						}
					} else {
						error.setValid(false);
						error.setMessage("Please do your KYC first.");
					}
				}
			}


		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return error;
	}

	@Override
	public JSONObject findTotalTransactionByMonth(MUser agent) {
		JsonArray jsonArray = new JsonArray();
		JSONObject data=new JSONObject();
		try {
			YearMonth thisMonth1 = YearMonth.now();
			YearMonth thisMonth = thisMonth1.minusMonths(5);
			List<String> month = new ArrayList<String>();
			List<Long> visits = new ArrayList<Long>();
			for (int i = 7; i > 1; i--) {
				YearMonth lastMonth = thisMonth.plusMonths(1);
				long count = mTransactionRepository.findCountByMonthUser(dateTimeFormat.parse(thisMonth + "-01 00:00"),
						dateTimeFormat.parse(lastMonth + "-01 00:00") , agent.getAccountDetail());
				DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MM");
				int ss = Integer.valueOf(thisMonth.format(monthYearFormatter));
				String Month = new DateFormatSymbols().getMonths()[ss - 1];
				thisMonth = lastMonth;

				JsonObject payload = new JsonObject();
				payload.addProperty("date", Month);
				payload.addProperty("visits", count + "");
				month.add(Month);
				visits.add(count);
				jsonArray.add(payload);
				
			}
			ObjectMapper objectMapper = new ObjectMapper();
			String arrayToJson = objectMapper.writeValueAsString(month);
			System.err.println(arrayToJson);
			
			String visitsToJson = objectMapper.writeValueAsString(visits);
			System.err.println(visitsToJson);

			data.put("arrayToJson", arrayToJson);
			data.put("visitsToJson", visitsToJson);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;

	}

	@Override
	public JSONObject findTotalUserByMonth(MUser agent) {
		JsonArray userArray = new JsonArray();
		JSONObject data=new JSONObject();
		
		try {
			YearMonth thisMonth1 = YearMonth.now();
			YearMonth thisMonth = thisMonth1.minusMonths(5);
			List<String> month = new ArrayList<String>();
			List<Long> visits = new ArrayList<Long>();
			
			for (int i = 7; i > 1; i--) {
				YearMonth lastMonth = thisMonth.plusMonths(1);
				long count = mUserAgentRepository.findUserCountByMonth(dateTimeFormat.parse(thisMonth + "-01 00:00"),
						dateTimeFormat.parse(lastMonth + "-01 00:00"),agent);
				DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MM");
				int ss = Integer.valueOf(thisMonth.format(monthYearFormatter));
				String Month = new DateFormatSymbols().getMonths()[ss - 1];
				thisMonth = lastMonth;

				JsonObject payload = new JsonObject();
				payload.addProperty("date", Month);
				payload.addProperty("visits", count + "");
				
				month.add(Month);
				visits.add(count);
				
				userArray.add(payload);
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			String arrayToJson = objectMapper.writeValueAsString(month);
			System.err.println(arrayToJson);
			
			String visitsToJson = objectMapper.writeValueAsString(visits);
			System.err.println(visitsToJson);

			data.put("arrayToJson", arrayToJson);
			data.put("visitsToJson", visitsToJson);
			
			/*userArray.add(data);*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}


	@Override
	public List<String> getAllBankDetails() throws Exception {
		try {
			List<String> bankName = bankRepository.getBankName();
			return bankName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
