package com.msscard.app.api.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;

import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.LoginResponse;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.app.sms.util.SMSUtil;
import com.msscard.entity.ApiVersion;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.BusTicket;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.Donation;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupOtpMobile;
import com.msscard.entity.GroupSms;
import com.msscard.entity.LoadCardOtp;
import com.msscard.entity.LoginLog;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MpinLogs;
import com.msscard.entity.PartnerDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PromoCode;
import com.msscard.entity.PromoCodeRequest;
import com.msscard.entity.UserSession;
import com.msscard.entity.VersionLogs;
import com.msscard.model.ApiVersionDTO;
import com.msscard.model.DonationDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.MPinChangeDTO;
import com.msscard.model.MpinDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.repositories.ApiVersionRepository;
import com.msscard.repositories.BulkCardAssignmentGroupRepository;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.BusTicketRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.DonationRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.GroupBulkRegisterRepository;
import com.msscard.repositories.GroupDetailsRepository;
import com.msscard.repositories.GroupOtpRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.LoadCardOtpRepository;
import com.msscard.repositories.LoginLogRepository;
import com.msscard.repositories.MBulkRegisterRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MPinLogsRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.OtpRepository;
import com.msscard.repositories.PartnerDetailsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.PromoCodeRepository;
import com.msscard.repositories.PromoCodeRequestRepository;
import com.msscard.repositories.TwoMinBlockRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.repositories.VersionLogsRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.ConvertUtil;
import com.msscard.util.MailTemplate;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.CommonValidation;
import com.msscards.session.SessionLoggingStrategy;

public class UserApiImpl implements IUserApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");

	private final PasswordEncoder passwordEncoder;
	private final MUserRespository mUserRespository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRespository;
	private final ISMSSenderApi smsSenderApi;
	private final LoginLogRepository loginLogRepository;
	private final MMCardRepository mmCardRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final MKycRepository mKycRepository;
	private final MBulkRegisterRepository mBulkRegisterRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final PromoCodeRepository promoCodeRepository;
	private final PromoCodeRequestRepository promoCodeRequestRepository;
	private final ApiVersionRepository apiVersionRepository;
	private final VersionLogsRepository versionLogsRepository;
	private final IMatchMoveApi matchMoveApi;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final PartnerDetailsRepository partnerDetailsRepository;
	private final MPinLogsRepository mPinLogsRepository;
	private final GroupDetailsRepository groupDetailsRepository;
	private final SessionLoggingStrategy sls;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final DonationRepository donationRepository;
	private final MMCardRepository mMCardRepository;
	private final OtpRepository otpRepository;
	private final GroupOtpRepository groupOtpRepository;
	private final GroupSmsRepository groupSmsRepository;
	private final BusTicketRepository busTicketRepository;
	private final TwoMinBlockRepository twoMinBlockRepository;
	private final IMailSenderApi mailSenderApi;
	private final LoadCardOtpRepository loadCardOtpRepository;
	private final GroupBulkRegisterRepository groupBulkRegisterRepository;
	private final BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository;
	private final GroupBulkKycRepository groupBulkKycRepository;

	private final SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public UserApiImpl(PasswordEncoder passwordEncoder, MUserRespository mUserRespository,
			MUserDetailRepository mUserDetailRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			MPQAccountTypeRepository mPQAccountTypeRespository, ISMSSenderApi smsSenderApi,
			LoginLogRepository loginLogRepository, MMCardRepository mmCardRepository,
			MTransactionRepository mTransactionRepository, MServiceRepository mServiceRepository,
			MKycRepository mKycRepository, MBulkRegisterRepository mBulkRegisterRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, PromoCodeRepository promoCodeRepository,
			PromoCodeRequestRepository promoCodeRequestRepository, ApiVersionRepository apiVersionRepository,
			VersionLogsRepository versionLogsRepository, IMatchMoveApi matchMoveApi, SessionLoggingStrategy sls,
			CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			GroupDetailsRepository groupDetailsRepository, BulkRegisterRepository bulkRegisterRepository,
			PartnerDetailsRepository partnerDetailsRepository, MPinLogsRepository mPinLogsRepository,
			AuthenticationManager authenticationManager, UserSessionRepository userSessionRepository,
			DonationRepository donationRepository, MMCardRepository mMCardRepository, OtpRepository otpRepository,
			GroupOtpRepository groupOtpRepository, GroupSmsRepository groupSmsRepository,
			BusTicketRepository busTicketRepository, TwoMinBlockRepository twoMinBlockRepository,
			IMailSenderApi mailSenderApi, LoadCardOtpRepository loadCardOtpRepository,
			GroupBulkRegisterRepository groupBulkRegisterRepository, BulkCardAssignmentGroupRepository bulkCardAssignmentGroupRepository
			, GroupBulkKycRepository groupBulkKycRepository) {
		super();
		this.passwordEncoder = passwordEncoder;
		this.mUserRespository = mUserRespository;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.mPQAccountTypeRespository = mPQAccountTypeRespository;
		this.smsSenderApi = smsSenderApi;
		this.loginLogRepository = loginLogRepository;
		this.mmCardRepository = mmCardRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mServiceRepository = mServiceRepository;
		this.mKycRepository = mKycRepository;
		this.mBulkRegisterRepository = mBulkRegisterRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.promoCodeRepository = promoCodeRepository;
		this.promoCodeRequestRepository = promoCodeRequestRepository;
		this.apiVersionRepository = apiVersionRepository;
		this.versionLogsRepository = versionLogsRepository;
		this.matchMoveApi = matchMoveApi;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.partnerDetailsRepository = partnerDetailsRepository;
		this.mPinLogsRepository = mPinLogsRepository;
		this.groupDetailsRepository = groupDetailsRepository;
		this.sls = sls;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.donationRepository = donationRepository;
		this.mMCardRepository = mMCardRepository;
		this.otpRepository = otpRepository;
		this.groupOtpRepository = groupOtpRepository;
		this.groupSmsRepository = groupSmsRepository;
		this.busTicketRepository = busTicketRepository;
		this.twoMinBlockRepository = twoMinBlockRepository;
		this.mailSenderApi = mailSenderApi;
		this.loadCardOtpRepository = loadCardOtpRepository;
		this.groupBulkRegisterRepository = groupBulkRegisterRepository;
		this.bulkCardAssignmentGroupRepository = bulkCardAssignmentGroupRepository;
		this.groupBulkKycRepository = groupBulkKycRepository;
	}

	@Override
	public void saveUser(RegisterDTO dto) throws Exception {
		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setAddress(dto.getAddress());
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setMiddleName(dto.getMiddleName());
		userDetail.setEmail(dto.getEmail());

		if(dto.getDateOfBirth()!=null) {
			if (UserType.Corporate.equals(dto.getUserType())) {
				  userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth())); 
				  }
		}
		 
		userDetail.setBrand(dto.getBrand());
		userDetail.setModel(dto.getModel());
		userDetail.setImeiNo(dto.getImeiNo());
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setRemark(dto.getRemark());
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}

		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
				|| dto.getUserType().equals(UserType.Corporate)) {
			if (dto.getUserType().equals(UserType.Admin)) {
				user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Merchant)) {
				user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Corporate)) {
				user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
			}
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			user.setPassword(hashedPassword);
			user.setFullyFilled(true);
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			GroupDetails group = groupDetailsRepository.getGroupDetailsByContact(dto.getGroup());
			if (group != null) {
				user.setGroupDetails(group);
			}

		} else {
			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

		}
		user.setUserDetail(userDetail);
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		if (dto.getUserType().equals(UserType.Corporate)) {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		} else {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		}
		if (dto.getUserType().equals(UserType.Corporate)) {

		}
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
				corporateDetails.setCorporateName(dto.getCorporateName());
				corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
				corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
				corporateDetails.setServiceStatus(true);
				corporateDetails.setBankAccountNo(dto.getBankAccountNo());
				corporateDetails.setIfscCode(dto.getIfscCode());
				corporateDetails.setCorporate(user);
				corporateDetails.setContactPersonNumber(dto.getContactNo());
				corporateAgentDetailsRepository.save(corporateDetails);

			}

		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
				corporateDetails.setCorporateName(dto.getCorporateName());
				corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
				corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
				corporateDetails.setServiceStatus(true);
				corporateDetails.setBankAccountNo(dto.getBankAccountNo());
				corporateDetails.setIfscCode(dto.getIfscCode());
				corporateDetails.setCorporate(user);
				corporateAgentDetailsRepository.save(corporateDetails);
			}
		}
		if (dto.getUserType().equals(UserType.User)) {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
		}

	}
	
	@Override
	public List<BulkCardAssignmentGroup> getgroupBulkCarFailListPost(UserDataRequestDTO dto) {
		List<BulkCardAssignmentGroup> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<BulkCardAssignmentGroup> page = bulkCardAssignmentGroupRepository.getBulkCardFailListPost(pageable,
					dto.getFrom(), dto.getTo(), Status.Failed, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	// bulk register corporate

	@Override
	public void saveUserCorpUser(RegisterDTO dto) throws Exception {
		try {
			MUser user = new MUser();
			user.setCorporateUser(true);
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setRemark(dto.getRemark());
			userDetail.setGroupStatus(Status.Active.getValue());

			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());
			GroupDetails group = groupDetailsRepository.getGroupDetailsByContact(dto.getGroup());
			if (group != null) {
				user.setGroupDetails(group);
			} else {
				GroupDetails g = groupDetailsRepository.getGroupDetailsByContact("None");
				user.setGroupDetails(g);
			}
			if (dto.getAndroidDeviceID() != null) {
				System.err.println(dto.getAndroidDeviceID());
				user.setAndroidDeviceID(dto.getAndroidDeviceID());
				MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
				if (androidUser != null) {
					user.setDeviceLocked(false);
				} else {
					user.setDeviceLocked(true);
				}
			}
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
					|| dto.getUserType().equals(UserType.Corporate)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Corporate)) {
					user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
				}
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				user.setPassword(hashedPassword);
				user.setFullyFilled(true);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());

			} else {
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
				user.setFullyFilled(true);
				user.setCorporateUser(false);

			}
			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setBranchCode(dto.getBranchCode());
				pqAccountDetail.setAccountType(nonKYCAccountType);
				user.setAccountDetail(pqAccountDetail);
			} else {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setBranchCode(dto.getBranchCode());
				pqAccountDetail.setAccountType(nonKYCAccountType);
				user.setAccountDetail(pqAccountDetail);
			}
			if (dto.getUserType().equals(UserType.Corporate)) {

			}
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				if (dto.getUserType().equals(UserType.Corporate)) {
					CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
					corporateDetails.setCorporateName(dto.getCorporateName());
					corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
					corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
					corporateDetails.setServiceStatus(true);
					corporateDetails.setBankAccountNo(dto.getBankAccountNo());
					corporateDetails.setIfscCode(dto.getIfscCode());
					corporateDetails.setCorporate(user);
					corporateDetails.setContactPersonNumber(dto.getContactNo());
					corporateAgentDetailsRepository.save(corporateDetails);

				}

			} else {

				mUserRespository.delete(tempUser);
				mUserDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				if (dto.getUserType().equals(UserType.Corporate)) {
					CorporateAgentDetails corporateDetails = new CorporateAgentDetails();
					corporateDetails.setCorporateName(dto.getCorporateName());
					corporateDetails.setCorporateLogo1(dto.getCorporateLogo1Path());
					corporateDetails.setCorporateLogo2(dto.getCorporateLogo2Path());
					corporateDetails.setServiceStatus(true);
					corporateDetails.setBankAccountNo(dto.getBankAccountNo());
					corporateDetails.setIfscCode(dto.getIfscCode());
					corporateDetails.setCorporate(user);
					corporateAgentDetailsRepository.save(corporateDetails);
				}
			}
			if (dto.getUserType().equals(UserType.User)) {
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CORPORATE_USER_PASSWORD, user, null);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Bulk Register

	public void saveUserForBulkRegister(RegisterDTO dto) throws Exception {

		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setAddress(dto.getAddress());
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setMiddleName(dto.getMiddleName());
		userDetail.setEmail(dto.getEmail());

		userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
		userDetail.setBrand(dto.getBrand());
		userDetail.setModel(dto.getModel());
		userDetail.setImeiNo(dto.getImeiNo());
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}

		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)
				|| dto.getUserType().equals(UserType.Corporate)) {
			if (dto.getUserType().equals(UserType.Admin)) {
				user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Merchant)) {
				user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			} else if (dto.getUserType().equals(UserType.Corporate)) {
				user.setAuthority(Authorities.CORPORATE + "," + Authorities.AUTHENTICATED);
			}
			String hashedPassword = passwordEncoder.encode(dto.getPassword());
			user.setPassword(hashedPassword);
			user.setFullyFilled(true);
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());

		} else {

			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

		}
		user.setUserDetail(userDetail);
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		if (dto.getUserType().equals(UserType.Corporate)) {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		} else {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
		}
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				MKycDetail kyc = new MKycDetail();
				kyc.setAadharImage(dto.getAadharImagePath());
				kyc.setPancardImage(dto.getPanCardImagePath());
				kyc.setAccountNo(dto.getBankAccountNo());
				kyc.setIfscCode(dto.getIfscCode());
				kyc.setUserType(UserType.Corporate);
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}

		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
			if (dto.getUserType().equals(UserType.Corporate)) {
				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("SUPERKYC");
				MKycDetail kyc = new MKycDetail();
				kyc.setAadharImage(dto.getAadharImagePath());
				kyc.setPancardImage(dto.getPanCardImagePath());
				kyc.setAccountNo(dto.getBankAccountNo());
				kyc.setIfscCode(dto.getIfscCode());
				kyc.setUserType(UserType.Corporate);
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}
		}
		if (dto.getUserType().equals(UserType.User)) {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
		}

	}

	@Override
	public MUser findByUserName(String name) {
		MUser user = mUserRespository.findByUsername(name);
		return user;
	}

	@Override
	public MMCards findByUserWallet(MUser u) {
		MMCards wallet = mmCardRepository.findCardByUser(u);
		return wallet;
	}

	@Override
	public MMCards findByUserWalletPhysical(MUser u) {
		MMCards wallet = mmCardRepository.getPhysicalCardByUser(u);
		return wallet;
	}

	@Override
	public MMCards findByUserWalletVirtual(MUser u) {
		MMCards wallet = mmCardRepository.getVirtualCardsByCardUser(u);
		return wallet;
	}

	@Override
	public void changePasswordRequest(MUser u) {
		u.setMobileToken(CommonUtil.generateSixDigitNumericString());
		mUserRespository.save(u);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CHANGE_PASSWORD_REQUEST, u, null);
	}

	@Override
	public boolean checkMobileToken(String key, String mobileNumber) {
		MUser user = mUserRespository.findByMobileTokenAndStatus(key, mobileNumber, Status.Inactive);
		if (user == null) {
			return false;
		} else {
			user.setMobileStatus(Status.Active);
			user.setMobileToken(null);
			mUserRespository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_SUCCESS, user, null);
			return true;
		}
	}

	@Override
	public boolean resendMobileTokenNew(String username) {
		MUser u = mUserRespository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			MUser uu = mUserRespository.findByUsernameAndStatus(username, Status.Inactive);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, uu, null);
			return true;
		} else {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public UserDTO getUserById(Long id) {
		try {
			MUser u = mUserRespository.findOne(id);
			if (u != null) {
				UserDTO d = ConvertUtil.convertUser(u);
				MMCards cards = mMCardRepository.getVirtualCardsByCardUser(u);
				if (cards != null) {
					if (Status.Inactive.getValue().equals(cards.getStatus())) {
						d.setVirtualCardBlock(true);
					} else {
						d.setVirtualCardBlock(false);
					}
				}
				MMCards card = mMCardRepository.getPhysicalCardByUser(u);
				if (card != null) {
					if (Status.Inactive.getValue().equals(card.getStatus())) {
						d.setPhysicalCardBlock(true);
					} else {
						d.setPhysicalCardBlock(false);
					}
				}
				return d;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int updateGcmId(String gcmId, String username) {
		int rowsUpdated = mUserRespository.updateGCMID(gcmId, username);
		return rowsUpdated;
	}

	@Override
	public void requestNewLoginDeviceForSuperAdmin(MUser user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		mUserRespository.save(user);
		smsSenderApi.sendAnotherUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SUPERADMIN_VERIFICATION_MOBILE, user, null);
	}

	@Override
	public void requestNewLoginDevice(MUser user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		mUserRespository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public boolean isValidLoginToken(String otp, MUser user) {
		boolean isValidToken = false;
		String mobileToken = user.getMobileToken();
		if (mobileToken != null) {
			if (otp.equalsIgnoreCase(mobileToken)) {
				isValidToken = true;
				user.setMobileToken(null);
				mUserRespository.save(user);
			}
		}
		return isValidToken;
	}

	@Override
	public String handleLoginFailure(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				if (Status.Active.equals(user.getMobileStatus())) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					LoginLog loginLog = new LoginLog();
					loginLog.setUser(user);
					loginLog.setRemoteAddress(ipAddress);
					loginLog.setServerIp(request.getRemoteAddr());
					loginLog.setStatus(Status.Failed);
					loginLogRepository.save(loginLog);

					List<LoginLog> llsFailed = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getLoginAttempts() - failedCount;
					if (remainingAttempts < 0 || failedCount == getLoginAttempts()) {
						if (user.getUserType() == UserType.Merchant) {
							String authority = Authorities.MERCHANT + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.Admin) {
							String authority = Authorities.ADMINISTRATOR + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.SuperAdmin) {
							String authority = Authorities.SUPER_ADMIN + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect password. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				LoginLog loginLog = new LoginLog();
				loginLog.setUser(user);
				loginLog.setRemoteAddress(ipAddress);
				loginLog.setServerIp(request.getRemoteAddr());
				loginLog.setStatus(Status.Success);
				loginLogRepository.save(loginLog);
				List<LoginLog> lls = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
				for (LoginLog ll : lls) {
					loginLogRepository.deleteLoginLogForId(Status.Deleted, ll.getId());
				}
			}
		}
	}

	@Override
	public int getMpinAttempts() {
		return 5;
	}

	@Override
	public int getLoginAttempts() {
		return 5;
	}

	@Override
	public void updateUserAuthority(String authority, long id) {
		mUserRespository.updateUserAuthority(authority, id);
	}

	@Override
	public long getMCardCount() {
		long cardCount = 0;
		try {
			cardCount = mmCardRepository.findTotalCards();
		} catch (Exception e) {
		}
		return cardCount;
	}

	@Override
	public List<MMCards> findAllVirtualCards() {
		return (List<MMCards>) mmCardRepository.findAllVirtualCards();
	}

	@Override
	public Page<MMCards> findCardByPage(PagingDTO dto, boolean flag) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Date date1 = dto.getFromDate();
		Date date2 = dto.getToDate();
		return mmCardRepository.getAllCardsByPage(pageable, flag, date1, date2, dto.getCardStatus());
	}

	@Override
	public List<MMCards> findAllPhysicalCards() {
		return (List<MMCards>) mmCardRepository.findAllPhysicalCards();
	}

	@Override
	public List<PhysicalCardDetails> findAllPhysicalCardRequestList(String status) {

		if (status.equalsIgnoreCase(Status.Requested + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Requested);
		} else if (status.equalsIgnoreCase(Status.Shipped + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository.findPhysicalCardRequestList(Status.Shipped);
		} else if (status.equalsIgnoreCase(Status.Declined + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Declined);
		} else if (status.equalsIgnoreCase(Status.Active + "")) {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository.findPhysicalCardRequestList(Status.Active);
		} else {
			return (List<PhysicalCardDetails>) physicalCardDetailRepository
					.findPhysicalCardRequestList(Status.Received);
		}
	}

	@Override
	public List<MMCards> findAllCards() {
		return (List<MMCards>) mmCardRepository.findAllCards();
	}

	@Override
	public List<MTransaction> findAllCardTransactioons(DateDTO dto) {
		try {
			dto.setStartDate(dateTime.parse(dto.getFromDate() + " 23:59:59"));
			dto.setEndDate(dateTime.parse(dto.getToDate() + " 00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MService service = mServiceRepository.findByName(dto.getServiceName());
		return mTransactionRepository.findCardTransactions(dto.getStartDate(), dto.getEndDate(), service);
	}

	@Override
	public MUser findByUserAccount(MPQAccountDetails account) {
		return mUserRespository.findByAccountDetails(account);
	}

	@Override
	public MMCards findCardByUser(MUser usera) {
		return mmCardRepository.findCardByUser(usera);
	}

	@Override
	public boolean changePassword(ChangePasswordRequest dto) {
		MUser user = mUserRespository.findByMobileTokenAndStatus(dto.getKey(), dto.getUsername(), Status.Active);
		if (user == null) {
			return false;
		} else {
			user.setMobileToken(null);
			String pass = CommonUtil.base64Decode(dto.getPassword());
			user.setPassword(passwordEncoder.encode(pass));
			mUserRespository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_SUCCESS, user, null);
			mailSenderApi.sendChangePassword("Ewire Password Change Successful", MailTemplate.CHANGE_PASSWORD, user);
			return true;
		}
	}

	@Override
	public boolean changeProfilePassword(ChangePasswordRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user == null) {
			return false;
		} else {
			user.setMobileToken(null);
			String newPass = CommonUtil.base64Decode(dto.getNewPassword());
			user.setPassword(passwordEncoder.encode(newPass));
			mUserRespository.save(user);
			mailSenderApi.sendChangePassword("Ewire Password Change Successful", MailTemplate.CHANGE_PASSWORD, user);
			return true;
		}
	}

	@Override
	public void saveCustomUser(RegisterDTO dto) throws Exception {

		MUser user = new MUser();
		MUserDetails userDetail = new MUserDetails();
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setLastLoginDevice(dto.getLastLoginDevice());
		if (dto.getAndroidDeviceID() != null) {
			System.err.println(dto.getAndroidDeviceID());
			user.setAndroidDeviceID(dto.getAndroidDeviceID());
			MUser androidUser = mUserRespository.findByAndroidDeviceId(dto.getAndroidDeviceID(), true);
			if (androidUser != null) {
				user.setDeviceLocked(false);
			} else {
				user.setDeviceLocked(true);
			}
		}
		user.setGcmId(dto.getRegistrationId());
		user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
		user.setMobileStatus(Status.Inactive);
		user.setEmailStatus(Status.Active);
		user.setUsername(dto.getContactNo().toLowerCase());
		user.setUserType(dto.getUserType());
		user.setMobileToken(CommonUtil.generateSixDigitNumericString());
		user.setEmailToken("E" + System.currentTimeMillis());
		user.setFullyFilled(false);
		user.setUserDetail(userDetail);
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName("None");
		user.setGroupDetails(gd);

		MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
		pqAccountDetail.setBalance(0);
		pqAccountDetail.setBranchCode(dto.getBranchCode());
		pqAccountDetail.setAccountType(nonKYCAccountType);
		user.setAccountDetail(pqAccountDetail);
		MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			mUserDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			mPQAccountDetailRepository.save(pqAccountDetail);
			mUserRespository.save(user);
		} else {

			mUserRespository.delete(tempUser);
			mUserDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			mUserDetailRepository.save(userDetail);
			mUserRespository.save(user);
		}
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public void saveFullDetails(RegisterDTO dto) throws Exception {
		MUser user = findByUserName(dto.getContactNo());
		MUserDetails userDetail = user.getUserDetail();
		userDetail.setFirstName(dto.getFirstName());
		userDetail.setLastName(dto.getLastName());
		userDetail.setEmail(dto.getEmail());
		userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
		userDetail.setIdType(dto.getIdType());
		userDetail.setIdNo(dto.getIdNo());
		userDetail.setGroupStatus(Status.Inactive.getValue());
		userDetail.setRemark(dto.getRemark());
		userDetail.setCountryName("India");
		GroupDetails group = groupDetailsRepository.getGroupDetailsByContact(dto.getGroup());
		if (group != null) {
			user.setGroupDetails(group);
		} else {
			GroupDetails g = groupDetailsRepository.getGroupDetailsByContact("None");
			user.setGroupDetails(g);
		}
		if (dto.isHasAadhar()) {
			userDetail.setAadharNo(dto.getAadharNo());
		}
		String pass = CommonUtil.base64Decode(dto.getPassword());
		user.setPassword(passwordEncoder.encode(pass));
		user.setFullyFilled(true);

		mUserDetailRepository.save(userDetail);
		mUserRespository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.WELCOME_SMS, user, null);
		mailSenderApi.sendRegistrationSuccess("Registration Successful", MailTemplate.REGISTRATION_SUCCESS, user);
	}

	@Override
	public List<MKycDetail> findAllCorporateAgent() {
		return (List<MKycDetail>) mKycRepository.findCorporateAgent(UserType.Corporate);
	}

	@Override
	public List<MTransaction> findSingleCorporateTransactioons(DateDTO dto) {
		try {
			dto.setStartDate(dateTime.parse(dto.getFromDate() + " 23:59:59"));
			dto.setEndDate(dateTime.parse(dto.getToDate() + " 00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MPQAccountDetails account = mPQAccountDetailRepository.findAccount(dto.getAccountId());
		return mTransactionRepository.findCorporateTransactions(dto.getStartDate(), dto.getEndDate(), account);
	}

	@Override
	public ResponseDTO saveBulkUser(RegisterDTO dto) {

		ResponseDTO resp = new ResponseDTO();
		try {
			for (int j = 0; j < dto.getBulkRegisterDTO().size(); j++) {
				MBulkRegister reg = mBulkRegisterRepository
						.findByContactno(dto.getBulkRegisterDTO().get(j).getContactNo());
				if (reg == null) {
					MBulkRegister user = new MBulkRegister();
					user.setContactNo(dto.getBulkRegisterDTO().get(j).getContactNo());
					user.setEmail(dto.getBulkRegisterDTO().get(j).getEmail());
					user.setName(dto.getBulkRegisterDTO().get(j).getFirstName());
					user.setDateOfBirth(formatter.parse(dto.getBulkRegisterDTO().get(j).getDateOfBirth()));
					mBulkRegisterRepository.save(user);
				}
			}
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			resp.setMessage("Bulk Registration Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("Service down, Please try again later.");
		}
		return resp;

	}

	@Override
	public List<MBulkRegister> findLast10Register() {
		return mBulkRegisterRepository.findLast10Register();
	}

	@Override
	public void upgradeAccount(UpgradeAccountDTO dto) {
		MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		MUser user = mUserRespository.findByUsername(dto.getContactNo());
		MKycDetail detail = mKycRepository.findByUser(user);
		if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("KYC")) {

		} else if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("NONKYC")
				&& dto.getImage2() != null) {
			// MKycDetail check=mKycRepository.findByUser(user);

			logger.info("address1 :: " + dto.getAddress1());
			logger.info("addres2 :: " + dto.getAddres2());
			logger.info("address2:: " + dto.getAddress2());
			logger.info("city:: " + dto.getCity());
			logger.info("state:: " + dto.getState());
			logger.info("pincode:: " + dto.getPincode());
			logger.info("country:: " + dto.getCountry());
			logger.info("idType:: " + dto.getIdType());
			logger.info("idNumber:: " + dto.getIdNumber());

			detail.setUserType(dto.getUserType());
			detail.setIdType(dto.getIdType());
			detail.setIdNumber(dto.getIdNumber());
			if (dto.getAddress1().length() > 25) {
				detail.setAddress1(dto.getAddress1().substring(0, 25).replace('/', ' ').replace('.', ' '));
			} else {
				detail.setAddress1(dto.getAddress1().replace('/', ' ').replace('.', ' '));
			}
			if (dto.getAddres2().length() > 25) {
				detail.setAddress2(dto.getAddres2().substring(0, 25).replace('/', ' ').replace('.', ' '));
			} else {
				detail.setAddress2(dto.getAddres2().replace('/', ' ').replace('.', ' '));
			}
			detail.setCity(dto.getCity());
			detail.setState(dto.getState());
			detail.setPinCode(dto.getPincode());
			detail.setCountry(dto.getCountry());
			detail.setIdImage(dto.getImagePath());
			detail.setIdImage1(dto.getImagePath2());
			if (detail.isRejectionStatus() == true) {
				detail.setRejectionStatus(false);
			}

			mKycRepository.save(detail);
			// UserKycRequest requ=new UserKycRequest();
			/*
			 * requ.setFile(file); resp=matchMoveApi.setImagesForKyc(detail);
			 */

		}

		else {
			MKycDetail check = mKycRepository.findByUser(user);
			if (check == null) {
				MKycDetail kyc = new MKycDetail();
				if (dto.getIdType().trim().equalsIgnoreCase("AADHAAR")) {
					dto.setIdType("aadhaar");
				}
				if (dto.getIdType().trim().equalsIgnoreCase("DRIVING LICENSE")) {
					dto.setIdType("drivers_id");
				}
				if (dto.getIdType().trim().equalsIgnoreCase("PASSPORT")) {
					dto.setIdType("passport");
				}

				logger.info("address1 :: " + dto.getAddress1());
				logger.info("addres2 :: " + dto.getAddres2());
				logger.info("address2:: " + dto.getAddress2());
				logger.info("city:: " + dto.getCity());
				logger.info("state:: " + dto.getState());
				logger.info("pincode:: " + dto.getPincode());
				logger.info("country:: " + dto.getCountry());
				logger.info("idType:: " + dto.getIdType());
				logger.info("idNumber:: " + dto.getIdNumber());

				kyc.setUserType(dto.getUserType());
				kyc.setIdType(dto.getIdType());
				kyc.setIdNumber(dto.getIdNumber());
				/*
				 * kyc.setAddress1(dto.getAddress1().substring(0, 25).replace('/', '
				 * ').replace('.', ' ')); kyc.setAddress2(dto.getAddres2().substring(0,
				 * 25).replace('/', ' ').replace('.', ' '));
				 */
				if (dto.getAddress1().length() > 25) {
					kyc.setAddress1(dto.getAddress1().substring(0, 25).replace('/', ' ').replace('.', ' '));
				} else {
					kyc.setAddress1(dto.getAddress1());
				}
				if (dto.getAddres2().length() > 25) {
					kyc.setAddress2(dto.getAddres2().substring(0, 25).replace('/', ' ').replace('.', ' '));
				} else {
					kyc.setAddress2(dto.getAddres2());
				}
				kyc.setCity(dto.getCity());
				kyc.setState(dto.getState());
				kyc.setPinCode(dto.getPincode());
				kyc.setCountry(dto.getCountry());
				kyc.setIdImage(dto.getImagePath());
				if (dto.getImage2() != null) {
					kyc.setIdImage1(dto.getImagePath2());
				}
				kyc.setAccountType(nonKYCAccountType);
				kyc.setUser(user);
				mKycRepository.save(kyc);
			}
		}
	}

	@Override
	public List<MKycDetail> findAllKycRequest() {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return (List<MKycDetail>) mKycRepository.findKycUser(UserType.User, AccountType);
	}

	@Override
	public void updateKycRequest(UpgradeAccountDTO dto) {
		if ("Success".equalsIgnoreCase(dto.getAction())) {
			MPQAccountType KYCAccountType = mPQAccountTypeRespository.findByCode("KYC");
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);

			UserKycRequest requ = new UserKycRequest();
			requ.setAddress1(kyc.getAddress1());
			requ.setAddress2(kyc.getAddress2());
			requ.setCity(kyc.getCity());
			requ.setCountry(kyc.getCountry());
			requ.setState(kyc.getState());
			requ.setPinCode(kyc.getPinCode());
			requ.setId_number(kyc.getIdNumber());
			requ.setId_type(kyc.getIdType());
			System.err.println(dto.getIdType());
			System.err.println(dto.getIdNumber());
			requ.setUser(kyc.getUser());
			requ.setId_type(kyc.getIdType());
			requ.setId_number(kyc.getIdNumber());

			requ.setFilePath(kyc.getIdImage());

			UserKycResponse resp = matchMoveApi.kycUserMM(requ);

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
				if (kyc.getUser().getUserDetail().getDateOfBirth() == null) {
					requ.setDob("1990-02-03");
					resp = matchMoveApi.setIdDetails(requ);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
							resp = matchMoveApi.setImagesForKyc(requ);
							if (kyc.getIdImage1() != null) {
								requ.setFilePath(kyc.getIdImage1());
								resp = matchMoveApi.setImagesForKyc(requ);
							}
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
								matchMoveApi.tempConfirmKyc(kyc.getUser());
								UserKycResponse userKycResponse = matchMoveApi.tempKycStatusApproval(kyc.getUser());
								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKycResponse.getCode())) {
									kyc.setAccountType(KYCAccountType);
									mKycRepository.save(kyc);
									MPQAccountDetails account = kyc.getUser().getAccountDetail();
									account.setAccountType(KYCAccountType);
									mPQAccountDetailRepository.save(account);
									smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
											SMSTemplete.KYCREQUEST_SUCCESS, kyc.getUser(),
											String.valueOf(KYCAccountType.getMonthlyLimit()));
								}
							}

					}
				} else {
					if (kyc.getUser().getUserDetail().getDateOfBirth() == null) {
						requ.setDob("1990-02-03");
					} else {
						requ.setDob(formatter.format(kyc.getUser().getUserDetail().getDateOfBirth()));
					}
					resp = matchMoveApi.setIdDetails(requ);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
							resp = matchMoveApi.setImagesForKyc(requ);
							if (kyc.getIdImage1() != null) {
								requ.setFilePath(kyc.getIdImage1());
								resp = matchMoveApi.setImagesForKyc(requ);
							}
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
								matchMoveApi.tempConfirmKyc(kyc.getUser());
								UserKycResponse userKycResponse = matchMoveApi.tempKycStatusApproval(
										kyc.getUser());
								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKycResponse.getCode())) {
									kyc.setAccountType(KYCAccountType);
									mKycRepository.save(kyc);
									MPQAccountDetails account = kyc.getUser().getAccountDetail();
									account.setAccountType(KYCAccountType);
									mPQAccountDetailRepository.save(account);
									smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
											SMSTemplete.KYCREQUEST_SUCCESS, kyc.getUser(),
											String.valueOf(KYCAccountType.getMonthlyLimit()));
								}

							}

						}
					}
				}

			}
		} else {
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);
			kyc.setRejectionStatus(true);
			kyc.setRejectionReason(dto.getRejectionReason());
			mKycRepository.save(kyc);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.KYCREQUEST_FAILURE, kyc.getUser(),
					null);
		}
	}

	@Override
	public double monthlyLoadMoneyTransactionTotal(MUser user) {
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			total = mTransactionRepository.getMonthlyTransactionTotal(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public List<MUser> findAllUser() {
		return (List<MUser>) mUserRespository.findAllUser();
	}

	@Override
	public List<MUser> findAllActiveUser() {
		return (List<MUser>) mUserRespository.findAllActiveUserList();
	}

	@Override
	public List<MUser> findAllKYCUser() {
		return (List<MUser>) mUserRespository.findAllKYCUserList();
	}

	@Override
	public Page<MUser> findAllUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllUserList(pageable, dto.getFromDate(), dto.getToDate());
	}

	@Override
	public Page<MUser> findAllUserBasedOnBlock(PagingDTO dto) {

		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Page<MUser> min = twoMinBlockRepository.findAllBlockUser(pageable, dto.getFromDate(), dto.getToDate(), true,
				"ROLE_USER,ROLE_BLOCKED");
		return min;

	}

	@Override
	public Page<MUser> findUserBasedOnGroup(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		logger.info("username:: " + dto.getContact());
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByContact(dto.getContact());
		logger.info("grooup details:: " + gd.getGroupName());
		return mUserRespository.findAllUserBasedOnGroup(pageable, dto.getFromDate(), dto.getToDate(), gd);
	}

	@Override
	public ResponseDTO changeUserGroup(PagingDTO dto) {
		ResponseDTO res = new ResponseDTO();
		try {
			GroupDetails gd = groupDetailsRepository.getGroupDetailsByContact(dto.getContact());
			if (gd != null) {
				MUser user = mUserRespository.findByUsername(dto.getUserContactNo());
				if (user != null) {
					MUserDetails ud = user.getUserDetail();
					ud.setGroupStatus(Status.Inactive.getValue());
					ud.setChangedGroupName(gd.getGroupName());
					ud.setGroupChange(true);
					mUserDetailRepository.save(ud);
					res.setCode(ResponseStatus.SUCCESS.getValue());
					res.setMessage("User Change Group Request sent to group : " + gd.getGroupName());
				} else {
					res.setCode(ResponseStatus.FAILURE.getValue());
					res.setMessage("user does not exist");
				}
			} else {
				res.setCode(ResponseStatus.FAILURE.getValue());
				res.setMessage("Group does not exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public Page<MUser> findAllActiveUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllActiveUserList(pageable, dto.getFromDate(), dto.getToDate());

	}

	@Override
	public Page<MUser> findAllKYCUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mUserRespository.findAllKYCUserList(pageable, dto.getFromDate(), dto.getToDate());
	}

	@Override
	public Page<MKycDetail> findAllKycRequest(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findAllKYCUserList(pageable, AccountType);
	}

	@Override
	public Page<MKycDetail> findUpdatedKycList(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return mKycRepository.findUpdatedUserList(pageable, dto.getFromDate(), dto.getToDate(), false);
	}

	@Override
	public Page<MKycDetail> findAllKycRequestRejected(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findAllKYCUserListRejected(pageable, dto.getFromDate(), dto.getToDate(), AccountType);

	}

	@Override
	public Page<PhysicalCardDetails> findAllPhysicalCardRequestUser(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		return physicalCardDetailRepository.findAllPhysicalCardRequestUserList(pageable, dto.getFromDate(),
				dto.getToDate());

	}

	@Override
	public Page<PhysicalCardDetails> findAllPhysicalCardRequestOnStatusList(PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
		if (dto.getStatus().equalsIgnoreCase(Status.Requested + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Requested);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Shipped + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Shipped);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Declined + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Declined);
		} else if (dto.getStatus().equalsIgnoreCase(Status.Active + "")) {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Active);
		} else {
			return physicalCardDetailRepository.findPhysicalCardRequestList(pageable, dto.getFromDate(),
					dto.getToDate(), Status.Received);
		}
	}

	@Override
	public List<PhysicalCardDetails> findAllPhysicalCardRequest() {
		Sort sort = new Sort(Sort.Direction.DESC, "status");
		Pageable pageable = new PageRequest(0, 1000, sort);
		return (List<PhysicalCardDetails>) physicalCardDetailRepository.findAllPhysicalCardRequest(pageable);
	}

	@Override
	public MKycDetail findKYCREquestByUser(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccount(us, AccountType);
	}

	@Override
	public MKycDetail findKYCRequestByUserUpdated(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccountUpdated(us, AccountType, false);
	}

	@Override
	public MKycDetail findKYCRequestByUserUpdatedRejectedKyc(MUser us) {
		MPQAccountType AccountType = mPQAccountTypeRespository.findByCode("NONKYC");
		return mKycRepository.findByUserAndAccountUpdated(us, AccountType, true);
	}

	@Override
	public long gettotalPhysicalCard() {
		long val = 0;
		try {
			val = mmCardRepository.findTotalPhysicalCards();
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public long getTotalActiveUser() {
		long val = 0;
		try {
			val = mUserRespository.findAllActiveUser();
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public long getTotalLoadMoney() {
		long val = 0;
		try {
			MService service = mServiceRepository.findByName("Load Money");
			val = mTransactionRepository.getTotalLoadMoney(service);
		} catch (Exception e) {
		}
		return val;
	}

	@Override
	public ResponseDTO savePromoCode(PromoCodeDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		PromoCode promoCode = promoCodeRepository.fetchByPromoCode(dto.getPromoCode());
		MService service = mServiceRepository.findServiceByCode(dto.getServiceCode());
		if (promoCode == null) {
			try {
				Date sDate = formatter1.parse(dto.getStartDate());
				Date eDate = formatter1.parse(dto.getEndDate());
				PromoCode code = new PromoCode();
				code.setEndDate(sDate);
				code.setStartDate(eDate);
				code.setStatus(Status.Active);
				code.setPromoCode(dto.getPromoCode());
				code.setPercentage(dto.isPercentage());
				code.setMaxCashBack(Long.valueOf(dto.getMaxCashBack()));
				code.setMinTrxAmount(Long.valueOf(dto.getMinTrxAmount()));
				code.setService(service);
				if (dto.isPercentage()) {
					code.setPercentage(dto.getPercentageValue());
				} else {
					code.setAmount(dto.getAmount());
				}
				promoCodeRepository.save(code);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		resp.setCode(ResponseStatus.SUCCESS.getValue());
		return resp;
	}

	@Override
	public List<PromoCode> fetchPromoCodes() {
		return (List<PromoCode>) promoCodeRepository.findAllPromoCode();
	}

	@Override
	public List<MService> fetchServices() {
		return (List<MService>) mServiceRepository.findService();
	}

	@Override
	public List<PromoCodeRequest> fetchPromoCodeRequest() {
		return (List<PromoCodeRequest>) promoCodeRequestRepository.findAllRequest();
	}

	@Override
	public List<PromoCodeRequest> fetchPromoCodeRequestWithStatus() {
		return (List<PromoCodeRequest>) promoCodeRequestRepository.findPromoCodeWhichAreNotRedeemed();
	}

	@Override
	public List<VersionLogs> findAllVersion() {
		return (List<VersionLogs>) versionLogsRepository.findAllVersions();
	}

	@Override
	public ResponseDTO updateVersionStatus(String id, String status) {
		ResponseDTO resp = new ResponseDTO();
		try {
			long logId = Long.valueOf(id);
			VersionLogs log = versionLogsRepository.findById(logId);
			if ("Active".equalsIgnoreCase(status)) {
				log.setStatus("Inactive");
			} else {
				log.setStatus("Active");
			}
			log.setLastModified(new Date());
			versionLogsRepository.save(log);
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			resp.setMessage("Status updated succesfully.");
		} catch (Exception e) {
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Failed,Please try again.");
		}
		return resp;
	}

	@Override
	public ResponseDTO updatePhysicalCardStatus(String id, String status, String comment) {
		ResponseDTO resp = new ResponseDTO();
		try {
			long logId = Long.valueOf(id);
			PhysicalCardDetails card = physicalCardDetailRepository.findById(logId);
			if (card != null) {
				if (status.equalsIgnoreCase(Status.Requested + "")) {
					card.setStatus(Status.Requested);
				} else if (status.equalsIgnoreCase(Status.Shipped + "")) {
					card.setStatus(Status.Shipped);
				} else {
					card.setStatus(Status.Declined);
				}
				card.setComment(comment);
				card.setLastModified(new Date());
				physicalCardDetailRepository.save(card);
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setMessage("Status updated succesfully.");
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Failed,Please try again.");
			}

		} catch (Exception e) {
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Failed,Please try again.");
		}
		return resp;
	}

	@Override
	public boolean saveUserFromAdmin(RegisterDTO dto) {
		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());

			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public ResponseDTO Addversion(ApiVersionDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			ApiVersion apiVersion = apiVersionRepository.getByNameStatus(dto.getName(), "Active");
			if (apiVersion != null) {
				VersionLogs log1 = versionLogsRepository.findLogByVersion(dto.getVersion(), apiVersion);
				if (log1 == null) {
					VersionLogs log = versionLogsRepository.findPreviouLog(apiVersion);
					if (log != null) {
						log.setNewVersion(false);
						versionLogsRepository.save(log);
					}
					VersionLogs versionLogs = new VersionLogs();
					versionLogs.setAndroidVersion(apiVersion);
					versionLogs.setNewVersion(true);
					versionLogs.setStatus("Active");
					versionLogs.setVersion(dto.getVersion());
					versionLogsRepository.save(versionLogs);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("The version is added.");
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("The version log with this version is already present.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public List<MMCards> getListByDateCards(Date fromDate, Date toDate, boolean flag) {
		return mmCardRepository.getCardListByDate(fromDate, toDate, flag);
	}

	@Override
	public boolean setNewMpin(MpinDTO dto) {
		boolean updated = false;
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() == null) {
			String mpin = dto.getNewMpin();
			try {
				/*
				 * String hashedMpin = SecurityUtil.sha512(mpin);
				 * userDetailRepository.updateUserMPIN(hashedMpin, user.getUsername()); updated
				 * = true;
				 */
				String hashedMpin = passwordEncoder.encode(mpin);
				mUserDetailRepository.updateUserMPIN(hashedMpin, user.getUsername());
				updated = true;

			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public List<BulkRegister> findAllCorporateUser(String username) {
		MUser user = mUserRespository.findByUsername(username);
		CorporateAgentDetails adetail = corporateAgentDetailsRepository.getByCorporateId(user);
		return (List<BulkRegister>) bulkRegisterRepository.getRegisteredUsers(adetail);
	}

	@Override
	public boolean saveUserFromCorporate(RegisterDTO dto) {

		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setDateOfBirth(formatter1.parse(dto.getDateOfBirth()));
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setIdType(dto.getIdType());
			userDetail.setIdNo(dto.getIdNo());

			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);
			user.setCorporateUser(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public boolean saveCorporatePartner(RegisterDTO dto) {

		boolean value = false;
		try {
			MUser user = new MUser();
			MUserDetails userDetail = new MUserDetails();
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			user.setAuthority(Authorities.USER + "," + Authorities.CORPORATE_PARTNER);
			user.setPassword(passwordEncoder.encode(dto.getContactNo()));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getEmail().toLowerCase());
			user.setUserType(UserType.CorporatePartner);
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());
			user.setFullyFilled(true);

			user.setUserDetail(userDetail);
			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("KYC");
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			PartnerDetails partnerDetails = new PartnerDetails();
			partnerDetails.setPartnerEmail(dto.getEmail());
			partnerDetails.setPartnerMobile(dto.getContactNo());
			partnerDetails.setPartnerName(dto.getFirstName());
			partnerDetails.setPartnerServices(dto.getPartnerServices());
			partnerDetails.setCorporate(dto.getAgentDetails());
			partnerDetails.setLoadCardMaxLimit(dto.getMaxLoadCardLimit());
			partnerDetails.setStatus(Status.Active);

			MUser tempUser = mUserRespository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				mUserDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);
				mUserRespository.save(user);
				partnerDetails.setPartnerUser(user);
				partnerDetailsRepository.save(partnerDetails);
				value = true;
				return value;
			} else {
				mUserDetailRepository.delete(tempUser.getUserDetail());
				mPQAccountDetailRepository.delete(tempUser.getAccountDetail());
				mUserRespository.delete(tempUser);
				user.setAccountDetail(tempUser.getAccountDetail());
				mUserDetailRepository.save(userDetail);
				mUserRespository.save(user);
				partnerDetails.setPartnerUser(user);
				partnerDetailsRepository.save(partnerDetails);
				value = true;
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public boolean changeCurrentMpin(MPinChangeDTO dto) {
		boolean updated = false;
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() != null) {
			String mpin = dto.getNewMpin();
			try {
				mUserDetailRepository.updateUserMPIN(passwordEncoder.encode(mpin), user.getUsername());
				updated = true;
			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public String handleMpinFailure(HttpServletRequest request, String loginUsername) {
		if (loginUsername != null) {
			MUser user = mUserRespository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					MpinLogs mpinLog = new MpinLogs();
					mpinLog.setUser(user);
					mpinLog.setServerIpAddress(request.getRemoteAddr());
					mpinLog.setStatus(Status.Failed);
					mPinLogsRepository.save(mpinLog);
					List<MpinLogs> llsFailed = mPinLogsRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getMpinAttempts() - failedCount;
					if (failedCount == getMpinAttempts()) {
						if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect mpin. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public boolean deleteCurrentMpin(String username) {
		boolean deleted = false;
		MUser user = mUserRespository.findByUsername(username);
		if (user != null && user.getUserDetail().getMpin() != null) {
			mUserDetailRepository.deleteUserMPIN(username);
			deleted = true;
		}
		return deleted;
	}

	@Override
	public boolean changePasswordCorporate(MatchMoveCreateCardRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			mUserRespository.save(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean changePasswordAdmin(MatchMoveCreateCardRequest dto) {
		MUser user = mUserRespository.findByUsername(dto.getUsername());
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			mUserRespository.save(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ResponseDTO updateKycRequestCorp(UpgradeAccountDTO dto) {
		ResponseDTO response = new ResponseDTO();
		try {
			long id = Long.valueOf(dto.getId());
			MKycDetail kyc = mKycRepository.findById(id);
			UserKycRequest requ = new UserKycRequest();
			requ.setAddress1(dto.getAddress1());
			requ.setAddress2(dto.getAddress2());
			requ.setCity(dto.getCity());
			requ.setState(dto.getState());
			requ.setCountry(dto.getCountry());
			requ.setPinCode(dto.getPincode());
			System.err.println(dto.getIdType());
			System.err.println(dto.getIdNumber());
			requ.setUser(kyc.getUser());
			requ.setId_type(kyc.getIdType());
			requ.setId_number(kyc.getIdNumber());
			requ.setFile(dto.getImage());
			requ.setFilePath(kyc.getIdImage());
			UserKycResponse resp = matchMoveApi.kycUserMM(requ);
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
				resp = matchMoveApi.setIdDetails(requ);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
					// resp=matchMoveApi.setIdDetails(requ);

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
						resp = matchMoveApi.setImagesForKyc(requ);
						if (kyc.getIdImage1() != null) {
							requ.setFilePath(kyc.getIdImage1());
							resp = matchMoveApi.setImagesForKyc(requ);

						}
						if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							matchMoveApi.tempConfirmKyc(kyc.getUser());
							UserKycResponse approved = matchMoveApi.tempKycStatusApproval(
									kyc.getUser());
							if (approved.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								MPQAccountType KYCAccountType = mPQAccountTypeRespository.findByCode("KYC");

								kyc.setAccountType(KYCAccountType);
								mKycRepository.save(kyc);
								MPQAccountDetails account = kyc.getUser().getAccountDetail();
								account.setAccountType(KYCAccountType);
								mPQAccountDetailRepository.save(account);
								smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.KYCREQUEST_SUCCESS,
										kyc.getUser(), String.valueOf(KYCAccountType.getMonthlyLimit()));
								response.setCode(ResponseStatus.SUCCESS.getValue());
								return response;

							}
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getValue());
			return response;
		}
		return response;
	}

	@Override
	public ResponseDTO upgradeAccountCorp(UpgradeAccountDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
			MUser user = mUserRespository.findByUsername(dto.getContactNo());
			MKycDetail detail = mKycRepository.findByUser(user);
			if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("KYC")) {

			} else if (detail != null && detail.getAccountType().getCode().equalsIgnoreCase("NONKYC")
					&& dto.getImage2() != null) {
				detail.setIdImage(dto.getImagePath());
				detail.setIdImage1(dto.getImagePath2());
				if (detail.isRejectionStatus() == true) {
					detail.setRejectionStatus(false);
				}
				mKycRepository.save(detail);
			}

			else {
				MKycDetail check = mKycRepository.findByUser(user);
				if (check == null) {
					MKycDetail kyc = new MKycDetail();
					if (dto.getIdType().trim().equalsIgnoreCase("AADHAAR")) {
						dto.setIdType("aadhaar");
					}
					if (dto.getIdType().trim().equalsIgnoreCase("DRIVING LICENSE")) {
						dto.setIdType("drivers_id");
					}
					if (dto.getIdType().trim().equalsIgnoreCase("PASSPORT")) {
						dto.setIdType("passport");
					}

					kyc.setUserType(dto.getUserType());
					kyc.setIdType(dto.getIdType());
					kyc.setIdNumber(dto.getIdNumber());
					kyc.setAddress1(dto.getAddress1());
					kyc.setAddress2(dto.getAddres2());
					kyc.setCity(dto.getCity());
					kyc.setState(dto.getState());
					kyc.setPinCode(dto.getPincode());
					kyc.setCountry(dto.getCountry());
					kyc.setIdImage(dto.getImagePath());
					if (dto.getImage2() != null) {
						kyc.setIdImage1(dto.getImagePath2());
					}
					kyc.setAccountType(nonKYCAccountType);
					kyc.setUser(user);
					mKycRepository.save(kyc);
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					return resp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			return resp;
		}
		return resp;
	}

	@Override
	public List<GroupDetails> getGroupDetails() {
		List<GroupDetails> list = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupName(pageable);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<MUser> groupAddRequestByContactGet(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getContactNo());

			if (dto.getContactNo() == null) {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContactAdminNoDate(pageable,
						Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			} else {
				Page<MUser> user = mUserRespository.requestUserGroup(pageable, dto.getContactNo(),
						Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> addGroupRequestGet(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getContactNo());

			Page<MUser> user = mUserRespository.requestUserGroup(pageable, dto.getContactNo(),
					Status.Inactive.getValue(), true);
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> addGroupRequestpost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getContactNo());

			Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContact(pageable, dto.getFrom(), dto.getTo(),
					dto.getContactNo(), Status.Inactive.getValue(), true);
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public long getCountGroupRequest(String contact) {
		long count = mUserRespository.findAllUserByGroupStatusByCount(Status.Inactive.getValue(), contact, true);
		return count;
	}

	@Override
	public long getCountGroupAccept(String contact) {
		long acceptedCount = mUserRespository.findAllUserByGroupStatusCount(Status.Active.getValue(), contact);
		return acceptedCount;
	}

	@Override
	public long getCountGroupReject(String contact) {
		long rejectedCount = mUserRespository.findAllUserByCountReject(Status.Cancelled.getValue(), contact);
		return rejectedCount;
	}

	@Override
	public List<MUser> getRequestUser(String contact) {
		List<MUser> requestSingleUser = mUserRespository.getSingleRequest(Status.Inactive.getValue(), contact);
		return requestSingleUser;
	}

	@Override
	public List<MUser> getAcceptedUser(String contact) {
		List<MUser> acceptedSingleUser = mUserRespository.getSingleRequest(Status.Active.getValue(), contact);
		return acceptedSingleUser;
	}

	@Override
	public List<MUser> getRejectedUser(String contact) {
		List<MUser> rejectedSingleUser = mUserRespository.getSingleRequest(Status.Cancelled.getValue(), contact);
		return rejectedSingleUser;
	}

	@Override
	public List<MUser> groupAddRequestByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from:== " + dto.getFrom());
			logger.info("to :== " + dto.getTo());
			logger.info("status::== " + Status.Inactive.getValue());
			logger.info("contact::== " + dto.getContactNo());

			if (dto.getContactNo() == null) {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContactAdmin(pageable, dto.getFrom(),
						dto.getTo(), Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			} else {
				Page<MUser> user = mUserRespository.findAllUserByGroupStatusByContact(pageable, dto.getFrom(),
						dto.getTo(), dto.getContactNo(), Status.Inactive.getValue(), true);
				if (user != null) {
					lUserDTO = user.getContent();
				} else {
					throw new Exception("session is null");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> getSingleUserGroupRequest(String contact) {
		List<MUser> userList = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
			Page<MUser> user = mUserRespository.getListUserbyContact(pageable, contact);
			if (user != null) {
				userList = user.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public LoginResponse loginGroup(LoginDTO login, HttpServletRequest request, HttpServletResponse response) {
		LoginResponse result = new LoginResponse();
		try {
			LoginError error = checkLoginValidation(login);
			if (error.isSuccess()) {
				try {
					MUser user = findByUserName(login.getUsername());
					if (user.getAuthority().contains(Authorities.GROUP)
							|| user.getAuthority().contains(Authorities.SUPER_ADMIN)
							|| user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
						AuthenticationError auth = authentication(login, request);
						if (auth.isSuccess()) {
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sls.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Login successful.");
							result.setSuccess(true);
							result.setSessionId(userSession.getSessionId());
							result.setContactNo(userSession.getUser().getGroupDetails().getEmail());
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage(auth.getMessage());
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
						result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						result.setMessage("Unauthorized Role");
						result.setSuccess(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(error.getMessage());
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public LoginError checkLoginValidation(LoginDTO login) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getUsername())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Username");
		} else if (CommonValidation.isNull(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Password");
		} else if (CommonValidation.isNull(login.getIpAddress())) {
			loginError.setMessage("Not a valid device");
			loginError.setSuccess(false);
		}
		return loginError;
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

				/* for session 30 min active */

				session.setMaxInactiveInterval(30 * 60);

				/* for session 30 min active */

				error.setSuccess(true);
				error.setMessage("Login successful.");
				handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()), dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(
					handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()), dto.getIpAddress()));
			return error;
		}
	}

	@Override
	public CommonResponse createGroup(GroupDetailDTO dto, CommonResponse result) {
		String email = dto.getEmailAddressId();
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByEmail(email);
		if (gd == null) {
			String contactNo = dto.getMobileNoId();
			gd = groupDetailsRepository.getGroupDetailsByContact(contactNo);
			if (gd == null) {
				result = savegroup(dto, result);
				logger.info("contact no:: " + contactNo);
				gd = groupDetailsRepository.getGroupDetailsByContact(contactNo);

				List<GroupOtpMobile> groupOtp = groupOtpRepository.getGroupDetails(contactNo);
				if (groupOtp.isEmpty() || groupOtp == null) {
					GroupOtpMobile gotp = new GroupOtpMobile();
					gotp.setContactName(gd.getGroupName());
					gotp.setGroupName(gd.getDname());
					gotp.setMobileNumber(gd.getContactNo());
					groupOtpRepository.save(gotp);
				}

				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Group Added Successfully.");
				result.setSuccess(true);
				smsSenderApi.sendGroupCreateSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.CREATED_GROUP, gd,
						dto.getMobileNoId());
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Contact no already exist.");
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Email id already exist.");
			result.setSuccess(false);
		}
		return result;
	}

	private CommonResponse savegroup(GroupDetailDTO dto, CommonResponse result) {
		try {

			MUser u = mUserRespository.findByUsernameAndAuthority(dto.getEmailAddressId());
			if (u == null) {
				GroupDetails gd = new GroupDetails();
				byte[] byteArr = null;

				gd.setAddress(dto.getAddress());
				gd.setEmail(dto.getEmailAddressId());
				gd.setCity(dto.getCity());
				gd.setContactNo(dto.getMobileNoId());
				gd.setCountry(dto.getCountry());
				gd.setState(dto.getState());
				gd.setGroupName(dto.getName());
				gd.setDname(dto.getDname());
				gd.setStatus(Status.Active.getValue());
				try {
					byteArr = Base64.getDecoder().decode(Base64.getEncoder().encodeToString(dto.getImage().getBytes()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				gd.setImageContent(byteArr);
				gd.setImage(dto.getImage().getContentType());
				groupDetailsRepository.save(gd);

				u = new MUser();
				u.setUserType(UserType.Group);
				u.setUsername(dto.getEmailAddressId());
				u.setMobileStatus(Status.Inactive);
				u.setAuthority(Authorities.GROUP + "," + Authorities.AUTHENTICATED);
				u.setGroupDetails(gd);
				u.setEmailStatus(Status.Active);
				u.setMobileStatus(Status.Active);
				u.setPassword(passwordEncoder.encode(dto.getMobileNoId()));

				MUserDetails ud = new MUserDetails();
				ud.setContactNo(dto.getMobileNoId());
				ud.setEmail(dto.getEmailAddressId());
				ud.setCountryName(dto.getCountry());
				ud.setFirstName(dto.getName());
				ud.setLastName(dto.getName());
				ud.setGroupStatus(Status.Closed.getValue());
				u.setUserDetail(ud);
				mUserDetailRepository.save(ud);
				mUserRespository.save(u);

				MPQAccountType nonKYCAccountType = mPQAccountTypeRespository.findByCode("NONKYC");
				MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
				pqAccountDetail.setBalance(0);
				pqAccountDetail.setAccountType(nonKYCAccountType);
				pqAccountDetail.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER + ud.getId());
				mPQAccountDetailRepository.save(pqAccountDetail);

				u.setAccountDetail(pqAccountDetail);
				mUserRespository.save(u);

				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Group details added successfully.");
				result.setSuccess(true);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Group not added.");
				result.setSuccess(false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<MUser> groupAddRequest(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.getAllRequestListGroup(pageable, Status.Inactive.getValue(), true);
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<GroupDetails> getGroupDetailsAdmin() {
		List<GroupDetails> list = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupNameAdmin(pageable);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Donation> getDonationDetails() {
		List<Donation> list = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetails(pageable);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public CommonResponse acceptGroupAddRequest(String contactNo, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(contactNo);
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				GroupDetails g = u.getGroupDetails();
				if (detail.getGroupStatus().equals(Status.Inactive.getValue())) {
					detail.setGroupStatus(Status.Active.getValue());
					mUserDetailRepository.save(detail);
					if (detail.isGroupChange() == true) {
						GroupDetails gr = groupDetailsRepository.getGroupDetailsByName(detail.getChangedGroupName());
						if (gr != null) {
							detail.setGroupChange(false);
							mUserDetailRepository.save(detail);
							u.setGroupDetails(gr);
							mUserRespository.save(u);
						} else {
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Group does not exist");
						}
					}
					smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_ADD, u, null);
					smsSenderApi.sendGroupSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_MESSAGE, g, u, null);
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setSuccess(true);
					result.setMessage("User added to Group Successfully");
					result.setStatus(ResponseStatus.SUCCESS.getKey());
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setSuccess(false);
					result.setMessage("User Already in Group");
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setSuccess(false);
				result.setMessage("User Not added to Group");
				result.setStatus(ResponseStatus.FAILURE.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public CommonResponse rejectGroupRequest(PagingDTO dto, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(dto.getUsername());
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				GroupDetails g = u.getGroupDetails();
				if (detail.getGroupStatus().equals(Status.Inactive.getValue())) {
					detail.setGroupStatus(Status.Cancelled.getValue());
					detail.setPreviousGroupContact(g.getContactNo());
					detail.setGroupRejectReason(dto.getStatus());
					mUserDetailRepository.save(detail);

					smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.REJECTED_GROUP, u, null);
					smsSenderApi.sendGroupSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.GROUP_MESSAGE_REJECTED, g, u, null);

					GroupDetails gr = groupDetailsRepository.getGroupDetailsByName("None");
					u.setGroupDetails(gr);
					mUserRespository.save(u);
					if (detail.isGroupChange() == true) {
						detail.setGroupChange(false);
						mUserDetailRepository.save(detail);
					}

					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setMessage("User is Rejected From Group");
					result.setSuccess(true);
				} else if (detail.getGroupStatus().equals(Status.Active.getValue())) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User Already in Group");
					result.setSuccess(false);
				} else if (detail.getGroupStatus().equals(Status.Cancelled.getValue())) {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User is Rejected");
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public CommonResponse deleteUserFromGroup(PagingDTO page, CommonResponse result) {
		try {
			MUser u = mUserRespository.findByUsername(page.getUsername());
			if (u != null) {
				MUserDetails detail = u.getUserDetail();
				if (detail.getGroupStatus().equalsIgnoreCase(Status.Active.getValue())) {
					detail.setGroupStatus(Status.Deleted.getValue());
					detail.setDeletionGroupReason(page.getStatus());
					mUserDetailRepository.save(detail);
					GroupDetails group = groupDetailsRepository.getGroupDetailsByName("None");
					if (group != null) {
						u.setGroupDetails(group);
						mUserRespository.save(u);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setMessage("User is Deleted From Group");
						result.setSuccess(true);
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage("Group does not exist");
						result.setSuccess(false);
					}
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage("User is Inactive");
					result.setSuccess(false);
				}
			} else {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("User Does not exist");
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// group sms
	@Override
	public void sendGroupSMS(String groupName, String text) {
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName(groupName);
		List<MUser> mobileList = mUserRespository.getGroupUsersSms(gd);
		for (MUser mobile : mobileList) {
			GroupSms g = new GroupSms();
			g.setCronStatus(false);
			g.setGroupName(gd.getGroupName());
			g.setMobile(mobile.getUserDetail().getContactNo());
			g.setSingle(false);
			g.setStatus(Status.Inactive.getValue());
			g.setText(text);
			groupSmsRepository.save(g);
		}
	}

	@Override
	public GroupDetails getGroupDetails(String groupName) {
		GroupDetails gd = groupDetailsRepository.getGroupDetailsByName(groupName);
		if (gd != null) {
			return gd;
		} else {
			GroupDetails gr = groupDetailsRepository.getGroupDetailsByName("None");
			return gr;
		}
	}

	// send single sms
	@Override
	public void sendSingleGroupSMS(String mobile, String text) {
		smsSenderApi.sendUserGroupSms(text, mobile);
	}

	// bulk sms
	@Override
	public void sendBulkSMS() {
		List<String> mobileList = mUserRespository.getAllActiveUsername(UserType.User);
		for (String mobile : mobileList) {
			sendSingleSMS(mobile);
		}
	}

	@Override
	public void sendSingleSMS(String mobile) {
		MUser user = findByUserName(mobile);
		if (user != null) {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.BULK_SMS, user, null);
		}
	}

	@Override
	public List<MUser> groupAcceptedUserByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusByContact(pageable, dto.getContactNo(),
					Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> acceptedUserByContactPostGroup(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusByContactPost(pageable, dto.getFrom(), dto.getTo(),
					dto.getContactNo(), Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserByContact(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());
			logger.info("contactno :: " + dto.getContactNo());
			Page<MUser> user = mUserRespository.rejectedGroupByContactStatusGet(pageable, dto.getContactNo(),
					Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
				for (int i = 0; i < lUserDTO.size(); i++) {
					System.err.println("id: " + lUserDTO.get(i).getUsername());
				}
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserByContactPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupByContactStatus(pageable, dto.getFrom(), dto.getTo(),
					dto.getContactNo(), Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	// Admin

	@Override
	public List<MUser> groupAcceptedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatusGet(pageable, Status.Active.getValue());
			if (user != null) {

				lUserDTO = user.getContent();
				for (int i = 0; i < lUserDTO.size(); i++) {
					System.err.println("accepted user :: " + lUserDTO.get(i).getUsername());
				}
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupAcceptedUserPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.acceptedGroupStatus(pageable, dto.getFrom(), dto.getTo(),
					Status.Active.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupStatusGet(pageable, Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> groupRejectedUserPost(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			logger.info("status:: " + Status.Inactive.getValue());

			Page<MUser> user = mUserRespository.rejectedGroupStatus(pageable, dto.getFrom(), dto.getTo(),
					Status.Cancelled.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<MUser> getDeletedUser(UserDataRequestDTO dto) {
		List<MUser> lUserDTO = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			logger.info("date from: " + dto.getFrom());
			logger.info("to : " + dto.getTo());
			Page<MUser> user = mUserRespository.findAllUserByGroupStatusDeleted(pageable, dto.getFrom(), dto.getTo(),
					Status.Deleted.getValue());
			if (user != null) {
				lUserDTO = user.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public ResponseDTO saveDonatee(GroupDetailDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("contact :: " + dto.getMobileNoId());
			Donation donate = donationRepository.getDonation(dto.getMobileNoId());
			if (donate == null) {
				donate = new Donation();
				donate.setContactNo(dto.getMobileNoId());
				donate.setEmail(dto.getEmailAddressId());
				donate.setDonateeName(dto.getName());
				donate.setAccount(dto.getAccount());
				donate.setOrganization(dto.getOrganization());
				donate.setMessage(dto.getRemark());
				donationRepository.save(donate);

				result.setSuccess(true);
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setMessage(ErrorMessage.DONATEE);

				smsSenderApi.sendUserSMSDonation(SMSAccount.PAYQWIK_OTP, SMSTemplete.DONATION_ADD, dto.getMobileNoId());
			} else {
				result.setSuccess(false);
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage(ErrorMessage.DONATEE_EXIST);
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<GroupDetails> getGroupListGet(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListGet(pageable);
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<GroupDetails> getGroupListPost(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListPost(pageable, dto.getFromDate(),
					dto.getToDate());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<GroupDetails> getSingleGroupList(PagingDTO dto) {
		List<GroupDetails> lUserDTO = new ArrayList<GroupDetails>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupDetails> page = groupDetailsRepository.getGroupListPost(pageable, dto.getUserContactNo());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<Donation> addDonationList(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDate(pageable);
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public List<Donation> addDonationListPost(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDatePost(pageable, dto.getFrom(), dto.getTo());
			if (page != null) {
				lUserDTO = page.getContent();
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<DonationDTO> donationListBasedOnGroup(UserDataRequestDTO dto) {
		List<Donation> lUserDTO = new ArrayList<Donation>();
		List<DonationDTO> list = new ArrayList<DonationDTO>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<Donation> page = donationRepository.getDonationDetailsDatePost(pageable, dto.getFrom(), dto.getTo());
			if (page != null) {
				lUserDTO = page.getContent();
				for (int i = 0; i < lUserDTO.size(); i++) {
					DonationDTO userList = new DonationDTO();
					MUser u = mUserRespository.getUserBasedOnUserNameAndAuthority(lUserDTO.get(i).getContactNo(),
							Status.Active);
					if (u != null) {
						userList.setDonateeName(lUserDTO.get(i).getDonateeName());
						userList.setContactNo(lUserDTO.get(i).getContactNo());
						userList.setGroupName(u.getGroupDetails().getGroupName());
						userList.setOrganization(lUserDTO.get(i).getOrganization());
						userList.setEmail(lUserDTO.get(i).getEmail());
						userList.setCreated(lUserDTO.get(i).getCreated().toLocaleString());
						list.add(userList);
					} else {
						continue;
					}
				}
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public boolean resendMobileTokenDeviceBindingGroup(String username) {

		String otp = CommonUtil.generateSixDigitNumericString();

		MUser u = mUserRespository.getUserBasedOnUserNameAndAuthorityGroup(username, Status.Active);
		if (u == null) {
			return false;
		} else {

			// List<String> mobileNumbers =
			// groupOtpRepository.getMobileGroup(u.getGroupDetails().getDname());
			u.setMobileToken(otp);
			mUserRespository.save(u);
			// for(int i = 0; i < mobileNumbers.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP,
					u.getGroupDetails().getContactNo(), otp);
			// }
			return true;

		}
	}

	@Override
	public boolean resendMobileTokenDeviceBinding(String username) {
		List<String> users = otpRepository.getMobileNumbers();
		String otp = CommonUtil.generateSixDigitNumericString();
		for (int i = 0; i < users.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP, users.get(i), otp);
		}
		MUser u = mUserRespository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(otp);
			mUserRespository.save(u);
			return true;
		}
	}

	@Override
	public ResponseDTO loadCardOtp() {
		ResponseDTO result = new ResponseDTO();
		List<LoadCardOtp> users = loadCardOtpRepository.getLoadCardList();
		String otp = CommonUtil.generateSixDigitNumericString();
		for (int i = 0; i < users.size(); i++) {
			smsSenderApi.sendUserSMSForAdmin(SMSAccount.PAYQWIK_OTP, SMSTemplete.REGENERATE_OTP,
					users.get(i).getMobile(), otp);
			users.get(i).setOtp(otp);
			loadCardOtpRepository.save(users);
		}
		result.setCode(ResponseStatus.SUCCESS.getValue());
		result.setMessage("OTP sent");
		result.setStatus(otp);
		return result;
	}

	@Override
	public String inactivePartener(MUser partner) {
		if (partner != null) {
			PartnerDetails details = partnerDetailsRepository.getPartnerDetails(partner);
			if (details != null) {
				details.setStatus(Status.Inactive);
				partner.setMobileStatus(Status.Inactive);
				partnerDetailsRepository.save(details);
				mUserRespository.save(partner);
				return ResponseStatus.SUCCESS.getKey();
			}
		}
		return null;
	}

	@Override
	public MUser getPartnerById(long parseLong) {
		return mUserRespository.findOne(parseLong);
	}

	@Override
	public List<MPQAccountDetails> getUserAccounts(PartnerDetails partner) {
		if (partner != null) {
			List<BulkRegister> register = bulkRegisterRepository.getPartnerUsers(partner);
			if (register != null) {
				List<MPQAccountDetails> accounts = new ArrayList<>();
				for (BulkRegister bulkRegister : register) {
					accounts.add(bulkRegister.getUser().getAccountDetail());
				}
				return accounts;
			}
		}
		return null;
	}

	@Override
	public PartnerDetails getPartnerDetails(long parseLong) {
		return partnerDetailsRepository.getPartnerById(parseLong, Status.Active);
	}

	@Override
	public List<BusTicket> getBusDetailsForAdminByDate(Date from, Date to) {
		List<BusTicket> lUserDTO = new ArrayList<BusTicket>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
			Page<BusTicket> page = busTicketRepository.getBusTicketByDate(pageable, from, to);
			if (page != null) {
				lUserDTO = page.getContent();
				logger.info("date from :: " + from);
				logger.info("date to:: " + to);
			} else {
				throw new Exception("session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lUserDTO;
	}

	@Override
	public MUser saveCorpUser(RegisterDTO registerDTO) {
		try {
			MUser user = mUserRespository.findByUsername(registerDTO.getContactNo());
			user = user != null ? user : new MUser();
			MUserDetails userDetail = user.getUserDetail() != null ? user.getUserDetail() : new MUserDetails();
			MPQAccountDetails pqAccountDetail = user.getAccountDetail() != null ? user.getAccountDetail()
					: new MPQAccountDetails();

			userDetail.setAddress(registerDTO.getAddress());
			userDetail.setContactNo(registerDTO.getContactNo());
			userDetail.setFirstName(registerDTO.getFirstName());
			userDetail.setLastName(registerDTO.getLastName());
			userDetail.setEmail(registerDTO.getEmail());
			userDetail.setIdType(registerDTO.getIdType());
			userDetail.setIdNo(registerDTO.getIdNo());
			userDetail.setGender(registerDTO.getGender());

			try {
				userDetail.setDateOfBirth(CommonUtil.DATEFORMATTER.parse(registerDTO.getDateOfBirth()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			user.setCorporateUser(true);
			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(registerDTO.getUsername());
			user.setUserType(UserType.User);
			user.setFullyFilled(true);
			String hashedPassword = passwordEncoder.encode("123456");
			user.setPassword(hashedPassword);

			userDetail = mUserDetailRepository.save(userDetail);

			pqAccountDetail.setBalance(pqAccountDetail.getBalance());
			pqAccountDetail.setBranchCode(pqAccountDetail.getBranchCode());
			pqAccountDetail.setAccountType(mPQAccountTypeRespository.findByCode("NONKYC"));
			pqAccountDetail
					.setAccountNumber(pqAccountDetail.getAccountNumber() != 0 ? pqAccountDetail.getAccountNumber()
							: SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());

			mPQAccountDetailRepository.save(pqAccountDetail);

			user.setUserDetail(userDetail);
			user.setAccountDetail(pqAccountDetail);
			mUserRespository.save(user);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<MUser> changeRequestListGet(UserDataRequestDTO dto) {
			List<MUser> list = new ArrayList<MUser>();
			try {
				Sort sort = new Sort(Sort.Direction.DESC, "id");
				Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

				Page<MUser> page = mUserRespository.getChangeList(pageable, dto.getEmail(), true, dto.getFrom(),
						dto.getTo());
				if (page != null) {
					list = page.getContent();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list;
}
	
	@Override
	public List<GroupBulkRegister> getsingleuserfailbulkreg(String contact, String email) {
		List<GroupBulkRegister> list = groupBulkRegisterRepository.getsingleuserfail(false, contact, email);
		return list;
	}
	
	@Override
	public List<MUser> changeRequestListPost(UserDataRequestDTO dto) {
		List<MUser> list = new ArrayList<MUser>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<MUser> page = mUserRespository.getChangeListPost(pageable, dto.getFrom(), dto.getTo(), dto.getEmail(),
					true);
			if (page != null) {
				list = page.getContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<GroupBulkRegister> getgroupBulkRegisterFailList(String email) {
		List<GroupBulkRegister> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkRegister> page = groupBulkRegisterRepository.getBulkRegisterFailList(false, email, pageable);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public List<GroupBulkRegister> getgroupBulkRegisterFailListPost(UserDataRequestDTO dto) {
		List<GroupBulkRegister> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
			Page<GroupBulkRegister> page = groupBulkRegisterRepository.getBulkRegisterFailListPost(pageable,
					dto.getFrom(), dto.getTo(), false, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public List<BulkCardAssignmentGroup> getgroupBulkCarFailList(String email) {
		List<BulkCardAssignmentGroup> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<BulkCardAssignmentGroup> page = bulkCardAssignmentGroupRepository.getBulkCardFailList(pageable,
					Status.Failed, email);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public List<GroupBulkKyc> getgroupBulkKycFailList(String email) {
		List<GroupBulkKyc> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkKyc> page = groupBulkKycRepository.getBulkKycFailList(pageable, false, email);
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public List<GroupBulkKyc> getgroupBulkLKycFailListPost(UserDataRequestDTO dto) {
		List<GroupBulkKyc> list = new ArrayList<>();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);

			Page<GroupBulkKyc> page = groupBulkKycRepository.getBulkKycFailListPost(pageable, dto.getFrom(),
					dto.getTo(), false, dto.getEmail());
			if (page != null) {
				list = page.getContent();
			} else {
				throw new Exception("Session is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public MUser saveGroupUser(RegisterDTO dto) {
			try {
				MUser user = mUserRespository.findByUsername(dto.getContactNo());
				user = user != null ? user : new MUser();
				MUserDetails userDetail = user.getUserDetail() != null ? user.getUserDetail() : new MUserDetails();
				MPQAccountDetails pqAccountDetail = user.getAccountDetail() != null ? user.getAccountDetail()
						: new MPQAccountDetails();

				userDetail.setAddress(dto.getAddress());
				userDetail.setContactNo(dto.getContactNo());
				userDetail.setFirstName(dto.getFirstName());
				userDetail.setLastName(dto.getLastName());
				userDetail.setEmail(dto.getEmail());
				userDetail.setIdType(dto.getIdType());
				userDetail.setIdNo(dto.getIdNo());
				userDetail.setGender(dto.getGender());
				userDetail.setMiddleName(dto.getMiddleName());
				userDetail.setGroupStatus(Status.Active.getValue());
				userDetail.setRemark(dto.getRemark());

				try {
					userDetail.setDateOfBirth(CommonUtil.DATEFORMATTER.parse(dto.getDateOfBirth()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				GroupDetails group = groupDetailsRepository.getGroupDetailsByEmail(dto.getGroup());
				if (group != null) {
					user.setGroupDetails(group);

				} else {
					GroupDetails g = groupDetailsRepository.getGroupDetailsByContact("None");
					user.setGroupDetails(g);
				}

				user.setCorporateUser(false);
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername());
				user.setUserType(UserType.User);
				user.setFullyFilled(true);
				String hashedPassword = passwordEncoder.encode("123456");
				user.setPassword(hashedPassword);

				userDetail = mUserDetailRepository.save(userDetail);

				pqAccountDetail.setBalance(pqAccountDetail.getBalance());
				pqAccountDetail.setBranchCode(pqAccountDetail.getBranchCode());
				pqAccountDetail.setAccountType(mPQAccountTypeRespository.findByCode(StartUpUtil.NON_KYC));
				pqAccountDetail
						.setAccountNumber(pqAccountDetail.getAccountNumber() != 0 ? pqAccountDetail.getAccountNumber()
								: SMSUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());

				mPQAccountDetailRepository.save(pqAccountDetail);

				user.setUserDetail(userDetail);
				user.setAccountDetail(pqAccountDetail);
				mUserRespository.save(user);

				return user;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}
	
}