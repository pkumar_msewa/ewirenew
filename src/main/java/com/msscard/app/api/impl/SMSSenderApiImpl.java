package com.msscard.app.api.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.model.request.SMSRequest;
import com.msscard.app.model.request.TicketDetailsDTO;
import com.msscard.app.model.response.SMSExecution;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSUtil;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MessageLog;
import com.msscard.repositories.MessageLogRepository;
import com.msscard.sms.api.ISMSApi;

public class SMSSenderApiImpl implements ISMSSenderApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private VelocityEngine velocityEngine;
	private MessageLogRepository messageLogRepository;
	private ISMSApi iSMSApi;
	@SuppressWarnings("unused")
	private MessageSource messageSource;

	public SMSSenderApiImpl(VelocityEngine velocityEngine, MessageLogRepository messageLogRepository, ISMSApi iSMSApi,
			MessageSource messageSource) {
		super();
		this.velocityEngine = velocityEngine;
		this.messageLogRepository = messageLogRepository;
		this.iSMSApi = iSMSApi;
		this.messageSource = messageSource;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void sendUserSMS(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		// iSMSApi.sendSMS(dto);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void sendUserSMSTopup(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			String amount) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("amount", amount);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		// iSMSApi.sendSMS(dto);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void sendUserSMSFund(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			MUser receipient) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("receiver", receipient.getUserDetail().getContactNo());
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		// iSMSApi.sendSMS(dto);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@Override
	public void sendUserGroupSms(String template, String user) {
		try {
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(user);
			logger.info("smsmessgae: " + template);
			smsRequest.setMessage(template);
			iSMSApi.sendSMS(smsRequest);
			saveLogNew(user, template, template, template, SMSAccount.PAYQWIK_OTP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendUserSMSDonation(SMSAccount smsAccount, String smsTemplate, String additionalInfo) {
		Map model = new HashMap();
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);

		MUser user = new MUser();
		user.setUsername(additionalInfo);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendUserSMSForAdmin(SMSAccount smsAccount, String smsTemplate, String additionalInfo, String otp) {
		Map model = new HashMap();
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		model.put("otp", otp);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(additionalInfo);
		MUser m = new MUser();
		m.setUsername(additionalInfo);
		sendSMSAdmin(smsAccount, m, smsMessage, smsTemplate);
	}

	private void sendSMSAdmin(final SMSAccount smsAccount, final MUser destination, final String smsMessage,
			final String smsTemplate) {
		try {
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(destination.getUsername());
			logger.info("smsmessgae: " + smsMessage);
			smsRequest.setMessage(smsMessage);
			iSMSApi.sendSMS(smsRequest);
			saveLog(destination, smsTemplate, smsMessage, smsMessage, SMSAccount.PAYQWIK_OTP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendGroupCreateSMS(SMSAccount smsAccount, String smsTemplate, GroupDetails gd, String additionalInfo) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", gd);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(gd.getContactNo());
		MUser m = new MUser();
		m.setUsername(gd.getContactNo());
		m.setGcmId(smsMessage);
		sendSMSGroup(smsAccount, m, smsTemplate, smsMessage);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendTransactionSMS(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			MTransaction transaction) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		model.put("transaction", transaction);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		// iSMSApi.sendSMS(dto);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	private void sendSMSGroup(final SMSAccount smsAccount, final MUser destination, final String smsMessage,
			final String smsTemplate) {
		try {
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(destination.getUsername());
			smsRequest.setMessage(destination.getGcmId());
			iSMSApi.sendSMS(smsRequest);
			saveLog(destination, smsTemplate, smsMessage, smsMessage, SMSAccount.PAYQWIK_OTP);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void sendSMS(final SMSAccount smsAccount, final MUser destination, final String smsMessage,
			final String smsTemplate) {
		try {
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(destination.getUsername());
			logger.info("smsmessgae: " + smsMessage);
			smsRequest.setMessage(smsMessage);
			iSMSApi.sendSMS(smsRequest);
			saveLog(destination, smsTemplate, smsMessage, smsMessage, SMSAccount.PAYQWIK_OTP);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Override
	public void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, MUser user, MTransaction transaction,
			TicketDetailsDTO additionalInfo) {

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);

	}

	protected void saveLog(MUser destination, String smsTemplate, String smsMessage, String smsResponse,
			SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(destination.getUsername());
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		mgslog.setSender(smsAccount.getUsername());
		messageLogRepository.save(mgslog);
	}

	protected void saveLogNew(String destination, String smsTemplate, String smsMessage, String smsResponse,
			SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(destination);
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		mgslog.setSender(smsAccount.getUsername());
		messageLogRepository.save(mgslog);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendAnotherUserSMS(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendAgentForgetPasswordSMS(SMSAccount smsAccount, String smsTemplate, MUser user,
			String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		logger.info("sms for agent forget password " + smsMessage);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		sendSMSForAgentForgetPassword(smsAccount, user, smsMessage, smsTemplate);
	}

	private void sendSMSForAgentForgetPassword(final SMSAccount smsAccount, final MUser destination,
			final String smsMessage, final String smsTemplate) {
		try {
			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(destination.getUserDetail().getContactNo());
			smsRequest.setMessage(smsMessage);
			iSMSApi.sendSMS(smsRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendUserSMSCashback(SMSAccount smsAccount, String smsTemplate, MUser user, String additionalInfo,
			double amount) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		model.put("amount", amount);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(user.getUserDetail().getContactNo());
		// iSMSApi.sendSMS(dto);
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@Override
	public void sendGroupSMS(SMSAccount smsAccount, String smsTemplate, GroupDetails gd, MUser user,
			String additionalInfo) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				SMSUtil.SMS_TEMPLATE + smsTemplate, model);
		SMSRequest dto = new SMSRequest();
		dto.setMessage(smsMessage);
		dto.setDestination(gd.getContactNo().substring(2, gd.getContactNo().length()));
		logger.info("contact no:: " + gd.getContactNo().substring(2, gd.getContactNo().length()));
		sendSMS(smsAccount, user, smsMessage, smsTemplate);
	}

	@Override
	public void sendTransactional(final SMSAccount smsAccount, final String destination, final String smsMessage) {
		try {

			SMSRequest smsRequest = new SMSRequest();
			smsRequest.setDestination(destination);
			smsRequest.setMessage(smsMessage);
			SMSExecution smsExe = iSMSApi.sendSMS(smsRequest);
			saveLogTransactional(destination, null, smsMessage, smsExe.getMessageId(), smsAccount);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	protected void saveLogTransactional(String destination, String smsTemplate, String smsMessage, String smsResponse,
			SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(destination);
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		mgslog.setSender(smsAccount.getUsername());
		messageLogRepository.save(mgslog);
	}

}
