package com.msscard.app.api.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IBusApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITravelBusApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.BusGetTxnIdRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.BusResponseOfMdex;
import com.msscard.entity.BusTicket;
import com.msscard.entity.MUser;
import com.msscard.entity.TravellerDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.BusSaveSeatDTO;
import com.msscard.model.BusTicketResp;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.repositories.BusJsonRepository;
import com.msscard.repositories.BusTicketRepository;
import com.msscard.repositories.TravelUserDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscards.session.PersistingSessionRegistry;

public class TravelBusApi implements ITravelBusApi {
	private final UserSessionRepository userSessionRepository;
	private final BusTicketRepository busTicketRepository;
	private final TravelUserDetailRepository travelUserDetailRepository;
	private final IUserApi iUserApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final BusJsonRepository busJsonRepository;
	private final IMdexApi iMdexApi;
	private final IBusApi iBusApi;
	
	public final SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
	public final SimpleDateFormat formater1 = new SimpleDateFormat("dd-MM-yyyy");
	
	public  TravelBusApi(UserSessionRepository userSessionRepository,
			BusTicketRepository busTicketRepository,TravelUserDetailRepository travelUserDetailRepository,
			IUserApi iUserApi,PersistingSessionRegistry persistingSessionRegistry,
			BusJsonRepository busJsonRepository,IMdexApi iMdexApi,IBusApi iBusApi) {
		this.userSessionRepository=userSessionRepository;
		this.busTicketRepository =busTicketRepository;
		this.travelUserDetailRepository= travelUserDetailRepository;
		this.iUserApi=iUserApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.busJsonRepository = busJsonRepository;
		this.iMdexApi= iMdexApi;
		this.iBusApi=iBusApi;
		
	}
	
	@Override
	public BusSaveSeatDetailsResponse saveSeatDetails(BusSaveSeatDetailsRequest dto,
			BusSaveSeatDetailsResponse result) {
		UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
		if (userSession != null) {
			UserDTO user = iUserApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(dto.getSessionId());

				//MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.BUS);
				
				
					result = saveSeat(dto, userSession.getUser(), result);
				
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ErrorMessage.FAILED_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
			result.setSuccess(false);
		}
		return result;
	}

	private BusSaveSeatDetailsResponse saveSeat(BusSaveSeatDetailsRequest dto, MUser user,
			BusSaveSeatDetailsResponse result) {
		try {
			BusTicket booking = null;
			if (dto.getTripId() != null && !dto.getTripId().isEmpty() && !"null".equalsIgnoreCase(dto.getTripId())) {
				booking = busTicketRepository.getTicketBytripId(dto.getTripId());
			} else {
				booking = new BusTicket();
				booking.setTripId(dto.getSeatDetailsId());
				booking = saveCommonDetails(booking, dto, user);
			}

			if (booking == null) {
				booking = new BusTicket();
				booking = saveCommonDetails(booking, dto, user);
			}

			booking.setUserMobile(dto.getUserMobile());
			booking.setUserEmail(dto.getUserEmail());
			booking.setSeatHoldId(dto.getSeatHoldId());
			booking.setEmtTxnId(dto.getEmtTxnId());
			booking.setEmtTransactionScreenId(dto.getEmtTransactionScreenId());
			booking.setPriceRecheckTotalFare(String.valueOf(dto.getTotalFare()));
			busTicketRepository.save(booking);

			if (dto.getTravellers() != null) {
				for (int i = 0; i < dto.getTravellers().size(); i++) {
					TravellerDetails travellerDetails = new TravellerDetails();
					travellerDetails.setfName(dto.getTravellers().get(i).getfName());
					travellerDetails.setlName(dto.getTravellers().get(i).getlName());
					travellerDetails.setAge(dto.getTravellers().get(i).getAge());
					travellerDetails.setGender(dto.getTravellers().get(i).getGender());
					travellerDetails.setSeatNo(dto.getTravellers().get(i).getSeatNo());
					travellerDetails.setSeatType(dto.getTravellers().get(i).getSeatType());
					travellerDetails.setFare(dto.getTravellers().get(i).getFare());
					travellerDetails.setBusTicketId(booking);
					travelUserDetailRepository.save(travellerDetails);
				}
			}

			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Seat Details save");
			result.setSuccess(true);
			result.setTripId(dto.getSeatDetailsId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Seat details not saved");
			result.setSuccess(false);
		}
		return result;
	}

	private BusTicket saveCommonDetails(BusTicket booking, BusSaveSeatDetailsRequest dto, MUser user) {
		booking.setBusId(dto.getBusId());
		booking.setTotalFare(dto.getTotalFare());
		booking.setTransaction(null);
		booking.setStatus(Status.Initiated);
		booking.setArrTime(dto.getArrTime());
		booking.setBusOperator(dto.getTravelName());
		booking.setSource(dto.getSource());
		booking.setDestination(dto.getDestination());

		try {
			Date d = formater.parse(dto.getJourneyDate().substring(0, 10));
			dto.setJourneyDate(formater1.format(d));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		booking.setJourneyDate(dto.getJourneyDate());
		booking.setDeptTime(dto.getDepTime());
		booking.setBoardTime(dto.getBoardTime());
		booking.setBoardingAddress(dto.getBoardLocation());
		booking.setBoardingId(dto.getBoardId());
		return booking;
	}
	
	@Override
	public CommonResponse getTxnIdUpdated(BusGetTxnIdRequest dto, CommonResponse result) {
		UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
		if (userSession != null) {
			UserDTO user = iUserApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(dto.getSessionId());

				result = iMdexApi.saveBusSeatDetails(dto, result);

				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(result.getCode())) {
					result = saveDetailsInDB(dto, result, userSession.getUser());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ErrorMessage.FAILED_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
			result.setSuccess(false);
		}
		return result;
	}

	private CommonResponse saveDetailsInDB(BusGetTxnIdRequest request, CommonResponse result, MUser user) {
		BusResponseOfMdex mdexresp = new BusResponseOfMdex();
		try {
			JSONObject getTxnIdResp = new JSONObject(result.getDetails().toString());
			mdexresp.setGetTxnIdResp(getTxnIdResp.getString("error"));
			mdexresp.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			mdexresp.setEmtTxnId(getTxnIdResp.getString("transactionid"));
			busJsonRepository.save(mdexresp);

			BusSaveSeatDetailsRequest req = new BusSaveSeatDetailsRequest();
			req.setEmtTransactionScreenId(getTxnIdResp.getString("emtTransactionScreenId"));
			req.setEmtTxnId(getTxnIdResp.getString("transactionid"));
			req.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			req.setTotalFare(getTxnIdResp.getDouble("totalAmount"));
			req.setTravellers(request.getTravellers());
			req.setTripId(request.getTripId());
			req.setUserEmail(request.getEmailId());
			req.setUserMobile(request.getMobileNo());

			BusSaveSeatDetailsResponse response = new BusSaveSeatDetailsResponse();
			response = saveSeat(req, user, response);

			BusSaveSeatDTO resp = new BusSaveSeatDTO();
			resp.setError(getTxnIdResp.getString("error"));
			resp.setTransactionCreated(getTxnIdResp.getBoolean("transactionCreated"));
			resp.setTransactionid(getTxnIdResp.getString("transactionid"));
			resp.setValidFor(getTxnIdResp.getString("validFor"));
			resp.setTransactionid(getTxnIdResp.getString("transactionid"));
			resp.setPriceRecheckAmt(getTxnIdResp.getString("totalAmount"));
			resp.setSeatHoldId(getTxnIdResp.getString("seatHoldId"));
			resp.setFareBreak(getTxnIdResp.getString("fareBreak"));
			resp.setPriceRecheck(getTxnIdResp.getBoolean("fareRecheck"));
			resp.setEmtTransactionScreenId(getTxnIdResp.getString("emtTransactionScreenId"));
			result.setDetails(resp);

			if (getTxnIdResp.getBoolean("transactionCreated") == true) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Transaction Initiated");
				result.setSuccess(true);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());

				String error = getTxnIdResp.getString("error");
				if (error != null) {
					if (!"null".equalsIgnoreCase(error)) {
						result.setMessage(error);
					} else {
						result.setMessage("Ticket not booked. Please contact customer care");
					}
				} else {
					result.setMessage("Ticket not booked. Please contact customer care");
				}
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public CommonResponse getAllBookTickets(String sessionId, CommonResponse result) {
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = iUserApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				List<BusTicketResp> allTickets = iBusApi.getAllTickets(user.getUsername());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setMessage("Book Ticket List");
				result.setDetails(allTickets);
				result.setSuccess(true);
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ErrorMessage.FAILED_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
			result.setSuccess(false);
		}
		return result;
	}
	
	
}


