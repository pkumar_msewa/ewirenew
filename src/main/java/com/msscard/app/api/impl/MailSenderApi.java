package com.msscard.app.api.impl;

import java.io.File;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.model.request.TicketDetailsDTO;
import com.msscard.entity.EmailLog;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.model.Status;
import com.msscard.model.SummaryDto;
import com.msscard.repositories.EmailLogRepository;
import com.msscard.util.MailConstants;
import com.msscards.metadatas.URLMetadatas;

public class MailSenderApi implements IMailSenderApi {
	public static final String MAIL_TEMPLATE = "com/msscard/mail/template/";

	private VelocityEngine velocityEngine;
	private EmailLogRepository emailLogRepository;

	public MailSenderApi(VelocityEngine velocityEngine, EmailLogRepository emailLogRepository) {

		this.velocityEngine = velocityEngine;
		this.emailLogRepository = emailLogRepository;
	}

	@Override
	public void dailyUpdates(String subject, String mailTemplate, List<SummaryDto> obj1, String heading,
			String filename) {

		try {
			Thread t = new Thread(new Runnable() {
				@SuppressWarnings("unchecked")
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						MimeMessage message = new MimeMessage(session);
						MimeMessageHelper helper = new MimeMessageHelper(message, true);
						helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
						@SuppressWarnings("rawtypes")
						Map model = new HashMap();
						model.put("showlist", obj1);
						model.put("logo", MailConstants.CASHIERUPIURL);
						model.put("url", MailConstants.CASHIERUPIURL);
						model.put("heading", heading);
						//String to="kbisht@msewa.com,abhijitp@msewa.com";
						String to = "kbisht@msewa.com,vjain@msewa.com,prashant@msewa.com,abhijitp@msewa.com,tnazare@msewa.com,pkumar@msewa.com,ggupta@msewa.com,akothari@msewa.com,ssureka@msewa.com,amanalath@msewa.com,ntiwari@msewa.com,hchoudhary@msewa.com,nkumar@msewa.com,mamtha@msewa.com";
						InternetAddress[] parse = InternetAddress.parse(to, true);
						message.setRecipients(javax.mail.Message.RecipientType.TO, parse);
						String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
								MAIL_TEMPLATE + mailTemplate, model);
						helper.setSubject(subject);
						helper.setText(mailBody, true);
						File file = new File(URLMetadatas.EXCEL_PATH + filename);
						helper.addAttachment(file.getName(), file);
						sendEmail(message);
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void sendBusTicketMail(String subject, String mailTemplate, MUser user, MTransaction transaction,
			TicketDetailsDTO additionalInfo) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));

							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info", additionalInfo);

							model.put("vpaylogo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("pointlogo",
									MailConstants.LIVE_URL + "resources/images/email/point.png");
							model.put("buslogo", MailConstants.LIVE_URL + "resources/images/email/Bus.png");
							model.put("buslogo1", MailConstants.LIVE_URL + "resources/images/email/Bus1.png");

							helper.setTo(new InternetAddress(additionalInfo.getUserEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendBillPayment(String subject, String mailTemplate, MUser user, MTransaction transaction
			) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));

							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
							model.put("transaction", transaction);							

							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("billpay",
									MailConstants.LIVE_URL + "resources/images/email/bill_pay.png");
							
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendChangePassword(String subject, String mailTemplate, MUser user) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
									
							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("changepwd",
									MailConstants.LIVE_URL + "resources/images/email/change_pwd.png");
							
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendFundTransfer(String subject, String mailTemplate, MUser user, MUser recipient, SendMoneyDetails sendMoneyDetails) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
							model.put("recipient", recipient);
							model.put("sendMoneyDetails", sendMoneyDetails);
							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("fundtrans",
									MailConstants.LIVE_URL + "resources/images/email/fund_trans.png");
							model.put("invitefrnds", MailConstants.LIVE_URL+"/resources/images/email/invite_frnds.png");
							
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendLoadMoney(String subject, String mailTemplate, MUser user, MTransaction senderTransaction) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
							model.put("transaction", senderTransaction);
							
							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("addmny",
									MailConstants.LIVE_URL + "resources/images/email/add_mny.png");
														
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendPrefundAlert(String subject, String mailTemplate, MUser user, String amount,String clientName ,String transactionRefNo) throws MailSendException {
		String[] emails = { MailConstants.ADMIN1_MAIL, MailConstants.ADMIN2_MAIL, MailConstants.ADMIN3_MAIL };
        try {
            Thread t = new Thread(new Runnable() {

                public void run() {
                    for (int i = 0; i < 3; i++) {
                        try {
                            Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
                            MimeMessage message = new MimeMessage(session);
                            MimeMessageHelper helper = new MimeMessageHelper(message, true);
                            helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));

                            System.out.println("Inside mail Sender");
                            Map<String, Object> model = new HashMap<String, Object>();

                            model.put("amount", amount);
                            model.put("clientName", clientName);
                            model.put("transactionRefNo", transactionRefNo);

                            model.put("logo", MailConstants.LIVE_URL + "/resources/images/email/logo.png");

                            helper.setTo(new InternetAddress(emails[i]));

                            String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                                    MailConstants.MAIL_TEMPLATE + mailTemplate, model);
                            helper.setSubject(subject);
                            helper.setText(mailBody, true);

                            sendEmail(message);

                            saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                            saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
                        }
                    }
                }
            });
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
            saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);

        }
	}

	@Override
	public void sendMobileRecharge(String subject, String mailTemplate, MUser user, MTransaction senderTransaction) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
							model.put("transaction", senderTransaction);
							
							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("recharge",
									MailConstants.LIVE_URL + "resources/images/email/recharge.png");
														
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	@Override
	public void sendRegistrationSuccess(String subject, String mailTemplate, MUser user) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message, true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map<String, Object> model = new HashMap<String, Object>();
							model.put("user", user);
														
							model.put("logo",
									MailConstants.LIVE_URL+ "/resources/images/email/logo.png");
							model.put("accverified",
									MailConstants.LIVE_URL + "resources/images/email/acc_verified.png");
							model.put("invitefrnds", MailConstants.LIVE_URL+ "/resources/images/email/invite_frnds.png");
							
														
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									MailConstants.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);

							sendEmail(message);
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL, Status.Failed);
		}
	}
	
	public static void sendEmail(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Transport.send(message, userId, password);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveLog(String subject, String mailTemplate, MUser user, String sender, Status status) {
		EmailLog email = new EmailLog();
		
		email.setDestination(user.getUserDetail().getEmail());
		
		email.setExcutionTime(new Date());
		email.setMailTemplate(mailTemplate);
		email.setStatus(status);
		email.setSender(sender);
		emailLogRepository.save(email);
	}

}
