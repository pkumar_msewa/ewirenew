package com.msscard.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;

import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.ChangeMpinError;
import com.msscard.model.ForgotMPinDTO;
import com.msscard.model.ForgotMPinError;
import com.msscard.model.MPinChangeDTO;
import com.msscard.model.MpinDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UserDTO;
import com.msscard.model.error.MpinError;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.MpinValidation;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class MpinController {

	private final MpinValidation mpinValidation;
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PasswordEncoder passwordEncoder;

	public MpinController(MpinValidation mpinValidation, IUserApi userApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, PasswordEncoder passwordEncoder) {
		super();
		this.mpinValidation = mpinValidation;
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.passwordEncoder = passwordEncoder;
	}

	@RequestMapping(value = "/SetMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> setNewMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MpinDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		MpinError error = mpinValidation.validateNewMpin(dto);
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							boolean updated = userApi.setNewMpin(dto);
							if (updated) {
								result.setDetails("Mpin Updated");
								result.setCode("S00");
								result.setStatus(ResponseStatus.SUCCESS);
							}

						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangeMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> changeOldMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MPinChangeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ChangeMpinError error = mpinValidation.validateChangeMpin(dto);
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							MpinDTO mpinDto = new MpinDTO();
							mpinDto.setUsername(dto.getUsername());
							mpinDto.setNewMpin(dto.getNewMpin());
							System.err.println("the mpin is ::" + user.getMpin());
							System.err.println("the old mpin is:::" + dto.getNewMpin());
							boolean encPassword = passwordEncoder.matches(dto.getOldMpin(), user.getMpin());
							if (encPassword) {
								boolean updated = userApi.changeCurrentMpin(dto);
								if (updated) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("MPIN is updated successfully.");
									result.setDetails("MPIN is updated successfully.");
								} else {
									result.setMessage("Failed, to update MPIN. Please try again later.");
									result.setDetails("Failed, to update MPIN. Please try again later.");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setDetails("MPIN not updated");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized User..");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("session Invalid.please try again..");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Session Invalid.please try again..");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Invalid hash..");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MPinChangeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							if (dto.getOldMpin() != null) {
								if (passwordEncoder.matches(dto.getOldMpin(),
										userSession.getUser().getUserDetail().getMpin())) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("MPIN Verified.");
									result.setDetails("MPIN Verified.");
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(userApi.handleMpinFailure(request, user.getUsername()));
									result.setDetails("MPIN Mismatch !");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("This is not your MPIN. Please enter your correct MPIN.");
								result.setDetails("This is not your MPIN. Please enter your correct MPIN.");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_HASH);
				result.setMessage("Failed, Please try again later.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> forgotMpinRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotMPinDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user != null) {
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MUser u = userApi.findByUserName(user.getUsername());
						ForgotMPinError error = mpinValidation.checkError(dto, u);
						if (error.isValid()) {
							boolean isDeleted = userApi.deleteCurrentMpin(user.getUsername());
							if (isDeleted) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Please reset your MPIN.");
								result.setDetails("Please reset your MPIN.");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("MPIN not deleted");
							result.setDetails(error);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
}
