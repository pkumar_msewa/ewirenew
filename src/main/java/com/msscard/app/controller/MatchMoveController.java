package com.msscard.app.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.enums.Device;
import com.cashiercards.errormessages.ErrorMessage;
import com.cashiercards.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.CustomAdminRequest;
import com.msscard.app.model.request.DebitRequest;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.SendMoneyRequestDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.app.model.request.TransactionSMSRequest;
import com.msscard.app.model.response.BalanceResponse;
import com.msscard.app.model.response.CustomAdminResponse;
import com.msscard.app.model.response.OrderListDTO;
import com.msscard.app.model.response.PinSetResponse;
import com.msscard.app.model.response.TransactionSMSResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.app.sms.util.SMSUtil;
import com.msscard.entity.BalanceReport;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.DonationAccount;
import com.msscard.entity.MMCards;
import com.msscard.entity.MOperator;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.MdexRequestLogs;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.UpiPay;
import com.msscard.entity.UserSession;
import com.msscard.model.BalanceSheetDTO;
import com.msscard.model.EditorDTO;
import com.msscard.model.ReconBalanceDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.SetCustomPinDTO;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.SendMoneyErrorDTO;
import com.msscard.repositories.BalanceReportRepository;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.DonationAccountRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.MdexRequestLogsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UpiPayRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.AESEncryption;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.LoadMoneyValidation;
import com.msscard.validation.SendMoneyValidation;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class MatchMoveController {

	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MUserRespository userRespository;
	private final MMCardRepository cardRepository;
	private final UserSessionRepository sessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IMatchMoveApi matchMoveApi;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final ISMSSenderApi senderApi;
	private final DataConfigRepository dataConfigRepository;
	private final SendMoneyValidation sendMoneyValidation;
	private final MTransactionRepository mTransactionRepository;
	private final MOperatorRepository operatorRepository;
	private final MdexRequestLogsRepository mdexRequestLogsRepository;
	private final MMCardRepository mMCardRepository;
	private final DonationAccountRepository donationAccountRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final ITransactionApi transactionApi;
	private final BalanceReportRepository balanceReportRepository;
	private final UpiPayRepository upiPayRepository;
	private final LoadMoneyValidation loadMoneyValidation;

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	public MatchMoveController(MatchMoveWalletRepository matchMoveWalletRepository, MUserRespository userRespository,
			MMCardRepository cardRepository, UserSessionRepository sessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry, IMatchMoveApi matchMoveApi,
			PhysicalCardDetailRepository physicalCardDetailRepository, ISMSSenderApi senderApi,
			DataConfigRepository dataConfigRepository, SendMoneyValidation sendMoneyValidation,
			MTransactionRepository mTransactionRepository, MOperatorRepository operatorRepository,
			MdexRequestLogsRepository mdexRequestLogsRepository, MMCardRepository mMCardRepository,
			DonationAccountRepository donationAccountRepository, BulkRegisterRepository bulkRegisterRepository,
			ITransactionApi transactionApi, BalanceReportRepository balanceReportRepository,
			UpiPayRepository upiPayRepository, LoadMoneyValidation loadMoneyValidation) {
		super();
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.userRespository = userRespository;
		this.cardRepository = cardRepository;
		this.sessionRepository = sessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.matchMoveApi = matchMoveApi;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.senderApi = senderApi;
		this.dataConfigRepository = dataConfigRepository;
		this.sendMoneyValidation = sendMoneyValidation;
		this.mTransactionRepository = mTransactionRepository;
		this.operatorRepository = operatorRepository;
		this.mdexRequestLogsRepository = mdexRequestLogsRepository;
		this.mMCardRepository = mMCardRepository;
		this.donationAccountRepository = donationAccountRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.transactionApi = transactionApi;
		this.balanceReportRepository = balanceReportRepository;
		this.upiPayRepository = upiPayRepository;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@RequestMapping(value = "/GenerateVirtualCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> generateVirtualCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
						createCardRequest.setEmail(user.getEmail());
						try {
							createCardRequest.setPassword(SecurityUtil.md5(user.getContactNo()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						MatchMoveWallet matchMoveWallet = matchMoveWalletRepository.findByUser(userSession.getUser());
						if (matchMoveWallet != null) {
							WalletResponse walletResponse4 = matchMoveApi.assignVirtualCard(userSession.getUser());
							if (walletResponse4.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VIRTUAL_CARD_GENERATION,
										userSession.getUser(), null);
								senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.MYNTRA_OFFERS,
										userSession.getUser(), null);

								walletResponse4.setCode(ResponseStatus.SUCCESS.getValue());
								walletResponse4.setMessage("Virtual card generated successfully");
								return new ResponseEntity<>(walletResponse4, HttpStatus.OK);

							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse
										.setMessage("You are not eligible for card.Please contact Customer care.");
							}

						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("You are not eligible for card.Please contact Customer care.");
						}
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User..");
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session...");
					return new ResponseEntity<>(walletResponse, HttpStatus.OK);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		walletResponse.setCode(ResponseStatus.FAILURE.getValue());
		walletResponse.setMessage("Operation Failed.Please contact Customer Care");
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GenerateVirtualCardSession", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> generateVirtualCardWithoutSession(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		if (role.equalsIgnoreCase("User")) {
			MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
			MUser user = userApi.findByUserName(cardRequest.getUsername());
			createCardRequest.setEmail(user.getUserDetail().getEmail());
			try {
				createCardRequest.setPassword(SecurityUtil.md5(user.getUserDetail().getContactNo()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			MatchMoveWallet matchMoveWallet = matchMoveWalletRepository.findByUser(user);
			if (matchMoveWallet != null) {
				WalletResponse walletResponse4 = matchMoveApi.assignVirtualCard(user);
				if (walletResponse4.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.VIRTUAL_CARD_GENERATION, user, null);
					senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.MYNTRA_OFFERS, user, null);

					walletResponse4.setCode(ResponseStatus.SUCCESS.getValue());
					walletResponse4.setMessage("Virtual card generated successfully");
					return new ResponseEntity<>(walletResponse4, HttpStatus.OK);
				} else {
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					walletResponse.setMessage("You are not eligible for card.Please contact Customer care.");
				}
			} else {
				walletResponse.setCode(ResponseStatus.FAILURE.getValue());
				walletResponse.setMessage("You are not eligible for card.Please contact Customer care.");
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			walletResponse.setMessage("Unauthorized User..");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		walletResponse.setCode(ResponseStatus.FAILURE.getValue());
		walletResponse.setMessage("Operation Failed.Please contact Customer Care");
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/FetchVirtualCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> getVirtualCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
						createCardRequest.setEmail(user.getEmail());
						try {
							createCardRequest.setPassword(SecurityUtil.md5(user.getContactNo()));
							createCardRequest.setUsername(user.getContactNo());
						} catch (Exception e) {
							e.printStackTrace();
						}
						MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
						if (cards != null) {
							createCardRequest.setCardId(cards.getCardId());
							walletResponse.setStatus(cards.getStatus());
						}
						walletResponse = matchMoveApi.inquireCard(createCardRequest);

						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * AFTER PROXYNO ENTERING
	 */

	@RequestMapping(value = "/AddPhysicalCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> addPhysicalCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse = matchMoveApi.assignPhysicalCard(userSession.getUser(),
								cardRequest.getProxy_number());
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
							PhysicalCardDetails card = physicalCardDetailRepository.findByUser(userSession.getUser());
							card.setStatus(Status.Received);
							physicalCardDetailRepository.save(card);
						}
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/AddPhysicalCardWeb", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> addPhysicalCardWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		WalletResponse walletResponse = new WalletResponse();
		/*
		 * boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(),
		 * hash); if (isValidHash) {
		 */
		if (role.equalsIgnoreCase("User")) {
			String sessionId = session.getId();
			UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					walletResponse = matchMoveApi.assignPhysicalCard(userSession.getUser(),
							cardRequest.getProxy_number());
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
						PhysicalCardDetails card = physicalCardDetailRepository.findByUser(userSession.getUser());
						if (card != null) {
							card.setStatus(Status.Received);
							physicalCardDetailRepository.save(card);
							return new ResponseEntity<>(walletResponse, HttpStatus.OK);

						} else {

						}
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User");
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session.Please login..");
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			walletResponse.setMessage("Unauthorized Role...");
		}
		/*
		 * }else{ walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		 * walletResponse.setMessage("Invalid Hash.."); }
		 */

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/FetchPhysicalCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> getPhysicalCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
						createCardRequest.setEmail(user.getEmail());
						try {
							createCardRequest.setPassword(SecurityUtil.md5(user.getContactNo()));
							createCardRequest.setUsername(user.getContactNo());
						} catch (Exception e) {
							e.printStackTrace();
						}
						MMCards cards = cardRepository.getPhysicalCardByUser(userSession.getUser());
						if (cards != null) {
							createCardRequest.setCardId(cards.getCardId());
						}
						walletResponse = matchMoveApi.inquireCard(createCardRequest);
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * ACTIVATE PHYSICAL CARD
	 * 
	 */

	@RequestMapping(value = "/ActivatePhysicalCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> activatePhysicalCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						ResponseDTO resp = matchMoveApi.activationPhysicalCard(cardRequest.getActivationCode(),
								userSession.getUser());
						if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							PhysicalCardDetails card = physicalCardDetailRepository.findByUser(userSession.getUser());
							card.setStatus(Status.Active);
							physicalCardDetailRepository.save(card);
							MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
							if (cards != null) {
								if (cards.getStatus().equalsIgnoreCase("Active")) {
									String cardId = cards.getCardId();
									MUser realUser = userSession.getUser();
									double balance = matchMoveApi.getBalance(userSession.getUser());
									if (balance > 0) {
										matchMoveApi.debitFromMMWalletToCard(realUser.getUserDetail().getEmail(),
												realUser.getUsername(), cardId, String.valueOf(balance));
										MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
										cardReq.setCardId(cardId);
										cardReq.setRequestType("suspend");
										matchMoveApi.deActivateCards(cardReq);
										MMCards phyCard = cardRepository.getPhysicalCardByUser(realUser);
										if (phyCard != null) {
											if (phyCard.getStatus().equalsIgnoreCase("Active")) {
												matchMoveApi.transferFundsToMMCard(realUser, String.valueOf(balance),
														phyCard.getCardId());
											}
										}
									} else {
										MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
										cardReq.setCardId(cardId);
										cardReq.setRequestType("suspend");
										matchMoveApi.deActivateCards(cardReq);
									}
								}
							}
						}
						walletResponse.setCode(resp.getCode());
						walletResponse.setMessage(resp.getMessage());
						senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.ACTIVATE_CARD, userSession.getUser(),
								null);
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/ActivatePhysicalCardWeb", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> activatePhysicalCardWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		WalletResponse walletResponse = new WalletResponse();
		/*
		 * boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(),
		 * hash); if (isValidHash) {
		 */
		if (role.equalsIgnoreCase("User")) {
			String sessionId = session.getId();
			UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					ResponseDTO resp = matchMoveApi.activationPhysicalCard(cardRequest.getActivationCode(),
							userSession.getUser());
					if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						PhysicalCardDetails card = physicalCardDetailRepository.findByUser(userSession.getUser());
						card.setStatus(Status.Active);
						physicalCardDetailRepository.save(card);
						MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
						if (cards != null) {
							if (cards.getStatus().equalsIgnoreCase("Active")) {
								String cardId = cards.getCardId();
								MUser realUser = userSession.getUser();
								double balance = matchMoveApi.getBalance(userSession.getUser());
								if (balance > 0) {
									matchMoveApi.debitFromMMWalletToCard(realUser.getUserDetail().getEmail(),
											realUser.getUsername(), cardId, String.valueOf(balance));
									MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
									cardReq.setCardId(cardId);
									cardReq.setRequestType("suspend");
									matchMoveApi.deActivateCards(cardReq);
									MMCards phyCard = cardRepository.getPhysicalCardByUser(realUser);
									if (phyCard != null) {
										if (phyCard.getStatus().equalsIgnoreCase("Active")) {
											matchMoveApi.transferFundsToMMCard(realUser, String.valueOf(balance),
													phyCard.getCardId());
										}
									}
								} else {
									MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
									cardReq.setCardId(cardId);
									cardReq.setRequestType("suspend");
									matchMoveApi.deActivateCards(cardReq);
								}
							}
						}
					}
					walletResponse.setCode(resp.getCode());
					walletResponse.setMessage(resp.getMessage());
					return new ResponseEntity<>(walletResponse, HttpStatus.OK);
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User");
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session.Please login..");
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			walletResponse.setMessage("Unauthorized Role...");
		}
		/*
		 * }else{ walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		 * walletResponse.setMessage("Invalid Hash.."); }
		 */

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * RESEND ACTIVATION CODE
	 */

	@RequestMapping(value = "/ResendActivationCode", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> resendActivationCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MMCards cards = cardRepository.getPhysicalCardByUser(userSession.getUser());
						if (cards != null) {
							PhysicalCardDetails phyDetails = physicalCardDetailRepository.findByCard(cards);
							if (phyDetails != null) {
								if (cards.getStatus().equalsIgnoreCase("Inactive")) {
									senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE,
											userSession.getUser(), phyDetails.getActivationCode());

									walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
									walletResponse.setMessage("Activation Code sent to Registered number");
									return new ResponseEntity<>(walletResponse, HttpStatus.OK);
								} else {
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage(
											"Your card is already activated.For any queries contact customer care");
								}

							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Opps!!something went wrong please contact Admin");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Errrrrrrrrrrrrrrrrrrrrr.....!!.Please contact Customer Care");
						}

					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/ResendActivationCodeWeb", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> resendActivationCodeWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MMCards cards = cardRepository.getPhysicalCardByUser(userSession.getUser());
						if (cards != null) {
							PhysicalCardDetails phyDetails = physicalCardDetailRepository.findByCard(cards);
							if (phyDetails != null) {
								if (cards.getStatus().equalsIgnoreCase("Inactive")) {
									senderApi.sendUserSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplete.ACTIVATION_CODE,
											userSession.getUser(), phyDetails.getActivationCode());

									walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
									walletResponse.setMessage("Activation Code resent to Registered number");
									return new ResponseEntity<>(walletResponse, HttpStatus.OK);
								} else {
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage(
											"Your card is already activated.For any queries contact customer care");
								}

							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Opps!!something went wrong please contact Admin");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Errrrrrrrrrrrrrrrrrrrrr.....!!.Please contact Customer Care");
						}

					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * GET BALANCE
	 */

	@RequestMapping(value = "/GetBalance", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getBalance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		BalanceResponse walletResponse = new BalanceResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if ("User".equalsIgnoreCase(role)) {
				String sessionId = session.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						double balance = matchMoveApi.getBalanceApp(userSession.getUser());

						walletResponse.setBalance(balance);
						walletResponse.setCode(ResponseStatus.SUCCESS);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH);
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GetBalanceCode", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getBalanceCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		BalanceResponse walletResponse = new BalanceResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						// double balance=matchMoveApi.getBalance(userSession.getUser());
						double balance = matchMoveApi.getBalanceApp(userSession.getUser());
						walletResponse.setBalance(balance);
						walletResponse.setCode(ResponseStatus.SUCCESS);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH);
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetBalanceWeb", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getBalanceWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, HttpServletRequest request, HttpServletResponse response,
			HttpSession httpSession) {
		BalanceResponse walletResponse = new BalanceResponse();
		/*
		 * boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(),
		 * hash); if (isValidHash) {
		 */
		if (role.equalsIgnoreCase("User")) {
			String sessionId = httpSession.getId();
			UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					MUser us = userApi.findByUserName(user.getUsername());
					if ("KYC".equalsIgnoreCase(us.getAccountDetail().getAccountType().getCode())) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MMCards cards = cardRepository.getPhysicalCardByUser(userSession.getUser());
						if (cards == null) {
							double balance = matchMoveApi.getBalanceApp(userSession.getUser());
							walletResponse.setBalance(balance);
							DataConfig con = dataConfigRepository.findDatas();
							double MinimumCardBalance = Double.valueOf(con.getMinimumCardBalance());
							if (balance < MinimumCardBalance) {
								walletResponse.setCode(ResponseStatus.FAILURE);
								walletResponse.setMessage("Insufficient balance you must have a minimum balance of Rs."
										+ con.getMinimumCardBalance()
										+ " in your virtual card to apply for physical card, Please add money.");
							} else {
								walletResponse.setCode(ResponseStatus.SUCCESS);
								walletResponse.setMessage(
										"Thank you for opting cashier physical card.Kindly provide your address details,your card will be sent to your address in 14 working days.");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE);
							walletResponse.setMessage(
									"Request already received for physical card.If you are facing issues please contact Customer care");
						}
					} else {
						walletResponse.setCode(ResponseStatus.FAILURE);
						walletResponse.setMessage("Please upgrade your account to KYC.");

					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION);
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
		}
		/*
		 * }else{ walletResponse.setCode(ResponseStatus.INVALID_HASH); }
		 */

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GetBalanceWebJS", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getBalanceWebJS(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, HttpServletRequest request, HttpServletResponse response,
			HttpSession httpSession) {
		BalanceResponse walletResponse = new BalanceResponse();
		/*
		 * boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(),
		 * hash); if (isValidHash) {
		 */
		if (role.equalsIgnoreCase("User")) {
			String sessionId = httpSession.getId();
			UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					double balance = matchMoveApi.getBalance(userSession.getUser());
					walletResponse.setBalance(balance);
					walletResponse.setCode(ResponseStatus.SUCCESS);
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION);
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
		}
		/*
		 * }else{ walletResponse.setCode(ResponseStatus.INVALID_HASH); }
		 */

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	/**
	 * GET TRANSACTIONS
	 * 
	 */

	@RequestMapping(value = "/GetTransactions", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> getTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse = matchMoveApi.getTransactions(userSession.getUser(), session.getPage());
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * 
	 * DEBIT FROM CARD
	 * 
	 */

	@RequestMapping(value = "/DebitFromCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> debitFromCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = debit.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
						if (cards != null) {
							if (cards.getStatus().equalsIgnoreCase("Active")) {
								String cardId = cards.getCardId();
								DataConfig dataConfig = dataConfigRepository.findDatas();
								double cardFees = Double.parseDouble(dataConfig.getCardFees());
								double bal = matchMoveApi.getBalance(userSession.getUser());
								if (bal >= cardFees) {
									if (device.equalsIgnoreCase("IOS")) {
										walletResponse = matchMoveApi.debitFromCard(
												userSession.getUser().getUserDetail().getEmail(),
												userSession.getUser().getUsername(), cardId, dataConfig.getCardFees(),
												"Card fees");
										senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SMS_DEDUCT_CARD,
												userSession.getUser(), dataConfig.getCardFees());
									} else {
										walletResponse = matchMoveApi.debitFromCard(
												userSession.getUser().getUserDetail().getEmail(),
												userSession.getUser().getUsername(), cardId, dataConfig.getCardFees(),
												"card fees");
										senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.SMS_DEDUCT_CARD,
												userSession.getUser(), dataConfig.getCardFees());
									}
								} else {
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage("Insufficient funds");
								}
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Card status Inactive");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Card not found");
						}
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash...");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	/**
	 * 
	 * PROMOCODE
	 * 
	 */

	@RequestMapping(value = "/PromoCode", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> promoCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = debit.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
						if (cards != null) {
							if (cards.getStatus().equalsIgnoreCase("Active")) {
								String cardId = cards.getCardId();
								if (Double.parseDouble(debit.getAmount()) >= 200) {
									walletResponse = matchMoveApi.debitFromMMWalletToCard(
											userSession.getUser().getUserDetail().getEmail(),
											userSession.getUser().getUsername(), cardId, debit.getAmount());
								} else {
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage("Amount should be greater or equals Rs 200/-");
								}
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Card status Inactive");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Card not found");
						}
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash...");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	/**
	 * RESET PIN
	 */

	@RequestMapping(value = "/ResetPin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> resetPins(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = debit.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse = matchMoveApi.resetPins(userSession.getUser());
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash...");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUser", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> getUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		walletResponse = matchMoveApi.getUsers(debit.getEmail(), debit.getMobile());
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetConsumer", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> getConsumer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();

		walletResponse = matchMoveApi.getConsumers();
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetWallet", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> getWallet(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		MatchMoveCreateCardRequest cdReq = new MatchMoveCreateCardRequest();
		cdReq.setEmail(debit.getEmail());
		try {
			cdReq.setPassword(SecurityUtil.md5(debit.getMobile()));
			cdReq.setUsername(debit.getMobile());
			walletResponse = matchMoveApi.fetchWalletMMOld(cdReq);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/TriggerTransfer", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> triggerTransfer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		String dd = sdf1.format(cal.getTime());

		List<MTransaction> listOfTransaction = null;
		try {
			listOfTransaction = mTransactionRepository.triggerTransfer(sdf1.parse(dd));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.err.println(listOfTransaction.size());
		for (MTransaction mTransaction : listOfTransaction) {
			if (mTransaction.getStatus().getValue().equalsIgnoreCase("Initiated")) {
				MPQAccountDetails accDetails = mTransaction.getAccount();
				MUser user = userRespository.findByAccountDetails(accDetails);
				if (user != null) {
					MMCards card = cardRepository.getVirtualCardsByCardUser(user);
					if (card != null) {
						WalletResponse suuucc = matchMoveApi.transferFundsToMMCard(user, "1000", card.getCardId());
						if (suuucc.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							mTransaction.setStatus(Status.Success);
							mTransactionRepository.save(mTransaction);
						}
					}
				}
			}
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendMoney", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> sendMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyRequestDTO debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
		if (isValidHash) {
			if ("User".equalsIgnoreCase(role)) {
				String sessionId = debit.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					MUser user = userSession.getUser();
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						CommonError error = sendMoneyValidation.validateKYCDebitLimit(user, debit.getAmount());
						if (error.isValid()) {
							MUser reciever = userRespository.findByUsername(debit.getRecipientNo());
							if (reciever != null && Status.Active.equals(reciever.getMobileStatus())) {
								CommonError creditCheck = loadMoneyValidation.validateKYCCreditLimit(reciever,
										Double.parseDouble(debit.getAmount()));
								if (creditCheck.isValid()) {
									MMCards pCard = cardRepository.getVirtualCardsByCardUser(user);
									MMCards vCard = cardRepository.getPhysicalCardByUser(user);

									if (pCard != null && Status.Active.getValue().equalsIgnoreCase(pCard.getStatus())) {
										walletResponse = matchMoveApi.transferFundFromCardToWallet(debit.getAmount(),
												user, pCard.getCardId(), reciever);
										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(walletResponse.getCode())) {
											return new ResponseEntity<>(walletResponse, HttpStatus.OK);
										}
									} else if (vCard != null && Status.Active.getValue().equalsIgnoreCase(vCard.getStatus())) {
										walletResponse = matchMoveApi.transferFundFromCardToWallet(debit.getAmount(),
												user, vCard.getCardId(), reciever);
										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(walletResponse.getCode())) {
											return new ResponseEntity<>(walletResponse, HttpStatus.OK);
										} else {
											matchMoveApi.transferFundsToMMCard(user, debit.getAmount(),
													pCard.getCardId());
											return new ResponseEntity<>(walletResponse, HttpStatus.OK);
										}
									}
								} else {
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage(creditCheck.getMessage());
									return new ResponseEntity<>(walletResponse, HttpStatus.OK);
								}
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Invalid Reciever.");
								return new ResponseEntity<>(walletResponse, HttpStatus.OK);
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage(error.getMessage());
							return new ResponseEntity<>(walletResponse, HttpStatus.OK);
						}
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session");
					return new ResponseEntity<>(walletResponse, HttpStatus.OK);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash...");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		walletResponse.setCode(ResponseStatus.FAILURE.getValue());
		walletResponse.setMessage("Operation failed");
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/AcknowledgeTransfer", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> acknowledgeTransfer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		walletResponse = matchMoveApi.acknowledgeFundTransfer(debit.getAmount(), debit.getTransactionId(),
				debit.getRecipientNo());
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetTransactionByPage", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> getTransactionByPage(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		MUser us = userRespository.findByUsername(debit.getMobile());
		walletResponse = matchMoveApi.getTransactions(us, debit.getPage());
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/ActivateCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> activationCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO walletResponse = new ResponseDTO();
		walletResponse = matchMoveApi.activateMMPhysicalCard(debit.getCardId(), debit.getActivation());
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/ActivateVirtualCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> activationVirtualCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO debit, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO walletResponse = new ResponseDTO();
		MatchMoveCreateCardRequest dto = new MatchMoveCreateCardRequest();
		dto.setCardId(debit.getCardId());
		walletResponse = matchMoveApi.reActivateCard(dto);
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/CustomActivatePhysicalCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> customActivatePhysicalCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO cardRequest, HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		if (role.equalsIgnoreCase("User")) {
			MUser user = userRespository.findByUsername(cardRequest.getMobile());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				if (true) {
					/*
					 * PhysicalCardDetails card=physicalCardDetailRepository.findByUser(user);
					 * card.setStatus(Status.Active); physicalCardDetailRepository.save(card);
					 */
					MMCards cards = cardRepository.getVirtualCardsByCardUser(user);
					if (cards != null) {
						if (true) {
							String cardId = cards.getCardId();
							MUser realUser = user;
							double balance = matchMoveApi.getBalance(user);
							if (balance > 0) {
								matchMoveApi.debitFromMMWalletToCard(realUser.getUserDetail().getEmail(),
										realUser.getUsername(), cardId, String.valueOf(balance));
								MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
								cardReq.setCardId(cardId);
								cardReq.setRequestType("suspend");
								matchMoveApi.deActivateCards(cardReq);
								MMCards phyCard = cardRepository.getPhysicalCardByUser(realUser);
								if (phyCard != null) {
									if (true) {
										matchMoveApi.transferFundsToMMCard(realUser, String.valueOf(balance),
												phyCard.getCardId());
									}
								}
							} /*
								 * else{ MatchMoveCreateCardRequest cardReq=new MatchMoveCreateCardRequest();
								 * cardReq.setCardId(cardId); cardReq.setRequestType("suspend");
								 * matchMoveApi.deActivateCards(cardReq); }
								 */
						}
					}
				}
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User");
			}

		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			walletResponse.setMessage("Unauthorized Role...");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GenerateVirtualCardCustom", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> generateVirtualCardCustom(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				// String sessionId = cardRequest.getSessionId();
				// UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (true) {
					// UserDTO user = userApi.getUserById(userSession.getUser().getId());
					MUser user = userApi.findByUserName(cardRequest.getMobile());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
						createCardRequest.setEmail(user.getUserDetail().getEmail());
						try {
							createCardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						MatchMoveWallet matchMoveWallet = matchMoveWalletRepository.findByUser(user);
						if (matchMoveWallet == null) {
							WalletResponse walletResponse4 = matchMoveApi.createWallet(createCardRequest);
							if (walletResponse4.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								// senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP,
								// SMSTemplete.VIRTUAL_CARD_GENERATION, userSession.getUser(), null);
								if (walletResponse4.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									MatchMoveWallet moveWallet = new MatchMoveWallet();
									moveWallet.setWalletId(walletResponse4.getWalletId());
									moveWallet.setWalletNumber(walletResponse4.getWalletNumber());
									moveWallet.setUser(user);
									moveWallet.setMmUserId(walletResponse4.getMmUserId());
									matchMoveWalletRepository.save(moveWallet);
									System.err.println("saved successfully");
									walletResponse4 = matchMoveApi.assignVirtualCard(user);
									return new ResponseEntity<>(walletResponse4, HttpStatus.OK);
								}

							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse
										.setMessage("You are not eligible for card.Please contact Customer care.");
							}
						} else {
							walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							walletResponse.setMessage("Unauthorized User..");
							return new ResponseEntity<>(walletResponse, HttpStatus.OK);
						}
					} else {
						walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
						walletResponse.setMessage("Invalid Session...");
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					}
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
				walletResponse.setMessage("Invalid Hash");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
			walletResponse.setCode(ResponseStatus.FAILURE.getValue());
			walletResponse.setMessage("Operation Failed.Please contact Customer Care");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);

		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetMyBalance", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getMyBalance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		BalanceResponse walletResponse = new BalanceResponse();
		// boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(),
		// hash);
		if (true) {
			if (role.equalsIgnoreCase("User")) {
				/*
				 * String sessionId = session.getSessionId(); UserSession userSession =
				 * sessionRepository.findByActiveSessionId(sessionId);
				 */
				if (true) {
					MUser us = userRespository.findByUsername(session.getMobile());
					UserDTO user = userApi.getUserById(us.getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						double balance = matchMoveApi.myBalance(us);
						walletResponse.setBalance(balance);
						walletResponse.setCode(ResponseStatus.SUCCESS);
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
					}
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
			}
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GetBalanceExp", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getBalanceexp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO walletResponse = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse = matchMoveApi.getBalanceExp(userSession.getUser());
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/CreateUser", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> createUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO walletResponse = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				MUser user = userRespository.findByUsername(session.getSessionId());
				matchMoveApi.createUserOnMM(user);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/CreateUserMM", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> createUserOnMM(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto,
			/* @RequestHeader(value="hash",required=false) String hash, */ HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO walletResponse = new ResponseDTO();
		// boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(),
		// hash);
		// if (isValidHash) {
		if (role.equalsIgnoreCase("User")) {
			MUser user = userApi.findByUserName(dto.getContactNo());
			walletResponse = matchMoveApi.createUserOnMM(user);
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
		}
		/*
		 * }else{ walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue()); }
		 */

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/MyOrders", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> orders(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO dto = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			System.err.println("hi m here");
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = sessionRepository.findByActiveSessionId(session.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(session.getSessionId());

						List<OrderListDTO> olist = new ArrayList<OrderListDTO>();
						Pageable paging = new PageRequest(Integer.parseInt(session.getPage()),
								Integer.parseInt(session.getSize()));
						MOperator operator = operatorRepository.findOperatorByName("MDEX");

						Page<MTransaction> list = mTransactionRepository.getAllDetails(paging, operator,
								userSession.getUser().getAccountDetail());
						System.err.println("This is Size:::" + list.getSize());

						for (MTransaction mTransaction : list) {
							OrderListDTO order = new OrderListDTO();
							order.setTransactionNumber(mTransaction.getTransactionRefNo());
							order.setAmount(mTransaction.getAmount());
							order.setStatus(mTransaction.getStatus());
							// order.setDate(mTransaction.getCreated());

							order.setServiceCode(mTransaction.getService().getCode());
							order.setServiceName(mTransaction.getService().getName());
							order.setImageUrl(mTransaction.getService().getServiceImage());
							// order.setDescription(mTransaction.getDescription());
							order.setDate(sdf.format(mTransaction.getCreated()));
							String tranRefNo = mTransaction.getTransactionRefNo();
							MTransaction mdexTran = mTransactionRepository.getByUpiId(tranRefNo);
							if (mdexTran != null) {
								MdexRequestLogs requestLogs = mdexRequestLogsRepository.findByTransaction(mdexTran);
								if (requestLogs != null) {
									System.err.println("hi m here...........");
									if (requestLogs.getTopUpType().equalsIgnoreCase("Prepaid")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
										System.err.println("Prepaid" + requestLogs.getRequest());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Postpaid")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
										System.err.println("Postpaid" + requestLogs.getRequest());

									}

									else if (requestLogs.getTopUpType().equalsIgnoreCase("DTH")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getDthNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("GAS")) {
										order.setDescription(mTransaction.getDescription() + " on "
												+ requestLogs.getAccountNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Landline")) {
										order.setDescription(mTransaction.getDescription() + " on "
												+ requestLogs.getLandlineNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("DataCard")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Insurance")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getPolicyNumber());
									} else {
										order.setDescription(mTransaction.getDescription());

									}
								}
							} else {

								MdexRequestLogs requestLogs = mdexRequestLogsRepository.findByTransaction(mTransaction);
								if (requestLogs != null) {
									System.err.println("hi m here...........");
									if (requestLogs.getTopUpType().equalsIgnoreCase("Prepaid")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
										System.err.println("Prepaid" + requestLogs.getRequest());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Postpaid")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
										System.err.println("Postpaid" + requestLogs.getRequest());

									}

									else if (requestLogs.getTopUpType().equalsIgnoreCase("DTH")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getDthNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("GAS")) {
										order.setDescription(mTransaction.getDescription() + " on "
												+ requestLogs.getAccountNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Landline")) {
										order.setDescription(mTransaction.getDescription() + " on "
												+ requestLogs.getLandlineNumber());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("DataCard")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getRequest());
									} else if (requestLogs.getTopUpType().equalsIgnoreCase("Insurance")) {
										order.setDescription(
												mTransaction.getDescription() + " on " + requestLogs.getPolicyNumber());
									} else {
										order.setDescription(mTransaction.getDescription());

									}
								}

							}
							System.err.println("The list::" + order);
							olist.add(order);

						}
						dto.setCode(ResponseStatus.SUCCESS.getValue());
						dto.setDetails(olist);
						dto.setTotalPages(list.getTotalPages());
						return new ResponseEntity<ResponseDTO>(dto, HttpStatus.OK);
					} else {
						dto.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());

					}

				} else {
					dto.setCode(ResponseStatus.INVALID_SESSION.getValue());
				}
			} else {
				dto.setCode(ResponseStatus.INVALID_HASH.getValue());
			}

		}
		return new ResponseEntity<ResponseDTO>(dto, HttpStatus.OK);

	}

	// Transfer from wallet to card
	@RequestMapping(value = "/TransferWalletToCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> transferWalleToCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		if (role.equalsIgnoreCase("User")) {
			if (true) {
				MUser us = userRespository.findByUsername(session.getMobile());
				UserDTO user = userApi.getUserById(us.getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					WalletResponse resp = matchMoveApi.transferFundsToMMCard(us, session.getAmount(),
							session.getCardId());
					walletResponse.setDetails(resp);
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/DebitPhysicalCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> debitCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = debit.getSessionId();
				MUser user = userRespository.findByUsername(sessionId);
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					MMCards cards = cardRepository.getPhysicalCardByUser(user);
					if (cards != null) {
						if (cards.getStatus().equalsIgnoreCase("Active")) {
							String cardId = cards.getCardId();
							DataConfig dataConfig = dataConfigRepository.findDatas();
							double cardFees = Double.parseDouble(dataConfig.getCardFees());
							if (Double.parseDouble(debit.getAmount()) >= cardFees) {
								walletResponse = matchMoveApi.debitFromCard(user.getUserDetail().getEmail(),
										user.getUsername(), cardId, debit.getAmount(), "Physical Card fees");
								senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplete.SMS_DEDUCT, user,
										null);
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Amount should be greater or equals Rs " + cardFees + "/-");
							}
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Card status Inactive");
						}
					} else {
						walletResponse.setCode(ResponseStatus.FAILURE.getValue());
						walletResponse.setMessage("Card not found");
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User");
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session");
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			walletResponse.setMessage("Unauthorized Role...");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetTransfers", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> getTransfers(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		BalanceResponse walletResponse = new BalanceResponse();
		if (role.equalsIgnoreCase("User")) {
			if (true) {
				MUser us = userRespository.findByUsername(session.getMobile());
				UserDTO user = userApi.getUserById(us.getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					WalletResponse resp = matchMoveApi.getFundTransfer(us);
					walletResponse.setCode(ResponseStatus.SUCCESS);
					walletResponse.setDetails(resp.getDetails());
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
				}
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/CancelTransfer", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BalanceResponse> cancelTransfer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EditorDTO session, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		BalanceResponse walletResponse = new BalanceResponse();
		if (role.equalsIgnoreCase("User")) {
			if (true) {
				MUser us = userRespository.findByUsername(session.getMobile());
				UserDTO user = userApi.getUserById(us.getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					WalletResponse resp = matchMoveApi.cancelFundTransfer(us, session.getAmount(), session.getCardId());
					walletResponse.setCode(ResponseStatus.SUCCESS);
					walletResponse.setDetails(resp.getDetails());
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER);
				}
			}
		} else {
			walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE);
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/GetTransactionsCustom", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> getTransactionCustom(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(session.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse = matchMoveApi.getTransactionsCustom(userSession.getUser(), session.getPage());
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/CustomActivatePhysicalCardNew", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> customActivatePhysicalCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest cardRequest,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						PhysicalCardDetails cardDetails = physicalCardDetailRepository
								.findByUser(userSession.getUser());
						if (cardDetails != null) {
							ResponseDTO resp = matchMoveApi.activationPhysicalCard(cardDetails.getActivationCode(),
									userSession.getUser());
							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								PhysicalCardDetails card = physicalCardDetailRepository
										.findByUser(userSession.getUser());
								card.setStatus(Status.Active);
								physicalCardDetailRepository.save(card);
								MMCards cards = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
								if (cards != null) {
									if (cards.getStatus().equalsIgnoreCase("Active")) {
										String cardId = cards.getCardId();
										MUser realUser = userSession.getUser();
										double balance = matchMoveApi.getBalance(userSession.getUser());
										if (balance > 0) {
											matchMoveApi.debitFromMMWalletToCard(realUser.getUserDetail().getEmail(),
													realUser.getUsername(), cardId, String.valueOf(balance));
											MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
											cardReq.setCardId(cardId);
											cardReq.setRequestType("suspend");
											matchMoveApi.deActivateCards(cardReq);
											MMCards phyCard = cardRepository.getPhysicalCardByUser(realUser);
											if (phyCard != null) {
												if (phyCard.getStatus().equalsIgnoreCase("Active")) {
													matchMoveApi.transferFundsToMMCard(realUser,
															String.valueOf(balance), phyCard.getCardId());
												}
											}
										} else {
											MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
											cardReq.setCardId(cardId);
											cardReq.setRequestType("suspend");
											matchMoveApi.deActivateCards(cardReq);
										}
									}
								}
							}
							walletResponse.setCode(resp.getCode());
							walletResponse.setMessage(resp.getMessage());
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.ACTIVATE_CARD,
									userSession.getUser(), null);
							return new ResponseEntity<>(walletResponse, HttpStatus.OK);
						} else {
							walletResponse.setCode(ResponseStatus.FAILURE.getValue());
							walletResponse.setMessage("Invalid Request.Please contact customer care");
						}
					} else {
						walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						walletResponse.setMessage("Unauthorized User");
					}
				} else {
					walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					walletResponse.setMessage("Invalid Session.Please login..");
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role...");
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			walletResponse.setMessage("Invalid Hash..");
		}

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/UpdateEmail", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> updateEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();

		MUser user = userApi.findByUserName(debit.getSessionId());

		walletResponse = matchMoveApi.updateEmail(user, debit.getAmount());

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendTransactionSMS", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionSMSResponse> sendTransactionalSMS(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionSMSRequest smsRequest, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		TransactionSMSResponse smsResponse = new TransactionSMSResponse();
		try {

			String clientKeyAc = SecurityUtil.md5("AbhijitPriyadarshan" + "123456");
			String clientTokenAc = SecurityUtil.md5("8895093779" + "123456");
			System.err.println(clientKeyAc);
			System.err.println(clientTokenAc);
			if (smsRequest.getClientKey().equalsIgnoreCase(clientKeyAc)
					&& smsRequest.getClientToken().equalsIgnoreCase(clientTokenAc)) {

				MUser user = userApi.findByUserName(smsRequest.getTo().trim());
				if (user == null) {
					smsResponse.setCode(ResponseStatus.FAILURE.getValue());
					smsResponse.setStatus("Invalid User");
					return new ResponseEntity<TransactionSMSResponse>(smsResponse, HttpStatus.OK);
				}

				senderApi.sendTransactional(SMSAccount.PAYQWIK_TRANSACTIONAL, smsRequest.getTo(),
						smsRequest.getMessage());
				smsResponse.setCode(ResponseStatus.SUCCESS.getValue());
				smsResponse.setStatus("Success");

			} else {
				smsResponse.setCode("E00");
				smsResponse.setStatus("Keys Mismatch");
			}
		} catch (Exception e) {
			e.printStackTrace();
			smsResponse.setCode(ResponseStatus.FAILURE.getValue());
			smsResponse.setStatus("Failed");
		}
		return new ResponseEntity<TransactionSMSResponse>(smsResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/SetCustomPin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<PinSetResponse> setCustomPIN(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "Authorization") String authorization, @RequestBody SetCustomPinDTO pinReq,
			HttpServletRequest request, HttpServletResponse response) {
		PinSetResponse walletResponse = new PinSetResponse();
		try {
			UpiPay value = upiPayRepository.getDataUsingKeys(SMSConstant.UPIKEY);
			String upiBasicAuth = SMSConstant.getBasicAuthorizationUpi(value.getUsername(), value.getPassword());
			if (upiBasicAuth.equals(authorization)) {
				SetCustomPinDTO treq = SMSConstant
						.prepareCustomPinRequest(AESEncryption.decrypt(pinReq.getCustomPinRequest()));
				if (role.equalsIgnoreCase("User")) {
					String sessionId = treq.getSessionId();
					UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							if (treq.getPin().length() == 4) {
								UserKycResponse pinsetResp = matchMoveApi.setPIN(userSession.getUser(), treq.getPin());
								if (pinsetResp != null
										&& pinsetResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									Date date = format.parse(treq.getDob());
									UserKycResponse confirmPin = matchMoveApi.setPINConfirmReset(userSession.getUser(),
											treq.getPin(), date);
									if (confirmPin != null && confirmPin.getCode()
											.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
										walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
										walletResponse.setMessage("Pin set Successful");
										return new ResponseEntity<PinSetResponse>(walletResponse, HttpStatus.OK);
									} else {
										walletResponse.setCode(confirmPin.getCode());
										walletResponse.setMessage(confirmPin.getMessage());
									}
								} else {
									walletResponse.setCode(pinsetResp.getCode());
									walletResponse.setMessage(pinsetResp.getMessage());
								}
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("PIN must be 4-digit long");
							}
						}
					} else {
						walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
						walletResponse.setMessage("Invalid Session");
					}
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				walletResponse.setMessage("Unauthorized Role");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<PinSetResponse>(walletResponse, HttpStatus.OK);
	}

	// block unblock card
	@RequestMapping(value = "/BlockCard", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> reactivateOrSuspendCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest dto) {
		ResponseDTO result = new ResponseDTO();

		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
						UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
						MUser m = userApi.findByUserName(activeUser.getUsername());

						if (dto.getCardType().equals("physicalCard")) {
							MMCards cards1 = userApi.findByUserWalletPhysical(m);
							// MMCards cards2 = userApi.findByUserWalletVirtual(m);
							if (cards1 != null) {
								if (cards1.getStatus().equals(Status.Active.getValue()) && !(cards1.isBlocked())) {
									dto.setCardId(cards1.getCardId());
									result = matchMoveApi.deActivateCards(dto);

								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(ErrorMessage.PHYSICAL_CARD_BLOCKED);
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage(ErrorMessage.CARDS);
								result.setSuccess(false);
							}

						} else {
							// MMCards cards1 = userApi.findByUserWalletPhysical(m);
							MMCards cards3 = userApi.findByUserWalletVirtual(m);
							if (cards3 != null) {
								dto.setCardId(cards3.getCardId());
								result = matchMoveApi.deActivateCards(dto);
								if (!(dto.getCardType().equals("physicalCard"))) {
									cards3.setStatus("Inactive");
									cards3.setBlocked(true);
									mMCardRepository.save(cards3);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage(ErrorMessage.CARDS);
								result.setSuccess(false);
							}
						}

					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ErrorMessage.DEVICE_MSG);
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.AUTHORITY_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ReactivateCard", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> reactivateCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MatchMoveCreateCardRequest dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
						UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
						MUser m = userApi.findByUserName(activeUser.getUsername());

						if (dto.getCardType().equals("physicalCard")) {
							MMCards cards1 = userApi.findByUserWalletPhysical(m);
							MMCards cards2 = userApi.findByUserWalletVirtual(m);
							if (cards1 != null) {
								if (cards1.isHasPhysicalCard()) {
									if (cards1.isBlocked()) {
										dto.setCardId(cards1.getCardId());
										result = matchMoveApi.reActivateCard(dto);
										/*
										 * double balance= matchMoveApi.getBalance(m); if(balance > 0) {
										 * matchMoveApi.transferFundsToMMCard(m, String.valueOf(balance),
										 * cards1.getCardId()); }
										 */
									}
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage(ErrorMessage.CARDS);
								result.setSuccess(false);
							}

							if (cards2 != null) {
								if (!cards2.isBlocked() && cards2.getStatus().equals(Status.Active.getValue())) {
									result = matchMoveApi.deActivateCards(dto);
									if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
										cards2.setStatus(Status.Inactive.getValue());
										cards2.setBlocked(true);
										mMCardRepository.save(cards2);
									} else {
										result.setStatus(ResponseStatus.FAILURE.getKey());
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage(result.getMessage());
										result.setSuccess(false);
									}
								}
							}

						} else {
							MMCards cards1 = userApi.findByUserWalletPhysical(m);
							MMCards cards2 = userApi.findByUserWalletVirtual(m);
							if (cards1 != null) {
								if (cards2 != null) {
									if (!cards2.isHasPhysicalCard()) {
										if (cards2.isBlocked()) {
											if (cards1.isBlocked()) {
												dto.setCardId(cards2.getCardId());
												result = matchMoveApi.reActivateCard(dto);
												/*
												 * double balance= matchMoveApi.getBalance(m); if(balance > 0) {
												 * matchMoveApi.debitFromMMWalletToCard(m.getUserDetail().getEmail(),
												 * m.getUsername(), cards2.getCardId(), String.valueOf(balance)); }
												 */
											} else {
												result.setStatus(ResponseStatus.FAILURE.getKey());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage(ErrorMessage.PHYSICAL_CARD);
												result.setSuccess(false);
											}
										} else {
											result.setStatus(ResponseStatus.FAILURE.getKey());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setMessage(ErrorMessage.VIRTUAL_CARD_BLOCKED);
											result.setSuccess(false);
										}
									} else {
										result.setStatus(ResponseStatus.FAILURE.getKey());
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage(ErrorMessage.PHYSICAL_CARD);
										result.setSuccess(false);
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(ErrorMessage.CARDS);
									result.setSuccess(false);
								}
							} else {
								if (cards2 != null) {
									dto.setCardId(cards2.getCardId());
									result = matchMoveApi.reActivateCard(dto);
									/*
									 * double balance= matchMoveApi.getBalance(m); if(balance > 0) {
									 * matchMoveApi.debitFromMMWalletToCard(m.getUserDetail().getEmail(),
									 * m.getUsername(), cards2.getCardId(), String.valueOf(balance)); }
									 */
								}
							}
						}

					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ErrorMessage.DEVICE_MSG);
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.AUTHORITY_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.TRY_AGAIN_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Donation", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<WalletResponse> donationProcess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyRequestDTO debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		WalletResponse walletResponse = new WalletResponse();
		try {
			boolean isValidHash = SecurityUtil.isHashMatches(debit.getSessionId(), hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("User")) {
					String sessionId = debit.getSessionId();
					UserSession userSession = sessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							SendMoneyErrorDTO error = sendMoneyValidation.donationMoneyError(debit,
									userSession.getUser());
							if (error.isValid()) {
								MMCards pCard = cardRepository.getVirtualCardsByCardUser(userSession.getUser());
								MMCards vCard = cardRepository.getPhysicalCardByUser(userSession.getUser());
								if (Status.Active.getValue().equalsIgnoreCase(pCard.getStatus())) {
									walletResponse = matchMoveApi.transferFundFromCardToWalletDonation(
											debit.getAmount(), userSession.getUser(), pCard.getCardId(),
											debit.getRecipientNo());
									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
										DonationAccount donation = new DonationAccount();
										donation.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER
												+ userSession.getUser().getUserDetail().getId());
										donation.setBalance(debit.getAmount());
										donation.setDonateeTo(debit.getRecipientNo());
										donation.setUser(userSession.getUser());
										donationAccountRepository.save(donation);
										walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
										walletResponse.setStatus(ResponseStatus.SUCCESS.getKey());
										walletResponse.setMessage("Transfer successfull");
										return new ResponseEntity<>(walletResponse, HttpStatus.OK);
									} else {
										matchMoveApi.transferFundsToMMCard(userSession.getUser(), debit.getAmount(),
												pCard.getCardId());
										return new ResponseEntity<>(walletResponse, HttpStatus.OK);
									}
								} else {
									if (Status.Active.getValue().equalsIgnoreCase(vCard.getStatus())) {
										walletResponse = matchMoveApi.transferFundFromCardToWalletDonation(
												debit.getAmount(), userSession.getUser(), vCard.getCardId(),
												debit.getRecipientNo());
										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(walletResponse.getCode())) {
											DonationAccount donation = new DonationAccount();
											donation.setAccountNumber(SMSUtil.BASE_ACCOUNT_NUMBER
													+ userSession.getUser().getUserDetail().getId());
											donation.setBalance(debit.getAmount());
											donation.setDonateeTo(debit.getRecipientNo());
											donation.setUser(userSession.getUser());
											donationAccountRepository.save(donation);
											walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
											walletResponse.setStatus(ResponseStatus.SUCCESS.getKey());
											walletResponse.setMessage("Transfer successfull");
											return new ResponseEntity<>(walletResponse, HttpStatus.OK);
										} else {
											matchMoveApi.transferFundsToMMCard(userSession.getUser(), debit.getAmount(),
													pCard.getCardId());
											return new ResponseEntity<>(walletResponse, HttpStatus.OK);
										}
									}
								}
							} else {
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage(error.getMessage());
								return new ResponseEntity<>(walletResponse, HttpStatus.OK);
							}
						} else {
							walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							walletResponse.setMessage("Unauthorized User");
							return new ResponseEntity<>(walletResponse, HttpStatus.OK);
						}
					} else {
						walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
						walletResponse.setMessage("Invalid Session");
						return new ResponseEntity<>(walletResponse, HttpStatus.OK);
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					walletResponse.setMessage("Unauthorized Role...");
					return new ResponseEntity<>(walletResponse, HttpStatus.OK);
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
				walletResponse.setMessage("Invalid Hash...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
			walletResponse.setCode(ResponseStatus.FAILURE.getValue());
			walletResponse.setMessage("Operation failed");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/DebitDriveu", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> debitDriveu(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		List<BulkRegister> listBulkRegister = bulkRegisterRepository.getRegisteredUsers();
		if (listBulkRegister != null && listBulkRegister.size() > 0) {
			for (BulkRegister bulkRegister : listBulkRegister) {
				System.err.println("inside>>>>>>>>>>>>>>>>>>>");
				MUser user = bulkRegister.getUser();
				double balance = matchMoveApi.getBalanceCus(user);

				MMCards cards = cardRepository.getPhysicalCardByUser(user);
				if (cards != null) {
					String cardId = cards.getCardId();
					MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
					cardReq.setUsername(user.getUsername());
					cardReq.setEmail(user.getUserDetail().getEmail());
					cardReq.setCardId(cardId);
					WalletResponse inqResp = matchMoveApi.inquireCard(cardReq);
					if (inqResp != null && inqResp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						if (inqResp.getStatus().equalsIgnoreCase("active")) {
							UserKycResponse debitResponse = matchMoveApi.debitFromCardCorp(
									user.getUserDetail().getEmail(), user.getUsername(), cardId,
									String.valueOf(balance));
							if (debitResponse != null
									&& debitResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								transactionApi.dobulkDebitTransactionCorp(user, String.valueOf(balance));
								System.err.println("Debit Successfull>>>>>>>>>>>");
							} else {
								transactionApi.dobulkDebitTransactionCorpFailed(user, String.valueOf(balance));
							}
						} else {
							MatchMoveCreateCardRequest reActiveReq = new MatchMoveCreateCardRequest();
							reActiveReq.setCardId(cardId);
							ResponseDTO reactive = matchMoveApi.reActivateCardCorp(reActiveReq);
							if (reactive != null
									&& reactive.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								UserKycResponse debitResponse = matchMoveApi.debitFromCardCorp(
										user.getUserDetail().getEmail(), user.getUsername(), cardId,
										String.valueOf(balance));
								if (debitResponse != null && debitResponse.getCode()
										.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									transactionApi.dobulkDebitTransactionCorp(user, String.valueOf(balance));
									System.err.println("Debit Successfull>>>>>>>>>>>>>>>>>");
								} else {
									transactionApi.dobulkDebitTransactionCorpFailed(user, String.valueOf(balance));
								}
							} else {
								transactionApi.dobulkDebitTransactionCorpFailed(user, String.valueOf(balance));

							}

						}
					}
				}

			}

		}
		walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/DebitFromCardTemp", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> debitCardMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();

		MUser user = userRespository.findByUsername(debit.getMobile());
		MMCards cards = cardRepository.getVirtualCardsByCardUser(user);
		if (!cards.isBlocked()) {
			String cardId = cards.getCardId();
			walletResponse = matchMoveApi.debitFromCard(user.getUserDetail().getEmail(), user.getUsername(), cardId,
					debit.getAmount(), "debit from Card");

		} else {
			MMCards card = cardRepository.getPhysicalCardByUser(user);
			if (card != null) {
				if (!card.isBlocked()) {
					String cardId = card.getCardId();
					walletResponse = matchMoveApi.debitFromCard(user.getUserDetail().getEmail(), user.getUsername(),
							cardId, debit.getAmount(), "debit from card");
				} else {
					walletResponse.setCode("FOO");
					walletResponse.setMessage("physical card is blocked");
				}
			} else {
				walletResponse.setCode(ResponseStatus.FAILURE.getValue());
				walletResponse.setMessage("virtual card is blocked");
			}
		}
		return new ResponseEntity<UserKycResponse>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAllWalletBalance", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> getAvailableBalance(HttpServletRequest request,
			HttpServletResponse response) {
		ReconBalanceDTO walletResponse = new ReconBalanceDTO();
		try {
			String ipAddress1 = request.getHeader("X-Forwarded-For");// capitalisation
			String ipAddress2 = request.getHeader("x-forwarded-for");// doesn't
			String ipAddress3 = request.getHeader("X-FORWARDED-FOR");// matter
			System.err.println("IP 1::" + ipAddress1);
			System.err.println("IP 2::" + ipAddress2);
			System.err.println("IP 3::" + ipAddress3);
			System.err.println("Remote address::" + request.getRemoteAddr());
			List<MUser> listUsers = userRespository.getListOfUsers(Status.Active, UserType.User);
			List<BalanceSheetDTO> balanceLsit = new ArrayList<>();
			for (MUser mUser : listUsers) {
				MMCards virtualCard = mMCardRepository.getVirtualCardsByCardUser(mUser);
				if (virtualCard != null) {
					Double balance = matchMoveApi.getBalanceAppWallet(mUser);
					if (balance != null) {
						BalanceSheetDTO balSheet = new BalanceSheetDTO();
						balSheet.setBalance(String.valueOf(balance));
						balSheet.setUsername(mUser.getUsername());
						balanceLsit.add(balSheet);
						BalanceReport balReport = new BalanceReport();
						balReport.setBalance(String.valueOf(balance));
						balReport.setUsername(mUser.getUsername());
						balanceReportRepository.save(balReport);
					}

				}
			}

			walletResponse.setDetails(balanceLsit);
			walletResponse.setMessage("Details received");
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<ReconBalanceDTO>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUserResponse/{username}", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> getUserResponse(@PathVariable(value = "username") String username,
			HttpServletRequest request, HttpServletResponse response) {
		ReconBalanceDTO walletResponse = new ReconBalanceDTO();
		try {
			System.err.println("Remote address::" + request.getRemoteAddr());
			MUser user = userRespository.findByUsername(username);
			UserKycResponse aresp = matchMoveApi.getUsers(user.getUserDetail().getEmail(), user.getUsername());
			walletResponse.setDetails(aresp.getMmUserId());
			walletResponse.setMessage(String.valueOf(matchMoveApi.getBalanceAppWallet(user)));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<ReconBalanceDTO>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUserResponseUpdate", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> getUserResponseUpdate(HttpServletRequest request,
			HttpServletResponse response) {
		ReconBalanceDTO walletResponse = new ReconBalanceDTO();
		try {
			int count = 0;
			System.err.println("Remote address::" + request.getRemoteAddr());
			List<MatchMoveWallet> listWallet = matchMoveWalletRepository.getListOfWallet();
			if (listWallet != null && !listWallet.isEmpty()) {
				for (MatchMoveWallet matchMoveWallet : listWallet) {
					UserKycResponse aresp = matchMoveApi.getUsers(matchMoveWallet.getUser().getUserDetail().getEmail(),
							matchMoveWallet.getUser().getUsername());
					if (aresp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						matchMoveWallet.setMmUserId(aresp.getMmUserId());
						matchMoveWalletRepository.save(matchMoveWallet);
						count++;
					}
				}
			}
			walletResponse.setDetails("Done");
			walletResponse.setMessage(String.valueOf(count));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<ReconBalanceDTO>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/DebitToPrefund", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> debitToPrefund(HttpServletRequest request, HttpServletResponse response) {
		ReconBalanceDTO walletResponse = new ReconBalanceDTO();
		try {
			int count = 0;
			System.err.println("Remote address::" + request.getRemoteAddr());
			List<MatchMoveWallet> listWallet = matchMoveWalletRepository.getListOfWallet();
			if (listWallet != null && !listWallet.isEmpty()) {
				for (MatchMoveWallet matchMoveWallet : listWallet) {
					if (matchMoveWallet.getMmUserId() != null) {
						double balance = matchMoveApi.getBalanceAppWallet(matchMoveWallet.getUser());
						if (balance > 0) {
							UserKycResponse userResponse = matchMoveApi.debitFromWalletToPool(matchMoveWallet,
									balance + "", "Debit to prefund");
							if (userResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								count++;
							}
						}
					}
				}
			}
			walletResponse.setDetails("Done");
			walletResponse.setMessage(String.valueOf(count));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<ReconBalanceDTO>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/DebitToPrefund/{username}", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> debitToPrefund(@PathVariable(value = "username") String username,
			HttpServletRequest request, HttpServletResponse response) {
		ReconBalanceDTO walletResponse = new ReconBalanceDTO();
		try {
			int count = 0;
			System.err.println("Remote address::" + request.getRemoteAddr());
			MUser user = userRespository.findByUsername(username);
			if (user != null) {
				MatchMoveWallet matchMoveWallet = matchMoveWalletRepository.findByUser(user);
				if (matchMoveWallet != null) {
					double balance = matchMoveApi.getBalanceAppWallet(matchMoveWallet.getUser());
					System.err.println("The balance is:::" + balance);
					if (balance > 0) {
						UserKycResponse userResponse = matchMoveApi.debitFromWalletToPool(matchMoveWallet, balance + "",
								"Debit from prefund");
						if (userResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							count++;
						}
					}
				}
			}
			walletResponse.setDetails("Done");
			walletResponse.setMessage(String.valueOf(count));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<ReconBalanceDTO>(walletResponse, HttpStatus.OK);
	}

	public static void main(String[] args) {
		try {
			String sessionId = "C5609EF1845F0F6C59A31219FA42945B";
			String amount = "699";
			JSONObject ob = new JSONObject();
			ob.put("sessionId", sessionId);
			ob.put("amount", amount);
			String encrypt = AESEncryption.encrypt(ob.toString());
			System.out.println("encrypt:: " + encrypt);

			Calendar now = Calendar.getInstance();
			System.out.println("calender:: " + now);

			System.err.println("date today and time " + now.get(Calendar.YEAR) + " next "
					+ (now.get(Calendar.MONTH) + 1) + " next " + now.get(Calendar.DATE));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Custom method for moving funds from wallet to card
	 * 
	 * @param customAdminRequest
	 * @param request
	 * @param response
	 * @return
	 */

	@RequestMapping(value = "/MoveWalletToCard", method = RequestMethod.POST)
	public ResponseEntity<CustomAdminResponse> moveWalletToCard(@RequestBody CustomAdminRequest customAdminRequest,
			HttpServletRequest request, HttpServletResponse response) {
		CustomAdminResponse customResponse = new CustomAdminResponse();
		try {
			MUser mUser = userApi.findByUserName(customAdminRequest.getMobile());
			if (mUser != null) {
				WalletResponse moveToWalletResponse = matchMoveApi.moveToCardFromWallet(mUser,
						customAdminRequest.getAmount(), customAdminRequest.getCardId());
				customResponse.setDetails(moveToWalletResponse);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return new ResponseEntity<CustomAdminResponse>(customResponse, HttpStatus.OK);
	}

	/**
	 * Custom method to acknowledge send money transfer
	 * 
	 * @param role
	 * @param device
	 * @param language
	 * @param debit
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/CustomAcknowledgeTransfer", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CustomAdminResponse> customSendMoneyAcknowledgement(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CustomAdminRequest debit, HttpServletRequest request, HttpServletResponse response) {
		CustomAdminResponse customResp = new CustomAdminResponse();
		if (role.equalsIgnoreCase("User")) {
			MUser receiver = userApi.findByUserName(debit.getReceiver());
			WalletResponse walletResponse = matchMoveApi.acknowledgeFundTransfer(debit.getAmount(), debit.getRefNo(),
					debit.getReceiver());
			walletResponse = matchMoveApi.transferFundsToMMCard(receiver, debit.getAmount(), debit.getCardId());

			customResp.setCode(ResponseStatus.FAILURE.getValue());
			customResp.setDetails(walletResponse);
		}

		return new ResponseEntity<>(customResp, HttpStatus.OK);
	}

	@RequestMapping(value = "/RefundToWalletCustomAdmin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CustomAdminResponse> refundToWalletCustomAdmin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CustomAdminRequest debit, HttpServletRequest request, HttpServletResponse response) {
		CustomAdminResponse customResp = new CustomAdminResponse();
		if (role.equalsIgnoreCase("Admin")) {
			MUser mUser = userApi.findByUserName(debit.getMobile());
			if (mUser != null) {
				MTransaction originalTrx = transactionApi.getTransactionByRefNo(debit.getOriginalTrxId());
				if (originalTrx != null) {
					WalletResponse walletResponse = matchMoveApi.refundToWalletFromCustomAdmin(mUser, debit.getAmount(),
							debit.getServiceCode(), debit.getOriginalTrxId(), request.getRemoteAddr());
					if (walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
						walletResponse = matchMoveApi.transferFundsToMMCard(mUser, debit.getAmount(),
								debit.getCardId());
						customResp.setDetails(walletResponse);
					}
				}
			}
		}
		return new ResponseEntity<CustomAdminResponse>(customResp, HttpStatus.OK);
	}

}
