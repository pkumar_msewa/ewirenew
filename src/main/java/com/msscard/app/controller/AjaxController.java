package com.msscard.app.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.ekyc.EKYCRequestDTO;
import com.cashiercards.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.PhysicalCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.EKYCDetails;
import com.msscard.entity.MCommission;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PromoCode;
import com.msscard.entity.PromoCodeRequest;
import com.msscard.entity.UserSession;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.TransactionValidationError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.KYCDetailsRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.PromoCodeRepository;
import com.msscard.repositories.PromoCodeRequestRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.MatchMoveUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.TransactionTypeValidation;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}")
public class AjaxController {

	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final MMCardRepository mMCardsRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final TransactionTypeValidation transactionTypeValidation;
	private final IMatchMoveApi matchMoveApi;
	private final ISMSSenderApi senderApi;
	private final DataConfigRepository dataConfigRepository;
	private final KYCDetailsRepository kycDetailsRepository;
	private final PromoCodeRepository promoCodeRepository;
	private final PromoCodeRequestRepository promoCodeRequestRepository;
	private final MPQAccountDetailRepository accountDetailRepository;
	private final MPQAccountTypeRepository accountTypeRepository;
	private final MKycRepository mKycRepository;
	private final MServiceRepository mServiceRepository;
	private final MCommissionRepository mCommissionRepository;
	
	DateFormat  formatter1 = new SimpleDateFormat("MM-dd-yyyy");
	private final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

	public AjaxController(UserSessionRepository userSessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry, MMCardRepository mMCardsRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository,
			MatchMoveWalletRepository matchMoveWalletRepository, TransactionTypeValidation transactionTypeValidation,
			IMatchMoveApi matchMoveApi, ISMSSenderApi senderApi, DataConfigRepository dataConfigRepository,
			KYCDetailsRepository kycDetailsRepository, PromoCodeRepository promoCodeRepository,
			PromoCodeRequestRepository promoCodeRequestRepository, MPQAccountDetailRepository accountDetailRepository,
			MPQAccountTypeRepository accountTypeRepository, MKycRepository mKycRepository,
			MServiceRepository mServiceRepository, MCommissionRepository mCommissionRepository) {
		super();
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.mMCardsRepository = mMCardsRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.transactionTypeValidation = transactionTypeValidation;
		this.matchMoveApi = matchMoveApi;
		this.senderApi = senderApi;
		this.dataConfigRepository = dataConfigRepository;
		this.kycDetailsRepository = kycDetailsRepository;
		this.promoCodeRepository = promoCodeRepository;
		this.promoCodeRequestRepository = promoCodeRequestRepository;
		this.accountDetailRepository = accountDetailRepository;
		this.accountTypeRepository = accountTypeRepository;
		this.mKycRepository = mKycRepository;
		this.mServiceRepository = mServiceRepository;
		this.mCommissionRepository = mCommissionRepository;
	}

	@RequestMapping(value = "/GetUserDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						MUser activeUser = userApi.findByUserName(user.getUsername());

						MMCards cards = mMCardsRepository.getVirtualCardsByCardUser(activeUser);
						if (cards != null) {
							if (cards.getStatus().equals("Inactive")) {
								result.setVirtualCardBlock("Inactive");
								result.setVirtualIsBlock(true);
							} else {
								result.setVirtualCardBlock("Active");
								result.setVirtualIsBlock(false);
							}

							result.setHasVirtualCard(true);
							MatchMoveCreateCardRequest createCard = new MatchMoveCreateCardRequest();
							createCard.setEmail(user.getEmail());
							createCard.setCardId(cards.getCardId());
							try {
								createCard.setPassword(SecurityUtil.md5(user.getUsername()));
								createCard.setUsername(user.getUsername());
							} catch (Exception e) {
								e.printStackTrace();
							}
							WalletResponse resp = matchMoveApi.inquireCard(createCard);
							result.setCardDetails(resp);
							result.setHasVirtualCard(true);
						} else {
							result.setHasVirtualCard(false);
						}
						MMCards physicalCards = mMCardsRepository.getPhysicalCardByUser(activeUser);
						if (physicalCards != null) {
							if (physicalCards.isBlocked()) {
								result.setPhysicalCardBlock("Inactive");
								result.setPhysicalIsBlock(true);
							} else {
								result.setPhysicalCardBlock("Active");
								result.setPhysicalIsBlock(false);
							}
							result.setHasPhysicalCard(true);
							result.setPhysicalCardStatus(physicalCards.getStatus());
						} else {
							result.setHasPhysicalCard(false);
						}
						try {
							if (activeUser.getGroupDetails() == null
									|| activeUser.getGroupDetails().getGroupName() == null) {
								result.setGroupName("NONE");
							} else {
								if (activeUser.getUserDetail().getGroupStatus().equals(Status.Active.getValue())) {
									result.setGroupName(activeUser.getGroupDetails().getDname());
								} else {
									result.setGroupName("NONE");
								}
								result.setImage(activeUser.getGroupDetails().getImage());
								result.setEncodedImage(
										activeUser.getGroupDetails().getImageContent() != null ? Base64.getEncoder()
												.encodeToString(activeUser.getGroupDetails().getImageContent()) : "");
							}
						} catch (Exception e) {
							result.setGroupName("NONE");
							e.printStackTrace();
						}
						MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(activeUser);
						if (wallet != null) {
							PhysicalCardDetails cardDetails = physicalCardDetailRepository.findByWallet(wallet);
							if (cardDetails != null) {
								result.setHasPhysicalRequest(true);
							} else {
								result.setHasPhysicalRequest(false);
							}
						}
						MKycDetail kycDetails = mKycRepository.findByUser(activeUser);
						if (kycDetails != null) {
							if (kycDetails.isRejectionStatus() == true) {
								result.setKycRequest(false);
							} else {
								result.setKycRequest(true);
							}

						}
						MService service = mServiceRepository.findServiceByCode("LMS");
						if (service != null) {
							MCommission comm = mCommissionRepository.findCommissionByService(service);
							result.setLoadMoneyComm(String.valueOf(comm.getValue()));

						}
						MUserDetails userDetails = activeUser.getUserDetail();
						if (userDetails != null) {
							String mpin = userDetails.getMpin();
							if (mpin != null) {
								result.setHasMpin(true);
							}
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User details received.");
						detail.put("userDetail", user);
						MPQAccountDetails account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f", account.getBalance())));
						detail.put("accountDetail", account);
						result.setDetails(detail);
						DataConfig dataConfig = dataConfigRepository.findDatas();
						result.setMinimumCardBalance(dataConfig.getMinimumCardBalance());
						result.setCardFees(dataConfig.getCardFees());
						result.setCardBaseFare(dataConfig.getCardBaseFare());

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Agent")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						MUser activeUser = userApi.findByUserName(user.getUsername());

						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User details received.");
						detail.put("userDetail", user);
						MPQAccountDetails account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f", account.getBalance())));
						detail.put("accountDetail", account);
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized Agent.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Admin")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						MUser activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Admin details received.");
						detail.put("userDetail", user);
						detail.put("accountDetail", activeUser.getAccountDetail());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						MUser activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant details received.");
						detail.put("userDetail", user);
						MPQAccountDetails account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f", account.getBalance())));
						detail.put("accountDetail", account);
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			} else if (role.equalsIgnoreCase("Donatee")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.DONATEE)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						MUser activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant details received.");
						detail.put("userDetail", user);
						detail.put("accountDetail", activeUser.getAccountDetail());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			}

			else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

/**
 * 	CREATE PHYSICAL CARD REQUEST
 **/
	
	@RequestMapping(value = "/PhysicalCardRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> savePhysicalCardRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PhysicalCardRequest cardRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						TransactionValidationError error=transactionTypeValidation.validateTransactionType(userSession.getUser());
						if(error.getCode().equalsIgnoreCase(ResponseStatus.PHYSICAL_CARD_REQUEST_EXIST.getValue())){
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(error.getMessage());
							return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
						}
						if(error.getCode().equalsIgnoreCase(ResponseStatus.WALLET_EXISTS.getValue())){
							MatchMoveWallet wallet=matchMoveWalletRepository.findByUser(userSession.getUser());
							PhysicalCardDetails physicalCard=new PhysicalCardDetails();
							physicalCard.setAddress1(cardRequest.getAddress1());
							physicalCard.setAddress2(cardRequest.getAddress2());
							physicalCard.setPin(cardRequest.getPinCode());
							physicalCard.setCity(cardRequest.getCity());
							physicalCard.setState(cardRequest.getState());
							physicalCard.setWallet(wallet);
							physicalCard.setStatus(Status.Requested);
							physicalCardDetailRepository.save(physicalCard);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Your Request is being processed..");
							senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.PHYSICAL_CARD_REQUEST, userSession.getUser(), null);

						}else if(error.getCode().equalsIgnoreCase(ResponseStatus.PHYSICAL_CARD_EXIST.getValue())){
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(error.getMessage());
						}else{
							ResponseDTO respDTO=matchMoveApi.createUserOnMM(userSession.getUser());
							if(respDTO.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
								MatchMoveWallet wallet=matchMoveWalletRepository.findByUser(userSession.getUser());
								PhysicalCardDetails physicalCard=new PhysicalCardDetails();
								physicalCard.setAddress1(cardRequest.getAddress1());
								physicalCard.setAddress2(cardRequest.getAddress2());
								physicalCard.setPin(cardRequest.getPinCode());
								physicalCard.setCity(cardRequest.getCity());
								physicalCard.setState(cardRequest.getState());
								physicalCard.setStatus(Status.Requested);
								physicalCard.setWallet(wallet);
								physicalCardDetailRepository.save(physicalCard);
								result.setCode(ResponseStatus.SUCCESS.getValue());
								result.setMessage("Your Request is being processed..");
								senderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplete.PHYSICAL_CARD_REQUEST, userSession.getUser(), null);

							}else{
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Your Request Has been Declined Please contact Admin");
							}
							
						}
					}else{
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
					}
				}else{
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
		}
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/PhysicalCardRequestWeb", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> savePhysicalCardRequestWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PhysicalCardRequest cardRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,HttpSession session) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId=session.getId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						DataConfig config=dataConfigRepository.findDatas();
						double validBalance=Double.valueOf(config.getMinimumCardBalance());
						double balance=matchMoveApi.getBalance(userSession.getUser());
						if(balance>=validBalance){
						TransactionValidationError error=transactionTypeValidation.validateTransactionType(userSession.getUser());
						if(error.getCode().equalsIgnoreCase(ResponseStatus.PHYSICAL_CARD_REQUEST_EXIST.getValue())){
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(error.getMessage());
							return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
						}
						if(error.getCode().equalsIgnoreCase(ResponseStatus.WALLET_EXISTS.getValue())){

							UserKycResponse walletResponse=new UserKycResponse(); 
							MMCards cards=mMCardsRepository.getVirtualCardsByCardUser(userSession.getUser());
							if(cards!=null){
								if(cards.getStatus().equalsIgnoreCase("Active")){	
							String cardId=cards.getCardId();
							walletResponse=matchMoveApi.debitFromCard(userSession.getUser().getUserDetail().getEmail(), userSession.getUser().getUsername(), cardId, config.getCardFees(),  "Physical card charge");
							if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())){
							MatchMoveWallet wallet=matchMoveWalletRepository.findByUser(userSession.getUser());
							PhysicalCardDetails physicalCard=new PhysicalCardDetails();
							physicalCard.setAddress1(cardRequest.getAddress1());
							physicalCard.setAddress2(cardRequest.getAddress2());
							physicalCard.setPin(cardRequest.getPinCode());
							physicalCard.setCity(cardRequest.getCity());
							physicalCard.setState(cardRequest.getState());
							physicalCard.setStatus(Status.Requested);
							physicalCard.setWallet(wallet);
							physicalCardDetailRepository.save(physicalCard);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Your Request is being processed..");}
							else{
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Failed,Please try again later.");
							}
							}else{
								walletResponse.setCode(ResponseStatus.FAILURE.getValue());
								walletResponse.setMessage("Card status Inactive");
							}
								}else{
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage("Card not found");
							}
						}else if(error.getCode().equalsIgnoreCase(ResponseStatus.PHYSICAL_CARD_EXIST.getValue())){
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(error.getMessage());
						}else{
							ResponseDTO respDTO=matchMoveApi.createUserOnMM(userSession.getUser());
							if(respDTO.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){

								UserKycResponse walletResponse=new UserKycResponse(); 
								MMCards cards=mMCardsRepository.getVirtualCardsByCardUser(userSession.getUser());
								if(cards!=null){
									if(cards.getStatus().equalsIgnoreCase("Active")){	
								String cardId=cards.getCardId();
								walletResponse=matchMoveApi.debitFromCard(userSession.getUser().getUserDetail().getEmail(), userSession.getUser().getUsername(), cardId,MatchMoveUtil.MATCHMOVE_PHYSICALCARD_FEE, "Physical card fees");
								if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())){
								MatchMoveWallet wallet=matchMoveWalletRepository.findByUser(userSession.getUser());
								PhysicalCardDetails physicalCard=new PhysicalCardDetails();
								physicalCard.setAddress1(cardRequest.getAddress1());
								physicalCard.setAddress2(cardRequest.getAddress2());
								physicalCard.setPin(cardRequest.getPinCode());
								physicalCard.setCity(cardRequest.getCity());
								physicalCard.setState(cardRequest.getState());
								physicalCard.setStatus(Status.Requested);
								physicalCard.setWallet(wallet);
								physicalCardDetailRepository.save(physicalCard);
								result.setCode(ResponseStatus.SUCCESS.getValue());
								result.setMessage("Your Request is being processed..");}
								else{
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage("Failed,Please try again later.");
								}
								}else{
									walletResponse.setCode(ResponseStatus.FAILURE.getValue());
									walletResponse.setMessage("Card status Inactive");
								}
									}else{
										walletResponse.setCode(ResponseStatus.FAILURE.getValue());
										walletResponse.setMessage("Card not found");
								}

							}else{
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Your Request Has been Declined Please contact Admin");
							}
							
						}}else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Insufficient balance you must have minimum balance of Rs."+config.getMinimumCardBalance()+" in wallet, Please add money.");
						}
					}else{
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
					}
				}else{
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
		}
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SubmitKYC", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveKycData(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EKYCRequestDTO cardRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					try{	
						EKYCDetails kycDetails=new EKYCDetails();
						kycDetails.setAddress1(cardRequest.getAddress1());
						kycDetails.setAddress2(cardRequest.getAddress2());
						kycDetails.setDob(cardRequest.getDob());
						kycDetails.setName(cardRequest.getName());
						kycDetails.setTransactionId(cardRequest.getTrasactionId());
						kycDetails.setUser(userSession.getUser());
						kycDetails=kycDetailsRepository.save(kycDetails);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setMessage("KYC details saved successfully..");
						MPQAccountDetails accountDetails=userSession.getUser().getAccountDetail();
						MPQAccountType acccType=accountTypeRepository.findByCode("KYC");
						accountDetails.setAccountType(acccType);
						accountDetailRepository.save(accountDetails);
						
					}catch(Exception e){
						e.printStackTrace();
						result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
						result.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
					}
						
					}else{
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
					}
				}else{
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
		}
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}

	@RequestMapping(value = "/requestPromo", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> requestPromo(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PromoCodeDTO cardRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(cardRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = cardRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					try{	
						Date date1= new Date();
						String vDateString= date.format(date1);
						 Date vDate = formatter1.parse(vDateString);
						PromoCode code=promoCodeRepository.fetchByPromoCode(cardRequest.getPromoCode());
												
						if(code!=null){
							//Double amonut  =mTransactionRepository.findByCreatedAndUser(userSession.getUser().getAccountDetail(),code.getStartDate(),code.getEndDate());	
							
							//if(amonut>=code.getMinTrxAmount()){
							if(vDate.before(code.getEndDate())){
							MUser usr=userApi.findByUserName(user.getUsername());
							PromoCodeRequest codeRequest= promoCodeRequestRepository.findByPromoCodeAndUSer(cardRequest.getPromoCode(),usr);
							if(codeRequest == null){
								MMCards phyCard=mMCardsRepository.getPhysicalCardByUser(usr);
								if(phyCard!=null){
							PromoCodeRequest req= new PromoCodeRequest();
							req.setPromocode(cardRequest.getPromoCode());
							req.setStatus(Status.Requested);
							req.setUser(usr);
							promoCodeRequestRepository.save(req);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("your request has been received, Balance will be updated soon");
								}else{
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("This promocode is only applicable on Physical cards");
								}
							}else{
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("This promo code is already reedemed.");
							}
							}else{
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Invalid promo code");
							}
						/*else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("This promocode is not applicable.");
						}*/
						/*else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("This promocode is not applicable.");
						}*/	
						}else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Invalid promo code");
						}
					}catch(Exception e){
						e.printStackTrace();
						result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
						result.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
					}
						
					}else{
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
					}
				}else{
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
		}
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}
	
	
	
}
