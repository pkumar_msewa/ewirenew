package com.msscard.app.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.cashiercards.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MCommission;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.LoginValidation;
import com.msscards.session.SessionLoggingStrategy;
@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class LoginAPIController {

	private final LoginValidation loginValidation;
	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final MMCardRepository mMCardRepository;
	private final IMatchMoveApi matchMoveApi;
	private final PhysicalCardDetailRepository physicalCardRequestRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final DataConfigRepository dataConfigRepository;
	private final MKycRepository mKycRepository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MServiceRepository mServiceRepository;
	private final MCommissionRepository mCommissionRepository;

	
	public LoginAPIController(LoginValidation loginValidation, IUserApi userApi,
			AuthenticationManager authenticationManager, UserSessionRepository userSessionRepository,
			SessionLoggingStrategy sessionLoggingStrategy, ISessionApi sessionApi,
			MMCardRepository mMCardRepository, IMatchMoveApi matchMoveApi
			,PhysicalCardDetailRepository physicalCardRequestRepository,
			MatchMoveWalletRepository matchMoveWalletRepository,DataConfigRepository dataConfigRepository,
			MKycRepository mKycRepository,MUserDetailRepository mUserDetailRepository,MServiceRepository mServiceRepository,MCommissionRepository mCommissionRepository) {
		super();
		this.loginValidation = loginValidation;
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.mMCardRepository=mMCardRepository;
		this.matchMoveApi=matchMoveApi;
		this.physicalCardRequestRepository=physicalCardRequestRepository;
		this.matchMoveWalletRepository=matchMoveWalletRepository;
		this.dataConfigRepository=dataConfigRepository;
		this.mKycRepository=mKycRepository;
		this.mUserDetailRepository=mUserDetailRepository;
		this.mServiceRepository=mServiceRepository;
		this.mCommissionRepository=mCommissionRepository;
		
	}

	@RequestMapping(value = "/Login/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> userLogin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody LoginDTO login, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		ResponseDTO result = new ResponseDTO();
		LoginError error = loginValidation.checkLoginValidation(login);
		if (error.isSuccess()) {
				try {
					MUser user = userApi.findByUserName(login.getUsername());
					if (user != null) {
						if(user.isFullyFilled()){
						if (user.getAuthority().contains(Authorities.BLOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is blocked");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else if (user.getAuthority().contains(Authorities.LOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is locked,contact Customer-Care");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else if (user.getAuthority().contains(Authorities.SPECIAL_USER)) {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else if (role.equalsIgnoreCase("User")) {
							if (user.getAuthority().contains(Authorities.USER)) {
								if (!login.isValidate()) {
                                    AuthenticationError authError = isValidUsernamePassword(login,request);
                                    if(authError.isSuccess()) {
									boolean validDevice=true;
									boolean wallet=false;
                                    if (validDevice) {
                                        if (sessionApi.checkActiveSession(user)) {
                                            AuthenticationError auth = authentication(login,request);
                                            if (auth.isSuccess()) {
                                            	 MatchMoveWallet mmWallets= matchMoveWalletRepository.findByUser(user);
                                            	 if(mmWallets != null) {
                                            		 wallet=true;
                                            	 }else{
                                            		 try{
                                            		 MatchMoveCreateCardRequest cd=new MatchMoveCreateCardRequest();
                                            		 cd.setEmail(user.getUserDetail().getEmail());
                                            		 cd.setMobile(user.getUsername());
                                            		 cd.setUsername(user.getUsername());
                                            	
                                            		UserKycResponse userResponse= matchMoveApi.getUsers(user.getUserDetail().getEmail(),user.getUsername());
                                                	WalletResponse walletResp=	matchMoveApi.fetchWalletMM(cd);

                                            		if(userResponse!=null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userResponse.getCode())){
                                            			if(walletResp!=null && ResponseStatus.FAILURE.getValue().equalsIgnoreCase(walletResp.getCode())){
                                            				MatchMoveCreateCardRequest cardReq=new MatchMoveCreateCardRequest();
                                            				cardReq.setEmail(user.getUserDetail().getEmail());
                                            				cardReq.setMobile(user.getUsername());
                                            				cardReq.setPassword(SecurityUtil.md5(user.getUsername()));
                                            				WalletResponse createWall=matchMoveApi.createWallet(cardReq);
                                            				if(createWall!=null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(createWall.getCode())){
                                            					MatchMoveWallet createWallet=new MatchMoveWallet();
                                            					createWallet.setMmUserId(userResponse.getMmUserId());
                                            					createWallet.setUser(user);
                                            					createWallet.setWalletId(walletResp.getWalletId());
                                            					createWallet.setWalletNumber(walletResp.getWalletNumber());
                                            					matchMoveWalletRepository.save(createWallet);
                                            					wallet=true;
                                            				}else{
                                            					wallet=false;
                                            				}
                                            			}else{
                                            				if(walletResp!=null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResp.getCode())){
                                            					MatchMoveWallet createWallet=new MatchMoveWallet();
                                            					createWallet.setMmUserId(userResponse.getMmUserId());
                                            					createWallet.setUser(user);
                                            					createWallet.setWalletId(walletResp.getWalletId());
                                            					createWallet.setWalletNumber(walletResp.getWalletNumber());
                                            					matchMoveWalletRepository.save(createWallet);
                                            					wallet=true;
                                            				}
                                            			}
                                            		}else{
                                            			wallet=false;
                                            		}
                                            	 }catch(Exception e){
                                            		 e.printStackTrace();
                                            		 wallet=false;
                                            	 }
                                            		 }
                                            	 if(wallet){
                                                try {
                                                    String gcmId = login.getRegistrationId();
                                                    if (gcmId != null) {
                                                        userApi.updateGcmId(gcmId, user.getUsername());
                                                        MUserDetails userDetails=user.getUserDetail();
                                                        userDetails.setLastLoginDevice(device);
                                                        mUserDetailRepository.save(userDetails);
                                                    }
                                                } catch (NullPointerException e) {
                                                    e.printStackTrace();
                                                }
                                                user.setMobileToken(CommonUtil.generateNDigitNumericString(6));
                                                Map<String, Object> detail = new HashMap<String, Object>();
                                                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                                                sessionLoggingStrategy.onAuthentication(authentication, request, response);
                                                UserSession userSession = userSessionRepository.findByActiveSessionId(
                                                        RequestContextHolder.currentRequestAttributes().getSessionId());
                                                UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
                                                result.setStatus(ResponseStatus.SUCCESS);
                                                result.setMessage("Login successful.");
                                                detail.put("sessionId", userSession.getSessionId());
                                                detail.put("userDetail", activeUser);
                                                MPQAccountDetails account = user.getAccountDetail();
                                                account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
                                                detail.put("accountDetail", account);
                                                result.setDetails(detail);
	                                               MMCards cards= mMCardRepository.getVirtualCardsByCardUser(user);
		                                              if(cards!=null){
		                                            	
		                                            	  MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		                                            	  cardRequest.setEmail(user.getUserDetail().getEmail());
		                                            	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
		                                            	  cardRequest.setCardId(cards.getCardId());
		                                            	  cardRequest.setUsername(user.getUsername());
		                                            	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);
		                                            	 if(Status.Inactive.getValue().equals(cards.getStatus())) {
		                                            		  result.setVirtualCardBlock(Status.Inactive.getValue()); 
		                                            		  result.setVirtualIsBlock(true);
		                                            	 } else {
		                                            		 result.setVirtualCardBlock(Status.Active.getValue());
		                                            		 result.setVirtualIsBlock(false);
		                                            	 }
		                                            	  result.setHasVirtualCard(true);
		                                            	  result.setCardDetails(walletResponse);
		                                              }else{
		                                            	  result.setHasVirtualCard(false);
		                                              }
		                                              MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
		                                              	if(physicalCards!=null){
		                                              		if(physicalCards.isBlocked()) {
		                                              			result.setPhysicalCardBlock(Status.Inactive.getValue());
		                                              			result.setPhysicalIsBlock(true);
		                                              		} else {
		                                              			result.setPhysicalCardBlock(Status.Active.getValue());
		                                              			result.setPhysicalIsBlock(false);
		                                              		}
		                                              		result.setHasPhysicalCard(true);
		                                              		result.setPhysicalCardStatus(physicalCards.getStatus());
		                                              	}else{
		                                              		result.setHasPhysicalCard(false);
		                                              	}
		                                              	
		                                             MatchMoveWallet mmWallet= matchMoveWalletRepository.findByUser(user);		                                             
		                                             if(mmWallet!=null){
		                                            	 PhysicalCardDetails phyReq= physicalCardRequestRepository.findByWallet(mmWallet);
		                                            	 if(phyReq!=null){
		                                            		 result.setHasPhysicalRequest(true);
		                                            	 }
		                                            	 if(mmWallet.getMmUserId() == null) {
		                                            		 UserKycResponse walletResponse = matchMoveApi.getUsers(user.getUserDetail().getEmail(), user.getUserDetail().getContactNo());
		                                            		 mmWallet.setMmUserId(walletResponse.getMmUserId());
		                                            		 matchMoveWalletRepository.save(mmWallet);
		                                            	 }
		                                            } else {
		                                            	MatchMoveCreateCardRequest req = new MatchMoveCreateCardRequest();
		                                            	req.setEmail(user.getUserDetail().getEmail());
		                                            	req.setPassword(SecurityUtil.md5(user.getUserDetail().getContactNo()));
		                                            }
		                                             DataConfig dataConfig=dataConfigRepository.findDatas();
		                     						result.setMinimumCardBalance(dataConfig.getMinimumCardBalance());
		                     						result.setCardFees(dataConfig.getCardFees());
		                     						result.setCardBaseFare(dataConfig.getCardBaseFare());
		                     						MKycDetail kyc=mKycRepository.findByUser(user);
		                     						if(kyc!=null){
		                     							if(kyc.isRejectionStatus()==true){
		                     						result.setKycRequest(false);
		                     							}else{
		                     								result.setKycRequest(true);
		                     							}
		                     						}
		                     						MService service= mServiceRepository.findServiceByCode("LMS");
	                     		                      if(service!=null){
	                     		                    	  MCommission comm=mCommissionRepository.findCommissionByService(service);
	                     		                        	result.setLoadMoneyComm(String.valueOf(comm.getValue()));

	                     		                      }

                                                return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                            }  else{
                                            	result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
                                              	 result.setStatus(ResponseStatus.WALLET_NOT_CREATED.getKey());
                                                   result.setMessage(ResponseStatus.WALLET_NOT_CREATED.getKey());
                                               	return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                            }
                                        }
                                        } else {
                                        	result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
                                       	 result.setStatus(ResponseStatus.WALLET_NOT_CREATED.getKey());
                                            result.setMessage(ResponseStatus.WALLET_NOT_CREATED.getKey());
                                        	return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                        }
                                    }else {
                                        userApi.requestNewLoginDevice(user);
                                        result.setStatus(ResponseStatus.NEW_DEVICE);
                                        result.setMessage("OTP sent to :" + user.getUsername());
                                       
                                        MMCards cards= mMCardRepository.getVirtualCardsByCardUser(user);
                                        if(cards!=null){
                                        	MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
                                      	  cardRequest.setEmail(user.getUserDetail().getEmail());
                                      	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
                                      	  cardRequest.setCardId(cards.getCardId());
                                      	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);
                                   	  result.setHasVirtualCard(true);
                                   	  result.setCardDetails(walletResponse);

                                        }else{
                                      	  result.setHasVirtualCard(false);
                                        }
                                        MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
                                        	if(physicalCards!=null){
                                        		result.setHasPhysicalCard(true);
                                        	}else{
                                        		result.setHasPhysicalCard(false);
                                        	}
                                        	DataConfig dataConfig=dataConfigRepository.findDatas();
                    						result.setMinimumCardBalance(dataConfig.getMinimumCardBalance());
                    						result.setCardFees(dataConfig.getCardFees());
                    						
                                        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                      }
                                    }else {
                                        result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
                                        result.setMessage(authError.getMessage());
                                    }
								}else {
									if(true) {
										if (sessionApi.checkActiveSession(user)) {
											AuthenticationError auth = authentication(login,request);
											if (auth.isSuccess()) {
												try {
													String gcmId = login.getRegistrationId();
													if (gcmId != null) {
														userApi.updateGcmId(gcmId, user.getUsername());
													}
												} catch (NullPointerException e) {

												}
												Map<String, Object> detail = new HashMap<String, Object>();
												Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
												sessionLoggingStrategy.onAuthentication(authentication, request, response);
												UserSession userSession = userSessionRepository.findByActiveSessionId(
														RequestContextHolder.currentRequestAttributes().getSessionId());
												UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
												result.setStatus(ResponseStatus.SUCCESS);
												result.setMessage("Login successful.");
												detail.put("sessionId", userSession.getSessionId());
												detail.put("userDetail", activeUser);
												MPQAccountDetails account = user.getAccountDetail();
												account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
												detail.put("accountDetail", account);
												result.setDetails(detail);
												
	                                               MMCards cards= mMCardRepository.getVirtualCardsByCardUser(user);
		                                              if(cards!=null){
		                                            	  
		                                            	  MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		                                            	  cardRequest.setEmail(user.getUserDetail().getEmail());
		                                            	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
		                                            	  cardRequest.setCardId(cards.getCardId());
		                                            	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);
		                                            	  result.setHasVirtualCard(true);
		                                            	  result.setCardDetails(walletResponse);

		                                              }else{
		                                            	  result.setHasVirtualCard(false);
		                                              }
		                                              MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
		                                              	if(physicalCards!=null){
		                                              		result.setHasPhysicalCard(true);
		                                              	}else{
		                                              		result.setHasPhysicalCard(false);
		                                              	}
		                                            
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											} else {
												result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
												result.setMessage(auth.getMessage());
												result.setDetails(null);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}
										}
									}
								}
							}
						}else if (role.equalsIgnoreCase("Corporate")) {
						if (user.getAuthority().contains(Authorities.CORPORATE)) {
								if (login.isValidate()) {
								LoginError loginError = loginValidation.checkSuperAdminValidation(login, user);
								if (loginError.isSuccess()) {
									AuthenticationError authError = isValidUsernamePassword(login, request);
									if (authError.isSuccess()) {
										userApi.requestNewLoginDeviceForSuperAdmin(user);
										result.setStatus(ResponseStatus.NEW_DEVICE);
										result.setMessage("OTP sent to ::" + user.getUserDetail().getContactNo());
									} else {
										result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
										result.setMessage(authError.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(loginError.getMessage());
								}
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								if (true) {
									if (sessionApi.checkActiveSession(user)) {
										AuthenticationError auth = authentication(login, request);
										if (auth.isSuccess()) {
											Map<String, Object> detail = new HashMap<String, Object>();
											Authentication authentication = SecurityContextHolder.getContext()
													.getAuthentication();
											sessionLoggingStrategy.onAuthentication(authentication, request, response);
											UserSession userSession = userSessionRepository.findByActiveSessionId(
													RequestContextHolder.currentRequestAttributes().getSessionId());
											System.err.println("session iD ::" +RequestContextHolder.currentRequestAttributes().getSessionId());
											UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
											result.setStatus(ResponseStatus.SUCCESS);
											result.setMessage("Login successful.");
											detail.put("sessionId", userSession.getSessionId());
											detail.put("userDetail", activeUser);
											detail.put("accountDetail", user.getAccountDetail());
											result.setDetails(detail);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										} else {
											result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
											result.setMessage(auth.getMessage());
											result.setDetails(null);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}
									}
								}
							}
						}
					} else if (role.equalsIgnoreCase("Admin")) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						}
					} else if (role.equalsIgnoreCase("Merchant")) {
						System.err.println("role merchant");
						if (user.getAuthority().contains(Authorities.MERCHANT)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						}
					} 
					else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.......");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}

					}else{
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Please fill out the form completely to access your profile");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
					}
					} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your request is declined please contact customer care or try again later");
				result.setDetails("your request is declined please contact customer care or try again later");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}

		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage(error.getMessage());
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), CommonUtil.base64Decode(dto.getPassword()));
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), CommonUtil.base64Decode(dto.getPassword()));
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(30*24*60*60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	
	@RequestMapping(value = "/Logout", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> logoutUserApi(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);

		if (isValidHash) {
			try {
				UserSession userSession = userSessionRepository.findBySessionId(session.getSessionId());
				if (userSession != null) {
					sessionApi.expireSession(session.getSessionId());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("User logout successful");
					result.setDetails("Session Out");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails("Failed, invalid request.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
}
