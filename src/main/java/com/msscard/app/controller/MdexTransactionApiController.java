package com.msscard.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.TransactionInitiateResponse;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MdexRequestLogs;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UserDTO;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MdexRequestLogsRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;


@Controller
@RequestMapping(value="Api/v1/{role}/{device}/{language}/MdexTransaction")
public class MdexTransactionApiController {

	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final MdexRequestLogsRepository mdexRequestLogsRepository;
	private final MTransactionRepository mTransactionRepository;
	
	public MdexTransactionApiController(ITransactionApi transactionApi, UserSessionRepository userSessionRepositor,
			IUserApi userApi, PersistingSessionRegistry persistingSessionRegistry,

			MdexRequestLogsRepository mdexRequestLogsRepository, MTransactionRepository mTransactionRepository){
		super();
		this.transactionApi=   transactionApi;
		this.userSessionRepository= userSessionRepositor;
		this.userApi = userApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.mdexRequestLogsRepository=mdexRequestLogsRepository;
		this.mTransactionRepository= mTransactionRepository;
	
		
	}
	
	@RequestMapping(value="/InitiateMdex",method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public String initiateLoadMoney(@PathVariable(value="role") String role,@PathVariable(value="device") String device,
			@PathVariable(value="language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest,HttpServletRequest request,HttpServletResponse response,ModelMap map){
		TransactionInitiateResponse initiateResponse=new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						/*UserKycResponse resp=matchMoveApi.getConsumers();
						if(resp.getCode().equalsIgnoreCase("S00")){
						String prefundingBalance=resp.getPrefundingBalance();
						double doublePrefundingBalance=Double.parseDouble(prefundingBalance);
						double actTransactonAmt=Double.parseDouble(transactionRequest.getAmount());
					
						if(doublePrefundingBalance>=actTransactonAmt){*/
							transactionRequest.setUser(userSession.getUser());
							//credit
						MTransaction creditTransaction = transactionApi.initiateMdexTransactionCredit(transactionRequest);
						if(creditTransaction!=null){
							MTransaction debitTransaction = transactionApi.initiateMdexTransactionDebit(transactionRequest);
							if(debitTransaction!=null){
							creditTransaction.setUpiId(debitTransaction.getTransactionRefNo());
							creditTransaction.setMdexTransaction(true); 
							mTransactionRepository.save(creditTransaction);
							debitTransaction.setMdexTransaction(true);
							mTransactionRepository.save(debitTransaction);
							System.err.println("Debit Transaction:::"+debitTransaction);	
							System.err.println("Credit TRansaciton::::"+creditTransaction);
							MdexRequestLogs requestEntries = new MdexRequestLogs();
							requestEntries.setRequest(transactionRequest.getMobileNumber());
							requestEntries.setServiceCode(transactionRequest.getServiceCode());
							requestEntries.setArea(transactionRequest.getArea());
							requestEntries.setTransaction(creditTransaction);
							requestEntries.setTopUpType(transactionRequest.getTopUpType());
							requestEntries.setSenderUser(user.getUsername());
							requestEntries.setAdditionalInfo(transactionRequest.getAdditionalInfo());
							requestEntries.setAccountNumber(transactionRequest.getAccountNumber()+"");
							requestEntries.setBillGroupNumber(transactionRequest.getBillGroupNumber());
							requestEntries.setBillingUnit(transactionRequest.getBillingUnit());
							requestEntries.setDthNumber(transactionRequest.getDthNumber());
							requestEntries.setLandlineNumber(transactionRequest.getLandLineNumber());
							requestEntries.setStdCode(transactionRequest.getStdCode());
							requestEntries.setCycleNumber(transactionRequest.getCycleNumber());
							requestEntries.setCityName(transactionRequest.getCityName());
							requestEntries.setProcessingCycle(transactionRequest.getProcessingCycle());
							requestEntries.setPolicyNumber(transactionRequest.getPolicyNumber());
							requestEntries.setPolicyDate(transactionRequest.getPolicyDate());
						    mdexRequestLogsRepository.save(requestEntries);
							try{
							String info=SecurityUtil.md5(RazorPayConstants.UPI_TOKEN+"|"+String.format("%.2f", Double.parseDouble(transactionRequest.getAmount()))+"|"+RazorPayConstants.UPI_MERCHANT_ID+"|"+creditTransaction.getTransactionRefNo());
							transactionRequest.setHash(info);
							transactionRequest.setMerchant_id(RazorPayConstants.UPI_MERCHANT_ID);
							transactionRequest.setAdditionalInfo(sessionId);
							transactionRequest.setTransactionRefNo(creditTransaction.getTransactionRefNo());
							transactionRequest.setAmount(String.valueOf(transactionRequest.getAmount()));
							map.addAttribute("xxx", transactionRequest);
							return "User/UPIPay";
							}
							catch(Exception e){
								e.printStackTrace();
							}
						}
						
						}else{
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}	
					/*}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}*/
						/*}else{
						initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
						initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
					}*/
					}else{
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				}else{
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			}else{
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		}else{
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}
		
		map.addAttribute("xxx", transactionRequest);
		return "redirect:User/Home";
	}
	
	
}
