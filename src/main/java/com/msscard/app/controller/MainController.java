package com.msscard.app.controller;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.Role;
import com.msscard.util.Authorities;
import com.msscard.util.MailConstants;
import com.msscard.util.ModelMapKey;

@Controller
public class MainController implements MessageSourceAware{
	
	@SuppressWarnings("unused")
	private MessageSource messageSource;
	private final IAuthenticationApi authenticationApi;
	private VelocityEngine velocityEngine;

	public MainController(IAuthenticationApi authenticationApi,VelocityEngine velocityEngine) {
		this.authenticationApi = authenticationApi;
		this.velocityEngine=velocityEngine;
	}

	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String getHome(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, RedirectAttributes modelMap,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (msg != null) {
			modelMap.addAttribute(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			modelMap.addAttribute(ModelMapKey.ERROR, error);
		}
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/Admin/Home";
				} else if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/User/Home";
				} else if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/Agent/Home";
				} else {
					return "Home";
				}
			}
		}
		return "Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getHome(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				return "redirect:/";
			}
		}
		if (msg != null) {
			modelMap.put(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			modelMap.put(ModelMapKey.ERROR, error);
		}
		return "Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PrivacyPolicy")
	public String getPrivacy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "policy";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/TermsnConditions")
	public String getTerms(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Terms";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Faq")
	public String getFaqs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Faqs";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/GrievancePolicy")
	public String getGrievancePolicy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {		
		return "grievancepolicy";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/RefundPolicy")
	public String getRefundPolicy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {		
		return "refundpolicy";
	}

	@RequestMapping(method = RequestMethod.GET, value = "User/SignUp")
	public String getSignUp(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "User/SignUp";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "User/ForgotPassword")
	public String getForgotPassword(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "User/ForgotPassword";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "User/Login")
	public String getLogin(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "User/Login";
	}
	

	@RequestMapping(method = RequestMethod.GET, value = "/Aboutus")
	public String getAboutUs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "About";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ContactUs")
	public String getContactUs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "contactus";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Offers")
	public String getOffers(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Offers";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/faq")
	public String getFaq(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Faqs";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/off")
	public String getoff(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "off";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Demo")
	public String getDemo(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Admin/Demo";
	}

	/*@RequestMapping(method = RequestMethod.GET, value = "/Admin/Home")
	public String getAdminHome(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				return "Admin/Home";
			}
		}
		if (msg != null) {
			modelMap.put(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			modelMap.put(ModelMapKey.ERROR, error);
		}
		return "Admin/Login";
	}
*/
	@RequestMapping(method = RequestMethod.GET, value = "/About")
	public String boutUs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "About";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Fingoole")
	public String fingooledoc(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		return "Fingoole";
	}
	
	
	@RequestMapping(value="/Send/Mail",method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> upgradeAccountApp(@RequestBody RequestDTO dto,HttpServletRequest request,HttpServletResponse response,ModelMap modelmap,HttpSession session)throws MailSendException{
		ResponseDTO respons= new ResponseDTO();
		String subject="FeedBack!";
		 String mailTemplate="feedBack.vm";
		 String MAIL_TEMPLATE = "com/msscard/mail/template/";
		 Thread t = new Thread(new Runnable() {
				public void run() {
		try {
			Session sessi = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
			MimeMessage message = new MimeMessage(sessi);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
			Map<String, Object> model = new HashMap<String, Object>();
//			model.put("tpImage", MailConstants.Path+"/resources/images/tp.png");
//			model.put("btImage", MailConstants.Path+"/resources/images/bt.png");
			model.put("fName",dto.getFirstName());
			model.put("lName",dto.getLastName());
			model.put("contactNo",dto.getContactNo());
			model.put("message",dto.getMessage());
			model.put("email",dto.getEmail());
			String email = MailConstants.USER_ID;
			helper.setTo(new InternetAddress(email));
			String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
					MAIL_TEMPLATE + mailTemplate,model);
			helper.setSubject(subject);
			helper.setText(mailBody, true);
			sendEmail(message);
//			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Success);
		} catch (MessagingException ex) {
			ex.printStackTrace();
//			saveLog(subject, mailTemplate, user, MailConstants.SENDER_EMAIL_NO_REPLY, Status.Failed);
		}
			}
		 });
			t.start();	
		return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
	}
	
	public static void sendEmail(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Transport.send(message, userId, password);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
