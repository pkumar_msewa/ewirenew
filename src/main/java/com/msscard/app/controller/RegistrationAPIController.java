package com.msscard.app.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.enums.Device;
import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.ForgotPasswordDTO;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.TransactionalRequest;
import com.msscard.app.model.request.VerifyMobileDTO;
import com.msscard.app.model.response.FingooleResponse;
import com.msscard.app.model.response.FingooleTransactionListDTO;
import com.msscard.app.model.response.ForgotPasswordError;
import com.msscard.app.model.response.RegisterResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.Donation;
import com.msscard.entity.FingooleRegistration;
import com.msscard.entity.FingooleServices;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.DonationDTO;
import com.msscard.model.FingooleRegisterDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.Status;
import com.msscard.model.UserType;
import com.msscard.model.error.RegisterError;
import com.msscard.model.error.VerifyMobileOTPError;
import com.msscard.repositories.FingooleRegistrationRepository;
import com.msscard.repositories.FingooleServicesRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.MobileOTPValidation;
import com.msscard.validation.RegisterValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;

@Controller
@RequestMapping("/Api/{role}/{device}/{language}")
public class RegistrationAPIController {

	private RegisterValidation registerValidation;
	private IUserApi userApi;
	private MobileOTPValidation mobileOTPValidation;
	private MMCardRepository mMCardRepository;
	private final UserSessionRepository userSessionRepository;
	private final IMatchMoveApi matchMoveApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IMdexApi iMdexApi;
	private final ITransactionApi transactionApi;
	private final MCommissionRepository mCommissionRepository;
	private final MServiceRepository mServiceRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final FingooleServicesRepository fingooleServicesRepository;
	private final FingooleRegistrationRepository fingooleRegistrationRepository;

	public RegistrationAPIController(RegisterValidation registerValidation, IUserApi userApi,
			MobileOTPValidation mobileOTPValidation, MMCardRepository mMCardRepository,
			UserSessionRepository userSessionRepository, IMatchMoveApi matchMoveApi,
			PersistingSessionRegistry persistingSessionRegistry, IMdexApi iMdexApi, ITransactionApi transactionApi,
			MCommissionRepository mCommissionRepository, MServiceRepository mServiceRepository,
			MTransactionRepository mTransactionRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			FingooleServicesRepository fingooleServicesRepository,
			FingooleRegistrationRepository fingooleRegistrationRepository) {
		super();
		this.registerValidation = registerValidation;
		this.userApi = userApi;
		this.mobileOTPValidation = mobileOTPValidation;
		this.mMCardRepository = mMCardRepository;
		this.userSessionRepository = userSessionRepository;
		this.matchMoveApi = matchMoveApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.iMdexApi = iMdexApi;
		this.transactionApi = transactionApi;
		this.mCommissionRepository = mCommissionRepository;
		this.mServiceRepository = mServiceRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.fingooleServicesRepository = fingooleServicesRepository;
		this.fingooleRegistrationRepository = fingooleRegistrationRepository;
	}

	@RequestMapping(value = "/Register", method = RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> registerNormalUser(@RequestBody RegisterDTO dto,
			HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash) {

		RegisterResponseDTO responseDTO = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError registerError = registerValidation.validateNormalUser(dto);
				if (registerError.isValid()) {
					dto.setUserType(UserType.User);
					try {
						userApi.saveUser(dto);
						responseDTO.setStatus(ResponseStatus.SUCCESS);
						responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");

					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(ResponseStatus.FAILURE);
						responseDTO.setMessage("Opps!something is wrong please try again....");
						return new ResponseEntity<>(responseDTO, HttpStatus.OK);
					}
					responseDTO.setStatus(ResponseStatus.SUCCESS);
					responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
							+ " and  verification mail sent to ::" + dto.getEmail() + " .");
					responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
							+ " and  verification mail sent to ::" + dto.getEmail() + " .");
				} else {
					responseDTO.setStatus(ResponseStatus.FAILURE);
					responseDTO.setMessage(registerError.getMessage());
				}
			} else {
				responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				responseDTO.setMessage("Unauthorized Role..");
			}
		} else {
			responseDTO.setStatus(ResponseStatus.INVALID_HASH);
			responseDTO.setMessage("Invalid Hash..");
		}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> forgotPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotPasswordDTO forgot, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(forgot, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = forgot.getUsername();
				ForgotPasswordError error = registerValidation.forgotPassword(username);
				if (error.isValid() == true) {
					MUser u = userApi.findByUserName(username);
					if (u != null) {
						if (u.getAuthority().contains(Authorities.AUTHENTICATED)) {
							if (u.getMobileStatus() == Status.Active) {

								userApi.changePasswordRequest(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Forgot Password");
								result.setDetails("Please, Check your SMS for OTP Code to change your password.");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Your account is blocked! Please try after 24 hrs.");
							result.setDetails("Your account is blocked! Please try after 24 hrs.");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("User doesn't Exist,Please Register.");
						result.setDetails("User doesn't Exist,Please Register.");

					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error.getErrorLength());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Resend/Mobile/OTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = dto.getMobileNumber();
				VerifyMobileOTPError error = new VerifyMobileOTPError();
				error = mobileOTPValidation.validateResendOTP(username);
				if (error.isValid()) {
					userApi.resendMobileTokenNew(username);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Resend OTP");
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Mobile Number Already Verified.");
					result.setDetails(error.getOtp());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "ChangePasswordOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
				ForgotPasswordError error = registerValidation.forgotPassword(dto);
				if (error.isValid()) {
					if (userApi.changePassword(dto)) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails("Password successfully Updated");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Not a valid OTP");
						result.setDetails("Not a valid OTP");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage(error.getMessage());
					result.setDetails(error.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePasswrd(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String sessionId = dto.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		System.err.println(sessionId);
		if (userSession != null) {
			if (isValidHash) {
				if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					ForgotPasswordError error = registerValidation.changePassword(dto);
					if (error.isValid()) {
						System.err.println("hi i am herer");
						dto.setUsername(userSession.getUser().getUsername());
						if (userApi.changeProfilePassword(dto)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails("Password successfully Updated");
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Error occured while operation");
							result.setDetails("Error occured while operation");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(error.getMessage());
						result.setDetails(error.getMessage());
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Not a valid User");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid Secure Hash");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Invalid Session, Please login and try again");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangePasswordWeb", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> changePasswrdWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String sessionId = session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		System.err.println(sessionId);
		if (userSession != null) {
			if (isValidHash) {
				if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					ForgotPasswordError error = registerValidation.changePassword(dto);
					if (error.isValid()) {
						System.err.println("hi i am herer");
						dto.setUsername(userSession.getUser().getUsername());
						if (userApi.changeProfilePassword(dto)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails("Password successfully Updated");
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Error occured while operation");
							result.setDetails("Error occured while operation");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(error.getMessage());
						result.setDetails(error.getMessage());
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Not a valid User");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid Secure Hash");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Invalid Session, Please login and try again");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	private boolean verifyUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping(value = "/CustomRegister", method = RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> customRegisterNormalUser(@RequestBody RegisterDTO dto,
			HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash) {

		RegisterResponseDTO responseDTO = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				System.err.println("before validation");
				RegisterError registerError = registerValidation.customRegisterError(dto);
				System.err.println(registerError);
				if (registerError.isValid()) {
					dto.setUserType(UserType.User);
					try {
						dto.setLastLoginDevice(device);
						userApi.saveCustomUser(dto);
						responseDTO.setStatus(ResponseStatus.SUCCESS);
						responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo());
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo());

					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(ResponseStatus.FAILURE);
						responseDTO.setMessage("Opps!something is wrong please try again....");
						return new ResponseEntity<>(responseDTO, HttpStatus.OK);
					}

				} else {
					responseDTO.setStatus(ResponseStatus.FAILURE);
					responseDTO.setMessage(registerError.getMessage());
					responseDTO.setFullyFilled(registerError.isFullyFilled());
					responseDTO.setHasError(registerError.isHasError());
				}
			} else {
				responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				responseDTO.setMessage("Unauthorized Role..");
			}
		} else {
			responseDTO.setStatus(ResponseStatus.INVALID_HASH);
			responseDTO.setMessage("Invalid Hash..");
		}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/CustomActivate/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> customVerifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
				if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
					result.setMessage("Activate Mobile");
					result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Activate Mobile");
					MUser user = userApi.findByUserName(dto.getMobileNumber());
					result.setFullyFilled(user.isFullyFilled());
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Invalid Activation Key");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid Hash");
		}

		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RegisterFullDetails", method = RequestMethod.POST)
	public ResponseEntity<RegisterResponseDTO> customRegisterFullDetails(@RequestBody RegisterDTO dto,
			HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash) {

		RegisterResponseDTO responseDTO = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				System.err.println("before validation");
				RegisterError registerError = registerValidation.validateFullDetails(dto);
				System.err.println(registerError);
				if (registerError.isValid()) {
					try {
						ResponseDTO resp = new ResponseDTO();
						userApi.saveFullDetails(dto);
						MUser user = userApi.findByUserName(dto.getContactNo());
						resp = matchMoveApi.createUserOnMM(user);
						if (ResponseStatus.SUCCESS.getValue().equals(resp.getCode())) {
							MService service = mServiceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
							responseDTO.setStatus(ResponseStatus.SUCCESS.getKey());
							responseDTO.setCode(ResponseStatus.SUCCESS.getValue());
							responseDTO.setMessage("You have completed minimum KYC for doing transactions upto INR "
									+ service.getMaxAmount()
									+ ". Please upgrade your KYC with OSV to get maximum benefit of the application");
							responseDTO.setDetails("You have completed minimum KYC for doing transactions upto INR "
									+ service.getMaxAmount()
									+ ". Please upgrade your KYC with OSV to get maximum benefit of the application");
							responseDTO.setFullyFilled(true);
						} else {
							responseDTO.setCode(ResponseStatus.SUCCESS.getValue());
							responseDTO.setStatus(ResponseStatus.FAILURE.getKey());
							responseDTO.setMessage("Unable to create Profile");
							responseDTO.setDetails("Unable to create Profile");
							responseDTO.setFullyFilled(true);
						}
					} catch (Exception e) {
						e.printStackTrace();
						responseDTO.setStatus(ResponseStatus.FAILURE);
						responseDTO.setMessage("Opps!something is wrong please try again....");
						return new ResponseEntity<>(responseDTO, HttpStatus.OK);
					}
				} else {
					responseDTO.setStatus(ResponseStatus.FAILURE);
					responseDTO.setMessage(registerError.getMessage());
				}
			} else {
				responseDTO.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				responseDTO.setMessage("Unauthorized Role..");
			}
		} else {
			responseDTO.setStatus(ResponseStatus.INVALID_HASH);
			responseDTO.setMessage("Invalid Hash..");
		}
		return new ResponseEntity<RegisterResponseDTO>(responseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/ValidateEmail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegisterResponseDTO> validateEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError registerError = registerValidation.validateEmail(dto);
				if (registerError.isValid()) {
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setMessage("proceed Ahead");
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(registerError.getMessage());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Invalid Activation Key");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid Hash");
		}
		return new ResponseEntity<RegisterResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Group", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GroupDetailDTO> getGroupDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language) {
		GroupDetailDTO result = new GroupDetailDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				List<GroupDetails> details = userApi.getGroupDetails();
				result.setGroupName(details);
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setSuccess(true);
				result.setMessage("Success");
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<GroupDetailDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DonationList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<DonationDTO> getDonationDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language) {
		DonationDTO result = new DonationDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				List<Donation> donationList = userApi.getDonationDetails();
				result.setDonationList(donationList);
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setSuccess(true);
				result.setMessage("Success");
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<DonationDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Fingoole/Services", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> fingooleServices(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();
					if (sessionId != null) {
						result = iMdexApi.getServices();
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setSuccess(true);
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Fingoole/Amount", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> getFingooleAmount(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();
					if (sessionId != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						if (user != null) {
							FingooleServices service = fingooleServicesRepository
									.getServiceBasedOnCode(dto.getBatchNumber());
							if (service != null) {
								if (dto.getBatchNumber().equalsIgnoreCase(service.getCode())) {
									result = iMdexApi.batchNumber(dto);
									result.setAmount(service.getAmount());
									result.setBatch(result.getBatch());
									result.setSuccess(true);
									result.setCode(ResponseStatus.SUCCESS.getValue());
									result.setStatus(ResponseStatus.SUCCESS.getKey());
								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Invalid batch number");
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Service does not exist");
								result.setSuccess(false);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("User does not Exist");
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Fingoole/Register", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> fingooleRegister(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();
					if (sessionId != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						dto.setBatchNumber(dto.getBatchNumber());
						dto.setEmail(user.getUserDetail().getEmail());
						result = iMdexApi.fingooleRegister(dto);
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Fingoole/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> processTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = dto.getSessionId();
					if (sessionId != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser user = userSession.getUser();
						System.err.println("registration list:: " + dto.getRegistrationData());
						System.err.println("batch:: " + dto.getBatchNumber());
						for (int i = 0; i < dto.getRegistrationData().size(); i++) {
							FingooleRegistration fr = new FingooleRegistration();
							fr.setName(dto.getRegistrationData().get(i).getName());
							fr.setAmount(dto.getRegistrationData().get(i).getAmount());
							fr.setBatchNumber(dto.getBatchNumber());
							fr.setMobileNumber(dto.getRegistrationData().get(i).getMobileNumber());
							fr.setNoOfDays(dto.getRegistrationData().get(i).getNoOfDays());
							fr.setServiceProvider(dto.getRegistrationData().get(i).getServiceProvider());
							fr.setUser(user);
							fingooleRegistrationRepository.save(fr);
						}
						MService service = mServiceRepository.findServiceByCode("FINGL");
						if (service != null && !Status.Active.equals(service.getStatus())) {
							MCommission commission = mCommissionRepository.findCommissionByService(service);
							List<FingooleRegistration> reg = fingooleRegistrationRepository
									.getRegistrationByUserAndBatch(user, dto.getBatchNumber());
							Double amount = 0.0;
							for (int i = 0; i < reg.size(); i++) {
								Double d1 = new Double(reg.get(i).getAmount());
								amount += d1.doubleValue();
							}
							TransactionalRequest request = new TransactionalRequest();
							request.setAmount(amount);
							request.setAndroid(true);
							request.setDescription("Fingoole Insurance Transaction");
							request.setCommission(commission);
							request.setDescription("Fingoole Insurance");
							request.setService(service);
							request.setUser(user);
							request.setBatch(dto.getBatchNumber());
							double balance = matchMoveApi.getBalanceApp(user);
							if (amount <= balance) {
								MTransaction transaction = transactionApi.initiateFingooleTransaction(request);
								dto.setBatchNumber(dto.getBatchNumber());
								dto.setTransactionRefNo(transaction.getTransactionRefNo());
								MTransaction transactionf = transactionApi
										.getTransactionByRefNo(transaction.getTransactionRefNo());
								if (transactionf != null) {
									MMCards cards1 = mMCardRepository.getPhysicalCardByUser(user);
									if (cards1 != null) {
										if (!cards1.isBlocked()) {
											UserKycResponse response = matchMoveApi.debitFromCardFingoole(user,
													cards1.getCardId(), transactionf.getAmount() + "",
													"Fingoole Insurance");
											if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
												double availableBalance = matchMoveApi.getBalanceApp(user);
												if (balance != availableBalance + amount) {
													result.setStatus(ResponseStatus.FAILURE.getKey());
													result.setCode(ResponseStatus.FAILURE.getValue());
													result.setMessage("transaction failed amount will be refunded.");
													result.setSuccess(false);
												} else {
													result = iMdexApi.fingooleService(dto);
													if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
														for (int i = 0; i < result.getCustomerTransactionList()
																.size(); i++) {
															FingooleRegistration ref = fingooleRegistrationRepository
																	.getUserByBatch(
																			result.getCustomerTransactionList().get(i)
																					.getCustomerName(),
																			result.getBatch());
															if (ref != null) {
																ref.setCoino(result.getCustomerTransactionList().get(i)
																		.getCoino());
																ref.setCoiurl(result.getCustomerTransactionList().get(i)
																		.getCoiurl());
																ref.setTransactionRefNo(
																		transaction.getTransactionRefNo());
																fingooleRegistrationRepository.save(ref);
															}
														}
														transactionf.setStatus(Status.Success);
														transactionf.setCardLoadStatus(Status.Success.getValue());
														transactionf.setSuspicious(false);
														transactionf.setAuthReferenceNo(response.getIndicator());
														transactionf.setCurrentBalance(availableBalance);
														transactionf
																.setMdexTransactionStatus(Status.Success.getValue());
														mTransactionRepository.save(transactionf);
														MPQAccountDetails account = user.getAccountDetail();
														account.setBalance(availableBalance);
														mPQAccountDetailRepository.save(account);
														result.setStatus(ResponseStatus.SUCCESS.getKey());
														result.setCode(ResponseStatus.SUCCESS.getValue());
														result.setMessage("Transaction Successfull");
														result.setSuccess(true);
														return new ResponseEntity<FingooleResponse>(result,
																HttpStatus.OK);
													} else {
														WalletResponse resp = matchMoveApi.refundDebitTxns(
																transactionf.getTransactionRefNo(), user);
														if (ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(resp.getCode())) {
															WalletResponse tranferCard = matchMoveApi
																	.transferFundsToMMCard(user,
																			String.valueOf(transactionf.getAmount()),
																			cards1.getCardId());
															if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																	.equalsIgnoreCase(tranferCard.getCode())) {
																transactionf.setStatus(Status.Refunded);
																String authRefNo = tranferCard.getAuthRetrivalNo();
																transactionf.setRetrivalReferenceNo(authRefNo);
																transactionf.setSuspicious(false);
															} else {
																transactionf.setStatus(Status.Refund_Failed);
															}
														} else {
															result.setStatus(ResponseStatus.FAILURE.getKey());
															result.setCode(ResponseStatus.FAILURE.getValue());
															result.setMessage("Transaction Failed");
															result.setSuccess(false);
														}
													}
												}
											} else {
												result.setStatus(ResponseStatus.FAILURE.getKey());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage("Transaction Failed");
												result.setSuccess(false);
											}
										} else {
											result.setStatus(ResponseStatus.FAILURE.getKey());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setMessage("Card is Blocked");
											result.setSuccess(false);
										}
									} else {
										System.err.println("in virtual card fingoole insurance");
										MMCards card3 = mMCardRepository.getVirtualCardsByCardUser(user);
										if (card3 != null) {
											if (!card3.isBlocked()) {
												UserKycResponse response = matchMoveApi.debitFromCardFingoole(user,
														card3.getCardId(), transactionf.getAmount() + "",
														"Fingoole Insurance");
												if (ResponseStatus.SUCCESS.getValue().equals(response.getCode())) {
													double availableBalance = matchMoveApi.getBalanceApp(user);
													if (balance != availableBalance + amount) {
														result.setStatus(ResponseStatus.FAILURE.getKey());
														result.setCode(ResponseStatus.FAILURE.getValue());
														result.setMessage(
																"transaction failed amount will be refunded.");
														result.setSuccess(false);
													} else {
														result = iMdexApi.fingooleService(dto);
														if (result.getCode()
																.equals(ResponseStatus.SUCCESS.getValue())) {
															for (int i = 0; i < result.getCustomerTransactionList()
																	.size(); i++) {
																FingooleRegistration ref = fingooleRegistrationRepository
																		.getUserByBatch(
																				result.getCustomerTransactionList()
																						.get(i).getCustomerName(),
																				result.getBatch());
																if (ref != null) {
																	ref.setCoino(result.getCustomerTransactionList()
																			.get(i).getCoino());
																	ref.setCoiurl(result.getCustomerTransactionList()
																			.get(i).getCoiurl());
																	ref.setTransactionRefNo(
																			transaction.getTransactionRefNo());
																	fingooleRegistrationRepository.save(ref);
																}
															}
															transactionf.setStatus(Status.Success);
															transactionf.setCardLoadStatus(Status.Success.getValue());
															transactionf.setSuspicious(false);
															transactionf.setAuthReferenceNo(response.getIndicator());
															transactionf.setCurrentBalance(availableBalance);
															transactionf.setMdexTransactionStatus(
																	Status.Success.getValue());
															mTransactionRepository.save(transactionf);
															MPQAccountDetails account = user.getAccountDetail();
															account.setBalance(availableBalance);
															mPQAccountDetailRepository.save(account);
															result.setStatus(ResponseStatus.SUCCESS.getKey());
															result.setCode(ResponseStatus.SUCCESS.getValue());
															result.setMessage("Transaction Successfull");
															result.setSuccess(true);
															return new ResponseEntity<FingooleResponse>(result,
																	HttpStatus.OK);
														} else {
															// WalletResponse resp = matchMoveApi
															// .reversalToWalletForRechargeFailure(
															// userSession.getUser(),
															// String.valueOf(transactionf.getAmount()),
															// transactionf.getService().getCode(),
															// transactionf.getTransactionRefNo(),
															// transactionf.getDescription());

															WalletResponse resp = matchMoveApi.refundDebitTxns(
																	transactionf.getTransactionRefNo(), user);
															if (ResponseStatus.SUCCESS.getValue()
																	.equalsIgnoreCase(resp.getCode())) {
																WalletResponse tranferCard = matchMoveApi
																		.transferFundsToMMCard(user,
																				String.valueOf(
																						transactionf.getAmount()),
																				card3.getCardId());
																if (tranferCard != null && ResponseStatus.SUCCESS
																		.getValue()
																		.equalsIgnoreCase(tranferCard.getCode())) {
																	transactionf.setStatus(Status.Refunded);
																	String authRefNo = tranferCard.getAuthRetrivalNo();
																	transactionf.setRetrivalReferenceNo(authRefNo);
																	transactionf.setSuspicious(false);
																} else {
																	transactionf.setStatus(Status.Refund_Failed);
																}
															} else {
																result.setStatus(ResponseStatus.FAILURE.getKey());
																result.setCode(ResponseStatus.FAILURE.getValue());
																result.setMessage("Transaction Failed");
																result.setSuccess(false);
															}
														}
													}
												} else {
													result.setStatus(ResponseStatus.FAILURE.getKey());
													result.setCode(ResponseStatus.FAILURE.getValue());
													result.setMessage(response.getMessage());
													result.setSuccess(false);
												}
											} else {
												result.setStatus(ResponseStatus.FAILURE.getKey());
												result.setCode(ResponseStatus.FAILURE.getValue());
												result.setMessage("Card is Blocked");
												result.setSuccess(false);
											}
										} else {
											result.setStatus(ResponseStatus.FAILURE.getKey());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setMessage("card does not exist");
											result.setSuccess(false);
										}
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage("Transaction Does not exist");
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage("Low Balance");
								result.setSuccess(false);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Currently service is down.");
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/Fingoole/TransactionList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FingooleResponse> getTransactionList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FingooleRegisterDTO dto) {
		FingooleResponse result = new FingooleResponse();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser user = userSession.getUser();
				ArrayList<FingooleTransactionListDTO> tran = new ArrayList<FingooleTransactionListDTO>();
				List<FingooleRegistration> reference = fingooleRegistrationRepository.getListByUsersNoBatch(user);
				for (int i = 0; i < reference.size(); i++) {
					FingooleTransactionListDTO list = new FingooleTransactionListDTO();
					list.setAmount(reference.get(i).getAmount());
					list.setCoi(reference.get(i).getCoino());
					list.setDate(reference.get(i).getCreated().toLocaleString());
					list.setMobile(reference.get(i).getMobileNumber());
					list.setName(reference.get(i).getName());
					list.setTransactionNo(reference.get(i).getTransactionRefNo());
					list.setDescription("Fingoole Insurance");
					tran.add(list);
				}
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setSuccess(true);
				result.setTransactions(tran);
				result.setMessage("Transaction List");
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<FingooleResponse>(result, HttpStatus.OK);
	}
}