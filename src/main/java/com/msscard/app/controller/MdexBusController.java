package com.msscard.app.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.enums.Device;
import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IMdexBusApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.BusAvailableTripListRequest;
import com.msscard.app.model.request.BusBookTicketRequest;
import com.msscard.app.model.request.BusSaveSeatDetailsRequest;
import com.msscard.app.model.request.CommonRequest;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.response.BusSaveSeatDetailsResponse;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.MDEXBusAppResponse;
import com.msscard.app.model.response.TopupAppResponse;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.BusSeatDetailRequest;
import com.msscard.model.GetSeatDetailsResp;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.TransactionIdDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.TransactionValidationError;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.util.ServiceCodeUtil;
import com.msscard.validation.CommonValidation;
import com.msscard.validation.TransactionTypeValidation;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Travel/Bus")
public class MdexBusController {
	
	private final UserSessionRepository userSessionRepository;
	private final IMdexBusApi mdexBusApiImpl;
	private final IUserApi userApiImpl;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final CommonValidation commonValidation;
	private final MServiceRepository mServiceRepository;
	private TransactionTypeValidation transactionTypeValidation;	

	public MdexBusController(UserSessionRepository userSessionRepository, IMdexBusApi mdexBusApiImpl,
			IUserApi userApiImpl, PersistingSessionRegistry persistingSessionRegistry,
			CommonValidation commonValidation, MServiceRepository mServiceRepository,
			TransactionTypeValidation transactionTypeValidation) {
		this.userSessionRepository = userSessionRepository;
		this.mdexBusApiImpl = mdexBusApiImpl;
		this.userApiImpl = userApiImpl;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.commonValidation = commonValidation;
		this.mServiceRepository = mServiceRepository;
		this.transactionTypeValidation = transactionTypeValidation;		
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/SetUpi")
	public ResponseEntity<CommonResponse> setupi(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language) {
		mdexBusApiImpl.setUpi();
		CommonResponse response = new CommonResponse();
		response.setCode(ResponseStatus.SUCCESS.getValue());
		return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/SetCity")
	public ResponseEntity<CommonResponse> setcity(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language) {
		CommonResponse response = mdexBusApiImpl.setCityList();
		return new ResponseEntity<CommonResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetCityList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getAllCityList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody CommonRequest dto) {
		CommonResponse result = new CommonResponse();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
							result = mdexBusApiImpl.getAllCityList(result);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage(ErrorMessage.FAILED_MSG);
							result.setSuccess(false);
						} 
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
						result.setSuccess(false);
					}
				} catch (Exception e) {
					System.out.println(e);
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage("Service Unavailable");
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/GetAllAvailableTrips" , method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getAllAvailableTrips(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BusAvailableTripListRequest dto) {
		CommonResponse result = new CommonResponse();
		try {			
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String session = dto.getSessionId();
					if(session != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
						if (userSession != null) {
							UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
								CommonError error = commonValidation.getAllAvailableTripsValidation(dto);
								if(error.isValid()) {
									result = mdexBusApiImpl.getAvailableTripsForBus(dto, result);
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(error.getMessage());
									
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
								result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
								result.setMessage(ErrorMessage.FAILED_MSG);
								
							}
						} else {
							result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
							result.setCode(ResponseStatus.INVALID_SESSION.getValue());
							result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
							
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
						
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
				
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				
			}
		} catch(Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetSeatDetails", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<GetSeatDetailsResp> getSeatDetails(@PathVariable(value = "version") String version,
            @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
            @PathVariable(value = "language") String language, @RequestBody BusSeatDetailRequest dto,
            @RequestHeader(value = "hash", required = false) String hash) {

        GetSeatDetailsResp resp = new GetSeatDetailsResp();
        try {
        	if (role.equalsIgnoreCase(Role.USER.getValue())) {
    			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
    					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
                  
    				CommonError error = commonValidation.busSeatDetailsValidation(dto);
					if (error.isValid()) {
                            resp = mdexBusApiImpl.getBusSeatDetails(dto);
                        } else {
                            resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
                            resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
                            resp.setMessage(error.getMessage());
                        }
                   
                } else {
                    resp.setCode(ResponseStatus.FAILURE.getValue());
                    resp.setMessage(ErrorMessage.DEVICE_MSG);
                    resp.setStatus(ResponseStatus.FAILURE.getKey());
                }
            } else {
                resp.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
                resp.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
                resp.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
            resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
            resp.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
        }
        return new ResponseEntity<GetSeatDetailsResp>(resp, HttpStatus.OK);    
    }

	@RequestMapping(method = RequestMethod.POST, value = "/SaveSeat", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusSaveSeatDetailsResponse> saveSeatDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BusSaveSeatDetailsRequest dto) {
		BusSaveSeatDetailsResponse result = new BusSaveSeatDetailsResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
							MUser u = userApiImpl.findByUserName(user.getUsername());
							result = mdexBusApiImpl.saveSeatDetails(dto, result, u);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage(ErrorMessage.FAILED_MSG);
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
						result.setSuccess(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<BusSaveSeatDetailsResponse>(result, HttpStatus.OK);
	}

	
	@RequestMapping(value="/GetTransactionId", method=RequestMethod.POST,  produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getTransactionId(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TransactionIdDTO dto) {
		CommonResponse result = new CommonResponse();	
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String session = dto.getSessionId();
					if(session != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
						if (userSession != null) {
							UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
								CommonError error = commonValidation.getTransactionIdVal(dto);
								if(error.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.BUS);
									CommonError err = commonValidation.checkTravelValidation(service);
									if (err.isValid()) {
										result = mdexBusApiImpl.getTransactionId(dto);
										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(result.getCode())) {
											result = mdexBusApiImpl.saveDetailsInDB(dto, result, userSession.getUser());
										}
									} else {
										result.setCode(ResponseStatus.FAILURE.getValue());
										result.setMessage(err.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(error.getMessage());
									result.setSuccess(false);
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
								result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
								result.setMessage(ErrorMessage.FAILED_MSG);
								result.setSuccess(false);
							}
						} else {
							result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
							result.setCode(ResponseStatus.INVALID_SESSION.getValue());
							result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/BookTicketUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> bookTicketUpdated(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BusBookTicketRequest dto) {
		CommonResponse result = new CommonResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					MUser user = userSession.getUser();
					CommonError error = commonValidation.checkBookTicketValidation(dto);
					if (error.isValid()) {
						TransactionValidationError resp = transactionTypeValidation.checkPaymentForBus(user, dto.getAmount());
						if (resp.isValid()) {
							TopupAppResponse resp1 = mdexBusApiImpl.initiatePayment(dto, user);
							if(resp1.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								dto.setTransactionId(resp1.getTransactionId());
								MDEXBusAppResponse resp2 = mdexBusApiImpl.bookTicket(dto);
								resp2.setTransactionId(resp1.getTransactionId());
								result = mdexBusApiImpl.confirmBusPayment(resp2, dto, user);
							} else {
								result.setStatus(resp1.getStatus());
								result.setCode(resp1.getCode());
								result.setMessage(resp1.getMessage());
								result.setSuccess(resp1.isSuccess());
							}							
						} else {
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setCode(resp.getCode());
							result.setMessage(resp.getMessage());							
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
						result.setCode(ResponseStatus.BAD_REQUEST.getValue());
						result.setMessage(error.getMessage());
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.DEVICE_MSG);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
				result.setSuccess(false);
			}
		} catch(Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cancelInitPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<CommonResponse> cancelPaymentforInit(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody IsCancellableReq dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		CommonResponse result = new CommonResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

							result = mdexBusApiImpl.cancellInitiRequest(dto.getvPqTxnId());
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setMessage("Transection cancel successful");
							result.setCode(ResponseStatus.SUCCESS.getValue());

						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/MyTickets", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> getAllTicketsByUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody CommonRequest dto) {
		CommonResponse result = new CommonResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
							MUser u = userApiImpl.findByUserName(user.getUsername());
							result = mdexBusApiImpl.getAllBookTickets(dto.getSessionId(), u, result);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage(ErrorMessage.FAILED_MSG);
							result.setSuccess(false);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
						result.setSuccess(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					result.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					result.setSuccess(false);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.DEVICE_MSG);
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setMessage(ErrorMessage.AUTHORITY_MSG);
			result.setSuccess(false);
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
		

}
