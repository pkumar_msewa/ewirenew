package com.msscard.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.LoadWalletByVARequest;
import com.msscard.model.Status;
import com.msscard.repositories.LoadWalletByVARequestRepository;
import com.msscard.sms.constant.LoadWalletByVirtualAccountPayload;

@RequestMapping("/transaction")
@Controller
public class CallBackController {

	private final LoadWalletByVARequestRepository loadWalletByVARequestRepository;

	public CallBackController(LoadWalletByVARequestRepository loadWalletByVARequestRepository) {
		this.loadWalletByVARequestRepository = loadWalletByVARequestRepository;
	}

	@RequestMapping(value = "/LoadWalletByVirtualAccount", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> LoadVirtualAccount(@ModelAttribute LoadWalletByVirtualAccountPayload dto) {
		System.out.println("Load Wallet By VA: " + dto.toString());

		CommonResponse result = new CommonResponse();

		try {
			LoadWalletByVARequest loadWalletByVARequest = new LoadWalletByVARequest();
			loadWalletByVARequest.setAgentCode(dto.getAgent_code());
			loadWalletByVARequest.setAgentName(dto.getAgent_name());
			loadWalletByVARequest.setAmount(dto.getAmount());
			loadWalletByVARequest.setDateAdded(dto.getDate_added());
			loadWalletByVARequest.setMmId(dto.getId());
			loadWalletByVARequest.setMmStatus(dto.getStatus());
			loadWalletByVARequest.setPrefundingType(dto.getPrefunding_type());
			loadWalletByVARequest.setProductCode(dto.getProduct_code());
			loadWalletByVARequest.setStatus(Status.Pending);
			loadWalletByVARequest.setTransactionComments(dto.getTransaction_comments());
			loadWalletByVARequest.setVirtualAccountNumber(dto.getVirtual_account_number());
			loadWalletByVARequestRepository.save(loadWalletByVARequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
}