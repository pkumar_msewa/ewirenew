package com.msscard.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.imps.entity.TestNewUsers;
import com.msscard.app.api.CorporateMatchMoveApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.model.ReconBalanceDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserType;
import com.msscard.repositories.KYCPendingRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.TestNewUsersRepository;

@Controller
@RequestMapping("/TestController")
public class TestController {

	private KYCPendingRepository KYCPendingRepository;
	private MUserRespository mUserRespository;
	private MKycRepository kycRepository;
	private IMatchMoveApi matchMoveApi;
	private MPQAccountDetailRepository mPQAccountDetailRepository;
	private MPQAccountTypeRepository mPQAccountTypeRepository;
	private MatchMoveWalletRepository matchMoveWalletRepository;
	private MMCardRepository cardRepository;
	private IUserApi userApi;
	private CorporateMatchMoveApi corporateMatchMoveApi;
	private TestNewUsersRepository testNewUsersRepository;
	private MKycRepository mKycRepository;
	private MMCardRepository mMCardRepository;
	private PhysicalCardDetailRepository physicalCardDetailRepository;

	public TestController(KYCPendingRepository KYCPendingRepository, MUserRespository mUserRespository,
			MKycRepository kycRepository, IMatchMoveApi matchMoveApi,
			MPQAccountDetailRepository mPQAccountDetailRepository, MPQAccountTypeRepository mPQAccountTypeRepository,
			MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository cardRepository, IUserApi userApi,
			CorporateMatchMoveApi corporateMatchMoveApi, TestNewUsersRepository testNewUsersRepository,
			MKycRepository mKycRepository, MMCardRepository mMCardRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository) {
		this.KYCPendingRepository = KYCPendingRepository;
		this.mUserRespository = mUserRespository;
		this.kycRepository = kycRepository;
		this.matchMoveApi = matchMoveApi;
		this.mPQAccountDetailRepository = mPQAccountDetailRepository;
		this.mPQAccountTypeRepository = mPQAccountTypeRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.cardRepository = cardRepository;
		this.userApi = userApi;
		this.corporateMatchMoveApi = corporateMatchMoveApi;
		this.testNewUsersRepository = testNewUsersRepository;
		this.mKycRepository = mKycRepository;
		this.mMCardRepository = mMCardRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
	}

	@RequestMapping(value = "/kycpending", method = RequestMethod.GET)
	ResponseEntity<CommonResponse> kycpening() {
		CommonResponse result = new CommonResponse();

		List<String> ky = KYCPendingRepository.getList(Status.Active);
		if (ky != null && ky.size() > 0) {
			List<Long> userList = mUserRespository.getListByMobile(ky);
			if (userList != null && userList.size() > 0) {
				List<MKycDetail> kycList = kycRepository.getListByMobile(userList);
				if (kycList != null && kycList.size() > 0) {
					MPQAccountType accountTypeKYC = mPQAccountTypeRepository.findByCode("KYC");
					MPQAccountType accountTypeNONKYC = mPQAccountTypeRepository.findByCode("NONKYC");

					for (MKycDetail km : kycList) {
						try {
							MUser user = km.getUser();

							MPQAccountDetails accountDetails = user.getAccountDetail();
							if (accountDetails != null) {
								MPQAccountType accountType = accountDetails.getAccountType();

								if ("NONKYC".equalsIgnoreCase(accountType.getCode())) {
									UserKycResponse userKyc = matchMoveApi.kycUserMMCorporate(km);
									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKyc.getCode())) {
										UserKycResponse userIdDetails = matchMoveApi.setIdDetailsCorporate(km);
										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(userIdDetails.getCode())) {
											UserKycResponse image1Upload = matchMoveApi.setImagesForKyc1(km);
											if (ResponseStatus.SUCCESS.getValue()
													.equalsIgnoreCase(image1Upload.getCode())) {
												UserKycResponse imageUpload2 = matchMoveApi.setImagesForKyc2(km);
												if (ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(imageUpload2.getCode())) {
													UserKycResponse approvalResp = matchMoveApi
															.tempConfirmKycCorporate(km);
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(approvalResp.getCode())) {
														MPQAccountDetails accDetails = user.getAccountDetail();
														if (accDetails != null) {
															accDetails.setAccountType(accountTypeKYC);
															mPQAccountDetailRepository.save(accDetails);

															km.setAccountType(accountTypeKYC);
															km.setRejectionStatus(false);
															kycRepository.save(km);
														} else {
															km.setAccountType(accountTypeNONKYC);
															km.setRejectionStatus(true);
															km.setRejectionReason("No Account found");
															kycRepository.save(km);
														}
													} else {
														km.setAccountType(accountTypeNONKYC);
														km.setRejectionStatus(true);
														km.setRejectionReason(approvalResp.getMessage());
														kycRepository.save(km);
													}
												} else {
													km.setAccountType(accountTypeNONKYC);
													km.setRejectionStatus(true);
													km.setRejectionReason(imageUpload2.getMessage());
													kycRepository.save(km);
												}
											} else {
												km.setAccountType(accountTypeNONKYC);
												km.setRejectionStatus(true);
												km.setRejectionReason(image1Upload.getMessage());
												kycRepository.save(km);
											}
										} else {
											km.setAccountType(accountTypeNONKYC);
											km.setRejectionStatus(true);
											km.setRejectionReason(userIdDetails.getMessage());
											kycRepository.save(km);
										}
									} else {
										km.setAccountType(accountTypeNONKYC);
										km.setRejectionStatus(true);
										km.setRejectionReason(userKyc.getMessage());
										kycRepository.save(km);
									}
								} else {
									MPQAccountDetails accDetails = user.getAccountDetail();
									UserKycResponse kycResponse = matchMoveApi.checkKYCStatus(user);

									if (!("Approved".equalsIgnoreCase(kycResponse.getStatus()) || ResponseStatus.SUCCESS
											.getValue().equalsIgnoreCase(kycResponse.getCode()))) {
										kycRepository.delete(km);

										UserKycResponse userKyc = matchMoveApi.kycUserMMCorporate(km);
										if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKyc.getCode())) {
											UserKycResponse userIdDetails = matchMoveApi.setIdDetailsCorporate(km);
											if (ResponseStatus.SUCCESS.getValue()
													.equalsIgnoreCase(userIdDetails.getCode())) {
												UserKycResponse image1Upload = matchMoveApi.setImagesForKyc1(km);
												if (ResponseStatus.SUCCESS.getValue()
														.equalsIgnoreCase(image1Upload.getCode())) {
													UserKycResponse imageUpload2 = matchMoveApi.setImagesForKyc2(km);
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(imageUpload2.getCode())) {
														UserKycResponse approvalResp = matchMoveApi
																.tempConfirmKycCorporate(km);
														if (ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(approvalResp.getCode())) {
															if (accDetails != null) {
																accDetails.setAccountType(accountTypeKYC);
																mPQAccountDetailRepository.save(accDetails);

																km.setAccountType(accountTypeKYC);
																km.setRejectionStatus(false);
																kycRepository.save(km);
															} else {
																km.setAccountType(accountTypeNONKYC);
																km.setRejectionStatus(true);
																km.setRejectionReason("No Account found");
																kycRepository.save(km);
															}
														} else {
															km.setAccountType(accountTypeNONKYC);
															km.setRejectionStatus(true);
															km.setRejectionReason(approvalResp.getMessage());
															kycRepository.save(km);

															accDetails.setAccountType(accountTypeNONKYC);
															mPQAccountDetailRepository.save(accDetails);
														}
													} else {
														km.setAccountType(accountTypeNONKYC);
														km.setRejectionStatus(true);
														km.setRejectionReason(imageUpload2.getMessage());
														kycRepository.save(km);

														accDetails.setAccountType(accountTypeNONKYC);
														mPQAccountDetailRepository.save(accDetails);
													}
												} else {
													km.setAccountType(accountTypeNONKYC);
													km.setRejectionStatus(true);
													km.setRejectionReason(image1Upload.getMessage());
													kycRepository.save(km);

													accDetails.setAccountType(accountTypeNONKYC);
													mPQAccountDetailRepository.save(accDetails);
												}
											} else {
												km.setAccountType(accountTypeNONKYC);
												km.setRejectionStatus(true);
												km.setRejectionReason(userIdDetails.getMessage());
												kycRepository.save(km);

												accDetails.setAccountType(accountTypeNONKYC);
												mPQAccountDetailRepository.save(accDetails);
											}
										} else {
											km.setAccountType(accountTypeNONKYC);
											km.setRejectionStatus(true);
											km.setRejectionReason(userKyc.getMessage());
											kycRepository.save(km);

											accDetails.setAccountType(accountTypeNONKYC);
											mPQAccountDetailRepository.save(accDetails);
										}
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DebitWalletToPrefund/{username}/{amount}", method = RequestMethod.GET)
	public ResponseEntity<ReconBalanceDTO> debitWalletToPrefund(@PathVariable(value = "username") String username,
			@PathVariable(value = "amount") String amount, HttpServletRequest request, HttpServletResponse response) {
		ReconBalanceDTO resp = new ReconBalanceDTO();

		try {
			MUser user = mUserRespository.findByUsername(username);
			if (user != null) {
				MatchMoveWallet matchMoveWallet = matchMoveWalletRepository.findByUser(user);
				if (matchMoveWallet != null) {
					double balance = matchMoveApi.getBalanceApp(matchMoveWallet.getUser());
					double amt = Double.parseDouble(amount);

					String activeCardId = null;
					List<MMCards> cardList = cardRepository.getCardsListByUser(matchMoveWallet.getUser());
					if (cardList != null && cardList.size() > 0) {
						for (MMCards mmCards : cardList) {
							if (mmCards.isHasPhysicalCard()
									&& Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
								activeCardId = mmCards.getCardId();
							} else if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
								activeCardId = mmCards.getCardId();
							}
						}
					}

					if (activeCardId != null) {
						if (balance > 0 && balance >= amt) {
							// UserKycResponse userResponse =
							// matchMoveApi.debitFromCardByAdmin(activeCardId, amount);
							// if
							// (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userResponse.getCode()))
							// {
							// resp.setMessage("Done Donna Dan Dan");
							// } else {
							// resp.setMessage("Fail Response");
							// }
						} else {
							resp.setMessage("Balance is low");
						}
					} else {
						resp.setMessage("Card id not found.");
					}
				} else {
					resp.setMessage("User wallet is not available.");
				}
			} else {
				resp.setMessage("User is not available");
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			resp.setMessage("Please try again later.");
		}
		return new ResponseEntity<ReconBalanceDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/newRegister", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> newRegister(HttpServletRequest request, HttpServletResponse response) {
		CommonResponse result = new CommonResponse();

		List<TestNewUsers> userList = testNewUsersRepository.getData();
		if (userList != null) {
			for (TestNewUsers tnu : userList) {
				try {
					String fname = tnu.getFirstName();
					String lname = tnu.getLastName();
					String mobile = tnu.getMobile();
					String email = tnu.getEmail();
					String dob = tnu.getDob();
					String idType = tnu.getIdType();
					String idNumber = tnu.getIdNumber();

					RegisterDTO dto = new RegisterDTO();
					dto.setFirstName(fname);
					dto.setLastName(lname);
					dto.setUserType(UserType.User);
					dto.setPassword("123456");
					dto.setIdType(idType);
					dto.setIdNo(idNumber);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date d1 = sdf.parse(dob);

					dto.setDateOfBirth(sdf.format(d1));
					dto.setEmail(email);
					dto.setContactNo(mobile);
					dto.setUsername(mobile);
					dto.setGroup("");
					dto.setRemark("");
					dto.setAddress("");

					MUser usermob = userApi.findByUserName(mobile);
					if (usermob == null) {
						// userApi.saveUserForKYC(dto);
						usermob = userApi.findByUserName(mobile);

						UserKycResponse users = matchMoveApi.getUsers(email, mobile);
						if (!ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(users.getCode())) {
							ResponseDTO resUser = corporateMatchMoveApi.createUserOnMM(usermob);

							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resUser.getCode())) {
								tnu.setUserCreationStatus(true);
								tnu.setWalletCreationStatus(true);

								MMCards cardUser = cardRepository.getVirtualCardsByCardUser(usermob);
								if (cardUser == null) {
									WalletResponse virCard = corporateMatchMoveApi.assignVirtualCard(usermob);
									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(virCard.getCode())) {
										tnu.setCardCreationStatus(true);
									} else {
										tnu.setFailedReason(virCard.getMessage());
										tnu.setFailedStatus(true);
									}
								} else {
									tnu.setFailedReason("card is not null");
									tnu.setFailedStatus(true);
								}
							} else {
								tnu.setFailedReason(resUser.getMessage());
								tnu.setFailedStatus(true);
							}
						} else {
							tnu.setFailedReason("User is already registered at MM");
							tnu.setFailedStatus(true);
						}
					} else {
						tnu.setFailedReason("User is already registered");
						tnu.setFailedStatus(true);
					}
					testNewUsersRepository.save(tnu);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		result.setCode("AK");
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/saveKYCDetails", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> mkycDetailsEntry(HttpServletRequest request, HttpServletResponse response) {
		CommonResponse result = new CommonResponse();

		List<TestNewUsers> userList = testNewUsersRepository.getSuccessData();
		if (userList != null) {
			for (TestNewUsers tnu : userList) {
				try {
					String mobile = tnu.getMobile();

					MUser user = userApi.findByUserName(mobile);
					if (user != null) {
						MKycDetail mk = new MKycDetail();
						mk.setUserType(UserType.User);
						mk.setAccountType(user.getAccountDetail().getAccountType());
						mk.setUser(user);
						mKycRepository.save(mk);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		result.setCode("AK");
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/activateCard/{mobile}", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> physicalCardActivate(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("mobile") String mobile) {
		CommonResponse result = new CommonResponse();
		try {
			MUser user = mUserRespository.findByUsername(mobile);
			if (user != null) {
				MatchMoveWallet mmw = matchMoveWalletRepository.findByUser(user);
				if (mmw != null) {
					MMCards card = mMCardRepository.getInactivePhysical(true, Status.Inactive.getValue(), mmw);
					if (card != null) {
						ResponseDTO resp = matchMoveApi.activationPhysicalCard(card.getActivationCode(), user);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {

							PhysicalCardDetails card1 = physicalCardDetailRepository.findByUser(user);
							if (card1 != null) {
								card1.setStatus(Status.Active);
								physicalCardDetailRepository.save(card1);
							}

							result.setMessage("Done dona done done");
						} else {
							System.out.println("Fail from MM (Card Id): " + card.getId());
						}
					} else {
						System.out.println("card not found (Wallet Id): " + mmw.getId());
					}
				} else {
					System.out.println("Wallet not found (User Id): " + user.getId());
				}
			} else {
				System.out.println("User not found (bulk kyc corporate Table Id):");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("Please try again later.");
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
}