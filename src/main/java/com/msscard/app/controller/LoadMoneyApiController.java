package com.msscard.app.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.api.ILoadMoneyApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.PGHandlerDTO;
import com.msscard.app.model.request.PGTransaction;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.PGStatusCheckDTO;
import com.msscard.app.model.response.TransactionInitiateResponse;
import com.msscard.app.model.response.TransactionRedirectResponse;
import com.msscard.app.model.response.UPIResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MdexRequestLogs;
import com.msscard.entity.UpiPay;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MdexRequestLogsRepository;
import com.msscard.repositories.UpiPayRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.sms.constant.SMSConstant;
import com.msscard.util.AESEncryption;
import com.msscard.util.Authorities;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.LoadMoneyValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Controller
@RequestMapping(value = "/Api/v1/{role}/{device}/{language}/LoadMoney")
public class LoadMoneyApiController {

	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ILoadMoneyApi loadMoneyApi;
	private final IMatchMoveApi matchMoveApi;
	private final MTransactionRepository mTransactionRepository;
	private final MMCardRepository mMCardRepository;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MServiceRepository mServiceRepository;
	private final ITransactionApi transactionApi;
	private final MdexRequestLogsRepository mdexRequestLogsRepository;
	private final IMdexApi iMdexApi;
	private final UpiPayRepository upiPayRepository;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public LoadMoneyApiController(UserSessionRepository userSessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry, ILoadMoneyApi loadMoneyApi, IMatchMoveApi matchMoveApi,
			MTransactionRepository transactionRepository, MMCardRepository cardRepository,
			LoadMoneyValidation loadMoneyValidation, MServiceRepository serviceRepository,
			ITransactionApi transactionApi, MdexRequestLogsRepository mdexRequestLogsRepository, IMdexApi iMdexApi,
			UpiPayRepository upiPayRepository) {
		super();
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.loadMoneyApi = loadMoneyApi;
		this.matchMoveApi = matchMoveApi;
		this.mTransactionRepository = transactionRepository;
		this.mMCardRepository = cardRepository;
		this.loadMoneyValidation = loadMoneyValidation;
		this.mServiceRepository = serviceRepository;
		this.transactionApi = transactionApi;
		this.mdexRequestLogsRepository = mdexRequestLogsRepository;
		this.iMdexApi = iMdexApi;
		this.upiPayRepository = upiPayRepository;
	}

	@RequestMapping(value = "/InitiateRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestHeader(value = "Authorization") String authorization,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		try {
		UpiPay value = upiPayRepository.getDataUsingKeys(SMSConstant.UPIKEY);
		String upiBasicAuth = SMSConstant.getBasicAuthorizationUpi(value.getUsername(),value.getPassword());
		if(upiBasicAuth.equals(authorization)) {
			TransactionInitiateRequest treq = SMSConstant.prepareTopupRequest(AESEncryption.decrypt(transactionRequest.getUpiRequest()));
			boolean isValidHash = SecurityUtil.isHashMatches(treq.getSessionId(), hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("User")) {
					String sessionId = treq.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							String remoteAddress = request.getRemoteAddr();
							userSession.setIpAddress(remoteAddress);
							userSessionRepository.save(userSession);
							UserKycResponse resp = matchMoveApi.getConsumers();
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
								MService service = mServiceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
								if (Status.Active.equals(service.getStatus())) {
									CommonError error = loadMoneyValidation.validateKYCCreditLimit(
											userSession.getUser(), Double.parseDouble(treq.getAmount()));
									if (error.isValid()) {
										String prefundingBalance = resp.getPrefundingBalance();
										double doublePrefundingBalance = Double.parseDouble(prefundingBalance);
										double actTransactonAmt = Double.parseDouble(treq.getAmount());
										if (doublePrefundingBalance >= actTransactonAmt) {
											TransactionError transactionError = loadMoneyValidation
													.validateLoadMoneyTransaction(treq.getAmount(),
															userSession.getUser().getUsername(), service);
											if (transactionError.isValid()) {
												MTransaction transaction = loadMoneyApi.initiateTransaction(
														user.getUsername(),
														Double.parseDouble(treq.getAmount()),
														RazorPayConstants.SERVICE_CODE);
												if (transaction != null) {
													initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
													initiateResponse.setMessage("Transaction initiated");
													initiateResponse.setStatus(ResponseStatus.SUCCESS.getKey());
													initiateResponse
															.setTransactionRefNo(transaction.getTransactionRefNo());
													initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
													initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
													initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);
												} else {
													initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
													initiateResponse.setMessage("Transaction failed");
												}
											} else {
												initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
												initiateResponse.setMessage(transactionError.getMessage());
											}
										} else {
											initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
											initiateResponse.setMessage(
													"Currently we are facing some issues.Please try after sometime");
										}
									} else {
										initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
										initiateResponse.setMessage(error.getMessage());
									}

								} else {
									initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
									initiateResponse.setMessage(
											"Currently services are down.Please try after sometime");
								}
							} else {
								initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
								initiateResponse.setMessage("Currently we are facing some issues.Please try after sometime");
							}
						} else {
							initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							initiateResponse.setMessage("Unauthorized User");
						}
					} else {
						initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
						initiateResponse.setMessage("Please login and try again");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					initiateResponse.setMessage("Unauthorized Role");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
				initiateResponse.setMessage("Invalid hash");
			}
	}else{
		initiateResponse.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		initiateResponse.setMessage("Invalid access");
	}
		} catch (Exception e) {
			e.printStackTrace();
			initiateResponse.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
			initiateResponse.setMessage("Fatal Error");
			return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);

		}
		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/Redirect", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> redirectResponse(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MTransaction transaction = mTransactionRepository
								.findByTransactionRefNo(transactionRequest.getTransactionRefNo());
						if (transaction != null) {
							transactionRequest.setAmount(transaction.getAmount() * 100 + "");
							TransactionRedirectResponse redirectResponse = loadMoneyApi
									.redirectResponse(transactionRequest);
							MMCards Physical = null;
							MMCards virtual = null;
							WalletResponse cardTransferResposne = null;
							if (redirectResponse.isSuccess()) {
								double actualTransactionAmt = transaction.getAmount();
								actualTransactionAmt = transaction.getAmount() - transaction.getCommissionEarned();
								String actAmount = String.valueOf(actualTransactionAmt);
								WalletResponse walletResponse = matchMoveApi
										.initiateLoadFundsToMMWalletRazor(userSession.getUser(), actAmount);
								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
									String authRefNo = walletResponse.getAuthRetrivalNo();
									transaction.setAuthReferenceNo(authRefNo);
									transaction.setStatus(Status.Success);
									transaction.setCardLoadStatus("Pending");
									if (transactionRequest.getPhysicalCard() != null) {
										if (transactionRequest.getPhysicalCard().equalsIgnoreCase("Yes")) {
											Physical = mMCardRepository.getPhysicalCardByUser(userSession.getUser());
											if (Status.Active.getValue().equalsIgnoreCase(Physical.getStatus())) {
												cardTransferResposne = matchMoveApi.transferFundsToMMCard(
														userSession.getUser(), actAmount, Physical.getCardId());
											}
										}
									}
									if (transactionRequest.getVirtualCard() != null) {
										if (transactionRequest.getVirtualCard().equalsIgnoreCase("Yes")) {
											virtual = mMCardRepository.getVirtualCardsByCardUser(userSession.getUser());
											if (virtual.getStatus().equalsIgnoreCase("Active"))
												cardTransferResposne = matchMoveApi.transferFundsToMMCard(
														userSession.getUser(), actAmount, virtual.getCardId());
										}
									}
									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(cardTransferResposne.getCode())) {
										transaction.setCardLoadStatus(ResponseStatus.SUCCESS.getValue());
										mTransactionRepository.save(transaction);
										initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
										initiateResponse.setMessage("Transaction Success");
										initiateResponse.setStatus(ResponseStatus.SUCCESS.getKey());
									} else {
										transaction.setCardLoadStatus(Status.Pending.getValue());
										transaction.setRemarks("CardEr::" + cardTransferResposne.getMessage());
										mTransactionRepository.save(transaction);
										initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
										initiateResponse.setMessage("Transaction Successful.But card not loaded");
									}

								} else {
									transaction.setCardLoadStatus(Status.Pending.getValue());
									transaction.setRemarks("WalEr::" + walletResponse.getMessage());
									mTransactionRepository.save(transaction);
									initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
									initiateResponse.setMessage("Transaction Successful.But card not loaded");
								}
							} else {
								initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
								initiateResponse.setMessage("Transaction failed");
							}
						} else {
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}

					} else {
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		} else {
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}

		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

	/**
	 * INITIATE CARD REQUEST
	 */

	@RequestMapping(value = "/InitiateCardRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateCardRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MTransaction transaction = loadMoneyApi.initiateTransaction(user.getUsername(),
								Double.parseDouble(transactionRequest.getAmount()), RazorPayConstants.SERVICE_CODE);
						if (transaction != null) {
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction initiated");
							initiateResponse.setStatus("Success");
							initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
							initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
							initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
							initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);
						} else {
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}

					} else {
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		} else {
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}

		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

	/**
	 * CONFIRM CARD REQUEST
	 */

	@RequestMapping(value = "/ConfirmCardRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> confirmCardRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						TransactionRedirectResponse redirectResponse = loadMoneyApi
								.redirectResponse(transactionRequest);
						if (redirectResponse.isSuccess()) {
							initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
							initiateResponse.setMessage("Transaction Success");
							initiateResponse.setStatus("Success");
						} else {
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse.setMessage("Transaction failed");
						}

					} else {
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		} else {
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}

		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/InitiateUPI", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> initiateUPI(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestHeader(value = "Authorization") String authorization,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		try {
			UpiPay value = upiPayRepository.getDataUsingKeys(SMSConstant.UPIKEY);
			String upiBasicAuth = SMSConstant.getBasicAuthorizationUpi(value.getUsername(), value.getPassword());
			if (upiBasicAuth.equals(authorization)) {
				TransactionInitiateRequest treq = SMSConstant
						.prepareTopupRequest(AESEncryption.decrypt(transactionRequest.getUpiRequest()));
				boolean isValidHash = SecurityUtil.isHashMatches(treq.getSessionId(), hash);
				if (isValidHash) {
					if (role.equalsIgnoreCase("User")) {
						String sessionId = treq.getSessionId();
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						if (userSession != null) {
							UserDTO user = userApi.getUserById(userSession.getUser().getId());
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								System.err.println("ip address:: " + request.getRemoteAddr());
								UserKycResponse resp = matchMoveApi.getConsumers();
								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
									String prefundingBalance = resp.getPrefundingBalance();
									double doublePrefundingBalance = Double.parseDouble(prefundingBalance);
									double actTransactonAmt = Double.parseDouble(treq.getAmount());

									if (doublePrefundingBalance >= actTransactonAmt) {
										MService service = mServiceRepository
												.findServiceByCode(RazorPayConstants.UPI_SERVICE);
										TransactionError transactionError = loadMoneyValidation
												.validateLoadMoneyTransactionUpi(treq.getAmount(),
														userSession.getUser().getUsername(), service,
														request.getRemoteAddr());
										if (transactionError.isValid()) {
											MTransaction transaction = loadMoneyApi.initiateUPITransaction(
													user.getUsername(), Double.parseDouble(treq.getAmount()),
													RazorPayConstants.UPI_SERVICE, true);
											if (transaction != null) {
												initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
												initiateResponse.setMessage("Transaction initiated");
												initiateResponse.setStatus("Success");
												initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
												initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
												initiateResponse.setKey_id("");
												initiateResponse.setKey_secret("");
											} else {
												initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
												initiateResponse.setMessage("Transaction failed");
											}
										} else {
											initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
											initiateResponse.setMessage(transactionError.getMessage());
										}
									} else {
										initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
										initiateResponse.setMessage(
												"Currently we are facing some issues.Please try after sometime");
									}
								} else {
									initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
									initiateResponse.setMessage(
											"Currently we are facing some issues.Please try after sometime");
								}
							} else {
								initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
								initiateResponse.setMessage("Unauthorized User");
							}
						} else {
							initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
							initiateResponse.setMessage("Please login and try again");
						}
					} else {
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						initiateResponse.setMessage("Unauthorized Role");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
					initiateResponse.setMessage("Invalid hash");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectUPI", method = RequestMethod.POST)
	public String redirectLoadMoneyUPI(@ModelAttribute TransactionInitiateRequest transactionRequest, Model model,
			ModelMap map, HttpSession session) {
		String sessionId = transactionRequest.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					MTransaction trx = transactionApi.getTransactionByRefNo(transactionRequest.getTransactionRefNo());
					if (trx != null && Status.Initiated.equals(trx.getStatus())) {
						String hash = SecurityUtil.md5(RazorPayConstants.UPI_TOKEN + "|"
								+ String.format("%.2f", trx.getAmount()) + "|" + RazorPayConstants.UPI_MERCHANT_ID + "|"
								+ transactionRequest.getTransactionRefNo());
						transactionRequest.setHash(hash);
						transactionRequest.setMerchant_id(RazorPayConstants.UPI_MERCHANT_ID);
						transactionRequest.setAdditionalInfo(sessionId);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				map.addAttribute("xxx", transactionRequest);
				return "User/UPIPay";
			}
		}
		return "redirect:/User/Login/Process";

	}

	@RequestMapping(value = "/SuccessUPI", method = RequestMethod.POST)
	public String successUPI(@ModelAttribute UPIResponseDTO upiResponse, Model model, ModelMap map, HttpSession session,
			HttpServletRequest request) {
		System.err.println("upiresponse:: " + upiResponse.toString());
		String sessionId = upiResponse.getAdditionalInfo();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		UserDTO user = userApi.getUserById(userSession.getUser().getId());
		if (user.getAuthority().contains(Authorities.USER) && user.getAuthority().contains(Authorities.AUTHENTICATED)) {
			persistingSessionRegistry.refreshLastRequest(sessionId);
			userSession.setIpAddress(request.getRemoteAddr());
			userSessionRepository.save(userSession);
			MService service = mServiceRepository.findServiceByOperatorCode("UPS");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(service.getStatus().getValue())) {
				MTransaction trans = null;
				UPIResponseDTO responseDTO = loadMoneyApi.checkUPIStatus(upiResponse.getPaymentId(),
						RazorPayConstants.UPI_MERCHANT_ID);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(responseDTO.getCode())
						&& ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(responseDTO.getStatus())) {
					MTransaction merchantTransaction = transactionApi
							.getTransactionByRefNo(responseDTO.getMerchantRefNo());
					if (merchantTransaction != null && Status.Initiated.equals(merchantTransaction.getStatus())) {
						trans = transactionApi.successLoadMoneyUPI(responseDTO.getMerchantRefNo(),
								responseDTO.getPaymentId(), responseDTO.getPayerVA(), responseDTO.getPayerName(),
								responseDTO.getPayerMobile());
						MMCards activeCard = null;
						WalletResponse cardTransferResposne = null;
						MTransaction transaction = mTransactionRepository
								.findByTransactionRefNo(responseDTO.getMerchantRefNo());
						String upiId = transaction.getUpiId();
						String[] spllitted = null;
						if (upiId != null) {
							spllitted = upiId.split("D");
						}
						if (transaction != null) {
							if (!transaction.isMdexTransaction()) {
								if (trans != null) {
									System.err.println("in wallet response");
									WalletResponse walletResponse = matchMoveApi.initiateLoadFundsToMMWalletUpi(
											userSession.getUser(), responseDTO.getAmount(),
											transaction.getTransactionRefNo(), transaction.getRetrivalReferenceNo());
									if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
										System.err.println("in success s00");
										transaction.setAuthReferenceNo(walletResponse.getAuthRetrivalNo());
										transaction.setCardLoadStatus(Status.Pending.getValue());
										List<MMCards> cards = mMCardRepository.findCardByUserId(userSession.getUser());
										for (MMCards mmCards : cards) {
											if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
												activeCard = mmCards;
											}
										}
										mTransactionRepository.save(transaction);
										cardTransferResposne = matchMoveApi.transferFundsToMMCard(userSession.getUser(),
												upiResponse.getAmount(), activeCard.getCardId());
										if (ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(cardTransferResposne.getCode())) {
											System.err.println("in transfer fund success");
											transaction.setCardLoadStatus(ResponseStatus.SUCCESS.getValue());
											mTransactionRepository.save(transaction);
											if (trans.isAndriod()) {
												return "User/SuccessUPI";
											} else {
												map.put("LoadMess", "Transaction Success");
												session.setAttribute("LoadMessage", "Transaction Success");
												return "redirect:/User/Login/Process";
											}
										} else {
											transaction.setCardLoadStatus(Status.Pending.getValue());
											transaction.setRemarks("NA");
											mTransactionRepository.save(transaction);
											if (trans.isAndriod()) {
												return "User/SuccessUPI";
											} else {
												map.put("LoadMess", "Transaction Successful.But card not loaded");
												session.setAttribute("LoadMessage",
														"Transaction Successful.But card not loaded");
												return "redirect:/User/Login/Process";
											}
										}
									} else {
										transaction.setCardLoadStatus(Status.Pending.getValue());
										transaction.setRemarks("NA");
										mTransactionRepository.save(transaction);
										if (trans.isAndriod()) {
											return "User/SuccessUPI";
										} else {
											map.put("LoadMess", "Transaction Successful.But card not loaded");
											session.setAttribute("LoadMessage",
													"Transaction Successful.But card not loaded");
											return "redirect:/User/Login/Process";
										}
									}
								}
							} else {
								transaction.setStatus(Status.Success);
								mTransactionRepository.save(transaction);
								MdexRequestLogs requestEntries = mdexRequestLogsRepository
										.findByTransaction(transaction);
								MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();
								if (requestEntries != null) {
									switch (requestEntries.getTopUpType().toLowerCase()) {
									case "prepaid":
										MdexTransactionRequestDTO requ = new MdexTransactionRequestDTO();
										requ.setServiceProvider(requestEntries.getServiceCode());
										requ.setAmount(transaction.getAmount() + "");
										requ.setTransactionId(spllitted[0]);
										requ.setMobileNumber(requestEntries.getRequest());
										requ.setArea(requestEntries.getArea());
										resp = iMdexApi.doPrepaid(requ);
										if (resp.isSuccess()) {
											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {
											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);
											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());
												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;
									case "postpaid":
										MdexTransactionRequestDTO request1 = new MdexTransactionRequestDTO();
										request1.setServiceProvider(requestEntries.getServiceCode());
										request1.setMobileNumber(requestEntries.getRequest());
										request1.setAmount(transaction.getAmount() + "");
										request1.setTransactionId(spllitted[0]);
										request1.setAdditionalInfo(requestEntries.getAdditionalInfo());
										resp = iMdexApi.doPostpaid(request1);
										if (resp.isSuccess()) {

											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);
											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());
												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}
										}
										break;
									case "datacard":
										MdexTransactionRequestDTO request2 = new MdexTransactionRequestDTO();
										request2.setServiceProvider(requestEntries.getServiceCode());
										request2.setMobileNumber(requestEntries.getRequest());
										request2.setAmount(transaction.getAmount() + "");
										request2.setTransactionId(spllitted[0]);
										request2.setAdditionalInfo(requestEntries.getAdditionalInfo());
										resp = iMdexApi.doDataCard(request2);
										if (resp.isSuccess()) {

											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;
									case "dth":
										MdexTransactionRequestDTO request3 = new MdexTransactionRequestDTO();
										request3.setServiceProvider(requestEntries.getServiceCode());
										request3.setAmount(transaction.getAmount() + "");
										request3.setTransactionId(spllitted[0]);
										request3.setDthNumber(requestEntries.getDthNumber());
										request3.setAdditionalInfo(requestEntries.getAdditionalInfo());
										resp = iMdexApi.dodTH(request3);
										if (resp.isSuccess()) {

											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												client.addFilter(new LoggingFilter(System.out));

												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}
										}
										break;
									case "landline":
										MdexTransactionRequestDTO request4 = new MdexTransactionRequestDTO();
										request4.setServiceProvider(requestEntries.getServiceCode());
										request4.setAccountNumber(requestEntries.getAccountNumber());
										request4.setAdditionalInfo(requestEntries.getAdditionalInfo());
										request4.setLandlineNumber(requestEntries.getLandlineNumber());
										request4.setStdCode(requestEntries.getStdCode());
										request4.setAmount(transaction.getAmount() + "");
										request4.setTransactionId(spllitted[0]);
										resp = iMdexApi.doLandLine(request4);
										if (resp.isSuccess()) {
											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;
									case "electricity":
										MdexTransactionRequestDTO request5 = new MdexTransactionRequestDTO();
										request5.setServiceProvider(requestEntries.getServiceCode());
										request5.setAccountNumber(requestEntries.getAccountNumber());
										request5.setAdditionalInfo(requestEntries.getAdditionalInfo());
										request5.setCycleNumber(requestEntries.getCycleNumber());
										request5.setCityName(requestEntries.getCityName());
										request5.setBillingUnit(requestEntries.getBillingUnit());
										request5.setProcessingCycle(requestEntries.getProcessingCycle());
										request5.setTransactionId(spllitted[0]);
										request5.setAmount(transaction.getAmount() + "");
										resp = iMdexApi.doElectricity(request5);
										if (resp.isSuccess()) {

											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;
									case "insurance":
										MdexTransactionRequestDTO request6 = new MdexTransactionRequestDTO();
										request6.setServiceProvider(requestEntries.getServiceCode());
										request6.setPolicyNumber(requestEntries.getPolicyNumber());
										request6.setAmount(transaction.getAmount() + "");
										request6.setTransactionId(spllitted[0]);
										request6.setAdditionalInfo(requestEntries.getAdditionalInfo());
										request6.setPolicyDate(requestEntries.getPolicyDate());
										resp = iMdexApi.doInsurance(request6);
										if (resp.isSuccess()) {

											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {

											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;
									case "gas":
										MdexTransactionRequestDTO request7 = new MdexTransactionRequestDTO();
										request7.setServiceProvider(requestEntries.getServiceCode());
										request7.setAdditionalInfo(requestEntries.getAdditionalInfo());
										request7.setAccountNumber(requestEntries.getAccountNumber());
										request7.setAmount(transaction.getAmount() + "");
										request7.setTransactionId(spllitted[0]);

										request7.setBillGroupNumber(requestEntries.getBillGroupNumber());
										resp = iMdexApi.doGas(request7);
										if (resp.isSuccess()) {
											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										} else {
											MTransaction debitTransaction = mTransactionRepository
													.findByTransactionRefNo(transaction.getUpiId());
											debitTransaction.setStatus(Status.Failed);
											debitTransaction.setMdexTransactionStatus("Failed");
											mTransactionRepository.save(debitTransaction);
											requestEntries.setStatus(false);
											mdexRequestLogsRepository.save(requestEntries);

											JSONObject payload = new JSONObject();
											try {
												payload.put("token", RazorPayConstants.UPI_TOKEN);
												payload.put("key", RazorPayConstants.UPI_MERCHANT_ID);
												payload.put("transactionId", transaction.getRetrivalReferenceNo());

												Client client = Client.create();
												WebResource webResource = client
														.resource("https://mycashier.in/UPI/RefundMoney");
												ClientResponse clientResponse = webResource.accept("application/json")
														.type("application/json").post(ClientResponse.class, payload);
												String stringResponse = clientResponse.getEntity(String.class);
												JSONObject respObj = new JSONObject(stringResponse);
												if (respObj != null) {
													String code = respObj.getString("code");
													if (clientResponse.getStatus() == 200
															&& code.equalsIgnoreCase("S00")) {
														transaction.setStatus(Status.Refunded);
													}
												}
												transaction.setSuspicious(false);
												mTransactionRepository.save(transaction);
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}
										break;

									case "bus":
										MdexTransactionRequestDTO request8 = new MdexTransactionRequestDTO();
										request8.setAmount(transaction.getAmount() + "");
										request8.setSeatHoldId(requestEntries.getSeatHoldId());
										request8.setTransactionId(spllitted[0]);
										resp = iMdexApi.doBusPayment(request8);
										if (resp.isSuccess()) {
											requestEntries.setStatus(true);
											mdexRequestLogsRepository.save(requestEntries);
										}
										break;
									}

									return "User/SuccessUPI";

								} else {
									return "User/FailureUPI";

								}
							}
						}
					} else {
						trans = transactionApi.failedLoadMoneyUPI(upiResponse.getMerchantRefNo());
						trans.setCardLoadStatus("Failed");
						mTransactionRepository.save(trans);
						if (trans.isAndriod()) {
							return "User/FailureUPI";
						} else {
							map.put("LoadMess", "Transaction Failed");
							session.setAttribute("LoadMessage", "Transaction Failed");
							return "redirect:/User/Login/Process";
						}
					}
				}
			} else {
				return "User/FailureUPI";
			}
		}
		return "redirect:/User/Login/Process";

	}

	@RequestMapping(value = "/RedirectPG", method = RequestMethod.POST)
	public String redirectPG(@ModelAttribute TransactionInitiateRequest transactionRequest, Model model, ModelMap map,
			HttpSession session) {
		String sessionId = transactionRequest.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				PGTransaction pgTrx = new PGTransaction();

				try {

					MTransaction trx = transactionApi.getTransactionByRefNo(transactionRequest.getTransactionRefNo());
					if (trx != null && Status.Initiated.equals(trx.getStatus())) {
						pgTrx.setSaleUrl(RazorPayConstants.SALE_URL);
						pgTrx.setMerchantId(RazorPayConstants.MERCHANT_ID);
						pgTrx.setMerchantTxnNo(trx.getTransactionRefNo());
						pgTrx.setAmount(String.valueOf(trx.getAmount()));
						pgTrx.setCurrencyCode(RazorPayConstants.CURRENCY_CODE);
						pgTrx.setPayType(RazorPayConstants.PAY_TYPE);
						pgTrx.setCustomerEmailId(userSession.getUser().getUserDetail().getEmail());
						pgTrx.setTransactionType(RazorPayConstants.TRANSACTION_TYPE);
						pgTrx.setReturnURL(RazorPayConstants.RETURN_URL);
						pgTrx.setTxnDate(sdf.format(trx.getCreated()));
						pgTrx.setCustomerMobileNo(userSession.getUser().getUsername());
						String secureHash = RazorPayConstants.composeMessage(pgTrx);
						pgTrx.setSecureHash(secureHash);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				map.addAttribute("pg", pgTrx);
				return "User/Pay";
			}
		}
		return "User/FailureUPI";

	}

	/**
	 * Handler for PG
	 * 
	 * @param transactionRequest
	 * @param model
	 * @param map
	 * @param session
	 * @return
	 */

	@RequestMapping(value = "/PGHandler", method = RequestMethod.POST)
	public String pgHandler(@ModelAttribute PGHandlerDTO transactionRequest, Model model, ModelMap map,
			HttpSession session) {
		System.err.println("Request::" + transactionRequest.toString());
		MTransaction transaction = transactionApi.getTransactionByRefNo(transactionRequest.getMerchantTxnNo());
		if (transaction != null) {
			if (transaction.getStatus().getValue().equalsIgnoreCase("Initiated")) {
				PGStatusCheckDTO statusCheck = transactionApi.checkPGStatus(transaction.getTransactionRefNo(),
						transactionRequest.getTxnID());
				if (statusCheck.getResponseCode().equalsIgnoreCase("0000")) {
					if (Double.parseDouble(statusCheck.getAmount()) == transaction.getAmount()) {
						MTransaction successTransaction = transactionApi.successLoadMoneyCustom(
								transaction.getTransactionRefNo(), transactionRequest.getTxnID());
						if (successTransaction != null
								&& successTransaction.getStatus().getValue().equalsIgnoreCase("Success")) {
							MUser user = userApi.findByUserAccount(successTransaction.getAccount());
							if (user != null) {
								double amount = successTransaction.getAmount()
										- successTransaction.getCommissionEarned();
								WalletResponse walletResponse = matchMoveApi.initiateLoadFundsToMMWalletPG(user,
										String.valueOf(amount), successTransaction.getTransactionRefNo(),
										successTransaction.getRetrivalReferenceNo());
								if (walletResponse != null) {
									System.err.println("wallet response is not null");
									if (walletResponse.getCode().equalsIgnoreCase("S00")) {
										successTransaction.setAuthReferenceNo(walletResponse.getAuthRetrivalNo());
										successTransaction.setCardLoadStatus("Pending");

										List<MMCards> cards = mMCardRepository.findCardByUserId(user);
										MMCards activeCard = null;
										for (MMCards mmCards : cards) {
											if (mmCards.getStatus().equalsIgnoreCase("Active")) {
												activeCard = mmCards;
											}
										}
										WalletResponse cardTransferResponse = matchMoveApi.transferFundsToMMCard(user,
												String.valueOf(amount), activeCard.getCardId());
										if (cardTransferResponse.getCode().equalsIgnoreCase("S00")) {
											System.err.println("in transfer fund success");
											successTransaction.setCardLoadStatus("Success");
										}
										mTransactionRepository.save(successTransaction);
										return "User/SuccessUPI";
									}
								}
							}

						}
					} else {
						transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(),
								transactionRequest.getTxnID());
						return "User/FailureUPI";

					}
				} else {
					transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(),
							transactionRequest.getTxnID());
					return "User/FailureUPI";

				}
			} else {
				transactionApi.failedLoadMoneyCustom(transaction.getTransactionRefNo(), transactionRequest.getTxnID());
				return "User/FailureUPI";

			}
		}
		return "User/SuccessUPI";
	}

	@RequestMapping(value = "/FailureUPI", method = RequestMethod.POST)
	public String failureUPI(@ModelAttribute UPIResponseDTO upiResponse, Model model, ModelMap map,
			HttpSession session) {
		String sessionId = upiResponse.getAdditionalInfo();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				MTransaction trans = transactionApi.failedLoadMoneyUPI(upiResponse.getMerchantRefNo());
				if (trans != null && trans.isAndriod()) {
					return "User/FailureUPI";
				} else {
					map.put("LoadMess", "Transaction Failed");
					return "redirect:/User/Login/Process";
				}
			}
		} else {
			return "User/FailureUPI";
		}
		return "redirect:/User/Login/Process";

	}

	/**
	 * CREDIT CASHBACK
	 */

	@RequestMapping(value = "/CreditCashBack", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TransactionInitiateResponse> creditCashBack(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest request,
			HttpServletResponse response) {
		TransactionInitiateResponse initiateResponse = new TransactionInitiateResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(transactionRequest.getSessionId(), hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = transactionRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						UserKycResponse resp = matchMoveApi.getConsumers();
						if (resp.getCode().equalsIgnoreCase("S00")) {
							String prefundingBalance = resp.getPrefundingBalance();
							double doublePrefundingBalance = Double.parseDouble(prefundingBalance);
							double actTransactonAmt = Double.parseDouble(transactionRequest.getAmount());

							if (doublePrefundingBalance >= actTransactonAmt) {

								MService service = mServiceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
								TransactionError transactionError = loadMoneyValidation.validateLoadMoneyTransaction(
										transactionRequest.getAmount(), userSession.getUser().getUsername(), service);
								if (transactionError.isValid()) {
									MTransaction transaction = loadMoneyApi.initiateTransaction(user.getUsername(),
											Double.parseDouble(transactionRequest.getAmount()),
											RazorPayConstants.SERVICE_CODE);
									if (transaction != null) {
										initiateResponse.setCode(ResponseStatus.SUCCESS.getValue());
										initiateResponse.setMessage("Transaction initiated");
										initiateResponse.setStatus("Success");
										initiateResponse.setTransactionRefNo(transaction.getTransactionRefNo());
										initiateResponse.setAmount(String.valueOf(transaction.getAmount()));
										initiateResponse.setKey_id(RazorPayConstants.KEY_ID);
										initiateResponse.setKey_secret(RazorPayConstants.KEY_SECRET);

									} else {
										initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
										initiateResponse.setMessage("Transaction failed");
									}
								} else {
									initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
									initiateResponse.setMessage(transactionError.getMessage());
								}
							} else {
								initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
								initiateResponse
										.setMessage("Currently we are facing some issues.Please try after sometime");
							}
						} else {
							initiateResponse.setCode(ResponseStatus.FAILURE.getValue());
							initiateResponse
									.setMessage("Currently we are facing some issues.Please try after sometime");
						}
					} else {
						initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						initiateResponse.setMessage("Unauthorized User");
					}
				} else {
					initiateResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
					initiateResponse.setMessage("Please login and try again");
				}
			} else {
				initiateResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				initiateResponse.setMessage("Unauthorized Role");
			}
		} else {
			initiateResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
			initiateResponse.setMessage("Invalid hash");
		}

		return new ResponseEntity<TransactionInitiateResponse>(initiateResponse, HttpStatus.OK);
	}

}
