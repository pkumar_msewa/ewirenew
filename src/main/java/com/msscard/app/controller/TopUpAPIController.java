package com.msscard.app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cashiercards.enums.Device;
import com.msscard.app.api.IMailSenderApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MdexTransactionRequestDTO;
import com.msscard.app.model.request.OperatorCircleResponse;
import com.msscard.app.model.request.TopupPlansList;
import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.app.model.response.MdexTransactionResponseDTO;
import com.msscard.app.model.response.RechargeAppResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.MMCards;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MdexRequestLogs;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Role;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MdexRequestLogsRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.MailTemplate;
import com.msscard.validation.LoadMoneyValidation;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping(value = "Api/{version}/{role}/{device}/{language}/BillPayment")
public class TopUpAPIController {

	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final IMatchMoveApi matchMoveApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final LoadMoneyValidation loadMoneyValidation;
	private final MdexRequestLogsRepository mdexRequestLogsRepository;
	private final MTransactionRepository mTransactionRepository;
	private final IMdexApi iMdexApi;
	private final MMCardRepository cardRepository;
	private final ISMSSenderApi senderApi;
	private final IMailSenderApi mailSenderApi;

	public TopUpAPIController(ITransactionApi transactionApi, UserSessionRepository userSessionRepository,
			IUserApi userApi, IMatchMoveApi matchMoveApi, PersistingSessionRegistry persistingSessionRegistry,
			LoadMoneyValidation loadMoneyValidation, MdexRequestLogsRepository mdexRequestLogsRepository,
			MTransactionRepository mTransactionRepository, IMdexApi iMdexApi, MMCardRepository cardRepository,
			ISMSSenderApi senderApi, IMailSenderApi mailSenderApi) {
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.matchMoveApi = matchMoveApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.loadMoneyValidation = loadMoneyValidation;
		this.mdexRequestLogsRepository = mdexRequestLogsRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.iMdexApi = iMdexApi;
		this.cardRepository = cardRepository;
		this.senderApi = senderApi;
		this.mailSenderApi = mailSenderApi;
	}

	@RequestMapping(value = "/GetCircleOperator", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperatorCircleResponse> getcircleOperator(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TransactionInitiateRequest req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		OperatorCircleResponse result = new OperatorCircleResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.getOperatorAndCircles(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<OperatorCircleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetTelcoMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TopupPlansList> getTelcoMobile(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TransactionInitiateRequest req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TopupPlansList result = new TopupPlansList();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.getTelcoBasedOnMobile(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TopupPlansList>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetPlansPrepaidData", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TopupPlansList> getPlans(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MdexTransactionRequestDTO req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TopupPlansList result = new TopupPlansList();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.getPlans(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TopupPlansList>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DueAmount", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TopupPlansList> getDueAmount(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MdexTransactionRequestDTO req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TopupPlansList result = new TopupPlansList();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.dueAmount(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TopupPlansList>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RechargeAppResponse> recharge(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestBody TransactionInitiateRequest transactionRequest, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {
		System.err.println(transactionRequest);
		RechargeAppResponse result = new RechargeAppResponse();
		String sessionId = transactionRequest.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			try {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						if (userSession != null) {
							UserDTO user = userApi.getUserById(userSession.getUser().getId());
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								String activeCardId = null;
								List<MMCards> cardList = cardRepository.getCardsListByUser(userSession.getUser());
								if (cardList != null && cardList.size() > 0) {
									for (MMCards mmCards : cardList) {
										if (mmCards.isHasPhysicalCard()) {
											if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
												activeCardId = mmCards.getCardId();
											}
										} else if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())) {
											activeCardId = mmCards.getCardId();
										}
									}
								} else {
									result.setCode(ResponseStatus.FAILURE);
									return new ResponseEntity<RechargeAppResponse>(result, HttpStatus.OK);
								}
								if (activeCardId == null) {
									result.setCode(ResponseStatus.FAILURE);
									return new ResponseEntity<RechargeAppResponse>(result, HttpStatus.OK);
								}
								double balance = matchMoveApi.getBalanceApp(userSession.getUser());
								double transactionAmt = Double.parseDouble(transactionRequest.getAmount());
								if (balance < transactionAmt) {
									result.setCode(ResponseStatus.INSUFFICIENT_FUNDS);
									return new ResponseEntity<>(result, HttpStatus.OK);
								}

								transactionRequest.setUser(userSession.getUser());
								TransactionError transactionError = loadMoneyValidation
										.validateMdexTimestampTransaction(transactionAmt,
												userSession.getUser().getUsername());
								if (transactionError.isValid()) {
									MTransaction debitTransaction = transactionApi
											.initiateMdexTransactionDebitnew(transactionRequest);
									if (debitTransaction != null) {
										MdexRequestLogs requestEntries = new MdexRequestLogs();
										requestEntries.setRequest(transactionRequest.getMobileNumber());
										requestEntries.setServiceCode(transactionRequest.getServiceCode());
										requestEntries.setArea(transactionRequest.getArea());
										requestEntries.setTransaction(debitTransaction);
										requestEntries.setTopUpType(transactionRequest.getTopUpType());
										requestEntries.setSenderUser(user.getUsername());
										requestEntries.setAdditionalInfo(transactionRequest.getAdditionalInfo());
										requestEntries.setAccountNumber(transactionRequest.getAccountNumber() + "");
										requestEntries.setBillGroupNumber(transactionRequest.getBillGroupNumber());
										requestEntries.setBillingUnit(transactionRequest.getBillingUnit());
										requestEntries.setDthNumber(transactionRequest.getDthNumber());
										requestEntries.setLandlineNumber(transactionRequest.getLandLineNumber());
										requestEntries.setStdCode(transactionRequest.getStdCode());
										requestEntries.setCycleNumber(transactionRequest.getCycleNumber());
										requestEntries.setCityName(transactionRequest.getCityName());
										requestEntries.setProcessingCycle(transactionRequest.getProcessingCycle());
										requestEntries.setPolicyNumber(transactionRequest.getPolicyNumber());
										requestEntries.setPolicyDate(transactionRequest.getPolicyDate());
										requestEntries.setReferenceNumber(transactionRequest.getReferenceNumber());
										mdexRequestLogsRepository.save(requestEntries);
										result.setTransactionRefNo(debitTransaction.getTransactionRefNo());

										UserKycResponse debitResponse = matchMoveApi.debitFromCardInHouse(
												userSession.getUser(), activeCardId, debitTransaction.getAmount() + "",
												transactionRequest.getTopUpType());

										if (debitResponse != null && !ResponseStatus.SUCCESS.getValue()
												.equalsIgnoreCase(debitResponse.getCode())) {
											result.setCode(ResponseStatus.FAILURE);
											return new ResponseEntity<>(result, HttpStatus.OK);
										} else {
											debitTransaction.setRetrivalReferenceNo(debitResponse.getAuthID());
											mTransactionRepository.save(debitTransaction);
										}

										double balance1 = matchMoveApi.getBalanceApp(userSession.getUser());

										if (balance != (balance1 + debitTransaction.getAmount())) {
											debitTransaction.setRetrivalReferenceNo(
													"Amount debited successfully, but balance is mismatch.");
											mTransactionRepository.save(debitTransaction);
											result.setCode(ResponseStatus.FAILURE);
											return new ResponseEntity<>(result, HttpStatus.OK);
										}
									}

									String upiId = debitTransaction.getTransactionRefNo();
									String[] spllitted = null;
									if (upiId != null) {
										spllitted = upiId.split("D");
									}
									MdexRequestLogs requestEntries = mdexRequestLogsRepository
											.findByTransaction(debitTransaction);
									System.err.println("returned transaction:::" + debitTransaction);
									MdexTransactionResponseDTO resp = new MdexTransactionResponseDTO();

									if (requestEntries != null) {
										switch (requestEntries.getTopUpType().toLowerCase()) {
										case "prepaid":
											MdexTransactionRequestDTO request = new MdexTransactionRequestDTO();
											request.setServiceProvider(requestEntries.getServiceCode());
											request.setAmount(debitTransaction.getAmount() + "");
											request.setTransactionId(spllitted[0]);
											request.setMobileNumber(requestEntries.getRequest());
											request.setArea(requestEntries.getArea());
											resp = iMdexApi.doPrepaid(request);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendMobileRecharge("Prepaid Recharge Successful",
														MailTemplate.MOBILE_RECHARGE, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction.setMdexTransactionStatus("Failed");
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);
												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());
												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "postpaid":
											MdexTransactionRequestDTO request1 = new MdexTransactionRequestDTO();
											request1.setServiceProvider(requestEntries.getServiceCode());
											request1.setMobileNumber(requestEntries.getRequest());
											request1.setAmount(debitTransaction.getAmount() + "");
											request1.setTransactionId(spllitted[0]);
											request1.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request1.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doPostpaid(request1);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendBillPayment("Postpaid Bill Payment Successful",
														MailTemplate.BILL_PAYMENT, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction.setMdexTransactionStatus(Status.Failed.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "datacard":
											MdexTransactionRequestDTO request2 = new MdexTransactionRequestDTO();
											request2.setServiceProvider(requestEntries.getServiceCode());
											request2.setMobileNumber(requestEntries.getRequest());
											request2.setAmount(debitTransaction.getAmount() + "");
											request2.setTransactionId(spllitted[0]);
											request2.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request2.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doDataCard(request2);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendMobileRecharge("Data Card Recharge Successful",
														MailTemplate.MOBILE_RECHARGE, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "dth":
											MdexTransactionRequestDTO request3 = new MdexTransactionRequestDTO();
											request3.setServiceProvider(requestEntries.getServiceCode());
											request3.setAmount(debitTransaction.getAmount() + "");
											request3.setTransactionId(spllitted[0]);
											request3.setDthNumber(requestEntries.getDthNumber());
											request3.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request3.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.dodTH(request3);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendMobileRecharge("DTH Recharge Successful",
														MailTemplate.MOBILE_RECHARGE, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "landline":
											MdexTransactionRequestDTO request4 = new MdexTransactionRequestDTO();
											request4.setServiceProvider(requestEntries.getServiceCode());
											request4.setAccountNumber(requestEntries.getAccountNumber());
											request4.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request4.setLandlineNumber(requestEntries.getLandlineNumber());
											request4.setStdCode(requestEntries.getStdCode());
											request4.setAmount(debitTransaction.getAmount() + "");
											request4.setTransactionId(spllitted[0]);
											request4.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doLandLine(request4);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendBillPayment("Landline Bill Payment Successful",
														MailTemplate.BILL_PAYMENT, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "electricity":
											MdexTransactionRequestDTO request5 = new MdexTransactionRequestDTO();
											request5.setServiceProvider(requestEntries.getServiceCode());
											request5.setAccountNumber(requestEntries.getAccountNumber());
											request5.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request5.setCycleNumber(requestEntries.getCycleNumber());
											request5.setCityName(requestEntries.getCityName());
											request5.setBillingUnit(requestEntries.getBillingUnit());
											request5.setProcessingCycle(requestEntries.getProcessingCycle());
											request5.setTransactionId(spllitted[0]);
											request5.setAmount(debitTransaction.getAmount() + "");
											request5.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doElectricity(request5);
											if (resp.isSuccess()) {

												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendBillPayment("Electricity Bill Payment Successful",
														MailTemplate.BILL_PAYMENT, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);

												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "insurance":
											MdexTransactionRequestDTO request6 = new MdexTransactionRequestDTO();
											request6.setServiceProvider(requestEntries.getServiceCode());
											request6.setPolicyNumber(requestEntries.getPolicyNumber());
											request6.setAmount(debitTransaction.getAmount() + "");
											request6.setTransactionId(spllitted[0]);
											request6.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request6.setPolicyDate(requestEntries.getPolicyDate());
											request6.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doInsurance(request6);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);

											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "gas":
											MdexTransactionRequestDTO request7 = new MdexTransactionRequestDTO();
											request7.setServiceProvider(requestEntries.getServiceCode());
											request7.setAdditionalInfo(requestEntries.getAdditionalInfo());
											request7.setAccountNumber(requestEntries.getAccountNumber());
											request7.setAmount(debitTransaction.getAmount() + "");
											request7.setTransactionId(spllitted[0]);

											request7.setBillGroupNumber(requestEntries.getBillGroupNumber());
											request7.setReferenceNumber(transactionRequest.getReferenceNumber());
											resp = iMdexApi.doGas(request7);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);
												mailSenderApi.sendBillPayment("Gas Bill Payment Successful",
														MailTemplate.BILL_PAYMENT, userSession.getUser(),
														debitTransaction);
											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										case "bus":
											MdexTransactionRequestDTO request8 = new MdexTransactionRequestDTO();
											request8.setAmount(debitTransaction.getAmount() + "");
											request8.setSeatHoldId(requestEntries.getSeatHoldId());
											request8.setTransactionId(spllitted[0]);
											resp = iMdexApi.doBusPayment(request8);
											if (resp.isSuccess()) {
												requestEntries.setStatus(true);
												mdexRequestLogsRepository.save(requestEntries);
												result.setCode(ResponseStatus.SUCCESS);

											} else {
												debitTransaction.setStatus(Status.Failed);
												debitTransaction
														.setMdexTransactionStatus(ResponseStatus.FAILURE.getValue());
												mTransactionRepository.save(debitTransaction);
												requestEntries.setStatus(false);
												mdexRequestLogsRepository.save(requestEntries);

												// WalletResponse walletResp = matchMoveApi
												// .reversalToWalletForRechargeFailure(userSession.getUser(),
												// String.valueOf(debitTransaction.getAmount()),
												// debitTransaction.getService().getCode(),
												// debitTransaction.getTransactionRefNo(),
												// debitTransaction.getDescription());

												WalletResponse walletResp = matchMoveApi.refundDebitTxns(
														debitTransaction.getRetrivalReferenceNo(),
														userSession.getUser());

												if (walletResp != null) {
													if (ResponseStatus.SUCCESS.getValue()
															.equalsIgnoreCase(walletResp.getCode())) {
														WalletResponse tranferCard = matchMoveApi.transferFundsToMMCard(
																userSession.getUser(),
																String.valueOf(debitTransaction.getAmount()),
																activeCardId);
														if (tranferCard != null && ResponseStatus.SUCCESS.getValue()
																.equalsIgnoreCase(tranferCard.getCode())) {
															debitTransaction.setStatus(Status.Refunded);
															String authRefNo = walletResp.getAuthRetrivalNo();
															debitTransaction.setRetrivalReferenceNo(authRefNo);
															debitTransaction.setSuspicious(false);
														} else {
															debitTransaction.setStatus(Status.Refund_Failed);
														}
													} else {
														debitTransaction.setStatus(Status.Refund_Failed);
													}
												} else {
													debitTransaction.setStatus(Status.Refund_Failed);
												}
												mTransactionRepository.save(debitTransaction);
												result.setCode(ResponseStatus.RECHARGE_FAILURE_REFUNDED);
												senderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL,
														SMSTemplete.MDEX_FAILURE, userSession.getUser(),
														debitTransaction.getTransactionRefNo());
											}
											break;
										}
										return new ResponseEntity<RechargeAppResponse>(result, HttpStatus.OK);
									} else {
										result.setCode(ResponseStatus.FAILURE);
										result.setMessage("Transaction failed.");
									}
								} else {
									result.setCode(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									return new ResponseEntity<>(result, HttpStatus.OK);
								}
							} else {
								result.setCode(ResponseStatus.INVALID_SESSION);
							}
						} else {
							result.setCode(ResponseStatus.FAILURE);
						}
					} else {
						result.setCode(ResponseStatus.UNAUTHORIZED_USER);
						result.setCode(ResponseStatus.INVALID_SESSION);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setCode(ResponseStatus.EXCEPTION_OCCURED);
				return new ResponseEntity<RechargeAppResponse>(result, HttpStatus.OK);
			}
		} else {
			result.setCode(ResponseStatus.INVALID_SESSION);
		}
		return new ResponseEntity<RechargeAppResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetMnp", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TopupPlansList> getMnp(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TransactionInitiateRequest req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TopupPlansList result = new TopupPlansList();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.getMnpLookUp(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TopupPlansList>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetBrowsePlans", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TopupPlansList> getBrowse(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TransactionInitiateRequest req,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		TopupPlansList result = new TopupPlansList();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = iMdexApi.getBrowsePlans(req);
				} else {
					result.setCode(ResponseStatus.FAILURE.getValue());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<TopupPlansList>(result, HttpStatus.OK);
	}

}