package com.msscard.app.model.response;

import com.msscard.model.Status;

public class OrderListDTO {
	
   private String transactionNumber;
   private double amount;
   private Status status;
   private String serviceName;
   private String serviceCode;
   private int service_id;
   private String code;
   private String imageUrl;
   private long totalPages;
   private String date;
   private String description;
   

   

public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getTransactionNumber() {
	return transactionNumber;
}
public void setTransactionNumber(String transactionNumber) {
	this.transactionNumber = transactionNumber;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public Status getStatus() {
	return status;
}
public void setStatus(Status status) {
	this.status = status;
}
public String getServiceName() {
	return serviceName;
}
public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}

public String getServiceCode() {
	return serviceCode;
}
public void setServiceCode(String serviceCode) {
	this.serviceCode = serviceCode;
}
public int getService_id() {
	return service_id;
}
public void setService_id(int service_id) {
	this.service_id = service_id;
}
public String getCode() {
	return code;
}
public void setCode(String code) {
	this.code = code;
}
public long getTotalPages() {
	return totalPages;
}
public void setTotalPages(long totalPages) {
	this.totalPages = totalPages;
}
public String getImageUrl() {
	return imageUrl;
}
public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
}

@Override
public String toString() {
	return "OrderListDTO [transactionNumber=" + transactionNumber + ", amount=" + amount + ", status=" + status
			+ ", serviceName=" + serviceName + ", serviceCode=" + serviceCode + ", service_id=" + service_id + ", code="
			+ code + ", imageUrl=" + imageUrl + ", totalPages=" + totalPages + ", date=" + date + ", description="
			+ description + "]";
}
   
   
   

}
