package com.msscard.app.model.response;

public class PGStatusCheckDTO {

	private String responseCode;
	private String respDescription;
	private String merchantTxnNo;
	private PGStatus pgStatus;
	private String txnID;
	private String txnRespDescription;
	private String paymentDateTime;
	private String txnAuthID;
	private String secureHash;
	private String amount;
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getRespDescription() {
		return respDescription;
	}
	public void setRespDescription(String respDescription) {
		this.respDescription = respDescription;
	}
	public String getMerchantTxnNo() {
		return merchantTxnNo;
	}
	public void setMerchantTxnNo(String merchantTxnNo) {
		this.merchantTxnNo = merchantTxnNo;
	}
	public PGStatus getPgStatus() {
		return pgStatus;
	}
	public void setPgStatus(PGStatus pgStatus) {
		this.pgStatus = pgStatus;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getTxnRespDescription() {
		return txnRespDescription;
	}
	public void setTxnRespDescription(String txnRespDescription) {
		this.txnRespDescription = txnRespDescription;
	}
	public String getPaymentDateTime() {
		return paymentDateTime;
	}
	public void setPaymentDateTime(String paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}
	public String getTxnAuthID() {
		return txnAuthID;
	}
	public void setTxnAuthID(String txnAuthID) {
		this.txnAuthID = txnAuthID;
	}
	public String getSecureHash() {
		return secureHash;
	}
	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
}
