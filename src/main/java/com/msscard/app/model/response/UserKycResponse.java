package com.msscard.app.model.response;

public class UserKycResponse {

	private String code;

	private String message;

	private Object details;

	private Object virtualTransactions;

	private Object physicalCardTransactions;

	private String prefundingBalance;

	private String merchantName;

	private String amount;

	private String transactionType;

	private String customMessage;

	private String indicator;

	private String date;

	private String balance;

	private String status;

	private Object customVirtualTransaction;

	private Object customPhysicalTransaction;

	private String mmUserId;

	private String username;

	private String authID;

	private Object cardTransactions;

	private String debitStatus;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public Object getVirtualTransactions() {
		return virtualTransactions;
	}

	public void setVirtualTransactions(Object virtualTransactions) {
		this.virtualTransactions = virtualTransactions;
	}

	public Object getPhysicalCardTransactions() {
		return physicalCardTransactions;
	}

	public void setPhysicalCardTransactions(Object physicalCardTransactions) {
		this.physicalCardTransactions = physicalCardTransactions;
	}

	public String getPrefundingBalance() {
		return prefundingBalance;
	}

	public void setPrefundingBalance(String prefundingBalance) {
		this.prefundingBalance = prefundingBalance;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getCustomVirtualTransaction() {
		return customVirtualTransaction;
	}

	public void setCustomVirtualTransaction(Object customVirtualTransaction) {
		this.customVirtualTransaction = customVirtualTransaction;
	}

	public Object getCustomPhysicalTransaction() {
		return customPhysicalTransaction;
	}

	public void setCustomPhysicalTransaction(Object customPhysicalTransaction) {
		this.customPhysicalTransaction = customPhysicalTransaction;
	}

	public String getMmUserId() {
		return mmUserId;
	}

	public void setMmUserId(String mmUserId) {
		this.mmUserId = mmUserId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthID() {
		return authID;
	}

	public void setAuthID(String authID) {
		this.authID = authID;
	}

	public Object getCardTransactions() {
		return cardTransactions;
	}

	public void setCardTransactions(Object cardTransactions) {
		this.cardTransactions = cardTransactions;
	}

	public String getDebitStatus() {
		return debitStatus;
	}

	public void setDebitStatus(String debitStatus) {
		this.debitStatus = debitStatus;
	}

}