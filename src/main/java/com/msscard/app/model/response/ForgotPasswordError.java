package com.msscard.app.model.response;

public class ForgotPasswordError {

	private String errorLength;
	private boolean valid;
	private String message;
	
	public String getErrorLength() {
		return errorLength;
	}
	public void setErrorLength(String errorLength) {
		this.errorLength = errorLength;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
}
