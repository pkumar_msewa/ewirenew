package com.msscard.app.model.response;

public class WalletResponse {

	private String status;
	private String code;
	private String message;
	private Object details;
	private String expiryDate;
	private String issueDate;
	private String walletNumber;
	private String walletId;
	private String withholdingAmt;
	private String availableAmt;
	private boolean card_status;
	private String mmUserId;
	private String cardId;
	private String cvv;
	private String holderName;
	private int cardCount;
	private boolean countLimit;
	private String authRetrivalNo;
	private String activationCode;
	
	
	
	
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public String getAuthRetrivalNo() {
		return authRetrivalNo;
	}
	public void setAuthRetrivalNo(String authRetrivalNo) {
		this.authRetrivalNo = authRetrivalNo;
	}
	public boolean isCountLimit() {
		return countLimit;
	}
	public void setCountLimit(boolean countLimit) {
		this.countLimit = countLimit;
	}
	public int getCardCount() {
		return cardCount;
	}
	public void setCardCount(int cardCount) {
		this.cardCount = cardCount;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getMmUserId() {
		return mmUserId;
	}
	public void setMmUserId(String mmUserId) {
		this.mmUserId = mmUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getWalletNumber() {
		return walletNumber;
	}
	public void setWalletNumber(String walletNumber) {
		this.walletNumber = walletNumber;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getWithholdingAmt() {
		return withholdingAmt;
	}
	public void setWithholdingAmt(String withholdingAmt) {
		this.withholdingAmt = withholdingAmt;
	}
	public String getAvailableAmt() {
		return availableAmt;
	}
	public void setAvailableAmt(String availableAmt) {
		this.availableAmt = availableAmt;
	}
	public boolean isCard_status() {
		return card_status;
	}
	public void setCard_status(boolean card_status) {
		this.card_status = card_status;
	}
	@Override
	public String toString() {
		return "WalletResponse [status=" + status + ", code=" + code + ", message=" + message + ", details=" + details
				+ ", expiryDate=" + expiryDate + ", issueDate=" + issueDate + ", walletNumber=" + walletNumber
				+ ", walletId=" + walletId + ", withholdingAmt=" + withholdingAmt + ", availableAmt=" + availableAmt
				+ ", card_status=" + card_status + ", mmUserId=" + mmUserId + ", cardId=" + cardId + ", cvv=" + cvv
				+ ", holderName=" + holderName + "]";
	}
	
	
}
