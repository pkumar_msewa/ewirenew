package com.msscard.app.model.response;

public class CommonResponse {
	

	private boolean success;
	private String code;
	private String message;
	private String status;
	private Object details;
	private String email;
	private String mobile;	
	private Object details2;	
		
	
	public Object getDetails2() {
		return details2;
	}
	public void setDetails2(Object details2) {
		this.details2 = details2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}

}
