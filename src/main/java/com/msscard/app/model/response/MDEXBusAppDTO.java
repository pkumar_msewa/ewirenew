package com.msscard.app.model.response;

public class MDEXBusAppDTO {

	private String pqTxnId;

	private String transactionId;

	private String seatHoldId;

	private double amount;

	private String sessionId;

	private String transactionRefNo;
	

	public String getPqTxnId() {
		return pqTxnId;
	}

	public void setPqTxnId(String pqTxnId) {
		this.pqTxnId = pqTxnId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getSeatHoldId() {
		return seatHoldId;
	}

	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	
}
