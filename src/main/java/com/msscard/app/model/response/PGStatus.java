package com.msscard.app.model.response;

public enum PGStatus {

	REQ("request_received"),SUC("transaction_successful"),REJ("transaction_rejected"),ERR("error_in_trx_process");


	private final String value;

	private PGStatus(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	
	public static PGStatus getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (PGStatus v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}

}
