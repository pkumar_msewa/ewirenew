package com.msscard.app.model.response;

public class MDEXBusAppResponse extends CommonResponse {
	
	private String ticketPnr;

	private String operatorPnr;

	private String transactionId;

	private boolean ticket;

	private String mdexResponse;
	

	public String getTicketPnr() {
		return ticketPnr;
	}

	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}

	public String getOperatorPnr() {
		return operatorPnr;
	}

	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isTicket() {
		return ticket;
	}

	public void setTicket(boolean ticket) {
		this.ticket = ticket;
	}

	public String getMdexResponse() {
		return mdexResponse;
	}

	public void setMdexResponse(String mdexResponse) {
		this.mdexResponse = mdexResponse;
	}

}
