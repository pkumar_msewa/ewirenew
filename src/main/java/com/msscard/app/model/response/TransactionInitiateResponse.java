package com.msscard.app.model.response;

public class TransactionInitiateResponse {

	private String transactionRefNo;
	private String status;
	private String code;
	private String amount;
	private String message;
	private String key_id;
	private String key_secret;
	private String authReferenceNo;
	
	
	
	
	
	
	
	public String getAuthReferenceNo() {
		return authReferenceNo;
	}
	public void setAuthReferenceNo(String authReferenceNo) {
		this.authReferenceNo = authReferenceNo;
	}
	public String getKey_id() {
		return key_id;
	}
	public void setKey_id(String key_id) {
		this.key_id = key_id;
	}
	public String getKey_secret() {
		return key_secret;
	}
	public void setKey_secret(String key_secret) {
		this.key_secret = key_secret;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
