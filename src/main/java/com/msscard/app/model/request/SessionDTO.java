package com.msscard.app.model.request;

import com.msscard.model.PageDTO;

public class SessionDTO extends PageDTO{

	private String sessionId;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
