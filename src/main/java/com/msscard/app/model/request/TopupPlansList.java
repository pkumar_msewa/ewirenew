package com.msscard.app.model.request;

public class TopupPlansList {
	
	private String status;
	private String code;
	private String message;
	private Object details;
	private String circles;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getCircles() {
		return circles;
	}
	public void setCircles(String circles) {
		this.circles = circles;
	}
	
	

}
