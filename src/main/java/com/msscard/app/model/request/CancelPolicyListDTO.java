package com.msscard.app.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CancelPolicyListDTO {
	
	@JsonProperty("timeFrom")
	private int timeFrom;

	@JsonProperty("timeTo")
	private int timeTo;

	@JsonProperty("percentageCharge")
	private int percentageCharge;

	@JsonProperty("flatCharge")
	private int flatCharge;

	@JsonProperty("isFlat")
	private boolean isFlat;

	@JsonProperty("seatno")
	private String seatno;
	
	public int getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(int timeFrom) {
		this.timeFrom = timeFrom;
	}

	public int getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(int timeTo) {
		this.timeTo = timeTo;
	}

	public int getPercentageCharge() {
		return percentageCharge;
	}

	public void setPercentageCharge(int percentageCharge) {
		this.percentageCharge = percentageCharge;
	}

	public int getFlatCharge() {
		return flatCharge;
	}

	public void setFlatCharge(int flatCharge) {
		this.flatCharge = flatCharge;
	}

	public boolean isFlat() {
		return isFlat;
	}

	public void setFlat(boolean isFlat) {
		this.isFlat = isFlat;
	}

	public String getSeatno() {
		return seatno;
	}

	public void setSeatno(String seatno) {
		this.seatno = seatno;
	}

}
