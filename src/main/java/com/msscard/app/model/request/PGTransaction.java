package com.msscard.app.model.request;

public class PGTransaction {

	private String saleUrl;
	private String merchantId;
	private String merchantTxnNo;
	private String amount;
	private String currencyCode;
	private String payType;
	private String customerEmailId;
	private String transactionType;
	private String returnURL;
	private String txnDate;
	private String customerMobileNo;
	private String secureHash;
	
	public String getSaleUrl() {
		return saleUrl;
	}
	public void setSaleUrl(String saleUrl) {
		this.saleUrl = saleUrl;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getMerchantTxnNo() {
		return merchantTxnNo;
	}
	public void setMerchantTxnNo(String merchantTxnNo) {
		this.merchantTxnNo = merchantTxnNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getReturnURL() {
		return returnURL;
	}
	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getCustomerMobileNo() {
		return customerMobileNo;
	}
	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}
	public String getSecureHash() {
		return secureHash;
	}
	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}
	
	
}
