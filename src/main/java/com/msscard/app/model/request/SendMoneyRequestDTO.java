package com.msscard.app.model.request;

public class SendMoneyRequestDTO extends SessionDTO{

	private String recipientNo;
	private String amount;
	private String message;
	
	public String getRecipientNo() {
		return recipientNo;
	}
	public void setRecipientNo(String recipientNo) {
		this.recipientNo = recipientNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
