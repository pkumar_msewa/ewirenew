package com.msscard.app.model.request;

public class LoginDTO extends Utility{

	private String username;
	private String password;
	private boolean validate;
	private String mobileToken;
	private String androidDeviceID;
	private boolean hasDeviceId;
	private String mPin;
	private String daterange;
	
		
	public String getDaterange() {
		return daterange;
	}
	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isValidate() {
		return validate;
	}
	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	public String getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}
	public String getAndroidDeviceID() {
		return androidDeviceID;
	}
	public void setAndroidDeviceID(String androidDeviceID) {
		this.androidDeviceID = androidDeviceID;
	}
	public boolean isHasDeviceId() {
		return hasDeviceId;
	}
	public void setHasDeviceId(boolean hasDeviceId) {
		this.hasDeviceId = hasDeviceId;
	}
	public String getmPin() {
		return mPin;
	}
	public void setmPin(String mPin) {
		this.mPin = mPin;
	}
	
	
	
}
