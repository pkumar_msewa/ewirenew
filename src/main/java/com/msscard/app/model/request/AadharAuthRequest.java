package com.msscard.app.model.request;

public class AadharAuthRequest  extends SessionDTO{

	private String transactionId;
	private String aadharHashId;
	
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getAadharHashId() {
		return aadharHashId;
	}
	public void setAadharHashId(String aadharHashId) {
		this.aadharHashId = aadharHashId;
	}
	
	
}
