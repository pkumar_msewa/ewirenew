package com.msscard.app.model.request;

public class PGHandlerDTO {

	private String responseCode;
	private String respDescription;
	private String merchantId;
	private String merchantTxnNo;
	private String txnID;
	private String paymentDateTime;
	private String txnAuthID;
	private String secureHash;
	
	
	
	@Override
	public String toString() {
		return "PGHandlerDTO [responseCode=" + responseCode + ", respDescription=" + respDescription + ", merchantId="
				+ merchantId + ", merchantTxnNo=" + merchantTxnNo + ", txnID=" + txnID + ", paymentDateTime="
				+ paymentDateTime + ", txnAuthID=" + txnAuthID + ", secureHash=" + secureHash + "]";
	}



	public String getResponseCode() {
		return responseCode;
	}



	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}



	public String getRespDescription() {
		return respDescription;
	}



	public void setRespDescription(String respDescription) {
		this.respDescription = respDescription;
	}



	public String getMerchantId() {
		return merchantId;
	}



	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}



	public String getMerchantTxnNo() {
		return merchantTxnNo;
	}



	public void setMerchantTxnNo(String merchantTxnNo) {
		this.merchantTxnNo = merchantTxnNo;
	}



	public String getTxnID() {
		return txnID;
	}



	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}



	public String getPaymentDateTime() {
		return paymentDateTime;
	}



	public void setPaymentDateTime(String paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}



	public String getTxnAuthID() {
		return txnAuthID;
	}



	public void setTxnAuthID(String txnAuthID) {
		this.txnAuthID = txnAuthID;
	}



	public String getSecureHash() {
		return secureHash;
	}



	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}
	
	
}
