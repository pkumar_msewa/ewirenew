package com.msscard.app.model.request;

import java.util.List;

import com.google.gson.JsonArray;
import com.msscard.model.ResponseStatus;

public class ResponseDTO {

	private String status;
	private String code;
	private String amount;
	private String transactionType;
	private String description;
	private String date;
	private String message;
	private double balance;
	private Object details;
	private String txnId;
	private Object error;
	private boolean valid;
	private String info;
	private boolean hasRefer;
	private Object userList;
	private boolean hasVirtualCard;
	private boolean hasPhysicalCard;
	private Object cardDetails;
	private boolean hasPhysicalRequest;
	private String physicalCardStatus;
	private String minimumCardBalance;
	private String cardFees;
	private Object jsonArray;
	private long totalPages;
	private boolean kycRequest;
	private String loadMoneyComm;
	private String tripId;
	private Object downloadHistory;
	private Long pendingStatusCount;
	private String cardBaseFare;
	private boolean hasMpin;
	private boolean success;

	private String groupName;
	private String encodedImage;
	private String image;

	private String virtualCardBlock;
	private String physicalCardBlock;
	private boolean virtualIsBlock;
	private boolean physicalIsBlock;

	private boolean userCreated;

	private List<String> seatNo;
	private Object extraInfo;

	private String transactionRefNo;
	private double refAmt;
	private double cancellationCharges;

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public double getRefAmt() {
		return refAmt;
	}

	public void setRefAmt(double refAmt) {
		this.refAmt = refAmt;
	}

	public double getCancellationCharges() {
		return cancellationCharges;
	}

	public void setCancellationCharges(double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}

	public List<String> getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(List<String> seatNo) {
		this.seatNo = seatNo;
	}

	public Object getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(Object extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getVirtualCardBlock() {
		return virtualCardBlock;
	}

	public void setVirtualCardBlock(String virtualCardBlock) {
		this.virtualCardBlock = virtualCardBlock;
	}

	public String getPhysicalCardBlock() {
		return physicalCardBlock;
	}

	public void setPhysicalCardBlock(String physicalCardBlock) {
		this.physicalCardBlock = physicalCardBlock;
	}

	public boolean isVirtualIsBlock() {
		return virtualIsBlock;
	}

	public void setVirtualIsBlock(boolean virtualIsBlock) {
		this.virtualIsBlock = virtualIsBlock;
	}

	public boolean isPhysicalIsBlock() {
		return physicalIsBlock;
	}

	public void setPhysicalIsBlock(boolean physicalIsBlock) {
		this.physicalIsBlock = physicalIsBlock;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEncodedImage() {
		return encodedImage;
	}

	public void setEncodedImage(String encodedImage) {
		this.encodedImage = encodedImage;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isHasMpin() {
		return hasMpin;
	}

	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}

	public String getCardBaseFare() {
		return cardBaseFare;
	}

	public void setCardBaseFare(String cardBaseFare) {
		this.cardBaseFare = cardBaseFare;
	}

	public Long getPendingStatusCount() {
		return pendingStatusCount;
	}

	public void setPendingStatusCount(Long pendingStatusCount) {
		this.pendingStatusCount = pendingStatusCount;
	}

	public Object getDownloadHistory() {
		return downloadHistory;
	}

	public void setDownloadHistory(Object downloadHistory) {
		this.downloadHistory = downloadHistory;
	}

	private List<BeneficiaryDTO> beneficiary;

	public List<BeneficiaryDTO> getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(List<BeneficiaryDTO> beneficiary) {
		this.beneficiary = beneficiary;
	}

	private JsonArray userValues;
	private JsonArray txnValues;

	public String getLoadMoneyComm() {
		return loadMoneyComm;
	}

	public void setLoadMoneyComm(String loadMoneyComm) {
		this.loadMoneyComm = loadMoneyComm;
	}

	public boolean isKycRequest() {
		return kycRequest;
	}

	public void setKycRequest(boolean kycRequest) {
		this.kycRequest = kycRequest;
	}

	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

	public Object getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(Object jsonArray) {
		this.jsonArray = jsonArray;
	}

	public String getMinimumCardBalance() {
		return minimumCardBalance;
	}

	public void setMinimumCardBalance(String minimumCardBalance) {
		this.minimumCardBalance = minimumCardBalance;
	}

	public String getCardFees() {
		return cardFees;
	}

	public void setCardFees(String cardFees) {
		this.cardFees = cardFees;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPhysicalCardStatus() {
		return physicalCardStatus;
	}

	public void setPhysicalCardStatus(String physicalCardStatus) {
		this.physicalCardStatus = physicalCardStatus;
	}

	public boolean isHasPhysicalRequest() {
		return hasPhysicalRequest;
	}

	public void setHasPhysicalRequest(boolean hasPhysicalRequest) {
		this.hasPhysicalRequest = hasPhysicalRequest;
	}

	public Object getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(Object cardDetails) {
		this.cardDetails = cardDetails;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public boolean isHasRefer() {
		return hasRefer;
	}

	public void setHasRefer(boolean hasRefer) {
		this.hasRefer = hasRefer;
	}

	public Object getUserList() {
		return userList;
	}

	public void setUserList(Object userList) {
		this.userList = userList;
	}

	public boolean isHasVirtualCard() {
		return hasVirtualCard;
	}

	public void setHasVirtualCard(boolean hasVirtualCard) {
		this.hasVirtualCard = hasVirtualCard;
	}

	public boolean isHasPhysicalCard() {
		return hasPhysicalCard;
	}

	public void setHasPhysicalCard(boolean hasPhysicalCard) {
		this.hasPhysicalCard = hasPhysicalCard;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public JsonArray getUserValues() {
		return userValues;
	}

	public void setUserValues(JsonArray userValues) {
		this.userValues = userValues;
	}

	public JsonArray getTxnValues() {
		return txnValues;
	}

	public void setTxnValues(JsonArray txnValues) {
		this.txnValues = txnValues;
	}

	@Override
	public String toString() {
		return "ResponseDTO [status=" + status + ", code=" + code + ", message=" + message + ", balance=" + balance
				+ ", details=" + details + ", txnId=" + txnId + ", error=" + error + ", valid=" + valid + ", info="
				+ info + ", hasRefer=" + hasRefer + ", userList=" + userList + ", hasVirtualCard=" + hasVirtualCard
				+ ", hasPhysicalCard=" + hasPhysicalCard + ", cardDetails=" + cardDetails + "]";
	}

	public boolean isUserCreated() {
		return userCreated;
	}

	public void setUserCreated(boolean userCreated) {
		this.userCreated = userCreated;
	}

}
