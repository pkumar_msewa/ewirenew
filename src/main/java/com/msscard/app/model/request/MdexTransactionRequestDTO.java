package com.msscard.app.model.request;

public class MdexTransactionRequestDTO {
	private String serviceProvider;
	private String mobileNumber;
	private String amount;
	private String transactionId;
	private String additionalInfo;
	private String dthNumber;
	private String landlineNumber;
	private String stdCode;
	private String accountNumber;
	private String cycleNumber;
	private String billingUnit;
	private String processingCycle;
	private String cityName;
	private String customermobile;
	private String policyNumber;
	private String policyDate;
	private String area;
	private String billGroupNumber;
	private String senderUser;
	private String seatHoldId;
	private String referenceNumber;
	
	
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getSenderUser() {
		return senderUser;
	}
	public void setSenderUser(String senderUser) {
		this.senderUser = senderUser;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getDthNumber() {
		return dthNumber;
	}
	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}
	public String getLandlineNumber() {
		return landlineNumber;
	}
	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}
	public String getStdCode() {
		return stdCode;
	}
	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCycleNumber() {
		return cycleNumber;
	}
	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}
	public String getBillingUnit() {
		return billingUnit;
	}
	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}
	public String getProcessingCycle() {
		return processingCycle;
	}
	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCustomermobile() {
		return customermobile;
	}
	public void setCustomermobile(String customermobile) {
		this.customermobile = customermobile;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getPolicyDate() {
		return policyDate;
	}
	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}
	public String getBillGroupNumber() {
		return billGroupNumber;
	}
	public void setBillGroupNumber(String billGroupNumber) {
		this.billGroupNumber = billGroupNumber;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	

}
