package com.msscard.app.model.request;

import com.msscard.entity.MUser;

public class TransactionInitiateRequest extends SessionDTO {

	private String amount;
	
	private String transactionRefNo;
	
	private String paymentId;
	
	private String status;
	
	private String transactionType;
	
	private MUser user;
	
	private String authReferenceNo;
	
	private String physicalCard;
	
	private String virtualCard;
	
	private String razorpay_payment_id;
	
	private String username;
	
	private String merchant_id;
	
	private String hash;
	
	private String additionalInfo;
	
	private boolean isUPI;
	
	private String ua;
	
	private boolean isAndroid;
	
	private String serviceCode;
	
	private String mobileNumber;
	
	private String area;
	
	private String topUpType;
	
	private String accountNumber;
	
	private String billGroupNumber;
	
	private String billingUnit;
	
	private String dthNumber;
	
	private String landLineNumber;
	
	private String stdCode;
	
	private String cycleNumber;
	
	private String cityName;
	
	private String processingCycle;
	
	private String policyNumber;
	
	private String policyDate;
	
	private String upiRequest;
	
	private String serviceProvider;
	
	private String circleCode;
	
	private String rechargeType;
	
	private String referenceNumber;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public String getAuthReferenceNo() {
		return authReferenceNo;
	}

	public void setAuthReferenceNo(String authReferenceNo) {
		this.authReferenceNo = authReferenceNo;
	}

	public String getPhysicalCard() {
		return physicalCard;
	}

	public void setPhysicalCard(String physicalCard) {
		this.physicalCard = physicalCard;
	}

	public String getVirtualCard() {
		return virtualCard;
	}

	public void setVirtualCard(String virtualCard) {
		this.virtualCard = virtualCard;
	}

	public String getRazorpay_payment_id() {
		return razorpay_payment_id;
	}

	public void setRazorpay_payment_id(String razorpay_payment_id) {
		this.razorpay_payment_id = razorpay_payment_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public boolean isUPI() {
		return isUPI;
	}

	public void setUPI(boolean isUPI) {
		this.isUPI = isUPI;
	}

	public String getUa() {
		return ua;
	}

	public void setUa(String ua) {
		this.ua = ua;
	}

	public boolean isAndroid() {
		return isAndroid;
	}

	public void setAndroid(boolean isAndroid) {
		this.isAndroid = isAndroid;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getTopUpType() {
		return topUpType;
	}

	public void setTopUpType(String topUpType) {
		this.topUpType = topUpType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBillGroupNumber() {
		return billGroupNumber;
	}

	public void setBillGroupNumber(String billGroupNumber) {
		this.billGroupNumber = billGroupNumber;
	}

	public String getBillingUnit() {
		return billingUnit;
	}

	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}

	public String getDthNumber() {
		return dthNumber;
	}

	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}

	public String getLandLineNumber() {
		return landLineNumber;
	}

	public void setLandLineNumber(String landLineNumber) {
		this.landLineNumber = landLineNumber;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getCycleNumber() {
		return cycleNumber;
	}

	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProcessingCycle() {
		return processingCycle;
	}

	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyDate() {
		return policyDate;
	}

	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}

	public String getUpiRequest() {
		return upiRequest;
	}

	public void setUpiRequest(String upiRequest) {
		this.upiRequest = upiRequest;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getCircleCode() {
		return circleCode;
	}

	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public String toString() {
		return "TransactionInitiateRequest [amount=" + amount + ", transactionRefNo=" + transactionRefNo
				+ ", paymentId=" + paymentId + ", status=" + status + ", transactionType=" + transactionType + ", user="
				+ user + ", authReferenceNo=" + authReferenceNo + ", physicalCard=" + physicalCard + ", virtualCard="
				+ virtualCard + ", razorpay_payment_id=" + razorpay_payment_id + ", username=" + username
				+ ", merchant_id=" + merchant_id + ", hash=" + hash + ", additionalInfo=" + additionalInfo + ", isUPI="
				+ isUPI + ", ua=" + ua + ", isAndroid=" + isAndroid + ", serviceCode=" + serviceCode + ", mobileNumber="
				+ mobileNumber + ", area=" + area + ", topUpType=" + topUpType + ", accountNumber=" + accountNumber
				+ ", billGroupNumber=" + billGroupNumber + ", billingUnit=" + billingUnit + ", dthNumber=" + dthNumber
				+ ", landLineNumber=" + landLineNumber + ", stdCode=" + stdCode + ", cycleNumber=" + cycleNumber
				+ ", cityName=" + cityName + ", processingCycle=" + processingCycle + ", policyNumber=" + policyNumber
				+ ", policyDate=" + policyDate + ", upiRequest=" + upiRequest + ", serviceProvider=" + serviceProvider
				+ ", circleCode=" + circleCode + ", rechargeType=" + rechargeType + ", referenceNumber="
				+ referenceNumber + "]";
	}

}
