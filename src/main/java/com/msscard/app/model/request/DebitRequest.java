package com.msscard.app.model.request;

public class DebitRequest extends SessionDTO{

	private String amount;
	private String mobile;
	
	

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
