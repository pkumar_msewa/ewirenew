package com.msscard.app.model.request;

import java.util.List;

import com.cashiercards.enums.BusType;
import com.msscard.entity.TravellerDetails;

public class TicketDetailsDTO {
	
	private String userMobile;
	private String userEmail;
	private String ticketPnr;
	private String operatorPnr;
	private String emtTxnId;
	private String busId;
	private double totalFare;
	private String journeyDate;
	private String source;
	private String destination;
	private String boardingId;
	private String boardingAddress;
	private String busOperator;
	private String arrTime;
	private String deptTime;
	private BusType busType;
	private String seats;
	private String boardTime;

	private List<TravellerDetails> travellerList;

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getTicketPnr() {
		return ticketPnr;
	}

	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}

	public String getOperatorPnr() {
		return operatorPnr;
	}

	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}

	public String getEmtTxnId() {
		return emtTxnId;
	}

	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getBoardingId() {
		return boardingId;
	}

	public void setBoardingId(String boardingId) {
		this.boardingId = boardingId;
	}

	public String getBoardingAddress() {
		return boardingAddress;
	}

	public void setBoardingAddress(String boardingAddress) {
		this.boardingAddress = boardingAddress;
	}

	public String getBusOperator() {
		return busOperator;
	}

	public void setBusOperator(String busOperator) {
		this.busOperator = busOperator;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getDeptTime() {
		return deptTime;
	}

	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}

	public BusType getBusType() {
		return busType;
	}

	public void setBusType(BusType busType) {
		this.busType = busType;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	public String getBoardTime() {
		return boardTime;
	}

	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}

	public List<TravellerDetails> getTravellerList() {
		return travellerList;
	}

	public void setTravellerList(List<TravellerDetails> travellerList) {
		this.travellerList = travellerList;
	}
	
	
	
}
