package com.msscard.app.model.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class FCMRequestDTO extends SessionDTO{

	private String message;
	private String imageUrl;
	private String title;
	private String gcmId;
	private List<String> gcmIds;
	private JSONObject toJson;
	private boolean withImage;
	private boolean singleUser;
	private String username;
	private boolean allUser;
	
	
	
	
	
	public boolean isAllUser() {
		return allUser;
	}
	public void setAllUser(boolean allUser) {
		this.allUser = allUser;
	}
	public boolean isWithImage() {
		return withImage;
	}
	public void setWithImage(boolean withImage) {
		this.withImage = withImage;
	}
	public boolean isSingleUser() {
		return singleUser;
	}
	public void setSingleUser(boolean singleUser) {
		this.singleUser = singleUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setToJson(JSONObject toJson) {
		this.toJson = toJson;
	}
	public JSONObject getToJson() {
		return toJson;
	}
		public String getGcmId() {
		return gcmId;
	}
	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}
	public List<String> getGcmIds() {
		return gcmIds;
	}
	public void setGcmIds(List<String> gcmIds) {
		this.gcmIds = gcmIds;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public JSONObject setToJson(){
		JSONObject jObject=new JSONObject();
		JSONObject kSon=new JSONObject();
		Map<String, String> map=new HashMap<>();
		map.put("registration_ids", this.gcmId);
		try {
			kSon.put("title", this.title);
			kSon.put("message", this.message);
			kSon.put("image", this.imageUrl);
			jObject.put("registration_ids", map);
			jObject.put("data", kSon);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jObject;
	}
	
}
