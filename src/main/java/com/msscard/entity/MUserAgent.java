package com.msscard.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class MUserAgent extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser user;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser agent;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MKycDetail kycDetail;

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public MUser getAgent() {
		return agent;
	}

	public void setAgent(MUser agent) {
		this.agent = agent;
	}

	public MKycDetail getKycDetail() {
		return kycDetail;
	}

	public void setKycDetail(MKycDetail kycDetail) {
		this.kycDetail = kycDetail;
	}
	
	

}
