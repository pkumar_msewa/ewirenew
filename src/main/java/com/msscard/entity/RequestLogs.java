package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

	@Entity
	@Table(name="RequestLogs")
	public class RequestLogs extends AbstractEntity<Long> {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@ManyToOne
		private MUser user;

		@ManyToOne
		private MService service;
		
		public MUser getUser() {
			return user;
		}

		public void setUser(MUser user) {
			this.user = user;
		}

		public MService getService() {
			return service;
		}

		public void setService(MService service) {
			this.service = service;
		}
}
