package com.msscard.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class PrefundRequest extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 1L;

	@Column(nullable=false)
	private double amount;
	
	@Column(nullable=false)
	private String senderBankName;
	
	@Column(nullable=false)
	private String senderAccountNo;
	
	@Column(nullable=false)
	private String senderReferenceNo;
	
	@Column(nullable=false)
	private String receiverBankName;
	
	@Column(nullable=false)
	private String receiverAccountNo;
	
	@Column(nullable=true)
	private String remarks;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser user;
	
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private Status status;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getSenderBankName() {
		return senderBankName;
	}

	public void setSenderBankName(String senderBankName) {
		this.senderBankName = senderBankName;
	}

	public String getSenderAccountNo() {
		return senderAccountNo;
	}

	public void setSenderAccountNo(String senderAccountNo) {
		this.senderAccountNo = senderAccountNo;
	}

	public String getSenderReferenceNo() {
		return senderReferenceNo;
	}

	public void setSenderReferenceNo(String senderReferenceNo) {
		this.senderReferenceNo = senderReferenceNo;
	}

	public String getReceiverBankName() {
		return receiverBankName;
	}

	public void setReceiverBankName(String receiverBankName) {
		this.receiverBankName = receiverBankName;
	}

	public String getReceiverAccountNo() {
		return receiverAccountNo;
	}

	public void setReceiverAccountNo(String receiverAccountNo) {
		this.receiverAccountNo = receiverAccountNo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	

	
}
