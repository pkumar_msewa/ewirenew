package com.msscard.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.msscard.model.Status;

@Entity
@Table(name = "GroupBulkKyc")
public class GroupBulkKyc extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column
	private String documentUrl;
	
	@Column
	private String documentUrl1;
	
	@Column
	private String pinCode;
	
	@Column
	private String address1;
	
	@Column
	private String address2;
	
	@Column	
	private String mobile;
	
	@Column
	private String email;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	@Column
	private String idType;
	
	@Column
	private String idNumber;
	
	@OneToOne
	private GroupDetails groupDetail;
		
	@OneToOne
	private GroupFileCatalogue groupFileCatalogue;
	
	@Column
	private boolean cronStatus=false;
	
	@Column
	private String failReason;
	
	@Column
	private boolean successStatus=false;
	
	@Column
	private Status status;

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public String getDocumentUrl1() {
		return documentUrl1;
	}

	public void setDocumentUrl1(String documentUrl1) {
		this.documentUrl1 = documentUrl1;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public GroupDetails getGroupDetail() {
		return groupDetail;
	}

	public void setGroupDetail(GroupDetails groupDetail) {
		this.groupDetail = groupDetail;
	}

	public GroupFileCatalogue getGroupFileCatalogue() {
		return groupFileCatalogue;
	}

	public void setGroupFileCatalogue(GroupFileCatalogue groupFileCatalogue) {
		this.groupFileCatalogue = groupFileCatalogue;
	}

	public boolean isCronStatus() {
		return cronStatus;
	}

	public void setCronStatus(boolean cronStatus) {
		this.cronStatus = cronStatus;
	}

	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public boolean isSuccessStatus() {
		return successStatus;
	}

	public void setSuccessStatus(boolean successStatus) {
		this.successStatus = successStatus;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
