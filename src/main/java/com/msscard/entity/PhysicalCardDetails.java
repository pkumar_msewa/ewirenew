package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class PhysicalCardDetails extends AbstractEntity<Long> {
	private static final long serialVersionUID = 1L;
	@Column
	private String address1;
	@Column
	private String address2;
	@Column
	private String state;
	@Column
	private String city;
	@Column
	private String pin;
	@Column
	private String proxyNumber;
	@Column
	private String activationCode;
	@Column
	private Status status;

	@Column
	@Lob
	private String comment;

	@Column
	private boolean fromAdmin = false;

	@OneToOne(fetch = FetchType.EAGER)
	private MMCards cards;

	@OneToOne(fetch = FetchType.EAGER)
	private MatchMoveWallet wallet;

	public MatchMoveWallet getWallet() {
		return wallet;
	}

	public void setWallet(MatchMoveWallet wallet) {
		this.wallet = wallet;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getProxyNumber() {
		return proxyNumber;
	}

	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public MMCards getCards() {
		return cards;
	}

	public void setCards(MMCards cards) {
		this.cards = cards;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isFromAdmin() {
		return fromAdmin;
	}

	public void setFromAdmin(boolean fromAdmin) {
		this.fromAdmin = fromAdmin;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
