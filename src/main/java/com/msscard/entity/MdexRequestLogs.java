package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class MdexRequestLogs extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String request;
	@OneToOne
	private MTransaction transaction;
	@Column(nullable=true)
	private String additionalInfo;
	private String serviceCode;
	private String topUpType;
	private String dthNumber;
	private String landlineNumber;
	private String stdCode;
	private String cycleNumber;
	private String cityName;
	private String billingUnit;
	private String processingCycle;
	private String policyNumber;
	private String policyDate;
	private String billGroupNumber;
	private String accountNumber;
	private String senderUser;
	private boolean status=false;
	private String seatHoldId;
	private String referenceNumber;
		
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getSenderUser() {
		return senderUser;
	}
	public void setSenderUser(String senderUser) {
		this.senderUser = senderUser;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@Column(nullable=true)
	private String area;
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public MTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(MTransaction transaction) {
		this.transaction = transaction;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getTopUpType() {
		return topUpType;
	}
	public void setTopUpType(String topUpType) {
		this.topUpType = topUpType;
	}
	public String getDthNumber() {
		return dthNumber;
	}
	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}
	public String getLandlineNumber() {
		return landlineNumber;
	}
	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}
	public String getStdCode() {
		return stdCode;
	}
	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}
	public String getCycleNumber() {
		return cycleNumber;
	}
	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}
	

	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getBillingUnit() {
		return billingUnit;
	}
	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}
	

	public String getProcessingCycle() {
		return processingCycle;
	}
	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyDate() {
		return policyDate;
	}
	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}
	public String getBillGroupNumber() {
		return billGroupNumber;
	}
	public void setBillGroupNumber(String billGroupNumber) {
		this.billGroupNumber = billGroupNumber;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	
	
}
