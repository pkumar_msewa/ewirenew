package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.msscard.entity.MUser;
import com.msscard.model.Status;

@Entity
@Table(name = "BulkCardAssignmentGroup")
public class BulkCardAssignmentGroup extends AbstractEntity<Long>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false, unique = true)
	private String proxyNumber;

	@Column(nullable = false)
	private String mobileNumber;

	@Column(nullable = true)
	private String remarks;

	@ManyToOne(optional = false)
	private MUser groupDetails;

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	private Status creationStatus;

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	private Status activationStatus;

	@Column(nullable = true)
	private String fileName;

	private boolean failedStatus = false;

	private String activationCode;

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public boolean isFailedStatus() {
		return failedStatus;
	}

	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getProxyNumber() {
		return proxyNumber;
	}

	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public MUser getGroupDetails() {
		return groupDetails;
	}

	public void setGroupDetails(MUser groupDetails) {
		this.groupDetails = groupDetails;
	}

	public Status getCreationStatus() {
		return creationStatus;
	}

	public void setCreationStatus(Status creationStatus) {
		this.creationStatus = creationStatus;
	}

	public Status getActivationStatus() {
		return activationStatus;
	}

	public void setActivationStatus(Status activationStatus) {
		this.activationStatus = activationStatus;
	}
}
