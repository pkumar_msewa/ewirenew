package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="loadcardotp")
@Entity
public class LoadCardOtp extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String mobile;
	
	@Column
	private String name;
	
	@Column
	private String otp;
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
