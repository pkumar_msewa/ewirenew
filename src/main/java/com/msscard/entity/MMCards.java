package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class MMCards extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false, unique = true)
	private String cardId;

	@Column
	private boolean hasPhysicalCard = false;

	@Column
	private boolean blocked = false;

	@Column
	private String status;

	@ManyToOne(optional = false)
	private MatchMoveWallet wallet;

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	@Column(nullable = true)
	private String activationCode;

	public MatchMoveWallet getWallet() {
		return wallet;
	}

	public void setWallet(MatchMoveWallet wallet) {
		this.wallet = wallet;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public boolean isHasPhysicalCard() {
		return hasPhysicalCard;
	}

	public void setHasPhysicalCard(boolean hasPhysicalCard) {
		this.hasPhysicalCard = hasPhysicalCard;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

}
