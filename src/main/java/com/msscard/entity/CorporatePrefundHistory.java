package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
public class CorporatePrefundHistory extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String transactionRefNo;
	private String corporateName;
	private String amount;
	private String filePath;
	@ManyToOne
	private MUser corporate;
	private boolean reviewStatus=false;
	private boolean prefundStatus=false;
	
	
	
	
	public boolean isPrefundStatus() {
		return prefundStatus;
	}
	public void setPrefundStatus(boolean prefundStatus) {
		this.prefundStatus = prefundStatus;
	}
	public boolean isReviewStatus() {
		return reviewStatus;
	}
	public void setReviewStatus(boolean reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
	public MUser getCorporate() {
		return corporate;
	}
	public void setCorporate(MUser corporate) {
		this.corporate = corporate;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	
}
