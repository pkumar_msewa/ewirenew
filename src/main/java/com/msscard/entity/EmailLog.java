package com.msscard.entity;

import java.util.Date;

	import javax.persistence.*;

import com.msscard.model.Status;

	@Entity
	public class EmailLog extends AbstractEntity<Long> {

		private static final long serialVersionUID = 1L;

		@Temporal(TemporalType.TIMESTAMP)
		private Date excutionTime;

		private String destination;

		private String mailTemplate;

		private String sender;

		@Lob
		private String mailContent;

		@Enumerated(EnumType.STRING)
		private Status status;

		public String getMailContent() {
			return mailContent;
		}

		public void setMailContent(String mailContent) {
			this.mailContent = mailContent;
		}

		public Date getExcutionTime() {
			return excutionTime;
		}

		public void setExcutionTime(Date excutionTime) {
			this.excutionTime = excutionTime;
		}

		public String getDestination() {
			return destination;
		}

		public void setDestination(String destination) {
			this.destination = destination;
		}


		public String getMailTemplate() {
			return mailTemplate;
		}

		public void setMailTemplate(String mailTemplate) {
			this.mailTemplate = mailTemplate;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		public String getSender() {
			return sender;
		}

		public void setSender(String sender) {
			this.sender = sender;
		}

	}



