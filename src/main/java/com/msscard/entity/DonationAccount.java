package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="donationaccount")
@Entity
public class DonationAccount extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String balance;
	
	@Column(unique = true, nullable = false)
	private long accountNumber;
		
	@Column
	private String donateeTo;
	
	@OneToOne
	private MUser user;
	

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDonateeTo() {
		return donateeTo;
	}

	public void setDonateeTo(String donateeTo) {
		this.donateeTo = donateeTo;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}
	
	
	
}
