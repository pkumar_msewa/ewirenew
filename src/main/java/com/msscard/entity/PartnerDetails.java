package com.msscard.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class PartnerDetails extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerName;
	private String partnerEmail;
	private String partnerMobile;
	@OneToOne
	private MUser partnerUser;
	@ManyToMany(fetch=FetchType.EAGER)
	private List<MService> partnerServices;
	@ManyToOne
	private CorporateAgentDetails corporate;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	private double loadCardMaxLimit;
	
	public double getLoadCardMaxLimit() {
		return loadCardMaxLimit;
	}
	public void setLoadCardMaxLimit(double loadCardMaxLimit) {
		this.loadCardMaxLimit = loadCardMaxLimit;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerEmail() {
		return partnerEmail;
	}
	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}
	public String getPartnerMobile() {
		return partnerMobile;
	}
	public void setPartnerMobile(String partnerMobile) {
		this.partnerMobile = partnerMobile;
	}
	public MUser getPartnerUser() {
		return partnerUser;
	}
	public void setPartnerUser(MUser partnerUser) {
		this.partnerUser = partnerUser;
	}
	public List<MService> getPartnerServices() {
		return partnerServices;
	}
	public void setPartnerServices(List<MService> partnerServices) {
		this.partnerServices = partnerServices;
	}
	public CorporateAgentDetails getCorporate() {
		return corporate;
	}
	public void setCorporate(CorporateAgentDetails corporate) {
		this.corporate = corporate;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
