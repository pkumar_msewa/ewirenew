package com.msscard.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class MPQAccountDetails extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double balance;
	
	@Column(unique = true, nullable = false)
	private long accountNumber;

	@Column(columnDefinition = "bigint(20) default 0")
	private long points;

	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MPQAccountType accountType;

	private String branchCode;

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public MPQAccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(MPQAccountType accountType) {
		this.accountType = accountType;
	}
	
	

}
