package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="groupfilecatalogue")
@Entity
public class GroupFileCatalogue extends AbstractEntity<Long>{	

	private static final long serialVersionUID = 8453654076725018243L;
	
	private String absPath;
	private String s3Path;
	private String fileDescription;
	@ManyToOne
	private MUser user;
	@ManyToOne
	private PartnerDetails partnerDetails;
	private boolean reviewStatus=false;
	private boolean fileDeletionStatus=false;
	private boolean fileRejectionStatus=false;
	private String categoryType;
	private boolean schedulerStatus=false;
	private boolean transactionStatus=false;
	
	public String getAbsPath() {
		return absPath;
	}
	public void setAbsPath(String absPath) {
		this.absPath = absPath;
	}
	public String getS3Path() {
		return s3Path;
	}
	public void setS3Path(String s3Path) {
		this.s3Path = s3Path;
	}
	public String getFileDescription() {
		return fileDescription;
	}
	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}
	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}
	public boolean isReviewStatus() {
		return reviewStatus;
	}
	public void setReviewStatus(boolean reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
	public boolean isFileDeletionStatus() {
		return fileDeletionStatus;
	}
	public void setFileDeletionStatus(boolean fileDeletionStatus) {
		this.fileDeletionStatus = fileDeletionStatus;
	}
	public boolean isFileRejectionStatus() {
		return fileRejectionStatus;
	}
	public void setFileRejectionStatus(boolean fileRejectionStatus) {
		this.fileRejectionStatus = fileRejectionStatus;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public boolean isSchedulerStatus() {
		return schedulerStatus;
	}
	public void setSchedulerStatus(boolean schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}
	public boolean isTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(boolean transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	
	
}
