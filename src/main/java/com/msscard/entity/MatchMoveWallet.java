package com.msscard.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class MatchMoveWallet extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String walletNumber;
	private String walletId;
	private String mmUserId;
	@OneToOne(fetch=FetchType.EAGER)
	private MUser user;
	private String issueDate;
	@OneToMany(fetch=FetchType.EAGER,mappedBy="wallet")
	private List<MMCards> cards;
	
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getWalletNumber() {
		return walletNumber;
	}
	public void setWalletNumber(String walletNumber) {
		this.walletNumber = walletNumber;
	}
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public String getMmUserId() {
		return mmUserId;
	}
	public void setMmUserId(String mmUserId) {
		this.mmUserId = mmUserId;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public List<MMCards> getCards() {
		return cards;
	}
	public void setCards(List<MMCards> cards) {
		this.cards = cards;
	}
	
	
	
	
}
