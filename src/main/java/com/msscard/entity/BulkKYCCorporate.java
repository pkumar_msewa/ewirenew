package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "BulkKYCCorporate")
@Entity
public class BulkKYCCorporate extends AbstractEntity<Long> {

	private static final long serialVersionUID = 6807282765558433987L;

	@Column(unique = true, nullable = false)
	private String mobile;

	@Column
	private String email;

	@Column
	private String address1;

	@Column
	private String address2;

	@Column
	private String dob;

	@Column
	private String pinCode;

	@Column
	private String docUrl;

	@Column
	private String docUrl1;

	@Column
	private String idType;

	@Column
	private String idNumber;

	private boolean kycStatus = false;

	@Column
	private String failedReason;

	private boolean failedStatus = false;

	@OneToOne(fetch = FetchType.EAGER)
	private MUser user;

	@ManyToOne
	private CorporateAgentDetails agentDetails;

	@ManyToOne
	private PartnerDetails partnerDetails;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getDocUrl() {
		return docUrl;
	}

	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}

	public String getDocUrl1() {
		return docUrl1;
	}

	public void setDocUrl1(String docUrl1) {
		this.docUrl1 = docUrl1;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public boolean isKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(boolean kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	public boolean isFailedStatus() {
		return failedStatus;
	}

	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public CorporateAgentDetails getAgentDetails() {
		return agentDetails;
	}

	public void setAgentDetails(CorporateAgentDetails agentDetails) {
		this.agentDetails = agentDetails;
	}

	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}

	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}

}