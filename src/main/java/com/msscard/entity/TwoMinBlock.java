package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="twominblock")
@Entity
public class TwoMinBlock extends AbstractEntity<Long>{

	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String status;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MUser user;
	
	@Column
	private boolean block;
	
	@Column
	private int counting;
	
	public int getCounting() {
		return counting;
	}

	public void setCounting(int counting) {
		this.counting = counting;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public boolean isBlock() {
		return block;
	}

	public void setBlock(boolean block) {
		this.block = block;
	}
	
	
		
}
