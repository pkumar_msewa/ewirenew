package com.msscard.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class PromoCode extends AbstractEntity<Long>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column( nullable = true)
	private String promoCode;
	@Column (nullable = true)
	private Date startDate;
	@Column (nullable = true)
	private Date endDate;
	@Column (nullable = true)
	private String amount;
	@Column (nullable = true)
	private String percentage;
	@Column (nullable = true)
	private long minTrxAmount;
	@Column (nullable = true)
	private long maxCashBack;
	@Column (nullable = true)
	private boolean isPercentage;
	@Column (nullable = true)
	private Status Status;
	@OneToOne(fetch=FetchType.EAGER)
	private MService service;
	
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public boolean isPercentage() {
		return isPercentage;
	}
	public void setPercentage(boolean isPercentage) {
		this.isPercentage = isPercentage;
	}
	
	public com.msscard.model.Status getStatus() {
		return Status;
	}
	public void setStatus(com.msscard.model.Status status) {
		Status = status;
	}
	public long getMinTrxAmount() {
		return minTrxAmount;
	}
	public void setMinTrxAmount(long minTrxAmount) {
		this.minTrxAmount = minTrxAmount;
	}
	public long getMaxCashBack() {
		return maxCashBack;
	}
	public void setMaxCashBack(long maxCashBack) {
		this.maxCashBack = maxCashBack;
	}
	public MService getService() {
		return service;
	}
	public void setService(MService service) {
		this.service = service;
	}
	
}
