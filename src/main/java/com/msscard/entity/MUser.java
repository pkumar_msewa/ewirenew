package com.msscard.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.msscard.model.Status;
import com.msscard.model.UserType;

@Entity
public class MUser extends AbstractEntity<Long> {

	private static final long serialVersionUID = 8453654076725018243L;

	@Column(unique = true, nullable = false)
	private String username;

	@Column(nullable = true)
	private String password;

	@Column(nullable = false)
	private UserType userType;

	@Column(nullable = false)
	private String authority;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status emailStatus;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status mobileStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date otpGenerationTime;

	@OneToOne(fetch = FetchType.EAGER)
	private MUserDetails userDetail;

	@OneToOne(fetch = FetchType.EAGER)
	private MPQAccountDetails accountDetail;

	@OneToOne(fetch = FetchType.EAGER)
	private GroupDetails groupDetails;

	@Column
	private String emailToken;

	@Column
	private String mobileToken;

	@Lob
	private String gcmId;

	@Column(nullable = true)
	private String androidDeviceID;
	@Column(nullable = true)
	private boolean isDeviceLocked;
	private boolean isFullyFilled = true;
	private boolean isCorporateUser = false;
	private String mMUserId;

	public GroupDetails getGroupDetails() {
		return groupDetails;
	}

	public void setGroupDetails(GroupDetails groupDetails) {
		this.groupDetails = groupDetails;
	}

	public String getmMUserId() {
		return mMUserId;
	}

	public void setmMUserId(String mMUserId) {
		this.mMUserId = mMUserId;
	}

	public boolean isCorporateUser() {
		return isCorporateUser;
	}

	public void setCorporateUser(boolean isCorporateUser) {
		this.isCorporateUser = isCorporateUser;
	}

	public boolean isFullyFilled() {
		return isFullyFilled;
	}

	public void setFullyFilled(boolean isFullyFilled) {
		this.isFullyFilled = isFullyFilled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Status getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(Status emailStatus) {
		this.emailStatus = emailStatus;
	}

	public Status getMobileStatus() {
		return mobileStatus;
	}

	public void setMobileStatus(Status mobileStatus) {
		this.mobileStatus = mobileStatus;
	}

	public Date getOtpGenerationTime() {
		return otpGenerationTime;
	}

	public void setOtpGenerationTime(Date otpGenerationTime) {
		this.otpGenerationTime = otpGenerationTime;
	}

	public MUserDetails getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(MUserDetails userDetail) {
		this.userDetail = userDetail;
	}

	public MPQAccountDetails getAccountDetail() {
		return accountDetail;
	}

	public void setAccountDetail(MPQAccountDetails accountDetail) {
		this.accountDetail = accountDetail;
	}

	public String getEmailToken() {
		return emailToken;
	}

	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}

	public String getMobileToken() {
		return mobileToken;
	}

	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getAndroidDeviceID() {
		return androidDeviceID;
	}

	public void setAndroidDeviceID(String androidDeviceID) {
		this.androidDeviceID = androidDeviceID;
	}

	public boolean isDeviceLocked() {
		return isDeviceLocked;
	}

	public void setDeviceLocked(boolean isDeviceLocked) {
		this.isDeviceLocked = isDeviceLocked;
	}

}
