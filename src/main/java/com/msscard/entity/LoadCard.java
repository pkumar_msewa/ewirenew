package com.msscard.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Role;

@Entity
public class LoadCard extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;

	@Column(nullable=true)
	private String referenceNo;
	
	@Column(nullable=true)
	private String comment;
	@Column(nullable=true)
	private double amount;
	@Column(nullable=false)
	private boolean direct;
	@Column(nullable=false)
	private boolean physicalCard;
	@ManyToOne(fetch=FetchType.EAGER)
	private MUser user;
	
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@OneToOne(fetch=FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser agent;

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public boolean isDirect() {
		return direct;
	}

	public void setDirect(boolean direct) {
		this.direct = direct;
	}

	public boolean isPhysicalCard() {
		return physicalCard;
	}

	public void setPhysicalCard(boolean physicalCard) {
		this.physicalCard = physicalCard;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public MUser getAgent() {
		return agent;
	}

	public void setAgent(MUser agent) {
		this.agent = agent;
	}
	
	
}
