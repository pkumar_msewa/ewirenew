package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class PromoCodeRequest extends AbstractEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column (nullable=true)
	private String promocode;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private MUser user;
	
	@OneToOne(fetch=FetchType.EAGER)
	private MTransaction transaction;
	
	@Column(nullable=true)
	private Status status;

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public MTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(MTransaction transaction) {
		this.transaction = transaction;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	
}
