package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "travellerdetails")
@Entity
public class TravellerDetails extends AbstractEntity<Long>  {
	
	private static final long serialVersionUID = 1L;

	@Column
	private String fName;
	@Column
	private String lName;
	@Column
	private String age;
	@Column
	private String gender;
	@Column
	private String seatNo;
	@Column
	private String seatType;
	@Column
	private String fare;

	@ManyToOne
	BusTicket busTicketId;

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public BusTicket getBusTicketId() {
		return busTicketId;
	}

	public void setBusTicketId(BusTicket busTicketId) {
		this.busTicketId = busTicketId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
