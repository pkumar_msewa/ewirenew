package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CorporateFileCatalogue extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String absPath;
	private String s3Path;
	private String fileDescription;
	@ManyToOne
	private MUser corporate;
	@ManyToOne
	private PartnerDetails partnerDetails;
	private boolean reviewStatus=false;
	private boolean fileDeletionStatus=false;
	private boolean fileRejectionStatus=false;
	private String categoryType;
	private boolean schedulerStatus=false;
	private boolean transactionStatus=false;
	
	
	
	
	
	
	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}
	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}
	public boolean isTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(boolean transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public boolean isSchedulerStatus() {
		return schedulerStatus;
	}
	public void setSchedulerStatus(boolean schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public boolean isFileRejectionStatus() {
		return fileRejectionStatus;
	}
	public void setFileRejectionStatus(boolean fileRejectionStatus) {
		this.fileRejectionStatus = fileRejectionStatus;
	}
	public String getAbsPath() {
		return absPath;
	}
	public void setAbsPath(String absPath) {
		this.absPath = absPath;
	}
	public String getS3Path() {
		return s3Path;
	}
	public void setS3Path(String s3Path) {
		this.s3Path = s3Path;
	}
	public String getFileDescription() {
		return fileDescription;
	}
	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}
	
	public MUser getCorporate() {
		return corporate;
	}
	public void setCorporate(MUser corporate) {
		this.corporate = corporate;
	}
	public boolean isReviewStatus() {
		return reviewStatus;
	}
	public void setReviewStatus(boolean reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
	public boolean isFileDeletionStatus() {
		return fileDeletionStatus;
	}
	public void setFileDeletionStatus(boolean fileDeletionStatus) {
		this.fileDeletionStatus = fileDeletionStatus;
	}
	@Override
	public String toString() {
		return "CorporateFileCatalogue [absPath=" + absPath + ", s3Path=" + s3Path + ", fileDescription="
				+ fileDescription + ", corporate=" + corporate + ", reviewStatus=" + reviewStatus
				+ ", fileDeletionStatus=" + fileDeletionStatus + ", fileRejectionStatus=" + fileRejectionStatus
				+ ", categoryType=" + categoryType + ", schedulerStatus=" + schedulerStatus + "]";
	}
	
	
	
	
}
