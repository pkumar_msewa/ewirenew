package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.msscard.model.Status;

@Table(name = "LoadWalletByVARequest")
@Entity
public class LoadWalletByVARequest extends AbstractEntity<Long> {

	private static final long serialVersionUID = -1158329217970443121L;

	@Column(unique = true)
	private String mmId;

	@Column
	private String amount;

	@Column
	private String dateAdded;

	@Column
	private String mmStatus;

	@Column
	private String prefundingType;

	@Column
	private String productCode;

	@Column
	private String agentName;

	@Column
	private String agentCode;

	@Column
	private String virtualAccountNumber;

	@Column
	private String transactionComments;

	@Enumerated(EnumType.STRING)
	private Status status;

	@Column
	private String failedReason;

	@Column
	private boolean failedStatus = false;

	@Column
	private String transactionRefNo;

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getMmStatus() {
		return mmStatus;
	}

	public void setMmStatus(String mmStatus) {
		this.mmStatus = mmStatus;
	}

	public String getPrefundingType() {
		return prefundingType;
	}

	public void setPrefundingType(String prefundingType) {
		this.prefundingType = prefundingType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getVirtualAccountNumber() {
		return virtualAccountNumber;
	}

	public void setVirtualAccountNumber(String virtualAccountNumber) {
		this.virtualAccountNumber = virtualAccountNumber;
	}

	public String getTransactionComments() {
		return transactionComments;
	}

	public void setTransactionComments(String transactionComments) {
		this.transactionComments = transactionComments;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	public boolean isFailedStatus() {
		return failedStatus;
	}

	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

}