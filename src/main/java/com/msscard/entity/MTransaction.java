package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.msscard.model.Status;
import com.msscard.model.TransactionType;

@Entity
public class MTransaction extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(optional = false)
	private MPQAccountDetails account;

	private double amount;

	private boolean debit;

	@Column(nullable = true)
	private boolean favourite;

	private double currentBalance;

	@Column(nullable = true)
	private String description;

	private TransactionType transactionType = TransactionType.DEFAULT;

	@OneToOne(fetch = FetchType.EAGER)
	private MService service;

	@Column(unique = true)
	private String transactionRefNo;

	@Column(nullable = true)
	private String OTP;


	@Enumerated(EnumType.STRING)
	private Status status;

	@Lob
	private String request;
	
	@Column(unique=true)
	private String retrivalReferenceNo;
	
	@Column(unique=true)
	private String authReferenceNo;
	
	private String upiId;
	
	private String remarks;

	private String cardLoadStatus;
	
	@Column
	private boolean isAndriod=false;
	
	private double commissionEarned=0.0;

	
	private boolean isMdexTransaction=false;
	
	private boolean isSuspicious=true;
	
	private String mdexTransactionStatus;
	
	@Column	
	private String batch;	
	
	
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getMdexTransactionStatus() {
		return mdexTransactionStatus;
	}

	public void setMdexTransactionStatus(String mdexTransactionStatus) {
		this.mdexTransactionStatus = mdexTransactionStatus;
	}

	public boolean isSuspicious() {
		return isSuspicious;
	}

	public void setSuspicious(boolean isSuspicious) {
		this.isSuspicious = isSuspicious;
	}

	public double getCommissionEarned() {
		return commissionEarned;
	}

	public void setCommissionEarned(double commissionEarned) {
		this.commissionEarned = commissionEarned;
	}

	public String getCardLoadStatus() {
		return cardLoadStatus;
	}

	public void setCardLoadStatus(String cardLoadStatus) {
		this.cardLoadStatus = cardLoadStatus;
	}

	public MPQAccountDetails getAccount() {
		return account;
	}

	public void setAccount(MPQAccountDetails account) {
		this.account = account;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isDebit() {
		return debit;
	}

	public void setDebit(boolean debit) {
		this.debit = debit;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public MService getService() {
		return service;
	}

	public void setService(MService service) {
		this.service = service;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String oTP) {
		OTP = oTP;
	}

	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getRetrivalReferenceNo() {
		return retrivalReferenceNo;
	}

	public void setRetrivalReferenceNo(String retrivalReferenceNo) {
		this.retrivalReferenceNo = retrivalReferenceNo;
	}

	public String getAuthReferenceNo() {
		return authReferenceNo;
	}

	public void setAuthReferenceNo(String authReferenceNo) {
		this.authReferenceNo = authReferenceNo;
	}

	public String getUpiId() {
		return upiId;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isAndriod() {
		return isAndriod;
	}

	public void setAndriod(boolean isAndriod) {
		this.isAndriod = isAndriod;
	}
	
	public boolean isMdexTransaction() {
		return isMdexTransaction;
	}

	public void setMdexTransaction(boolean isMdexTransaction) {
		this.isMdexTransaction = isMdexTransaction;
	}

	@Override
	public String toString() {
		return "MTransaction [account=" + account + ", amount=" + amount + ", debit=" + debit + ", favourite="
				+ favourite + ", currentBalance=" + currentBalance + ", description=" + description
				+ ", transactionType=" + transactionType + ", service=" + service + ", transactionRefNo="
				+ transactionRefNo + ", OTP=" + OTP + ", status=" + status + ", request=" + request
				+ ", retrivalReferenceNo=" + retrivalReferenceNo + ", authReferenceNo=" + authReferenceNo + ", upiId="
				+ upiId + ", remarks=" + remarks + ", cardLoadStatus=" + cardLoadStatus + ", isAndriod=" + isAndriod
				+ ", commissionEarned=" + commissionEarned + ", isMdexTransaction=" + isMdexTransaction
				+ ", isSuspicious=" + isSuspicious + "]";
	}

	
}
