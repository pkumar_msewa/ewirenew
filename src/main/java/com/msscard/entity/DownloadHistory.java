package com.msscard.entity;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.msscard.model.Status;

@Entity

public class DownloadHistory extends AbstractEntity<Long>  {
	
	private static final long serialVersionUID = 1L;
	
	
	
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	private String fileExtension;

	private boolean isCorporate;
	
	
	

	public boolean isCorporate() {
		return isCorporate;
	}

	public void setCorporate(boolean isCorporate) {
		this.isCorporate = isCorporate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

}
