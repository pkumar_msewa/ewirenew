package com.msscard.entity;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class CashBack extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double amount;
	@ManyToOne
	private MerchantDetails merchantDetails;
	@OneToOne(fetch=FetchType.EAGER)
	private MTransaction transaction;
	@ManyToOne
	private MUser user;
	
	
	public MerchantDetails getMerchantDetails() {
		return merchantDetails;
	}
	public void setMerchantDetails(MerchantDetails merchantDetails) {
		this.merchantDetails = merchantDetails;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public MTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(MTransaction transaction) {
		this.transaction = transaction;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	
	
	
}
