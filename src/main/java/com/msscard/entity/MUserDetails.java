package com.msscard.entity;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity

public class MUserDetails extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = true)
	private String firstName;

	@Column
	private String middleName;

	@Column(nullable = true)
	private String lastName;

	@Column
	private String address;

	@Column(nullable = false)
	private String contactNo;

	@Column(nullable = true)
	private String email;

	@Column
	private String image;

	@Column
	private String mpin;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column
	private byte[] imageContent;

	@Column
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	@OneToOne(fetch = FetchType.EAGER)
	private MLocationDetails location;

//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
//	private IPLPredictEntity> predict;

	@Column
	private String brand;

	@Column
	private String model;

	@Column
	private String imeiNo;

	@Column
	private String fingerPrint;

	@Column
	private String latitude;

	@Column
	private String longitude;

	private String gender;

	@Column(nullable = true)
	private String aadharNo;

	@Column(nullable = false)
	private boolean hasMpin = false;

	@Column(nullable = true)
	private boolean hasAadhar = false;

	@Column(nullable = true)
	private String idType;

	@Column(nullable = true)
	private String idNo;

	@Column(nullable = true)
	private String lastLoginDevice;

	private String transactionId;

	private String aadharHashId;

	@Column
	private String groupStatus;

	@Column
	private String countryName;

	@Column
	private String remark;

	@Column
	private String batchNo;

	@Column
	private String fingooleService;

	@Column
	private String fingooleAmount;

	@Column
	private boolean groupChange = false;

	@Column
	private String changedGroupName;

	@Column
	private String deletionGroupReason;

	@Column
	private String groupRejectReason;

	@Column
	private String previousGroupContact;

	public String getPreviousGroupContact() {
		return previousGroupContact;
	}

	public void setPreviousGroupContact(String previousGroupContact) {
		this.previousGroupContact = previousGroupContact;
	}

	public String getGroupRejectReason() {
		return groupRejectReason;
	}

	public void setGroupRejectReason(String groupRejectReason) {
		this.groupRejectReason = groupRejectReason;
	}

	public String getDeletionGroupReason() {
		return deletionGroupReason;
	}

	public void setDeletionGroupReason(String deletionGroupReason) {
		this.deletionGroupReason = deletionGroupReason;
	}

	public String getChangedGroupName() {
		return changedGroupName;
	}

	public void setChangedGroupName(String changedGroupName) {
		this.changedGroupName = changedGroupName;
	}

	public boolean isGroupChange() {
		return groupChange;
	}

	public void setGroupChange(boolean groupChange) {
		this.groupChange = groupChange;
	}

	public String getFingooleAmount() {
		return fingooleAmount;
	}

	public void setFingooleAmount(String fingooleAmount) {
		this.fingooleAmount = fingooleAmount;
	}

	public String getFingooleService() {
		return fingooleService;
	}

	public void setFingooleService(String fingooleService) {
		this.fingooleService = fingooleService;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(String groupStatus) {
		this.groupStatus = groupStatus;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getAadharHashId() {
		return aadharHashId;
	}

	public void setAadharHashId(String aadharHashId) {
		this.aadharHashId = aadharHashId;
	}

	public String getLastLoginDevice() {
		return lastLoginDevice;
	}

	public void setLastLoginDevice(String lastLoginDevice) {
		this.lastLoginDevice = lastLoginDevice;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public byte[] getImageContent() {
		return imageContent;
	}

	public void setImageContent(byte[] imageContent) {
		this.imageContent = imageContent;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public MLocationDetails getLocation() {
		return location;
	}

	public void setLocation(MLocationDetails location) {
		this.location = location;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getImeiNo() {
		return imeiNo;
	}

	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	public String getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(String fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public boolean isHasMpin() {
		return hasMpin;
	}

	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}

	public boolean isHasAadhar() {
		return hasAadhar;
	}

	public void setHasAadhar(boolean hasAadhar) {
		this.hasAadhar = hasAadhar;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((aadharHashId == null) ? 0 : aadharHashId.hashCode());
		result = prime * result + ((aadharNo == null) ? 0 : aadharNo.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((contactNo == null) ? 0 : contactNo.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fingerPrint == null) ? 0 : fingerPrint.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + (hasAadhar ? 1231 : 1237);
		result = prime * result + (hasMpin ? 1231 : 1237);
		result = prime * result + ((idNo == null) ? 0 : idNo.hashCode());
		result = prime * result + ((idType == null) ? 0 : idType.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + Arrays.hashCode(imageContent);
		result = prime * result + ((imeiNo == null) ? 0 : imeiNo.hashCode());
		result = prime * result + ((lastLoginDevice == null) ? 0 : lastLoginDevice.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((mpin == null) ? 0 : mpin.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MUserDetails))
			return false;
		MUserDetails other = (MUserDetails) obj;
		if (aadharHashId == null) {
			if (other.aadharHashId != null)
				return false;
		} else if (!aadharHashId.equals(other.aadharHashId))
			return false;
		if (aadharNo == null) {
			if (other.aadharNo != null)
				return false;
		} else if (!aadharNo.equals(other.aadharNo))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (contactNo == null) {
			if (other.contactNo != null)
				return false;
		} else if (!contactNo.equals(other.contactNo))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fingerPrint == null) {
			if (other.fingerPrint != null)
				return false;
		} else if (!fingerPrint.equals(other.fingerPrint))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (hasAadhar != other.hasAadhar)
			return false;
		if (hasMpin != other.hasMpin)
			return false;
		if (idNo == null) {
			if (other.idNo != null)
				return false;
		} else if (!idNo.equals(other.idNo))
			return false;
		if (idType == null) {
			if (other.idType != null)
				return false;
		} else if (!idType.equals(other.idType))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (!Arrays.equals(imageContent, other.imageContent))
			return false;
		if (imeiNo == null) {
			if (other.imeiNo != null)
				return false;
		} else if (!imeiNo.equals(other.imeiNo))
			return false;
		if (lastLoginDevice == null) {
			if (other.lastLoginDevice != null)
				return false;
		} else if (!lastLoginDevice.equals(other.lastLoginDevice))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (mpin == null) {
			if (other.mpin != null)
				return false;
		} else if (!mpin.equals(other.mpin))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
