package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.msscard.model.UserType;

@Entity
public class MKycDetail extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = true)
	private String pancardImage;

	@Column(nullable = true)
	private String aadharImage;
	
	@Column(nullable = true)
	private String aadharImage1;
	
	@Column(nullable = true)
	private String accountNo;

	@Column(nullable = true)
	private String ifscCode;
	
	@Column(nullable = true)
	private String idType;

	@Column(nullable = true)
	private String idNumber;
	
	@Column(nullable = true)
	private String idImage;
	
	@Column(nullable=true)
	private String idImage1;
	
	@Column(nullable = true)
	private String address1;
	
	@Column(nullable = true)
	private String address2;

	@Column(nullable = true)
	private String city;
	
	@Column(nullable = true)
	private String state;

	@Column(nullable = true)
	private String country;
	
	@Column(nullable = true)
	private String pinCode;
	
	@Column(nullable = false)
	private UserType userType;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MPQAccountType accountType;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MUser user;

	private boolean rejectionStatus=false;
	
	private String rejectionReason;
	
	
	
	
	
	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public boolean isRejectionStatus() {
		return rejectionStatus;
	}

	public void setRejectionStatus(boolean rejectionStatus) {
		this.rejectionStatus = rejectionStatus;
	}

	public String getIdImage1() {
		return idImage1;
	}

	public void setIdImage1(String idImage1) {
		this.idImage1 = idImage1;
	}

	public String getPancardImage() {
		return pancardImage;
	}

	public void setPancardImage(String pancardImage) {
		this.pancardImage = pancardImage;
	}

	public String getAadharImage() {
		return aadharImage;
	}

	public void setAadharImage(String aadharImage) {
		this.aadharImage = aadharImage;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public MPQAccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(MPQAccountType accountType) {
		this.accountType = accountType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdImage() {
		return idImage;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getAadharImage1() {
		return aadharImage1;
	}

	public void setAadharImage1(String aadharImage1) {
		this.aadharImage1 = aadharImage1;
	}

	
	
}
