package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CorporateRoles extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	
	private String description;
	
	@ManyToOne
	private MService services;
	
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MService getServices() {
		return services;
	}

	public void setServices(MService services) {
		this.services = services;
	} 
	
	

}
