package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class MMMerchantDetails extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private String logo;

	@Column(unique = true,nullable = false)
	private String mid;

	private String email;

	private String mobile;

	private long mcc;

	@OneToOne(fetch = FetchType.EAGER)
	private MPQAccountDetails account;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getMcc() {
		return mcc;
	}

	public void setMcc(long mcc) {
		this.mcc = mcc;
	}

	public MPQAccountDetails getAccount() {
		return account;
	}

	public void setAccount(MPQAccountDetails account) {
		this.account = account;
	}


}
