package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.msscard.model.Status;

@Entity
public class MService extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String code;
	private String operatorCode;
	private String description;
	private double minAmount;
	private double maxAmount;
	private String serviceName;
	private String serviceImage;
	
	
	
	public String getServiceImage() {
		return serviceImage;
	}
	public void setServiceImage(String serviceImage) {
		this.serviceImage = serviceImage;
	}
	@OneToOne(fetch = FetchType.EAGER)
	private MServiceType serviceType;

	@OneToOne(fetch = FetchType.EAGER)
	private MOperator operator;

	@Enumerated(EnumType.STRING)
	private Status status;

	
	
	
	public MServiceType getServiceType() {
		return serviceType;
	}
	public void setServiceType(MServiceType serviceType) {
		this.serviceType = serviceType;
	}
	public MOperator getOperator() {
		return operator;
	}
	public void setOperator(MOperator operator) {
		this.operator = operator;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getOperatorCode() {
		return operatorCode;
	}
	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(double minAmount) {
		this.minAmount = minAmount;
	}
	public double getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	
}
