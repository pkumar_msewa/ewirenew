package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.Lob;
@Entity
public class FCMDetails extends AbstractEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Lob
	private String message;
	private String title;
	private String imagePath;
	private boolean hasImage=false;
	private String status;
	private boolean cronStatus=false;
	private boolean allUsers=false;
	private boolean singleUser=false;
	private String username;
	private boolean forIos=false;
	
	
	
	
	public boolean isForIos() {
		return forIos;
	}
	public void setForIos(boolean forIos) {
		this.forIos = forIos;
	}
	public boolean isAllUsers() {
		return allUsers;
	}
	public void setAllUsers(boolean allUsers) {
		this.allUsers = allUsers;
	}
	public boolean isSingleUser() {
		return singleUser;
	}
	public void setSingleUser(boolean singleUser) {
		this.singleUser = singleUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public boolean isHasImage() {
		return hasImage;
	}
	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isCronStatus() {
		return cronStatus;
	}
	public void setCronStatus(boolean cronStatus) {
		this.cronStatus = cronStatus;
	}
	
	
	
	
	
}
