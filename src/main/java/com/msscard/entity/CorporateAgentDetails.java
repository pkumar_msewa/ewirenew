package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class CorporateAgentDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 6472487246973418411L;

	private String corporateName;

	private String contactPersonName;

	private String contactPersonNumber;

	private String bankAccountNo;

	private String ifscCode;

	private String seriesSize;

	@ManyToOne
	private MService services;

	@OneToOne
	private MUser corporate;

	private boolean serviceStatus = false;

	private String corporateLogo1;

	private String corporateLogo2;

	private String virtualAccountNumber;

	public String getCorporateLogo1() {
		return corporateLogo1;
	}

	public void setCorporateLogo1(String corporateLogo1) {
		this.corporateLogo1 = corporateLogo1;
	}

	public String getCorporateLogo2() {
		return corporateLogo2;
	}

	public void setCorporateLogo2(String corporateLogo2) {
		this.corporateLogo2 = corporateLogo2;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public boolean isServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(boolean serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public MUser getCorporate() {
		return corporate;
	}

	public void setCorporate(MUser corporate) {
		this.corporate = corporate;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getSeriesSize() {
		return seriesSize;
	}

	public void setSeriesSize(String seriesSize) {
		this.seriesSize = seriesSize;
	}

	public MService getServices() {
		return services;
	}

	public void setServices(MService services) {
		this.services = services;
	}

	public String getVirtualAccountNumber() {
		return virtualAccountNumber;
	}

	public void setVirtualAccountNumber(String virtualAccountNumber) {
		this.virtualAccountNumber = virtualAccountNumber;
	}

}