package com.msscard.entity;

import javax.persistence.Entity;

@Entity
public class MerchantDetails extends AbstractEntity<Long>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantName;
	private String bcName;
	private String bcNumber;
	private String bcEmail;
	private String filePath;
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getBcName() {
		return bcName;
	}
	public void setBcName(String bcName) {
		this.bcName = bcName;
	}
	public String getBcNumber() {
		return bcNumber;
	}
	public void setBcNumber(String bcNumber) {
		this.bcNumber = bcNumber;
	}
	public String getBcEmail() {
		return bcEmail;
	}
	public void setBcEmail(String bcEmail) {
		this.bcEmail = bcEmail;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	

}
