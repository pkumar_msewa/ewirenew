package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GroupOtpMobile extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String mobileNumber;
	
	@Column
	private String groupName;
	
	@Column
	private String contactName;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	
	
}
