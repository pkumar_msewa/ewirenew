package com.msscard.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class MAgentAssignPhysicalCard extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser agent;
	
	@OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
	private MUser user;
	
	@Column(nullable = false)
	private double amount;
	
	@Column(nullable = false)
	private String kitNo;

	public MUser getAgent() {
		return agent;
	}

	public void setAgent(MUser agent) {
		this.agent = agent;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getKitNo() {
		return kitNo;
	}

	public void setKitNo(String kitNo) {
		this.kitNo = kitNo;
	}
	
	
	
	

	
}
