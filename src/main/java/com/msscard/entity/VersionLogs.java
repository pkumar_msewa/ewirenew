package com.msscard.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class VersionLogs extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String version;

	private String status;
	
	private boolean isNewVersion=false;
	
	@OneToOne(fetch = FetchType.EAGER)
	private ApiVersion androidVersion;
	
	

	public boolean isNewVersion() {
		return isNewVersion;
	}

	public void setNewVersion(boolean isNewVersion) {
		this.isNewVersion = isNewVersion;
	}

	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ApiVersion getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(ApiVersion androidVersion) {
		this.androidVersion = androidVersion;
	}
}
