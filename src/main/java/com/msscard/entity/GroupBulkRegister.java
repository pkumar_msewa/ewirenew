package com.msscard.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.msscard.model.Status;

@Entity
@Table(name = "GroupBulkRegister")
public class GroupBulkRegister extends AbstractEntity<Long> {

	private static final long serialVersionUID = 8453654076725018243L;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column
	private String middlename;

	@Column
	private String mobile;

	@Column
	private String email;

	@Column
	@Temporal(TemporalType.DATE)
	private Date dob;

	@Column
	private String idType;

	@Column
	private String idNumber;

	@OneToOne
	private GroupDetails groupDetail;

	@OneToOne
	private GroupFileCatalogue groupFileCatalogue;

	@Column
	private boolean cronStatus = false;

	@Column
	private String failReason;

	@Column
	private boolean successStatus = false;

	@Column
	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public GroupFileCatalogue getGroupFileCatalogue() {
		return groupFileCatalogue;
	}

	public void setGroupFileCatalogue(GroupFileCatalogue groupFileCatalogue) {
		this.groupFileCatalogue = groupFileCatalogue;
	}

	public boolean isCronStatus() {
		return cronStatus;
	}

	public void setCronStatus(boolean cronStatus) {
		this.cronStatus = cronStatus;
	}

	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public boolean isSuccessStatus() {
		return successStatus;
	}

	public void setSuccessStatus(boolean successStatus) {
		this.successStatus = successStatus;
	}

	public GroupDetails getGroupDetail() {
		return groupDetail;
	}

	public void setGroupDetail(GroupDetails groupDetail) {
		this.groupDetail = groupDetail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

}