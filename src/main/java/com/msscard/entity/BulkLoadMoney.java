package com.msscard.entity;

 import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class BulkLoadMoney extends AbstractEntity<Long> {

	private static final long serialVersionUID = 4151010998619330509L;

	private String amount;

	private String mobile;

	private String email;

	private String dateOfTransaction;

	private String transactionRefNo;

	private boolean walletLoadStatus = false;

	private boolean cardLoadStatus = false;

	private String failedReason;

	private boolean failedStatus = false;

	@ManyToOne
	private MUser corporate;

	@ManyToOne
	private PartnerDetails partnerDetails;

	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	public boolean isFailedStatus() {
		return failedStatus;
	}

	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public MUser getCorporate() {
		return corporate;
	}

	public void setCorporate(MUser corporate) {
		this.corporate = corporate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(String dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public boolean isWalletLoadStatus() {
		return walletLoadStatus;
	}

	public void setWalletLoadStatus(boolean walletLoadStatus) {
		this.walletLoadStatus = walletLoadStatus;
	}

	public boolean isCardLoadStatus() {
		return cardLoadStatus;
	}

	public void setCardLoadStatus(boolean cardLoadStatus) {
		this.cardLoadStatus = cardLoadStatus;
	}

	public PartnerDetails getPartnerDetails() {
		return partnerDetails;
	}

	public void setPartnerDetails(PartnerDetails partnerDetails) {
		this.partnerDetails = partnerDetails;
	}

}