package com.msscard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="bulkregistergroup")
@Entity
public class BulkRegisterGroup extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 8453654076725018243L;
	
	@Column
	private String firstName;
	private String lastName;
	private String idNumber;
	private String idtype;
	private String failedReason;
	private boolean failedStatus;
	@OneToOne
	private GroupDetails groupdetails;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getIdtype() {
		return idtype;
	}
	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}
	public String getFailedReason() {
		return failedReason;
	}
	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}
	public boolean isFailedStatus() {
		return failedStatus;
	}
	public void setFailedStatus(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}
	public GroupDetails getGroupdetails() {
		return groupdetails;
	}
	public void setGroupdetails(GroupDetails groupdetails) {
		this.groupdetails = groupdetails;
	}
	@Column(unique=true,nullable=false)
	private String mobile;
	@Column(unique=true,nullable=false)
	private String email;
	private String kycStatus;
	private String dob;
	private String proxyNo;
	private String cardNo;
	private boolean userCreationStatus=false;
	private boolean walletCreationStatus=false;
	private boolean cardCreationStatus=false;
	private boolean physicalCardCreationStatus=false;
	private boolean phyCardActivationStatus=false;
	private String userCreationError;
	private String walletCreationError;
	private String cardCreationError;
	private String phyCardCreationStatus;
	private String cardActivationStatus;
	private String driver_id;
	
	
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	@OneToOne(fetch=FetchType.EAGER)
	private MUser user;	
	
	
	public boolean isPhysicalCardCreationStatus() {
		return physicalCardCreationStatus;
	}
	public void setPhysicalCardCreationStatus(boolean physicalCardCreationStatus) {
		this.physicalCardCreationStatus = physicalCardCreationStatus;
	}
	public boolean isPhyCardActivationStatus() {
		return phyCardActivationStatus;
	}
	public void setPhyCardActivationStatus(boolean phyCardActivationStatus) {
		this.phyCardActivationStatus = phyCardActivationStatus;
	}
	public String getPhyCardCreationStatus() {
		return phyCardCreationStatus;
	}
	public void setPhyCardCreationStatus(String phyCardCreationStatus) {
		this.phyCardCreationStatus = phyCardCreationStatus;
	}
	public String getCardActivationStatus() {
		return cardActivationStatus;
	}
	public void setCardActivationStatus(String cardActivationStatus) {
		this.cardActivationStatus = cardActivationStatus;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getKycStatus() {
		return kycStatus;
	}
	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getProxyNo() {
		return proxyNo;
	}
	public void setProxyNo(String proxyNo) {
		this.proxyNo = proxyNo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public boolean isUserCreationStatus() {
		return userCreationStatus;
	}
	public void setUserCreationStatus(boolean userCreationStatus) {
		this.userCreationStatus = userCreationStatus;
	}
	public boolean isWalletCreationStatus() {
		return walletCreationStatus;
	}
	public void setWalletCreationStatus(boolean walletCreationStatus) {
		this.walletCreationStatus = walletCreationStatus;
	}
	public boolean isCardCreationStatus() {
		return cardCreationStatus;
	}
	public void setCardCreationStatus(boolean cardCreationStatus) {
		this.cardCreationStatus = cardCreationStatus;
	}
	public String getUserCreationError() {
		return userCreationError;
	}
	public void setUserCreationError(String userCreationError) {
		this.userCreationError = userCreationError;
	}
	public String getWalletCreationError() {
		return walletCreationError;
	}
	public void setWalletCreationError(String walletCreationError) {
		this.walletCreationError = walletCreationError;
	}
	public String getCardCreationError() {
		return cardCreationError;
	}
	public void setCardCreationError(String cardCreationError) {
		this.cardCreationError = cardCreationError;
	}

}
