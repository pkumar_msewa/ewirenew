package com.msscard.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SeatObject {

    @JsonProperty("firstColumn")
    private List<SeatDTO> firstColumn;

    @JsonProperty("secondColumn")
    private List<SeatDTO> secondColumn;

    @JsonProperty("thirdColumn")
    private List<SeatDTO> thirdColumn;

    @JsonProperty("fourthColumn")
    private List<SeatDTO> fourthColumn;

    @JsonProperty("fifthColumn")
    private List<SeatDTO> fifthColumn;

    @JsonProperty("sixthColumn")
    private List<SeatDTO> sixthColumn;

    @JsonProperty("seventhColumn")
    private List<SeatDTO> seventhColumn;

    @JsonProperty("eightColumn")
    private List<SeatDTO> eightColumn;

    @JsonProperty("ninethColumn")
    private List<SeatDTO> ninethColumn;

    public List<SeatDTO> getFirstColumn() {
        return firstColumn;
    }

    public void setFirstColumn(List<SeatDTO> firstColumn) {
        this.firstColumn = firstColumn;
    }

    public List<SeatDTO> getSecondColumn() {
        return secondColumn;
    }

    public void setSecondColumn(List<SeatDTO> secondColumn) {
        this.secondColumn = secondColumn;
    }

    public List<SeatDTO> getThirdColumn() {
        return thirdColumn;
    }

    public void setThirdColumn(List<SeatDTO> thirdColumn) {
        this.thirdColumn = thirdColumn;
    }

    public List<SeatDTO> getFourthColumn() {
        return fourthColumn;
    }

    public void setFourthColumn(List<SeatDTO> fourthColumn) {
        this.fourthColumn = fourthColumn;
    }

    public List<SeatDTO> getFifthColumn() {
        return fifthColumn;
    }

    public void setFifthColumn(List<SeatDTO> fifthColumn) {
        this.fifthColumn = fifthColumn;
    }

    public List<SeatDTO> getSixthColumn() {
        return sixthColumn;
    }

    public void setSixthColumn(List<SeatDTO> sixthColumn) {
        this.sixthColumn = sixthColumn;
    }

    public List<SeatDTO> getSeventhColumn() {
        return seventhColumn;
    }

    public void setSeventhColumn(List<SeatDTO> seventhColumn) {
        this.seventhColumn = seventhColumn;
    }

    public List<SeatDTO> getEightColumn() {
        return eightColumn;
    }

    public void setEightColumn(List<SeatDTO> eightColumn) {
        this.eightColumn = eightColumn;
    }

    public List<SeatDTO> getNinethColumn() {
        return ninethColumn;
    }

    public void setNinethColumn(List<SeatDTO> ninethColumn) {
        this.ninethColumn = ninethColumn;
    }
    
}
