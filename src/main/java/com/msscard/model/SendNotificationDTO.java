package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class SendNotificationDTO {

	private String message;
	private String title;
	private MultipartFile image;
	private String withImage;
	private String username;
	private String singleUser;
	private String allUser;
	private String userMobile;
	private String forIos;
	private String shortenedimageUrl;
	private String checked;
		
	
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getShortenedimageUrl() {
		return shortenedimageUrl;
	}
	public void setShortenedimageUrl(String shortenedimageUrl) {
		this.shortenedimageUrl = shortenedimageUrl;
	}
	public String getForIos() {
		return forIos;
	}
	public void setForIos(String forIos) {
		this.forIos = forIos;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public String getSingleUser() {
		return singleUser;
	}
	public void setSingleUser(String singleUser) {
		this.singleUser = singleUser;
	}
	public String getAllUser() {
		return allUser;
	}
	public void setAllUser(String allUser) {
		this.allUser = allUser;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getWithImage() {
		return withImage;
	}
	public void setWithImage(String withImage) {
		this.withImage = withImage;
	}
	
	
	
}
