package com.msscard.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.msscard.entity.GroupDetails;

public class GroupDetailDTO {

	private String sessionId;	
	private String address;
	private String emailAddressId;
	private String mobileNoId;
	private String organization;
	private String account;
	private String remark;
	private String name;
	private String city;
	private String state;
	private String country;
	private List<GroupDetails> groupName;
	private String code;
	private String message;
	private String status;
	private boolean success;
	private String countryCode;
	private MultipartFile image;	
	private String dname;
		
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailAddressId() {
		return emailAddressId;
	}
	public void setEmailAddressId(String emailAddressId) {
		this.emailAddressId = emailAddressId;
	}
	public String getMobileNoId() {
		return mobileNoId;
	}
	public void setMobileNoId(String mobileNoId) {
		this.mobileNoId = mobileNoId;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<GroupDetails> getGroupName() {
		return groupName;
	}
	public void setGroupName(List<GroupDetails> groupName) {
		this.groupName = groupName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
}
