package com.msscard.model;

import java.util.Date;

public class MUserDTO {
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private String contactNo;
	
	private Date dob;
	
	private String aadharCardImageFront;
	
	private String aadharCardImageBack;
	
	private String panCardImage;
	
	private boolean isKYC;
	
	private String daterange;
	
	private String fromDate;
	
	private String toDate;
	
	private String name;
	
	private String userType;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}


	public String getPanCardImage() {
		return panCardImage;
	}

	public void setPanCardImage(String panCardImage) {
		this.panCardImage = panCardImage;
	}

	public String getAadharCardImageFront() {
		return aadharCardImageFront;
	}

	public void setAadharCardImageFront(String aadharCardImageFront) {
		this.aadharCardImageFront = aadharCardImageFront;
	}

	public String getAadharCardImageBack() {
		return aadharCardImageBack;
	}

	public void setAadharCardImageBack(String aadharCardImageBack) {
		this.aadharCardImageBack = aadharCardImageBack;
	}

	public boolean isKYC() {
		return isKYC;
	}

	public void setKYC(boolean isKYC) {
		this.isKYC = isKYC;
	}

	public String getDaterange() {
		return daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	

}
