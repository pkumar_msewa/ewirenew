package com.msscard.model;

import com.msscard.entity.MUser;

public class CorporateBulkUsers {

	private String username;
	private String mobile;
	private String email;
	private String dob;
	private String authority;
	private String cardNumber;
	private String cardStatus;
	private MUser user;
	private boolean userCreationStatus;
	private boolean virtualCardCreationStatus;
	private boolean physicalCardCreationStatus;
	private String driver_id;
	private String kycStatus;
	
	
	
	
	
	
	
	public String getKycStatus() {
		return kycStatus;
	}
	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	public MUser getUser() {
		return user;
	}
	public void setUser(MUser user) {
		this.user = user;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	

	public String getCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	public boolean isUserCreationStatus() {
		return userCreationStatus;
	}
	public void setUserCreationStatus(boolean userCreationStatus) {
		this.userCreationStatus = userCreationStatus;
	}
	public boolean isVirtualCardCreationStatus() {
		return virtualCardCreationStatus;
	}
	public void setVirtualCardCreationStatus(boolean virtualCardCreationStatus) {
		this.virtualCardCreationStatus = virtualCardCreationStatus;
	}
	public boolean isPhysicalCardCreationStatus() {
		return physicalCardCreationStatus;
	}
	public void setPhysicalCardCreationStatus(boolean physicalCardCreationStatus) {
		this.physicalCardCreationStatus = physicalCardCreationStatus;
	}
	@Override
	public String toString() {
		return "CorporateBulkUsers [username=" + username + ", mobile=" + mobile + ", email=" + email + ", dob=" + dob
				+ ", authority=" + authority + ", cardNumber=" + cardNumber + ", cardStatus=" + cardStatus + ", user="
				+ user + ", userCreationStatus=" + userCreationStatus + ", virtualCardCreationStatus="
				+ virtualCardCreationStatus + ", physicalCardCreationStatus=" + physicalCardCreationStatus + "]";
	}
		
	
}
