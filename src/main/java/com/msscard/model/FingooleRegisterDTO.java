package com.msscard.model;

import java.util.ArrayList;

public class FingooleRegisterDTO {

	private String sessionId;
	private String name;
	private String mobileNumber;
	private String noOfDays;
	private String email;
	private String batchNumber;
	private String serviceProvider;	
	private String transactionRefNo;
	private String amount;
	
	private ArrayList<FingooleRegisterDTO> registrationData;	
			
	public ArrayList<FingooleRegisterDTO> getRegistrationData() {
		return registrationData;
	}
	public void setRegistrationData(ArrayList<FingooleRegisterDTO> registrationData) {
		this.registrationData = registrationData;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	
	
}
