package com.msscard.model;

public class PromoCodeRequestDTO {

	private String promoCode;
	private String name;
	private String amount;
	private String percentage;
	private String contactNo;
	private String email;
	private String maxCashBack;
	private String minTrxAmount;
	private boolean amountType;
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMaxCashBack() {
		return maxCashBack;
	}
	public void setMaxCashBack(String maxCashBack) {
		this.maxCashBack = maxCashBack;
	}
	public String getMinTrxAmount() {
		return minTrxAmount;
	}
	public void setMinTrxAmount(String minTrxAmount) {
		this.minTrxAmount = minTrxAmount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public boolean isAmountType() {
		return amountType;
	}
	public void setAmountType(boolean amountType) {
		this.amountType = amountType;
	}
	
}
