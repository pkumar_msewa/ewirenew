package com.msscard.model;

public class BusDetailsDTO {	
	
	private String userMobile;	
	private String userEmail;	
	private String ticketPnr;	
	private String operatorPnr;	
	private String emtTxnId;	
	private String emtTransactionScreenId;	
	private String busId;
	private double totalFare;	
	private String priceRecheckTotalFare;	
	private Status status;
	private String journeyDate;
	private String source;
	private String destination;
	private String boardingId;
	private String boardingAddress;
	private String busOperator;
	private String arrTime;
	private String deptTime;
	private String boardTime;
	private String seatHoldId;
	private String tripId;
	private String transactionId;
	private String transactionDate;
	private String transactionStatus;
	private String commissionAmount;
	private String firstName;
	private String dob;
	private String lastName;
	
	public String getCommissionAmount() {
		return commissionAmount;
	}
	public void setCommissionAmount(String commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getTicketPnr() {
		return ticketPnr;
	}
	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}
	public String getOperatorPnr() {
		return operatorPnr;
	}
	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}
	public String getEmtTxnId() {
		return emtTxnId;
	}
	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}
	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}
	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getPriceRecheckTotalFare() {
		return priceRecheckTotalFare;
	}
	public void setPriceRecheckTotalFare(String priceRecheckTotalFare) {
		this.priceRecheckTotalFare = priceRecheckTotalFare;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getBoardingId() {
		return boardingId;
	}
	public void setBoardingId(String boardingId) {
		this.boardingId = boardingId;
	}
	public String getBoardingAddress() {
		return boardingAddress;
	}
	public void setBoardingAddress(String boardingAddress) {
		this.boardingAddress = boardingAddress;
	}
	public String getBusOperator() {
		return busOperator;
	}
	public void setBusOperator(String busOperator) {
		this.busOperator = busOperator;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getDeptTime() {
		return deptTime;
	}
	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	@Override
	public String toString() {
		return "BusDetailsDTO [userMobile=" + userMobile + ", userEmail=" + userEmail + ", ticketPnr=" + ticketPnr
				+ ", operatorPnr=" + operatorPnr + ", emtTxnId=" + emtTxnId + ", emtTransactionScreenId="
				+ emtTransactionScreenId + ", busId=" + busId + ", totalFare=" + totalFare + ", priceRecheckTotalFare="
				+ priceRecheckTotalFare + ", status=" + status + ", journeyDate=" + journeyDate + ", source=" + source
				+ ", destination=" + destination + ", boardingId=" + boardingId + ", boardingAddress=" + boardingAddress
				+ ", busOperator=" + busOperator + ", arrTime=" + arrTime + ", deptTime=" + deptTime + ", boardTime="
				+ boardTime + ", seatHoldId=" + seatHoldId + ", tripId=" + tripId + ", transactionId=" + transactionId
				+ ", transactionDate=" + transactionDate + ", transactionStatus=" + transactionStatus
				+ ", commissionAmount=" + commissionAmount + ", firstName=" + firstName + ", dob=" + dob + ", lastName="
				+ lastName + "]";
	}
	
	
}
