package com.msscard.model.error;

public class RegisterError {

	private boolean valid;

	private String username;

	private String password;

	private String confirmPassword;

	private String userType;
	
	private String message;

	private String authority;

	private String status;

	private String firstName;

	private String middleName;

	private String lastName;

	private String address;

	private String contactNo;

	private String locationCode;

	private String email;

	private String gender;

	private String dateOfBirth;
	
	private String shopNo;
	
	private String panCardNo;
	
	private String aadharNo;
	
	private String bankName;
	
	private String bankAccountNo;
	
	private String branchName;
	
	private String ifscCode;
	
	private boolean isFullyFilled;
	
	private boolean hasError;
	
	
	

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public boolean isFullyFilled() {
		return isFullyFilled;
	}

	public void setFullyFilled(boolean isFullyFilled) {
		this.isFullyFilled = isFullyFilled;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getShopNo() {
		return shopNo;
	}

	public void setShopNo(String shopNo) {
		this.shopNo = shopNo;
	}

	public String getPanCardNo() {
		return panCardNo;
	}

	public void setPanCardNo(String panCardNo) {
		this.panCardNo = panCardNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	@Override
	public String toString() {
		return "RegisterError [valid=" + valid + ", username=" + username + ", password=" + password
				+ ", confirmPassword=" + confirmPassword + ", userType=" + userType + ", message=" + message
				+ ", authority=" + authority + ", status=" + status + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", address=" + address + ", contactNo=" + contactNo
				+ ", locationCode=" + locationCode + ", email=" + email + ", gender=" + gender + ", dateOfBirth="
				+ dateOfBirth + ", shopNo=" + shopNo + ", panCardNo=" + panCardNo + ", aadharNo=" + aadharNo
				+ ", bankName=" + bankName + ", bankAccountNo=" + bankAccountNo + ", branchName=" + branchName
				+ ", ifscCode=" + ifscCode + "]";
	}

	
	
}
