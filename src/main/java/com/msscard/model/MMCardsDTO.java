package com.msscard.model;

public class MMCardsDTO {
	
	private String id;
	private String cardId;
	private String email;
	private String contactNO;
	private String status;
	private String userName;
	private String issueDate;
	private String firstName;
	private String lastName;
	private String authority;
	private String address;
	private String pincode;
	private String proxyNumber;
	private String city;
	private boolean hasPhysicalCard;
	private String mobileToken;
	private String balance;
	private String dob;
	private String comment;
	private String lastModified;
	private String accountType;
	private boolean role;
	private String cardNumber;
	private String Date;
	private String activationCode;
	private String group;
	private boolean changeGroupStatus;
	private String changedGroupName;
	private boolean addUserBool;
	
	
			
	public boolean isAddUserBool() {
		return addUserBool;
	}
	public void setAddUserBool(boolean addUserBool) {
		this.addUserBool = addUserBool;
	}
	public String getChangedGroupName() {
		return changedGroupName;
	}
	public void setChangedGroupName(String changedGroupName) {
		this.changedGroupName = changedGroupName;
	}
	public boolean isChangeGroupStatus() {
		return changeGroupStatus;
	}
	public void setChangeGroupStatus(boolean changeGroupStatus) {
		this.changeGroupStatus = changeGroupStatus;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public boolean isRole() {
		return role;
	}
	public void setRole(boolean role) {
		this.role = role;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNO() {
		return contactNO;
	}
	public void setContactNO(String contactNO) {
		this.contactNO = contactNO;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getProxyNumber() {
		return proxyNumber;
	}
	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public boolean isHasPhysicalCard() {
		return hasPhysicalCard;
	}
	public void setHasPhysicalCard(boolean hasPhysicalCard) {
		this.hasPhysicalCard = hasPhysicalCard;
	}
	public String getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
