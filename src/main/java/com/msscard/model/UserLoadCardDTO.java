package com.msscard.model;

import com.msscard.entity.MUser;

public class UserLoadCardDTO {
	
	private String contactNo;
	
	private double amount;
	
	private String comment;
	
	private boolean transactionType;
	
	private String transactionrefNo;
	
	private MUser user;


	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
	public String getTransactionrefNo() {
		return transactionrefNo;
	}

	public void setTransactionrefNo(String transactionrefNo) {
		this.transactionrefNo = transactionrefNo;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public MUser getUser() {
		return user;
	}

	public void setUser(MUser user) {
		this.user = user;
	}

	public boolean isTransactionType() {
		return transactionType;
	}

	public void setTransactionType(boolean transactionType) {
		this.transactionType = transactionType;
	}
	
	
	

}
