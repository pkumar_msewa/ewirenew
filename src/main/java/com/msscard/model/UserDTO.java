package com.msscard.model;

public class UserDTO {

	private String sessionId;

	private String username;

	private String userId;

	private String firstName;

	private String middleName;

	private String lastName;

	private String address;

	private String contactNo;

	private UserType userType;

	private String authority;

	private Status emailStatus;

	private Status mobileStatus;

	private String email;

	private String image;

	private String mpin;

	private boolean mpinPresent;

	private String dateOfBirth;

	private  String gcmId;

	private String pinCode;

	private String circleName;

	private String districtName;

	private String locality;

	private String encodedImage;
	
	private boolean hasMpin;
	
	private boolean isDeviceLocked;
	
	private String groupName;
	
	private boolean virtualCardBlock;
	
	private boolean physicalCardBlock;	
	
	public boolean isVirtualCardBlock() {
		return virtualCardBlock;
	}

	public void setVirtualCardBlock(boolean virtualCardBlock) {
		this.virtualCardBlock = virtualCardBlock;
	}

	public boolean isPhysicalCardBlock() {
		return physicalCardBlock;
	}

	public void setPhysicalCardBlock(boolean physicalCardBlock) {
		this.physicalCardBlock = physicalCardBlock;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Status getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(Status emailStatus) {
		this.emailStatus = emailStatus;
	}

	public Status getMobileStatus() {
		return mobileStatus;
	}

	public void setMobileStatus(Status mobileStatus) {
		this.mobileStatus = mobileStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public boolean isMpinPresent() {
		return mpinPresent;
	}

	public void setMpinPresent(boolean mpinPresent) {
		this.mpinPresent = mpinPresent;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getEncodedImage() {
		return encodedImage;
	}

	public void setEncodedImage(String encodedImage) {
		this.encodedImage = encodedImage;
	}

	public boolean isHasMpin() {
		return hasMpin;
	}

	public void setHasMpin(boolean hasMpin) {
		this.hasMpin = hasMpin;
	}

	public boolean isDeviceLocked() {
		return isDeviceLocked;
	}

	public void setDeviceLocked(boolean isDeviceLocked) {
		this.isDeviceLocked = isDeviceLocked;
	}
	
	

}
