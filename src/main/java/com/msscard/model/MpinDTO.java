package com.msscard.model;

import com.msscard.app.model.request.SessionDTO;

public class MpinDTO extends SessionDTO{

	private String username;
	private String newMpin;
	private String confirmMpin;
	private String dateOfBirth;
	private String ipAddress;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNewMpin() {
		return newMpin;
	}
	public void setNewMpin(String newMpin) {
		this.newMpin = newMpin;
	}
	public String getConfirmMpin() {
		return confirmMpin;
	}
	public void setConfirmMpin(String confirmMpin) {
		this.confirmMpin = confirmMpin;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	
}
