package com.msscard.model;

import java.util.Date;


public class LoadCardList {
	
	private String contactNo;
	
	private String email;
	
	private double amount;
	
	private Date date;
	
	private String physicalCard;
	
	private String name;
	
	private boolean isKYC;
	
	private String userType;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPhysicalCard() {
		return physicalCard;
	}

	public void setPhysicalCard(String physicalCard) {
		this.physicalCard = physicalCard;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isKYC() {
		return isKYC;
	}

	public void setKYC(boolean isKYC) {
		this.isKYC = isKYC;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	

}
