package com.msscard.model;

import org.springframework.web.multipart.MultipartFile;

public class UpgradeAccountDTO {
	
	private String sessionId;
	
	private String id;
	
	private String idType;
	
	private String idNumber;
	
	private String action;
	
	private String address1;
	
	private String addres2;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String country;
	
	private String username;
	
	private String contactNo;
	
	private MultipartFile image;
	
	private MultipartFile image2;
	
	private String imagePath;
	
	private String imagePath2;
	
	private String email;

	private String name;
	
	private UserType userType;
	
	private String panCardImagePath;
	
	private String aadharCardImagepath;
	
	private MultipartFile panCardImage;
	
	private MultipartFile aadharImage;
	
	private String isCheckkk;
		
	private String address2;
	
	private String dob;
	
	private String rejectionReason;
	
	
	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getIsCheckkk() {
		return isCheckkk;
	}

	public void setIsCheckkk(String isCheckkk) {
		this.isCheckkk = isCheckkk;
	}

	public String getImagePath2() {
		return imagePath2;
	}

	public void setImagePath2(String imagePath2) {
		this.imagePath2 = imagePath2;
	}

	public MultipartFile getImage2() {
		return image2;
	}

	public void setImage2(MultipartFile image2) {
		this.image2 = image2;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPanCardImagePath() {
		return panCardImagePath;
	}

	public void setPanCardImagePath(String panCardImagePath) {
		this.panCardImagePath = panCardImagePath;
	}

	public String getAadharCardImagepath() {
		return aadharCardImagepath;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public void setAadharCardImagepath(String aadharCardImagepath) {
		this.aadharCardImagepath = aadharCardImagepath;
	}

	public MultipartFile getPanCardImage() {
		return panCardImage;
	}

	public void setPanCardImage(MultipartFile panCardImage) {
		this.panCardImage = panCardImage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public MultipartFile getAadharImage() {
		return aadharImage;
	}

	public void setAadharImage(MultipartFile aadharImage) {
		this.aadharImage = aadharImage;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddres2() {
		return addres2;
	}

	public void setAddres2(String addres2) {
		this.addres2 = addres2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	
}
