package com.msscard.model;

import java.util.ArrayList;

public class GetAllAvailableTripsDTO {

	private String code;
	private String status;
	private String message;
	private ArrayList<DroppingPointsDTO> droppingPoints;
	private String maxPrice;
	private String source;
	private String sourceId;
	private String destination;
	private String destinationId;
	private String minPrice;
	private String totalTrips;
	private int maximumPrice;
	private int minimumPrice;
	private ArrayList<BoardingPointsDTO> bdPoints;
	private ArrayList<AvailableTripsDTO> availableTripsDTOs;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<DroppingPointsDTO> getDroppingPoints() {
		return droppingPoints;
	}

	public void setDroppingPoints(ArrayList<DroppingPointsDTO> droppingPoints) {
		this.droppingPoints = droppingPoints;
	}

	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public String getTotalTrips() {
		return totalTrips;
	}

	public void setTotalTrips(String totalTrips) {
		this.totalTrips = totalTrips;
	}

	public int getMaximumPrice() {
		return maximumPrice;
	}

	public void setMaximumPrice(int maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public int getMinimumPrice() {
		return minimumPrice;
	}

	public void setMinimumPrice(int minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public ArrayList<BoardingPointsDTO> getBdPoints() {
		return bdPoints;
	}

	public void setBdPoints(ArrayList<BoardingPointsDTO> bdPoints) {
		this.bdPoints = bdPoints;
	}

	public ArrayList<AvailableTripsDTO> getAvailableTripsDTOs() {
		return availableTripsDTOs;
	}

	public void setAvailableTripsDTOs(ArrayList<AvailableTripsDTO> availableTripsDTOs) {
		this.availableTripsDTOs = availableTripsDTOs;
	}

}
