package com.msscard.model;

public enum ResponseStatus {

	SUCCESS("Success","S00"),
	
	PENDING("Pending","P00"),
	
	FAILURE("Failure","F00"),

	INTERNAL_SERVER_ERROR("Internal Server Error","F02"),

	INVALID_SESSION("Session Invalid","F03"),

	BAD_REQUEST("Bad Request","F04"),
	
	UNAUTHORIZED_USER("Un-Authorized User","F05"),
	
	WALLET_NOT_CREATED("Un-Authorized User, please contact customer care","YOO"),
	
	UNAUTHORIZED_ROLE("Un-Authorized Role","F06"),
	
	INVALID_HASH("Invalid hash","F07"),

	INVALID_VERSION("Invalid Version","F08"),

	INVALID_IP("Invalid IP","F09"),
	
	INVALID_APIKEY("Invalid ApiKey","F10"),

	NEW_DEVICE("New Device","L01"),

	NEW_REGISTRATION("New Registration","R01"),

	NEW_REGISTRATION_OTP("OTP Validation","R02"),

	CHANGE_USERNAME("Change Username/MobileNo","U01"),
	
	NEW_ACTIVATION("New Activation","S01"),
	
	RENEWAL_ACTIVE("Renewal Activation","S02"),
	
	INVALID_MPIN("Wrong Mpin","F10"),
	
	EXCEPTION_OCCURED("Services are temporarily down.Please try after sometime","E00"),
	
	EMAIL_EXIST("Please use a different email","E01"),

	OAUTH_FAILURE("Failed to authenticate.Please use another email id","E02"),
	
	WALLET_EXISTS("Wallet already exist","W00"),
	
	PHYSICAL_CARD_EXIST("PhysicalCard already exist","W01"),
	
	CARD_STATUS_ACTIVE("Card status ACTIVE","C00"),
	
	CARD_STATUS_INACTIVE("Card status Inactive","C01"),
	
	PHYSICAL_CARD_REQUEST_EXIST("Request already received for physical card.If you are facing issues please contact Admin","C02"),
	
	RECHARGE_FAILURE_REFUNDED("Transaction Failed.Refund process initiated","T00"),
	
	INSUFFICIENT_FUNDS("Transaction Failed due to insufficient funds","T01");
	
	
	
	
	


	private ResponseStatus() {

	}
	
	private String key;

	private String value;

	private ResponseStatus(String key,String value) {
		this.key=key;
		this.value = value;
	}
	
	public String getKey(){
		return key;
	}
	public String getValue() {
		return value;
	}

	public static ResponseStatus getEnumByValue(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (ResponseStatus v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
	
	public static ResponseStatus getEnumByKey(String key) {
		if (key == null)
			throw new IllegalArgumentException();
		for (ResponseStatus v : values())
			if (key.equalsIgnoreCase(v.getKey()))
				return v;
		throw new IllegalArgumentException();
	}

}
