package com.msscard.model;

public class BusSaveSeatDTO {
	
	private boolean isTransactionCreated;

	private String transactionid;

	private String emtTransactionScreenId;

	private String error;

	private String validFor;

	private String mpQTxnId;

	private String vpQTxnId;

	private String priceRecheckAmt;

	private String seatHoldId;

	private String fareBreak;

	private boolean priceRecheck;

	public boolean isTransactionCreated() {
		return isTransactionCreated;
	}

	public void setTransactionCreated(boolean isTransactionCreated) {
		this.isTransactionCreated = isTransactionCreated;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}

	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getValidFor() {
		return validFor;
	}

	public void setValidFor(String validFor) {
		this.validFor = validFor;
	}

	public String getMpQTxnId() {
		return mpQTxnId;
	}

	public void setMpQTxnId(String mpQTxnId) {
		this.mpQTxnId = mpQTxnId;
	}

	public String getVpQTxnId() {
		return vpQTxnId;
	}

	public void setVpQTxnId(String vpQTxnId) {
		this.vpQTxnId = vpQTxnId;
	}

	public String getPriceRecheckAmt() {
		return priceRecheckAmt;
	}

	public void setPriceRecheckAmt(String priceRecheckAmt) {
		this.priceRecheckAmt = priceRecheckAmt;
	}

	public String getSeatHoldId() {
		return seatHoldId;
	}

	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}

	public String getFareBreak() {
		return fareBreak;
	}

	public void setFareBreak(String fareBreak) {
		this.fareBreak = fareBreak;
	}

	public boolean isPriceRecheck() {
		return priceRecheck;
	}

	public void setPriceRecheck(boolean priceRecheck) {
		this.priceRecheck = priceRecheck;
	}

}
