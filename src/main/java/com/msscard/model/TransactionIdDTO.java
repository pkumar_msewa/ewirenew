package com.msscard.model;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import com.msscard.app.model.request.CancelPolicyListDTO;
import com.msscard.app.model.request.GSTDetailsDTO;
import com.msscard.app.model.request.TravellerDetailsDTO;
import com.razorpay.constants.MdexConstants;

public class TransactionIdDTO extends TravellerDetailsDTO{
	
	private String mobileNo;
	private String emailId;
	private String idProofId;
	private String idProofNo;
	private String sessionId;
	private String address;
	private String engineId;
	private String busid;
	private String busType;
	private String boardId;
	private String boardLocation;
	private String boardName;
	private String arrTime;
	private String depTime;
	private String travelName;
	private String source;
	private String destination;
	private String sourceid;
	private String destinationId;
	private String journeyDate;
	private String boardpoint;
	private String boardprime;
	private String boardTime;
	private String boardlandmark;
	private String boardContactNo;
	private String dropId;
	private String dropName;
	private String droplocatoin;
	private String dropprime;
	private String dropTime;
	private String routeId;
	private String seatDetail;
	private String couponCode;
	private double discount;
	private List<TravellerDetailsDTO> travellers;
	private GSTDetailsDTO gstDetails;
	private List<CancelPolicyListDTO> cancelPolicyList; 
	private String wLCode;
	private String ipAddress;
	private String version;
	private double commission;
	private double markup;
	private String agentCode;	
	private double totalFare;
	private String tripId;
	private String data;
	
	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		
		payload.put("clientIp", "52.15.165.153");
		payload.put("cllientKey", MdexConstants.MDEX_KEY);
		payload.put("clientToken", MdexConstants.MDEX_TOKEN);
		payload.put("clientApiName", "Ewire");
		payload.put("mobileNo", (getMobileNo() == null) ? "" :getMobileNo());
		payload.put("emailId", (getEmail()== null) ? "" :getEmail());
		payload.put("idProofId",(getIdProofId()== null) ? "" :getIdProofId());
		payload.put("idProofNo",(getIdProofNo()== null) ? "" :getIdProofNo());
		payload.put("sessionId",(getSessionId()== null) ? "" :getSessionId());
		payload.put("address",(getAddress()== null) ? "" :getAddress());
		payload.put("engineId",(getEngineId()== null) ? "" :getEngineId());
		payload.put("busid",(getBusid()== null) ? "" :getBusid());
		payload.put("busType",(getBusType()== null) ? "" :getBusType());
		payload.put("boardId",(getBoardId()== null) ? "" :getBoardId());
		payload.put("boardLocation",(getBoardLocation()== null) ? "" :getBoardLocation());
		payload.put("boardName",(getBoardName()== null) ? "" :getBoardName());
		payload.put("arrTime",(getArrTime()== null) ? "" :getArrTime());
		payload.put("depTime",(getDepTime()== null) ? "" :getDepTime());
		payload.put("travelName",(getTravelName()== null) ? "" :getTravelName());
		payload.put("source",(getSource()== null) ? "" :getSource());
		payload.put("destination",(getDestination()== null) ? "" :getDestination());
		payload.put("sourceid",(getSourceid()== null) ? "" :getSourceid());
		payload.put("destinationId",(getDestinationId()== null) ? "" :getDestinationId());
		payload.put("journeyDate",(getJourneyDate()== null) ? "" :getJourneyDate());
		payload.put("boardpoint",(getBoardpoint()== null) ? "" :getBoardpoint());
		payload.put("boardprime",(getBoardprime()== null) ? "" :getBoardprime());
		payload.put("boardTime",(getBoardTime()== null) ? "" :getBoardTime());
		payload.put("boardlandmark",(getBoardlandmark()== null) ? "" :getBoardlandmark());
		payload.put("boardContactNo",(getBoardContactNo()== null) ? "" :getBoardContactNo());
		payload.put("dropId",(getDropId()== null) ? "" :getDropId());
		payload.put("dropName",(getDropName()== null) ? "" :getDropName());
		payload.put("droplocatoin",(getDroplocatoin()== null) ? "" :getDroplocatoin());
		payload.put("dropprime",(getDropprime()== null) ? "" :getDropprime());
		payload.put("dropTime",(getDropTime()== null) ? "" :getDropTime());
		payload.put("routeId",(getRouteId()== null) ? "" :getRouteId());
		payload.put("seatDetail",(getSeatDetail()== null) ? "" :getSeatDetail());
		payload.put("couponCode",(getCouponCode()== null) ? "" :getCouponCode());
		payload.put("discount",getDiscount());
		payload.put("commission",getCommission());
		payload.put("markup",getMarkup());
		payload.put("wLCode",(getwLCode()== null) ? "" :getwLCode());
		payload.put("ipAddress",(getIpAddress()== null) ? "" :getIpAddress());
		payload.put("version",(getVersion()== null) ? "" :getVersion());
		payload.put("agentCode",(getAgentCode()== null) ? "" :getAgentCode());
		
		
		JSONArray travellersDetails=new JSONArray();
				
		for (int j = 0; j < travellers.size(); j++) {
			
			JSONObject travellersDetail=new JSONObject();
			travellersDetail.put("title",(travellers.get(j).getTitle()== null) ? "" :travellers.get(j).getTitle());
			travellersDetail.put("fName",(travellers.get(j).getfName()== null) ? "" :travellers.get(j).getfName());
			travellersDetail.put("mName",(travellers.get(j).getmName()== null) ? "" :travellers.get(j).getmName());
			travellersDetail.put("lName",(travellers.get(j).getlName()== null) ? "" :travellers.get(j).getlName());
			travellersDetail.put("age",(travellers.get(j).getAge()== null) ? "" :travellers.get(j).getAge());
			travellersDetail.put("gender",(travellers.get(j).getGender()== null) ? "" :travellers.get(j).getGender());
			travellersDetail.put("seatNo",(travellers.get(j).getSeatNo()== null) ? "" :travellers.get(j).getSeatNo());
			travellersDetail.put("seatType",(travellers.get(j).getSeatType()== null) ? "" :travellers.get(j).getSeatType());
			travellersDetail.put("fare",(travellers.get(j).getFare()== null) ? "" :travellers.get(j).getFare());
			travellersDetail.put("seatId",(travellers.get(j).getSeatId()== null) ? "" :travellers.get(j).getSeatId());
			
			travellersDetails.put(travellersDetail);
		}
		
		
		JSONObject gstDetail= new JSONObject();
		gstDetail.put("phone", (gstDetails.getPhone()== null) ? "" :gstDetails.getPhone());
		gstDetail.put("email", (gstDetails.getEmail()== null) ? "" :gstDetails.getEmail());
		gstDetail.put("companyName", (gstDetails.getCompanyName()== null) ? "" :gstDetails.getCompanyName());
		gstDetail.put("address", (gstDetails.getAddress()== null) ? "" :gstDetails.getAddress());
		gstDetail.put("gstNumber", (gstDetails.getGstNumber()== null) ? "" :gstDetails.getGstNumber());
		
		
		JSONArray cancelPolicyLists=new JSONArray();
		
		for (int j = 0; j < cancelPolicyList.size(); j++) {
		JSONObject cancelPolicy=new JSONObject();
		cancelPolicy.put("timeFrom", cancelPolicyList.get(j).getTimeFrom());
		cancelPolicy.put("timeTo", cancelPolicyList.get(j).getTimeTo());
		cancelPolicy.put("percentageCharge", cancelPolicyList.get(j).getPercentageCharge());
		cancelPolicy.put("flatCharge", cancelPolicyList.get(j).getFlatCharge());
		cancelPolicy.put("flat", cancelPolicyList.get(j).isFlat());
		cancelPolicy.put("seatno", (cancelPolicyList.get(j).getSeatno()== null) ? "" :cancelPolicyList.get(j).getSeatno());
		cancelPolicyLists.put(cancelPolicy);
		}
		
		payload.put("travellers", travellersDetails);
		payload.put("gstDetails",gstDetail);
		payload.put("cancelPolicyList", cancelPolicyLists);
		
		System.err.println("PayLoad is:: "+payload);
		return payload;
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getIdProofId() {
		return idProofId;
	}
	public void setIdProofId(String idProofId) {
		this.idProofId = idProofId;
	}
	public String getIdProofNo() {
		return idProofNo;
	}
	public void setIdProofNo(String idProofNo) {
		this.idProofNo = idProofNo;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEngineId() {
		return engineId;
	}
	public void setEngineId(String engineId) {
		this.engineId = engineId;
	}
	public String getBusid() {
		return busid;
	}
	public void setBusid(String busid) {
		this.busid = busid;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getBoardLocation() {
		return boardLocation;
	}
	public void setBoardLocation(String boardLocation) {
		this.boardLocation = boardLocation;
	}
	public String getBoardName() {
		return boardName;
	}
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getDepTime() {
		return depTime;
	}
	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}
	public String getTravelName() {
		return travelName;
	}
	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getSourceid() {
		return sourceid;
	}
	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}
	public String getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getBoardpoint() {
		return boardpoint;
	}
	public void setBoardpoint(String boardpoint) {
		this.boardpoint = boardpoint;
	}
	public String getBoardprime() {
		return boardprime;
	}
	public void setBoardprime(String boardprime) {
		this.boardprime = boardprime;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public String getBoardlandmark() {
		return boardlandmark;
	}
	public void setBoardlandmark(String boardlandmark) {
		this.boardlandmark = boardlandmark;
	}
	public String getBoardContactNo() {
		return boardContactNo;
	}
	public void setBoardContactNo(String boardContactNo) {
		this.boardContactNo = boardContactNo;
	}
	public String getDropId() {
		return dropId;
	}
	public void setDropId(String dropId) {
		this.dropId = dropId;
	}
	public String getDropName() {
		return dropName;
	}
	public void setDropName(String dropName) {
		this.dropName = dropName;
	}
	public String getDroplocatoin() {
		return droplocatoin;
	}
	public void setDroplocatoin(String droplocatoin) {
		this.droplocatoin = droplocatoin;
	}
	public String getDropprime() {
		return dropprime;
	}
	public void setDropprime(String dropprime) {
		this.dropprime = dropprime;
	}
	public String getDropTime() {
		return dropTime;
	}
	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	public String getSeatDetail() {
		return seatDetail;
	}
	public void setSeatDetail(String seatDetail) {
		this.seatDetail = seatDetail;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public List<TravellerDetailsDTO> getTravellers() {
		return travellers;
	}
	public void setTravellers(List<TravellerDetailsDTO> travellers) {
		this.travellers = travellers;
	}
	public GSTDetailsDTO getGstDetails() {
		return gstDetails;
	}
	public void setGstDetails(GSTDetailsDTO gstDetails) {
		this.gstDetails = gstDetails;
	}
	public List<CancelPolicyListDTO> getCancelPolicyList() {
		return cancelPolicyList;
	}
	public void setCancelPolicyList(List<CancelPolicyListDTO> cancelPolicyList) {
		this.cancelPolicyList = cancelPolicyList;
	}
	public String getwLCode() {
		return wLCode;
	}
	public void setwLCode(String wLCode) {
		this.wLCode = wLCode;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	public double getMarkup() {
		return markup;
	}
	public void setMarkup(double markup) {
		this.markup = markup;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
