package com.msscard.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SeatDTO {
    
    @JsonProperty("available")
    private boolean available;
    
    @JsonProperty("baseFare")
    private double baseFare;
    
    @JsonProperty("column")
    private String column;
    
    @JsonProperty("ladiesSeat")
    private String ladiesSeat;
    
    @JsonProperty("length")
    private String length;
    
    @JsonProperty("seatAvail")
    private int seatAvail;
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("row")
    private String row;
    
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("seatStyle")
    private String seatStyle;
    
    @JsonProperty("width")
    private String width;
    
    @JsonProperty("zIndex")
    private String zIndex;
    
    @JsonProperty("fare")
    private double fare;
    
    @JsonProperty("seatType")
    private String seatType;
    
    @JsonProperty("imageUrl")
    private String imageUrl;
    
    @JsonProperty("gender")
    private String gender;
    
    @JsonProperty("upperShow")
    private boolean upperShow;
    
    @JsonProperty("lowerShow")
    private boolean lowerShow;
    @JsonProperty("columnNo")
    private int columnNo;
    
    @JsonProperty("rowNo")
    private int rowNo;
    
    @JsonProperty("ac")
    private boolean ac;
    
    @JsonProperty("sleeper")
    private boolean sleeper;
    
    public boolean isAvailable() {
        return available;
    }
    public void setAvailable(boolean available) {
        this.available = available;
    }
    public double getBaseFare() {
        return baseFare;
    }
    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }
    public String getColumn() {
        return column;
    }
    public void setColumn(String column) {
        this.column = column;
    }
    public String getLadiesSeat() {
        return ladiesSeat;
    }
    public void setLadiesSeat(String ladiesSeat) {
        this.ladiesSeat = ladiesSeat;
    }
    public String getLength() {
        return length;
    }
    public void setLength(String length) {
        this.length = length;
    }
    public int getSeatAvail() {
        return seatAvail;
    }
    public void setSeatAvail(int seatAvail) {
        this.seatAvail = seatAvail;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRow() {
        return row;
    }
    public void setRow(String row) {
        this.row = row;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSeatStyle() {
        return seatStyle;
    }
    public void setSeatStyle(String seatStyle) {
        this.seatStyle = seatStyle;
    }
    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getzIndex() {
        return zIndex;
    }
    public void setzIndex(String zIndex) {
        this.zIndex = zIndex;
    }
    public double getFare() {
        return fare;
    }
    public void setFare(double fare) {
        this.fare = fare;
    }
    public String getSeatType() {
        return seatType;
    }
    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public boolean isUpperShow() {
        return upperShow;
    }
    public void setUpperShow(boolean upperShow) {
        this.upperShow = upperShow;
    }
    public boolean isLowerShow() {
        return lowerShow;
    }
    public void setLowerShow(boolean lowerShow) {
        this.lowerShow = lowerShow;
    }
    public int getColumnNo() {
        return columnNo;
    }
    public void setColumnNo(int columnNo) {
        this.columnNo = columnNo;
    }
    public int getRowNo() {
        return rowNo;
    }
    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }
    public boolean isAc() {
        return ac;
    }
    public void setAc(boolean ac) {
        this.ac = ac;
    }
    public boolean isSleeper() {
        return sleeper;
    }
    public void setSleeper(boolean sleeper) {
        this.sleeper = sleeper;
    }
}

