package com.msscard.model;

import java.util.List;

public class AddPartnerDTO {

	private String partnerName;
	private String partnerEmail;
	private String partnerMobile;
	private List<String> services;
	private double loadCardMaxLimit;
	
	public double getLoadCardMaxLimit() {
		return loadCardMaxLimit;
	}
	public void setLoadCardMaxLimit(double loadCardMaxLimit) {
		this.loadCardMaxLimit = loadCardMaxLimit;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerEmail() {
		return partnerEmail;
	}
	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}
	public String getPartnerMobile() {
		return partnerMobile;
	}
	public void setPartnerMobile(String partnerMobile) {
		this.partnerMobile = partnerMobile;
	}
	public List<String> getServices() {
		return services;
	}
	public void setServices(List<String> services) {
		this.services = services;
	}
	
	
}
