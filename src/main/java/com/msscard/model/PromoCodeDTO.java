package com.msscard.model;

public class PromoCodeDTO {

	private String promoCode;
	private String sessionId;
	private String startDate;
	private String endDate;
	private String amount;
	private String percentageValue;
	private String minTrxAmount;
	private String maxCashBack;
	private boolean isPercentage;
	private Status Status;
	private String cardStatus;
	private String serviceCode;

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPercentageValue() {
		return percentageValue;
	}

	public void setPercentageValue(String percentageValue) {
		this.percentageValue = percentageValue;
	}

	public String getMinTrxAmount() {
		return minTrxAmount;
	}

	public void setMinTrxAmount(String minTrxAmount) {
		this.minTrxAmount = minTrxAmount;
	}

	public String getMaxCashBack() {
		return maxCashBack;
	}

	public void setMaxCashBack(String maxCashBack) {
		this.maxCashBack = maxCashBack;
	}

	public boolean isPercentage() {
		return isPercentage;
	}

	public void setPercentage(boolean isPercentage) {
		this.isPercentage = isPercentage;
	}

	public Status getStatus() {
		return Status;
	}

	public void setStatus(Status status) {
		Status = status;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
