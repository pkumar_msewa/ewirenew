package com.msscard.controller.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.imps.entity.AddBeneficiary;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.AadharAuthRequest;
import com.msscard.app.model.request.BeneficiaryDTO;
import com.msscard.app.model.request.LoadMoneyRequest;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.app.model.request.UserKycRequest;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.ErrorResponse;
import com.msscard.repositories.AddBeneficiaryRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.CommonValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.razorpay.constants.RazorPayConstants;

@Controller
@RequestMapping("/{role}")
public class UserController {

	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final CommonValidation validation;
	private final IMatchMoveApi matchMoveApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final MMCardRepository mmCardRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MKycRepository mKycRepository;
	private final AddBeneficiaryRepository addBeneficiaryRepository;
	private final MUserDetailRepository userDetailRepository;
	private final MPQAccountDetailRepository mPQAccountDetailRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRepository;
	
	public UserController(IUserApi userApi, UserSessionRepository userSessionRepository, CommonValidation validation,
			IMatchMoveApi matchMoveApi, PersistingSessionRegistry persistingSessionRegistry,
			MMCardRepository mmCardRepository, PhysicalCardDetailRepository physicalCardDetailRepository,
			MKycRepository mKycRepository, AddBeneficiaryRepository addBeneficiaryRepository,
			MUserDetailRepository userDetailRepository, MPQAccountDetailRepository mPQAccountDetailRepository,
			MPQAccountTypeRepository mPQAccountTypeRepository) {
		super();
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.validation=validation;
		this.matchMoveApi=matchMoveApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.mmCardRepository=mmCardRepository;
		this.physicalCardDetailRepository=physicalCardDetailRepository;
		this.mKycRepository=mKycRepository;
		this.addBeneficiaryRepository=addBeneficiaryRepository;
		this.userDetailRepository=userDetailRepository;
		this.mPQAccountDetailRepository=mPQAccountDetailRepository;
		this.mPQAccountTypeRepository=mPQAccountTypeRepository;
	}
	
	
	@RequestMapping(value="/LoadMoney",method=RequestMethod.GET)
	public String getLoadMoney(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					return "User/LoadMoney";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	
	@RequestMapping(value="/Help",method=RequestMethod.GET)
	public String getHelp(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					return "User/Help";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	
	@RequestMapping(value="/Pay",method=RequestMethod.POST)
	public String initiateTransaction(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session,@ModelAttribute LoadMoneyRequest loadMoneyRequest){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("name", user.getUserDetail().getFirstName());
					model.addAttribute("contactNo", user.getUsername());
					model.addAttribute("email", user.getUserDetail().getEmail());
					model.addAttribute("key", RazorPayConstants.KEY_ID);
					model.addAttribute("logo", RazorPayConstants.LOGO_PATH);
					model.addAttribute("amount", loadMoneyRequest.getAmount());
					return "User/Pay";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	@RequestMapping(value="/LoadMoney/Response",method=RequestMethod.POST)
	public String redirectResponse(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session,@ModelAttribute LoadMoneyRequest loadMoneyRequest){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("name", user.getUserDetail().getFirstName());
					model.addAttribute("contactNo", user.getUsername());
					model.addAttribute("email", user.getUserDetail().getEmail());
					model.addAttribute("key", RazorPayConstants.KEY_ID);
					model.addAttribute("logo", RazorPayConstants.LOGO_PATH);
					model.addAttribute("amount", loadMoneyRequest.getAmount());
					return "User/Pay";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	
	@RequestMapping(value="/MyProfile",method=RequestMethod.GET)
	public String getProfile(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					PhysicalCardDetails card=physicalCardDetailRepository.findByUser(user);	
					if(card!=null){
						model.addAttribute("cardstats", card.getStatus()+"");
					}
					MKycDetail detail=mKycRepository.findByUser(user);
					if(detail!=null){
						model.addAttribute("mDetail", detail.getAccountType().getCode());
					}
					model.addAttribute("user", user);
					model.addAttribute("KycMsg", session.getAttribute("msg"));
					session.setAttribute("msg", "");
					return "User/MyProfile";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	@RequestMapping(value="/Recharge",method=RequestMethod.GET)
	public String geteRecharg(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					return "User/Recharge";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	@RequestMapping(value="/Billpayments",method=RequestMethod.GET)
	public String getBillpayments(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					return "User/BillPayments";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	@RequestMapping(value="/UpgradeAccount",method=RequestMethod.GET)
	public String UpgradeAccount(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					return "User/UpgradeAccount";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	
	@RequestMapping(value="/PhysicalCardRequest",method=RequestMethod.GET)
	public String physicalCardRequest(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					try{
					PhysicalCardDetails card=physicalCardDetailRepository.findByUser(user);
					 WalletResponse walletResponse=new WalletResponse();
					if(card!=null){
						MMCards physcard=mmCardRepository.getPhysicalCardByUser(user);
						if(physcard!=null){
					MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
                   	  cardRequest.setEmail(user.getUserDetail().getEmail());
                   	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
                   	  cardRequest.setCardId(physcard.getCardId());
                   	  cardRequest.setUsername(user.getUsername());
                   	   walletResponse= matchMoveApi.inquireCard(cardRequest);
                   	   walletResponse.setWalletNumber(walletResponse.getWalletNumber().toUpperCase().replaceAll("....", "$0  "));
                   	  model.addAttribute("cardResponse", walletResponse);
						}else{
							  walletResponse.setExpiryDate("xx/xx");
							  walletResponse.setCvv("xxx");
							  walletResponse.setHolderName("HOLDER NAME");
							  walletResponse.setWalletNumber("xxxx xxxx xxxx xxxx");
		                   	  model.addAttribute("cardResponse", walletResponse);

						}
						model.addAttribute("cardstats", card.getStatus()+"");
					}
					model.addAttribute("user", user);
					}catch(Exception e){
						e.printStackTrace();
					}
					return "User/PhysicalCardRequest";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	@RequestMapping(value="/Upgrade/Account",method=RequestMethod.POST)
	public String upgradeAccount(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session,@ModelAttribute UpgradeAccountDTO dto){
		ResponseDTO respons=new ResponseDTO();
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					MKycDetail detail = mKycRepository.findByUser(user);
					if(detail!=null){
						respons.setCode(ResponseStatus.SUCCESS.getValue());
						respons.setMessage("Your request has been saved already.Please wait to get it approved");
						model.addAttribute("user", user);
						session.setAttribute("msg", "Your request has been saved already.Please wait to get it approved");
						return "redirect:/User/MyProfile";
					}else{
					ErrorResponse error=validation.checkKycRequest(dto);
					if(error.isValid()){
//					if(true){
					try {
						dto.setUserType(UserType.User);
						dto.setEmail(user.getUserDetail().getEmail());
						dto.setContactNo(user.getUserDetail().getContactNo());
						dto.setName(user.getUserDetail().getFirstName());
						if(dto.getImage()!=null) {
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage(),dto.getContactNo(),dto.getIdType());
						dto.setImagePath(aadharCardImgPath);
						}
						if(dto.getImage2()!=null){
							String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage2(),dto.getContactNo(),dto.getIdType());
							dto.setImagePath2(aadharCardImgPath);

						}

						userApi.upgradeAccount(dto);
						respons.setCode(ResponseStatus.SUCCESS.getValue());
						respons.setMessage("Your request has been saved.Please wait to get it approved");
						model.addAttribute("user", user);
						model.put("msg", "Your request has been saved.Please wait to get it approved");
						session.setAttribute("msg", "Your request has been saved.Please wait to get it approved");
						return "redirect:/User/MyProfile";
					} catch (Exception e) {
						e.printStackTrace();
						respons.setMessage("Service Unavailable");
						respons.setCode(ResponseStatus.FAILURE.getValue());
						model.addAttribute("user", user);
						session.setAttribute("msg", "Service Unavailable");
						return "redirect:/User/MyProfile";
					}
				}
				else{
					respons.setCode(ResponseStatus.FAILURE.getValue());
					model.addAttribute("user", user);
					session.setAttribute("msg", error.getMessage());
					return "redirect:/User/MyProfile";
					}
				}
			}else{
				session.invalidate();
				return "Home";
			}
		}else{
			session.invalidate();
			return "Home";
		}
	}
		else{
			session.invalidate();
			return "Home";
		}
	}
	
	@RequestMapping(value="/Upgrade/KycAccount",method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> upgradeAccountApp(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session,@ModelAttribute UpgradeAccountDTO dto){
		ResponseDTO respons=new ResponseDTO();
		String sessionId=dto.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null) {
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				dto.setContactNo(user.getUsername());
				if (user.getAuthority().contains(Authorities.USER)) {
					MKycDetail detail = mKycRepository.findByUser(user);
					
					if(detail!=null){
						if(dto.getImage2()!=null){
							if(dto.getIdType().equalsIgnoreCase("DRIVING LICENSE")){
								dto.setIdType("drivers_id");
							}
														
							String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage2(),dto.getContactNo(),dto.getIdType());
							String aadharCardImgPath2=CommonUtil.uploadImage(dto.getImage(),dto.getContactNo(),dto.getIdType());
							dto.setImagePath2(aadharCardImgPath);
							dto.setImagePath(aadharCardImgPath2);
							dto.setUserType(UserType.User);
							
						userApi.upgradeAccount(dto);
						respons.setCode(ResponseStatus.SUCCESS.getValue());
						respons.setMessage("Your request has been saved.Please wait to get it approved");
						} else {
							respons.setCode(ResponseStatus.FAILURE.getValue());
							respons.setMessage("");
						}
						return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
					} else{
					ErrorResponse error=validation.checkKycRequest(dto);
					if(error.isValid()) {
					try {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						dto.setUserType(UserType.User);
						dto.setEmail(user.getUserDetail().getEmail());
						dto.setContactNo(user.getUserDetail().getContactNo());
						dto.setName(user.getUserDetail().getFirstName());
						
						if(dto.getImage()!=null) {
							if(dto.getIdType().equalsIgnoreCase("DRIVING LICENSE")){
								dto.setIdType("drivers_id");
							}
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage(),dto.getContactNo(),dto.getIdType());
						dto.setImagePath(aadharCardImgPath);
						}
						if(dto.getImage2()!=null){
							if(dto.getIdType().equalsIgnoreCase("DRIVING LICENSE")) {
								dto.setIdType("drivers_id");
							}
							String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage2(),dto.getContactNo(),dto.getIdType());
							dto.setImagePath2(aadharCardImgPath);

						}
						
						userApi.upgradeAccount(dto);
						UserKycRequest requ=new UserKycRequest();
						requ.setAddress1(dto.getAddress1());
						requ.setAddress2(dto.getAddres2());
						requ.setCity(dto.getCity());
						requ.setState(dto.getState());
						requ.setCountry(dto.getCountry());
						requ.setPinCode(dto.getPincode());
						requ.setUser(user);
						requ.setId_type(dto.getIdType());
						requ.setId_number(dto.getIdNumber());
						requ.setFile(dto.getImage()); 
						UserKycResponse resp=matchMoveApi.kycUserMM(requ);  
						if("S00".equalsIgnoreCase(resp.getCode())){
							 resp=matchMoveApi.setIdDetails(requ);
							 if("S00".equalsIgnoreCase(resp.getCode())){
								 resp=matchMoveApi.setIdDetails(requ);
								 
								 if("S00".equalsIgnoreCase(resp.getCode())){
									 resp=matchMoveApi.setImagesForKyc(requ);
								 }
							 }
						
						respons.setCode(ResponseStatus.SUCCESS.getValue());
						respons.setMessage("Your request has been saved,Please wait to get it approved");
						return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
						}
							respons.setCode(ResponseStatus.SUCCESS.getValue());
							respons.setMessage("Your request has been saved.Please wait to get it approved");
							return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
						
					} catch (Exception e) {
						e.printStackTrace();
						respons.setMessage("Service Unavailable");
						respons.setCode(ResponseStatus.FAILURE.getValue());
						return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
					}
				}
				else{
					respons.setCode(ResponseStatus.FAILURE.getValue());
					respons.setMessage(error.getMessage());}
				}
			}
				else{
					respons.setMessage("Unauthorized User");
					respons.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			}
			else{
				respons.setMessage("Unauthorized User");
				respons.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			}
		}else{
			respons.setMessage("Invalid session,please login and try again.");
			respons.setCode(ResponseStatus.INVALID_SESSION.getValue());
			return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
		}
		   return new ResponseEntity<ResponseDTO>(respons,HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/VirtualCardRequest",method=RequestMethod.GET)
	public String virtualCardRequest(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					try {
						MMCards cards = mmCardRepository.getVirtualCardsByCardUser(user);
						if (cards == null) {
							MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
							cardRequest.setEmail(user.getUserDetail().getEmail());
							cardRequest.setPassword(SecurityUtil.md5(user.getUserDetail().getContactNo()));
							cardRequest.setUsername(user.getUsername());
							ResponseDTO responseDTO = matchMoveApi.fetchMMUser(cardRequest);
							WalletResponse walletResponse = new WalletResponse();
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(responseDTO.getCode())) {
								walletResponse = matchMoveApi.assignVirtualCard(user);
								cards = mmCardRepository.getVirtualCardsByCardUser(user);
								model.put("virtMess", walletResponse.getMessage());
							} else {
								ResponseDTO resp = matchMoveApi.createUserOnMM(user);
								if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getCode())) {
									walletResponse = matchMoveApi.assignVirtualCard(user);
									cards = mmCardRepository.getVirtualCardsByCardUser(user);
									model.put("virtMess", walletResponse.getMessage());
								} else {
									model.put("virtMess", walletResponse.getMessage());
								}
							}
						} else {
							model.put("virtMess",
									"You are already assigned with a card.Please Contact Customer care for queries");
						}
						ResponseDTO result = new ResponseDTO();
						MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
						cardRequest.setEmail(user.getUserDetail().getEmail());
						cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
						cardRequest.setCardId(cards.getCardId());
						cardRequest.setUsername(user.getUsername());
						WalletResponse walletResponse = matchMoveApi.inquireCard(cardRequest);
						result.setHasVirtualCard(true);
						result.setCardDetails(walletResponse);
						String cardNo = walletResponse.getWalletNumber();
						model.addAttribute("cardNo", cardNo.replaceAll("....", "$0   "));
						model.addAttribute("resultSet", result);
						model.addAttribute("user", user);
					} catch (Exception e) {
 						e.printStackTrace();
 					}
					return "User/DashBoard";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}
	
	
	@RequestMapping(value="/Expenses",method=RequestMethod.GET)
	public String getTransaction(@PathVariable(value="role") String role,
			@ModelAttribute SessionDTO session, HttpServletRequest request,HttpServletResponse response,HttpSession httpSession,ModelMap map){
		UserKycResponse walletResponse=new UserKycResponse(); 
		MUser user1=null;
			if (role.equalsIgnoreCase("User")) {
				String sessionId = httpSession.getId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					user1=userSession.getUser();
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						try {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						walletResponse=matchMoveApi.getTransactions(userSession.getUser());
						if(walletResponse.getCardTransactions()!=null){
						 JSONObject	obj = new JSONObject(walletResponse.getCardTransactions().toString());
						 JSONArray aa= obj.getJSONArray("transactions");
						 System.err.println(aa);
						List<ResponseDTO> resul=new ArrayList<>();
						for(int i=0;i<aa.length();i++){
							ResponseDTO resp= new ResponseDTO();
							resp.setAmount(aa.getJSONObject(i).getString("amount"));
							resp.setDate(aa.getJSONObject(i).getString("date"));
							if(aa.getJSONObject(i).getJSONObject("details")!=null){
								JSONObject ojj=aa.getJSONObject(i).getJSONObject("details");
								if(ojj.has("merchantname")){
									resp.setDescription(aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
								}else{
									resp.setDescription(aa.getJSONObject(i).getString("description"));
								}
							}
							resp.setStatus(aa.getJSONObject(i).getString("status"));
							resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
							resul.add(resp);
						}
						map.put("Vtransactions", resul);
						}
						if(walletResponse.getCardTransactions()!=null){
							 JSONObject	obj = new JSONObject(walletResponse.getCardTransactions().toString());
							 JSONArray aa= obj.getJSONArray("transactions");
							List<ResponseDTO> resul=new ArrayList<>();
							for(int i=0;i<aa.length();i++){
								ResponseDTO resp= new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if(aa.getJSONObject(i).getJSONObject("details")!=null){
									JSONObject ojj=aa.getJSONObject(i).getJSONObject("details");
									if(ojj.has("merchantname")){
										resp.setDescription(aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									}else{
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
							map.put("Ptransactions", resul);
							}
						
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						httpSession.invalidate();
						return "Home";
					}
				}else{
					httpSession.invalidate();
					return "Home";
				}
			}else{
				httpSession.invalidate();
				return "Home";
			}
			map.put("user", user1);
			return "User/MyReceipts";
		
	}
	
	@RequestMapping(value="/ImpsService",method=RequestMethod.GET)
	public String getImps(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session){
		
		String sessionId=session.getId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					model.addAttribute("user", user);
					List<AddBeneficiary> listBeneficiary=addBeneficiaryRepository.findByUser(user);
					List<BeneficiaryDTO> blist=new ArrayList<>();
					if(listBeneficiary.size()>0){
						for (AddBeneficiary beneficiary : listBeneficiary) {
							BeneficiaryDTO be = new BeneficiaryDTO();
							be.setAccountNo(beneficiary.getAccountNo());
							be.setIfscNo(beneficiary.getIfscNo());
							be.setName(beneficiary.getName());
							be.setId(beneficiary.getId()+"");
							be.setBankname(beneficiary.getBankName());
							blist.add(be);
						}
					}
					model.addAttribute("beneficiary", blist);
					return "User/Imps";
				}
			}
		}else{
			session.invalidate();
			return "Home";
		}
		return "redirect:/User/Login/Process";
	}

	@RequestMapping(value="/AadharAuth",method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO> authAadhar(@PathVariable("role") String role,HttpServletRequest request,HttpServletResponse response,ModelMap model,HttpSession session,@RequestBody AadharAuthRequest aadharreq){
		ResponseDTO resp=new ResponseDTO();
		UserSession userSession = userSessionRepository.findByActiveSessionId(aadharreq.getSessionId());
		if(userSession!=null){
			MUser user=userSession.getUser();
			if (role.equalsIgnoreCase("User")) {
				if (user.getAuthority().contains(Authorities.USER)) {
					MUserDetails userDetails=user.getUserDetail();
					if(userDetails!=null){
						userDetails.setAadharHashId(aadharreq.getAadharHashId());
						userDetails.setTransactionId(aadharreq.getTransactionId());
						userDetailRepository.save(userDetails);
						MPQAccountType accType=mPQAccountTypeRepository.findByCode("KYC");
						MPQAccountDetails accDetails=user.getAccountDetail();
						if(accDetails!=null){
							accDetails.setAccountType(accType);
							mPQAccountDetailRepository.save(accDetails);
						}
						resp.setCode("S00");
						resp.setMessage("Account upgradeded to KYC");
					}
				}
			}
		}else{
			resp.setCode("F03");
			resp.setMessage("Invalid Session");
		}
			return new ResponseEntity<ResponseDTO>(resp,HttpStatus.OK);
	}

	
	
}
