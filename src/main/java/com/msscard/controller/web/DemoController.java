package com.msscard.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.msscard.app.model.response.RegisterDemoResponse;
import com.msscard.entity.DemoUsers;
import com.msscard.model.RegisterDTO;
import com.msscard.repositories.DemoUsersRepository;
import com.msscard.util.ModelMapKey;

@Controller
public class DemoController{

	private final DemoUsersRepository demoUserRepository;
	

	public DemoController(DemoUsersRepository demoUserRepository) {
		super();
		this.demoUserRepository = demoUserRepository;
	}


	@RequestMapping(value="/RegisterDemo",method=RequestMethod.POST)
	public ResponseEntity<RegisterDemoResponse> getUserForDemo(@RequestBody RegisterDTO dto,HttpServletRequest request,HttpServletResponse response){
		
		RegisterDemoResponse demoResponse=new RegisterDemoResponse();
		System.err.println("hi m here");
		try{
		if(dto.getContactNo()!=null||dto.getFirstName()!=null||dto.getLastName()!=null||dto.getEmail()!=null){
		DemoUsers demo=new DemoUsers();
		demo.setContactNo(dto.getContactNo());
		demo.setName(dto.getFirstName()+" "+dto.getLastName());
		demo.setEmail(dto.getEmail());
		demoUserRepository.save(demo);
		demoResponse.setCode("S00");
		demoResponse.setMessage("Users saved SuccessFully...");
		}else{
			demoResponse.setCode("F00");
			demoResponse.setMessage("User not saved..");
		}
		}catch(Exception e){
			demoResponse.setCode("F00");
			demoResponse.setMessage("User saving failed");
			return new ResponseEntity<RegisterDemoResponse>(demoResponse,HttpStatus.OK);
		}
		return new ResponseEntity<>(demoResponse,HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String getHome(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, RedirectAttributes modelMap,
			HttpSession session) {
		
		return "promo";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getHome(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpSession session) {
		
		return "promo";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PrivacyPolicy")
	public String getPrivacy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "policy";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Terms")
	public String getTerms(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Terms";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/About")
	public String getAboutUs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "About";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/faqs")
	public String getFaqs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "Faqs";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/GrievancePolicy")
	public String getGrievancePolicy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {		
		return "grievancepolicy";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/RefundPolicy")
	public String getRefundPolicy(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {		
		return "refundpolicy";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ContactUs")
	public String getContactUs(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			 HttpSession session) {
		
		return "contactus";
	}


}
