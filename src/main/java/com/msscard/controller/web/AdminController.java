package com.msscard.controller.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;

import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IAgentApi;
import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IMdexBusApi;
import com.msscard.app.api.ISMSSenderApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.AllTransactionRequest;
import com.msscard.app.model.request.BusTicketDTO;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.IsCancellableReq;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CancelTicketResp;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.PrefundRequestDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.app.sms.util.SMSAccount;
import com.msscard.app.sms.util.SMSTemplete;
import com.msscard.entity.AdminAccessIdentifier;
import com.msscard.entity.AdminActivity;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.BusTicket;
import com.msscard.entity.CashBack;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.CorporateFileCatalogue;
import com.msscard.entity.CorporatePrefundHistory;
import com.msscard.entity.Donation;
import com.msscard.entity.DownloadHistory;
import com.msscard.entity.FCMDetails;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.MCommission;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.MerchantDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.PromoCode;
import com.msscard.entity.PromoCodeRequest;
import com.msscard.entity.SendMoneyDetails;
import com.msscard.entity.TwoMinBlock;
import com.msscard.entity.UserSession;
import com.msscard.entity.VersionLogs;
import com.msscard.model.AddMerchantDTO;
import com.msscard.model.AgentPrefundDTO;
import com.msscard.model.AgentTransactionListDTO;
import com.msscard.model.ApiVersionDTO;
import com.msscard.model.BulkSMSRequest;
import com.msscard.model.BusDetailsDTO;
import com.msscard.model.CardListRespDTO;
import com.msscard.model.CashBackDTO;
import com.msscard.model.CorporateListDTO;
import com.msscard.model.DownloadCsv;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.MAgentDTO;
import com.msscard.model.MCorporateAgentDTO;
import com.msscard.model.MMCardsDTO;
import com.msscard.model.MTransactionDTO;
import com.msscard.model.MserviceDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.PromoCodeRequestDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.ReviewStatusDTO;
import com.msscard.model.SendNotificationDTO;
import com.msscard.model.Status;
import com.msscard.model.TransactionListDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.CashBackError;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.LoginError;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.AdminAccessIdentifierRepository;
import com.msscard.repositories.AdminActivityRepository;
import com.msscard.repositories.CashBackRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.CorporateFileCatalogueRepository;
import com.msscard.repositories.CorporatePrefundHistoryRepository;
import com.msscard.repositories.DownloadHistoryRepository;
import com.msscard.repositories.FCMDetailsRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MServiceTypeRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.MerchantDetailsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.PromoCodeRepository;
import com.msscard.repositories.PromoCodeRequestRepository;
import com.msscard.repositories.SendMoneyDetailsRepository;
import com.msscard.repositories.TwoMinBlockRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.ExcelWriter;
import com.msscard.util.ModelMapKey;
import com.msscard.util.SecurityUtil;
import com.msscard.util.ServiceCodeUtil;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.CashBackValidations;
import com.msscard.validation.CommonValidation;
import com.msscard.validation.LoginValidation;
import com.msscard.validation.RegisterValidation;
import com.msscard.validation.TransactionTypeValidation;
import com.msscards.session.PersistingSessionRegistry;
import com.msscards.session.SessionLoggingStrategy;
import com.razorpay.constants.RazorPayConstants;

@Controller
@RequestMapping("/Admin")
public class AdminController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final ITransactionApi transactionApi;
	private final IMatchMoveApi matchMoveApi;
	private final RegisterValidation registerValidation;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final MMCardRepository mmCardRepository;
	private final PromoCodeRepository promoCodeRepository;
	private final CommonValidation commonValidation;
	private final MTransactionRepository mTransactionRepository;
	private final MMCardRepository cardRepository;
	private final PromoCodeRequestRepository promoCodeRequestRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final SendMoneyDetailsRepository sendMoneyDetailsRepository;
	private final MServiceRepository mServiceRepository;
	private final IAgentApi agentApi;
	private final CorporateFileCatalogueRepository corporateFileCatalogueRepository;
	private final MUserRespository mUserRespository;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final CorporatePrefundHistoryRepository corporatePrefundHistoryRepository;
	private final MServiceTypeRepository mServiceTypeRepository;
	private final DownloadHistoryRepository downloadHistoryRepository;
	private final MKycRepository mKycRepository;
	private final MerchantDetailsRepository merchantDetailsRepository;
	private final CashBackRepository cashBackRepository;
	private final ISMSSenderApi senderApi;
	private final CashBackValidations cashBackValidations;
	private final FCMDetailsRepository fcmDetailsRepository;
	private final IAuthenticationApi authenticationApi;
	private final AdminAccessIdentifierRepository adminAccessIdentifierRepository;
	private final IMdexBusApi mdexBusApiImpl;
	private final TransactionTypeValidation transactionTypeValidation;
	private final TwoMinBlockRepository twoMinBlockRepository;
	private final AdminActivityRepository adminActivityRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final MMCardRepository mMCardRepository;

	public AdminController(IUserApi userApi, AuthenticationManager authenticationManager,
			UserSessionRepository userSessionRepository, SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi, LoginValidation loginValidation, ITransactionApi transactionApi,
			IMatchMoveApi matchMoveApi, RegisterValidation registerValidation,
			PersistingSessionRegistry persistingSessionRegistry, MMCardRepository mmCardRepository,
			PromoCodeRepository promoCodeRepository, CommonValidation commonValidation,
			MTransactionRepository mTransactionRepository, MMCardRepository cardRepository,
			PromoCodeRequestRepository promoCodeRequestRepository,MatchMoveWalletRepository matchMoveWalletRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository,
			SendMoneyDetailsRepository sendMoneyDetailsRepository, MServiceRepository mServiceRepository,
			IAgentApi agentApi, CorporateFileCatalogueRepository corporateFileCatalogueRepository,
			MUserRespository mUserRespository, CorporateAgentDetailsRepository corporateAgentDetailsRepository,
			CorporatePrefundHistoryRepository corporatePrefundHistoryRepository,
			MServiceTypeRepository mServiceTypeRepository, DownloadHistoryRepository downloadHistoryRepository,
			MKycRepository mKycRepository, MerchantDetailsRepository merchantDetailsRepository, CashBackRepository cashBackRepository,
			ISMSSenderApi senderApi, CashBackValidations cashBackValidations, FCMDetailsRepository fcmDetailsRepository,
			IAuthenticationApi authenticationApi, AdminAccessIdentifierRepository adminAccessIdentifierRepository,
			IMdexBusApi mdexBusApiImpl, TransactionTypeValidation transactionTypeValidation,
			TwoMinBlockRepository twoMinBlockRepository, AdminActivityRepository adminActivityRepository,
			GroupFileCatalogueRepository groupFileCatalogueRepository, MMCardRepository mMCardRepository) {
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.loginValidation = loginValidation;
		this.transactionApi = transactionApi;
		this.matchMoveApi = matchMoveApi;
		this.registerValidation = registerValidation;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.mmCardRepository = mmCardRepository;
		this.promoCodeRepository = promoCodeRepository;
		this.commonValidation = commonValidation;
		this.mTransactionRepository = mTransactionRepository;
		this.cardRepository = cardRepository;
		this.promoCodeRequestRepository = promoCodeRequestRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.sendMoneyDetailsRepository = sendMoneyDetailsRepository;
		this.mServiceRepository = mServiceRepository;
		this.agentApi = agentApi;
		this.corporateFileCatalogueRepository = corporateFileCatalogueRepository;
		this.mUserRespository = mUserRespository;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.corporatePrefundHistoryRepository = corporatePrefundHistoryRepository;
		this.mServiceTypeRepository = mServiceTypeRepository;
		this.downloadHistoryRepository = downloadHistoryRepository;
		this.mKycRepository = mKycRepository;
		this.merchantDetailsRepository = merchantDetailsRepository;
		this.cashBackRepository = cashBackRepository;
		this.senderApi = senderApi;
		this.cashBackValidations = cashBackValidations;
		this.fcmDetailsRepository = fcmDetailsRepository;
		this.authenticationApi = authenticationApi;
		this.adminAccessIdentifierRepository = adminAccessIdentifierRepository;
		this.mdexBusApiImpl = mdexBusApiImpl;
		this.transactionTypeValidation = transactionTypeValidation;
		this.twoMinBlockRepository = twoMinBlockRepository;
		this.adminActivityRepository = adminActivityRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.mMCardRepository = mMCardRepository;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getHomess(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		logger.info("admin session id::--> " + sessionId);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Double totalTxnAmount = transactionApi.getTransactionAmount();
				model.addAttribute("totalTxn", totalTxnAmount);
				session.setAttribute("totalTxn", totalTxnAmount);
				session.setAttribute("totalTxnAmount", totalTxnAmount);
				long totalCards = userApi.getMCardCount();
				long totalPhysicalCard = userApi.gettotalPhysicalCard();
				long totalActiveUser = userApi.getTotalActiveUser();
				if (String.valueOf(totalTxnAmount) != null) {
					DecimalFormat f = new DecimalFormat("##.00");
					session.setAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
				}
				session.setAttribute("totalCards", totalCards);
				session.setAttribute("totalPhysicalCards", totalPhysicalCard);
				session.setAttribute("totalActiveUser", totalActiveUser);
				UserKycResponse walletResponse = new UserKycResponse();
				walletResponse = matchMoveApi.getConsumers();
				try {
					if (walletResponse != null) {
						DecimalFormat f = new DecimalFormat("##.00");
						session.setAttribute("totalPrefundAmount",
								f.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				AllTransactionRequest transRequest = new AllTransactionRequest();
				transRequest.setPage(0);
				transRequest.setSize(10);
				transRequest.setSessionId(sessionId);
				List<MTransaction> transactions = transactionApi.findTransactions(transRequest);
				session.setAttribute("txList", transactions);
				Long phyCount = mmCardRepository.getCountPhysicalCards();
				if (phyCount != null) {
					model.addAttribute("phycount", phyCount.longValue());
				} else {
					model.addAttribute("phycount", 0);
				}
				Long virtualCard = mmCardRepository.getCountVirtualCards();
				if (virtualCard != null) {
					model.addAttribute("virCount", virtualCard.longValue());
				} else {
					model.addAttribute("virCount", 0);
				}
				Long pendingCardReq = physicalCardDetailRepository.getCountPhysicalCardRequestPending();
				if (pendingCardReq != null) {
					model.addAttribute("pendingReq", pendingCardReq.longValue());
				} else {
					model.addAttribute("pendingReq", 0);
				}
				MService razorpay = mServiceRepository.findServiceByCode("LMS");
				MService upiPay = mServiceRepository.findServiceByCode("UPS");
				List<String> monthsListRazorPay = mTransactionRepository.getMonthsForBarGraph(Status.Success, razorpay);
				List<String> monthsListUpi = mTransactionRepository.getMonthsForBarGraph(Status.Success, upiPay);
				List<Double> upiAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success, upiPay);
				List<Double> razorAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success,
						razorpay);
				System.err.println("size monthsListRazorPay:" + monthsListRazorPay.size());
				System.err.println("size razorAmtList:" + razorAmtList.size());
				if (upiAmtList == null || upiAmtList.isEmpty()) {
					upiAmtList.add(0.0);
				}
				if (monthsListUpi == null || monthsListUpi.isEmpty()) {
					monthsListUpi.add("june");
				}
				System.err.println("size monthsListUpi:" + monthsListUpi.size());
				System.err.println("size upiAmtList:" + upiAmtList.size());
				if (monthsListRazorPay.size() >= monthsListUpi.size()) {
					List<String> stringList = new ArrayList<>();
					for (String string : monthsListRazorPay) {
						stringList.add("'" + string + "'");
					}
					model.addAttribute("monthsList", stringList);
					int sizeDiff = monthsListRazorPay.size() - monthsListUpi.size();
					System.err.println(sizeDiff);
					if (sizeDiff != 0) {
						List<Double> doubleList = new ArrayList<>();

						for (int i = 0; i < sizeDiff; i++) {
							doubleList.add(0.0);
						}
						for (Double double1 : upiAmtList) {
							doubleList.add(double1);
						}
						model.addAttribute("monthsAmountUPI", doubleList);
					}
				} else {
					int sizeDiff = monthsListRazorPay.size() - monthsListUpi.size();
					System.err.println(sizeDiff);
					if (sizeDiff != 0) {
						List<Double> doubleList = new ArrayList<>();
						for (int i = 0; i < sizeDiff; i++) {
							doubleList.add(0.0);
						}
						for (Double double1 : upiAmtList) {
							doubleList.add(double1);
						}
						model.addAttribute("monthsAmountUPI", doubleList);
					}
				}
				model.addAttribute("amountListRazorPay", razorAmtList);
				return "Admin/Home";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Home")
	public String adminLogin(@ModelAttribute("adminLogin") LoginDTO adminDto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) {
		System.out.println("Admin Login");
		session = request.getSession();

		LoginError error = loginValidation.checkLoginValidation(adminDto);
		try {
			if (error.isSuccess()) {
				MUser user = userApi.findByUserName(adminDto.getUsername());
				if (user != null && user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
					AuthenticationError authError = isValidUsernamePassword(adminDto, request);
					if (authError.isSuccess()) {
						AuthenticationError auth = authentication(adminDto, request);
						if (auth.isSuccess()) {
							String fromDate = CommonUtil.getDefaultDateRange()[0];
							String toDate = CommonUtil.getDefaultDateRange()[1];
							session.setAttribute("graphFromDate", fromDate);
							session.setAttribute("graphToDate", toDate);
							session.setAttribute("firstName", user.getUserDetail().getFirstName());
							session.setAttribute("username", user.getUsername());
							long totalTxn = transactionApi.getTransactionCountForAdmin();
							Double totalTxnAmount = transactionApi.getTransactionAmountForAdmin();
							model.addAttribute("totalTxn", totalTxn);
							session.setAttribute("totalTxn", totalTxn);
							if (String.valueOf(totalTxnAmount) != null) {
								DecimalFormat f = new DecimalFormat("##.00");
								session.setAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
							}
							session.setAttribute("totalTxnAmount", totalTxnAmount);
							long totalCards = userApi.getMCardCount();
							session.setAttribute("totalCards", totalCards);
							UserKycResponse walletResponse = new UserKycResponse();
							walletResponse = matchMoveApi.getConsumers();
							if (walletResponse != null) {
								DecimalFormat f = new DecimalFormat("##.00");
								session.setAttribute("totalPrefundAmount",
										f.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
							}
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							request.getSession().setAttribute("adminSessionId", userSession.getSessionId());
							model.addAttribute("graphFromDate", fromDate);
							model.addAttribute("graphToDate", toDate);
							return "Admin/Home";
						} else {
							model.addAttribute("loginMsg", "Invalid Username or Password");
						}
					} else {
						model.addAttribute("loginMsg", "Invalid Password");
						return "Admin/Login";
					}
				} else {
					model.addAttribute("loginMsg", "Invalid Username");
					return "Admin/Login";
				}
			} else {
				session.setAttribute("loginType", "Admin");
				session.setAttribute("loginMsg", error.getMessage());
				return "redirect:/";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("loginMsg", "Invalid Username or Password");
			return "Admin/Login";
		}
		session.setAttribute("loginType", "Admin");
		session.setAttribute("loginMsg", "Invalid Username or Password");
		return "redirect:/";
	}

	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage("Inavlid Password");
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setSuccess(false);
			error.setMessage(e.getMessage());
			return error;
		}
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				return error;
			} else {
				error.setSuccess(false);
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			return error;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CardList")
	public String getVirtualCardList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				return "Admin/CardList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CardListByPage")
	public ResponseEntity<ResponseDTO> getVirtualCardByPage(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model, ModelMap map) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				System.err.println("the date reange is :::::" + page.getDaterange());
				System.err.println("the status is :::::::::" + page.getCardStatus());
				String[] daterange = null;
				try {
					if (page.getDaterange().length() != 0) {
						System.err.println(page.getDaterange().length());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						String pardF = df2.format(fromDate);
						String pardT = df2.format(toDate);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Page<MMCards> card = userApi.findCardByPage(page, false);
				List<CardListRespDTO> cardList = new ArrayList<>();
				for (MMCards mmCards : card) {
					double balance = matchMoveApi.getBalance(mmCards.getWallet().getUser());
					CardListRespDTO lists = new CardListRespDTO();
					lists.setName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					lists.setContactNo(mmCards.getWallet().getUser().getUsername());
					lists.setCreated(mmCards.getCreated() + "");
					lists.setCardId(mmCards.getCardId());
					lists.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					lists.setStatus(mmCards.getStatus());
					lists.setBalance(balance);
					if (mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
						lists.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					} else {
						lists.setDob("NA");
					}
					cardList.add(lists);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Card List");
				adminActivityRepository.save(activity);
				System.out.println(cardList);
				resp.setJsonArray(cardList);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				session.setAttribute("cardList", card);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleUser")
	public ResponseEntity<ResponseDTO> getSingleUser(@ModelAttribute RequestDTO dto, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());
				MUser use = userApi.findByUserName(dto.getUserName());
				session.setAttribute("user", use);
				List<MMCardsDTO> cards = new ArrayList<>();
				MMCardsDTO car = new MMCardsDTO();
				car.setContactNO(use.getUserDetail().getContactNo());
				car.setEmail(use.getUserDetail().getEmail());
				car.setUserName(use.getUsername());
				car.setStatus(use.getMobileStatus() + "");
				car.setIssueDate(use.getCreated() + "");
				car.setFirstName(use.getUserDetail().getFirstName());
				car.setLastName(use.getUserDetail().getLastName());
				if (use.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
					car.setRole(true);
				} else {
					car.setRole(false);
				}
				car.setAccountType(use.getAccountDetail().getAccountType().getCode());
				if (use.getMobileToken() == null) {
					car.setMobileToken("NA");
				} else if ("".equalsIgnoreCase(use.getMobileToken())) {
					car.setMobileToken("NA");
				} else {
					car.setMobileToken(use.getMobileToken());
				}
				try {
					if (use.getGroupDetails().getGroupName() == null) {
						car.setGroup("-");
					} else {
						car.setGroup(use.getGroupDetails().getGroupName());
					}
				} catch (Exception e) {
					e.printStackTrace();
					car.setGroup("-");
				}
				car.setAuthority(use.getAuthority());
				if (use.getUserDetail().getDateOfBirth() != null) {
					car.setDob(use.getUserDetail().getDateOfBirth() + "");
				} else {
					car.setDob("NA");
				}
				car.setAddUserBool(accessing.isAddUser());
				cards.add(car);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Single User");
				adminActivityRepository.save(activity);
				session.setAttribute("userList", cards);
				resp.setJsonArray(cards);
				resp.setTotalPages(0);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CardListPhyByPage")
	public ResponseEntity<ResponseDTO> getPhysicalCardByPage(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				System.err.println("the date reange is :::::" + page.getDaterange());
				System.err.println("the status is :::::::::" + page.getCardStatus());
				String[] daterange = null;
				try {
					if (page.getDaterange().length() != 0) {
						System.err.println(page.getDaterange().length());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						String pardF = df2.format(fromDate);
						String pardT = df2.format(toDate);

						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Page<MMCards> card = userApi.findCardByPage(page, true);
				List<CardListRespDTO> cardList = new ArrayList<>();
				for (MMCards mmCards : card) {
					double balance = matchMoveApi.getBalance(mmCards.getWallet().getUser());
					CardListRespDTO lists = new CardListRespDTO();
					lists.setName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					lists.setContactNo(mmCards.getWallet().getUser().getUsername());
					lists.setCreated(mmCards.getCreated() + "");
					lists.setCardId(mmCards.getCardId());
					lists.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					lists.setStatus(mmCards.getStatus());
					lists.setBalance(balance);
					if (mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
						lists.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					} else {
						lists.setDob("NA");
					}
					cardList.add(lists);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Physical Card List");
				adminActivityRepository.save(activity);
				System.out.println(cardList);
				resp.setJsonArray(cardList);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				session.setAttribute("cardList", card);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CardList")
	public String getVirtualCardListPost(ModelMap modelMap, @ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model, ModelMap map) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Date date = new Date();
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String dateText = df2.format(date);
				Date toDate = null;
				Date fromDate = null;
				try {
					toDate = df2.parse(dateText);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, -1);
				String fr = df2.format(cal.getTime());
				try {
					fromDate = df2.parse(fr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				List<MMCards> card = userApi.getListByDateCards(fromDate, toDate, false);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MMCards mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setCardId(mmCards.getCardId());
					car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getWallet().getUser().getUsername());
					double balance = matchMoveApi.getBalance(mmCards.getWallet().getUser());
					car.setBalance(String.valueOf(balance));
					car.setStatus(mmCards.getStatus());
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
					car.setHasPhysicalCard(mmCards.isHasPhysicalCard());
					if (mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Card List");
				adminActivityRepository.save(activity);
				session.setAttribute("cardList", cards);
				return "Admin/CardList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PhysicalCardList")
	public String getPhysicalCardList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR) && user.getAuthority().contains(
					Authorities.AUTHENTICATED)) {
				return "Admin/PhysicalCardList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Card/Transactions")
	public String getCardTransactions(ModelMap modelMap, DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				if (dto.getFromDate() == null || dto.getToDate() == null || dto.getFromDate().equalsIgnoreCase("")
						|| dto.getToDate().equalsIgnoreCase("")) {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					dto.setToDate(dateText);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					dto.setFromDate(df2.format(cal.getTime()));
				}
				dto.setServiceName("");
				List<MTransaction> card = userApi.findAllCardTransactioons(dto);
				List<MTransactionDTO> cards = new ArrayList<>();
				for (MTransaction mmCards : card) {
					MTransactionDTO car = new MTransactionDTO();
					car.setAmount(mmCards.getAmount() + "");
					car.setAuthReferenceNo(mmCards.getAuthReferenceNo());
					car.setDebit(mmCards.isDebit() + "");
					car.setDescription(mmCards.getDescription());
					MUser usera = userApi.findByUserAccount(mmCards.getAccount());
					MMCards cardd = userApi.findCardByUser(usera);
					car.setCardId(cardd.getCardId());
					car.setEmail(usera.getUserDetail().getEmail());
					car.setName(usera.getUserDetail().getFirstName() + usera.getUserDetail().getLastName());
					car.setRequest(mmCards.getRequest());
					car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
					car.setStatus(mmCards.getStatus() + "");
					car.setTransactionDate(mmCards.getCreated() + "");
					car.setTransactionRefNo(mmCards.getTransactionRefNo());
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Card Transactions");
				adminActivityRepository.save(activity);
				session.setAttribute("cardTransList", cards);
				return "Admin/CardTransaction";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Card/Transactions")
	public String getCardTransactionsPost(ModelMap modelMap, @ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				if (dto.getFromDate() == null || dto.getToDate() == null || dto.getFromDate().equalsIgnoreCase("")
						|| dto.getToDate().equalsIgnoreCase("")) {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					dto.setToDate(dateText);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					dto.setFromDate(df2.format(cal.getTime()));
				}
				dto.setServiceName("");
				List<MTransaction> card = userApi.findAllCardTransactioons(dto);
				List<MTransactionDTO> cards = new ArrayList<>();
				for (MTransaction mmCards : card) {
					MTransactionDTO car = new MTransactionDTO();
					car.setAmount(mmCards.getAmount() + "");
					car.setAuthReferenceNo(mmCards.getAuthReferenceNo());
					car.setDebit(mmCards.isDebit() + "");
					car.setDescription(mmCards.getDescription());
					MUser usera = userApi.findByUserAccount(mmCards.getAccount());
					MMCards cardd = userApi.findCardByUser(usera);
					car.setCardId(cardd.getCardId());
					car.setEmail(usera.getUserDetail().getEmail());
					car.setName(usera.getUserDetail().getFirstName() + usera.getUserDetail().getLastName());
					car.setRequest(mmCards.getRequest());
					car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
					car.setStatus(mmCards.getStatus() + "");
					car.setTransactionDate(mmCards.getCreated() + "");
					car.setTransactionRefNo(mmCards.getTransactionRefNo());
					cards.add(car);
				}
				session.setAttribute("cardTransList", cards);
				return "Admin/CardTransaction";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SingleCard/{email}/{cardId}/{contactNo}")
	public String getSingleCard(@PathVariable(value = "email") String email,
			@PathVariable(value = "cardId") String cardId, @PathVariable(value = "contactNo") String contactNo,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MatchMoveCreateCardRequest cardFetch = new MatchMoveCreateCardRequest();
				cardFetch.setCardId(cardId);
				cardFetch.setEmail(email);
				try {
					MUser use = userApi.findByUserName(contactNo);
					cardFetch.setPassword(SecurityUtil.md5(contactNo));
					cardFetch.setUsername(contactNo);
					WalletResponse fetchResponse = matchMoveApi.inquireCard(cardFetch);
					if (fetchResponse.getWalletNumber() != null && fetchResponse != null) {
						String nor = fetchResponse.getWalletNumber();
						System.err.println(nor);
						String aaa = nor.substring(0, 4) + " " + nor.substring(4, 8) + " " + nor.substring(8, 12) + " "
								+ nor.substring(12, 16);
						fetchResponse.setWalletNumber(aaa);
						UserKycResponse walletRespon = new UserKycResponse();
						walletRespon = matchMoveApi.getTransactions(use);
						MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
						List<ResponseDTO> resul = new ArrayList<>();
						if (!whichCard.isHasPhysicalCard()) {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						} else {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						}
						session.setAttribute("transactions", resul);
					}
					double balance = matchMoveApi.getBalance(use);
					model.addAttribute("Ubalance", balance);
					MMCards card = matchMoveApi.findCardByCardId(cardId);
					model.addAttribute("cardstatus", card.isBlocked());
					session.setAttribute("cardDetail", fetchResponse);
					MUser us = userSession.getUser();
					AdminAccessIdentifier access = adminAccessIdentifierRepository.getallaccess(us);
					model.addAttribute("cardBlockInfo", access.isCardBlock());

					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Card Details");
					adminActivityRepository.save(activity);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Admin/CardDetail";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String getLogout(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Logout");
				adminActivityRepository.save(activity);
				sessionApi.expireSession(sessionId);
				return "Admin/Login";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddCorporateAgent")
	public String getPage(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				return "Admin/AddCorporateAgent";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddVersion")
	public String addVersion(@ModelAttribute ApiVersionDTO dto, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				ResponseDTO resp = userApi.Addversion(dto);
				List<VersionLogs> version = userApi.findAllVersion();
				List<ApiVersionDTO> dtoVersion = new ArrayList<>();
				for (int i = 0; i < version.size(); i++) {
					ApiVersionDTO ver = new ApiVersionDTO();
					ver.setName(version.get(i).getAndroidVersion().getName());
					ver.setApiKey(version.get(i).getAndroidVersion().getApiKey());
					ver.setApiVersion(version.get(i).getAndroidVersion().getApiVersion());
					ver.setApiStatus(version.get(i).getStatus());
					ver.setId(version.get(i).getId() + "");
					ver.setVersion(version.get(i).getVersion());
					ver.setCreated(version.get(i).getCreated() + "");
					ver.setUpdated(version.get(i).getLastModified() + "");
					dtoVersion.add(ver);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Add Version");
				adminActivityRepository.save(activity);
				modelMap.put("versionMessage", resp.getMessage());
				modelMap.put("versions", dtoVersion);
				return "Admin/UpdateVersion";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UpdateVersion")
	public String updateVersion(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				List<VersionLogs> version = userApi.findAllVersion();
				List<ApiVersionDTO> dtoVersion = new ArrayList<>();
				for (int i = 0; i < version.size(); i++) {
					ApiVersionDTO ver = new ApiVersionDTO();
					ver.setName(version.get(i).getAndroidVersion().getName());
					ver.setApiKey(version.get(i).getAndroidVersion().getApiKey());
					ver.setApiVersion(version.get(i).getAndroidVersion().getApiVersion());
					ver.setApiStatus(version.get(i).getStatus());
					ver.setId(version.get(i).getId() + "");
					ver.setVersion(version.get(i).getVersion());
					ver.setCreated(version.get(i).getCreated() + "");
					ver.setUpdated(version.get(i).getLastModified() + "");
					dtoVersion.add(ver);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("update version");
				adminActivityRepository.save(activity);

				modelMap.put("versions", dtoVersion);
				return "Admin/UpdateVersion";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AssignPhysicalCard")
	public String AssignPhysicalCard(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				return "Admin/AssignPhysicalCard";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddUser")
	public String AddUser(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				return "Admin/AddUser";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GeneratePromoCode")
	public String getPromoCode(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				List<MService> service = userApi.fetchServices();
				List<MserviceDTO> serv = new ArrayList<>();
				if (service != null) {
					for (int i = 0; i < service.size(); i++) {
						if (!service.get(i).getCode().equalsIgnoreCase("PROMO")) {
							MserviceDTO dto = new MserviceDTO();
							dto.setCode(service.get(i).getCode());
							dto.setName(service.get(i).getName());
							serv.add(dto);
						}
					}
					modelMap.put("services", serv);

					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Generate Promo code");
					adminActivityRepository.save(activity);
				}
				return "Admin/GeneratePromoCode";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PromoCodeList")
	public String getPromoCodeList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					List<PromoCode> promocodes = userApi.fetchPromoCodes();
					List<PromoCodeDTO> list = new ArrayList<>();
					if (promocodes != null) {
						for (int i = 0; i < promocodes.size(); i++) {
							PromoCodeDTO val = new PromoCodeDTO();
							val.setAmount(promocodes.get(i).getAmount());
							val.setEndDate(promocodes.get(i).getEndDate() + "");
							val.setStartDate(promocodes.get(i).getStartDate() + "");
							val.setMaxCashBack(promocodes.get(i).getMaxCashBack() + "");
							val.setMinTrxAmount(promocodes.get(i).getMinTrxAmount() + "");
							val.setPromoCode(promocodes.get(i).getPromoCode());
							val.setServiceCode(promocodes.get(i).getService().getName());
							val.setCardStatus(promocodes.get(i).getStatus() + "");
							list.add(val);
						}
						modelMap.put("Promocode", list);

						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Promo Code List");
						adminActivityRepository.save(activity);
					}
					return "Admin/PromoCodeList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Admin/PromoCodeList";
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PromoCode/RequestList")
	public String getPromoCodeRequest(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					List<PromoCodeRequest> service = userApi.fetchPromoCodeRequestWithStatus();
					List<PromoCodeRequestDTO> serv = new ArrayList<>();
					if (service != null) {
						for (int i = 0; i < service.size(); i++) {
							PromoCode code = promoCodeRepository.fetchByPromoCode(service.get(i).getPromocode());
							PromoCodeRequestDTO req = new PromoCodeRequestDTO();
							req.setPromoCode(service.get(i).getPromocode());
							req.setName(service.get(i).getUser().getUserDetail().getFirstName());
							req.setContactNo(service.get(i).getUser().getUserDetail().getContactNo());
							req.setEmail(service.get(i).getUser().getUserDetail().getEmail());
							req.setMaxCashBack(code.getMaxCashBack() + "");
							req.setMinTrxAmount(code.getMinTrxAmount() + "");
							if (code.isPercentage()) {
								req.setPercentage(code.getPercentage());
								req.setAmountType(true);
							} else {
								req.setAmount(code.getAmount());
								req.setAmountType(false);
							}
							serv.add(req);
						}
						modelMap.put("requestss", serv);

						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Promo Code Request List");
						adminActivityRepository.save(activity);
					}
					return "Admin/PromoCodeRequestList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Admin/PromoCodeRequestList";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/BlockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> blockCard(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				System.err.println(cardRequest.getRequestType());
				System.err.println(cardRequest.getCardId());
				walletResponse = matchMoveApi.deActivateCards(cardRequest);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(true);
					card.setStatus(Status.Inactive.getValue());
					mmCardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Block Card");
				adminActivityRepository.save(activity);

				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/UnblockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> unblockCard(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				walletResponse = matchMoveApi.reActivateCard(cardRequest);
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setStatus(Status.Active.getValue());
					card.setBlocked(false);
					mmCardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Unblock Card");
				adminActivityRepository.save(activity);

				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/AddCorporateAgent", method = RequestMethod.POST)
	public String addCorporateAgent(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				dto.setUsername(dto.getEmail());
				dto.setPassword(dto.getContactNo());
				RegisterError registerError = registerValidation.validateCorporateAgent(dto);
				if (registerError.isValid()) {
					dto.setUserType(UserType.Corporate);
					try {
						if (dto.getCorporateLogo1() != null) {
							String panCardImgPath = CommonUtil.uploadCorporateLogo(dto.getCorporateLogo1(),
									dto.getCorporateName(), "CorporateLogos");
							dto.setCorporateLogo1Path(panCardImgPath);
						}
						if (dto.getCorporateLogo2() != null) {
							String aadharCardImgPath = CommonUtil.uploadCorporateLogo(dto.getCorporateLogo2(),
									dto.getCorporateName(), "CorporateLogos");
							dto.setCorporateLogo2Path(aadharCardImgPath);
						}

						userApi.saveUser(dto);
						List<MUser> listCorporate = mUserRespository.findAllMerchants();
						List<CorporateListDTO> corporateList = new ArrayList<>();
						for (MUser mUser : listCorporate) {
							CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
									.getByCorporateId(mUser);
							CorporateListDTO lust = new CorporateListDTO();
							lust.setCorporateName(agentDetails.getCorporateName());
							lust.setCorporateLogo1(agentDetails.getCorporateLogo1());
							lust.setCorporateLogo2(agentDetails.getCorporateLogo2());
							lust.setContactNo(agentDetails.getContactPersonNumber());
							if (agentDetails.isServiceStatus()) {
								lust.setStatus("Active");
							} else {
								lust.setStatus("Inactive");
							}
							lust.setUsername(agentDetails.getCorporate().getUsername());
							lust.setBalance(
									String.valueOf(agentDetails.getCorporate().getAccountDetail().getBalance()));
							lust.setAccountNumber(agentDetails.getBankAccountNo());
							lust.setIfscCode(agentDetails.getIfscCode());
							corporateList.add(lust);

						}
						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Add Corporate Agent");
						adminActivityRepository.save(activity);

						session.setAttribute("cardList", corporateList);
						map.put("msg", "Corporate Agent is added.");
						return "Admin/CorporateAgentList";
					} catch (Exception e) {
						e.printStackTrace();
						walletResponse.setMessage("Service Unavailable");
						map.put("msg", "Failed to add Corporate agent.");
						walletResponse.setCode(ResponseStatus.FAILURE.getValue());
						return "Admin/AddCorporateAgent";
					}

				} else {
					System.err.println(registerError.getMessage());
					walletResponse.setMessage(registerError.getMessage());
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					map.put("msg", registerError.getMessage());
					return "Admin/AddCorporateAgent";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}

	}

	@RequestMapping(value = "/fetchCorporateAgent", method = RequestMethod.GET)
	public String fetchCorporateAgent(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					List<MUser> listCorporate = mUserRespository.findAllMerchants();
					List<CorporateListDTO> corporateList = new ArrayList<>();
					for (MUser mUser : listCorporate) {
						CorporateAgentDetails agentDetails = corporateAgentDetailsRepository.getByCorporateId(mUser);
						if (agentDetails != null) {
							CorporateListDTO lust = new CorporateListDTO();
							lust.setCorporateName(agentDetails.getCorporateName());
							lust.setCorporateLogo1(agentDetails.getCorporateLogo1());
							lust.setCorporateLogo2(agentDetails.getCorporateLogo2());
							lust.setContactNo(agentDetails.getContactPersonNumber());
							if (agentDetails.isServiceStatus()) {
								lust.setStatus("Active");
							} else {
								lust.setStatus("Inactive");
							}
							lust.setUsername(agentDetails.getCorporate().getUsername());
							lust.setBalance(
									String.valueOf(agentDetails.getCorporate().getAccountDetail().getBalance()));
							lust.setAccountNumber(agentDetails.getBankAccountNo());
							lust.setIfscCode(agentDetails.getIfscCode());
							lust.setId(agentDetails.getId() + "");
							System.err.println(lust);
							corporateList.add(lust);
						}
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Fetch Corporate Agent");
					adminActivityRepository.save(activity);

					model.addAttribute("cardList", corporateList);
					return "Admin/CorporateAgentList";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/CorporateAgentList";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserList")
	public String getUserList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				return "Admin/UserList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/SingleCorporate/Transactions/{username}/{accountId}", method = RequestMethod.GET)
	public String fetchCorporateAgentTransaction(@PathVariable String username, @PathVariable String accountId,
			DateDTO dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				if (dto.getFromDate() == null || dto.getToDate() == null || dto.getFromDate().equalsIgnoreCase("")
						|| dto.getToDate().equalsIgnoreCase("")) {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					dto.setToDate(dateText);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					dto.setFromDate(df2.format(cal.getTime()));
				}
				dto.setServiceName("");
				long aId = Long.valueOf(accountId);
				dto.setAccountId(aId);
				MUser agent = userApi.findByUserName(username);
				List<MTransaction> transa = userApi.findSingleCorporateTransactioons(dto);
				List<MTransactionDTO> cards = new ArrayList<>();
				if (transa != null) {
					for (MTransaction mmCards : transa) {
						MTransactionDTO car = new MTransactionDTO();
						car.setAmount(mmCards.getAmount() + "");
						car.setDebit(mmCards.isDebit() + "");
						car.setDescription(mmCards.getDescription());
						car.setEmail(agent.getUserDetail().getEmail());
						car.setName(agent.getUserDetail().getFirstName() + agent.getUserDetail().getLastName());
						car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
						car.setTransactionDate(mmCards.getCreated() + "");
						cards.add(car);
					}
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Corporate Agent Transaction");
				adminActivityRepository.save(activity);

				session.setAttribute("cardTransList", cards);
				return "Admin/CorporateAgentTransaction";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/KycRequest", method = RequestMethod.GET)
	public String fetchKycRequest(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Kyc Request");
				adminActivityRepository.save(activity);
				return "Admin/KycRequest";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/UpdatedKycList", method = RequestMethod.GET)
	public String fetchKycRequestUpdate(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				return "Admin/KycRequest";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/UpdatedKycList", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> getKycRequestListUpdated(@ModelAttribute PagingDTO page,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				try {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());
					Page<MKycDetail> agent = userApi.findUpdatedKycList(page);
					List<MCorporateAgentDTO> list = new ArrayList<>();
					for (MKycDetail mAgent : agent) {
						MCorporateAgentDTO agnt = new MCorporateAgentDTO();
						if (mAgent.getUser() != null) {
							MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(mAgent.getUser());
							if (wallet != null) {
								agnt.setFirstName(mAgent.getUser().getUserDetail().getFirstName() + " "
										+ mAgent.getUser().getUserDetail().getLastName());
								agnt.setEmail(mAgent.getUser().getUserDetail().getEmail());
								agnt.setContactNo(mAgent.getUser().getUserDetail().getContactNo());
								agnt.setImage(mAgent.getIdImage());
								agnt.setImage2(mAgent.getIdImage1());
								agnt.setAddress1(mAgent.getAddress1());
								agnt.setAddress2(mAgent.getAddress2());
								agnt.setIdType(mAgent.getIdType());
								agnt.setIdNumber(mAgent.getIdNumber());
								agnt.setState(mAgent.getState());
								agnt.setCountry(mAgent.getCountry());
								agnt.setPinCode(mAgent.getPinCode());
								agnt.setCity(mAgent.getCity());
								agnt.setAccountId(mAgent.getUser().getAccountDetail().getId() + "");
								agnt.setUserId(mAgent.getId() + "");
								agnt.setAccountType(mAgent.getUser().getAccountDetail().getAccountType().getCode());
								agnt.setDob(mAgent.getUser().getUserDetail().getDateOfBirth() + "");
								agnt.setCreated(mAgent.getCreated());
								agnt.setAddUserBoolean(accessing.isAddUser());
								list.add(agnt);
							}
							walletResponse.setJsonArray(list);
							walletResponse.setDate(page.getDaterange());
							walletResponse.setTotalPages(agent.getTotalPages());
						}
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Updated Kyc List");
					adminActivityRepository.save(activity);
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/KycRequest", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> fetchKycRequestPOst(@ModelAttribute PagingDTO page, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				try {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					Page<MKycDetail> agent = userApi.findAllKycRequest(page);
					List<MCorporateAgentDTO> list = new ArrayList<>();
					for (MKycDetail mAgent : agent) {
						MCorporateAgentDTO agnt = new MCorporateAgentDTO();
						if (mAgent.getUser() != null) {
							MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(mAgent.getUser());
							if (wallet != null) {
								agnt.setFirstName(mAgent.getUser().getUserDetail().getFirstName() + " "
										+ mAgent.getUser().getUserDetail().getLastName());
								agnt.setEmail(mAgent.getUser().getUserDetail().getEmail());
								agnt.setContactNo(mAgent.getUser().getUserDetail().getContactNo());
								agnt.setImage(mAgent.getIdImage());
								agnt.setImage2(mAgent.getIdImage1());
								agnt.setAddress1(mAgent.getAddress1());
								agnt.setAddress2(mAgent.getAddress2());
								agnt.setIdType(mAgent.getIdType());
								agnt.setIdNumber(mAgent.getIdNumber());
								agnt.setState(mAgent.getState());
								agnt.setCountry(mAgent.getCountry());
								agnt.setPinCode(mAgent.getPinCode());
								agnt.setCity(mAgent.getCity());
								agnt.setAccountId(mAgent.getUser().getAccountDetail().getId() + "");
								agnt.setUserId(mAgent.getId() + "");
								agnt.setAccountType(mAgent.getUser().getAccountDetail().getAccountType().getCode());
								agnt.setDob(mAgent.getUser().getUserDetail().getDateOfBirth() + "");
								agnt.setCreated(mAgent.getCreated());
								list.add(agnt);
							}
							walletResponse.setJsonArray(list);
							walletResponse.setDate(page.getDaterange());
							walletResponse.setTotalPages(agent.getTotalPages());
						}
					}
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);

				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/SingleKycRequest", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> fetchSingleKycRequestPOst(@ModelAttribute PagingDTO page,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				try {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					MUser us = userApi.findByUserName(page.getUsername());
					MKycDetail mAgent = userApi.findKYCRequestByUserUpdated(us);
					List<MCorporateAgentDTO> list = new ArrayList<>();
					MCorporateAgentDTO agnt = new MCorporateAgentDTO();
					if (mAgent.getUser() != null) {
						agnt.setFirstName(mAgent.getUser().getUserDetail().getFirstName());
						agnt.setEmail(mAgent.getUser().getUserDetail().getEmail());
						agnt.setContactNo(mAgent.getUser().getUserDetail().getContactNo());
						agnt.setImage(mAgent.getIdImage());
						agnt.setImage2(mAgent.getIdImage1());
						agnt.setAddress1(mAgent.getAddress1());
						agnt.setAddress2(mAgent.getAddress2());
						agnt.setIdType(mAgent.getIdType());
						agnt.setIdNumber(mAgent.getIdNumber());
						agnt.setState(mAgent.getState());
						agnt.setCountry(mAgent.getCountry());
						agnt.setPinCode(mAgent.getPinCode());
						agnt.setCity(mAgent.getCity());
						agnt.setAccountId(mAgent.getUser().getAccountDetail().getId() + "");
						agnt.setUserId(mAgent.getId() + "");
						agnt.setAddUserBoolean(accessing.isAddUser());
						list.add(agnt);
						walletResponse.setJsonArray(list);
						walletResponse.setTotalPages(1);
					} else {
						walletResponse.setJsonArray(null);
						walletResponse.setTotalPages(1);
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Single Kyc Request Search");
					adminActivityRepository.save(activity);
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);

				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/SingleKycRequestRejected", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> fetchSingleKycRequestPOstREjected(@ModelAttribute PagingDTO page,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				try {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					MUser us = userApi.findByUserName(page.getUsername());
					MKycDetail mAgent = userApi.findKYCRequestByUserUpdatedRejectedKyc(us);
					List<MCorporateAgentDTO> list = new ArrayList<>();
					MCorporateAgentDTO agnt = new MCorporateAgentDTO();
					if (mAgent.getUser() != null) {
						if (mAgent.getRejectionReason() == null) {
							agnt.setRejectionReason("NA");
						} else {
							agnt.setRejectionReason(mAgent.getRejectionReason());
						}
						agnt.setFirstName(mAgent.getUser().getUserDetail().getFirstName());
						agnt.setEmail(mAgent.getUser().getUserDetail().getEmail());
						agnt.setContactNo(mAgent.getUser().getUserDetail().getContactNo());
						agnt.setImage(mAgent.getIdImage());
						agnt.setImage2(mAgent.getIdImage1());
						agnt.setAddress1(mAgent.getAddress1());
						agnt.setAddress2(mAgent.getAddress2());
						agnt.setIdType(mAgent.getIdType());
						agnt.setIdNumber(mAgent.getIdNumber());
						agnt.setState(mAgent.getState());
						agnt.setCountry(mAgent.getCountry());
						agnt.setPinCode(mAgent.getPinCode());
						agnt.setCity(mAgent.getCity());
						agnt.setAccountId(mAgent.getUser().getAccountDetail().getId() + "");
						agnt.setUserId(mAgent.getId() + "");
						list.add(agnt);
						walletResponse.setJsonArray(list);
						walletResponse.setTotalPages(1);
					}
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);

				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateKycRequest", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> updateKycRequest(@ModelAttribute UpgradeAccountDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					userApi.updateKycRequest(dto);
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Updated Kyc Request");
					adminActivityRepository.save(activity);
					map.put("mess", "User request Updated");
					walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateAllKycRequest", method = RequestMethod.POST)
	public String updateKyc(@ModelAttribute UpgradeAccountDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					System.err.println("tstststs" + dto.getIsCheckkk());
					map.put("mess", "User request Updated");
					walletResponse.setCode(ResponseStatus.SUCCESS.getValue());
					return "redirect:/Admin/KycRequest";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "redirect:/Admin/KycRequest";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "redirect:/Admin/Home";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "redirect:/Admin/Home";
		}
	}

	@RequestMapping(value = "/UserTransaction/{username}/{cardId}", method = RequestMethod.GET)
	public String fetchUserTransaction(@ModelAttribute RegisterDTO dto, @PathVariable String username,
			@PathVariable String cardId, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApi.findByUserName(username);
				try {
					UserKycResponse walletRespon = new UserKycResponse();
					walletRespon = matchMoveApi.getTransactions(use);
					List<ResponseDTO> resul = new ArrayList<>();
					MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
					if (!whichCard.isHasPhysicalCard()) {
						if (walletRespon.getCardTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					} else {
						if (walletRespon.getCardTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					}
					map.put("transactions", resul);
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("User Card Transaction");
					adminActivityRepository.save(activity);
					return "Admin/UserTransaction";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/UserTransaction";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	/**
	 * PROMO CODE
	 */

	@RequestMapping(value = "/GeneratePromoCode", method = RequestMethod.POST)
	public String generatePromoCode(@ModelAttribute RegisterDTO dto, @PathVariable String username,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				MUser use = userApi.findByUserName(username);
				try {
					UserKycResponse walletRespon = new UserKycResponse();
					persistingSessionRegistry.refreshLastRequest(sessionId);
					walletRespon = matchMoveApi.getTransactions(use);
					if (walletRespon.getDetails() != null) {
						JSONObject obj = new JSONObject(walletRespon.getDetails().toString());
						JSONArray aa = obj.getJSONArray("transactions");
						List<ResponseDTO> resul = new ArrayList<>();
						for (int i = 0; i < aa.length(); i++) {
							ResponseDTO resp = new ResponseDTO();
							resp.setAmount(aa.getJSONObject(i).getString("amount"));
							resp.setDate(aa.getJSONObject(i).getString("date"));
							resp.setDescription(aa.getJSONObject(i).getString("description"));
							resp.setStatus(aa.getJSONObject(i).getString("status"));
							resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
							resul.add(resp);
						}
						map.put("transactions", resul);
					}
					return "Admin/UserTransaction";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/UserTransaction";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/fetchUserList")
	public String getCorporateAddedUserList(@PathVariable String id, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				List<BulkRegister> card = userApi.findAllCorporateUser(id);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (BulkRegister mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getUser().getUsername());
					car.setStatus(mmCards.getUser().getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getUser().getUserDetail().getLastName());
					car.setAccountType(mmCards.getUser().getAccountDetail().getAccountType().getCode());
					if (mmCards.getUser().getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getUser().getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getUser().getMobileToken());
					}
					car.setAuthority(mmCards.getUser().getAuthority());
					if (mmCards.getUser().getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUser().getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Corporate User List");
				adminActivityRepository.save(activity);
				session.setAttribute("userList", cards);
				model.addAttribute("usern", id);
				return "Admin/CorporateUserList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserGroupList")
	public ResponseEntity<ResponseDTO> getUserListPOstGroup(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				Page<MUser> card = null;
				if (page.getContact() != null && !page.getContact().equals("All")) {
					card = userApi.findUserBasedOnGroup(page);
				} else {
					card = userApi.findAllUser(page);
				}

				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					if (mmCards.getGroupDetails().getGroupName() == null) {
						car.setGroup("-");
					} else {
						car.setGroup(mmCards.getGroupDetails().getGroupName());
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setChangeGroupStatus(mmCards.getUserDetail().isGroupChange());
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserList")
	public ResponseEntity<ResponseDTO> getUserListPOst(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());
				Page<MUser> card = userApi.findAllUser(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					try {
						if (mmCards.getGroupDetails().getGroupName() != null) {
							car.setGroup(mmCards.getGroupDetails().getGroupName());
						} else {
							car.setGroup("-");
						}

					} catch (Exception e) {
						car.setGroup("None");
						e.printStackTrace();
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setChangeGroupStatus(mmCards.getUserDetail().isGroupChange());
					if (mmCards.getUserDetail().isGroupChange() == true) {
						car.setChangedGroupName(mmCards.getUserDetail().getChangedGroupName());
					} else {
						car.setChangedGroupName("-");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("User List");
				adminActivityRepository.save(activity);

				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ActiveUserList")
	public String getActiveUserList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				return "Admin/ActiveUserList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ActiveUserList")
	public ResponseEntity<ResponseDTO> getActiveUserListPOst(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				Page<MUser> card = userApi.findAllActiveUser(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Active User List");
				adminActivityRepository.save(activity);

				session.setAttribute("userList", cards);
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/KYCUserList")
	public String getKYCUserList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<MUser> card = userApi.findAllKYCUser();
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					car.setEmail(mmCards.getUserDetail().getEmail());
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getUserDetail().getFirstName());
					car.setLastName(mmCards.getUserDetail().getLastName());
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Kyc User List");
				adminActivityRepository.save(activity);

				session.setAttribute("userList", cards);
				return "Admin/KYCUserList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/KYCUserList")
	public ResponseEntity<ResponseDTO> getKycUserListPOst(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				Page<MUser> card = userApi.findAllKYCUser(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				session.setAttribute("userList", cards);
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Status/block/unblock", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> userStatus(@RequestBody RequestDTO cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApi.findByUserName(cardRequest.getUserName());
				System.err.println("auth" + cardRequest.getAuthority());
				userApi.updateUserAuthority(cardRequest.getAuthority(), use.getId());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Card/PhysicalCardRequest")
	public String getPhysicalCardRequest(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<PhysicalCardDetails> card = userApi.findAllPhysicalCardRequest();
				List<MMCardsDTO> cards = new ArrayList<>();
				for (PhysicalCardDetails mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getWallet().getUser().getUsername());
					car.setStatus(mmCards.getStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
					car.setAddress(mmCards.getAddress1() + mmCards.getAddress2());
					car.setPincode(mmCards.getPin());
					car.setCity(mmCards.getCity());
					car.setLastModified(mmCards.getLastModified() + "");
					car.setId(mmCards.getId() + "");
					if (mmCards.getActivationCode() != null) {
						car.setActivationCode(mmCards.getActivationCode());
					} else {
						car.setActivationCode("NA");
					}
					if (mmCards.getComment() != null) {
						car.setComment(mmCards.getComment());
					} else {
						car.setComment("NA");
					}
					if (mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				session.setAttribute("physicalcardrequest", cards);
				modelMap.put("Rstatus", "All");
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("PhysicalCard Request List");
				adminActivityRepository.save(activity);
				return "Admin/PhysicalCardRequest";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Card/PhysicalCardRequestList")
	public ResponseEntity<ResponseDTO> getPhysicalCardRequestedList(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				Page<PhysicalCardDetails> card = null;
				if ("All".equalsIgnoreCase(page.getStatus())) {
					card = userApi.findAllPhysicalCardRequestUser(page);
				} else {
					card = userApi.findAllPhysicalCardRequestOnStatusList(page);
				}
				List<MMCardsDTO> cards = new ArrayList<>();
				for (PhysicalCardDetails mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getWallet().getUser().getUsername());
					car.setStatus(mmCards.getStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
					car.setAddress(mmCards.getAddress1() + mmCards.getAddress2());
					car.setPincode(mmCards.getPin());
					car.setCity(mmCards.getCity());
					if (mmCards.getActivationCode() != null) {
						car.setActivationCode(mmCards.getActivationCode());
					} else {
						car.setActivationCode("NA");
					}
					car.setId(mmCards.getId() + "");
					car.setLastModified(mmCards.getLastModified() + "");
					if (mmCards.getComment() == null) {
						car.setComment("NA");
					} else {
						car.setComment(mmCards.getComment());
					}
					car.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					car.setAddUserBool(accessing.isAddUser());
					cards.add(car);
				}
				resp.setJsonArray(cards);
				resp.setStatus(page.getStatus());
				resp.setTotalPages(card.getTotalPages());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PhysicalCardRequestSingleUser")
	public ResponseEntity<ResponseDTO> getPhysicalCardSingleRequest(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					MUser us = userApi.findByUserName(page.getUsername());
					if (us != null) {
						PhysicalCardDetails mmCards = physicalCardDetailRepository.findByUser(us);
						if (mmCards != null) {
							List<MMCardsDTO> cards = new ArrayList<>();
							MMCardsDTO car = new MMCardsDTO();
							car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
							car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
							car.setUserName(mmCards.getWallet().getUser().getUsername());
							car.setStatus(mmCards.getStatus() + "");
							car.setIssueDate(mmCards.getCreated() + "");
							car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
							car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
							car.setAddress(mmCards.getAddress1() + mmCards.getAddress2());
							car.setPincode(mmCards.getPin());
							car.setCity(mmCards.getCity());
							if (mmCards.getActivationCode() != null) {
								car.setActivationCode(mmCards.getActivationCode());
							} else {
								car.setActivationCode("NA");
							}
							car.setId(mmCards.getId() + "");
							car.setLastModified(mmCards.getLastModified() + "");
							if (mmCards.getComment() == null) {
								car.setComment("NA");
							} else {
								car.setComment(mmCards.getComment());
							}
							car.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
							cards.add(car);

							resp.setJsonArray(cards);
							resp.setStatus(page.getStatus());
						}
					}
					resp.setTotalPages(1);
				} catch (Exception e) {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailbale");
				}
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PhysicalCardRequestUserList")
	public ResponseEntity<ResponseDTO> getPhysicalCardRequestUserListPOst(@ModelAttribute PagingDTO page,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Page<PhysicalCardDetails> card = userApi.findAllPhysicalCardRequestUser(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (PhysicalCardDetails mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getWallet().getUser().getUsername());
					car.setStatus(mmCards.getStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
					car.setAddress(mmCards.getAddress1() + mmCards.getAddress2());
					car.setPincode(mmCards.getPin());
					car.setCity(mmCards.getCity());
					if (mmCards.getActivationCode() != null) {
						car.setActivationCode(mmCards.getActivationCode());
					} else {
						car.setActivationCode("NA");
					}
					car.setId(mmCards.getId() + "");
					car.setLastModified(mmCards.getLastModified() + "");
					if (mmCards.getComment() == null) {
						car.setComment("NA");
					} else {
						car.setComment(mmCards.getComment());
					}
					car.setDob(mmCards.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("PhysicalCard Request UserList");
				adminActivityRepository.save(activity);
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/AddPromo", method = RequestMethod.POST)
	public String addpromocode(@ModelAttribute PromoCodeDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					CommonError error = commonValidation.checkAddPromo(dto);
					if (error.isValid()) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (!dto.getPercentageValue().equalsIgnoreCase("")) {
							dto.setPercentage(true);
						}
						walletResponse = userApi.savePromoCode(dto);
						map.put("sucMessage", "Promo code added successfully.");
						List<MService> service = userApi.fetchServices();
						List<MserviceDTO> serv = new ArrayList<>();
						if (service != null) {
							for (int i = 0; i < service.size(); i++) {
								if (!service.get(i).getCode().equalsIgnoreCase("PROMO")) {
									MserviceDTO to = new MserviceDTO();
									to.setCode(service.get(i).getCode());
									to.setName(service.get(i).getName());
									serv.add(to);
								}
							}
							map.put("services", serv);
						}
						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Add Promo");
						adminActivityRepository.save(activity);
						return "Admin/GeneratePromoCode";
					} else {
						map.put("errMessage", error.getMessage());
						List<MService> service = userApi.fetchServices();
						List<MserviceDTO> serv = new ArrayList<>();
						if (service != null) {
							for (int i = 0; i < service.size(); i++) {
								if (!service.get(i).getCode().equalsIgnoreCase("PROMO")) {
									MserviceDTO to = new MserviceDTO();
									to.setCode(service.get(i).getCode());
									to.setName(service.get(i).getName());
									serv.add(to);
								}
							}
							map.put("services", serv);
						}
						return "Admin/GeneratePromoCode";
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Admin/GeneratePromoCode";
		}

	}

	@RequestMapping(value = "/Promocode", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getProm(@RequestBody PromoCodeDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					PromoCode code = promoCodeRepository.fetchByPromoCode(dto.getPromoCode());
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Promo Code");
					adminActivityRepository.save(activity);
					if (code == null) {
						walletResponse.setMessage("Valid promo code");
					} else {
						walletResponse.setMessage("Not a valid promo code");
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/PromoCodeRequest/Action", method = RequestMethod.POST)
	public String PromoCodeAction(@ModelAttribute RequestDTO cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					System.err.println("contactNo::::" + cardRequest.getContactNo());
					MUser use = userApi.findByUserName(cardRequest.getContactNo().trim());
					System.err.println("username,email::" + use.getUserDetail().getEmail());
					PromoCode promoCode = promoCodeRepository.fetchByPromoCode(cardRequest.getPromoCode());
					PromoCodeRequest codeRequest = promoCodeRequestRepository
							.findByPromoCodeAndUSer(cardRequest.getPromoCode(), use);
					String loadAmount;
					System.err.println(cardRequest.getAmount() + " his is the amt");
					System.err.println("");
					loadAmount = String.valueOf(promoCode.getMaxCashBack());
					MMCards Physical = cardRepository.getPhysicalCardByUser(use);
					MMCards virtual = cardRepository.getVirtualCardsByCardUser(use);
					WalletResponse cardTransferResposne = null;
					if (Physical != null && Physical.getStatus().equalsIgnoreCase("Active")) {
						WalletResponse walletResponse1 = matchMoveApi.initiateLoadFundsToMMWalletTopup(use, loadAmount);
						if (walletResponse1.getCode().equalsIgnoreCase("S00")) {
							cardTransferResposne = matchMoveApi.transferFundsToMMCard(use, loadAmount,
									Physical.getCardId());
						}
					} else if (virtual != null && virtual.getStatus().equalsIgnoreCase("Active")) {
						System.err.println("hi m here");
						WalletResponse walletResponse2 = matchMoveApi.initiateLoadFundsToMMWalletTopup(userSession.getUser(),
								loadAmount);
						if (walletResponse2.getCode().equalsIgnoreCase("S00")) {
							cardTransferResposne = matchMoveApi.transferFundsToMMCard(use, loadAmount,
									virtual.getCardId());
						}
					}
					if ("S00".equalsIgnoreCase(cardTransferResposne.getCode())) {
						map.put("sucMessage", "Amount loaded succesfully");
						transactionApi.doPromocodeTransaction(use, loadAmount);
						codeRequest.setStatus(Status.Received);
						promoCodeRequestRepository.save(codeRequest);
					} else {
						map.put("errMessage", "Service unavailable");
					}
					List<PromoCodeRequest> service = userApi.fetchPromoCodeRequest();
					List<PromoCodeRequestDTO> serv = new ArrayList<>();
					if (service != null) {
						for (int i = 0; i < service.size(); i++) {
							PromoCode code = promoCodeRepository.fetchByPromoCode(service.get(i).getPromocode());
							PromoCodeRequestDTO req = new PromoCodeRequestDTO();
							req.setPromoCode(service.get(i).getPromocode());
							req.setName(service.get(i).getUser().getUserDetail().getFirstName());
							req.setContactNo(service.get(i).getUser().getUserDetail().getContactNo());
							req.setEmail(service.get(i).getUser().getUserDetail().getEmail());
							req.setMaxCashBack(code.getMaxCashBack() + "");
							req.setMinTrxAmount(code.getMinTrxAmount() + "");
							if (code.isPercentage()) {
								req.setPercentage(code.getPercentage());
								req.setAmountType(true);
							} else {
								req.setAmount(code.getAmount());
								req.setAmountType(false);
							}
							serv.add(req);
						}
						map.put("requestss", serv);
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("PromoCode Request List");
					adminActivityRepository.save(activity);
					return "Admin/PromoCodeRequestList";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/PromoCodeRequestList";
		}

	}

	@RequestMapping(value = "/LoadMoney/Transactions", method = RequestMethod.GET)
	public String getLoadTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = mTransactionRepository.getAllLoadTransactions(oneMonthBack,
							present, pageable);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						if (userAccount == null) {
							listDTO.setContactNo("NA");
							listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
							listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
							listDTO.setCreated(sdf.format(mTransaction.getCreated()));
							listDTO.setStatus(mTransaction.getStatus().getValue());
							listDTO.setUsername("NA");
							listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
							if (mTransaction.getAuthReferenceNo() == null) {
								listDTO.setRetrivalRefNo("NA");
							} else {
								listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
							}
							if (mTransaction.getCardLoadStatus() == null) {
								listDTO.setCardLoadStatus("NA");
							} else {
								listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
							}
							if (mTransaction.getRemarks() == null) {
								listDTO.setError("NA");
							} else {
								listDTO.setError(mTransaction.getRemarks());
							}
							transactionList.add(listDTO);
						} else {

							listDTO.setContactNo(userAccount.getUsername());
							listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
							listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
							listDTO.setCreated(sdf.format(mTransaction.getCreated()));
							listDTO.setStatus(mTransaction.getStatus().getValue());
							listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
									+ userAccount.getUserDetail().getLastName());
							listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
							if (mTransaction.getAuthReferenceNo() == null) {
								listDTO.setRetrivalRefNo("NA");
							} else {
								listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
							}
							if (mTransaction.getCardLoadStatus() == null) {
								listDTO.setCardLoadStatus("NA");
							} else {
								listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
							}
							if (mTransaction.getRemarks() == null) {
								listDTO.setError("NA");
							} else {
								listDTO.setError(mTransaction.getRemarks());
							}
							transactionList.add(listDTO);
						}
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Load Money Transaction List");
					adminActivityRepository.save(activity);

					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					return "Admin/LoadTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/Home";
		}
	}

	@RequestMapping(value = "/PromoCode/Transactions", method = RequestMethod.GET)
	public String promoTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = mTransactionRepository.getAllPromoTransactions(oneMonthBack,
							present, pageable);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					return "Admin/PromoTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/Home";
		}

	}

	@RequestMapping(value = "/Promo/TransactionsDatewise", method = RequestMethod.POST)
	public String promoTransactionsDateWise(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					if ("All".equalsIgnoreCase(dto.getStatus())) {
						listTransaction = mTransactionRepository.getAllPromoTransactions(from, to, pageable);
					} else {
						if ("Failed".equalsIgnoreCase(dto.getStatus())) {

							listTransaction = mTransactionRepository.getPromosTransaction(from, to, Status.Failed,
									pageable);
						} else if ("Initiated".equalsIgnoreCase(dto.getStatus())) {
							listTransaction = mTransactionRepository.getPromosTransaction(from, to, Status.Initiated,
									pageable);
						} else {
							listTransaction = mTransactionRepository.getLoadPromoTransactions(from, to, pageable);
						}
					}
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", dto.getStatus());
					map.put("dat", dto.getToDate());
					return "Admin/PromoTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/PromoTransactions";
		}

	}

	@RequestMapping(value = "/LoadMoneyTransactions", method = RequestMethod.POST)
	public String getLoadTransaction(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					if ("All".equalsIgnoreCase(dto.getStatus())) {
						listTransaction = mTransactionRepository.getAllLoadTransactions(from, to, pageable);
					} else {
						if ("Failed".equalsIgnoreCase(dto.getStatus())) {

							listTransaction = mTransactionRepository.getLoadTransactions(from, to, Status.Failed,
									pageable);
						} else if ("Initiated".equalsIgnoreCase(dto.getStatus())) {
							listTransaction = mTransactionRepository.getLoadTransactions(from, to, Status.Initiated,
									pageable);
						} else {
							listTransaction = mTransactionRepository.getLoadTransactions(from, to, pageable);
						}
					}
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("LoadMoney Transaction List");
					adminActivityRepository.save(activity);

					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", dto.getStatus());
					map.put("dat", dto.getToDate());
					return "Admin/LoadTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/LoadTransactions";
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddUser")
	public String addUser(ModelMap modelMap, @ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				dto.setUsername(dto.getContactNo());
				if (true) {
					System.err.println(dto);
					dto.setUserType(UserType.User);
					try {
						boolean val = userApi.saveUserFromAdmin(dto);
						if (val) {
							if (!dto.isPhysicalCard()) {
								MUser user1 = userApi.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									WalletResponse walletResponse = matchMoveApi.assignVirtualCard(user1);
									matchMoveApi.tempKycUserMM(user1);
									matchMoveApi.tempSetIdDetails(user1);
									matchMoveApi.tempSetImagesForKyc(user1);
									matchMoveApi.tempConfirmKyc(user1);
									matchMoveApi.tempKycStatusApproval(user1);
									if (walletResponse.getCode().equalsIgnoreCase("S00")) {
										map.put("regmessage", "User added Sucessfully and virtual card assigned. ");
									} else {
										map.put("regmessage", "User added Sucessfully but virtual card not assigned. ");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							} else {

								MUser user1 = userApi.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									matchMoveApi.tempKycUserMM(user1);
									matchMoveApi.tempSetIdDetails(user1);
									matchMoveApi.tempSetImagesForKyc(user1);
									matchMoveApi.tempConfirmKyc(user1);
									matchMoveApi.tempKycStatusApproval(user1);
									WalletResponse walletResponse = new WalletResponse();
									MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
									if (wallet != null) {
										PhysicalCardDetails physicalCard = new PhysicalCardDetails();
										physicalCard.setFromAdmin(true);
										physicalCard.setWallet(wallet);
										physicalCardDetailRepository.save(physicalCard);
										walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getProxyNumber());
										if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
											PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
											card.setStatus(Status.Received);
											physicalCardDetailRepository.save(card);
											ResponseDTO respo = matchMoveApi
													.activationPhysicalCard(card.getActivationCode(), user1);
											if (respo.getCode().equalsIgnoreCase("S00")) {
												map.put("sucMsg", "Physical card assigned.");
												PhysicalCardDetails card1 = physicalCardDetailRepository
														.findByUser(user1);
												card.setStatus(Status.Active);
												physicalCardDetailRepository.save(card1);
												map.put("regmessage",
														"User added Sucessfully and physical card assigned. ");
											} else {
												map.put("regmessage",
														"User added Sucessfully but physical card not assigned. ");
											}
										} else {
											map.put("regmessage",
													"User added Sucessfully but physical card not assigned. ");
										}

									} else {
										map.put("regmessage", "failed, Please try again later.");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							}
						} else {
							map.put("regmessage", "User not added.");
						}
					} catch (Exception e) {
						e.printStackTrace();
						map.put("regmessage", "User not added please try again later");
					}
				} 
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Add User");
				adminActivityRepository.save(activity);
				return "Admin/AddUser";
			}
			return "Admin/Login";
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/UpdateVersionStatus", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> updateVersionStatus(@RequestBody ApiVersionDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					walletResponse = userApi.updateVersionStatus(dto.getId(), dto.getStatus());
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/assignPhysical")
	public String assignPhysical(ModelMap modelMap, @ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				MUser user1 = userApi.findByUserName(dto.getContactNo());
				WalletResponse walletResponse = new WalletResponse();
				if (user1 != null) {
					MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
					if (wallet != null) {
						PhysicalCardDetails physicalCard = new PhysicalCardDetails();
						physicalCard.setFromAdmin(true);
						physicalCard.setWallet(wallet);
						physicalCardDetailRepository.save(physicalCard);
						walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getKitNo());
						if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
							PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
							card.setStatus(Status.Received);
							physicalCardDetailRepository.save(card);
							ResponseDTO resp = matchMoveApi.activationPhysicalCard(card.getActivationCode(), user1);
							if (resp.getCode().equalsIgnoreCase("S00")) {
								map.put("sucMsg", "Physical card assigned.");
								PhysicalCardDetails card1 = physicalCardDetailRepository.findByUser(user1);
								card.setStatus(Status.Active);
								physicalCardDetailRepository.save(card1);
								MMCards cards = cardRepository.getVirtualCardsByCardUser(user1);
								if (cards != null) {
									if (cards.getStatus().equalsIgnoreCase("Active")) {

										String cardId = cards.getCardId();
										double balance = matchMoveApi.getBalance(user1);
										if (balance > 0) {
											matchMoveApi.debitFromMMWalletToCard(user1.getUserDetail().getEmail(),
													user1.getUsername(), cardId, String.valueOf(balance));
											MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
											cardReq.setCardId(cardId);
											cardReq.setRequestType("suspend");
											matchMoveApi.deActivateCards(cardReq);
											MMCards phyCard = cardRepository.getPhysicalCardByUser(user1);
											if (phyCard != null) {
												if (phyCard.getStatus().equalsIgnoreCase("Active")) {
													matchMoveApi.transferFundsToMMCard(user1, String.valueOf(balance),
															phyCard.getCardId());
												}
											}
											map.put("sucMsg", "Physical card assigned.");
										} else {
											MatchMoveCreateCardRequest cardReq = new MatchMoveCreateCardRequest();
											cardReq.setCardId(cardId);
											cardReq.setRequestType("suspend");
											matchMoveApi.deActivateCards(cardReq);
										}
									}
								}
								map.put("sucMsg", "Physical card assigned.");
								AdminActivity activity = new AdminActivity();
								activity.setActivityName("Assign physical card");
								adminActivityRepository.save(activity);
							} else {
								map.put("errorMsg", "Failed,please try again later.");
							}
						} else {
							map.put("errorMsg", "Failed,please try again later.");
						}
					} else {
						map.put("errorMsg", "Wallet not exist.");
					}
				} else {
					map.put("errorMsg", "User not exist.");
				}

				return "Admin/AssignPhysicalCard";
			}
			return "Admin/Login";
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/UpdatePhysicalCardStatus", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> updatePhysicalCardStatus(@RequestBody RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					walletResponse = userApi.updatePhysicalCardStatus(dto.getId(), dto.getStatus(), dto.getComment());
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Update physical card status");
		adminActivityRepository.save(activity);
		return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/SendMoneyDetails")
	public String getSendMoneyDetails(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				List<SendMoneyDetails> sendMoneyDetails = sendMoneyDetailsRepository.getSendMoneyTransactions();
				session.setAttribute("sendMoney", sendMoneyDetails);

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Send Money Details");
				adminActivityRepository.save(activity);
				return "Admin/SendMoneyDetails";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleCardPhyByPage")
	public String getSinglePhysicalCardByPage(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser cardUser = userApi.findByUserName(page.getUsername());
				List<CardListRespDTO> list = new ArrayList<>();
				if (cardUser != null) {
					MMCards card = mmCardRepository.getPhysicalCardByUser(cardUser);
					if (card != null) {
						double balance = matchMoveApi.getBalance(card.getWallet().getUser());
						CardListRespDTO lists = new CardListRespDTO();
						lists.setName(card.getWallet().getUser().getUserDetail().getFirstName());
						lists.setContactNo(card.getWallet().getUser().getUsername());
						lists.setCreated(card.getCreated() + "");
						lists.setCardId(card.getCardId());
						lists.setEmail(card.getWallet().getUser().getUserDetail().getEmail());
						lists.setStatus(card.getStatus());
						lists.setBalance(balance);
						if (card.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
							lists.setDob(card.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
						} else {
							lists.setDob("NA");
						}
						list.add(lists);
					}
					modelMap.put("cardList", list);
				} else {

				}
				modelMap.put("cardList", list);
				return "Admin/SinglePhysicalCard";
			}
			return "Admin/Login";
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleCardvirtualByPage")
	public String getSingleVirtualCardByPage(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser cardUser = userApi.findByUserName(page.getUsername());
				List<CardListRespDTO> list = new ArrayList<>();
				if (cardUser != null) {
					MMCards card = mmCardRepository.getVirtualCardsByCardUser(cardUser);
					if (card != null) {
						double balance = matchMoveApi.getBalance(card.getWallet().getUser());
						CardListRespDTO lists = new CardListRespDTO();
						lists.setName(card.getWallet().getUser().getUserDetail().getFirstName());
						lists.setContactNo(card.getWallet().getUser().getUsername());
						lists.setCreated(card.getCreated() + "");
						lists.setCardId(card.getCardId());
						lists.setEmail(card.getWallet().getUser().getUserDetail().getEmail());
						lists.setStatus(card.getStatus());
						lists.setBalance(balance);
						if (card.getWallet().getUser().getUserDetail().getDateOfBirth() != null) {
							lists.setDob(card.getWallet().getUser().getUserDetail().getDateOfBirth() + "");
						} else {
							lists.setDob("NA");
						}
						list.add(lists);
					}
					modelMap.put("cardList", list);
				} else {

				}
				modelMap.put("cardList", list);
				return "Admin/SingleVirtualCard";
			}
			return "Admin/Login";
		}
		return "Admin/Login";
	}

	// UPI

	@RequestMapping(value = "/LoadMoney/TransactionsUPI", method = RequestMethod.GET)
	public String getLoadTransactionsUPI(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = mTransactionRepository.getAllLoadTransactionsUPI(oneMonthBack,
							present, pageable);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						if (mTransaction.getAccount() == null) {
							System.err.println("The transaction::::" + mTransaction.getId());
						}
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						if (userAccount == null) {
							System.err.println("The user acc which is missing:::" + mTransaction.getAccount());
							listDTO.setContactNo("NA");
							listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
							listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
							listDTO.setCreated(sdf.format(mTransaction.getCreated()));
							listDTO.setStatus(mTransaction.getStatus().getValue());
							listDTO.setUsername("NA");
							listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
							if (mTransaction.getAuthReferenceNo() == null) {
								listDTO.setRetrivalRefNo("NA");
							} else {
								listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
							}
							if (mTransaction.getCardLoadStatus() == null) {
								listDTO.setCardLoadStatus("NA");
							} else {
								listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
							}
							if (mTransaction.getRemarks() == null) {
								listDTO.setError("NA");
							} else {
								listDTO.setError(mTransaction.getRemarks());
							}
							if (mTransaction.getUpiId() != null) {
								listDTO.setMdexTransactions(mTransaction.getUpiId());
							} else {
								listDTO.setMdexTransactions("NA");
							}
							transactionList.add(listDTO);
						} else {
							listDTO.setContactNo(userAccount.getUsername());
							listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
							listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
							listDTO.setCreated(sdf.format(mTransaction.getCreated()));
							listDTO.setStatus(mTransaction.getStatus().getValue());
							listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
									+ userAccount.getUserDetail().getLastName());
							listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
							if (mTransaction.getAuthReferenceNo() == null) {
								listDTO.setRetrivalRefNo("NA");
							} else {
								listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
							}
							if (mTransaction.getCardLoadStatus() == null) {
								listDTO.setCardLoadStatus("NA");
							} else {
								listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
							}
							if (mTransaction.getRemarks() == null) {
								listDTO.setError("NA");
							} else {
								listDTO.setError(mTransaction.getRemarks());
							}
							if (mTransaction.getUpiId() != null) {
								listDTO.setMdexTransactions(mTransaction.getUpiId());
							} else {
								listDTO.setMdexTransactions("NA");
							}
							transactionList.add(listDTO);
						}
					}

					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Load Money Upi List");
					adminActivityRepository.save(activity);

					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					return "Admin/LoadTransactionsUPI";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/Home";
		}

	}

	@RequestMapping(value = "/LoadMoneyTransactionsUPI", method = RequestMethod.POST)
	public String getLoadTransactionUPI(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					if ("All".equalsIgnoreCase(dto.getStatus())) {
						listTransaction = mTransactionRepository.getAllLoadTransactionsUPI(from, to, pageable);
					} else {
						listTransaction = mTransactionRepository.getLoadTransactionsUPI(from, to, pageable);
					}
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						if (mTransaction.getUpiId() != null) {
							listDTO.setMdexTransactions(mTransaction.getUpiId());
						} else {
							listDTO.setMdexTransactions("NA");
						}
						transactionList.add(listDTO);
					}
					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", dto.getStatus());
					return "Admin/LoadTransactionsUPI";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/LoadTransactionsUPI";
		}

	}

	/* START OF MDEX TRANSACTION API */
	@RequestMapping(value = "/LoadMoney/MdexTransaction", method = RequestMethod.GET)
	public String getMdexTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					List<MTransaction> listTransaction = mTransactionRepository.getAllMdexTransaction();
					System.err.println(listTransaction);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						listDTO.setDescription(mTransaction.getDescription());
						transactionList.add(listDTO);
					}
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Mdex Transaction List");
					adminActivityRepository.save(activity);

					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					return "Admin/MdexTransactionList";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/Home";
		}
	}

	@RequestMapping(value = "/MdexTransaction", method = RequestMethod.POST)
	public String getMdexTransaction(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					if ("All".equalsIgnoreCase(dto.getStatus())) {
						switch (dto.getServiceType()) {
						case "Prepaid":
							MServiceType service1 = mServiceTypeRepository.findServiceTypeByName("Prepaid Topup");

							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service1,
									pageable);
							break;
						case "Postpaid":
							MServiceType service2 = mServiceTypeRepository.findServiceTypeByName("Postpaid Topup");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service2,
									pageable);

							break;
						case "DataCard":
							MServiceType service3 = mServiceTypeRepository.findServiceTypeByName("Prepaid Topup");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service3,
									pageable);

							break;
						case "DTH":
							MServiceType service4 = mServiceTypeRepository.findServiceTypeByName("DTH Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service4,
									pageable);

							break;
						case "LandLine":
							MServiceType service5 = mServiceTypeRepository
									.findServiceTypeByName("Landline Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service5,
									pageable);

							break;
						case "Electricity":
							MServiceType service6 = mServiceTypeRepository
									.findServiceTypeByName("Electricity Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service6,
									pageable);

							break;
						case "Insurance":
							MServiceType service7 = mServiceTypeRepository
									.findServiceTypeByName("Insurance Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service7,
									pageable);

							break;
						case "Gas":
							MServiceType service8 = mServiceTypeRepository.findServiceTypeByName("Gas Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionList(from, to, service8,
									pageable);
							break;
						}
					} else {
						switch (dto.getServiceType()) {
						case "Prepaid":
							MServiceType service1 = mServiceTypeRepository.findServiceTypeByName("Prepaid Topup");

							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service1, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "Postpaid":
							MServiceType service2 = mServiceTypeRepository.findServiceTypeByName("Postpaid Topup");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service2, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "DataCard":
							MServiceType service3 = mServiceTypeRepository.findServiceTypeByName("Prepaid Topup");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service3, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "DTH":
							MServiceType service4 = mServiceTypeRepository.findServiceTypeByName("DTH Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service4, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "LandLine":
							MServiceType service5 = mServiceTypeRepository
									.findServiceTypeByName("Landline Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service5, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "Electricity":
							MServiceType service6 = mServiceTypeRepository
									.findServiceTypeByName("Electricity Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service6, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "Insurance":
							MServiceType service7 = mServiceTypeRepository
									.findServiceTypeByName("Insurance Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service7, Status.valueOf(dto.getStatus()), pageable);
							break;
						case "Gas":
							MServiceType service8 = mServiceTypeRepository.findServiceTypeByName("Gas Bill Payment");
							listTransaction = mTransactionRepository.getAllMdexTransactionListByServiceType(from, to,
									service8, Status.valueOf(dto.getStatus()), pageable);
							break;
						}
					}
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						listDTO.setDescription(mTransaction.getDescription());
						transactionList.add(listDTO);
					}

					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", dto.getStatus());
					return "Admin/MdexTransactionList";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/MdexTransactionList";
		}
	}
	/* END OF MDEX TRANSACTION API */

	@RequestMapping(method = RequestMethod.GET, value = "/SingleUserDetails/{contactNo}")
	public String getSingleUserDetails(@PathVariable(value = "contactNo") String contactNo, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApi.findByUserName(contactNo);
				MMCards card = mmCardRepository.getCardByUserAndStatus(use, Status.Active + "");
				try {
					MatchMoveCreateCardRequest cardFetch = new MatchMoveCreateCardRequest();
					cardFetch.setCardId(card.getCardId());
					cardFetch.setEmail(use.getUserDetail().getEmail());
					cardFetch.setPassword(SecurityUtil.md5(contactNo));
					cardFetch.setUsername(contactNo);
					WalletResponse fetchResponse = matchMoveApi.inquireCard(cardFetch);
					if (fetchResponse.getWalletNumber() != null && fetchResponse != null) {
						String nor = fetchResponse.getWalletNumber();
						System.err.println(nor);
						String aaa = nor.substring(0, 4) + " " + nor.substring(4, 8) + " " + nor.substring(8, 12) + " "
								+ nor.substring(12, 16);
						fetchResponse.setWalletNumber(aaa);
						UserKycResponse walletRespon = new UserKycResponse();
						walletRespon = matchMoveApi.getTransactions(use);
						List<ResponseDTO> resul = new ArrayList<>();
						if (!card.isHasPhysicalCard()) {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						} else {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						}
						session.setAttribute("transactions", resul);
					}
					MService service = mServiceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
					List<MTransactionDTO> credttrns = new ArrayList<>();
					if (service != null) {
						List<MTransaction> creditTransaction = mTransactionRepository
								.getByUserAndService(use.getAccountDetail(), service);
						for (int i = 0; i < 5; i++) {
							MTransactionDTO trx = new MTransactionDTO();
							trx.setAmount(creditTransaction.get(i).getAmount() + "");
							trx.setDescription(creditTransaction.get(i).getDescription());
							trx.setTransactionRefNo(creditTransaction.get(i).getTransactionRefNo());
							trx.setStatus(creditTransaction.get(i).getStatus() + "");
							trx.setCreated(creditTransaction.get(i).getCreated() + "");
							credttrns.add(trx);
						}
					}
					double balance = matchMoveApi.getBalance(use);
					model.addAttribute("Ubalance", balance);
					model.addAttribute("User", use);
					model.addAttribute("credttrx", credttrns);
					model.addAttribute("cardstatus", card.isBlocked());
					session.setAttribute("cardDetail", fetchResponse);
					Double amt = mTransactionRepository.findTotalLoadMoneyByUser(use.getAccountDetail());
					model.addAttribute("Lamt", amt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Admin/UserDetail";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddAgent")
	public String addAgent(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
					} else {
						return "Admin/Login";
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Admin/AddAgent";
		}
		return "Admin/AddAgent";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddAgent")
	public String saveAgentDetails(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @ModelAttribute RegisterDTO dto) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						dto.setUsername(dto.getEmail());
						dto.setPassword("123456");
						RegisterError registerError = registerValidation.validateAgent(dto);
						if (registerError.isValid()) {
							dto.setUserType(UserType.Agent);
							if (dto.getPanCardImage() != null) {
								String panCardImgPath = CommonUtil.uploadImage(dto.getPanCardImage(),
										dto.getContactNo(), "Pan");
								dto.setPanCardImagePath(panCardImgPath);
							}
							if (dto.getAadharImage1() != null) {
								String aadharCardImgPath = CommonUtil.uploadImage(dto.getAadharImage1(),
										dto.getContactNo(), "Aadhar");
								dto.setAadharImagePath1(aadharCardImgPath);
							}
							if (dto.getAadharImage2() != null) {
								String aadharCardImgPath = CommonUtil.uploadImage(dto.getAadharImage2(),
										dto.getContactNo(), "Aadhar");
								dto.setAadharImagePath2(aadharCardImgPath);
							}
							agentApi.saveAgent(dto);
							modelMap.addAttribute("success", "Agent registration is successful.");

							AdminActivity activity = new AdminActivity();
							activity.setActivityName("Add Agent");
							adminActivityRepository.save(activity);
						} else {
							modelMap.addAttribute("error", registerError.getMessage());
						}
					} else {
						return "Admin/Login";
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Admin/Login";
		}
		return "/Admin/AddAgent";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AgentList")
	public String getAgentListDetails(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Agent List");
						adminActivityRepository.save(activity);
						List<MAgentDTO> mAgentDTOs = agentApi.getAllAgentDetail();
						if (mAgentDTOs != null && !mAgentDTOs.isEmpty()) {
							modelMap.addAttribute("agentList", mAgentDTOs);
						}
					} else {
						return "Admin/Login";
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "/Admin/AgentList";
		}
		return "/Admin/AgentList";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AgentPrefundList")
	public String showAgentPrefundRequest(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						AdminAccessIdentifier accessing = adminAccessIdentifierRepository
								.getallaccess(userSession.getUser());

						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Agent prefund List");
						adminActivityRepository.save(activity);
						List<AgentPrefundDTO> agentPrefundDTOs = agentApi.getAgentPrefundDetails();
						if (agentPrefundDTOs != null && !agentPrefundDTOs.isEmpty()) {
							modelMap.addAttribute("agentPrefundRequest", agentPrefundDTOs);
						}
						modelMap.addAttribute("addUserBool", accessing.isAddUser());
					} else {
						return "Admin/Login";
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Admin/AgentPrefund";
		}
		return "Admin/AgentPrefundRequest";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ApproveOrRejectPrefundRequest")
	public ResponseEntity<PrefundRequestDTO> registerNormalUser(@RequestBody AgentPrefundDTO dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) {
		PrefundRequestDTO responseDTO = new PrefundRequestDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						responseDTO = agentApi.updateAgentPrefundStatus(dto);
						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Agent Prefund Request List");
						adminActivityRepository.save(activity);
					} else {
						responseDTO.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						responseDTO.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					}

				} else {
					responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
					responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
				responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			responseDTO.setCode(ResponseStatus.BAD_REQUEST.getValue());
			responseDTO.setMessage(ResponseStatus.BAD_REQUEST.getKey());
		}
		return new ResponseEntity<PrefundRequestDTO>(responseDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AgentTransactionList")
	public String getAgentTransactionList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<AgentTransactionListDTO> agentTransactionListDTOs = agentApi.getAgentTransactionList();
						modelMap.addAttribute("agentTransactionList", agentTransactionListDTOs);
						AdminActivity activity = new AdminActivity();
						activity.setActivityName("Agent Transaction List");
						adminActivityRepository.save(activity);
					} else {
						return "Admin/Login";
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "/Admin/AgentTransactionList";
		}
		return "/Admin/AgentTransactionList";

	}

	@RequestMapping(value = "/KYCRejectedList", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> fetchRejectedList(@ModelAttribute PagingDTO page, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				try {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					Page<MKycDetail> agent = userApi.findAllKycRequestRejected(page);
					List<MCorporateAgentDTO> list = new ArrayList<>();
					for (MKycDetail mAgent : agent) {
						MCorporateAgentDTO agnt = new MCorporateAgentDTO();
						agnt.setFirstName(mAgent.getUser().getUserDetail().getFirstName() + " "
								+ mAgent.getUser().getUserDetail().getLastName());
						agnt.setEmail(mAgent.getUser().getUserDetail().getEmail());
						agnt.setContactNo(mAgent.getUser().getUserDetail().getContactNo());
						agnt.setImage(mAgent.getIdImage());
						agnt.setImage2(mAgent.getIdImage1());
						agnt.setAddress1(mAgent.getAddress1());
						agnt.setAddress2(mAgent.getAddress2());
						agnt.setIdType(mAgent.getIdType());
						agnt.setIdNumber(mAgent.getIdNumber());
						agnt.setState(mAgent.getState());
						agnt.setCountry(mAgent.getCountry());
						agnt.setPinCode(mAgent.getPinCode());
						agnt.setCity(mAgent.getCity());
						agnt.setAccountId(mAgent.getUser().getAccountDetail().getId() + "");
						agnt.setUserId(mAgent.getId() + "");
						agnt.setAccountType(mAgent.getUser().getAccountDetail().getAccountType().getCode());
						agnt.setDob(mAgent.getUser().getUserDetail().getDateOfBirth() + "");
						agnt.setCreated(mAgent.getCreated());
						if (mAgent.getRejectionReason() == null) {
							agnt.setRejectionReason("NA");
						} else {
							agnt.setRejectionReason(mAgent.getRejectionReason());
						}
						list.add(agnt);
					}
					walletResponse.setJsonArray(list);
					walletResponse.setDate(page.getDaterange());
					walletResponse.setTotalPages(agent.getTotalPages());

					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Kyc Rejected List");
					adminActivityRepository.save(activity);

					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<ResponseDTO>(walletResponse, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/KycRequestRejected", method = RequestMethod.GET)
	public String fetchKycRequestRejected(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				return "Admin/KYCRejectedList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/fetchBulkUploadList", method = RequestMethod.GET)
	public String fetchBulkUploadList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<CorporateFileCatalogue> fileCatalogue = corporateFileCatalogueRepository.getUnapprovedList();
				model.addAttribute("CatalogueList", fileCatalogue);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Bulk upload List");
				adminActivityRepository.save(activity);
				model.addAttribute("addUserBool", accessing.isAddUser());
				return "Admin/BulkUploadApproval";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/fetchBulkUploadListGroup", method = RequestMethod.GET)
	public String fetchBulkUploadListGroup(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<GroupFileCatalogue> fileCatalogue = groupFileCatalogueRepository.getUnapprovedListGroup();
				model.addAttribute("CatalogueList", fileCatalogue);
				model.addAttribute("addUserBool", accessing.isAddUser());
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Bulk upload List");
				adminActivityRepository.save(activity);

				return "Admin/BulkUploadApprovalGroup";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/fetchBulkUploadListSms", method = RequestMethod.GET)
	public String fetchBulkUploadListSms(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<GroupFileCatalogue> fileCatalogue = groupFileCatalogueRepository.getUnapprovedListSms();
				model.addAttribute("CatalogueList", fileCatalogue);
				model.addAttribute("addUserBool", accessing.isAddUser());

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Bulk SMS upload List");
				adminActivityRepository.save(activity);

				return "Admin/BulkUploadApprovalSms";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/ReviewCorporate", method = RequestMethod.POST)
	public String reviewCorporate(@ModelAttribute ReviewStatusDTO reviewStatus, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println("hi m hererer");
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Review Corporate");
				adminActivityRepository.save(activity);
				CorporateFileCatalogue fileCatalogue = corporateFileCatalogueRepository
						.findOne(Long.parseLong(reviewStatus.getFileId()));
				if (reviewStatus.getAction().equalsIgnoreCase("Accept")) {
					fileCatalogue.setReviewStatus(true);
					corporateFileCatalogueRepository.save(fileCatalogue);
				} else {
					fileCatalogue.setReviewStatus(false);
					fileCatalogue.setFileRejectionStatus(true);
					corporateFileCatalogueRepository.save(fileCatalogue);
				}
				List<CorporateFileCatalogue> catList = corporateFileCatalogueRepository.getUnapprovedList();
				model.addAttribute("CatalogueList", catList);

				return "redirect:/Admin/fetchBulkUploadList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "redirect:/Admin/Home";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "redirect:/Admin/Home";
		}
	}

	@RequestMapping(value = "/ReviewGroup", method = RequestMethod.POST)
	public String reviewGroup(@ModelAttribute ReviewStatusDTO reviewStatus, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println("hi m hererer");
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Review Group");
				adminActivityRepository.save(activity);
				GroupFileCatalogue fileCatalogue = groupFileCatalogueRepository
						.findOne(Long.parseLong(reviewStatus.getFileId()));
				if (reviewStatus.getAction().equalsIgnoreCase("Accept")) {
					fileCatalogue.setReviewStatus(true);
					groupFileCatalogueRepository.save(fileCatalogue);
				} else {
					fileCatalogue.setReviewStatus(false);
					fileCatalogue.setFileRejectionStatus(true);
					groupFileCatalogueRepository.save(fileCatalogue);
				}
				List<GroupFileCatalogue> catList = groupFileCatalogueRepository.getUnapprovedListGroup();
				model.addAttribute("CatalogueList", catList);

				return "Admin/BulkUploadApprovalGroup";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/ReviewSMS", method = RequestMethod.POST)
	public String reviewSms(@ModelAttribute ReviewStatusDTO reviewStatus, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println("hi m hererer");
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Review SMS");
				adminActivityRepository.save(activity);
				GroupFileCatalogue fileCatalogue = groupFileCatalogueRepository
						.findOne(Long.parseLong(reviewStatus.getFileId()));
				if (reviewStatus.getAction().equalsIgnoreCase("Accept")) {
					fileCatalogue.setReviewStatus(true);
					groupFileCatalogueRepository.save(fileCatalogue);
				} else {
					fileCatalogue.setReviewStatus(false);
					fileCatalogue.setFileRejectionStatus(true);
					groupFileCatalogueRepository.save(fileCatalogue);
				}
				List<GroupFileCatalogue> catList = groupFileCatalogueRepository.getUnapprovedListSms();
				model.addAttribute("CatalogueList", catList);

				return "Admin/BulkUploadApprovalSms";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/PrefundRequests", method = RequestMethod.GET)
	public String prefundRequests(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println("hi m hererer");
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<CorporatePrefundHistory> listCorp = corporatePrefundHistoryRepository.findAllRequests();
				model.addAttribute("prefundList", listCorp);
				model.addAttribute("addUserBool", accessing.isAddUser());
				return "Corporate/CorporatePrefundList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/ReviewCorporatePrefund", method = RequestMethod.POST)
	public String reviewCorporatePrefund(@ModelAttribute ReviewStatusDTO reviewStatus, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println("hi m hererer");
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Corporate Prefund Review");
				adminActivityRepository.save(activity);
				CorporatePrefundHistory corpPrefund = corporatePrefundHistoryRepository
						.findOne(Long.parseLong(reviewStatus.getFileId()));
				if (reviewStatus.getAction().equalsIgnoreCase("Accept")) {
					corpPrefund.setReviewStatus(true);
					transactionApi.doPrefundTransaction(corpPrefund.getCorporate(), corpPrefund.getAmount());
					corpPrefund.setPrefundStatus(true);
					corporatePrefundHistoryRepository.save(corpPrefund);
				} else {
					corpPrefund.setReviewStatus(true);
					corpPrefund.setPrefundStatus(false);
					corporatePrefundHistoryRepository.save(corpPrefund);
				}
				List<CorporatePrefundHistory> catList = corporatePrefundHistoryRepository.findAllRequests();
				model.addAttribute("prefundList", catList);

				return "Corporate/CorporatePrefundList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/Imps/Transactions", method = RequestMethod.GET)
	public String getImpsTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					MService service = mServiceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
					List<MTransaction> listTransaction = mTransactionRepository.getAllImpsTransactions(oneMonthBack,
							present, pageable, service);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						listDTO.setCommission(mTransaction.getCommissionEarned() + "");
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					Double commission = mTransactionRepository.findTotalImpsCommission(service);
					map.put("Commis", commission);
					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Imps Transactions");
					adminActivityRepository.save(activity);

					return "Admin/ImpsTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/ImpsTransactions";
		}
	}

	@RequestMapping(value = "/ImpsTransactions", method = RequestMethod.POST)
	public String getImpsTransaction(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					MService service = mServiceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
					if ("Success".equalsIgnoreCase(dto.getStatus())) {
						listTransaction = mTransactionRepository.getAllImpsTransactionsByStatus(from, to, pageable,
								service, Status.Success);
					} else {
						if ("Reversed".equalsIgnoreCase(dto.getStatus())) {

							listTransaction = mTransactionRepository.getAllImpsTransactionsByStatus(from, to, pageable,
									service, Status.Reversed);
						} else if ("Initiated".equalsIgnoreCase(dto.getStatus())) {
							listTransaction = mTransactionRepository.getAllImpsTransactionsByStatus(from, to, pageable,
									service, Status.Initiated);
						} else {
							listTransaction = mTransactionRepository.getAllImpsTransactions(from, to, pageable,
									service);

						}
					}
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						listDTO.setCommission(mTransaction.getCommissionEarned() + "");
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					map.addAttribute("transactions", transactionList);
					Double commission = mTransactionRepository.findTotalImpsCommission(service);
					map.put("Commis", commission);
					map.put("Lstatus", dto.getStatus());
					map.put("dat", dto.getToDate());
					return "Admin/ImpsTransactions";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/ImpsTransactions";
		}

	}

	@RequestMapping(value = "/GetLineGraphData", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Object> getLineGraphData(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			HttpSession session) {
		ResponseDTO result = new ResponseDTO();
		List<Object> fullList = new ArrayList<>();

		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -2);
					String fromDate = df2.format(cal.getTime());
					Date date1 = sdformat.parse(fromDate + " 23:59:59");
					Date date2 = sdformat.parse(dateText + " 00:00:00");
					List<MTransaction> transactionList = mTransactionRepository.getDataForAdminLineGraph(date1, date2);
					for (MTransaction mTransaction : transactionList) {
						List<Object> objectList = new ArrayList<>();
						objectList.add(mTransaction.getCreated().getTime());
						objectList.add(mTransaction.getAmount());
						fullList.add(objectList);
					}
					System.err.println(fullList);
				} catch (Exception e) {
					e.printStackTrace();
					result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
					result.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}
		} else {
			result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
		}
		return new ResponseEntity<Object>(fullList, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/DownloadSheet/{status}/{type}")
	public String getDownloadSheet(@RequestParam String dateRange, @PathVariable(value = "status") String status,
			@PathVariable(value = "type") String type) {
		Date from = null;
		Date to = null;
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		String filename = ("Card_Records" + System.currentTimeMillis() + ".xls");
		String[] daterange = null;
		try {
			if (dateRange.length() != 0) {
				String aaaaa = dateRange.substring(0, 10);
				String baaaa = dateRange.substring(13);
				from = sdformat.parse(aaaaa + " 00:00:00");
				to = sdformat.parse(baaaa + " 00:00:00");
			} else {
				daterange = CommonUtil.getDefaultDateRange();
				from = df2.parse(daterange[0]);
				to = df2.parse(daterange[1]);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<MMCardsDTO> dto = new ArrayList<MMCardsDTO>();
		switch (type) {
		case "virtualCards":
			List<MMCards> cardsdto = mmCardRepository.getAllCards(false, from, to, status);
			for (MMCards cards : cardsdto) {
				double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
				MMCardsDTO card = new MMCardsDTO();
				card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
				card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
				card.setBalance(String.valueOf(balance));
				card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
				card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				dto.add(card);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Download sheet virtualcard");
				adminActivityRepository.save(activity);
			}
			break;
		case "physicalCards":
			List<MMCards> cardsdto1 = mmCardRepository.getAllCards(true, from, to, status);
			for (MMCards cards : cardsdto1) {
				double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
				MMCardsDTO card = new MMCardsDTO();
				card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
				card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
				card.setBalance(String.valueOf(balance));
				card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
				card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				dto.add(card);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Download sheet physical card");
				adminActivityRepository.save(activity);
			}
			break;
		}
		String fileName = ExcelWriter.makeExcelSheet(dto, filename);
		return fileName;
	}

	/**
	 * BAR GRAPH
	 */
	@RequestMapping(value = "/GetBarGraph", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Object> getBarGraph(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			HttpSession session) {
		ResponseDTO result = new ResponseDTO();
		List<Object> fullList = new ArrayList<>();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);

					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -2);
					String fromDate = df2.format(cal.getTime());
					Date date1 = sdformat.parse(fromDate + " 23:59:59");
					Date date2 = sdformat.parse(dateText + " 00:00:00");
					List<MTransaction> transactionList = mTransactionRepository.getDataForAdminLineGraph(date1, date2);
					for (MTransaction mTransaction : transactionList) {
						List<Object> objectList = new ArrayList<>();
						objectList.add(mTransaction.getCreated().getTime());
						objectList.add(mTransaction.getAmount());
						fullList.add(objectList);
					}
					System.err.println(fullList);
				} catch (Exception e) {
					e.printStackTrace();
					result.setCode(ResponseStatus.EXCEPTION_OCCURED.getValue());
					result.setMessage(ResponseStatus.EXCEPTION_OCCURED.getKey());
				}
			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}
		} else {
			result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
		}
		return new ResponseEntity<Object>(fullList, HttpStatus.OK);
	}

	/**
	 * DOWLOAD AS CSV
	 */

	@RequestMapping(value = "/download/csv", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getDownloadBulkTransferFile(@ModelAttribute DownloadCsv downloadCsv,
			HttpServletRequest request, HttpServletResponse res, HttpSession session) throws IOException {

		ResponseDTO response = new ResponseDTO();
		Date from = null;
		Date to = null;
		String filename = ("Card_Records" + System.currentTimeMillis() + ".xls");
		DownloadHistory download = new DownloadHistory();
		download.setStatus(Status.Pending);
		download.setFileExtension(filename);
		DownloadHistory dwHis = downloadHistoryRepository.save(download);
		System.err.println("the req::" + downloadCsv);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		String[] daterange = null;
		try {
			if (downloadCsv.getDateRange().length() != 0) {
				String aaaaa = downloadCsv.getDateRange().substring(0, 10);
				String baaaa = downloadCsv.getDateRange().substring(13);
				from = sdformat.parse(aaaaa + " 00:00:00");
				to = sdformat.parse(baaaa + " 00:00:00");
			} else {
				daterange = CommonUtil.getDefaultDateRange();
				from = df2.parse(daterange[0]);
				to = df2.parse(daterange[1]);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<MMCardsDTO> dto = new ArrayList<MMCardsDTO>();
		switch (downloadCsv.getRequestType()) {
		case "virtualCards":
			List<MMCards> cardsdto = mmCardRepository.getAllCards(false, from, to, downloadCsv.getStatus());

			for (MMCards cards : cardsdto) {
				double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				try {
					cardRequest.setPassword(SecurityUtil.md5(cards.getWallet().getUser().getUsername()));
					cardRequest.setUsername(cards.getWallet().getUser().getUsername());
				} catch (Exception e) {
					e.printStackTrace();
				}
				cardRequest.setCardId(cards.getCardId());
				WalletResponse walletResp = matchMoveApi.inquireCard(cardRequest);
				MMCardsDTO card = new MMCardsDTO();
				card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
				card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
				card.setBalance(String.valueOf(balance));
				card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
				card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				card.setCardNumber(walletResp.getWalletNumber());
				card.setDate(sdformat.format(cards.getCreated()));
				dto.add(card);

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Download csv virtualcard");
				adminActivityRepository.save(activity);
			}
			break;
		case "physicalCards":
			List<MMCards> cardsdto1 = mmCardRepository.getAllCards(true, from, to, downloadCsv.getStatus());
			for (MMCards cards : cardsdto1) {
				MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
				cardRequest.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				try {
					cardRequest.setPassword(SecurityUtil.md5(cards.getWallet().getUser().getUsername()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				cardRequest.setCardId(cards.getCardId());
				WalletResponse walletResp = matchMoveApi.inquireCard(cardRequest);

				double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
				MMCardsDTO card = new MMCardsDTO();
				card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
				card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
				card.setBalance(String.valueOf(balance));
				card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
				card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
				card.setCardNumber(walletResp.getWalletNumber());
				card.setDate(sdformat.format(cards.getCreated()));
				dto.add(card);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Download csv physical card");
				adminActivityRepository.save(activity);
			}
			break;
		}
		dwHis.setStatus(Status.Success);
		downloadHistoryRepository.save(dwHis);
		String fileName = ExcelWriter.makeExcelSheet(dto, filename);
		System.err.println(fileName);
		@SuppressWarnings("deprecation")
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		response.setCode("\\resources\\excelSheets\\" + fileName);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/userDetails/{contactNo}", method = RequestMethod.GET)
    public String getUserDataListBasedOnContactNo(HttpServletRequest request, HttpSession session, ModelMap model,
            @PathVariable(value = "contactNo") String contactNo, ModelMap map, @ModelAttribute RequestDTO dto) {
        try {
            String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
            UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
            if (userSession != null) {
                MUser user = userSession.getUser();
                if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
                        && user.getAuthority().contains(Authorities.AUTHENTICATED)) {
                    persistingSessionRegistry.refreshLastRequest(sessionId);
                    AdminAccessIdentifier access = adminAccessIdentifierRepository.getallaccess(userSession.getUser());
                    MUser mUser = userApi.findByUserName(contactNo);
                    if (mUser != null) {
                        model.put("userdetails", mUser);
                        MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
                        WalletResponse walletResponse = new WalletResponse();
                        MMCards cards = cardRepository.getVirtualCardsByCardUser(mUser);
                        if (cards != null) {
                            createCardRequest.setCardId(cards.getCardId());
                            createCardRequest.setEmail(mUser.getUserDetail().getEmail());
                            createCardRequest.setPassword(SecurityUtil.md5(mUser.getUsername()));
                            createCardRequest.setUsername(mUser.getUsername());
                            walletResponse.setStatus(cards.getStatus());
                            walletResponse = matchMoveApi.inquireCard(createCardRequest);
                            if (walletResponse != null
                                    && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
                                double balance = matchMoveApi.getBalanceApp(userSession.getUser());
                                model.put("cardDetailsVir", walletResponse);
                                model.put("cardBalanceVir", balance);
                            }
                        }
                        MMCards card = cardRepository.getPhysicalCardByUser(mUser);
                        if (card != null) {
                            createCardRequest.setCardId(card.getCardId());
                            createCardRequest.setEmail(mUser.getUserDetail().getEmail());
                            createCardRequest.setPassword(SecurityUtil.md5(mUser.getUsername()));
                            WalletResponse walletResponse2 = matchMoveApi.inquireCard(createCardRequest);
                            if (walletResponse2 != null
                                    && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse2.getCode())) {
                                double balance = matchMoveApi.getBalanceApp(userSession.getUser());
                                model.put("physicalCard", walletResponse2);
                                model.put("phyBalance", balance);
                            }
                        }
                        UserKycResponse userKycResponse = matchMoveApi.getTransactionsCustomAdmin(mUser);
                        if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKycResponse.getCode())) {
                            model.put("virtualCardTrans", userKycResponse.getCustomVirtualTransaction());
                            model.put("physicalCardTrans", userKycResponse.getCustomPhysicalTransaction());
                        }
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MONTH, -1);
                        Date oneMonthBack = cal.getTime();
                        Calendar calPresent = Calendar.getInstance();
                        Date present = calPresent.getTime();
                        List<MTransaction> listTransaction = null;
                        MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.RAZORPAY_SERVICE);
                        listTransaction = mTransactionRepository.getAllServiceTransactions(oneMonthBack, present,
                                mUser.getAccountDetail(), service);
                        List<TransactionListDTO> transactionList = new ArrayList<>();
                        for (MTransaction mTransaction : listTransaction) {
                            TransactionListDTO listDTO = new TransactionListDTO();
                            MUser userAccount = mUserRespository.getUserFromAccountId(mTransaction.getAccount());
                            if (userAccount != null) {
                                listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
                                listDTO.setDescription(mTransaction.getDescription());
                                listDTO.setStatus(mTransaction.getStatus().getValue());
                                listDTO.setCreated(CommonUtil.DATEFORMATTER1.format(mTransaction.getCreated()));
                                listDTO.setTransactionType(String.valueOf(mTransaction.isDebit()));
                                listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
                                listDTO.setRetrivalRefNo(mTransaction.getRetrivalReferenceNo());
                                listDTO.setAuthRefNo(mTransaction.getAuthReferenceNo());
                            }
                            transactionList.add(listDTO);
                        }
                        map.addAttribute("transactions", transactionList);
                    }
                    MKycDetail kyc = mKycRepository.findByUser(mUser);
                    if (kyc != null) {
                        model.put("kycDetails", kyc);
                    }
                    model.addAttribute("addUserBool", access.isAddUser());
                    AdminActivity activity = new AdminActivity();
                    activity.setActivityName("User Details");
                    adminActivityRepository.save(activity);
                    map.put("Lstatus", "All");
                    return "Admin/userDetails";
                }
            } else {
            	return "Admin/Login";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/Admin/Home";
    }

	@RequestMapping(value = "/userDetails", method = RequestMethod.POST)
	public String getUserDetails(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");

					String contactNo = dto.getContactNo();
					MUser mUser = userApi.findByUserName(contactNo);
					if (mUser != null) {
						model.put("userdetails", mUser);

						MatchMoveCreateCardRequest createCardRequest = new MatchMoveCreateCardRequest();
						WalletResponse walletResponse = new WalletResponse();
						MMCards cards = cardRepository.getVirtualCardsByCardUser(mUser);
						if (cards != null) {
							createCardRequest.setCardId(cards.getCardId());
							createCardRequest.setEmail(mUser.getUserDetail().getEmail());
							createCardRequest.setUsername(mUser.getUsername());
							createCardRequest.setPassword(SecurityUtil.md5(mUser.getUsername()));
							walletResponse.setStatus(cards.getStatus());
							walletResponse = matchMoveApi.inquireCard(createCardRequest);
							if (walletResponse != null && ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(walletResponse.getCode())) {
								double balance = matchMoveApi.getBalanceApp(userSession.getUser());
								model.put("cardDetailsVir", walletResponse);
								model.put("cardBalanceVir", balance);
							}
						}
						MMCards card = cardRepository.getPhysicalCardByUser(mUser);
						if (card != null) {
							createCardRequest.setCardId(card.getCardId());
							createCardRequest.setEmail(mUser.getUserDetail().getEmail());
							createCardRequest.setPassword(SecurityUtil.md5(mUser.getUsername()));
							WalletResponse walletResponse2 = matchMoveApi.inquireCard(createCardRequest);
							if (walletResponse2 != null && walletResponse2.getCode().equalsIgnoreCase("S00")) {
								double balance = matchMoveApi.getBalanceApp(userSession.getUser());
								model.put("physicalCard", walletResponse2);
								model.put("phyBalance", balance);
							}
						}

						UserKycResponse userKycResponse = matchMoveApi.getTransactionsCustomAdmin(mUser);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(userKycResponse.getCode())) {
							model.put("virtualCardTrans", userKycResponse.getCustomVirtualTransaction());
							model.put("physicalCardTrans", userKycResponse.getCustomPhysicalTransaction());

						}

						List<MTransaction> listTransaction = null;
						if ("All".equalsIgnoreCase(dto.getServiceType())) {
							Status status = Status.Success;
							if ("Success".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Success;
								listTransaction = mTransactionRepository.getAllSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status);
							} else if ("Failed".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Failed;
								listTransaction = mTransactionRepository.getAllSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status);
							} else if ("Initiated".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Initiated;
								listTransaction = mTransactionRepository.getAllSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status);
							} else {
								listTransaction = mTransactionRepository.getAllLoadTransactions(from, to,
										mUser.getAccountDetail());
							}
						} else {
							MService service = mServiceRepository.findServiceByCode(dto.getServiceType());
							Status status = Status.Success;
							if ("Success".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Success;
								listTransaction = mTransactionRepository.getSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status, service);
							} else if ("Failed".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Failed;
								listTransaction = mTransactionRepository.getSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status, service);
							} else if ("Initiated".equalsIgnoreCase(dto.getStatus())) {
								status = Status.Initiated;
								listTransaction = mTransactionRepository.getSuccessLoadTransactions(from, to,
										mUser.getAccountDetail(), status, service);
							} else {
								listTransaction = mTransactionRepository.getAllServiceTransactions(from, to,
										mUser.getAccountDetail(), service);
							}
						}

						List<TransactionListDTO> transactionList = new ArrayList<>();
						for (MTransaction mTransaction : listTransaction) {
							TransactionListDTO listDTO = new TransactionListDTO();
							MUser userAccount = mUserRespository.getUserFromAccountId(mTransaction.getAccount());
							if (userAccount != null) {
								listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
								listDTO.setDescription(mTransaction.getDescription());
								listDTO.setStatus(mTransaction.getStatus().getValue());
								listDTO.setCreated(sdf.format(mTransaction.getCreated()));
								listDTO.setTransactionType(String.valueOf(mTransaction.isDebit()));
							}
							transactionList.add(listDTO);
						}
						map.addAttribute("transactions", transactionList);
					}
					MKycDetail kyc = mKycRepository.findByUser(mUser);
					if (kyc != null) {
						model.put("kycDetails", kyc);
					}
					map.put("Lstatus", dto.getStatus());
					map.put("dat", dto.getToDate());
					map.put("Type", dto.getServiceType());
					map.put("contactNo", contactNo);
					map.addAttribute("addUserBool", accessing.isAddUser());
					return "Admin/userDetails";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/userDetails";
	}

	/* ==================END===================== */
	@RequestMapping(value = "/downloadStatus", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getDownloadStatus() throws IOException {
		ResponseDTO response = new ResponseDTO();
		List<DownloadHistory> list = downloadHistoryRepository.getFileNameByOrder();
		Long statusCount = downloadHistoryRepository.getPendingCount();
		response.setPendingStatusCount(statusCount);
		response.setDownloadHistory(list);
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/AssignPhyCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<WalletResponse> assignCard(@RequestBody RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		WalletResponse walletResponse = new WalletResponse();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					PhysicalCardDetails phyDetails = physicalCardDetailRepository.findOne(Long.parseLong(dto.getId()));
					walletResponse = matchMoveApi.assignPhysicalCard(phyDetails.getWallet().getUser(), dto.getKitNo());
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<WalletResponse>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleUserCustom")
	public ResponseEntity<ResponseDTO> getSingleUserCustom(@ModelAttribute RequestDTO dto, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				switch (dto.getRequestType()) {
				case "name":
					List<MUser> userDetails = mUserRespository.getFirstName(dto.getUserName());
					resp.setUserList(userDetails);
					break;
				case "mobile":
					List<MUser> userList = mUserRespository.getBymobileNumber(dto.getUserName());
					resp.setUserList(userList);
					break;
				case "email":
					List<MUser> listUserDetails = mUserRespository.getByEmail(dto.getUserName());
					resp.setUserList(listUserDetails);
					break;
				default:
					break;
				}
				MUser use = userApi.findByUserName(dto.getUserName());
				session.setAttribute("user", use);
				List<MMCardsDTO> cards = new ArrayList<>();
				MMCardsDTO car = new MMCardsDTO();
				car.setContactNO(use.getUserDetail().getContactNo());
				car.setEmail(use.getUserDetail().getEmail());
				car.setUserName(use.getUsername());
				car.setStatus(use.getMobileStatus() + "");
				car.setIssueDate(use.getCreated() + "");
				car.setFirstName(use.getUserDetail().getFirstName());
				car.setLastName(use.getUserDetail().getLastName());
				if (use.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
					car.setRole(true);
				} else {
					car.setRole(false);
				}
				car.setAccountType(use.getAccountDetail().getAccountType().getCode());
				if (use.getMobileToken() == null) {
					car.setMobileToken("NA");
				} else if ("".equalsIgnoreCase(use.getMobileToken())) {
					car.setMobileToken("NA");
				} else {
					car.setMobileToken(use.getMobileToken());
				}
				car.setAuthority(use.getAuthority());
				if (use.getUserDetail().getDateOfBirth() != null) {
					car.setDob(use.getUserDetail().getDateOfBirth() + "");
				} else {
					car.setDob("NA");
				}
				cards.add(car);
				session.setAttribute("userList", cards);
				resp.setJsonArray(cards);
				resp.setTotalPages(0);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/AddMerchant", method = RequestMethod.POST)
	public String addMerchant(@ModelAttribute AddMerchantDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Add Merchant");
				adminActivityRepository.save(activity);

				RegisterError registerError = registerValidation.validateMerchant(dto);
				if (registerError.isValid()) {
					try {
						String filePath = CommonUtil.uploadPdf(dto.getImage(),
								dto.getMerchantName() + System.currentTimeMillis(), "MerchantDocs");
						MerchantDetails mercDetails = new MerchantDetails();
						mercDetails.setMerchantName(dto.getMerchantName());
						mercDetails.setBcEmail(dto.getBusinessCorrespondentEmail());
						mercDetails.setBcNumber(dto.getBusinessCorrespondentNumber());
						mercDetails.setBcName(dto.getBusinessCorrespondentName());
						mercDetails.setFilePath(filePath);
						merchantDetailsRepository.save(mercDetails);
						List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
						session.setAttribute("cardList", merchantDetails);
						map.put("msg", "Merchant is Added");
						return "Admin/MerchantList";
					} catch (Exception e) {
						e.printStackTrace();
						walletResponse.setMessage("Service Unavailable");
						map.put("msg", "Failed to Add Merchant.");
						walletResponse.setCode(ResponseStatus.FAILURE.getValue());
						return "Admin/MerchantList";
					}
				} else {
					System.err.println(registerError.getMessage());
					walletResponse.setMessage(registerError.getMessage());
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					map.put("msg", registerError.getMessage());
					return "Admin/MerchantList";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CashBack")
	public String cashBack(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
				modelMap.put("merchantList", merchantDetails);
				return "Admin/Casback";
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/fetchMerchantList", method = RequestMethod.GET)
	public String fetchMerchantList(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Merchant List");
				adminActivityRepository.save(activity);
				try {
					List<MerchantDetails> listMerchant = merchantDetailsRepository.getAllMerchants();
					model.addAttribute("cardList", listMerchant);
					return "Admin/MerchantList";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/MerchantList";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/AddCMerchant", method = RequestMethod.GET)
	public String addCMerchant(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					return "Admin/AddCashierMerchant";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/AddCashierMerchant";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/CashBackMod", method = RequestMethod.POST)
	public String cashBackMod(@ModelAttribute CashBackDTO dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser cashbackUser = userApi.findByUserName(dto.getMobile());
				if (cashbackUser != null) {
					CashBackError errorDto = cashBackValidations.validateCashBack(cashbackUser, dto.getMerchantName());
					if (errorDto.isValid()) {
						WalletResponse walletResponseCh = matchMoveApi.initiateLoadFundsToMMWalletTopup(cashbackUser,
								dto.getAmount());
						if (walletResponseCh.getCode().equalsIgnoreCase("S00")) {
							MMCards phyCard = cardRepository.getPhysicalCardByUser(cashbackUser);
							if (phyCard != null) {
								if (phyCard.getStatus().equalsIgnoreCase("Active")) {
									WalletResponse cardTransferResposne = matchMoveApi
											.transferFundsToMMCard(cashbackUser, dto.getAmount(), phyCard.getCardId());
									if (!cardTransferResposne.getCode().equalsIgnoreCase("S00")) {
										map.put("message", "Transfer to Card Failed");
										List<MerchantDetails> merchantDetails = merchantDetailsRepository
												.getAllMerchants();
										map.put("merchantList", merchantDetails);
										return "Admin/Casback";
									}
								} else {
									List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
									map.put("merchantList", merchantDetails);
									map.put("message", "User's physical card is not Active");
									return "Admin/Casback";
								}
							} else {
								List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
								map.put("merchantList", merchantDetails);
								map.put("message", "No Physical Card Found For This User");
								return "Admin/Casback";
							}
						} else {
							List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
							map.put("merchantList", merchantDetails);
							map.put("message", "Failed to credit Wallet");
							return "Admin/Casback";
						}
						MerchantDetails mercDetails = merchantDetailsRepository
								.findByMerchantName(dto.getMerchantName());
						dto.setUser(cashbackUser);
						MTransaction cashBackTransaction = transactionApi.cashBackTransaction(dto);
						CashBack cash = new CashBack();
						cash.setAmount(Double.parseDouble(dto.getAmount()));
						cash.setMerchantDetails(mercDetails);
						cash.setTransaction(cashBackTransaction);
						cash.setUser(cashbackUser);
						cashBackRepository.save(cash);
						map.put("message", "CashBack Added to Customer's Card");
						List<MerchantDetails> merchantDetails = merchantDetailsRepository.getAllMerchants();
						map.put("merchantList", merchantDetails);

						senderApi.sendUserSMSCashback(SMSAccount.PAYQWIK_PROMOTIONAL_MESSAGE, SMSTemplete.SMS_CASHBACK,
								cashbackUser, dto.getMerchantName(), cashBackTransaction.getAmount());
						return "Admin/Casback";
					}
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
		return "Admin/Login";

	}

	@RequestMapping(value = "/Cashback/Transactions", method = RequestMethod.GET)
	public String cashBackTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					MService service = mServiceRepository.findServiceByCode("CASH");
					List<MTransaction> listTransaction = mTransactionRepository.getAllCashBackTransactions(oneMonthBack,
							present, service, pageable);
					List<TransactionListDTO> transactionList = new ArrayList<>();
					for (MTransaction mTransaction : listTransaction) {
						TransactionListDTO listDTO = new TransactionListDTO();
						MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
						listDTO.setContactNo(userAccount.getUsername());
						listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
						listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
						listDTO.setCreated(sdf.format(mTransaction.getCreated()));
						CashBack cashBack = cashBackRepository.getCashBackTrasactionsByTran(mTransaction);
						if (cashBack != null) {
							listDTO.setMerchantName(cashBack.getMerchantDetails().getMerchantName());
						} else {
							listDTO.setMerchantName("NA");
						}
						listDTO.setStatus(mTransaction.getStatus().getValue());
						listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
								+ userAccount.getUserDetail().getLastName());
						listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
						if (mTransaction.getAuthReferenceNo() == null) {
							listDTO.setRetrivalRefNo("NA");
						} else {
							listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
						}
						if (mTransaction.getCardLoadStatus() == null) {
							listDTO.setCardLoadStatus("NA");
						} else {
							listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
						}
						if (mTransaction.getRemarks() == null) {
							listDTO.setError("NA");
						} else {
							listDTO.setError(mTransaction.getRemarks());
						}
						transactionList.add(listDTO);
					}
					map.addAttribute("transactions", transactionList);
					map.put("Lstatus", "All");
					return "Admin/cashbackTransaction";
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/Home";
		}

	}

	@RequestMapping(value = "/Cashback/TransactionsDatewise", method = RequestMethod.POST)
	public String cashBackTransactionsDateWise(@ModelAttribute RequestDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		try {
			ResponseDTO walletResponse = new ResponseDTO();
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					String aaaaa = dto.getToDate().substring(0, 10);
					String baaaa = dto.getToDate().substring(13);
					Date from = sdformat.parse(aaaaa + " 00:00:00");
					Date to = sdformat.parse(baaaa + " 00:00:00");
					Sort sort = new Sort(Sort.Direction.DESC, "id");
					Pageable pageable = new PageRequest(0, 1000, sort);
					List<MTransaction> listTransaction = null;
					MService service = mServiceRepository.findServiceByCode("CASH");

					listTransaction = mTransactionRepository.getAllCashBackTransactions(from, to, service, pageable);
					if (listTransaction != null) {
						List<TransactionListDTO> transactionList = new ArrayList<>();
						for (MTransaction mTransaction : listTransaction) {
							TransactionListDTO listDTO = new TransactionListDTO();
							MUser userAccount = userApi.findByUserAccount(mTransaction.getAccount());
							listDTO.setContactNo(userAccount.getUsername());
							listDTO.setAmount(String.valueOf(mTransaction.getAmount()));
							listDTO.setTransactionRefNo(mTransaction.getTransactionRefNo());
							listDTO.setCreated(sdf.format(mTransaction.getCreated()));
							CashBack cashBack = cashBackRepository.getCashBackTrasactionsByTran(mTransaction);
							if (cashBack != null) {
								listDTO.setMerchantName(cashBack.getMerchantDetails().getMerchantName());
							} else {
								listDTO.setMerchantName("NA");
							}
							listDTO.setStatus(mTransaction.getStatus().getValue());
							listDTO.setUsername(userAccount.getUserDetail().getFirstName() + " "
									+ userAccount.getUserDetail().getLastName());
							listDTO.setAuthRefNo(mTransaction.getRetrivalReferenceNo());
							if (mTransaction.getAuthReferenceNo() == null) {
								listDTO.setRetrivalRefNo("NA");
							} else {
								listDTO.setRetrivalRefNo(mTransaction.getAuthReferenceNo());
							}
							if (mTransaction.getCardLoadStatus() == null) {
								listDTO.setCardLoadStatus("NA");
							} else {
								listDTO.setCardLoadStatus(mTransaction.getCardLoadStatus());
							}
							if (mTransaction.getRemarks() == null) {
								listDTO.setError("NA");
							} else {
								listDTO.setError(mTransaction.getRemarks());
							}
							transactionList.add(listDTO);
						}
						map.addAttribute("transactions", transactionList);
						map.put("Lstatus", dto.getStatus());
						map.put("dat", dto.getToDate());
						return "Admin/cashbackTransaction";
					}
				} else {
					walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					walletResponse.setMessage("Unauthorized User...");
					return "Admin/Login";
				}
			} else {
				walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
				walletResponse.setMessage("Invalid Session....");
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errMessage", "Service unavailable");
			return "Admin/cashbackTransaction";
		}
		return null;

	}

	/**
	 * FCM INTEGRATION
	 */

	@RequestMapping(value = "/SendNotification", method = RequestMethod.GET)
	public String sendNotification(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				try {
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Send Notification");
					adminActivityRepository.save(activity);
					return "Admin/SendNoti";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Admin/SendNoti";
				}
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
	}

	@RequestMapping(value = "/SendNotification", method = RequestMethod.POST)
	public String sendNotificationPost(@ModelAttribute SendNotificationDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Send Notification Post");
				adminActivityRepository.save(activity);
				try {
					if (dto.getForIos() != null) {
						if (dto.getWithImage() != null && dto.getWithImage().equalsIgnoreCase("Yo")) {
							if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
								String imagePath = CommonUtil.uploadFcmImage(dto.getImage(),
										userSession.getUser().getId() + "", "FCMIMAGES");
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setImagePath(imagePath);
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(true);
								fcmDetails.setAllUsers(true);
								fcmDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your FCM content is set on Scheduler");
								return "Admin/SendNoti";
							} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoSingle")) {
								String imagePath = CommonUtil.uploadFcmImage(dto.getImage(),
										userSession.getUser().getId() + "", "FCMIMAGES");
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setImagePath(imagePath);
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(true);
								fcmDetails.setSingleUser(true);
								fcmDetails.setUsername(dto.getUserMobile());
								fcmDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your FCM content is set on Scheduler");
								return "Admin/SendNoti";
							}
						} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {

							FCMDetails fcmDetails = new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setAllUsers(true);
							fcmDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your FCM content is set on Scheduler");
							return "Admin/SendNoti";
						} else {
							FCMDetails fcmDetails = new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setSingleUser(true);
							fcmDetails.setUsername(dto.getUserMobile());
							fcmDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your FCM content is set on Scheduler");
							return "Admin/SendNoti";
						}
					} else {
						if (CommonUtil.calculatePayload(dto)) {
							if (dto.getWithImage() != null && dto.getWithImage().equalsIgnoreCase("Yo")) {
								if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
									FCMDetails fcmDetails = new FCMDetails();
									fcmDetails.setImagePath(dto.getShortenedimageUrl());
									fcmDetails.setMessage(dto.getMessage());
									fcmDetails.setTitle(dto.getTitle());
									fcmDetails.setStatus("Active");
									fcmDetails.setHasImage(true);
									fcmDetails.setAllUsers(true);
									fcmDetails.setForIos(true);
									fcmDetailsRepository.save(fcmDetails);
									model.addAttribute("message", "Your content is set on Scheduler");
									return "Admin/SendNoti";
								} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoSingle")) {
									FCMDetails fcmDetails = new FCMDetails();
									fcmDetails.setImagePath(dto.getShortenedimageUrl());
									fcmDetails.setMessage(dto.getMessage());
									fcmDetails.setTitle(dto.getTitle());
									fcmDetails.setStatus("Active");
									fcmDetails.setHasImage(true);
									fcmDetails.setSingleUser(true);
									fcmDetails.setForIos(true);
									fcmDetails.setUsername(dto.getUserMobile());
									fcmDetailsRepository.save(fcmDetails);
									model.addAttribute("message", "Your content is set on Scheduler");
									return "Admin/SendNoti";
								}
							} else if (dto.getAllUser() != null && dto.getAllUser().equalsIgnoreCase("YoAll")) {
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(false);
								fcmDetails.setAllUsers(true);
								fcmDetails.setForIos(true);
								fcmDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your content is set on Scheduler");
								return "Admin/SendNoti";
							} else {
								FCMDetails fcmDetails = new FCMDetails();
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(false);
								fcmDetails.setSingleUser(true);
								fcmDetails.setForIos(true);
								fcmDetails.setUsername(dto.getUserMobile());
								fcmDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your content is set on Scheduler");
								return "Admin/SendNoti";

							}
						} else {
							model.addAttribute("message",
									"Payload size limit exceeded.The Size should be less than 256 bytes");
							return "Admin/SendNoti";
						}
					}
				} catch (Exception e) {

					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					model.addAttribute("message", "Exception occurred");
					return "Admin/SendNoti";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Admin/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Admin/Login";
		}
		return "Admin/Login";

	}

	@RequestMapping(value = "/UploadImage", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> uploadImage(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println(dto.getBrand());
		String fileName = CommonUtil.uploadFcmImage(dto.getAadhar_Image(), "1", "FCMIMAGES");
		System.err.println(fileName);
		walletResponse.setCode(fileName);
		walletResponse.setCardDetails(fileName);
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/UploadKycFile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> uploadFile(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		System.err.println(dto.getBrand());
		String fileName = CommonUtil.uploadFcmImage(dto.getAadhar_Image(), "1", "FCMIMAGES");
		System.err.println(fileName);
		walletResponse.setCode(fileName);
		walletResponse.setCardDetails(fileName);
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendBulkSMS", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendBulkSMS(@RequestBody BulkSMSRequest bulkSMSRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		userApi.sendBulkSMS();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Message Sent Successfully");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AcceptedUserGroup", method = RequestMethod.GET)
	public String acceptedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.ADMINISTRATOR)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Accepted User Group List");
				adminActivityRepository.save(activity);
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					Date f = df2.parse(CommonUtil.getDefaultDateRange()[0]);
					Date t = df2.parse(CommonUtil.getDefaultDateRange()[1]);
					lUserData.setFrom(f);
					lUserData.setTo(t);
				} catch (Exception e) {
					e.printStackTrace();
				}
				List<MUser> lUserDataDto = userApi.groupAcceptedUser(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				model.addAttribute("userInfo", userInfo);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Admin/Home";
		}
		return "Admin/AcceptedUserGroup";
	}

	@RequestMapping(value = "/AcceptedUserGroup", method = RequestMethod.POST)
	public String acceptedUserListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.ADMINISTRATOR)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String daterange[] = null;
				try {
					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				List<MUser> lUserDataDto = userApi.groupAcceptedUserPost(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				model.addAttribute("userInfo", userInfo);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Admin/Home";
		}
		return "Admin/AcceptedUserGroup";
	}

	@RequestMapping(value = "/RejectedUserGroup", method = RequestMethod.GET)
	public String acceptedUserListGet(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.ADMINISTRATOR)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Rejected User Group List");
				adminActivityRepository.save(activity);
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					Date f = df2.parse(CommonUtil.getDefaultDateRange()[0]);
					Date t = df2.parse(CommonUtil.getDefaultDateRange()[1]);
					lUserData.setFrom(f);
					lUserData.setTo(t);
				} catch (Exception e) {
					e.printStackTrace();
				}
				List<MUser> lUserDataDto = userApi.groupRejectedUser(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());

				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				model.addAttribute("userInfo", userInfo);

			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Admin/Home";
		}
		return "Admin/RejectedUserGroup";
	}

	@RequestMapping(value = "/RejectedUserGroup", method = RequestMethod.POST)
	public String rejectedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					List<MUser> lUserDataDto = userApi.groupRejectedUserPost(lUserData);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);
					return "/Admin/RejectedUserGroup";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/RejectedUserGroup";
	}

	@RequestMapping(value = "/DeletedUserGroup", method = RequestMethod.GET)
	public String deletedUserListGet(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.ADMINISTRATOR)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("Rejected User Group List");
				adminActivityRepository.save(activity);
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);
				} catch (Exception e) {
					e.printStackTrace();
				}
				List<MUser> lUserDataDto = userApi.getDeletedUser(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				model.addAttribute("userInfo", userInfo);
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);
			}
		} else {
			return "redirect:/Admin/Home";
		}
		return "Admin/DeletedUserGroup";
	}

	@RequestMapping(value = "/DeletedUserGroup", method = RequestMethod.POST)
	public String deletedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						e.printStackTrace();
					}

					List<MUser> lUserDataDto = userApi.getDeletedUser(lUserData);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);

					return "/Admin/DeletedUserGroup";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/DeletedUserGroup";
	}

	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.GET)
	public String getGroupRequestList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Group Request List");
					adminActivityRepository.save(activity);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					Date f = df2.parse(CommonUtil.getDefaultDateRange()[0]);
					Date t = df2.parse(CommonUtil.getDefaultDateRange()[1]);
					lUserData.setFrom(f);
					lUserData.setTo(t);

					List<MUser> lUserDataDto = userApi.groupAddRequest(lUserData);
					List<GroupDetails> details = userApi.getGroupDetailsAdmin();
					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("addUserBool", accessing.isAddUser());
					model.put("Lstatus", "All");

					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);

					return "/Admin/AddGroupRequest";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.POST)
	public String getGroupRequestListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					logger.info("daterange " + dto.getReportrange());
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (dto.getContact() == null || "".equalsIgnoreCase(dto.getContact())) {
						lUserData.setStatus(CommonUtil.ALL);
					} else {
						lUserData.setStatus(dto.getContact().substring(0, dto.getContact().length() - 1));
					}
					lUserData.setContactNo(dto.getContact().substring(0, dto.getContact().length() - 1));
					logger.info("contact no:: " + lUserData.getContactNo().substring(0, dto.getContact().length() - 1));

					List<MUser> lUserDataDto = userApi.groupAddRequestByContact(lUserData);
					List<GroupDetails> details = userApi.getGroupDetailsAdmin();

					for (int i = 0; i < lUserDataDto.size(); i++) {
						logger.info("add request:: " + lUserDataDto.get(i).getUsername());
					}

					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("date", dto.getReportrange());
					model.addAttribute("toDate", lUserData.getTo());
					model.addAttribute("addUserBool", accessing.isAddUser());

					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);

					return "/Admin/AddGroupRequest";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SingleUserGroupRequest")
	public String getSingleUserGroupRequest(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (authority != null) {
				if ((authority.contains(Authorities.ADMINISTRATOR) || authority.contains(Authorities.SUPER_ADMIN)
						&& authority.contains(Authorities.AUTHENTICATED))) {
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Single User Group Request");
					adminActivityRepository.save(activity);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					try {
						List<MUser> user = mUserRespository.findByMobileNo(dto.getMobileNoId());
						if (user != null) {
							model.addAttribute("userData", user);
						}
						if (dto.getRemark().equalsIgnoreCase("request")) {
							return "Admin/AddGroupRequest";
						} else if (dto.getRemark().equalsIgnoreCase("rejected")) {
							return "Admin/RejectedUserGroup";
						} else if (dto.getRemark().equalsIgnoreCase("accepted")) {
							return "Admin/AcceptedUserGroup";
						} else if (dto.getRemark().equalsIgnoreCase("deleted")) {
							return "Admin/DeletedUserGroup";
						}
						model.addAttribute("addUserBool", accessing.isAddUser());
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}
		} else {
			return "Admin/Login";
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/DeleteGroup", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> deleteUserFromGroup(@RequestBody PagingDTO page,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Delete group Request");
		adminActivityRepository.save(activity);
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					result = userApi.deleteUserFromGroup(page, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// Add group
	@RequestMapping(method = RequestMethod.GET, value = "/AddGroup")
	public String addGroupDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				return "Admin/AddGroup";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/AddGroup", method = RequestMethod.POST)
	public String addGroupDetails(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				if ((authority.contains(Authorities.ADMINISTRATOR) || authority.contains(Authorities.SUPER_ADMIN)
						&& authority.contains(Authorities.AUTHENTICATED))) {
					AdminActivity activity = new AdminActivity();
					activity.setActivityName("Add Group");
					adminActivityRepository.save(activity);
					try {
						CommonResponse result = new CommonResponse();
						dto.setSessionId(sessionId);
						dto.setMobileNoId(dto.getMobileNoId());
						dto.setCountry("India");
						logger.info("in add group:: " + dto.getCountryCode());
						result = userApi.createGroup(dto, result);

						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(result.getCode())) {
							model.addAttribute(CommonUtil.GROUP_ADD_MSG, result.getMessage());
							return "Admin/AddGroup";
						} else {
							model.addAttribute(ModelMapKey.ERROR, result.getMessage());
							UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
							MUser userInfo = userSession.getUser();
							model.addAttribute("userInfo", userInfo);
							return "Admin/AddGroup";
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					return "Admin/Login";
				}
			} else {
				return "Admin/Login";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/AcceptGroup/{username}", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> acceptGroup(@PathVariable(value = "username") String contactNo,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Accept group Request");
		adminActivityRepository.save(activity);
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					logger.info("contact: " + contactNo);
					result = userApi.acceptGroupAddRequest(contactNo, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RejectGroup", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> rejectGroupReason(@RequestBody PagingDTO page,
			@ModelAttribute("fromDate") String fromDate, @ModelAttribute("toDate") String toDate,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Rejected Group Request");
		adminActivityRepository.save(activity);
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					logger.info("contact: " + page.getContact());
					result = userApi.rejectGroupRequest(page, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}

	// Donation Module
	@RequestMapping(value = "/AddDonation", method = RequestMethod.GET)
	public String addDonationGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				return "Admin/Donation";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/AddDonation", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> addDonation(@RequestBody GroupDetailDTO dto, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO result = new ResponseDTO();
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Add Donation");
		adminActivityRepository.save(activity);
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					result = userApi.saveDonatee(dto);
				} else {
					result.setSuccess(false);
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setMessage(ResponseStatus.FAILURE.getKey());
				}
			} else {
				result.setSuccess(false);
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage(ResponseStatus.FAILURE.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setSuccess(false);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DonationList", method = RequestMethod.GET)
	public String getDonationList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Donation List");
		adminActivityRepository.save(activity);
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();
					
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);

					List<Donation> lUserDataDto = userApi.addDonationList(lUserData);

					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFrom());
					model.addAttribute("toDate", lUserData.getTo());

					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);
					return "/Admin/DonationList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/DonationList", method = RequestMethod.POST)
	public String getDonationListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.ADMINISTRATOR)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}

					} catch (ParseException e) {
						e.printStackTrace();
					}

					List<Donation> lUserDataDto = userApi.addDonationListPost(lUserData);

					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFrom());
					model.addAttribute("toDate", lUserData.getTo());
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);
					return "/Admin/DonationList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getVerifyMobile(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute LoginDTO dto, HttpSession session, ModelMap map, Model model) {
		ResponseDTO result = new ResponseDTO();
		try {
			AuthenticationError auth = authenticationAdminOtp(dto, request);
			if (auth.isSuccess()) {
				System.err.println("captcha:: " + dto.getCaptcha());
				System.err.println("captcha text:: " + dto.getCaptchaText());
				if (dto.getUsername().equalsIgnoreCase("customercare@ewire.com")) {
					MUser us = mUserRespository.findByUsername(dto.getUsername());
					if (us.getAuthority().contains(Authorities.ADMINISTRATOR)) {
						AuthenticationError authenticate = authentication(dto, request);
						if (authenticate.isSuccess()) {
							Map<String, Object> detail = new HashMap<String, Object>();
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							try {
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								MUser u = userApi.findByUserName(userSession.getUser().getUsername());
								u.setMobileToken(null);
								mUserRespository.save(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								request.getSession().setAttribute("adminSessionId",
										RequestContextHolder.currentRequestAttributes().getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", u.getAccountDetail());
								Double totalTxnAmount = transactionApi.getTransactionAmount();
								map.put("totalTxnAmount", totalTxnAmount);
								DecimalFormat f = new DecimalFormat("##.00");
								if (totalTxnAmount == null || totalTxnAmount <= 0) {
									map.addAttribute("totalCredit", "0.0");

								} else {
									map.addAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
								}
							} catch (Exception e) {
								e.printStackTrace();
								return "Admin/Home";
							}
							try {
								long totalCards = userApi.getMCardCount();
								map.put("totalCards", totalCards);
							} catch (Exception e) {
								e.printStackTrace();
								map.put("totalCards", "Unable to fetch");
							}
							try {
								long totalPhysicalCard = userApi.gettotalPhysicalCard();
								map.put("totalPhysicalCards", totalPhysicalCard);
							} catch (Exception e) {
								map.put("totalPhysicalCards", "Unable to fetch");
								e.printStackTrace();
							}
							try {
								long totalActiveUser = userApi.getTotalActiveUser();
								map.put("totalActiveUser", totalActiveUser);
							} catch (Exception e) {
								map.put("totalActiveUser", "Unable to fetch");
								e.printStackTrace();
							}
							UserKycResponse walletResponse = new UserKycResponse();
							walletResponse = matchMoveApi.getConsumers();
							try {
								if (walletResponse != null) {
									DecimalFormat f1 = new DecimalFormat("##.00");
									map.put("totalPrefundAmount",
											f1.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
								}
							} catch (Exception e) {
								return "Admin/Home";
							}
							result.setDetails(detail);
							Long phyCount = mMCardRepository.getCountPhysicalCards();
							if (phyCount != null) {
								map.put("phycount", phyCount.longValue());
							} else {
								map.put("phycount", 0);
							}
							Long virtualCard = mMCardRepository.getCountVirtualCards();
							if (virtualCard != null) {
								map.put("virCount", virtualCard.longValue());
							} else {
								map.put("virCount", 0);
							}
							Long pendingCardReq = physicalCardDetailRepository.getCountPhysicalCardRequestPending();
							if (pendingCardReq != null) {
								map.put("pendingReq", pendingCardReq.longValue());
							} else {
								map.put("pendingReq", 0);
							}

							MService razorpay = mServiceRepository.findServiceByCode("LMS");
							MService upiPay = mServiceRepository.findServiceByCode("UPS");

							List<String> monthsListRazorPay = mTransactionRepository
									.getMonthsForBarGraph(Status.Success, razorpay);
							List<String> monthsListUpi = mTransactionRepository.getMonthsForBarGraph(Status.Success,
									upiPay);
							List<Double> upiAmtList = mTransactionRepository
									.getTransactionAmountMonthWise(Status.Success, upiPay);
							List<Double> razorAmtList = mTransactionRepository
									.getTransactionAmountMonthWise(Status.Success, razorpay);
							System.err.println("size monthsListRazorPay:" + monthsListRazorPay.size());

							System.err.println("size razorAmtList:" + razorAmtList.size());

							if (upiAmtList == null || upiAmtList.isEmpty()) {
								upiAmtList.add(0.0);
							}
							if (monthsListUpi == null || monthsListUpi.isEmpty()) {
								monthsListUpi.add("june");
							}
							System.err.println("size monthsListUpi:" + monthsListUpi.size());
							System.err.println("size upiAmtList:" + upiAmtList.size());
							if (monthsListRazorPay.size() >= monthsListUpi.size()) {
								List<String> stringList = new ArrayList<>();
								for (String string : monthsListRazorPay) {
									stringList.add("'" + string + "'");
								}
								model.addAttribute("monthsList", stringList);

								int sizeDiff = monthsListRazorPay.size() - monthsListUpi.size();
								System.err.println(sizeDiff);
								if (sizeDiff != 0) {
									List<Double> doubleList = new ArrayList<>();

									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}

							} else {
								int sizeDiff = monthsListRazorPay.size() - monthsListUpi.size();
								System.err.println(sizeDiff);
								if (sizeDiff != 0) {
									List<Double> doubleList = new ArrayList<>();

									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}
							}
							model.addAttribute("amountListRazorPay", razorAmtList);
							AdminActivity activity = new AdminActivity();
							activity.setActivityName("Admin Login");
							adminActivityRepository.save(activity);
							return "Admin/Home";
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage(auth.getMessage());
							result.setDetails(null);
							map.put(ModelMapKey.ERROR, auth.getMessage());
							return "Admin/Login";
						}
					}
				}
				boolean verified = userApi.resendMobileTokenDeviceBinding(dto.getUsername());
				if (verified) {
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setSuccess(true);
					result.setMessage("OTP sent");
					result.setDetails("OTP sent");
					map.addAttribute("msg", result.getMessage());
					map.addAttribute("username", dto.getUsername());
					map.addAttribute("password", dto.getPassword());
					return "Admin/VerifyMobile";
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setSuccess(false);
					result.setMessage("Enter valid Details");
					result.setDetails("Enter valid Details");
					map.addAttribute("msg", result.getMessage());
					return "Admin/VerifyMobile";
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage(auth.getMessage());
				result.setDetails(null);
				map.put(ModelMapKey.ERROR, auth.getMessage());
				return "Admin/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/ResendDeviceBindingOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> resendDeviceBindingOTP(@RequestBody LoginDTO dto, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("mobilenumber:: " + dto.getUsername());
			boolean verified = userApi.resendMobileTokenDeviceBinding(dto.getUsername());
			if (verified) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setSuccess(true);
				result.setMessage("New OTP sent");
				result.setDetails("New OTP sent");
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setSuccess(false);
				result.setMessage("Enter valid Details");
				result.setDetails("Enter valid Details");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	private AuthenticationError authenticationAdminOtp(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(24 * 60 * 60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}

	@RequestMapping(value = "/AdminAccess", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> restrictAdminAccess(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						MUser userInfo = userSession.getUser();
						AdminAccessIdentifier access = adminAccessIdentifierRepository.getallaccess(userInfo);
						result.setDetails(access);
						model.addAttribute("addUser", access.isAddUser());
						model.addAttribute("loadCard", access.isLoadCard());
						model.addAttribute("addCorporate", access.isAddCorporate());
						model.addAttribute("addMerchant", access.isAddMerchant());
						model.addAttribute("addAgent", access.isAddAgent());
						model.addAttribute("addDonation", access.isAddDonation());
						model.addAttribute("addGroup", access.isAddGroup());
						model.addAttribute("sendNotification", access.isSendNotification());
						model.addAttribute("assignPhysicalCard", access.isAssignPhysicalCard());
						model.addAttribute("assignVirtualCard", access.isAssignVirtualCard());
						model.addAttribute("androidVersion", access.isAndroidVersion());
						model.addAttribute("cardBlock", access.isCardBlock());
						model.addAttribute("myUnblock", access.isMyUnblock());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// Bus Admin

	@SuppressWarnings("deprecation")
	@RequestMapping(method = RequestMethod.GET, value = "/BusDetailsList")
	public String getBusdetails(@ModelAttribute PagingDTO dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		try {
			AdminActivity activity = new AdminActivity();
			activity.setActivityName("Bus Details");
			adminActivityRepository.save(activity);
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null) {
					if ((authority.contains(Authorities.ADMINISTRATOR))
							&& authority.contains(Authorities.AUTHENTICATED)) {
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						Date oneMonthBack = cal.getTime();
						Calendar calPresent = Calendar.getInstance();
						Date present = calPresent.getTime();

						AdminAccessIdentifier accessing = adminAccessIdentifierRepository
								.getallaccess(userSession.getUser());

						MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.BUS);
						MCommission senderCommission = transactionTypeValidation.findCommissionByService(service);

						List<BusTicket> list = userApi.getBusDetailsForAdminByDate(oneMonthBack, present);
						List<BusDetailsDTO> tickList = new ArrayList<BusDetailsDTO>();
						for (BusTicket ticket : list) {
							BusDetailsDTO tick = new BusDetailsDTO();
							if (ticket.getTransaction() != null) {
								MUser user = userApi.findByUserAccount(ticket.getTransaction().getAccount());
								if (user != null) {
									tick.setUserMobile(user.getUserDetail().getContactNo());
									tick.setUserEmail(user.getUserDetail().getEmail());
									tick.setTransactionId(ticket.getTransaction().getTransactionRefNo());
									tick.setFirstName(user.getUserDetail().getFirstName());
									tick.setLastName(user.getUserDetail().getLastName());
									tick.setDob(user.getUserDetail().getDateOfBirth().toLocaleString());
									tick.setTransactionDate(ticket.getTransaction().getCreated().toLocaleString());
									tick.setTicketPnr(ticket.getTicketPnr());
									tick.setOperatorPnr(ticket.getOperatorPnr());
									tick.setStatus(ticket.getStatus());
									tick.setTransactionStatus(ticket.getTransaction().getStatus().getValue());
									tick.setSource(ticket.getSource());
									tick.setDestination(ticket.getDestination());
									tick.setJourneyDate(ticket.getJourneyDate());
									tick.setArrTime(ticket.getArrTime());
									double totalFare = ticket.getTotalFare();
									tick.setTotalFare(totalFare);
									String priceRecheckTotalFare = ticket.getPriceRecheckTotalFare();
									if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
											&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {
										totalFare = Double.valueOf(priceRecheckTotalFare);
									}
									tick.setCommissionAmount(String.valueOf(senderCommission.getValue()));
									tick.setPriceRecheckTotalFare(ticket.getPriceRecheckTotalFare());
									tick.setEmtTxnId(ticket.getEmtTxnId());

									System.err.println("tick string :: " + tick.toString());
									tickList.add(tick);
								}
							}
						}
						model.put("busList", tickList);
						model.addAttribute("fromDate", oneMonthBack);
						model.addAttribute("dat", present);

						MUser userInfo = userSession.getUser();
						model.addAttribute("userInfo", userInfo);
						model.addAttribute("addUserBool", accessing.isAddUser());
						return "Admin/BusDetails";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/Login";
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(method = RequestMethod.POST, value = "/BusDetailsList")
	public String getBusdetailsPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				if ((authority.contains(Authorities.ADMINISTRATOR)) && authority.contains(Authorities.AUTHENTICATED)) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					AdminAccessIdentifier accessing = adminAccessIdentifierRepository
							.getallaccess(userSession.getUser());

					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;
					Date from = null;
					Date to = null;
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							from = sdformat.parse(aaaaa + " 00:00:00");
							to = sdformat.parse(baaaa + " 00:00:00");
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							from = df2.parse(daterange[0]);
							to = df2.parse(daterange[1]);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					MService service = mServiceRepository.findServiceByCode(ServiceCodeUtil.BUS);
					MCommission senderCommission = transactionTypeValidation.findCommissionByService(service);

					List<BusTicket> list = userApi.getBusDetailsForAdminByDate(from, to);
					List<BusDetailsDTO> tickList = new ArrayList<BusDetailsDTO>();
					for (BusTicket ticket : list) {
						if (ticket.getTransaction() != null) {
							BusDetailsDTO tick = new BusDetailsDTO();
							MUser user = userApi.findByUserAccount(ticket.getTransaction().getAccount());
							if (user != null) {
								tick.setUserMobile(user.getUserDetail().getContactNo());
								tick.setUserEmail(user.getUserDetail().getEmail());
								tick.setTransactionId(ticket.getTransaction().getTransactionRefNo());
								tick.setFirstName(user.getUserDetail().getFirstName());
								tick.setLastName(user.getUserDetail().getLastName());
								tick.setDob(user.getUserDetail().getDateOfBirth().toLocaleString());
								tick.setTransactionDate(ticket.getTransaction().getCreated().toLocaleString());
								if (ticket.getTicketPnr() == null) {
									tick.setTicketPnr("NA");
								} else {
									tick.setTicketPnr(ticket.getTicketPnr());
								}
								if (ticket.getOperatorPnr() == null) {
									tick.setOperatorPnr("NA");
								} else {
									tick.setOperatorPnr(ticket.getOperatorPnr());
								}
								tick.setStatus(ticket.getStatus());
								tick.setTransactionStatus(ticket.getTransaction().getStatus().getValue());
								tick.setSource(ticket.getSource());
								tick.setDestination(ticket.getDestination());
								tick.setJourneyDate(ticket.getJourneyDate());
								tick.setArrTime(ticket.getArrTime());
								double totalFare = ticket.getTotalFare();
								tick.setTotalFare(totalFare);
								String priceRecheckTotalFare = ticket.getPriceRecheckTotalFare();
								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {
									totalFare = Double.valueOf(priceRecheckTotalFare);
								}
								tick.setCommissionAmount(String.valueOf(senderCommission.getValue()));
								tick.setPriceRecheckTotalFare(ticket.getPriceRecheckTotalFare());
								tick.setEmtTxnId(ticket.getEmtTxnId());
								tickList.add(tick);
							}
						}
					}
					model.put("busList", tickList);
					model.addAttribute("fromDate", from);
					model.addAttribute("dat", to);

					MUser userInfo = userSession.getUser();
					model.addAttribute("userInfo", userInfo);
					model.addAttribute("addUserBool", accessing.isAddUser());

					return "Admin/BusDetails";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getSingleTicketDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSingleTicketDetails(HttpSession session, @RequestBody BusTicketDTO dto,
			HttpServletRequest request) {
		ResponseDTO resp = new ResponseDTO();
		ResponseDTO resp1 = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);

		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				if ((authority.contains(Authorities.ADMINISTRATOR)) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						IsCancellableReq cancellableReq = new IsCancellableReq();
						if (dto.getEmtTxnId() != null) {
							resp = mdexBusApiImpl.getSingleTicketTravellerDetails(dto.getEmtTxnId());

							BusTicket busTicket = (BusTicket) resp.getExtraInfo();

							cancellableReq.setSeatNo(resp.getSeatNo());
							cancellableReq.setBookId(busTicket.getEmtTransactionScreenId());

							session.setAttribute("emtTxnScreenId", busTicket.getEmtTransactionScreenId());
							session.setAttribute("seatForCancel", resp.getSeatNo());
							resp1 = mdexBusApiImpl.isCancellable(cancellableReq);
							CancelTicketResp result = (CancelTicketResp) resp1.getDetails();
							resp.setRefAmt(result.getRefundAmount());
							resp.setCancellationCharges(result.getCancellationCharges());
							resp.setTransactionRefNo(busTicket.getTransaction().getTransactionRefNo());
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.AUTHORITY_MSG);
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, value = "/cancelBookedTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> cancelBookedTicket(HttpSession session, HttpServletRequest request,
			@RequestBody IsCancellableReq dto) {
		CommonResponse resp = new CommonResponse();
		String sessionId = (String) session.getAttribute(CommonUtil.ADMIN_SESSION_ID);
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Cancel Booked Ticket Bus");
		adminActivityRepository.save(activity);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null) {
				if ((authority.contains(Authorities.ADMINISTRATOR)) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						String emtTxnScreenId = (String) session.getAttribute("emtTxnScreenId");
						List<String> seatNo = (List<String>) session.getAttribute("seatForCancel");

						dto.setSessionId(sessionId);
						dto.setSeatNo(seatNo);
						dto.setBookId(emtTxnScreenId);

						if (dto.getBookId() != null) {
							resp = mdexBusApiImpl.cancelBookedTicket(dto);
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage(ErrorMessage.SERVICE_UNAVAILABLE_ERROR);
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage(ErrorMessage.AUTHORITY_MSG);
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage(ErrorMessage.SESSION_LOGOUT_ERROR);
		}
		return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangePasssword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CommonResponse> changePasswordAdmin(@RequestBody MatchMoveCreateCardRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		CommonResponse resp = new CommonResponse();
		userApi.changePasswordAdmin(dto);
		resp.setMessage("Password changed Successfully");
		resp.setCode(ResponseStatus.SUCCESS.getValue());
		return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserBlockList")
	public String getBlockUserListGet(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("User Block List");
		adminActivityRepository.save(activity);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				model.addAttribute("addUserBool", accessing.isAddUser());
				return "Admin/SuccessTransactionBlock";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserBlockList")
	public ResponseEntity<ResponseDTO> getBlockUserListPost(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				AdminAccessIdentifier accessing = adminAccessIdentifierRepository.getallaccess(userSession.getUser());

				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);

				Page<MUser> card = userApi.findAllUserBasedOnBlock(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					try {
						if (mmCards.getGroupDetails().getGroupName() == null) {
							car.setGroup("-");
						} else {
							car.setGroup(mmCards.getGroupDetails().getGroupName());
						}
					} catch (Exception e) {
						car.setGroup("None");
						e.printStackTrace();
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus("Blocked");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
						TwoMinBlock minBlock = twoMinBlockRepository.getBlockedUser(mmCards);
						minBlock.setBlock(false);
						minBlock.setStatus(Status.Active.getValue());
						minBlock.setCounting(0);
						twoMinBlockRepository.save(minBlock);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					cards.add(car);
				}
				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				model.addAttribute("addUserBool", accessing.isAddUser());
				resp.setJsonArray(cards);
				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/AdminActivity", method = RequestMethod.GET)
	public String getAdminActivity(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		AdminActivity activity = new AdminActivity();
		activity.setActivityName("Admin Activity List");
		adminActivityRepository.save(activity);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, -1);
				Date oneMonthBack = cal.getTime();
				Calendar calPresent = Calendar.getInstance();
				Date present = calPresent.getTime();

				List<AdminActivity> active = new ArrayList<AdminActivity>();
				Sort sort = new Sort(Sort.Direction.DESC, "id");
				Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
				Page<AdminActivity> activitypage = adminActivityRepository.getActivities(pageable, oneMonthBack,
						present);
				if (activitypage != null) {
					active = activitypage.getContent();
				}
				modelMap.addAttribute("activity", active);
				return "Admin/AdminActivity";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/ChangeUserGroup", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> changeGroupName(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO result = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);

			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					result = userApi.changeUserGroup(page);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AdminActivity", method = RequestMethod.POST)
	public String adminActivityPost(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);

		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}

				List<AdminActivity> active = new ArrayList<AdminActivity>();
				Sort sort = new Sort(Sort.Direction.DESC, "id");
				Pageable pageable = new PageRequest(CommonUtil.PAGE, CommonUtil.SIZE, sort);
				Page<AdminActivity> activitypage = adminActivityRepository.getActivities(pageable, page.getFromDate(),
						page.getToDate());
				if (activitypage != null) {
					active = activitypage.getContent();
				}
				modelMap.addAttribute("activity", active);

				return "Admin/AdminActivity";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/LoadCardOtp", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getLoadCardOtp(@ModelAttribute PagingDTO page, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO result = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);

			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					result = userApi.loadCardOtp();
					if (result.getCode().equalsIgnoreCase("S00")) {
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setSuccess(false);
						result.setMessage("Enter valid Details");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GroupUserListing")
	public String getGroupUserList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);

				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				return "Admin/GroupUserList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GroupUserListing")
	public String getGroupUserListPOst(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				Page<MUser> card = userApi.findAllUser(page);
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MUser mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setContactNO(mmCards.getUserDetail().getContactNo());
					if (mmCards.getUserDetail().getEmail() == null) {
						car.setEmail("NA");
					} else {
						car.setEmail(mmCards.getUserDetail().getEmail());
					}
					try {
						if (mmCards.getGroupDetails().getGroupName() == null) {
							car.setGroup("-");
						} else {
							car.setGroup(mmCards.getGroupDetails().getGroupName());
						}
					} catch (Exception e) {
						car.setGroup("None");
						e.printStackTrace();
					}
					car.setUserName(mmCards.getUsername());
					car.setStatus(mmCards.getMobileStatus() + "");
					car.setIssueDate(mmCards.getCreated() + "");
					if (mmCards.getUserDetail().getFirstName() == null) {
						car.setFirstName("NA");
					} else {
						car.setFirstName(mmCards.getUserDetail().getFirstName());
					}
					if (mmCards.getUserDetail().getLastName() == null) {
						car.setLastName("NA");
					} else {
						car.setLastName(mmCards.getUserDetail().getLastName());
					}
					if (mmCards.getAuthority().trim().equalsIgnoreCase("ROLE_USER,ROLE_AUTHENTICATED")) {
						car.setRole(true);
					} else {
						car.setRole(false);
					}
					car.setAccountType(mmCards.getAccountDetail().getAccountType().getCode());
					if (mmCards.getMobileToken() == null) {
						car.setMobileToken("NA");
					} else if ("".equalsIgnoreCase(mmCards.getMobileToken())) {
						car.setMobileToken("NA");
					} else {
						car.setMobileToken(mmCards.getMobileToken());
					}
					car.setAuthority(mmCards.getAuthority());
					if (mmCards.getUserDetail().getDateOfBirth() != null) {
						car.setDob(mmCards.getUserDetail().getDateOfBirth() + "");
					} else {
						car.setDob("NA");
					}
					car.setChangeGroupStatus(mmCards.getUserDetail().isGroupChange());
					if (mmCards.getUserDetail().isGroupChange() == true) {
						car.setChangedGroupName(mmCards.getUserDetail().getChangedGroupName());
					} else {
						car.setChangedGroupName("-");
					}
					cards.add(car);
				}
				AdminActivity activity = new AdminActivity();
				activity.setActivityName("User List");
				adminActivityRepository.save(activity);

				List<GroupDetails> details = userApi.getGroupDetailsAdmin();
				model.addAttribute("groupList", details);
				resp.setJsonArray(cards);
				model.addAttribute("userData", cards);

				resp.setTotalPages(card.getTotalPages());
				resp.setDate(page.getDaterange());
				return "Admin/groupUserListing";
			}
		}
		return "Admin/Home";
	}

	@RequestMapping(value = "/FetchGroupList", method = RequestMethod.GET)
	public String getGroupListGet(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					List<GroupDetails> details = userApi.getGroupListGet(page);
					model.addAttribute("groupListNew", details);
					return "Admin/GroupList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/Home";
	}

	@RequestMapping(value = "/FetchGroupList", method = RequestMethod.POST)
	public String getGroupListPost(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {

		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange() != null) {
						System.err.println(page.getDaterange());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				persistingSessionRegistry.refreshLastRequest(sessionId);
				List<GroupDetails> details = userApi.getGroupListPost(page);

				model.addAttribute("groupListNew", details);
				return "Admin/GroupList";
			}
		}
		return "Admin/Home";
	}

	@RequestMapping(value = "/SingleGroupDetail", method = RequestMethod.POST)
	public String getSingleGroupListPost(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					List<GroupDetails> details = userApi.getSingleGroupList(page);
					model.addAttribute("groupListNew", details);
					return "Admin/GroupList";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/GroupList";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/DownloadFile/{url}")
	public void DownloadFile(HttpSession session, Model model, @PathVariable("url") String url,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			CSVReader.downloadFile(url, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}