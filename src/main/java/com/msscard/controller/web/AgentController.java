package com.msscard.controller.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/Agent")
public class AgentController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	/*private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final static String INVALID_USERNAME_OR_PASSWORD = "Invalid Username Or Password";
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final RegisterValidation registerValidation;
	private final IAgentApi agentApi;
	private final IMatchMoveApi matchMoveApi;
	private final CommonValidation commonValidation;
	
	public AgentController(IUserApi userApi, AuthenticationManager authenticationManager,  SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi, LoginValidation loginValidation , PersistingSessionRegistry persistingSessionRegistry , 
			RegisterValidation registerValidation , IAgentApi agentApi , IMatchMoveApi matchMoveApi , CommonValidation commonValidation) {
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.loginValidation = loginValidation;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.registerValidation = registerValidation;
		this.agentApi = agentApi;
		this.matchMoveApi = matchMoveApi;
		this.commonValidation = commonValidation;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String showAgentHome(HttpServletRequest request, HttpServletResponse response,
			HttpSession session , ModelMap modelMap) throws Exception {
		String sessionId = (String) session.getAttribute("agentSessionId");
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					long userCount = agentApi.userCountBasedOnAgent(userSession.getUser());
					modelMap.addAttribute("userCount", userCount);
					double totalAmount = agentApi.countTotalTransferredAmount(userSession.getUser());
					modelMap.addAttribute("totalAmount", totalAmount);
					long physicalCardCount = agentApi.countPhysicalCards(userSession.getUser());
					modelMap.addAttribute("physicalCardCount", physicalCardCount);
					
					JSONObject txnGraph= agentApi.findTotalTransactionByMonth(userSession.getUser());
				    modelMap.addAttribute("monthData", txnGraph.getString("arrayToJson"));
				    modelMap.addAttribute("valueData", txnGraph.getString("visitsToJson"));

				    JSONObject userGraph = agentApi.findTotalUserByMonth(userSession.getUser());
				    modelMap.addAttribute("Month", userGraph.getString("arrayToJson"));
				    modelMap.addAttribute("UserValues", userGraph.getString("visitsToJson"));
					
					return "Agent/Home";
				} else {
					return "Agent/Login";
				}
			} else {
				return "Agent/Login";
			}
		} else {
			return "Agent/Login";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/Home")
	public String showAgentHome(@ModelAttribute LoginDTO agentDto , HttpServletRequest request, 
			HttpServletResponse response, HttpSession session , ModelMap modelMap) {
		try {
			agentDto.setIpAddress("0.0.0.0");
			LoginError error = loginValidation.checkLoginValidation(agentDto);
			if (error.isSuccess()) {
				MUser user = userApi.findByUserName(agentDto.getUsername());
				if (user != null && (user.getAuthority().contains(Authorities.AGENT))) {
					AuthenticationError authError = isValidUsernamePassword(agentDto, request);
					if (authError.isSuccess()) {
						AuthenticationError authenticationError = authentication(agentDto, request);
						if (authenticationError.isSuccess()) {
							session.setAttribute("username", user.getUsername());
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = sessionApi.getUserSession(RequestContextHolder.currentRequestAttributes().getSessionId());
							request.getSession().setAttribute("agentSessionId", userSession.getSessionId());
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
							long userCount = agentApi.userCountBasedOnAgent(userSession.getUser());
							modelMap.addAttribute("userCount", userCount);
							double totalAmount = agentApi.countTotalTransferredAmount(userSession.getUser());
							modelMap.addAttribute("totalAmount", totalAmount);
							long physicalCardCount = agentApi.countPhysicalCards(userSession.getUser());
							modelMap.addAttribute("physicalCardCount", physicalCardCount);

							JSONObject txnGraph= agentApi.findTotalTransactionByMonth(userSession.getUser());
						    modelMap.addAttribute("monthData", txnGraph.getString("arrayToJson"));
						    modelMap.addAttribute("valueData", txnGraph.getString("visitsToJson"));

						    JSONObject userGraph = agentApi.findTotalUserByMonth(userSession.getUser());
						    modelMap.addAttribute("Month", userGraph.getString("arrayToJson"));
						    modelMap.addAttribute("UserValues", userGraph.getString("visitsToJson"));
							
						} else {
							modelMap.addAttribute("loginMsg", INVALID_USERNAME_OR_PASSWORD);
							return "Agent/Login";
						}
					} else {
						modelMap.addAttribute("loginMsg", INVALID_USERNAME_OR_PASSWORD);
						return "Agent/Login";
					}
				} else {
					modelMap.addAttribute("loginMsg", INVALID_USERNAME_OR_PASSWORD);
					return "Agent/Login";
				}
			} else {
				modelMap.addAttribute("loginMsg", error.getMessage());
				return "Agent/Login";
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/Login";
		}
		
		return "Agent/Home";
	}
	
	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage("Invalid Password");
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setSuccess(false);
			error.setMessage(e.getMessage());
			return error;
		}
	}
	
	
	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request) throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				return error;
			} else {
				error.setSuccess(false);
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			return error;
		}
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String agentLogout(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					sessionApi.expireSession(sessionId);
					session.invalidate();
					return "Agent/Login";
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "redirect:/Agent/Home";
		}
		return "redirect:/Agent/Home";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/UserRegister")
	public String userRegisterByAgent(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						return "Agent/UserRegister";
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/UserRegister";
		}
		
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/UserRegister")
	public String saveUserRegisterByAgent(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session , @ModelAttribute RegisterDTO dto) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						RegisterError registerError=registerValidation.validateUser(dto);
						if (registerError.isValid()) {
							dto.setUserType(UserType.User);
							if(dto.getPanCardImage() != null) {
								String panCardImgPath = CommonUtil.uploadImageForUser(dto.getPanCardImage(),dto.getContactNo(),"Pan");
								if (panCardImgPath != null && !panCardImgPath.isEmpty()) {
									dto.setPanCardImagePath(panCardImgPath);
								}
							}
							if(dto.getAadharImage1() != null) {
								String aadharCardImgPath = CommonUtil.uploadImageForUser(dto.getAadharImage1(),dto.getContactNo(),"Aadhar");
								if (aadharCardImgPath != null && !aadharCardImgPath.isEmpty()) {
									dto.setAadharImagePath1(aadharCardImgPath);
								}
							}
							if(dto.getAadharImage2() != null) {
								String aadharCardImgPath = CommonUtil.uploadImageForUser(dto.getAadharImage2(),dto.getContactNo(),"Aadhar");
								if (aadharCardImgPath != null && !aadharCardImgPath.isEmpty()) {
									dto.setAadharImagePath2(aadharCardImgPath);
								}
							}
							dto.setmUser(userSession.getUser());
							MUser mUser = agentApi.saveUserByAgent(dto);
							ResponseDTO resp = new ResponseDTO() ;
							WalletResponse walletResponse = new WalletResponse();
							if (mUser != null && mUser.getId() > 0) {
								resp = matchMoveApi.createUserOnMM(mUser);
								if(resp.getCode().equalsIgnoreCase("S00")) {
									walletResponse = matchMoveApi.assignVirtualCard(mUser);
									modelMap.addAttribute("success", "User successfully registered.");
								} else {
									modelMap.addAttribute("error", "User already exists.");
								}
							}
							logger.info("response.. "+walletResponse.getCode());
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						} else {
							modelMap.addAttribute("error", registerError.getMessage());
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						}
						
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/UserRegister";
		}
		return "Agent/UserRegister";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/UserList")
	public String showUserList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MUserDTO> mUser = agentApi.getAllUser(userSession.getUser() , null);
						if (mUser != null && !mUser.isEmpty()) {
							modelMap.addAttribute("userList", mUser);
						}
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						modelMap.addAttribute("fromDate" , CommonUtil.getDefaultDateRange()[0]);
						modelMap.addAttribute("toDate" , CommonUtil.getDefaultDateRange()[1]);
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/UserList";
		}
		return "Agent/UserList";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/UserList")
	public String showUserListBasedOnDate(@ModelAttribute MUserDTO dto, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String daterange[] = null;
						if (!dto.getDaterange().isEmpty() && dto.getDaterange() != null) {
							daterange = CommonUtil.getDateRange(dto.getDaterange());
						} else {
							daterange = CommonUtil.getDefaultDateRange();
						}
						dto.setFromDate(daterange[0]);
						dto.setToDate(daterange[1]);
						List<MUserDTO> mUser = agentApi.getAllUser(userSession.getUser() ,  dto);
						modelMap.addAttribute("userList", mUser);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						modelMap.addAttribute("fromDate" , dto.getFromDate());
						modelMap.addAttribute("toDate" , dto.getToDate());
						
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return "Agent/UserList";
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/LoadCardForUser")
	public String loadCardThroughAgent(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						return "Agent/LoadCardForUser";
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/LoadCardForUser";
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/LoadCardForUser")
	public String loadCardForUser(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session , @ModelAttribute UserLoadCardDTO dto) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MUser mUser=userApi.findByUserName(dto.getContactNo());
						dto.setUser(userSession.getUser());
						if (mUser != null) {
							//if (agentApi.checkUser(userSession.getUser(), mUser)) {
								ValidTransactionDTO validTransactionDTO = agentApi.validTransaction(dto);
								if (validTransactionDTO.isSuccess()) {
									MMCards mmCards = new MMCards();
									mmCards = agentApi.existCard(mUser);
									if (mmCards != null) {
										WalletResponse walletResponse = new WalletResponse();
										walletResponse=matchMoveApi.initiateLoadFundsToMMWallet(mUser, String.valueOf(dto.getAmount()));
										if(walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
											walletResponse=matchMoveApi.transferFundsToMMCard(mUser, String.valueOf(dto.getAmount()),mmCards.getCardId());
											if(walletResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
												agentApi.loadCardFromAgent(dto, mmCards , mUser);
												modelMap.addAttribute("succsseMsg", "Transaction successful, Amount loaded into the card.");
												modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
											} else {
												modelMap.addAttribute("errorMsg", walletResponse.getMessage());
												modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
											}
										} else {
											modelMap.addAttribute("errorMsg", "Transaction failed,"+walletResponse.getMessage());
											modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
										}
									} else {
										modelMap.addAttribute("errorMsg", "Sorry this user does not have card.");
										modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
									}
								} else {
									modelMap.addAttribute("errorMsg", validTransactionDTO.getMessage());
									modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
								}
							} else {
								modelMap.addAttribute("errorMsg", "Sorry you cant load money for this user.");
							}
						} else {
							modelMap.addAttribute("errorMsg", "User does not exist.");
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						}
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/LoadCardForUser";
		}
		return "Agent/LoadCardForUser";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/AgentPrefund")
	public String showAgentPrefund(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						
						ObjectMapper objectMapper = new ObjectMapper();
						String arrayToJson = objectMapper.writeValueAsString(agentApi.getAllBankDetails());
						logger.info(arrayToJson);
						modelMap.addAttribute("banks", arrayToJson);
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/AgentPrefund";
		}
		return "Agent/AgentPrefund";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/AgentPrefund")
	public String saveAgentPrefund(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session , @ModelAttribute AgentPrefundDTO dto ) throws JsonGenerationException, JsonMappingException, IOException, Exception {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null && user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						CommonError commonError = commonValidation.checkAgentPrefundError(dto);
						if (commonError.isValid()) {
							dto.setUser(userSession.getUser());
							agentApi.agentPrefundRequest(dto);
							modelMap.addAttribute("success", "Your prefund request has been raised sucessfully.");
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						} else {
							modelMap.addAttribute("error", commonError.getMessage());
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						}
						ObjectMapper objectMapper = new ObjectMapper();
						String arrayToJson = objectMapper.writeValueAsString(agentApi.getAllBankDetails());
						logger.info(arrayToJson);
						modelMap.addAttribute("banks", arrayToJson);
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			modelMap.addAttribute("error", e.getMessage());
			ObjectMapper objectMapper = new ObjectMapper();
			String arrayToJson = objectMapper.writeValueAsString(agentApi.getAllBankDetails());
			logger.info(arrayToJson);
			modelMap.addAttribute("banks", arrayToJson);
			return "Agent/AgentPrefund";
		}
		return "Agent/AgentPrefund";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/AssignPhysicalCard")
	public String assignPhysicalCard(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/AssignPhysicalCard";
		}
		return "Agent/AssignPhysicalCard";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/AssignPhysicalCard")
	public ResponseEntity<AssignPhysicalCardResponseDTO> saveAssignPhysicalCard(@RequestBody AssignPhysicalCardAgentDTO dto , 
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session ) {
		AssignPhysicalCardResponseDTO responseDTO = new AssignPhysicalCardResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						logger.info("inside method");
						CommonError error = commonValidation.checkKitNo(dto);
						if (error.isValid()) {
							MUser mUser = agentApi.checkUser(dto.getUserName());
							dto.setUser(mUser);
							if (mUser != null) {
								CommonError commonError = agentApi.checkUserKYC(mUser);
								if (commonError.isValid()) {
									WalletResponse  resp = matchMoveApi.assignPhysicalCardFromAgent(mUser, dto.getKitNo());
									logger.info("wallet response>>>> "+resp);
									if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
										agentApi.updateCardDetails(mUser);
										responseDTO.setCode(ResponseStatus.SUCCESS.getValue());
										responseDTO.setMessage(ResponseStatus.SUCCESS.getKey());
									} else {
										responseDTO.setCode(ResponseStatus.FAILURE.getValue());
										responseDTO.setMessage(resp.getMessage());
									}
								} else {
									responseDTO.setCode(ResponseStatus.FAILURE.getValue());
									responseDTO.setMessage(commonError.getMessage());
								}
							} else {
								responseDTO.setCode(ResponseStatus.FAILURE.getValue());
								responseDTO.setMessage("User does not exist.");
							}
						} else {
							responseDTO.setCode(ResponseStatus.FAILURE.getValue());
							responseDTO.setMessage(error.getMessage());
						}
					} else {
						responseDTO.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						responseDTO.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					}
				} else {
					responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
					responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
				responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			responseDTO.setCode(ResponseStatus.BAD_REQUEST.getValue());
			responseDTO.setMessage(ResponseStatus.BAD_REQUEST.getKey());
		}
		return new ResponseEntity<AssignPhysicalCardResponseDTO>(responseDTO , HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/VerifyPhysicalCard")
	public ResponseEntity<AssignPhysicalCardResponseDTO> verifyPhysicalCard(@RequestBody AssignPhysicalCardAgentDTO dto , 
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session ) {
		AssignPhysicalCardResponseDTO responseDTO = new AssignPhysicalCardResponseDTO();
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						MUser mUser = agentApi.checkUser(dto.getUserName());
						ResponseDTO resp = new ResponseDTO();
						if (mUser != null) {
							resp=matchMoveApi.activationPhysicalCard(dto.getOtp(), mUser);
							if(resp.getCode().equalsIgnoreCase("S00")) {
								 agentApi.updatePhysicalCard(mUser);
								 agentApi.transferMoneyToCard(mUser);
								 agentApi.saveAgentAssignPhysicalCard(userSession.getUser(), mUser,dto);
								 responseDTO.setCode(resp.getCode());
								 responseDTO.setMessage(resp.getMessage());
							}
						}
					} else {
						responseDTO.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
						responseDTO.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					}
				} else {
					responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
					responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				responseDTO.setCode(ResponseStatus.INVALID_SESSION.getValue());
				responseDTO.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			responseDTO.setCode(ResponseStatus.BAD_REQUEST.getValue());
			responseDTO.setMessage(ResponseStatus.BAD_REQUEST.getKey());
		}
		return new ResponseEntity<AssignPhysicalCardResponseDTO>(responseDTO , HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/LoadCardList")
	public String showLoadCardList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
	try {
		String sessionId = (String) session.getAttribute("agentSessionId");
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					List<LoadCardList> lCardLists = agentApi.getLoadCardList(userSession.getUser() ,  null);
					if (lCardLists != null && !lCardLists.isEmpty()) {
						modelMap.addAttribute("cardList", lCardLists);
					}
					modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					modelMap.addAttribute("fromDate" , CommonUtil.getDefaultDateRange()[0]);
					modelMap.addAttribute("toDate" , CommonUtil.getDefaultDateRange()[1]);
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} else {
			return "redirect:/Agent/Home";
		}
	} catch (Exception e) {
		logger.error(e.getMessage());
		e.printStackTrace();
		return "Agent/LoadCardList";
	}
		return "Agent/LoadCardList";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/LoadCardList")
	public String showLoadCardListBasedOnDate(@ModelAttribute MUserDTO dto, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String daterange[] = null;
						if (!dto.getDaterange().isEmpty() && dto.getDaterange() != null) {
							daterange = CommonUtil.getDateRange(dto.getDaterange());
						} else {
							daterange = CommonUtil.getDefaultDateRange();
						}
						dto.setFromDate(daterange[0]);
						dto.setToDate(daterange[1]);
						List<LoadCardList> lCardLists = agentApi.getLoadCardList(userSession.getUser() , dto);
						modelMap.addAttribute("cardList", lCardLists);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						modelMap.addAttribute("fromDate" , dto.getFromDate());
						modelMap.addAttribute("toDate" , dto.getToDate());
						
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return "Agent/LoadCardList";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/AssignPhysicalCardRequestList")
	public String showPhysicalCardList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
	try {
		String sessionId = (String) session.getAttribute("agentSessionId");
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					List<PhysicalCardRequestListDTO> physicalCardRequestListDTOs = agentApi.getPhysicalCardAssignList(userSession.getUser() , null);
					if (physicalCardRequestListDTOs != null && !physicalCardRequestListDTOs.isEmpty()) {
						modelMap.addAttribute("physicalCardAssignList", physicalCardRequestListDTOs);
					}
					modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					modelMap.addAttribute("fromDate" , CommonUtil.getDefaultDateRange()[0]);
					modelMap.addAttribute("toDate" , CommonUtil.getDefaultDateRange()[1]);
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} else {
			return "redirect:/Agent/Home";
		}
	} catch (Exception e) {
		logger.error(e.getMessage());
		e.printStackTrace();
		return "Agent/AssignPhysicalCardRequestList";
	}
		return "Agent/AssignPhysicalCardRequestList";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/AssignPhysicalCardRequestList")
	public String showPhysicalCardListBasedOnDate(@ModelAttribute MUserDTO dto , ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
	try {
		String sessionId = (String) session.getAttribute("agentSessionId");
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = sessionApi.getActiveUserSession(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					String daterange[] = null;
					if (!dto.getDaterange().isEmpty() && dto.getDaterange() != null) {
						daterange = CommonUtil.getDateRange(dto.getDaterange());
					} else {
						daterange = CommonUtil.getDefaultDateRange();
					}
					dto.setFromDate(daterange[0]);
					dto.setToDate(daterange[1]);
					List<PhysicalCardRequestListDTO> physicalCardRequestListDTOs = agentApi.getPhysicalCardAssignList(userSession.getUser() , dto);
					if (physicalCardRequestListDTOs != null && !physicalCardRequestListDTOs.isEmpty()) {
						modelMap.addAttribute("physicalCardAssignList", physicalCardRequestListDTOs);
					}
					modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					modelMap.addAttribute("fromDate" , dto.getFromDate());
					modelMap.addAttribute("toDate" , dto.getToDate());
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} else {
			return "redirect:/Agent/Home";
		}
	} catch (Exception e) {
		logger.error(e.getMessage());
		e.printStackTrace();
		return "Agent/AssignPhysicalCardRequestList";
	}
		return "Agent/AssignPhysicalCardRequestList";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/ChangePassword")
	public String showChangePassword(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return "Agent/ChangePassword";
		}
		return "Agent/ChangePassword";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/ChangePassword")
	public String saveChangePassword(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session , @ModelAttribute ChangePasswordRequest dto ) {
		try {
			String sessionId = (String) session.getAttribute("agentSessionId");
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = sessionApi.getActiveUserSession(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						CommonError error = commonValidation.changePassword(dto);
						if (error.isValid()) {
							dto.setUsername(userSession.getUser().getUsername());
							agentApi.changePassword(dto);
							modelMap.addAttribute("success", "Successfully you have changed your password.");
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						} else {
							modelMap.addAttribute("error", error.getMessage());
							modelMap.addAttribute("agentBalance", userSession.getUser().getAccountDetail().getBalance());
						}
					} else {
						return "redirect:/Agent/Home";
					}
				} else {
					return "redirect:/Agent/Home";
				}
			} else {
				return "redirect:/Agent/Home";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			modelMap.addAttribute("error", e.getMessage());
			return "Agent/ChangePassword";
		}
		return "Agent/ChangePassword";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/ForgetPassword")
	public ResponseEntity<ForgetPasswordRequestDTO> forgetPassword(@RequestBody ForgotPasswordDTO dto , 
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		ForgetPasswordRequestDTO resp = new ForgetPasswordRequestDTO();
		try {
			CommonError error = commonValidation.forgetPasswordRequest(dto);
			if (error.isValid()) {
				MUser mUser = agentApi.checkAgentExist(dto.getUsername());
				if (mUser != null) {
					resp = agentApi.otpForForgetPassword(mUser);
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("User does not exist.");
				}
				
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(error.getMessage());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setMessage(ResponseStatus.BAD_REQUEST.getKey());
		}
		return new ResponseEntity<ForgetPasswordRequestDTO>(resp,  HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/ForgetPasswordWithOtp")
	public ResponseEntity<ForgetPasswordRequestDTO> forgetPasswordWithOTP(@RequestBody ChangePasswordRequest dto , 
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
		ForgetPasswordRequestDTO resp = new ForgetPasswordRequestDTO();
		try {
			CommonError error = commonValidation.forgetPasswordRequestWithOTP(dto);
			if (error.isValid()) {
				MUser mUser = agentApi.checkAgentExist(dto.getUsername());
				if (mUser != null) {
					resp = agentApi.forgetPasswordRequest(mUser, dto);
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("User does not exist.");
				}
				
			} else {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage(error.getMessage());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setMessage(ResponseStatus.BAD_REQUEST.getKey());
		}
		return new ResponseEntity<ForgetPasswordRequestDTO>(resp,  HttpStatus.OK);
	}*/
	
}
