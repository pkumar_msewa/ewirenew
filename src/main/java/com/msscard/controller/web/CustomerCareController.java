package com.msscard.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.msscard.app.api.IAuthenticationApi;
import com.msscard.entity.AdminAccessIdentifier;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.repositories.AdminAccessIdentifierRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;

@Controller
@RequestMapping("/Customer")
public class CustomerCareController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IAuthenticationApi authenticationApi;
	private final UserSessionRepository userSessionRepository;
	private final AdminAccessIdentifierRepository adminAccessIdentifierRepository;
	
	public CustomerCareController(UserSessionRepository userSessionRepository, IAuthenticationApi authenticationApi,AdminAccessIdentifierRepository adminAccessIdentifierRepository
			) {
		this.userSessionRepository = userSessionRepository;
		this.authenticationApi = authenticationApi;
		this.adminAccessIdentifierRepository= adminAccessIdentifierRepository;
	}

	@RequestMapping(value="/Login/Process", method = RequestMethod.GET)
	public String getLoginPage(HttpSession session, HttpServletRequest request, ModelMap model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if(sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if(authority != null) {
				if(authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					AdminAccessIdentifier access = adminAccessIdentifierRepository.getallaccess(userInfo);
					model.addAttribute("addUser", access.isAddUser());
					model.addAttribute("loadCard", access.isLoadCard());
					model.addAttribute("addCorporate", access.isAddCorporate());
					model.addAttribute("addMerchant", access.isAddMerchant());
					model.addAttribute("addAgent", access.isAddAgent());
					model.addAttribute("addDonation", access.isAddDonation());
					model.addAttribute("addGroup", access.isAddGroup());
					model.addAttribute("sendNotification", access.isSendNotification());
					model.addAttribute("assignPhysicalCard", access.isAssignPhysicalCard());
					model.addAttribute("assignVirtualCard", access.isAssignVirtualCard());
					model.addAttribute("androidVersion", access.isAndroidVersion());
					model.addAttribute("cardBlock", access.isCardBlock());

					return "redirect:Admin/Login/Process";		
				}
			}
		}
		return "Admin/CustomerCare";
	}
	
}
