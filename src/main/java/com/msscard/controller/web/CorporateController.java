package com.msscard.controller.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.runtime.parser.ParseException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.api.impl.MailSenderApi;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.DebitRequest;
import com.msscard.app.model.request.DeleteCorporateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.BulkRegister;
import com.msscard.entity.CorporateAgentDetails;
import com.msscard.entity.CorporateFileCatalogue;
import com.msscard.entity.CorporatePrefundHistory;
import com.msscard.entity.DownloadHistory;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MKycDetail;
import com.msscard.entity.MMCards;
import com.msscard.entity.MOperator;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PartnerDetails;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.entity.UserSession;
import com.msscard.model.AddPartnerDTO;
import com.msscard.model.BulkRegisterDTO;
import com.msscard.model.CorporateBulkUsers;
import com.msscard.model.DownloadCsv;
import com.msscard.model.MMCardsDTO;
import com.msscard.model.MTransactionDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.PrefundHistoryDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.RequestDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionListDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.UserDTO;
import com.msscard.model.UserType;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.BulkRegisterRepository;
import com.msscard.repositories.CorporateAgentDetailsRepository;
import com.msscard.repositories.CorporateFileCatalogueRepository;
import com.msscard.repositories.CorporatePrefundHistoryRepository;
import com.msscard.repositories.DownloadHistoryRepository;
import com.msscard.repositories.MKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PartnerDetailsRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.ExcelWriter;
import com.msscard.util.MailTemplate;
import com.msscard.util.SecurityUtil;
import com.msscard.util.StartUpUtil;
import com.msscard.validation.LoginValidation;
import com.msscard.validation.RegisterValidation;
import com.msscards.metadatas.URLMetadatas;
import com.msscards.session.PersistingSessionRegistry;
import com.msscards.session.SessionLoggingStrategy;

@Controller
@RequestMapping("/Corporate")
public class CorporateController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final ITransactionApi transactionApi;
	private final IMatchMoveApi matchMoveApi;
	private final CorporateFileCatalogueRepository corporateFileCatalogueRepository;
	private final BulkRegisterRepository bulkRegisterRepository;
	private final CorporateAgentDetailsRepository corporateAgentDetailsRepository;
	private final MMCardRepository mMCardRepository;
	private final CorporatePrefundHistoryRepository corporatePrefundHistoryRepository;
	private final MMCardRepository cardRepository;
	private final MServiceRepository mServiceRepository;
	private final RegisterValidation registerValidation;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MOperatorRepository mOperatorRepository;
	private final PartnerDetailsRepository partnerDetailsRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PasswordEncoder passwordEncoder;
	private final MKycRepository mKycRepository;
	private final MUserDetailRepository mUserDetailRepository;
	private final MTransactionRepository mTransactionRepository;
	private final DownloadHistoryRepository downloadHistoryRepository;
	private final MailSenderApi iMailSenderApi;

	public CorporateController(IUserApi userApi, AuthenticationManager authenticationManager,
			UserSessionRepository userSessionRepository, SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi, LoginValidation loginValidation, ITransactionApi transactionApi,
			IMatchMoveApi matchMoveApi, CorporateFileCatalogueRepository corporateFileCatalogueRepository,
			BulkRegisterRepository bulkRegisterRepository,
			CorporateAgentDetailsRepository corporateAgentDetailsRepository, MMCardRepository mMCardRepository,
			CorporatePrefundHistoryRepository corporatePrefundHistoryRepository, MMCardRepository cardRepository,
			MServiceRepository mServiceRepository, RegisterValidation registerValidation,
			MatchMoveWalletRepository matchMoveWalletRepository,
			PhysicalCardDetailRepository physicalCardDetailRepository, MOperatorRepository mOperatorRepository,
			PartnerDetailsRepository partnerDetailsRepository, PersistingSessionRegistry persistingSessionRegistry,
			PasswordEncoder passwordEncoder, MKycRepository mKycRepository, MUserDetailRepository mUserDetailRepository,
			MTransactionRepository mTransactionRepository, DownloadHistoryRepository downloadHistoryRepository,
			MailSenderApi iMailSenderApi) {
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.loginValidation = loginValidation;
		this.transactionApi = transactionApi;
		this.matchMoveApi = matchMoveApi;
		this.corporateFileCatalogueRepository = corporateFileCatalogueRepository;
		this.bulkRegisterRepository = bulkRegisterRepository;
		this.corporateAgentDetailsRepository = corporateAgentDetailsRepository;
		this.mMCardRepository = mMCardRepository;
		this.corporatePrefundHistoryRepository = corporatePrefundHistoryRepository;
		this.cardRepository = cardRepository;
		this.mServiceRepository = mServiceRepository;
		this.registerValidation = registerValidation;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.mOperatorRepository = mOperatorRepository;
		this.partnerDetailsRepository = partnerDetailsRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.passwordEncoder = passwordEncoder;
		this.mKycRepository = mKycRepository;
		this.mUserDetailRepository = mUserDetailRepository;
		this.mTransactionRepository = mTransactionRepository;
		this.downloadHistoryRepository = downloadHistoryRepository;
		this.iMailSenderApi = iMailSenderApi;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getHomess(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				double corporateBalance = userSession.getUser().getAccountDetail().getBalance();
				model.addAttribute("corporateBalance", corporateBalance);
				List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
						.getByCorporate(userSession.getUser());
				model.addAttribute("prefundList", prefundHistory);
				model.addAttribute("userType", Authorities.CORPORATE);
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/Home";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				model.addAttribute("userType", Authorities.CORPORATE_PARTNER);
				return "Corporate/Home";

			}
		}
		return "Corporate/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Home")
	public String adminLogin(@ModelAttribute("corporateLogin") LoginDTO adminDto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) {
		System.out.println("corporate Login");
		session = request.getSession();
		adminDto.setIpAddress("0.0.0.0");
		LoginError error = loginValidation.checkLoginValidation(adminDto);
		try {
			if (error.isSuccess()) {
				MUser user = userApi.findByUserName(adminDto.getUsername());
				if (user != null && user.getAuthority().contains(Authorities.CORPORATE)) {
					System.err.println("m in coprorate login");
					AuthenticationError authError = isValidUsernamePassword(adminDto, request);
					if (authError.isSuccess()) {
						AuthenticationError auth = authentication(adminDto, request);
						if (auth.isSuccess()) {
							session.setAttribute("firstName", user.getUserDetail().getFirstName());
							session.setAttribute("username", user.getUsername());
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							request.getSession().setAttribute("corporateSessionId", userSession.getSessionId());
							double corporateBalance = user.getAccountDetail().getBalance();
							model.addAttribute("corporateBalance", corporateBalance);
							List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
									.getByCorporate(user);
							model.addAttribute("prefundList", prefundHistory);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);

							model.addAttribute("username", userSession.getUser().getUsername());

							return "Corporate/Home";
						} else {
							model.addAttribute("loginMsg", "Invalid Username or Password");
							return "Corporate/Login";
						}
					} else {
						model.addAttribute("loginMsg", "Invalid Username or Password");
						return "Corporate/Login";
					}
				} else if (user != null && user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
					System.err.println("inside corp_partner");
					AuthenticationError authError = isValidUsernamePassword(adminDto, request);
					if (authError.isSuccess()) {
						AuthenticationError auth = authentication(adminDto, request);
						if (auth.isSuccess()) {
							session.setAttribute("firstName", user.getUserDetail().getFirstName());
							session.setAttribute("username", user.getUsername());
							Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
							sessionLoggingStrategy.onAuthentication(authentication, request, response);
							UserSession userSession = userSessionRepository.findByActiveSessionId(
									RequestContextHolder.currentRequestAttributes().getSessionId());
							request.getSession().setAttribute("corporateSessionId", userSession.getSessionId());
							double corporateBalance = user.getAccountDetail().getBalance();
							model.addAttribute("corporateBalance", corporateBalance);
							// List<CorporatePrefundHistory>
							// prefundHistory=corporatePrefundHistoryRepository.getByCorporate(user);
							// model.addAttribute("prefundList", prefundHistory);
							PartnerDetails partnerDetails = partnerDetailsRepository
									.getPartnerDetails(userSession.getUser());
							if (partnerDetails != null) {
								List<MService> partnerServices = partnerDetails.getPartnerServices();
								if (partnerServices != null && !partnerServices.isEmpty()) {
									for (MService mService : partnerServices) {
										if (mService.getCode().equalsIgnoreCase("BRC")) {
											model.addAttribute("bulkRegistration", true);
										} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
											model.addAttribute("prefundC", true);
										} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
											model.addAttribute("BulkCL", true);
										} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
											model.addAttribute("BulkCardIssuance", true);
										} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
											model.addAttribute("SingleCardLoad", true);
										} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
											model.addAttribute("CardBlockUnblock", true);
										} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
											model.addAttribute("singleCardAssignment", true);

										}
									}
								}
							}
							model.addAttribute("UserType", false);
							model.addAttribute("username", userSession.getUser().getUsername());

							return "Corporate/Home";
						} else {
							model.addAttribute("loginMsg", "Invalid Username or Password");
							return "Corporate/Login";
						}
					} else {
						model.addAttribute("loginMsg", authError.getMessage());
						return "Corporate/Login";
					}
				} else {
					model.addAttribute("loginMsg", "Invalid Username or Password");
					return "Corporate/Login";
				}
			} else {
				session.setAttribute("loginType", "Corporate");
				model.addAttribute("loginMsg", error.getMessage());
				return "Corporate/Login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("loginMsg", "Invalid Username or Password");
			return "redirect:/Corporate/Home";
		}
	}

	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage("Inavlid Password");
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setSuccess(false);
			error.setMessage(e.getMessage());
			return error;
		}
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				// userApi.handleLoginSuccess(request, null, auth,
				// String.valueOf(token.getPrincipal()), dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				// error.setMessage(userApi.handleLoginFailure(request, null,
				// auth, String.valueOf(token.getPrincipal()),
				// dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			// error.setMessage(userApi.handleLoginFailure(request, null, auth,
			// String.valueOf(token.getPrincipal()), dto.getIpAddress()));
			return error;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CardList")
	public String getCardList(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				List<MMCards> card = userApi.findAllCards();
				List<MMCardsDTO> cards = new ArrayList<>();
				for (MMCards mmCards : card) {
					MMCardsDTO car = new MMCardsDTO();
					car.setCardId(mmCards.getCardId());
					car.setContactNO(mmCards.getWallet().getUser().getUserDetail().getContactNo());
					car.setEmail(mmCards.getWallet().getUser().getUserDetail().getEmail());
					car.setUserName(mmCards.getWallet().getUser().getUsername());
					car.setStatus(mmCards.getStatus());
					car.setIssueDate(mmCards.getCreated() + "");
					car.setFirstName(mmCards.getWallet().getUser().getUserDetail().getFirstName());
					car.setLastName(mmCards.getWallet().getUser().getUserDetail().getLastName());
					cards.add(car);
				}
				session.setAttribute("cardList", cards);
				return "Admin/CardList";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Transactions")
	public String getCardTransactions(ModelMap modelMap, DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				List<MTransactionDTO> cards = new ArrayList<>();
				for (int i = 0; i <= 50; i++) {
					MTransactionDTO car = new MTransactionDTO();
					car.setAmount("1000");
					car.setAuthReferenceNo("987987123");
					car.setCardId("msscard12345");
					car.setEmail("user@cashier.in");
					car.setName("User One");
					car.setMerchantId("Reliance Digital");
					car.setTransactionDate("24/02/2018");
					car.setTransactionRefNo("123123123431");
					cards.add(car);
				}
				session.setAttribute("cardTransList", cards);
				return "Corporate/CardTransaction";
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Card/Transactions")
	public String getCardTransactionsPost(ModelMap modelMap, DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				if (dto.getFromDate() == null || dto.getToDate() == null || dto.getFromDate().equalsIgnoreCase("")
						|| dto.getToDate().equalsIgnoreCase("")) {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					dto.setToDate(dateText);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					dto.setFromDate(df2.format(cal.getTime()));
				}
				dto.setServiceName("");
				List<MTransaction> card = userApi.findAllCardTransactioons(dto);
				List<MTransactionDTO> cards = new ArrayList<>();
				for (MTransaction mmCards : card) {
					MTransactionDTO car = new MTransactionDTO();
					car.setAmount(mmCards.getAmount() + "");
					car.setAuthReferenceNo(mmCards.getAuthReferenceNo());
					car.setDebit(mmCards.isDebit() + "");
					car.setDescription(mmCards.getDescription());
					MUser usera = userApi.findByUserAccount(mmCards.getAccount());
					MMCards cardd = userApi.findCardByUser(usera);
					car.setCardId(cardd.getCardId());
					car.setEmail(usera.getUserDetail().getEmail());
					car.setName(usera.getUserDetail().getFirstName() + usera.getUserDetail().getLastName());
					car.setRequest(mmCards.getRequest());
					car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
					car.setStatus(mmCards.getStatus() + "");
					car.setTransactionDate(mmCards.getCreated() + "");
					car.setTransactionRefNo(mmCards.getTransactionRefNo());
					cards.add(car);
				}
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				session.setAttribute("cardTransList", cards);
				return "Admin/CardTransaction";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {

				if (dto.getFromDate() == null || dto.getToDate() == null || dto.getFromDate().equalsIgnoreCase("")
						|| dto.getToDate().equalsIgnoreCase("")) {
					Date date = new Date();
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String dateText = df2.format(date);
					dto.setToDate(dateText);
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					dto.setFromDate(df2.format(cal.getTime()));
				}
				dto.setServiceName("");
				List<MTransaction> card = userApi.findAllCardTransactioons(dto);
				List<MTransactionDTO> cards = new ArrayList<>();
				for (MTransaction mmCards : card) {
					MTransactionDTO car = new MTransactionDTO();
					car.setAmount(mmCards.getAmount() + "");
					car.setAuthReferenceNo(mmCards.getAuthReferenceNo());
					car.setDebit(mmCards.isDebit() + "");
					car.setDescription(mmCards.getDescription());
					MUser usera = userApi.findByUserAccount(mmCards.getAccount());
					MMCards cardd = userApi.findCardByUser(usera);
					car.setCardId(cardd.getCardId());
					car.setEmail(usera.getUserDetail().getEmail());
					car.setName(usera.getUserDetail().getFirstName() + usera.getUserDetail().getLastName());
					car.setRequest(mmCards.getRequest());
					car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
					car.setStatus(mmCards.getStatus() + "");
					car.setTransactionDate(mmCards.getCreated() + "");
					car.setTransactionRefNo(mmCards.getTransactionRefNo());
					cards.add(car);
				}
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				session.setAttribute("cardTransList", cards);

				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				return "Admin/CardTransaction";

			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String getLogout(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				sessionApi.expireSession(sessionId);

				return "redirect:/Corporate/Home";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				sessionApi.expireSession(sessionId);
				return "redirect:/Corporate/Home";
			}
		}
		return "redirect:/Corporate/Home";
	}

	/**
	 * BULK REGISTER
	 */

	@RequestMapping(value = "/BulkRegister", method = RequestMethod.GET)
	public String getBulkRegister(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		List<MBulkRegister> lis = userApi.findLast10Register();
		List<BulkRegisterDTO> cards = new ArrayList<>();
		for (int i = 0; i < lis.size(); i++) {
			BulkRegisterDTO car = new BulkRegisterDTO();
			car.setEmail(lis.get(i).getEmail());
			car.setName(lis.get(i).getName());
			car.setDateOfBirth(lis.get(i).getDateOfBirth() + "");
			car.setContactNo(lis.get(i).getContactNo());
			car.setRegistrationDate(lis.get(i).getCreated() + "");
			cards.add(car);
		}
		model.addAttribute("cardTransList", cards);
		return "Corporate/BulkRegister";
	}

	/**
	 * BULK TRANSFER
	 */

	@RequestMapping(value = "/BulkTransfer", method = RequestMethod.GET)
	public String getBulkTransfer(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		List<MTransactionDTO> cards = new ArrayList<>();
		for (int i = 0; i <= 50; i++) {
			MTransactionDTO car = new MTransactionDTO();
			car.setAmount("1000");
			car.setAuthReferenceNo("987987123");
			car.setCardId("msscard12345");
			car.setEmail("user@cashier.in");
			car.setName("User One");
			car.setMerchantId("Reliance Digital");
			car.setTransactionDate("24/02/2018");
			car.setTransactionRefNo("123123123431");
			cards.add(car);
		}
		model.addAttribute("cardTransList", cards);
		return "Corporate/BulkTransfer";
	}

	@RequestMapping(value = "/BulkCardGenerate", method = RequestMethod.GET)
	public String getBulkT(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {

		return "Corporate/BulkCardCreation";
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/download/bulktransfer", method = RequestMethod.GET)
	public void getDownloadBulkTransferFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulktransfer.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/download/bulkKYC", method = RequestMethod.GET)
	public void getDownloadBulkKYCFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkKYCRegister.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/download/bulkCardCreation", method = RequestMethod.GET)
	public void getBulkCardCreation(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkCardCreation.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/download/bulkregister", method = RequestMethod.GET)
	public void getDownloadBulkRegistationFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		PrintWriter out = res.getWriter();
		String fileName = "bulkregisterCorp.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}

	/**
	 * BULK REGISTER
	 * 
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/BulkRegister")
	public String submitRequestRefund(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {

		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						try {
							String s3Path = CommonUtil.uploadCsv1(dto.getFile(), 123456 + "", "BLKREGISTER",
									URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKREGISTER_CORPORATE);
							System.err.println("the s3 path::" + s3Path);
							System.err.println("the username::" + userSession.getUser().getUsername());
							CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
							fileCatalogue.setAbsPath(s3Path);
							fileCatalogue.setCorporate(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKREGISTER");
							corporateFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);

							model.addAttribute("username", userSession.getUser().getUsername());

							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
						} catch (Exception e) {
							e.printStackTrace();
						}
						return "Corporate/BulkRegister";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						PartnerDetails partnerDetails = partnerDetailsRepository
								.getPartnerDetails(userSession.getUser());

						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKREGISTER");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);

						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
						fileCatalogue.setS3Path(s3Path);
						if (partnerDetails != null) {
							fileCatalogue.setPartnerDetails(partnerDetails);
							fileCatalogue.setCorporate(partnerDetails.getCorporate().getCorporate());
						}
						fileCatalogue.setCategoryType("BLKREGISTER");
						corporateFileCatalogueRepository.save(fileCatalogue);

						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
					}

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkRegister";

				}
			}
		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateBulkRegister")
	public String bulkRegster(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkRegister";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {
					System.err.println("i am in partner");
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkRegister";
				}
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkCardLoad")
	public String bulkCardLoad(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv1(dto.getFile(), 123456 + "", "BLKCARDLOAD",
								URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKCARDLOAD_CORPORATE);
						System.err.println("the s3 path::" + s3Path);
						
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(s3Path);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKCARDLOAD);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKCARDLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("username", userSession.getUser().getUsername());

						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Card Load has been taken.Please contact your admin for approval");
						return "Corporate/BulkCardCreation";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKCARDLOAD");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKCARDLOAD);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKCARDLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
					}

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}

					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkCardCreation";

				}
			}
		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateBulkCardLoad")
	public String bulkCardLoad(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkCardCreation";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkCardCreation";

				}
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserReport")
	public String userReport(@ModelAttribute PagingDTO page, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());
					model.addAttribute("singleCardAssignment", true);
					String[] daterange = null;
					try {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = CommonUtil.dateFormate.parse(daterange[0]);
						Date toDate = CommonUtil.dateFormate.parse(daterange[1]);
						String pardF = CommonUtil.dateFormate.format(fromDate);
						String pardT = CommonUtil.dateFormate.format(toDate);

						page.setFromDate(fromDate);
						page.setToDate(toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);

						System.out.println(fromDate +" "+toDate);
						CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						List<BulkRegister> corpDetails = bulkRegisterRepository
								.getRegisteredUsersByDate(agentDetails.getId(), page.getFromDate(), page.getToDate());
						List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
						if (corpDetails != null) {
							for (BulkRegister bulkRegister : corpDetails) {
								if (bulkRegister.getUser() != null) {
									CorporateBulkUsers blkUser = new CorporateBulkUsers();
									MMCards phyCard1 = mMCardRepository.getPhysicalCardByUser(bulkRegister.getUser());
									if (phyCard1 != null) {
										blkUser.setMobile(bulkRegister.getMobile());
										blkUser.setDob(bulkRegister.getDob());
										blkUser.setEmail(bulkRegister.getEmail());
										blkUser.setUsername(bulkRegister.getFirstName());
										blkUser.setAuthority(bulkRegister.getUser().getAuthority());
										blkUser.setUser(bulkRegister.getUser());
										blkUser.setDriver_id(bulkRegister.getDriver_id());
										blkUser.setKycStatus(
												bulkRegister.getUser().getAccountDetail().getAccountType().getCode());
										blkUser.setPhysicalCardCreationStatus(true);
										blkUser.setCardNumber(phyCard1.getCardId());
										blkUser.setCardStatus(phyCard1.getStatus());
									}

									bulkUsers.add(blkUser);
								}
							}
							model.addAttribute("userList", bulkUsers);
							return "Corporate/UserReport";
						}
						model.addAttribute("userList", bulkUsers);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Corporate/UserReport";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					String[] daterange = null;
					try {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = CommonUtil.formatter.parse(daterange[0]);
						Date toDate = CommonUtil.formatter.parse(daterange[1]);
						String pardF = CommonUtil.formatter.format(fromDate);
						String pardT = CommonUtil.formatter.format(toDate);
						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
						// }
					} catch (Exception e) {
						e.printStackTrace();
					}

					PartnerDetails agentDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					List<BulkRegister> corpDetails = bulkRegisterRepository.getRegisteredUserPartner(page.getFromDate(),
							page.getToDate(), agentDetails);
					List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
					if (corpDetails != null && !corpDetails.isEmpty()) {
						for (BulkRegister bulkRegister : corpDetails) {
							if (bulkRegister.getUser() != null) {
								CorporateBulkUsers blkUser = new CorporateBulkUsers();

								if (true) {
									MMCards phyCard = mMCardRepository.getPhysicalCardByUser(bulkRegister.getUser());
									if (phyCard != null) {

										blkUser.setMobile(bulkRegister.getMobile());
										blkUser.setDob(bulkRegister.getDob());
										blkUser.setEmail(bulkRegister.getEmail());
										blkUser.setUsername(bulkRegister.getName());
										blkUser.setAuthority(bulkRegister.getUser().getAuthority());
										blkUser.setUser(bulkRegister.getUser());
										blkUser.setDriver_id(bulkRegister.getDriver_id());
										blkUser.setKycStatus(
												bulkRegister.getUser().getAccountDetail().getAccountType().getCode());
										System.err.println("hiiii..");
										blkUser.setPhysicalCardCreationStatus(true);

										System.err.println(phyCard.getCardId());
										blkUser.setCardNumber(phyCard.getCardId());
										blkUser.setCardStatus(phyCard.getStatus());
									}

								}
								bulkUsers.add(blkUser);

							}
						}
						System.err.println(bulkUsers);
						model.addAttribute("userList", bulkUsers);
						return "Corporate/UserReport";
					}
					System.err.println(bulkUsers);

					model.addAttribute("userList", bulkUsers);

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/UserReport";
				}

			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserReport")
	public String userReportPost(@ModelAttribute PagingDTO page, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.CORPORATE)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("username", userSession.getUser().getUsername());

						SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
						System.err.println("the date reange is :::::" + page.getDaterange());
						System.err.println("the status is :::::::::" + page.getCardStatus());
						String[] daterange = null;
						try {
							if (page.getDaterange().length() != 0) {
								System.err.println(page.getDaterange().length());
								String aaaaa = page.getDaterange().substring(0, 10);
								String baaaa = page.getDaterange().substring(13);
								Date from = sdformat.parse(aaaaa + " 00:00:00");
								Date to = sdformat.parse(baaaa + " 00:00:00");
								page.setFromDate(from);
								page.setToDate(to);

							} else {

								daterange = CommonUtil.getDefaultDateRange();
								Date fromDate = df2.parse(daterange[0]);
								Date toDate = df2.parse(daterange[1]);
								String pardF = df2.format(fromDate);
								String pardT = df2.format(toDate);

								page.setFromDate(fromDate);
								page.setToDate(toDate);
								System.err.println(fromDate + "-" + toDate);
								model.addAttribute("dateRange", pardF + "-" + pardT);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						List<BulkRegister> corpDetails = bulkRegisterRepository
								.getRegisteredUsersByDate(agentDetails.getId(), page.getFromDate(), page.getToDate());
						List<CorporateBulkUsers> bulkUsers = new ArrayList<>();
						if (corpDetails != null) {
							for (BulkRegister bulkRegister : corpDetails) {
								if (bulkRegister.getUser() != null) {
									CorporateBulkUsers blkUser = new CorporateBulkUsers();
									if (true) {
										MMCards phyCard1 = mMCardRepository
												.getPhysicalCardByUser(bulkRegister.getUser());
										if (phyCard1 != null) {
											blkUser.setMobile(bulkRegister.getMobile());
											blkUser.setDob(bulkRegister.getDob());
											blkUser.setEmail(bulkRegister.getEmail());
											blkUser.setUsername(bulkRegister.getName());
											blkUser.setAuthority(bulkRegister.getUser().getAuthority());
											blkUser.setUser(bulkRegister.getUser());
											blkUser.setDriver_id(bulkRegister.getDriver_id());
											blkUser.setKycStatus(bulkRegister.getUser().getAccountDetail()
													.getAccountType().getCode());
											System.err.println("hiiii..");
											blkUser.setPhysicalCardCreationStatus(true);
											System.err.println(phyCard1.getCardId());
											blkUser.setCardNumber(phyCard1.getCardId());
											blkUser.setCardStatus(phyCard1.getStatus());
										}
									}
									bulkUsers.add(blkUser);
								}
							}
							System.err.println(bulkUsers);
							model.addAttribute("userList", bulkUsers);
							return "Corporate/UserReport";
						}
						System.err.println(bulkUsers);
						model.addAttribute("userList", bulkUsers);
						return "Corporate/UserReport";
					} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Corporate/Home";
	}

	private String saveRefundReport(String rootDirectory, MultipartFile file, String code) {
		String filePath = null;
		String fileName = String.valueOf(System.currentTimeMillis());
		File dirs = new File(rootDirectory + "/resources/register/" + fileName + "_" + file.getOriginalFilename());
		dirs.mkdirs();
		try {
			file.transferTo(dirs);
			filePath = "/resources/register/" + fileName + "_" + file.getOriginalFilename();
			return filePath + "#" + fileName + "_" + file.getOriginalFilename();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filePath;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CorporatePrefund")
	public String corporatePrefund(@ModelAttribute PrefundHistoryDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());
					String s3Path = CommonUtil.uploadCorporateLogo(dto.getFile(), user.getUserId(), "PREFUNDREQ");
					
					CorporatePrefundHistory prefund = new CorporatePrefundHistory();
					prefund.setAmount(dto.getAmount());
					prefund.setCorporate(userSession.getUser());
					prefund.setCorporateName(dto.getClientName());
					prefund.setFilePath(s3Path);
					prefund.setTransactionRefNo(dto.getTransactionRefNo());
					corporatePrefundHistoryRepository.save(prefund);
					iMailSenderApi.sendPrefundAlert("Prefund Request", MailTemplate.PREFUND_REQUEST,
                             userSession.getUser(), dto.getAmount(),dto.getClientName(),dto.getTransactionRefNo());
					model.addAttribute("successMsg",
							"Your Request has been submitted.Amount will be reflected shortly in your account upon confirmation.Contact your admin for other queries.");
					return "Corporate/Prefund";
				}
			}

		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Prefund")
	public String getcorporatePrefund(@ModelAttribute PrefundHistoryDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/Prefund";
				}
			}

		}
		return "redirect:/Corporate/Home";

	}

	public Set<BulkRegisterDTO> readFromFile(String fileName) {
		Set<BulkRegisterDTO> sendMoneyList = new HashSet<>();
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName));
			BulkRegisterDTO dto = null;
			while ((line = br.readLine()) != null) {
				Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
				Matcher m = p.matcher(line);
				dto = new BulkRegisterDTO();
				String value = null;
				int index = 1;
				while (m.find()) {
					if (m.group(2) != null) {
						value = m.group(2);
					}
					if (m.group(3) != null) {
						value = m.group(3);
					}
					if (value != null) {
						if (dto != null) {
							switch (index) {
							case 1:
								dto.setFirstName(value);
								break;
							case 2:
								dto.setContactNo(value);
								break;
							case 3:
								dto.setEmail(value);
								break;
							case 4:
								// dto.setK
								break;
							case 5:
								dto.setDateOfBirth(value);
								;
								break;
							default:
								break;
							}
							index = index + 1;
						}
					}
					sendMoneyList.add(dto);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sendMoneyList;
	}

	public static void main(String[] args) {
		String d = "/mmCards.csv#mmCards.csv";
		String[] s = d.split("#");
		System.err.println(s[1]);
	}

	@RequestMapping(value = "/Status/block/unblock", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> userStatus(@RequestBody RequestDTO cardRequest, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				// persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApi.findByUserName(cardRequest.getUserName());
				System.err.println("auth" + cardRequest.getAuthority());
				userApi.updateUserAuthority(cardRequest.getAuthority(), use.getId());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateSingleCardLoad")
	public String getCorporateSinleCardLoad(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/CorporateSinleCardLoad";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);
							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/CorporateSinleCardLoad";
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateSingleCardAssignment")
	public String getCorporateSingleCardAssignment(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/CorporateSingleCardAssignment";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
					&& user.getAuthority().contains(Authorities.USER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/CorporateSingleCardAssignment";
			}

		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddSingleUser")
	public String addUser(ModelMap modelMap, @ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			MUser agent = userApi.findByUserName(user.getUsername());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				model.addAttribute("CardBlockUnblock", true);

				dto.setUsername(dto.getContactNo());
				RegisterError registerError = registerValidation.validateNormalUser(dto);
				if (registerError.isValid()) {
					System.err.println(dto);
					dto.setUserType(UserType.User);
					try {
						boolean val = userApi.saveUserFromCorporate(dto);
						if (val) {
							BulkRegister bulkRegister = new BulkRegister();
							if (!dto.isPhysicalCard()) {
								MUser user1 = userApi.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);
									WalletResponse walletResponse = matchMoveApi.assignVirtualCard(user1);
									if (walletResponse.getCode().equalsIgnoreCase("S00")) {
										map.put("regmessage", "User added Sucessfully and virtual card assigned. ");
										CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
												.getByCorporateId(agent);
										MUser uuu = userApi.findByUserName(dto.getContactNo());
										bulkRegister.setCardNo("");
										bulkRegister.setDob(dto.getDateOfBirth());
										bulkRegister.setEmail(dto.getEmail());
										bulkRegister.setMobile(dto.getContactNo());
										bulkRegister.setProxyNo("");
										bulkRegister.setUser(uuu);
										bulkRegister.setDriver_id(dto.getDriver_id());

										bulkRegister.setAgentDetails(corpDetails);
										bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
										bulkRegister.setCardCreationStatus(true);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegisterRepository.save(bulkRegister);
									} else {
										map.put("regmessage", "User added Sucessfully but virtual card not assigned. ");
										CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
												.getByCorporateId(agent);
										MUser uuu = userApi.findByUserName(dto.getContactNo());
										bulkRegister.setCardNo("");
										bulkRegister.setDob(dto.getDateOfBirth());
										bulkRegister.setEmail(dto.getEmail());
										bulkRegister.setMobile(dto.getContactNo());
										bulkRegister.setProxyNo("");
										bulkRegister.setUser(uuu);
										bulkRegister.setDriver_id(dto.getDriver_id());

										bulkRegister.setAgentDetails(corpDetails);
										bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
										bulkRegister.setCardCreationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegister.setPhyCardActivationStatus(false);
										bulkRegisterRepository.save(bulkRegister);
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							} else {

								MUser user1 = userApi.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);
									WalletResponse walletResponse = new WalletResponse();
									MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
									if (wallet != null) {
										PhysicalCardDetails physicalCard = new PhysicalCardDetails();
										physicalCard.setFromAdmin(true);
										physicalCard.setWallet(wallet);
										physicalCardDetailRepository.save(physicalCard);
										walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getProxyNumber());
										if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
											PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
											card.setStatus(Status.Received);
											physicalCardDetailRepository.save(card);
											ResponseDTO respo = matchMoveApi
													.activationPhysicalCard(card.getActivationCode(), user1);
											if (respo.getCode().equalsIgnoreCase("S00")) {
												map.put("sucMsg", "Physical card assigned.");
												PhysicalCardDetails card1 = physicalCardDetailRepository
														.findByUser(user1);
												card.setStatus(Status.Active);
												physicalCardDetailRepository.save(card1);
												CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
														.getByCorporateId(agent);
												MUser uuu = userApi.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setDriver_id(dto.getDriver_id());
												bulkRegister.setUser(uuu);
												bulkRegister.setAgentDetails(corpDetails);
												bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegisterRepository.save(bulkRegister);
												map.put("regmessage",
														"User added Sucessfully and physical card assigned. ");
											} else {
												map.put("regmessage",
														"User added Sucessfully but physical card not assigned. ");
												CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
														.getByCorporateId(agent);
												MUser uuu = userApi.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setUser(uuu);
												bulkRegister.setDriver_id(dto.getDriver_id());

												bulkRegister.setAgentDetails(corpDetails);
												bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegisterRepository.save(bulkRegister);
											}
										} else {
											map.put("regmessage",
													"User added Sucessfully but physical card not assigned. ");
											CorporateAgentDetails corpDetails = corporateAgentDetailsRepository
													.getByCorporateId(agent);
											MUser uuu = userApi.findByUserName(dto.getContactNo());
											bulkRegister.setCardNo("");
											bulkRegister.setDob(dto.getDateOfBirth());
											bulkRegister.setEmail(dto.getEmail());
											bulkRegister.setMobile(dto.getContactNo());
											bulkRegister.setProxyNo(dto.getProxyNumber());
											bulkRegister.setUser(uuu);
											bulkRegister.setDriver_id(dto.getDriver_id());

											bulkRegister.setAgentDetails(corpDetails);
											bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
											bulkRegister.setCardCreationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegisterRepository.save(bulkRegister);
										}

									} else {
										map.put("regmessage", "failed, Please try again later.");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							}
						} else {
							map.put("regmessage", "User not added.");
						}
					} catch (Exception e) {
						e.printStackTrace();
						map.put("regmessage", "User not added please try again later");
					}
				} else {
					map.put("regmessage", registerError.getMessage());
				}
				return "Corporate/CorporateSingleCardAssignment";
			} else if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

				dto.setUsername(dto.getContactNo());
				RegisterError registerError = registerValidation.validateNormalUser(dto);
				if (registerError.isValid()) {
					System.err.println(dto);
					dto.setUserType(UserType.User);
					try {
						boolean val = userApi.saveUserFromCorporate(dto);
						if (val) {
							BulkRegister bulkRegister = new BulkRegister();
							if (!dto.isPhysicalCard()) {
								MUser user1 = userApi.findByUserName(dto.getContactNo());
								ResponseDTO resp = matchMoveApi.createUserOnMM(user1);
								if (resp.getCode().equalsIgnoreCase("S00")) {
									bulkRegister.setUserCreationStatus(true);
									bulkRegister.setWalletCreationStatus(true);
									WalletResponse walletResponse = new WalletResponse();
									MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user1);
									if (wallet != null) {
										PhysicalCardDetails physicalCard = new PhysicalCardDetails();
										physicalCard.setFromAdmin(true);
										physicalCard.setWallet(wallet);
										physicalCardDetailRepository.save(physicalCard);
										walletResponse = matchMoveApi.assignPhysicalCard(user1, dto.getProxyNumber());
										if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
											PhysicalCardDetails card = physicalCardDetailRepository.findByUser(user1);
											card.setStatus(Status.Received);
											physicalCardDetailRepository.save(card);
											ResponseDTO respo = matchMoveApi
													.activationPhysicalCard(card.getActivationCode(), user1);
											if (respo.getCode().equalsIgnoreCase("S00")) {
												map.put("sucMsg", "Physical card assigned.");
												PhysicalCardDetails card1 = physicalCardDetailRepository
														.findByUser(user1);
												card.setStatus(Status.Active);
												physicalCardDetailRepository.save(card1);
												PartnerDetails partnerDetails = partnerDetailsRepository
														.getPartnerDetails(agent);
												MUser uuu = userApi.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setDriver_id(dto.getDriver_id());
												bulkRegister.setUser(uuu);
												if (partnerDetails != null) {

													bulkRegister.setAgentDetails(partnerDetails.getCorporate());
													bulkRegister.setPartnerDetails(partnerDetails);
												}
												bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegister.setPhyCardActivationStatus(true);
												bulkRegisterRepository.save(bulkRegister);
												map.put("regmessage",
														"User added Sucessfully and physical card assigned. ");
											} else {
												map.put("regmessage",
														"User added Sucessfully but physical card not assigned. ");
												PartnerDetails partnerDetails = partnerDetailsRepository
														.getPartnerDetails(agent);
												MUser uuu = userApi.findByUserName(dto.getContactNo());
												bulkRegister.setCardNo("");
												bulkRegister.setDob(dto.getDateOfBirth());
												bulkRegister.setEmail(dto.getEmail());
												bulkRegister.setMobile(dto.getContactNo());
												bulkRegister.setProxyNo(dto.getProxyNumber());
												bulkRegister.setUser(uuu);
												bulkRegister.setDriver_id(dto.getDriver_id());
												if (partnerDetails != null) {

													bulkRegister.setAgentDetails(partnerDetails.getCorporate());
													bulkRegister.setPartnerDetails(partnerDetails);
												}
												bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
												bulkRegister.setCardCreationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegister.setPhyCardActivationStatus(false);
												bulkRegisterRepository.save(bulkRegister);
											}
										} else {
											map.put("regmessage",
													"User added Sucessfully but physical card not assigned. ");
											PartnerDetails partnerDetails = partnerDetailsRepository
													.getPartnerDetails(agent);

											MUser uuu = userApi.findByUserName(dto.getContactNo());
											bulkRegister.setCardNo("");
											bulkRegister.setDob(dto.getDateOfBirth());
											bulkRegister.setEmail(dto.getEmail());
											bulkRegister.setMobile(dto.getContactNo());
											bulkRegister.setProxyNo(dto.getProxyNumber());
											bulkRegister.setUser(uuu);
											bulkRegister.setDriver_id(dto.getDriver_id());
											if (partnerDetails != null) {

												bulkRegister.setAgentDetails(partnerDetails.getCorporate());
												bulkRegister.setPartnerDetails(partnerDetails);
											}
											bulkRegister.setFirstName(dto.getFirstName() + " " + dto.getLastName());
											bulkRegister.setCardCreationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegister.setPhyCardActivationStatus(false);
											bulkRegisterRepository.save(bulkRegister);
										}

									} else {
										map.put("regmessage", "failed, Please try again later.");
									}
								} else {
									map.put("regmessage",
											"User added Sucessfully but account not created in matchmove global.");
								}
							}
						} else {
							map.put("regmessage", "User not added.");
						}
					} catch (Exception e) {
						e.printStackTrace();
						map.put("regmessage", "User not added please try again later");
					}
				} else {
					map.put("regmessage", registerError.getMessage());
				}

				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());
				return "Corporate/CorporateSingleCardAssignment";

			}
			return "redirect:/Corporate/Home";
		}
		return "redirect:/Corporate/Home";
	}

	/**
	 * ADD PARTNER IN CORPORATE
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/AddCorporatePartner")
	public String getcorporatePrefund(@ModelAttribute AddPartnerDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					if (dto.getServices() != null && !dto.getServices().isEmpty()) {
						List<MService> serviceList = new ArrayList<>();
						for (String serviceCode : dto.getServices()) {
							MService service = mServiceRepository.findServiceByCode(serviceCode);
							if (service != null) {
								serviceList.add(service);
							}
						}
						RegisterDTO registerPartner = new RegisterDTO();
						registerPartner.setEmail(dto.getPartnerEmail());
						registerPartner.setContactNo(dto.getPartnerMobile());
						registerPartner.setPartnerServices(serviceList);
						registerPartner.setFirstName(dto.getPartnerName());
						registerPartner.setMaxLoadCardLimit(dto.getLoadCardMaxLimit());
						CorporateAgentDetails agent = corporateAgentDetailsRepository
								.getByCorporateId(userSession.getUser());
						registerPartner.setAgentDetails(agent);
						boolean success = userApi.saveCorporatePartner(registerPartner);
						if (success) {
							MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
							if (operator != null) {
								List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);
								model.addAttribute("serviceList", listOperators);
								model.addAttribute("message", "Partner Registered Successfully");
								return "Corporate/AddPartner";
							}
						} else {
							MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
							if (operator != null) {
								List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);

								model.addAttribute("serviceList", listOperators);

								model.addAttribute("message", "Partner Registration Failed");
								return "Corporate/AddPartner";

							}
						}
					}
					model.addAttribute("message", "Partner Registration Failed");

					return "Corporate/AddPartner";
				}
			}

		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddCorporatePartner")
	public String getcorporatePrefundGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
					if (operator != null) {
						List<MService> listOperators = mServiceRepository.getServicesByOperator(operator);
						model.addAttribute("serviceList", listOperators);
						return "Corporate/AddPartner";
					}
				} else {
					return "redirect:/Corporate/Home";
				}
			} else {
				return "redirect:/Corporate/Home";
			}

		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListCorporatePartner")
	public String listCorporatePartner(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					CorporateAgentDetails agentDetails = corporateAgentDetailsRepository
							.getByCorporateId(userSession.getUser());
					if (agentDetails != null) {
						List<String> serviceList = new ArrayList<>();
						List<MService> pseudoServices = new ArrayList<>();
						List<MService> listOperators = null;
						MOperator operator = mOperatorRepository.findOperatorByName("CORP_AGENT");
						if (operator != null) {
							listOperators = mServiceRepository.getServicesByOperator(operator);
						}
						List<PartnerDetails> listPartnerDetails = partnerDetailsRepository
								.getPartnerDetails(agentDetails, Status.Active);
						if (listPartnerDetails != null) {
							for (PartnerDetails partnerDetails : listPartnerDetails) {
								for (MService servicesList : partnerDetails.getPartnerServices()) {

									serviceList.add(servicesList.getDescription());
									pseudoServices.add(servicesList);
									System.err.println(servicesList.getCode());
								}

							}
							// System.err.println(someService.size());
							model.addAttribute("pseudoServices", pseudoServices);
							model.addAttribute("serviceList", serviceList);
							model.addAttribute("partnerDetailsList", listPartnerDetails);
							model.addAttribute("serviceExcluded", listOperators);
							model.addAttribute("mesg", model.get("mesg"));
							return "Corporate/ListPartner";
						}
					}
				}

			} else {
				return "redirect:/Corporate/Home";
			}

		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/SingleCard/{email}/{cardId}/{contactNo}")
	public String getSingleCard(@PathVariable(value = "email") String email,
			@PathVariable(value = "cardId") String cardId, @PathVariable(value = "contactNo") String contactNo,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				MUser use = userApi.findByUserName(contactNo);

				persistingSessionRegistry.refreshLastRequest(sessionId);
				MatchMoveCreateCardRequest cardFetch = new MatchMoveCreateCardRequest();
				cardFetch.setCardId(cardId);
				cardFetch.setEmail(use.getUserDetail().getEmail());
				try {
					cardFetch.setPassword(SecurityUtil.md5(contactNo));
					cardFetch.setUsername(contactNo);
					WalletResponse fetchResponse = matchMoveApi.inquireCard(cardFetch);
					if (fetchResponse.getWalletNumber() != null && fetchResponse != null) {
						String nor = fetchResponse.getWalletNumber();
						System.err.println(nor);
						String aaa = nor.substring(0, 4) + " " + nor.substring(4, 8) + " " + nor.substring(8, 12) + " "
								+ nor.substring(12, 16);
						fetchResponse.setWalletNumber(aaa);
						UserKycResponse walletRespon = new UserKycResponse();
						walletRespon = matchMoveApi.getTransactions(use);
						MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
						List<ResponseDTO> resul = new ArrayList<>();
						if (!whichCard.isHasPhysicalCard()) {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						} else {
							if (walletRespon.getCardTransactions() != null) {
								JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								for (int i = 0; i < aa.length(); i++) {
									if (i < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										if (aa.getJSONObject(i).getJSONObject("details") != null) {
											JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
											if (ojj.has("merchantname")) {
												resp.setDescription(aa.getJSONObject(i).getJSONObject("details")
														.getString("merchantname"));
											} else {
												resp.setDescription(aa.getJSONObject(i).getString("description"));
											}
										}
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
							}
						}
						session.setAttribute("transactions", resul);

						/*
						 * if(walletRespon.getDetails()!=null){ JSONObject obj = new
						 * JSONObject(walletRespon.getDetails().toString()); JSONArray aa=
						 * obj.getJSONArray("transactions"); List<ResponseDTO> resul=new ArrayList<>();
						 * for(int i=0;i<aa.length();i++){ if(1<5){ ResponseDTO resp= new ResponseDTO();
						 * resp.setAmount(aa.getJSONObject(i).getString("amount" ));
						 * resp.setDate(aa.getJSONObject(i).getString("date"));
						 * resp.setDescription(aa.getJSONObject(i).getString( "description"));
						 * resp.setStatus(aa.getJSONObject(i).getString("status" ));
						 * resp.setTransactionType(aa.getJSONObject(i).getString ("indicator"));
						 * resul.add(resp);} } session.setAttribute("transactions", resul);
						 * System.err.println(aaa); }
						 */}

					double balance = matchMoveApi.getBalance(use);
					model.addAttribute("Ubalance", balance);
					MMCards card = matchMoveApi.findCardByCardId(cardId);
					model.addAttribute("cardstatus", card.isBlocked());
					session.setAttribute("cardDetail", fetchResponse);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "Corporate/CardDetails";
			}
		}
		return "Corporate/Home";
	}

	@RequestMapping(value = "/UserTransaction/{username}/{cardId}", method = RequestMethod.GET)
	public String fetchUserTransaction(@ModelAttribute RegisterDTO dto, @PathVariable String username,
			@PathVariable String cardId, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				persistingSessionRegistry.refreshLastRequest(sessionId);
				MUser use = userApi.findByUserName(username);
				try {
					UserKycResponse walletRespon = new UserKycResponse();
					walletRespon = matchMoveApi.getTransactions(use);
					List<ResponseDTO> resul = new ArrayList<>();
					MMCards whichCard = matchMoveApi.findCardByCardId(cardId);
					if (!whichCard.isHasPhysicalCard()) {
						if (walletRespon.getCardTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					} else {
						if (walletRespon.getCardTransactions() != null) {
							JSONObject obj = new JSONObject(walletRespon.getCardTransactions().toString());
							JSONArray aa = obj.getJSONArray("transactions");
							for (int i = 0; i < aa.length(); i++) {
								ResponseDTO resp = new ResponseDTO();
								resp.setAmount(aa.getJSONObject(i).getString("amount"));
								resp.setDate(aa.getJSONObject(i).getString("date"));
								if (aa.getJSONObject(i).getJSONObject("details") != null) {
									JSONObject ojj = aa.getJSONObject(i).getJSONObject("details");
									if (ojj.has("merchantname")) {
										resp.setDescription(
												aa.getJSONObject(i).getJSONObject("details").getString("merchantname"));
									} else {
										resp.setDescription(aa.getJSONObject(i).getString("description"));
									}
								}
								resp.setStatus(aa.getJSONObject(i).getString("status"));
								resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
								resul.add(resp);
							}
						}
					}
					map.put("transactions", resul);

					return "Corporate/CardTransaction";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Corporate/CardTransaction";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Corporate/Home";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Corporate/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PrefundHistory")
	public String getPrefundHistory(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					daterange = CommonUtil.getDefaultDateRange();
					Date fromDate = df2.parse(daterange[0]);
					Date toDate = df2.parse(daterange[1]);
					String pardF = df2.format(fromDate);
					String pardT = df2.format(toDate);

					page.setFromDate(fromDate);
					page.setToDate(toDate);
					System.err.println(fromDate + "-" + toDate);
					model.addAttribute("dateRange", pardF + "-" + pardT);
				} catch (Exception e) {
					e.printStackTrace();
				}
				List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
						.getByCorporateByDate(page.getFromDate(), page.getToDate(), userSession.getUser());
				model.addAttribute("prefund", prefundHistory);
				return "Corporate/PrefundHistory";
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PrefundHistory")
	public String getPrefundHistoryPost(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				String[] daterange = null;
				try {
					if (page.getDaterange().length() != 0) {
						System.err.println(page.getDaterange().length());
						String aaaaa = page.getDaterange().substring(0, 10);
						String baaaa = page.getDaterange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");
						page.setFromDate(from);
						page.setToDate(to);

					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date fromDate = df2.parse(daterange[0]);
						Date toDate = df2.parse(daterange[1]);
						String pardF = df2.format(fromDate);
						String pardT = df2.format(toDate);

						page.setFromDate(fromDate);
						page.setToDate(toDate);
						System.err.println(fromDate + "-" + toDate);
						model.addAttribute("dateRange", pardF + "-" + pardT);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				// List<CorporatePrefundHistory>
				// prefundHistory=corporatePrefundHistoryRepository.getByCorporate(userSession.getUser());
				List<CorporatePrefundHistory> prefundHistory = corporatePrefundHistoryRepository
						.getByCorporateByDate(page.getFromDate(), page.getToDate(), userSession.getUser());
				model.addAttribute("prefund", prefundHistory);
				return "Corporate/PrefundHistory";
			}
		}
		return "redirect:/Corporate/Home";
	}

	/**
	 * BLOCK CARD
	 */

	@RequestMapping(value = "/BlockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> blockCardCorporate(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				System.err.println(cardRequest.getRequestType());
				System.err.println(cardRequest.getCardId());
				walletResponse = matchMoveApi.deActivateCards(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(true);
					cardRepository.save(card);

				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				System.err.println(cardRequest.getRequestType());
				System.err.println(cardRequest.getCardId());
				walletResponse = matchMoveApi.deActivateCards(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(true);
					cardRepository.save(card);

				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());

			}

			else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * UNBLOCK CARD
	 */

	@RequestMapping(value = "/UnblockCard", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> unblockCard(@RequestBody MatchMoveCreateCardRequest cardRequest,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				walletResponse = matchMoveApi.reActivateCard(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(false);
					card.setStatus("Active");
					cardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				walletResponse = matchMoveApi.reActivateCard(cardRequest);
				if ("S00".equalsIgnoreCase(walletResponse.getCode())) {
					MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
					card.setBlocked(false);
					card.setStatus("Active");
					cardRepository.save(card);
				}
				MMCards card = matchMoveApi.findCardByCardId(cardRequest.getCardId());
				model.addAttribute("cardstatus", card.isBlocked());
				return new ResponseEntity<>(walletResponse, HttpStatus.OK);

			}

		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return new ResponseEntity<>(walletResponse, HttpStatus.OK);
		}
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);

	}

	/**
	 * CHANGE PASSWORD
	 */

	@RequestMapping(value = "/ChangePasssword", method = RequestMethod.POST)
	public String changePassword(@ModelAttribute MatchMoveCreateCardRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				if (dto.getPassword().equalsIgnoreCase(dto.getConfirmPassword())) {
					if (passwordEncoder.matches(dto.getOldPassword(), userSession.getUser().getPassword())) {
						if (!dto.getOldPassword().equalsIgnoreCase(dto.getPassword())) {
							userApi.changePasswordCorporate(dto);
							model.addAttribute("message", "Password changed Successfully");
							return "Corporate/ChangePassword";
						} else {
							model.addAttribute("message", "Old Password and new Password cannot be same");
							return "Corporate/ChangePassword";

						}
					} else {
						model.addAttribute("message", "Please enter your current password correctly");
						return "Corporate/ChangePassword";
					}
				} else {
					model.addAttribute("message", "Please ensure new password and confirm password should be same");
					return "Corporate/ChangePassword";
				}

			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());
				if (dto.getPassword().equalsIgnoreCase(dto.getConfirmPassword())) {
					if (passwordEncoder.matches(dto.getOldPassword(), userSession.getUser().getPassword())) {
						if (!dto.getOldPassword().equalsIgnoreCase(dto.getPassword())) {
							userApi.changePasswordCorporate(dto);
							model.addAttribute("message", "Password changed Successfully");
							return "Corporate/ChangePassword";
						} else {
							model.addAttribute("message", "Old Password and new Password cannot be same");
							return "Corporate/ChangePassword";

						}
					} else {
						model.addAttribute("message", "Please enter your current password correctly");
						return "Corporate/ChangePassword";
					}
				} else {
					model.addAttribute("message", "Please ensure new password and confirm password should be same");
					return "Corporate/ChangePassword";
				}

			}

		} else {
			return "redirect:/Corporate/Home";
		}
		return "redirect:/Corporate/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/ChangePassword")
	public String getPasswordChange(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/ChangePassword";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/ChangePassword";

			}
		}
		return "redirect:/Corporate/Home";
	}

	/**
	 * DELETE PARTNER
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/DeletePartner")
	public ResponseEntity<ResponseDTO> deletePartner(@RequestBody DeleteCorporateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				PartnerDetails partner = partnerDetailsRepository.findOne(Long.parseLong(dto.getId()));
				if (partner != null) {
					try {
						partnerDetailsRepository.delete(partner);
						resp.setCode("S00");
						resp.setMessage("Partner Deleted SuccessFully");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setCode("F00");
						resp.setMessage("Exception Occurred");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					}
				}
			}
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	/**
	 * EDIT SERVICES
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/EditPartnerServices")
	public ResponseEntity<ResponseDTO> editPartnerServices(@ModelAttribute DeleteCorporateDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		System.err.println("agentId:" + dto.getId());
		// System.err.println(dto.getServices().toString());
		System.err.println(dto.getServices());
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				PartnerDetails partner = partnerDetailsRepository.findOne(Long.parseLong(dto.getId()));
				if (partner != null) {
					try {
						List<String> serviceList = dto.getServices();
						List<MService> serviceTobeReassigned = new ArrayList<>();
						if (serviceList != null && !serviceList.isEmpty()) {
							partner.getPartnerServices().clear();
							for (String string : serviceList) {
								System.err.println("This is the service" + string);
								MService assignedService = mServiceRepository.findServiceByCode(string);
								System.err.println("Service to be assigned::" + assignedService.getCode());
								serviceTobeReassigned.add(assignedService);
							}
							partner.setPartnerServices(serviceTobeReassigned);
							partnerDetailsRepository.save(partner);
							resp.setCode("S00");
							resp.setMessage("Partner Deleted SuccessFully");
							return new ResponseEntity<>(resp, HttpStatus.OK);
						}
					} catch (Exception e) {
						e.printStackTrace();
						resp.setCode("F00");
						resp.setMessage("Exception Occurred");
						return new ResponseEntity<>(resp, HttpStatus.OK);
					}
				}
			}
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpgradeAccount")
	public String updateKYC(@ModelAttribute UpgradeAccountDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		try {
			System.err.println("hello");
			System.err.println("this is username:::" + dto.getUsername());
			MUser user = userApi.findByUserName(dto.getUsername().trim());
			if (user != null) {
				MKycDetail detail = mKycRepository.findByUser(user);
				System.err.println("hey I am here");
				if (detail == null) {
					MUserDetails userDetails = user.getUserDetail();
					if (userDetails != null) {
						if (userDetails.getDateOfBirth() == null) {
							try {
								userDetails.setDateOfBirth(CommonUtil.formatter.parse(dto.getDob()));
								mUserDetailRepository.save(userDetails);

							} catch (java.text.ParseException e) {
								e.printStackTrace();
							}
						}
					}

					dto.setUserType(UserType.User);
					dto.setEmail(user.getUserDetail().getEmail());
					dto.setContactNo(user.getUserDetail().getContactNo());
					dto.setName(user.getUserDetail().getFirstName());
					if (dto.getImage() != null) {
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage(), dto.getContactNo(),
								dto.getIdType());
						dto.setImagePath(aadharCardImgPath);
					}
					if (dto.getImage2() != null) {
						String aadharCardImgPath = CommonUtil.uploadImage(dto.getImage2(), dto.getContactNo(),
								dto.getIdType());
						dto.setImagePath2(aadharCardImgPath);

					}

					ResponseDTO upg = userApi.upgradeAccountCorp(dto);
					if (upg.getCode().equalsIgnoreCase("S00")) {
						MKycDetail check = mKycRepository.findByUser(user);
						dto.setId(String.valueOf(check.getId()));
						ResponseDTO upg1 = userApi.updateKycRequestCorp(dto);
						if (upg1.getCode().equalsIgnoreCase("S00")) {
							model.addAttribute("message", "Account has been upgraded to KYC");
							return "Corporate/Upgradeaccount";

						} else {
							model.addAttribute("message",
									"Account upgradation Failed.Please try again or contact Admin");
							return "Corporate/Upgradeaccount";

						}
					} else {
						model.addAttribute("message",
								"Account upgradation Failed due to invalid data.Please try again or contact Admin");
						return "Corporate/Upgradeaccount";

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "Upgrade Account Failed,Please try again or contact Admin");
			return "Corporate/Upgradeaccount";
		}
		return "Corporate/Upgradeaccount";
	}

	@RequestMapping(method = RequestMethod.GET, value = "{userc}/UpgradeAccount")
	public String updateAcco(@PathVariable("userc") String userc, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				model.addAttribute("userc", userc);
				return "Corporate/Upgradeaccount";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());

				return "Corporate/Upgradeaccount";

			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FailedRegistration")
	public String getFailedReport(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());

				List<BulkRegister> bulkRegisters = bulkRegisterRepository.getFailedUserList(corpAgent);
				model.addAttribute("failed", bulkRegisters);
				return "Corporate/FailedRegistration";
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FailedBulkLoad")
	public String getBulkLoad(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());

				List<MTransaction> transactions = mTransactionRepository.getDriveUTransactionFailed(userSession.getUser().getAccountDetail());
				model.addAttribute("failed", transactions);
				return "Corporate/FailedLoad";
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BulkLoadReport")
	public String getBulkLoadReport(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());
				
				  List<MTransaction> transactions = mTransactionRepository
				  .getDriveUTransaction(userSession.getUser().getAccountDetail());
				  model.addAttribute("failed", transactions);
				 
				return "Corporate/BulkLoadReport";
			}
		}
		return "redirect:/Corporate/Home";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BulkLoadReport")
	public ResponseEntity<CommonResponse> getBulkLoadReportPOST(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		CommonResponse resp = new CommonResponse();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			List<MTransaction> transactions  = null;
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				transactions = mTransactionRepository.getSuccessTransaction(userSession.getUser().getAccountDetail());
				resp.setMessage("transactions fetched");
				System.out.println(transactions.get(0));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setDetails(transactions);
				return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
			}else{
				resp.setMessage("transactions not fetched");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		}
		return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BulkLoadReportByStatus")
	public ResponseEntity<CommonResponse> getBulkLoadReportByStatus(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		CommonResponse resp = new CommonResponse();
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			List<MTransaction> transactions  = null;
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				transactions = mTransactionRepository.getSuccessTransaction(userSession.getUser().getAccountDetail());
				resp.setMessage("transactions fetched");
				System.out.println(transactions.get(0));
				resp.setCode(ResponseStatus.SUCCESS.getValue());
				resp.setDetails(transactions);
				return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
			}else{
				resp.setMessage("transactions not fetched");
				resp.setCode(ResponseStatus.FAILURE.getValue());
			}
		}
		return new ResponseEntity<CommonResponse>(resp, HttpStatus.OK);
	}
	
	/**
	 * DOWLOAD AS CSV
	 */

	@RequestMapping(value = "/download/csv", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getDownloadBulkTransferFile(@ModelAttribute DownloadCsv downloadCsv,
			HttpServletRequest request, HttpServletResponse res, HttpSession session) throws IOException {

		ResponseDTO response = new ResponseDTO();
		Date from = null;
		Date to = null;
		String filename = ("Card_Records" + System.currentTimeMillis() + ".xls");
		DownloadHistory download = new DownloadHistory();
		download.setStatus(Status.Pending);
		download.setCorporate(true);
		download.setFileExtension(filename);
		DownloadHistory dwHis = downloadHistoryRepository.save(download);
		System.err.println("the req::" + downloadCsv);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		String[] daterange = null;
		// String dateRange="2018-05-01 - 2018-06-01";
		try {
			if (downloadCsv.getDateRange().length() != 0) {
				String aaaaa = downloadCsv.getDateRange().substring(0, 10);
				String baaaa = downloadCsv.getDateRange().substring(13);
				from = sdformat.parse(aaaaa + " 00:00:00");
				to = sdformat.parse(baaaa + " 00:00:00");
			} else {
				daterange = CommonUtil.getDefaultDateRange();
				from = df2.parse(daterange[0]);
				to = df2.parse(daterange[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<MMCardsDTO> dto = new ArrayList<MMCardsDTO>();

		switch (downloadCsv.getRequestType()) {
		case "physicalCards":

			List<MMCards> cardsdto1 = mMCardRepository.getAllCards(false, from, to);

			for (MMCards cards : cardsdto1) {
				BulkRegister crpUser = bulkRegisterRepository.getByUser(cards.getWallet().getUser());
				if (crpUser != null) {
					MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
					cardRequest.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
					cardRequest.setUsername(cards.getWallet().getUser().getUsername());
					try {
						cardRequest.setPassword(SecurityUtil.md5(cards.getWallet().getUser().getUsername()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					cardRequest.setCardId(cards.getCardId());
					WalletResponse walletResp = matchMoveApi.inquireCard(cardRequest);

					double balance = matchMoveApi.getBalance(cards.getWallet().getUser());
					MMCardsDTO card = new MMCardsDTO();
					card.setFirstName(cards.getWallet().getUser().getUserDetail().getFirstName());
					card.setLastName(cards.getWallet().getUser().getUserDetail().getLastName());
					card.setBalance(String.valueOf(balance));
					card.setContactNO(cards.getWallet().getUser().getUserDetail().getContactNo());
					card.setEmail(cards.getWallet().getUser().getUserDetail().getEmail());
					card.setCardNumber(walletResp.getWalletNumber());
					card.setDate(sdformat.format(cards.getCreated()));

					dto.add(card);
				}
			}
			break;
		}

		dwHis.setStatus(Status.Success);
		downloadHistoryRepository.save(dwHis);
		String fileName = ExcelWriter.makeExcelSheet(dto, filename);
		System.err.println(fileName);
		@SuppressWarnings("deprecation")
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
		@SuppressWarnings("unused")
		String filePath = contextPath + "/resources/excelSheets/";
		response.setCode("\\resources\\excelSheets\\" + fileName);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/downloadStatus", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getDownloadStatus() throws IOException {
		ResponseDTO response = new ResponseDTO();
		List<DownloadHistory> list = downloadHistoryRepository.getFileNameByOrder();
		Long statusCount = downloadHistoryRepository.getPendingCount();
		response.setPendingStatusCount(statusCount);
		response.setDownloadHistory(list);
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CorporateBulkKYCApproval")
	public String bulkKycApproval(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkKYCApproval";
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {
					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}
					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkKYCApproval";

				}
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CorporateBulkKYCApproval")
	public String bulkKYCUpload(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.CORPORATE)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv1(dto.getFile(), 123456 + "", "BLKKYCUPLOAD",
								URLMetadatas.EWIRE_MASTER_CSV_KEY_PATH_BULKKYC_CORPORATE);
						System.err.println("the s3 path::" + s3Path);
						System.err.println("the absult path::" + splitted[0]);
						System.err.println("the username::" + userSession.getUser().getUsername());
						// String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(),
						// "BLKKYCUPLOAD");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(s3Path);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKKYC);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKKYCUPLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration", true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("username", userSession.getUser().getUsername());

						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk KYC has been taken.Please contact your admin for approval");
						return "Corporate/BulkCardCreation";
					}
				} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)
						&& user.getAuthority().contains(Authorities.USER)) {

					dto.setSessionId(sessionId);

					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					String file = path.substring(19);

					// String fileName = StartUpUtil.CSV_FILE+file;
					System.err.println(file);
					String[] splitted = file.split("#");
					System.err.println(splitted[1]);
					String fileName = StartUpUtil.CSV_FILE + file;
					if (fileName != null) {
						String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKKYCUPLOAD");
						CorporateFileCatalogue fileCatalogue = new CorporateFileCatalogue();
						fileCatalogue.setAbsPath(splitted[1]);
						fileCatalogue.setCorporate(userSession.getUser());
						fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKKYC);
						fileCatalogue.setS3Path(s3Path);
						fileCatalogue.setCategoryType("BLKKYCUPLOAD");

						corporateFileCatalogueRepository.save(fileCatalogue);
						model.addAttribute("sucessMSG",
								"Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
					}

					PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
					if (partnerDetails != null) {
						List<MService> partnerServices = partnerDetails.getPartnerServices();
						if (partnerServices != null && !partnerServices.isEmpty()) {
							for (MService mService : partnerServices) {
								if (mService.getCode().equalsIgnoreCase("BRC")) {
									model.addAttribute("bulkRegistration", true);
								} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
									model.addAttribute("prefundC", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
									model.addAttribute("BulkCL", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
									model.addAttribute("BulkCardIssuance", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
									model.addAttribute("SingleCardLoad", true);
								} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
									model.addAttribute("CardBlockUnblock", true);
								} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
									model.addAttribute("singleCardAssignment", true);

								}
							}
						}
					}

					model.addAttribute("UserType", false);
					model.addAttribute("username", userSession.getUser().getUsername());

					return "Corporate/BulkCardCreation";

				}
			}
		}
		return "redirect:/Corporate/Home";

	}

	/**
	 * EDIT EMAIL
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/EditEmail")
	public ResponseEntity<ResponseDTO> editEmail(@RequestBody RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		ResponseDTO respDTO = new ResponseDTO();
		String emailToBeChanged = dto.getEmail();
		String mobile = dto.getContactNo();
		UserKycResponse walletResponse = new UserKycResponse();

		MUser user = userApi.findByUserName(mobile);

		walletResponse = matchMoveApi.updateEmail(user, emailToBeChanged);
		if (walletResponse.getCode().equalsIgnoreCase("S00")) {
			respDTO.setCode(walletResponse.getCode());
			respDTO.setMessage("Email Updated Successfully");
			return new ResponseEntity<ResponseDTO>(respDTO, HttpStatus.OK);
		} else {
			respDTO.setCode(walletResponse.getCode());
			respDTO.setMessage("Email Updation Failed");
		}

		return new ResponseEntity<ResponseDTO>(respDTO, HttpStatus.OK);
	}

	/**
	 * RESET PINS
	 */

	@RequestMapping(value = "/ResetPin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserKycResponse> resetPins(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DebitRequest debit, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		UserKycResponse walletResponse = new UserKycResponse();
		MUser user = userApi.findByUserName(debit.getMobile());
		walletResponse = matchMoveApi.resetPins(user);

		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BlockedCards")
	public String getBlockedCards(@ModelAttribute PagingDTO page, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);

				model.addAttribute("username", userSession.getUser().getUsername());
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());
				List<BulkRegister> corpUsers = bulkRegisterRepository.getRegisteredUsers(corpAgent);
				List<BulkRegister> blockedCardUser = new ArrayList<>();
				for (BulkRegister bulkRegister : corpUsers) {
					MMCards blkCard = mMCardRepository.getCardDetailsByUserAndStatus(bulkRegister.getUser(), "Inactive",
							true);
					if (blkCard != null) {
						blockedCardUser.add(bulkRegister);
					}
				}
				model.addAttribute("failed", blockedCardUser);
				return "Corporate/BlockedUsers";
			} else if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
				PartnerDetails partnerDetails = partnerDetailsRepository.getPartnerDetails(userSession.getUser());
				if (partnerDetails != null) {
					List<MService> partnerServices = partnerDetails.getPartnerServices();
					if (partnerServices != null && !partnerServices.isEmpty()) {
						for (MService mService : partnerServices) {
							if (mService.getCode().equalsIgnoreCase("BRC")) {
								model.addAttribute("bulkRegistration", true);
							} else if (mService.getCode().equalsIgnoreCase("PREFC")) {
								model.addAttribute("prefundC", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCL")) {
								model.addAttribute("BulkCL", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCS")) {
								model.addAttribute("BulkCardIssuance", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCSL")) {
								model.addAttribute("SingleCardLoad", true);
							} else if (mService.getCode().equalsIgnoreCase("BRCBL")) {
								model.addAttribute("CardBlockUnblock", true);
							} else if (mService.getCode().equalsIgnoreCase("CORSA")) {
								model.addAttribute("singleCardAssignment", true);

							}
						}
					}
				}
				CorporateAgentDetails corpAgent = corporateAgentDetailsRepository
						.getByCorporateId(userSession.getUser());
				List<BulkRegister> corpUsers = bulkRegisterRepository.getRegisteredUsers(corpAgent);
				List<BulkRegister> blockedCardUser = new ArrayList<>();
				for (BulkRegister bulkRegister : corpUsers) {
					MMCards blkCard = mMCardRepository.getCardDetailsByUserAndStatus(bulkRegister.getUser(), "Inactive",
							true);
					if (blkCard != null) {
						blockedCardUser.add(bulkRegister);
					}
				}
				model.addAttribute("failed", blockedCardUser);
				model.addAttribute("UserType", false);
				model.addAttribute("username", userSession.getUser().getUsername());
				return "Corporate/BlockedUsers";

			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/inactivePartner")
	public String inactiveCorporate(@ModelAttribute("partner") String partner, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model,
			RedirectAttributes redirect) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());

				if (partner != null) {
					String success = userApi.inactivePartener(userApi.getPartnerById(Long.parseLong(partner)));
					if (success != null) {
						redirect.addFlashAttribute("mesg", "Partner has been removed");
					} else {
						redirect.addFlashAttribute("mesg", "Error! while removing partner");
					}
				}
				return "redirect:/Corporate/ListCorporatePartner";
			}
		}
		return "redirect:/Corporate/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getPartnerTransaction")
	public String getPartnerTransaction(@ModelAttribute("partner") String partner, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model,
			RedirectAttributes redirect) {
		String sessionId = (String) session.getAttribute("corporateSessionId");
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.CORPORATE)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				model.addAttribute("UserType", true);
				model.addAttribute("bulkRegistration", true);
				model.addAttribute("prefundC", true);
				model.addAttribute("BulkCL", true);
				model.addAttribute("BulkCardIssuance", true);
				model.addAttribute("SingleCardLoad", true);
				model.addAttribute("CardBlockUnblock", true);
				model.addAttribute("singleCardAssignment", true);
				model.addAttribute("username", userSession.getUser().getUsername());
				List<TransactionListDTO> txnList = null;
				if (partner != null) {
					PartnerDetails details = userApi.getPartnerDetails(Long.parseLong(partner));
					if (details != null && details.getPartnerUser() != null) {
						List<MTransaction> transactions = mTransactionRepository
								.getTotalTxnByAccount(details.getPartnerUser().getAccountDetail());
						txnList = transactionApi.getTransactionByAccounts(transactions, details.getPartnerUser());
					}
				}
				modelMap.addAttribute("transactions", txnList);
				modelMap.put("Lstatus", "All");
				return "Corporate/LoadTransactions";
			}
		}
		return "redirect:/Corporate/Home";
	}
}
