package com.msscard.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/User")
public class RegistrationController {

	/*private RegisterValidation registerValidation;
	private IUserApi userApi;
	private MobileOTPValidation mobileOTPValidation;
	private MMCardRepository mMCardRepository;
	private IMCardApi iMCardApi;
	private final IMatchMoveApi matchMoveApi;
	public RegistrationController(RegisterValidation registerValidation, IUserApi userApi,
			MobileOTPValidation mobileOTPValidation, MMCardRepository mMCardRepository, IMCardApi iMCardApi,IMatchMoveApi matchMoveApi) {
		super();
		this.registerValidation = registerValidation;
		this.userApi = userApi;
		this.mobileOTPValidation = mobileOTPValidation;
		this.mMCardRepository = mMCardRepository;
		this.iMCardApi = iMCardApi;
		this.matchMoveApi=matchMoveApi;
	}*/

	
	/*@RequestMapping(value="/Registration",method=RequestMethod.POST)
	public String registerNormalUser(@ModelAttribute RegisterDTO dto,HttpServletRequest request,
			HttpServletResponse response,Model model,RedirectAttributes ra,HttpSession session){
		
		RegisterResponseDTO responseDTO=new RegisterResponseDTO();
		dto.setUsername(dto.getContactNo());
		RegisterError registerError=registerValidation.validateNormalUser(dto);
		if(registerError.isValid()){
			System.err.println(dto);
			dto.setUserType(UserType.User);
			try {
				userApi.saveUser(dto);
			} catch (Exception e) {
				e.printStackTrace();
				//model.addAttribute("Errormsg", registerError.getMessage());
				//model.addAttribute("username", dto.getContactNo());
				ra.addFlashAttribute("Errormsg", registerError.getMessage());
				return "redirect:/User/SignUp";
			}
			responseDTO.setStatus(ResponseStatus.SUCCESS);
			responseDTO.setMessage("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						responseDTO.setDetails("User Registration Successful and OTP sent to ::" + dto.getContactNo()
								+ " and  verification mail sent to ::" + dto.getEmail() + " .");
						model.addAttribute("username", dto.getContactNo());

						model.addAttribute("successMsg", "Please Enter the OTP sent to Registered No");
						session.setAttribute("username", dto.getContactNo());
						return "User/VerifyMobile";
		}else{
			System.err.println(registerError.getMessage());
			//model.addAttribute("Errormsg", registerError.getMessage());
			//model.addAttribute("username", dto.getContactNo());
			ra.addFlashAttribute("Errormsg", registerError.getMessage());

			return "redirect:/User/SignUp";
		}
	
}

	@RequestMapping(value="/Registration",method=RequestMethod.GET)
	public String registerNormalUser(HttpServletRequest request,
			HttpServletResponse response,Model model){
		System.err.println("hitting here...");
						return "User/SignUp";
				}
	

	@RequestMapping(value = "/Activate/Mobile", method = RequestMethod.POST)
 public	String verifyUserMobile(@ModelAttribute VerifyMobileDTO dto,Model model,
			HttpServletRequest request, HttpServletResponse response) {
		RegisterResponseDTO result = new RegisterResponseDTO();
		System.err.println(dto.getMobileNumber());
					if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
					MUser user=userApi.findByUserName(dto.getMobileNumber());
					ResponseDTO resp=matchMoveApi.createUserOnMM(user);
					if(resp.getCode().equalsIgnoreCase("S00")){
					WalletResponse walletResponse=matchMoveApi.assignVirtualCard(user);
					matchMoveApi.tempKycUserMM(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempSetIdDetails(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempSetImagesForKyc(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempConfirmKyc(user.getUserDetail().getEmail(),user.getUsername());
					matchMoveApi.tempKycStatusApproval(user.getUserDetail().getEmail(), user.getUsername());
					if(walletResponse.getCode().equalsIgnoreCase("S00")){
						result.setMessage("Activate Mobile||Card Generated");
					}
					}else{
						result.setMessage("Activate Mobile");
					}
				   	result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails("Your Mobile is Successfully Verified");
					model.addAttribute("successMsg", "Mobile Verified Sucessfully Please Login to Continue");
					return "User/Login";
					
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
					model.addAttribute("errorMsg", "Error verifying mobile");
					return "User/Home";
				}
			

	}

	private boolean verifyUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}
		
	}*/
}
