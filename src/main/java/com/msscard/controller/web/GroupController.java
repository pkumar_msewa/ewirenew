package com.msscard.controller.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cashiercards.errormessages.ErrorMessage;
import com.msscard.app.api.IAuthenticationApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.DateDTO;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.LoginResponse;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.CommonResponse;
import com.msscard.entity.BulkCardAssignmentGroup;
import com.msscard.entity.FCMDetails;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupBulkRegister;
import com.msscard.entity.GroupDetails;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.GroupSms;
import com.msscard.entity.LoginLog;
import com.msscard.entity.MBulkRegister;
import com.msscard.entity.MMCards;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.BulkRegisterDTO;
import com.msscard.model.DonationDTO;
import com.msscard.model.GroupDetailDTO;
import com.msscard.model.MTransactionDTO;
import com.msscard.model.PagingDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.SendNotificationDTO;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.UserDataRequestDTO;
import com.msscard.repositories.FCMDetailsRepository;
import com.msscard.repositories.GroupFileCatalogueRepository;
import com.msscard.repositories.GroupSmsRepository;
import com.msscard.repositories.LoginLogRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CSVReader;
import com.msscard.util.CommonUtil;
import com.msscard.util.ModelMapKey;
import com.msscard.util.StartUpUtil;
import com.msscards.metadatas.URLMetadatas;
import com.msscards.session.PersistingSessionRegistry;

@Controller
@RequestMapping("/Group")
public class GroupController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private final IAuthenticationApi authenticationApi;
	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApiImpl;
	private final MUserRespository mUserRespository;
	private final ISessionApi sessionApi;
	private final GroupSmsRepository groupSmsRepository;
	private final GroupFileCatalogueRepository groupFileCatalogueRepository;
	private final FCMDetailsRepository fcmDetailsRepository;	
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final LoginLogRepository loginLogRepository;
	
	public GroupController(IAuthenticationApi authenticationApi, UserSessionRepository userSessionRepository,
			IUserApi userApiImpl, MUserRespository mUserRespository, ISessionApi sessionApi,
			GroupSmsRepository groupSmsRepository, GroupFileCatalogueRepository groupFileCatalogueRepository,
			FCMDetailsRepository fcmDetailsRepository, PersistingSessionRegistry persistingSessionRegistry, LoginLogRepository loginLogRepository) {
		this.authenticationApi = authenticationApi;
		this.userSessionRepository = userSessionRepository;
		this.userApiImpl = userApiImpl;
		this.mUserRespository = mUserRespository;
		this.sessionApi = sessionApi;
		this.groupSmsRepository = groupSmsRepository;
		this.groupFileCatalogueRepository = groupFileCatalogueRepository;
		this.fcmDetailsRepository = fcmDetailsRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.loginLogRepository = loginLogRepository;
	}
	
	@RequestMapping(value="/Home", method = RequestMethod.GET)
	public String getLoginPage(HttpSession session, HttpServletRequest request, ModelMap modelMap) {
		String sessionId = (String) session.getAttribute("groupSessionId");
		if(sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if(authority != null) {
				if(authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED)) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					
					lUserData.setSessionId(sessionId);
					try {
										
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						Date oneMonthBack = cal.getTime();
						Calendar calPresent = Calendar.getInstance();
						Date present = calPresent.getTime();	
						lUserData.setFrom(oneMonthBack);
						lUserData.setTo(present);
						
					} catch(Exception e) {
						e.printStackTrace();
					}
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					
					List<GroupDetails> details = userApiImpl.getGroupDetails();
					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContact(lUserData);
					
					long count = userApiImpl.getCountGroupRequest(userInfo.getUserDetail().getContactNo());
					long acceptedcount = userApiImpl.getCountGroupAccept(userInfo.getUserDetail().getContactNo());
					long rejectedCount = userApiImpl.getCountGroupReject(userInfo.getUserDetail().getContactNo());
					
					modelMap.addAttribute("userData", lUserDataDto);
					
					modelMap.addAttribute("groupList", details);
					modelMap.put("userInfo", userInfo);
					
					modelMap.put(ModelMapKey.MESSAGE, session.getAttribute(ModelMapKey.MESSAGE));
					modelMap.put(ModelMapKey.ERROR, session.getAttribute(ModelMapKey.ERROR));
					modelMap.addAttribute("fromDate", lUserData.getFromDate());
					modelMap.addAttribute("toDate", lUserData.getToDate());
					modelMap.addAttribute("name", userInfo.getUserDetail().getFirstName());
					session.setAttribute("name", userInfo.getUserDetail().getFirstName());
					
					modelMap.addAttribute("requestCount", count);
					modelMap.addAttribute("acceptCount", acceptedcount);
					modelMap.addAttribute("rejectedCount", rejectedCount);
										
					request.getSession().setAttribute(ModelMapKey.MESSAGE, "");
					request.getSession().setAttribute(ModelMapKey.ERROR, "");
					session.setAttribute("mobile", userInfo.getUserDetail().getContactNo());

					return "Group/Home";		
				}
			}
		}
		return "Group/Login";
	}
	
	
	@RequestMapping(value="/Home", method = RequestMethod.POST)
	public String getLoginHomePage(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			HttpSession session, @ModelAttribute LoginDTO dto) {
		try {
			dto.setIpAddress(request.getRemoteAddr());
			
			MUser user = mUserRespository.findByUsername(dto.getUsername());
			if(user.getMobileToken().equalsIgnoreCase(dto.getMobileToken())) {
				LoginResponse loginResponse = userApiImpl.loginGroup(dto, request, response);
				String sessionId = loginResponse.getSessionId();
				request.getSession().setAttribute(CommonUtil.GROUP_SESSION_ID, sessionId);
				
				if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(loginResponse.getCode())) {
										
					MUser userInfo = mUserRespository.findByUsername(loginResponse.getContactNo());
					userInfo.setMobileToken(null);
					mUserRespository.save(userInfo);
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					String daterange[] = null;					
					try {
						if (dto.getDaterange() != null) {
							System.err.println(dto.getDaterange());
							String aaaaa = dto.getDaterange().substring(0, 10);
							String baaaa = dto.getDaterange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");
							lUserData.setFrom(from);
							lUserData.setTo(to);

						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date fromDate = df2.parse(daterange[0]);
							Date toDate = df2.parse(daterange[1]);
							lUserData.setFrom(fromDate);
							lUserData.setTo(toDate);
						}
						
					} catch (ParseException e) {
						e.printStackTrace();
					}
					logger.info("contact in post-- "+userInfo.getUserDetail().getContactNo());
					
					lUserData.setFromDate(daterange[0]);
					lUserData.setToDate(daterange[1]);
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					
					List<GroupDetails> details = userApiImpl.getGroupDetails();
					List<MUser> lUserDataDto = userApiImpl.groupAddRequestByContact(lUserData);
					
					long count = userApiImpl.getCountGroupRequest(userInfo.getUserDetail().getContactNo());
					long acceptedcount = userApiImpl.getCountGroupAccept(userInfo.getUserDetail().getContactNo());
					long rejectedCount = userApiImpl.getCountGroupReject(userInfo.getUserDetail().getContactNo());
					
					
					
					modelMap.addAttribute("userData", lUserDataDto);				
					modelMap.addAttribute("groupList", details);				
					modelMap.put("userInfo", userInfo);
					modelMap.addAttribute("fromDate", lUserData.getFromDate());
					modelMap.addAttribute("toDate", lUserData.getToDate());	
					modelMap.addAttribute("name", userInfo.getUserDetail().getFirstName());
					
					modelMap.addAttribute("requestCount", count);
					modelMap.addAttribute("acceptCount", acceptedcount);
					modelMap.addAttribute("rejectedCount", rejectedCount);
									
					return "Group/Home";
				} else {
					modelMap.put(ModelMapKey.ERROR, loginResponse.getMessage());
					return "Group/Login";
				}		
			} else {
				 modelMap.addAttribute("error", "Incorrect OTP");
				return "Group/VerifyMobile";				
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "Group/Login";
	}
	
	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	String logoutUserApi(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		ResponseDTO result = new ResponseDTO();

			try {
				UserSession userSession = userSessionRepository.findBySessionId(session.getId());
				if (userSession != null) {
					sessionApi.expireSession(session.getId());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("User logout successful");
					result.setDetails("Session Out");
					session.invalidate();
					return "Group/Login";
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					session.invalidate();
					return "Group/Login";
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails("Failed, invalid request.");
				return "Group/Login";
			}
		
	}

	
	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.GET)
	public String getGroupRequestList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {			
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: "+sessionId);
			
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.GROUP)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					try {						
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						Date oneMonthBack = cal.getTime();
						Calendar calPresent = Calendar.getInstance();
						Date present = calPresent.getTime();	
						lUserData.setFrom(oneMonthBack);
						lUserData.setTo(present);						
					} catch(Exception e) {
						e.printStackTrace();
					}
					
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					
										
					List<MUser> lUserDataDto = userApiImpl.addGroupRequestGet(lUserData);
					List<GroupDetails> details = userApiImpl.getGroupDetails();
					
					
					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());
														
					model.addAttribute("userInfo", userInfo);
					
					return "/Group/AddGroupRequest";
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/SingleBulkRegFail", method = RequestMethod.POST)
	public String getSingleFailedUser(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						List<GroupBulkRegister> user1 = userApiImpl.getsingleuserfailbulkreg(dto.getMobileNoId(),
								user.getUserDetail().getEmail());
						model.addAttribute("userData", user1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkRegFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/GrBulkRegFailList", method = RequestMethod.GET)
	public String getBulkRegisterFailList(HttpServletRequest request, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						List<GroupBulkRegister> list = userApiImpl.getgroupBulkRegisterFailList(user.getUserDetail().getEmail());
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkRegFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/GrBulkRegFailList", method = RequestMethod.POST)
	public String getBulkRegisterFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						String daterange[] = null;
						UserDataRequestDTO lUserData = new UserDataRequestDTO();

						if (dto.getReportrange() != null) {
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = CommonUtil.DATEFORMATTER1.parse(aaaaa + CommonUtil.startTime);
							Date to = CommonUtil.DATEFORMATTER1.parse(baaaa + CommonUtil.endTime);
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = CommonUtil.DATEFORMATTER.parse(daterange[0]);
							Date t = CommonUtil.DATEFORMATTER.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
						lUserData.setContactNo(userSession.getUser().getUserDetail().getContactNo());
						lUserData.setEmail(userSession.getUser().getUserDetail().getEmail());
						List<GroupBulkRegister> list = userApiImpl.getgroupBulkRegisterFailListPost(lUserData);
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkRegFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/GrBulkCarFailList", method = RequestMethod.GET)
	public String getBulkCardFailList(HttpServletRequest request, HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						List<BulkCardAssignmentGroup> list = userApiImpl.getgroupBulkCarFailList(user.getUserDetail().getEmail());
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkCarFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/GrBulkCarFailList", method = RequestMethod.POST)
	public String getBulkCardFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						String daterange[] = null;
						UserDataRequestDTO lUserData = new UserDataRequestDTO();

						if (dto.getReportrange() != null) {
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = CommonUtil.DATEFORMATTER1.parse(aaaaa + CommonUtil.startTime);
							Date to = CommonUtil.DATEFORMATTER1.parse(baaaa + CommonUtil.endTime);
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = CommonUtil.DATEFORMATTER.parse(daterange[0]);
							Date t = CommonUtil.DATEFORMATTER.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
						lUserData.setContactNo(userSession.getUser().getUserDetail().getContactNo());
						lUserData.setEmail(userSession.getUser().getUserDetail().getEmail());
						List<BulkCardAssignmentGroup> list = userApiImpl.getgroupBulkCarFailListPost(lUserData);
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/GBulkCarFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/GroupRequestList", method = RequestMethod.POST)
	public String getGroupRequestListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {		
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			logger.info("session:: "+sessionId);
			
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.GROUP)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");			
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
						
					} catch (ParseException e) {
						e.printStackTrace();
					}		
					
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());

					List<MUser> lUserDataDto = userApiImpl.addGroupRequestpost(lUserData);
					List<GroupDetails> details = userApiImpl.getGroupDetails();
					model.addAttribute("groupList", details);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());
					
					model.addAttribute("userInfo", userInfo);					
					return "/Group/AddGroupRequest";					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";		
	}
	
	@RequestMapping(value = "/ChangeRequestList", method = RequestMethod.GET)
	public String getGroupChangeList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					MUser user = userSession.getUser();
					if (user.getAuthority().contains(Authorities.GROUP)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						Date date = CommonUtil.getTodayDate();
						String presenceDate = CommonUtil.DATEFORMATTER.format(date);

						UserDataRequestDTO requestDTO = new UserDataRequestDTO();
						requestDTO.setSessionId(sessionId);
						requestDTO.setContactNo(user.getUserDetail().getContactNo());
						requestDTO.setEmail(user.getUserDetail().getEmail());
						requestDTO.setFrom(date);
						requestDTO.setTo(date);

						List<MUser> userList = userApiImpl.changeRequestListGet(requestDTO);
						List<GroupDetails> details = userApiImpl.getGroupDetails();

						model.addAttribute("groupList", details);
						model.addAttribute("userData", userList);
						model.addAttribute("startDate", presenceDate);
						model.addAttribute("endDate", presenceDate);
						model.addAttribute("name", user.getUserDetail().getFirstName());
						model.addAttribute("userInfo", user);

						return "/Group/ChangeGroupRequests";
					}
				} else {
					updateLoginLog(sessionId, request);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}

	private void updateLoginLog(String sessionId, HttpServletRequest request) {
		if (sessionId != null) {
			UserSession session1 = userSessionRepository.findBySessionId(sessionId);
			if (session1 != null) {
				MUser user = session1.getUser();
				Date lastReq = session1.getLastRequest();

				if (lastReq == null) {
					lastReq = new Date();
				} else {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(lastReq);
					calendar.add(Calendar.MINUTE, 15);
					lastReq = calendar.getTime();
				}

				Sort sort = new Sort(Direction.DESC, "id");
				Pageable pageable = new PageRequest(CommonUtil.PAGE, 1, sort);

				List<LoginLog> loginLogList = loginLogRepository.findByLoginIpAndUserAndStatus(request.getRemoteAddr(),
						user, Status.Success, pageable);
				if (loginLogList != null && loginLogList.size() > 0) {
					LoginLog l = loginLogList.get(0);
					l.setLogoutTime(lastReq);
					loginLogRepository.save(l);
				}
			}
		}
	}
	
	@RequestMapping(value = "/ChangeRequestList", method = RequestMethod.POST)
	public String getGroupChangeRequestListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);

			if (sessionId != null && !sessionId.isEmpty()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					MUser user = userSession.getUser();
					if (user.getAuthority().contains(Authorities.GROUP)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						UserDataRequestDTO requestDTO = new UserDataRequestDTO();
						requestDTO.setSessionId(sessionId);

						String daterange[] = null;
						try {
							if (dto.getReportrange() != null) {
								String aaaaa = dto.getReportrange().substring(0, 10);
								String baaaa = dto.getReportrange().substring(13);
								Date from = CommonUtil.DATEFORMATTER1.parse(aaaaa + CommonUtil.startTime);
								Date to = CommonUtil.DATEFORMATTER1.parse(baaaa + CommonUtil.endTime);
								requestDTO.setFrom(from);
								requestDTO.setTo(to);
							} else {
								daterange = CommonUtil.getDefaultDateRange();
								Date f = CommonUtil.DATEFORMATTER.parse(daterange[0]);
								Date t = CommonUtil.DATEFORMATTER.parse(daterange[1]);
								requestDTO.setFrom(f);
								requestDTO.setTo(t);
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}

						requestDTO.setContactNo(user.getUserDetail().getContactNo());
						requestDTO.setEmail(user.getUserDetail().getEmail());

						List<MUser> userList = userApiImpl.changeRequestListPost(requestDTO);
						List<GroupDetails> details = userApiImpl.getGroupDetails();

						model.addAttribute("groupList", details);
						model.addAttribute("userData", userList);
						model.addAttribute("startDate", CommonUtil.DATEFORMATTER.format(requestDTO.getFrom()));
						model.addAttribute("endDate", CommonUtil.DATEFORMATTER.format(requestDTO.getTo()));
						model.addAttribute("name", user.getUserDetail().getFirstName());
						model.addAttribute("userInfo", user);

						return "/Group/ChangeGroupRequests";
					}
				} else {
					updateLoginLog(sessionId, request);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value = "/GrBulkKycFailList", method = RequestMethod.GET)
	public String getBulkKycFailList(HttpServletRequest request, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						List<GroupBulkKyc> list = userApiImpl.getgroupBulkKycFailList(user.getUserDetail().getEmail());
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/BulkKycFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(value = "/GrBulkKycFailList", method = RequestMethod.POST)
	public String getBulkKycFailListPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, HttpSession session,
			ModelMap model) {

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					try {
						String daterange[] = null;
						UserDataRequestDTO lUserData = new UserDataRequestDTO();

						if (dto.getReportrange() != null) {
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = CommonUtil.DATEFORMATTER1.parse(aaaaa + CommonUtil.startTime);
							Date to = CommonUtil.DATEFORMATTER1.parse(baaaa + CommonUtil.endTime);
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = CommonUtil.DATEFORMATTER.parse(daterange[0]);
							Date t = CommonUtil.DATEFORMATTER.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
						lUserData.setContactNo(user.getUserDetail().getContactNo());
						lUserData.setEmail(user.getUserDetail().getEmail());
						List<GroupBulkKyc> list = userApiImpl.getgroupBulkLKycFailListPost(lUserData);
						model.addAttribute("userData", list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Group/BulkKycFailList";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value="/AcceptGroup/{username}", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> acceptGroup(@PathVariable(value = "username") String contactNo, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
						
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.GROUP)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					logger.info("contact: "+contactNo);
					result = userApiImpl.acceptGroupAddRequest(contactNo, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/RejectGroup", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> rejectGroupReason(@RequestBody PagingDTO page, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		CommonResponse result = new CommonResponse();
		
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
						
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.GROUP)
						&& authority.contains(Authorities.AUTHENTICATED))))) {
					logger.info("contact: "+page.getContact());
					result = userApiImpl.rejectGroupRequest(page, result);
				} else {
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setSuccess(false);
					result.setMessage("Unauthorized Role");
				}
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				result.setSuccess(false);
				result.setMessage("Session Expired");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CommonResponse>(result, HttpStatus.OK);
	}
	
	/**
	 * bulk card assignment
	 * 
	 * @param RegisterDTO
	 * @param request
	 * @param response
	 * @param session
	 * @param model
	 * @return String
	 * @throws ParseException
	 */

	@RequestMapping(value = "/BulkCardAssignment", method = RequestMethod.GET)
	public String getBulkCardAssign(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					request.getSession().setAttribute(CommonUtil.CSRF_TOKEN, UUID.randomUUID() + "");
					return "Group/BulkCardAssign";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkCardAssignment")
	public String bulkCardAssignment(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model, RedirectAttributes ra)
			throws ParseException {
		String token = (String) session.getAttribute(CommonUtil.CSRF_TOKEN);

		if (token != null && token.equals(dto.getCsrf_token())) {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && sessionId.length() != 0) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					MUser user = userSession.getUser();
					if (user.getAuthority().contains(Authorities.GROUP)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						dto.setSessionId(sessionId);

						try {
							String s3Path = CommonUtil.uploadCsv1(dto.getFile(), 123456 + "", "BLKCARDASSIGNGROUP",
									URLMetadatas.EWIRE_CSV_KEY_PATH_BULKCARDASSIGN_GROUP);

							GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
							fileCatalogue.setAbsPath(s3Path);
							fileCatalogue.setUser(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.BULK_CARD_ASSIGN);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKCARDASSIGNGROUP");
							groupFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("sucessMSG",
									"Upload successful. Your Request for Bulk Card Assignment has been taken. Please contact your Admin for approval.");
						} catch (Exception e) {
							e.printStackTrace();
						}
						return "Group/BulkCardAssign";
					}
				} else {
					updateLoginLog(sessionId, request);
				}
			}
		} else {
			ra.addFlashAttribute("error", "Invalid request");
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/BulkKYCApproval")
	public String bulkKycApproval(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model, ModelMap map) throws ParseException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && sessionId.length() != 0) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);

					request.getSession().setAttribute(CommonUtil.CSRF_TOKEN, UUID.randomUUID() + "");

					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", user.getUsername());

					return "Group/BulkKyc";
				}
			} else {
				updateLoginLog(sessionId, request);
			}
		}
		return "redirect:/Group/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkKYCApproval")
	public String bulkKYCUpload(@ModelAttribute RegisterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model, RedirectAttributes ra)
			throws ParseException {
		String token = (String) session.getAttribute(CommonUtil.CSRF_TOKEN);

		if (token != null && token.equals(dto.getCsrf_token())) {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && sessionId.length() != 0) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					MUser user = userSession.getUser();
					if (user.getAuthority().contains(Authorities.GROUP)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);

						dto.setSessionId(sessionId);

						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						String path = saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
						String file = path.substring(19);

						String[] splitted = file.split("#");
						String fileName = StartUpUtil.CSV_FILE + file;
						if (fileName != null) {
							String s3Path = CommonUtil.uploadCsv(dto.getFile(), user.getId() + "", "BLKKYCUPLOAD");
							GroupFileCatalogue fileCatalogue = new GroupFileCatalogue();
							fileCatalogue.setAbsPath(splitted[1]);
							fileCatalogue.setUser(userSession.getUser());
							fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKKYC);
							fileCatalogue.setS3Path(s3Path);
							fileCatalogue.setCategoryType("BLKKYCUPLOAD");

							groupFileCatalogueRepository.save(fileCatalogue);
							model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration", true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("singleCardAssignment", true);

							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("username", userSession.getUser().getUsername());

							model.addAttribute("sucessMSG",
									"Upload successful.Your Request for Bulk KYC has been taken.Please contact your admin for approval");
							return "Group/BulkKyc";
						}
					}
				} else {
					updateLoginLog(sessionId, request);
				}
			}
		} else {
			ra.addFlashAttribute("error", "Invalid request");
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value="/RejectedUserGroup", method = RequestMethod.GET)
	public String acceptedUserListGet(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.GROUP)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {						
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();	
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);						
				} catch(Exception e) {
					e.printStackTrace();
				}
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
								
				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
				
				List<MUser> lUserDataDto = userApiImpl.groupRejectedUserByContact(lUserData);
				
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				
				
				model.addAttribute("userInfo", userInfo);
				
				model.addAttribute("name", userInfo.getUserDetail().getFirstName());
							
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);				
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/RejectedUserGroup";
	}
	
	@RequestMapping(value="/RejectedUserGroup", method = RequestMethod.POST)
	public String rejectedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
				
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && (((authority.contains(Authorities.GROUP)
						|| authority.contains(Authorities.SUPER_ADMIN))
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);
					String daterange[] = null;
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						if (dto.getReportrange() != null) {
							System.err.println(dto.getReportrange());
							String aaaaa = dto.getReportrange().substring(0, 10);
							String baaaa = dto.getReportrange().substring(13);
							Date from = sdformat.parse(aaaaa + " 00:00:00");
							Date to = sdformat.parse(baaaa + " 00:00:00");			
							lUserData.setFrom(from);
							lUserData.setTo(to);
						} else {
							daterange = CommonUtil.getDefaultDateRange();
							Date f = df2.parse(daterange[0]);
							Date t = df2.parse(daterange[1]);
							lUserData.setFrom(f);
							lUserData.setTo(t);
						}
						
					} catch (ParseException e) {
						e.printStackTrace();
					}		
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					MUser userInfo = userSession.getUser();
					
					lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
					
					List<MUser> lUserDataDto = userApiImpl.groupRejectedUserByContactPost(lUserData);
					model.addAttribute("userData", lUserDataDto);
					model.addAttribute("fromDate", lUserData.getFromDate());
					model.addAttribute("toDate", lUserData.getToDate());
					
								
					model.addAttribute("userInfo", userInfo);
					model.addAttribute("name", userInfo.getUserDetail().getFirstName());
									
					return "/Group/RejectedUserGroup";					
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "Group/RejectedUserGroup";
	}
	
	@RequestMapping(value="/AcceptedUserGroup", method = RequestMethod.GET)
	public String acceptedUserList(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.GROUP)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				try {						
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					Date oneMonthBack = cal.getTime();
					Calendar calPresent = Calendar.getInstance();
					Date present = calPresent.getTime();	
					lUserData.setFrom(oneMonthBack);
					lUserData.setTo(present);						
				} catch(Exception e) {
					e.printStackTrace();
				}
				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
								
				List<MUser> lUserDataDto = userApiImpl.groupAcceptedUserByContact(lUserData);
								
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());
				model.addAttribute("name", userInfo.getUserDetail().getFirstName());

								
				model.addAttribute("userInfo", userInfo);
				
								
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);				
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/AcceptedUserGroup";
	}
	
	@RequestMapping(value="/AcceptedUserGroup", method = RequestMethod.POST)
	public String acceptedUserListPost(@ModelAttribute PagingDTO dto, @ModelAttribute("fromDate") String fromDate,
			@ModelAttribute("toDate") String toDate, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();

		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.GROUP)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				
				UserDataRequestDTO lUserData = new UserDataRequestDTO();
				lUserData.setSessionId(sessionId);
				String daterange[] = null;
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				try {
					if (dto.getReportrange() != null) {
						System.err.println(dto.getReportrange());
						String aaaaa = dto.getReportrange().substring(0, 10);
						String baaaa = dto.getReportrange().substring(13);
						Date from = sdformat.parse(aaaaa + " 00:00:00");
						Date to = sdformat.parse(baaaa + " 00:00:00");			
						lUserData.setFrom(from);
						lUserData.setTo(to);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						Date f = df2.parse(daterange[0]);
						Date t = df2.parse(daterange[1]);
						lUserData.setFrom(f);
						lUserData.setTo(t);
					}
					
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
								
				lUserData.setContactNo(userInfo.getUserDetail().getContactNo());
				List<MUser> lUserDataDto = userApiImpl.acceptedUserByContactPostGroup(lUserData);
				model.addAttribute("userData", lUserDataDto);
				model.addAttribute("fromDate", lUserData.getFromDate());
				model.addAttribute("toDate", lUserData.getToDate());

						
				model.addAttribute("userInfo", userInfo);			
				model.addAttribute("name", userInfo.getUserDetail().getFirstName());
							
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setMessage(ErrorMessage.AUTHORITY_MSG);				
			}
		} else {
			return "redirect:/Group/Home";
		}
		return "Group/AcceptedUserGroup";
	}
	
	@RequestMapping(value="/DonationList", method = RequestMethod.GET)
	public String getDonationBasedOnGroup(@ModelAttribute PagingDTO dto, HttpServletRequest request, 
			HttpServletResponse response, HttpSession session, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			if (sessionId != null && !sessionId.isEmpty()) {
				String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
				if (authority != null && ((authority.contains(Authorities.GROUP)
						&& authority.contains(Authorities.AUTHENTICATED)))) {
					UserDataRequestDTO lUserData = new UserDataRequestDTO();
					lUserData.setSessionId(sessionId);					
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");					
					Date f = df2.parse(CommonUtil.getDefaultDateRange()[0]);
					Date t = df2.parse(CommonUtil.getDefaultDateRange()[1]);
					lUserData.setFrom(f);
					lUserData.setTo(t);
					
					@SuppressWarnings("unused")
					List<DonationDTO> lUserDataDto = userApiImpl.donationListBasedOnGroup(lUserData);
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setMessage(ErrorMessage.AUTHORITY_MSG);				
				}
			} else {
				return "redirect:/Group/Home";
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "Group/DonationList";
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/SingleGroupRequest")
	public String getSingleUserGroupRequest(@ModelAttribute GroupDetailDTO dto, HttpServletRequest request, 
				HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if(sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if(authority != null) {
				if((authority.contains(Authorities.GROUP) && authority.contains(Authorities.AUTHENTICATED))) {					
					try {
						
						if(dto.getRemark().equalsIgnoreCase("request")) {
							List<MUser> user = userApiImpl.getRequestUser(dto.getMobileNoId());
							if(user != null) {
								model.addAttribute("userData", user);							
							}	
							return "Group/AddGroupRequest";
						} else if(dto.getRemark().equalsIgnoreCase("rejected")) {
							List<MUser> user = userApiImpl.getRejectedUser(dto.getMobileNoId());
							if(user != null) {
								model.addAttribute("userData", user);							
							}	
							return "Group/RejectedUserGroup";
						} else if(dto.getRemark().equalsIgnoreCase("accepted")) {
							List<MUser> user = userApiImpl.getAcceptedUser(dto.getMobileNoId());
							if(user != null) {
								model.addAttribute("userData", user);							
							}	
							return "Group/AcceptedUserGroup";
						} 						
					} catch(Exception e) {
						e.printStackTrace();
					}
				} else {
					return "Group/Login";
				}
			} else {
				return "Group/Login";
			}
		} else {
			return "Group/Login";
		}
		return "Group/Login";
	}	
	
	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getVerifyMobile(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute LoginDTO dto, HttpSession session, ModelMap map) {
		ResponseDTO result = new ResponseDTO();
		try {						
			logger.info("mobilenumber:: "+dto.getUsername());
			dto.setIpAddress(request.getRemoteAddr());
			LoginResponse loginResponse = userApiImpl.loginGroup(dto, request, response);
			if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(loginResponse.getCode())) {
				boolean verified = userApiImpl.resendMobileTokenDeviceBindingGroup(dto.getUsername());
				if (verified) {				
					result.setStatus(ResponseStatus.SUCCESS.getKey());				
					result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setSuccess(true);
					result.setMessage("OTP sent");
					result.setDetails("OTP sent");
					map.addAttribute("msg", result.getMessage());
					map.addAttribute("username", dto.getUsername());
					map.addAttribute("password", dto.getPassword());
				} else {
					result.setStatus(ResponseStatus.FAILURE.getKey());
					result.setCode(ResponseStatus.FAILURE.getValue());
					result.setSuccess(false);				
					result.setMessage("Enter valid Details");
					result.setDetails("Enter valid Details");
					map.addAttribute("msg", result.getMessage());
				}
			} else {
				map.put(ModelMapKey.ERROR, loginResponse.getMessage());
				return "Group/Login";
			}
		} catch(Exception e) {
			e.printStackTrace();
		}	
		return "Group/VerifyMobile";		
	}
	
	@RequestMapping(value = "/ResendDeviceBindingOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE } , consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> resendDeviceBindingOTP(@RequestBody LoginDTO dto,
			 HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("mobilenumber:: "+dto.getUsername());
			boolean verified = userApiImpl.resendMobileTokenDeviceBindingGroup(dto.getUsername());
			if (verified) {
				result.setStatus(ResponseStatus.SUCCESS.getKey());				
				result.setCode(ResponseStatus.SUCCESS.getValue());
				result.setSuccess(true);
				result.setMessage("New OTP sent");
				result.setDetails("New OTP sent");
			} else {
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setSuccess(false);				
				result.setMessage("Enter valid Details");
				result.setDetails("Enter valid Details");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/SendGroupSms", method = RequestMethod.GET)
	public String sendGroupSmsGet(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.GROUP)
					&& authority.contains(Authorities.AUTHENTICATED)))) {
				logger.info("in send sms get");
				return "Group/SendGroupSms";
			}
		} else {
			return "Group/SendGroupSms";
		}
		return "Group/SendGroupSms";
	}
	
	@RequestMapping(value="/SendGroupSms", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
	ResponseEntity<ResponseDTO> sendNotificationToGroupUser(@RequestBody SendNotificationDTO dto, HttpServletRequest request, 
			HttpServletResponse response, HttpSession session) throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();	
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		if (sessionId != null && !sessionId.isEmpty()) {
			String authority = authenticationApi.getAuthorityFromSessionRequest(sessionId, request);
			if (authority != null && ((authority.contains(Authorities.GROUP)
					&& authority.contains(Authorities.AUTHENTICATED)))) {				
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				MUser userInfo = userSession.getUser();
				try {
					logger.info("radio button value:: "+dto.getAllUser());
					logger.info("notify mobile no:: "+dto.getUserMobile());					
					if(dto.getSingleUser() == null) {
						dto.setAllUser("YoAll");
					} else {
						dto.setAllUser("YoSingle");
					}
					if(dto.getAllUser().equals("YoSingle")) {
						GroupSms single = new GroupSms();
						single.setCronStatus(false);
						single.setMobile(dto.getUserMobile());
						single.setText(dto.getMessage());
						single.setSingle(true);
						single.setStatus(Status.Inactive.getValue());						
						single.setGroupName(userInfo.getGroupDetails().getGroupName());
						groupSmsRepository.save(single);
						//userApiImpl.sendSingleGroupSMS(dto.getUserMobile(), dto.getMessage());
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setSuccess(true);
						result.setMessage("Message Sent Successfully");
					} else if(dto.getAllUser().equals("YoAll")) {						
						userApiImpl.sendGroupSMS(userInfo.getGroupDetails().getGroupName(), dto.getMessage());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setSuccess(true);
						result.setMessage("Message Sent Successfully");					
					}			
				} catch(Exception e) {
					e.printStackTrace();
				}	
			}
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	/**
	 * FCM INTEGRATION
	 * */
	
	@RequestMapping(value = "/SendNotification", method = RequestMethod.GET)
	public String sendNotification(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				
				try {
				
					return "Group/SendNotifiGroup";
				} catch (Exception e) {
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					return "Group/SendNotifiGroup";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
	}

	@RequestMapping(value = "/SendNotification", method = RequestMethod.POST)
	public String sendNotificationPost(@ModelAttribute SendNotificationDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				
				try {							
					//if(!dto.getForIos().equalsIgnoreCase("ios")){
					if(dto.getForIos() != null) {
					if(dto.getWithImage()!=null && dto.getWithImage().equalsIgnoreCase("Yo")){
						if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoAll")){
					String imagePath=CommonUtil.uploadFcmImage(dto.getImage(), userSession.getUser().getId()+"", "FCMIMAGES");
					FCMDetails fcmDetails=new FCMDetails();
					fcmDetails.setImagePath(imagePath);
					fcmDetails.setMessage(dto.getMessage());
					fcmDetails.setTitle(dto.getTitle());
					fcmDetails.setStatus("Active");
					fcmDetails.setHasImage(true);
					fcmDetails.setAllUsers(true);
					fcmDetailsRepository.save(fcmDetails);
					model.addAttribute("message", "Your FCM content is set on Scheduler");
					return "Group/SendNotifiGroup";
					}else if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoSingle")){
						String imagePath=CommonUtil.uploadFcmImage(dto.getImage(), userSession.getUser().getId()+"", "FCMIMAGES");
						FCMDetails fcmDetails=new FCMDetails();
						fcmDetails.setImagePath(imagePath);
						fcmDetails.setMessage(dto.getMessage());
						fcmDetails.setTitle(dto.getTitle());
						fcmDetails.setStatus("Active");
						fcmDetails.setHasImage(true);
						fcmDetails.setSingleUser(true);
						fcmDetails.setUsername(dto.getUserMobile());
						fcmDetailsRepository.save(fcmDetails);
						model.addAttribute("message", "Your FCM content is set on Scheduler");
						return "Group/SendNotifiGroup";
					}
						}else if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoAll")){
							
						FCMDetails fcmDetails=new FCMDetails();
						fcmDetails.setMessage(dto.getMessage());
						fcmDetails.setTitle(dto.getTitle());
						fcmDetails.setStatus("Active");
						fcmDetails.setHasImage(false);
						fcmDetails.setAllUsers(true);
						fcmDetailsRepository.save(fcmDetails);
						model.addAttribute("message", "Your FCM content is set on Scheduler");
						return "Group/SendNotifiGroup";
					}else{
						FCMDetails fcmDetails=new FCMDetails();
						fcmDetails.setMessage(dto.getMessage());
						fcmDetails.setTitle(dto.getTitle());
						fcmDetails.setStatus("Active");
						fcmDetails.setHasImage(false);
						fcmDetails.setSingleUser(true);
						fcmDetails.setUsername(dto.getUserMobile());
						fcmDetailsRepository.save(fcmDetails);
						model.addAttribute("message", "Your FCM content is set on Scheduler");
						return "Group/SendNotifiGroup";
				}
				}else{
					if(CommonUtil.calculatePayload(dto)){
						if(dto.getWithImage()!=null && dto.getWithImage().equalsIgnoreCase("Yo")){
							if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoAll")){
								FCMDetails fcmDetails=new FCMDetails();
								fcmDetails.setImagePath(dto.getShortenedimageUrl());
								fcmDetails.setMessage(dto.getMessage());
								fcmDetails.setTitle(dto.getTitle());
								fcmDetails.setStatus("Active");
								fcmDetails.setHasImage(true);
								fcmDetails.setAllUsers(true);
								fcmDetails.setForIos(false);
								fcmDetailsRepository.save(fcmDetails);
								model.addAttribute("message", "Your content is set on Scheduler");
								return "Group/SendNotifiGroup";
					}else if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoSingle")){
						FCMDetails fcmDetails=new FCMDetails();
						String imagePath=CommonUtil.uploadFcmImage(dto.getImage(), userSession.getUser().getId()+"", "FCMIMAGES");
						fcmDetails.setImagePath(imagePath);
						fcmDetails.setMessage(dto.getMessage());
						fcmDetails.setTitle(dto.getTitle());
						fcmDetails.setStatus("Active");
						fcmDetails.setHasImage(true);
						fcmDetails.setSingleUser(true);
						fcmDetails.setForIos(false);
						fcmDetails.setUsername(dto.getUserMobile());
						fcmDetailsRepository.save(fcmDetails);
						model.addAttribute("message", "Your content is set on Scheduler");
						return "Group/SendNotifiGroup";
					}
						}else if(dto.getAllUser()!=null && dto.getAllUser().equalsIgnoreCase("YoAll")){
							FCMDetails fcmDetails=new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setAllUsers(true);
							fcmDetails.setForIos(false);
							fcmDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your content is set on Scheduler");
							return "Group/SendNotifiGroup";
						}else{
							FCMDetails fcmDetails=new FCMDetails();
							fcmDetails.setMessage(dto.getMessage());
							fcmDetails.setTitle(dto.getTitle());
							fcmDetails.setStatus("Active");
							fcmDetails.setHasImage(false);
							fcmDetails.setSingleUser(true);
							fcmDetails.setForIos(false);
							fcmDetails.setUsername(dto.getUserMobile());
							fcmDetailsRepository.save(fcmDetails);
							model.addAttribute("message", "Your content is set on Scheduler");
							return "Group/SendNotifiGroup";

						}
					}else{
						model.addAttribute("message", "Payload size limit exceeded.The Size should be less than 256 bytes");
						return "Group/SendNotifiGroup";
					}
				}}catch (Exception e) {
				
					e.printStackTrace();
					walletResponse.setMessage("Service Unavailable");
					walletResponse.setCode(ResponseStatus.FAILURE.getValue());
					model.addAttribute("message", "Exception occurred");
					return "Group/SendNotifiGroup";
				}

			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
		return "Group/Login";

}
	
	//corporate module	
	@RequestMapping(method=RequestMethod.GET,value="/GroupBulkRegister")
    public String bulkRegster(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model,ModelMap map) throws ParseException  {
     String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
         if (sessionId != null && sessionId.length() != 0) {
        		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
        		if (userSession != null) {
        			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
        			if (user.getAuthority().contains(Authorities.GROUP)
        					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) { 
        				model.addAttribute("UserType", true);
        				model.addAttribute("bulkRegistration",true);
        				model.addAttribute("prefundC", true);
        				model.addAttribute("BulkCL", true);
        				model.addAttribute("BulkCardIssuance", true);
        				model.addAttribute("SingleCardLoad", true);
        				model.addAttribute("singleCardAssignment", true);

        				model.addAttribute("CardBlockUnblock", true);
        				model.addAttribute("username", userSession.getUser().getUsername());
        				model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());
        				return "Group/BulkRegister";
        			 } 
        		}
         	}
         	return "redirect:/Group/Home";
     	}
		
	@RequestMapping(value="/BulkRegister",method=RequestMethod.GET)
	public String getBulkRegister(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model){
		 List<MBulkRegister> lis=userApiImpl.findLast10Register();
         List<BulkRegisterDTO> cards= new ArrayList<>();
 		for (int i=0;i<lis.size();i++) {
 			BulkRegisterDTO car= new BulkRegisterDTO();
 			car.setEmail(lis.get(i).getEmail());
 			car.setName(lis.get(i).getName());
 			car.setDateOfBirth(lis.get(i).getDateOfBirth()+"");
 			car.setContactNo(lis.get(i).getContactNo());
 			car.setRegistrationDate(lis.get(i).getCreated()+"");
 			cards.add(car);
 		}
 		model.addAttribute("cardTransList", cards);
		return "Group/BulkRegister";
	}
	
	@RequestMapping(value="/download/bulkregister",method=RequestMethod.GET)
	public void getDownloadBulkRegistationFile(HttpServletRequest request,HttpServletResponse res,HttpSession session) throws IOException{
		@SuppressWarnings("deprecation")
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
			 PrintWriter out=res.getWriter();
			 String fileName="bulkregister.csv";
			 String filePath=contextPath+"/WEB-INF/bulkuploadformat/";
			 res.setContentType("APPLICATION/OCTET-STREAM");
			 res.setHeader("Content-Disposition","attachment;fileName=\""+fileName+"\"");
			 int i;
			 FileInputStream file=new FileInputStream(filePath +fileName);
			 while((i=file.read()) !=-1){
			   out.write(i);
			 }
			 file.close();
			 out.close();
	}
	
	@RequestMapping(value = "/download/bulkKyc", method = RequestMethod.GET)
	public void getDownloadBulkKycFile(HttpServletRequest request, HttpServletResponse res, HttpSession session)
			throws IOException {
		@SuppressWarnings("deprecation")
		String contextPath = request.getRealPath("/");

		PrintWriter out = res.getWriter();
		String fileName = "bulkkyc.csv";
		String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
		res.setContentType("APPLICATION/OCTET-STREAM");
		res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		int i;
		FileInputStream file = new FileInputStream(filePath + fileName);
		while ((i = file.read()) != -1) {
			out.write(i);
		}
		file.close();
		out.close();
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/BulkRegister")
    public String submitRequestRefund(@ModelAttribute RegisterDTO dto ,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws ParseException  {
  
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
         if (sessionId != null && sessionId.length() != 0) {
        		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
        		if (userSession != null) {
        			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
        			if (user.getAuthority().contains(Authorities.GROUP)
        					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {  
        				dto.setSessionId(sessionId);
        		  
					      String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					      String path=saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
					      String file=path.substring(19);
					      System.err.println(file);
					      String [] splitted=file.split("#");
					      System.err.println(splitted[1]);
					      String fileName = StartUpUtil.CSV_FILE+file;
					      if(fileName!=null) {
					      try {
					    	String s3Path=  CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKREGISTER");
					    	System.err.println("the s3 path::"+s3Path);
					    	System.err.println("the username::"+userSession.getUser().getUsername());
					    	GroupFileCatalogue fileCatalogue=new GroupFileCatalogue();
					    	fileCatalogue.setAbsPath(splitted[1]);
					    	fileCatalogue.setUser(userSession.getUser());
					    	fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKREGISTER);
					    	fileCatalogue.setS3Path(s3Path);
					    	fileCatalogue.setCategoryType("BLKREGISTER");
					    	groupFileCatalogueRepository.save(fileCatalogue);
					    	model.addAttribute("UserType", true);
							model.addAttribute("bulkRegistration",true);
							model.addAttribute("prefundC", true);
							model.addAttribute("BulkCL", true);
							model.addAttribute("BulkCardIssuance", true);
							model.addAttribute("SingleCardLoad", true);
							model.addAttribute("CardBlockUnblock", true);
							model.addAttribute("singleCardAssignment", true);
		
							model.addAttribute("username", userSession.getUser().getUsername());
							model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());
		
					    	model.addAttribute("sucessMSG", "Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
				    	  } catch(Exception e){
				    		  e.printStackTrace();
				    	  }
					      return "Group/BulkRegister";
					      }  
        			}
        		}
         	}
         	return "redirect:/Group/Home";
	}
	
	@RequestMapping(value="/BulkSMS",method=RequestMethod.GET)
	public String getBulkSms(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model){
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
        if (sessionId != null && sessionId.length() != 0) {
       	 UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
       		if (userSession != null) {
       			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
       			if (user.getAuthority().contains(Authorities.GROUP)
       					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) { 
       				model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration",true);
					model.addAttribute("bulkSms", true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);

					model.addAttribute("username", userSession.getUser().getUsername());
       				model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());
       			}
       		}
        }
		return "Group/BulkSms";
	}
	
	@RequestMapping(value="/BulkSMS", method = RequestMethod.POST)
	public String sendBulkSMSRequest(@ModelAttribute RegisterDTO dto ,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws ParseException {
		try {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
	         if (sessionId != null && sessionId.length() != 0) {
	        	 UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	        		if (userSession != null) {
	        			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
	        			if (user.getAuthority().contains(Authorities.GROUP)
	        					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) { 
	        				  dto.setSessionId(sessionId);
	              		  
						      String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						      String path=saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
						      String file=path.substring(19);
						      System.err.println(file);
						      String [] splitted=file.split("#");
						      System.err.println(splitted[1]);
						      String fileName = StartUpUtil.CSV_FILE+file;
						      if(fileName!=null) {
						    	  String s3Path=  CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKSMS");
							    	System.err.println("the s3 path::"+s3Path);
							    	System.err.println("the username::"+userSession.getUser().getUsername());
							    	GroupFileCatalogue fileCatalogue=new GroupFileCatalogue();
							    	fileCatalogue.setAbsPath(splitted[1]);
							    	fileCatalogue.setUser(userSession.getUser());
							    	fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKSMS);
							    	fileCatalogue.setS3Path(s3Path);
							    	fileCatalogue.setCategoryType("BLKSMS");
							    	groupFileCatalogueRepository.save(fileCatalogue);
							    	model.addAttribute("UserType", true);
									model.addAttribute("bulkRegistration",true);
									model.addAttribute("bulkSms", true);
									model.addAttribute("prefundC", true);
									model.addAttribute("BulkCL", true);
									model.addAttribute("BulkCardIssuance", true);
									model.addAttribute("SingleCardLoad", true);
									model.addAttribute("CardBlockUnblock", true);
									model.addAttribute("singleCardAssignment", true);
				
									model.addAttribute("username", userSession.getUser().getUsername());
									model.addAttribute("name", userSession.getUser().getUserDetail().getFirstName());
				
							    	model.addAttribute("sucessMSG", "Upload successful.Your Request for Bulk SMS has been taken.Please contact your admin for approval");
							    	return "Group/BulkSms";
						      }
	        			}
	        		}
	         }
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Group/Home";
	}
	
	@RequestMapping(value="/download/bulkSms",method=RequestMethod.GET)
	public void getDownloadBulkSmsFile(HttpServletRequest request,HttpServletResponse res,HttpSession session) throws IOException{
		@SuppressWarnings("deprecation")
		String contextPath = request.getRealPath("/");
		System.out.println(contextPath);
			 PrintWriter out=res.getWriter();
			 String fileName="bulksms.csv";
			 String filePath=contextPath+"/WEB-INF/bulkuploadformat/";
			 res.setContentType("APPLICATION/OCTET-STREAM");
			 res.setHeader("Content-Disposition","attachment;fileName=\""+fileName+"\"");
			 int i;
			 FileInputStream file=new FileInputStream(filePath +fileName);
			 while((i=file.read()) !=-1){
			   out.write(i);
			 }
			 file.close();
			 out.close();
	}
	
	@RequestMapping(value = "/BulkUploadSmsList", method = RequestMethod.GET)
	public String fetchBulkUploadListSms(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		ResponseDTO walletResponse = new ResponseDTO();
		String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.GROUP)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				
				List<GroupFileCatalogue> fileCatalogue = groupFileCatalogueRepository.getUnapprovedListSmsOFGroup(userSession.getUser(), false);
				
				model.addAttribute("CatalogueList", fileCatalogue);
								
				return "Group/GroupSmsBulkList";
			} else {
				walletResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				walletResponse.setMessage("Unauthorized User...");
				return "Group/Login";
			}
		} else {
			walletResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			walletResponse.setMessage("Invalid Session....");
			return "Group/Login";
		}
	}

	
	 private String saveRefundReport(String rootDirectory, MultipartFile file,String code) {
	       String filePath = null;
	        String fileName =String.valueOf(System.currentTimeMillis());
	        File dirs = new File(rootDirectory + "/resources/register/" + fileName+"_"+file.getOriginalFilename());
	        dirs.mkdirs();
	        try {
	         file.transferTo(dirs);
	         filePath = "/resources/register/" + fileName+"_"+file.getOriginalFilename();
	         return filePath+"#"+fileName+"_"+file.getOriginalFilename();
	        } catch (IOException e) {
	         e.printStackTrace();

	       }
	       return filePath;
	  }
	 
		@RequestMapping(value="/BulkTransfer",method=RequestMethod.GET)
		public String getBulkTransfer(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model){
			List<MTransactionDTO> cards= new ArrayList<>();
			for (int i=0;i<=50;i++) {
				MTransactionDTO car= new MTransactionDTO();
				car.setAmount("1000");
				car.setAuthReferenceNo("987987123");
				car.setCardId("msscard12345");
				car.setEmail("user@cashier.in");
				car.setName("User One");
				car.setMerchantId("Reliance Digital");
				car.setTransactionDate("24/02/2018");
				car.setTransactionRefNo("123123123431");
				cards.add(car);
			}
			model.addAttribute("cardTransList", cards);
			return "Group/BulkTransfer";
		}
		
		@RequestMapping(value="/download/bulktransfer",method=RequestMethod.GET)
		public void getDownloadBulkTransferFile(HttpServletRequest request,HttpServletResponse res,HttpSession session) throws IOException{
			@SuppressWarnings("deprecation")
			String contextPath = request.getRealPath("/");
			System.out.println(contextPath);
				      PrintWriter out=res.getWriter();
				      String fileName="bulktransfer.csv";
				      String filePath=contextPath+"/WEB-INF/bulkuploadformat/";
				      res.setContentType("APPLICATION/OCTET-STREAM");
				      res.setHeader("Content-Disposition","attachment;fileName=\""+fileName+"\"");
				      int i;
				      FileInputStream file=new FileInputStream(filePath +fileName);
				    while((i=file.read()) !=-1){
				      out.write(i);
				   }
				    file.close();
				   out.close();
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/Transactions")
		public String getCardTransactions(ModelMap modelMap,DateDTO dto, HttpServletRequest request, HttpServletResponse response,
				HttpSession session,Model model) {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					List<MTransactionDTO> cards= new ArrayList<>();
					for (int i=0;i<=50;i++) {
						MTransactionDTO car= new MTransactionDTO();
						car.setAmount("1000");
						car.setAuthReferenceNo("987987123");
						car.setCardId("msscard12345");
						car.setEmail("user@cashier.in");
						car.setName("User One");
						car.setMerchantId("Reliance Digital");
						car.setTransactionDate("24/02/2018");
						car.setTransactionRefNo("123123123431");
						cards.add(car);
					}
					session.setAttribute("cardTransList", cards);
					return "Group/CardTransaction";
				}
			}
			return "Group/Login";
		}
		
		@RequestMapping(method = RequestMethod.POST, value = "/Card/Transactions")
		public String getCardTransactionsPost(ModelMap modelMap, DateDTO dto, HttpServletRequest request, HttpServletResponse response,
				HttpSession session,Model model) {
			String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.GROUP)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					if (dto.getFromDate()==null || dto.getToDate()==null || dto.getFromDate().equalsIgnoreCase("") || dto.getToDate().equalsIgnoreCase("")) {
						Date date=new Date();
					    SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
					    String dateText = df2.format(date);
						dto.setToDate(dateText);
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, -1);
						dto.setFromDate(df2.format(cal.getTime()));
					}
					dto.setServiceName("");
					List<MTransaction> card=userApiImpl.findAllCardTransactioons(dto);
					List<MTransactionDTO> cards= new ArrayList<>();
					for (MTransaction mmCards : card) {
						MTransactionDTO car= new MTransactionDTO();
						car.setAmount(mmCards.getAmount()+"");
						car.setAuthReferenceNo(mmCards.getAuthReferenceNo());
						car.setDebit(mmCards.isDebit()+"");
						car.setDescription(mmCards.getDescription());
						MUser usera=userApiImpl.findByUserAccount(mmCards.getAccount());
						MMCards cardd=userApiImpl.findCardByUser(usera);
						car.setCardId(cardd.getCardId());
						car.setEmail(usera.getUserDetail().getEmail());
						car.setName(usera.getUserDetail().getFirstName() + usera.getUserDetail().getLastName());
						car.setRequest(mmCards.getRequest());
						car.setRetrivalReferenceNo(mmCards.getRetrivalReferenceNo());
						car.setStatus(mmCards.getStatus()+"");
						car.setTransactionDate(mmCards.getCreated()+"");
						car.setTransactionRefNo(mmCards.getTransactionRefNo());
						cards.add(car);
					}
					model.addAttribute("UserType", true);
					model.addAttribute("bulkRegistration",true);
					model.addAttribute("prefundC", true);
					model.addAttribute("BulkCL", true);
					model.addAttribute("BulkCardIssuance", true);
					model.addAttribute("SingleCardLoad", true);
					model.addAttribute("CardBlockUnblock", true);
					model.addAttribute("singleCardAssignment", true);


					session.setAttribute("cardTransList", cards);
					return "Group/CardTransaction";
				}
			}
			return "Group/Login";
		}
		
		@RequestMapping(method=RequestMethod.POST,value="/BulkCardLoad")
	    public String bulkCardLoad(@ModelAttribute RegisterDTO dto ,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws ParseException  {
	     String sessionId = (String) session.getAttribute(CommonUtil.GROUP_SESSION_ID);
	         if (sessionId != null && sessionId.length() != 0) {
	        		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	        		if (userSession != null) {
	        			UserDTO user = userApiImpl.getUserById(userSession.getUser().getId());
	        			if (user.getAuthority().contains(Authorities.GROUP)
	        					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {  
	        				
	        		  dto.setSessionId(sessionId);
	        		  
				      String rootDirectory = request.getSession().getServletContext().getRealPath("/");
				      String path=saveRefundReport(rootDirectory, dto.getFile(), dto.getFileName());
				      String file=path.substring(19);
				      
				      System.err.println(file);
				      String [] splitted=file.split("#");
				      System.err.println(splitted[1]);
				      String fileName = StartUpUtil.CSV_FILE+file;
				      if(fileName!=null) {
				    	String s3Path=  CommonUtil.uploadCsv(dto.getFile(), user.getUserId(), "BLKCARDLOAD");
				    	GroupFileCatalogue fileCatalogue=new GroupFileCatalogue();
				    	fileCatalogue.setAbsPath(splitted[1]);
				    	fileCatalogue.setUser(userSession.getUser());
				    	fileCatalogue.setFileDescription(CSVReader.DESCRIPTION_BULKCARDLOAD);
				    	fileCatalogue.setS3Path(s3Path);
				    	fileCatalogue.setCategoryType("BLKCARDLOAD");
				    	groupFileCatalogueRepository.save(fileCatalogue);
				    	
				    	model.addAttribute("UserType", true);
						model.addAttribute("bulkRegistration",true);
						model.addAttribute("prefundC", true);
						model.addAttribute("BulkCL", true);
						model.addAttribute("BulkCardIssuance", true);
						model.addAttribute("SingleCardLoad", true);
						model.addAttribute("singleCardAssignment", true);

						model.addAttribute("CardBlockUnblock", true);
						model.addAttribute("username", userSession.getUser().getUsername());

				    	model.addAttribute("sucessMSG", "Upload successful.Your Request for Bulk Registration has been taken.Please contact your admin for approval");
				    	return "Group/BulkCardCreation";
	             }
	          }
	        }
	      }
	      return "redirect:/Group/Home";
		}
		
		@RequestMapping(value = "/download/bulkcardassign", method = RequestMethod.GET)
		public void getDownloadBulkCardAssign(HttpServletRequest request, HttpServletResponse res, HttpSession session)
				throws IOException {
			@SuppressWarnings("deprecation")
			String contextPath = request.getRealPath("/");

			PrintWriter out = res.getWriter();
			String fileName = "bulkcardassign.csv";
			String filePath = contextPath + "/WEB-INF/bulkuploadformat/";
			res.setContentType("APPLICATION/OCTET-STREAM");
			res.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
			int i;
			FileInputStream file = new FileInputStream(filePath + fileName);
			while ((i = file.read()) != -1) {
				out.write(i);
			}
			file.close();
			out.close();
		}
	
}
