package com.msscard.controller.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ISessionApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.UserKycResponse;
import com.msscard.app.model.response.WalletResponse;
import com.msscard.entity.AdminActivity;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.UserDTO;
import com.msscard.model.error.AuthenticationError;
import com.msscard.model.error.LoginError;
import com.msscard.repositories.AdminActivityRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.PhysicalCardDetailRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.Authorities;
import com.msscard.util.CommonUtil;
import com.msscard.util.ModelMapKey;
import com.msscard.util.SecurityUtil;
import com.msscard.validation.LoginValidation;
import com.msscards.session.SessionLoggingStrategy;

@Controller
@RequestMapping("/{role}/Login")
public class LoginController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final LoginValidation loginValidation;
	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final MMCardRepository mMCardRepository;
	private final ITransactionApi transactionApi;
	private final IMatchMoveApi matchMoveApi;
	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MTransactionRepository mTransactionRepository;
	private final MServiceRepository mServiceRepository;
	private final MUserRespository mUserRespository;
	private final AdminActivityRepository adminActivityRepository;
	
	public LoginController(LoginValidation loginValidation, IUserApi userApi,
			AuthenticationManager authenticationManager, UserSessionRepository userSessionRepository,
			SessionLoggingStrategy sessionLoggingStrategy, ISessionApi sessionApi,MMCardRepository mMCardRepository,
			ITransactionApi transactionApi,IMatchMoveApi matchMoveApi,PhysicalCardDetailRepository physicalCardDetailRepository,
			MTransactionRepository mTransactionRepository,MServiceRepository mServiceRepository, MUserRespository mUserRespository,
			AdminActivityRepository adminActivityRepository) {
		super();
		this.loginValidation = loginValidation;
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.mMCardRepository=mMCardRepository;
		this.transactionApi=transactionApi;
		this.matchMoveApi=matchMoveApi;
		this.physicalCardDetailRepository=physicalCardDetailRepository;
		this.mTransactionRepository=mTransactionRepository;
		this.mServiceRepository=mServiceRepository;
		this.mUserRespository = mUserRespository;
		this.adminActivityRepository = adminActivityRepository;
	}
	
	@RequestMapping(value = "/Process")
	String userLogin(@PathVariable(value = "role") String role ,@ModelAttribute LoginDTO login, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request,HttpSession session, HttpServletResponse response,ModelMap map,Model model) {
try{
		ResponseDTO result = new ResponseDTO();
		login.setIpAddress("192.2.122.2");
		MUser userInfo = mUserRespository.findByUsername(login.getUsername());
		logger.info("username: "+login.getUsername());
		if(userInfo.getMobileToken().equalsIgnoreCase(login.getMobileToken())) {
		LoginError error = loginValidation.checkLoginValidation(login);
		if (error.isSuccess()) {
				try {
					MUser user = userApi.findByUserName(login.getUsername());
					System.err.println("");
					if (user != null) {
						
						if (user.getAuthority().contains(Authorities.BLOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is blocked");
							map.put("errormsgg", "Account is blocked");
							result.setDetails(null);
							return "Admin/Login";
						} else if (user.getAuthority().contains(Authorities.LOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is locked,contact Customer-Care");
							map.put("errormsgg", "Account is locked,contact Customer-Care");
							result.setDetails(null);
							return "Admin/Login";
						}else if (user.getAuthority().contains(Authorities.SPECIAL_USER)) {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								map.put("errormsgg", "Failed, Unauthorized user.");
								result.setDetails(null);
								return "Admin/Login";
						} else if (role.equalsIgnoreCase("User")) {
							if (user.getAuthority().contains(Authorities.USER)) {
								if(user.isFullyFilled()){
								if (!login.isValidate()) {
                                    AuthenticationError authError = isValidUsernamePassword(login,request);
                                    if(authError.isSuccess()) {
									boolean validDevice=true;
                                    if (validDevice) {
                                        if (sessionApi.checkActiveSession(user)) {
                                            AuthenticationError auth = authentication(login,request);
                                            if (auth.isSuccess()) {
                                                try {
                                                    String gcmId = login.getRegistrationId();
                                                    if (gcmId != null) {
                                                        userApi.updateGcmId(gcmId, user.getUsername());
                                                    }
                                                } catch (NullPointerException e) {
                                                    e.printStackTrace();
                                                }
                                                user.setMobileToken(CommonUtil.generateNDigitNumericString(6));
                                                Map<String, Object> detail = new HashMap<String, Object>();
                                                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                                                sessionLoggingStrategy.onAuthentication(authentication, request, response);
                                                UserSession userSession = userSessionRepository.findByActiveSessionId(
                                                        RequestContextHolder.currentRequestAttributes().getSessionId());
                                                System.err.println("session iD ::" +RequestContextHolder.currentRequestAttributes().getSessionId());
                                                UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
                                                result.setStatus(ResponseStatus.SUCCESS);
                                                result.setMessage("Login successful.");
                                                detail.put("sessionId", userSession.getSessionId());
                                                detail.put("userDetail", activeUser);
                                                MPQAccountDetails account = user.getAccountDetail();
                                                account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
                                                detail.put("accountDetail", account);
                                                result.setDetails(detail);
                                                String cardNo=null;
                                                   System.err.println("hi i here....");
                                                   MMCards cards= mMCardRepository.getCardByUserAndStatus(user, "Active");
                                                   System.err.println(cards);
		                                              if(cards!=null){
		                                            	  MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		                                            	  cardRequest.setEmail(user.getUserDetail().getEmail());
		                                            	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
		                                            	  cardRequest.setCardId(cards.getCardId());
		                                            	  cardRequest.setUsername(user.getUsername());
		                                            	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);
		                                            	  System.err.println(walletResponse);
		                                            	  result.setHasVirtualCard(true);
		                                            	  result.setCardDetails(walletResponse);
		                                            	  cardNo=walletResponse.getWalletNumber();
		                                              }else{
		                                            	  System.err.println("card is null");
		                                            	  result.setHasVirtualCard(false);
		                                              }
		                                              MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
		                                              	if(physicalCards!=null){
		                                              		result.setHasPhysicalCard(true);
		                                              	}else{
		                                              		result.setHasPhysicalCard(false);
		                                              		String phyCard="xxxxxxxxxxxxxxxx";
		                                              		map.addAttribute("phyCardNo", phyCard.toUpperCase().replaceAll("....", "$0  "));
		                                              		map.addAttribute("phycvv", "XXX");
		                                              		map.addAttribute("phyExp", "XXXX-XX");
		                                              		map.addAttribute("cardHolder", "XXXXXXXXX");
		                                              	}
		                                              	if(cardNo!=null){
		                                              	map.addAttribute("cardNo", cardNo.replaceAll("....", "$0  "));}
		                                              	map.addAttribute("resultSet", result);
		                                              	map.addAttribute("ActiveUser", activeUser);
		                                             	
		                                              	Map<Object,Object> map1 = null;
		                                            	List<List<Map<Object,Object>>> list = new ArrayList<List<Map<Object,Object>>>();
		                                            	List<Map<Object,Object>> dataPoints1 = new ArrayList<Map<Object,Object>>();
		                                            	
		                                            		map1 = new HashMap<Object,Object>(); map1.put("label", "Accomodation"); map1.put("y", 30);dataPoints1.add(map1);
		                                            		map1 = new HashMap<Object,Object>(); map1.put("label", "Food & Groceries"); map1.put("y", 25);dataPoints1.add(map1);
		                                            		list.add(dataPoints1);
		                                            	
		                                            		map.addAttribute("dataPointsList", list);
		                                              	
															try {
																UserKycResponse walletResponse = new UserKycResponse();
																walletResponse = matchMoveApi
																		.getTransactions(userSession.getUser());
																if (walletResponse.getDetails() != null) {
																	JSONObject obj = new JSONObject(
																			walletResponse.getDetails().toString());
																	JSONArray aa = obj.getJSONArray("transactions");
																	List<ResponseDTO> resul = new ArrayList<>();
																	for (int i = 0; i < aa.length(); i++) {
																		if (1 < 5) {
																			ResponseDTO resp = new ResponseDTO();
																			resp.setAmount(aa.getJSONObject(i)
																					.getString("amount"));
																			resp.setDate(aa.getJSONObject(i)
																					.getString("date"));
																			resp.setDescription(aa.getJSONObject(i)
																					.getString("description"));
																			resp.setStatus(aa.getJSONObject(i)
																					.getString("status"));
																			resp.setTransactionType(aa.getJSONObject(i)
																					.getString("indicator"));
																			resul.add(resp);
																		}
																	}
																	map.put("transactions", resul);
																}
															} catch (JSONException e) {
		                                							e.printStackTrace();
		                                						}
		                                            		map.addAttribute("user", user);
		                                                  	MMCards card=matchMoveApi.findCardByUserAndStatus(user);
		                                                  	if(card!=null){
		                                                  	map.addAttribute("cardstatus", card.isBlocked());
		                                                  	map.addAttribute("physicalCard", card.isHasPhysicalCard());
		                                                  	}
                                                return "Admin/DashBoard";
                                            } else {
                                                result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
                                                result.setMessage(auth.getMessage());
                                                map.put("errormsgg", auth.getMessage());
                                                return "Admin/Login";
                                            }
                                        }
                                    }else {
                                        userApi.requestNewLoginDevice(user);
                                        result.setStatus(ResponseStatus.NEW_DEVICE);
                                        result.setMessage("OTP sent to :" + user.getUsername());
                                        map.put(ModelMapKey.MESSAGE, "OTP sent to :" + user.getUsername());
                                        MMCards cards= mMCardRepository.getCardByUserAndStatus(user, "Active");
                                        if(cards!=null){
                                        	MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
                                      	  cardRequest.setEmail(user.getUserDetail().getEmail());
                                      	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
                                      	  cardRequest.setCardId(cards.getCardId());
                                      	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);	
                                      	  result.setHasVirtualCard(true);
                                      	  result.setCardDetails(walletResponse);
                                        }else{
                                      	  result.setHasVirtualCard(false);
                                        }
                                        MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
                                        	if(physicalCards!=null){
                                        		result.setHasPhysicalCard(true);
                                        	}else{
                                        		result.setHasPhysicalCard(false);
                                        	}
                                      
                                        return "Admin/VerifyMobile";
                                      }
                                    }else {
                                        result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
                                        result.setMessage(authError.getMessage());
                                        map.put("errormsgg", authError.getMessage());
                                        return "User/Login";
                                    }
								}else {
									if(true) {
										if (sessionApi.checkActiveSession(user)) {
											AuthenticationError auth = authentication(login,request);
											if (auth.isSuccess()) {
												try {
													String gcmId = login.getRegistrationId();
													if (gcmId != null) {
														userApi.updateGcmId(gcmId, user.getUsername());
													}
												} catch (NullPointerException e) {

												}
												Map<String, Object> detail = new HashMap<String, Object>();
												Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
												sessionLoggingStrategy.onAuthentication(authentication, request, response);
												UserSession userSession = userSessionRepository.findByActiveSessionId(
														RequestContextHolder.currentRequestAttributes().getSessionId());
												UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
												result.setStatus(ResponseStatus.SUCCESS);
												result.setMessage("Login successful.");
												detail.put("sessionId", userSession.getSessionId());
												detail.put("userDetail", activeUser);
												MPQAccountDetails account = user.getAccountDetail();
												account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
												detail.put("accountDetail", account);
												result.setDetails(detail);
												
	                                               MMCards cards= mMCardRepository.getVirtualCardsByCardUser(user);
		                                              if(cards!=null){
		                                            	  MatchMoveCreateCardRequest cardRequest=new MatchMoveCreateCardRequest();
		                                              	  cardRequest.setEmail(user.getUserDetail().getEmail());
		                                              	  cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
		                                              	  cardRequest.setCardId(cards.getCardId());
		                                              	  WalletResponse walletResponse= matchMoveApi.inquireCard(cardRequest);	
		                                            	  result.setHasVirtualCard(true);
		                                            	  result.setCardDetails(walletResponse);
		                                              }else{
		                                            	  result.setHasVirtualCard(false);
		                                              }
		                                              MMCards physicalCards= mMCardRepository.getPhysicalCardByUser(user);
		                                              	if(physicalCards!=null){
		                                              		result.setHasPhysicalCard(true);
		                                              	}else{
		                                              		result.setHasPhysicalCard(false);
		                                              	}
		                                            
		                                            	try {
		                                              		UserKycResponse walletResponse=new UserKycResponse(); 
		                            						walletResponse=matchMoveApi.getTransactions(userSession.getUser());
		                            						if(walletResponse.getDetails()!=null){
		                            						 JSONObject	obj = new JSONObject(walletResponse.getDetails().toString());
		                            						 JSONArray aa= obj.getJSONArray("transactions");
		                            						List<ResponseDTO> resul=new ArrayList<>();
		                            						for(int i=0;i<aa.length();i++){
		                            							if(1<5){
		                                							ResponseDTO resp= new ResponseDTO();
		                                							resp.setAmount(aa.getJSONObject(i).getString("amount"));
		                                							resp.setDate(aa.getJSONObject(i).getString("date"));
		                                							resp.setDescription(aa.getJSONObject(i).getString("description"));
		                                							resp.setStatus(aa.getJSONObject(i).getString("status"));
		                                							resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
		                                							resul.add(resp);}
		                            						}
		                            						map.put("transactions", resul);
		                            						}
		                            						} catch (JSONException e) {
		                            							e.printStackTrace();
		                            						}
		                                            	map.addAttribute("user", user);
		                                            	MMCards card=matchMoveApi.findCardByUserAndStatus(user);
	                                                  	map.addAttribute("cardstatus", card.isBlocked());
	                                                  	map.addAttribute("physicalCard", card.isHasPhysicalCard());
												return "Admin/DashBoard";
											} else {
												result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
												result.setMessage(auth.getMessage());
												result.setDetails(null);
                                                map.put("errormsgg", auth.getMessage());

												return "Admin/Login" ;
											}
										}
									}
								}
							}else{
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Looks like you haven't filled your profile completely..Please fill all the details or contact customer care");
								result.setDetails(null);
                                map.put("errormsgg", "Looks like you haven't filled your profile completely..Please fill all the details or contact customer care");

								return "Admin/Login" ;

							}
							}
						}else if (role.equalsIgnoreCase("Corporate")) {
						if (user.getAuthority().contains(Authorities.CORPORATE)) {
								if (login.isValidate()) {
								LoginError loginError = loginValidation.checkSuperAdminValidation(login, user);
								if (loginError.isSuccess()) {
									AuthenticationError authError = isValidUsernamePassword(login, request);
									if (authError.isSuccess()) {
										userApi.requestNewLoginDeviceForSuperAdmin(user);
										result.setStatus(ResponseStatus.NEW_DEVICE);
										result.setMessage("OTP sent to ::" + user.getUserDetail().getContactNo());
										 map.put(ModelMapKey.MESSAGE, "OTP sent to :" + user.getUserDetail().getContactNo());
									} else {
										result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
										result.setMessage(authError.getMessage());
										 map.put(ModelMapKey.ERROR, authError.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(loginError.getMessage());
								}
								return "Home";
							} else {
								if (true) {
									if (sessionApi.checkActiveSession(user)) {
										AuthenticationError auth = authentication(login, request);
										if (auth.isSuccess()) {
											Map<String, Object> detail = new HashMap<String, Object>();
											Authentication authentication = SecurityContextHolder.getContext()
													.getAuthentication();
											sessionLoggingStrategy.onAuthentication(authentication, request, response);
											UserSession userSession = userSessionRepository.findByActiveSessionId(
													RequestContextHolder.currentRequestAttributes().getSessionId());
											System.err.println("session iD ::" +RequestContextHolder.currentRequestAttributes().getSessionId());
											UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
											result.setStatus(ResponseStatus.SUCCESS);
											result.setMessage("Login successful.");
											detail.put("sessionId", userSession.getSessionId());
											detail.put("userDetail", activeUser);
											detail.put("accountDetail", user.getAccountDetail());
											result.setDetails(detail);
											return "SuperAdmin/Home";
										} else {
											result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
											result.setMessage(auth.getMessage());
											result.setDetails(null);
											return "Home";
										}
									}
								} 
							}
						}
					} else if (role.equalsIgnoreCase("Admin")) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								try {
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								MUser u = userApi.findByUserName(userSession.getUser().getUsername());
								u.setMobileToken(null);
								mUserRespository.save(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								request.getSession().setAttribute("adminSessionId", RequestContextHolder.currentRequestAttributes().getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								Double totalTxnAmount=transactionApi.getTransactionAmount();
								map.put("totalTxnAmount", totalTxnAmount);
								DecimalFormat f = new DecimalFormat("##.00");
								if(totalTxnAmount==null || totalTxnAmount<=0){
								map.addAttribute("totalCredit", "0.0");

								}else{
									map.addAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
								}
								} catch(Exception e) {
									e.printStackTrace();
									return "Admin/Home";
								}
								try {
								long totalCards=userApi.getMCardCount();
								map.put("totalCards", totalCards);}
								catch(Exception e) {
									e.printStackTrace();
									map.put("totalCards", "Unable to fetch");
								}
								try {	
								long totalPhysicalCard=userApi.gettotalPhysicalCard();
								map.put("totalPhysicalCards", totalPhysicalCard);
								} catch(Exception e){
									map.put("totalPhysicalCards", "Unable to fetch");
									e.printStackTrace();
								}
								try {
								long totalActiveUser=userApi.getTotalActiveUser();
								map.put("totalActiveUser", totalActiveUser);
								} catch(Exception e){
									map.put("totalActiveUser", "Unable to fetch");
									e.printStackTrace();
								}
								UserKycResponse walletResponse=new UserKycResponse(); 
								walletResponse=matchMoveApi.getConsumers();
								try {
									if(walletResponse!=null) {
										DecimalFormat f1 = new DecimalFormat("##.00");
										map.put("totalPrefundAmount", f1.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
									} 
								} catch(Exception e) {
									return "Admin/Home";
								}
								result.setDetails(detail);
								Long phyCount=mMCardRepository.getCountPhysicalCards();
								if(phyCount!=null){
									map.put("phycount", phyCount.longValue());
								} else{
									map.put("phycount", 0);
								}
								Long virtualCard=mMCardRepository.getCountVirtualCards();
								if(virtualCard!=null){
									map.put("virCount", virtualCard.longValue());
								}
								else {
									map.put("virCount", 0);
								}
							Long pendingCardReq=physicalCardDetailRepository.getCountPhysicalCardRequestPending();
							if(pendingCardReq!=null){
								map.put("pendingReq", pendingCardReq.longValue());
							}else{
								map.put("pendingReq",0);
							}
							
							MService razorpay= mServiceRepository.findServiceByCode("LMS");
							MService upiPay=mServiceRepository.findServiceByCode("UPS");
							
							List<String> monthsListRazorPay = mTransactionRepository.getMonthsForBarGraph(Status.Success, razorpay);
							List<String> monthsListUpi = mTransactionRepository.getMonthsForBarGraph(Status.Success, upiPay);
							List<Double> upiAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success, upiPay);
							List<Double> razorAmtList = mTransactionRepository.getTransactionAmountMonthWise(Status.Success, razorpay);
							System.err.println("size monthsListRazorPay:"+ monthsListRazorPay.size());
							
							System.err.println("size razorAmtList:"+razorAmtList.size());

							if(upiAmtList==null||upiAmtList.isEmpty()){
								upiAmtList.add(0.0);
							}
							if(monthsListUpi==null||monthsListUpi.isEmpty()){
								monthsListUpi.add("june");
							}
							System.err.println("size monthsListUpi:"+monthsListUpi.size());
							System.err.println("size upiAmtList:"+upiAmtList.size());
							if(monthsListRazorPay.size()>=monthsListUpi.size()){
								List<String> stringList=new ArrayList<>();
								for (String string : monthsListRazorPay) {
									stringList.add("'"+string+"'");
								}
								model.addAttribute("monthsList", stringList);

								int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
								System.err.println(sizeDiff);
								if(sizeDiff!=0){
									List<Double> doubleList=new ArrayList<>();
									
									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}
							}else{
								int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
								System.err.println(sizeDiff);
								if(sizeDiff!=0){
									List<Double> doubleList=new ArrayList<>();
									
									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}
							}
							model.addAttribute("amountListRazorPay", razorAmtList);
							AdminActivity activity = new AdminActivity();
							activity.setActivityName("Admin Login");
							adminActivityRepository.save(activity);
							return"Admin/Home";
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								map.put(ModelMapKey.ERROR, auth.getMessage());
								return "Admin/Login";
							}
						}
					} else if(role.equalsIgnoreCase("Partner")){

						if (user.getAuthority().contains(Authorities.CORPORATE_PARTNER)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								request.getSession().setAttribute("adminSessionId", RequestContextHolder.currentRequestAttributes().getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								Double totalTxnAmount=transactionApi.getTransactionAmount();
								map.put("totalTxnAmount", totalTxnAmount);
								DecimalFormat f = new DecimalFormat("##.00");
								map.addAttribute("totalCredit", f.format(Double.parseDouble(totalTxnAmount + "")));
								try{
								long totalCards=userApi.getMCardCount();
								map.put("totalCards", totalCards);}
								catch(Exception e){
								e.printStackTrace();
								map.put("totalCards", "Unable to fetch");
								}
								try{	
								long totalPhysicalCard=userApi.gettotalPhysicalCard();
								map.put("totalPhysicalCards", totalPhysicalCard);
								}catch(Exception e){
									map.put("totalPhysicalCards", "Unable to fetch");
									e.printStackTrace();
								}
								try{
								long totalActiveUser=userApi.getTotalActiveUser();
								map.put("totalActiveUser", totalActiveUser);
								}catch(Exception e){
									map.put("totalActiveUser", "Unable to fetch");
									e.printStackTrace();
								}
								UserKycResponse walletResponse=new UserKycResponse(); 
								walletResponse=matchMoveApi.getConsumers();
								if(walletResponse!=null){
									DecimalFormat f1 = new DecimalFormat("##.00");
									map.put("totalPrefundAmount", f1.format(Double.parseDouble(walletResponse.getPrefundingBalance())));
								}
								result.setDetails(detail);
								Long phyCount=mMCardRepository.getCountPhysicalCards();
								if(phyCount!=null){
									map.put("phycount", phyCount.longValue());
								}else{
									map.put("phycount", 0);

								}
								Long virtualCard=mMCardRepository.getCountVirtualCards();
								if(virtualCard!=null){
									map.put("virCount", virtualCard.longValue());
								}
								else{
									map.put("virCount", 0);

								}
							Long pendingCardReq=physicalCardDetailRepository.getCountPhysicalCardRequestPending();
							if(pendingCardReq!=null){
								map.put("pendingReq", pendingCardReq.longValue());
							}else{
								map.put("pendingReq",0);

							}
							
							MService razorpay= mServiceRepository.findServiceByCode("LMS");
							MService upiPay=mServiceRepository.findServiceByCode("UPS");
							
							List<String> monthsListRazorPay=mTransactionRepository.getMonthsForBarGraph(Status.Success, razorpay);
							List<String> monthsListUpi=mTransactionRepository.getMonthsForBarGraph(Status.Success, upiPay);
							List<Double> upiAmtList=mTransactionRepository.getTransactionAmountMonthWise(Status.Success, upiPay);
							List<Double> razorAmtList=mTransactionRepository.getTransactionAmountMonthWise(Status.Success, razorpay);
							System.err.println("size monthsListRazorPay:"+monthsListRazorPay.size());
							
							System.err.println("size razorAmtList:"+razorAmtList.size());

							if(upiAmtList==null||upiAmtList.isEmpty()){
								upiAmtList.add(0.0);
							}
							if(monthsListUpi==null||monthsListUpi.isEmpty()){
								monthsListUpi.add("june");
							}
							System.err.println("size monthsListUpi:"+monthsListUpi.size());
							System.err.println("size upiAmtList:"+upiAmtList.size());
							if(monthsListRazorPay.size()>=monthsListUpi.size()){
								List<String> stringList=new ArrayList<>();
								for (String string : monthsListRazorPay) {
									stringList.add("'"+string+"'");
								}
								model.addAttribute("monthsList", stringList);
								int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
								System.err.println(sizeDiff);
								if(sizeDiff!=0){
									List<Double> doubleList=new ArrayList<>();
									
									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}
							}else{
								int sizeDiff=monthsListRazorPay.size()-monthsListUpi.size();
								System.err.println(sizeDiff);
								if(sizeDiff!=0){
									List<Double> doubleList=new ArrayList<>();
									
									for (int i = 0; i < sizeDiff; i++) {
										doubleList.add(0.0);
									}
									for (Double double1 : upiAmtList) {
										doubleList.add(double1);
									}
									model.addAttribute("monthsAmountUPI", doubleList);
								}
							}
							model.addAttribute("amountListRazorPay", razorAmtList);			

							return"Admin/Home";
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								map.put(ModelMapKey.ERROR, auth.getMessage());
								return "Admin/Login";
							}
						}
					
					}

					else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.......");
						result.setDetails(null);
						map.put(ModelMapKey.ERROR, "Failed, Unauthorized user.......");
						return "Admin/Login";
					}

					
					} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails(null);
                    map.put("errormsgg","Failed, Unauthorized user.......");
					map.put(ModelMapKey.ERROR, "Failed, Unauthorized user.......");
					return "Admin/Login";
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your request is declined please contact customer care or try again later");
				result.setDetails("your request is declined please contact customer care or try again later");
				map.put(ModelMapKey.ERROR,"your request is declined please contact customer care or try again later");
				return "Home";
			}

		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage(error.getMessage());
		}
		}else {
			map.put("error", "Incorrect OTP");
			return "Admin/VerifyMobile";
		}
	
	} catch(Exception e){
		e.printStackTrace();
	}
		return "Home";
	}
	
	@RequestMapping(value = "/Process",method=RequestMethod.GET)
	String userLogin(@PathVariable(value = "role") String role ,
			HttpServletRequest request,HttpSession session, HttpServletResponse response,ModelMap map) {
		try {
			ResponseDTO result = new ResponseDTO();
			Map<String, Object> detail = new HashMap<String, Object>();
			String sessionId = request.getSession().getId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				MUser user = userSession.getUser();
				if (role.equalsIgnoreCase("User")) {
					if (user.getAuthority().contains(Authorities.USER)) {
						UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Login successful.");
						detail.put("sessionId", userSession.getSessionId());
						detail.put("userDetail", activeUser);
						MPQAccountDetails account = user.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f", account.getBalance())));
						detail.put("accountDetail", account);
						result.setDetails(detail);
						String cardNo = null;
						System.err.println("hi i here....");
						MMCards cards = mMCardRepository.getCardByUserAndStatus(user, "Active");
						System.err.println(cards);
						if (cards != null) {
							/*
							 * MMCardFetchRequest req=new MMCardFetchRequest();
							 * req.setCardId(cards.getCardId());
							 * req.setEmail(user.getUserDetail().getEmail());
							 * req.setMobileNo(user.getUsername()); MMCardFetchResponse resp=
							 * iMCardApi.fetchCardDetails(req);
							 */
							MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
							cardRequest.setEmail(user.getUserDetail().getEmail());
							try {
								cardRequest.setPassword(SecurityUtil.md5(user.getUsername()));
								cardRequest.setUsername(user.getUsername());
							} catch (Exception e) {
								e.printStackTrace();
							}
							cardRequest.setCardId(cards.getCardId());
							WalletResponse walletResponse = matchMoveApi.inquireCard(cardRequest);
							result.setHasVirtualCard(true);
							result.setCardDetails(walletResponse);
							cardNo = walletResponse.getWalletNumber();
							map.addAttribute("cardNo", cardNo.replaceAll("....", "$0   "));
						} else {
							result.setHasVirtualCard(false);
						}
						MMCards physicalCards = mMCardRepository.getPhysicalCardByUser(user);
						if (physicalCards != null) {
							result.setHasPhysicalCard(true);
						} else {
							result.setHasPhysicalCard(false);
							String phyCard = "xxxxxxxxxxxxxxxx";
							map.addAttribute("phyCardNo", phyCard.toUpperCase().replaceAll("....", "$0  "));
							map.addAttribute("phycvv", "XXX");
							map.addAttribute("phyExp", "XXXX-XX");
							map.addAttribute("cardHolder", "XXXXXXXXX");
						}
						map.addAttribute("resultSet", result);
						map.addAttribute("ActiveUser", activeUser);

						try {
							UserKycResponse walletResponse = new UserKycResponse();
							walletResponse = matchMoveApi.getTransactions(userSession.getUser());
							if (walletResponse.getDetails() != null) {
								JSONObject obj = new JSONObject(walletResponse.getDetails().toString());
								JSONArray aa = obj.getJSONArray("transactions");
								List<ResponseDTO> resul = new ArrayList<>();
								for (int i = 0; i < aa.length(); i++) {
									if (1 < 5) {
										ResponseDTO resp = new ResponseDTO();
										resp.setAmount(aa.getJSONObject(i).getString("amount"));
										resp.setDate(aa.getJSONObject(i).getString("date"));
										resp.setDescription(aa.getJSONObject(i).getString("description"));
										resp.setStatus(aa.getJSONObject(i).getString("status"));
										resp.setTransactionType(aa.getJSONObject(i).getString("indicator"));
										resul.add(resp);
									}
								}
								map.put("transactions", resul);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						Map<Object, Object> map1 = null;
						List<List<Map<Object, Object>>> list = new ArrayList<List<Map<Object, Object>>>();
						List<Map<Object, Object>> dataPoints1 = new ArrayList<Map<Object, Object>>();
						map1 = new HashMap<Object, Object>();
						map1.put("label", "Credit");
						map1.put("y", 30);
						dataPoints1.add(map1);
						map1 = new HashMap<Object, Object>();
						map1.put("label", "Debit");
						map1.put("y", 25);
						dataPoints1.add(map1);
						list.add(dataPoints1);
						map.addAttribute("dataPointsList", list);
						map.addAttribute("user", user);
						MMCards card = matchMoveApi.findCardByUserAndStatus(user);
						map.addAttribute("cardstatus", card.isBlocked());
						map.addAttribute("physicalCard", card.isHasPhysicalCard());
						map.put("LoadMoney", session.getAttribute("LoadMess"));
						session.setAttribute("LoadMess", "");
						return "User/DashBoard";

					}
				}
			}
		} catch(Exception e){
		e.printStackTrace();
	}
		session.invalidate();
		return "Home";
	}

	
	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			System.err.println("password ::" + dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setMaxInactiveInterval(24*60*60);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	String logoutUserApi(@PathVariable(value = "role") String role ,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		ResponseDTO result = new ResponseDTO();

			try {
				UserSession userSession = userSessionRepository.findBySessionId(session.getId());
				if (userSession != null) {
					sessionApi.expireSession(session.getId());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("User logout successful");
					result.setDetails("Session Out");
					session.invalidate();
					return "Home";
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					session.invalidate();
					return "Home";
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails("Failed, invalid request.");
				return "Home";
			}
		
	}
	

	public static void main(String[] args) {
		List<Integer> list=new ArrayList<>();
		list.add(4);
		list.add(9);
		list.add(1);
		System.err.println(Collections.min(list));
	}
}
