package com.msscard.validation;

import java.util.List;

import com.msscard.entity.CashBack;
import com.msscard.entity.MUser;
import com.msscard.model.error.CashBackError;
import com.msscard.repositories.CashBackRepository;

public class CashBackValidations {

	private final CashBackRepository cashBackRepository;
	
	
	public CashBackValidations(CashBackRepository cashBackRepository) {
		super();
		this.cashBackRepository = cashBackRepository;
	}


	public CashBackError validateCashBack(MUser user,String merchantName){
		
		CashBackError cashBackError=new CashBackError();
		boolean valid=true;
		List<CashBack> cashBackList=cashBackRepository.getCashBackByUser(user);
		for (CashBack cashBack : cashBackList) {
			if(cashBack.getMerchantDetails().getMerchantName().equalsIgnoreCase(merchantName.trim())){
				valid=false;
				cashBackError.setMessage("CashBack Already been given to this User for "+merchantName.trim());
				return cashBackError;
		}
		}
	cashBackError.setValid(valid);
	return cashBackError;
		
	}
}
