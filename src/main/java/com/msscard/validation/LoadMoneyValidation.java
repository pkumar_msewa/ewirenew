package com.msscard.validation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date; 
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.cashiercards.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.ITransactionApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MService;
import com.msscard.entity.MTransaction;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.TwoMinBlock;
import com.msscard.entity.UserSession;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.TransactionError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.TwoMinBlockRepository;
import com.msscard.repositories.UserSessionRepository;
import com.msscard.util.CommonUtil;
import com.msscard.util.MatchMoveUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class LoadMoneyValidation {

	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final MCommissionRepository commissionRepository;
	private final MMCardRepository cardRepository;
	private final DataConfigRepository dataConfigRepository;
	private final MUserRespository mUserRespository;
	private final TwoMinBlockRepository twoMinBlockRepository;
	private final MMCardRepository mMCardRepository;
	private final IMatchMoveApi matchMoveApi;
	private final UserSessionRepository userSessionRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public LoadMoneyValidation(ITransactionApi transactionApi, IUserApi userApi,
			MCommissionRepository commissionRepository, MMCardRepository cardRepository,
			DataConfigRepository dataConfigRepository, MUserRespository mUserRespository,
			TwoMinBlockRepository twoMinBlockRepository, MMCardRepository mMCardRepository, IMatchMoveApi matchMoveApi,
			UserSessionRepository userSessionRepository, MPQAccountTypeRepository mPQAccountTypeRepository,
			MatchMoveWalletRepository matchMoveWalletRepository) {
		super();
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.commissionRepository = commissionRepository;
		this.cardRepository = cardRepository;
		this.dataConfigRepository = dataConfigRepository;
		this.mUserRespository = mUserRespository;
		this.twoMinBlockRepository = twoMinBlockRepository;
		this.mMCardRepository = mMCardRepository;
		this.matchMoveApi = matchMoveApi;
		this.userSessionRepository = userSessionRepository;
		this.mPQAccountTypeRepository = mPQAccountTypeRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
	}

	public TransactionError validateLoadMoneyTransaction(String amount, String senderUsername, MService service) {
		TransactionError error = new TransactionError();
		DataConfig configDatas = dataConfigRepository.findDatas();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);

		MMCards virCard = cardRepository.getVirtualCardsByCardUser(senderUser);
		MMCards phyCard = cardRepository.getPhysicalCardByUser(senderUser);

		MCommission comm = commissionRepository.findCommissionByService(service);
		double finalTransactionAmount = Double.parseDouble(amount);

		if (comm != null) {
			double commis = CommonUtil.commissionEarned(comm, Double.parseDouble(amount));
			finalTransactionAmount = commis + Double.parseDouble(amount);

			BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
			BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			finalTransactionAmount = roundOff.doubleValue();
		}

		if (Status.Inactive.equals(senderUser.getMobileStatus())) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		if (!(senderUser.getAccountDetail().getAccountType().getCode().equals("KYC"))
				&& Double.valueOf(amount) > service.getMaxAmount()) {
			error.setMessage("User needs to be Full KYC to load money of above " + service.getMaxAmount());
			error.setValid(false);
			return error;
		}

		if (Status.Inactive.getValue().equalsIgnoreCase(service.getStatus().getValue())) {
			valid = false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}

		double transactionAmount = finalTransactionAmount;
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double virtualCardMonthlyLimit = configDatas.getVirtualCardLoadMonthlyLimit();
		double virtualCardDailyLimit = configDatas.getVirtualCardLoadDailyLimit();

		if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly, transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}

		if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit, transactionAmount)) {
			error.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
					+ " and you've already credited total of Rs." + totalDailyCredit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		if (virCard != null && virCard.getStatus().equalsIgnoreCase("Active")) {
			if (!CommonValidation.dailyCreditLimitCheck(virtualCardDailyLimit, totalDailyCredit, transactionAmount)) {
				error.setMessage("Daily Credit Limit Exceeded For Virtual Card. Your Daily limit is Rs."
						+ virtualCardDailyLimit + " and you've already credited total of Rs." + totalDailyCredit);
				valid = false;
				error.setValid(valid);
				return error;
			}

			if (!CommonValidation.monthlyCreditLimitCheck(virtualCardMonthlyLimit, totalCreditMonthly,
					transactionAmount)) {
				error.setMessage("Monthly Credit Limit Exceeded for your Virtual Card. Your monthly limit is Rs."
						+ virtualCardMonthlyLimit + " and you've already credited total of Rs." + totalCreditMonthly);
				valid = false;
				error.setValid(valid);
				return error;
			}
		}

		double amt = Double.parseDouble(amount);
		if (!CommonValidation.isNumeric(amount)) {
			error.setMessage("Please enter valid amount");
			valid = false;
		}

		if (amt < service.getMinAmount() || amt >= service.getMaxAmount()) {
			error.setMessage("Amount must be between " + service.getMinAmount() + " to " + service.getMaxAmount());
			valid = false;
		}

		if (phyCard != null && Status.Inactive.getValue().equalsIgnoreCase(phyCard.getStatus())) {
			valid = false;
			error.setValid(valid);
			error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			return error;
		} else if (virCard != null && virCard.getStatus().equalsIgnoreCase("Inactive")) {
			if (phyCard == null) {
				valid = false;
				error.setValid(valid);
				error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
				return error;
			}
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail(), false);
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please  try after one minute");
				error.setValid(false);
				return error;
			}
		}

		int count = 0;
		Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Success);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 2) {
				MTransaction tran = transactionApi.getLastTransactionDetails(reverseTransactionDate,
						senderUser.getAccountDetail(), false);
				double prevAmount = tran.getAmount();

				error.setMessage("Please try again after 2 mins");
				if (prevAmount == finalTransactionAmount) {
					++count;
				}

				if (count == 5) {
					senderUser.setAuthority("ROLE_USER,ROLE_BLOCKED");
					mUserRespository.save(senderUser);

					TwoMinBlock minBlock = new TwoMinBlock();
					minBlock.setBlock(true);
					minBlock.setUser(senderUser);
					minBlock.setStatus(Status.Blocked.getValue());
					twoMinBlockRepository.save(minBlock);

					String activeCardId = null;
					List<MMCards> cardList = mMCardRepository.getCardsListByUser(senderUser);
					if (cardList != null && cardList.size() > 0) {
						for (MMCards mmCards : cardList) {
							if (mmCards.isHasPhysicalCard()) {
								if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())
										|| !mmCards.isBlocked()) {
									activeCardId = mmCards.getCardId();
								}
							} else {
								if (Status.Active.getValue().equalsIgnoreCase(mmCards.getStatus())
										|| !mmCards.isBlocked()) {
									activeCardId = mmCards.getCardId();

									mmCards.setStatus(Status.Inactive.getValue());
									mMCardRepository.save(mmCards);
								}
							}
						}

						MatchMoveCreateCardRequest request = new MatchMoveCreateCardRequest();
						request.setRequestType("suspend");
						request.setCardId(activeCardId);
						ResponseDTO res = matchMoveApi.deActivateCards(request);

						if (res.getCode().equalsIgnoreCase("S00")) {
							if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(res.getCode())) {
								error.setMessage(res.getMessage());
								error.setValid(false);
								return error;
							}
						} else {
							error.setMessage(ResponseStatus.FAILURE.getValue());
							error.setValid(false);
							return error;
						}
					}
					error.setValid(false);
					return error;
				}
			}
		}

		error.setValid(valid);
		return error;
	}

	public TransactionError validateLoadMoneyTransactionUpi(String amount, String senderUsername, MService service,
			String address) {
		TransactionError error = new TransactionError();
		DataConfig configDatas = dataConfigRepository.findDatas();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);

		MMCards virCard = cardRepository.getVirtualCardsByCardUser(senderUser);
		MMCards phyCard = cardRepository.getPhysicalCardByUser(senderUser);

		MCommission comm = commissionRepository.findCommissionByService(service);
		double finalTransactionAmount = Double.parseDouble(amount);

		if (comm != null) {
			double commis = CommonUtil.commissionEarned(comm, Double.parseDouble(amount));
			finalTransactionAmount = commis + Double.parseDouble(amount);

			BigDecimal a = new BigDecimal(String.valueOf(finalTransactionAmount));
			BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			finalTransactionAmount = roundOff.doubleValue();
		}

		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		if (!(senderUser.getAccountDetail().getAccountType().getCode().equals("KYC"))
				&& Double.valueOf(amount) > service.getMaxAmount()) {
			error.setMessage("User needs to be Full KYC to load money of above " + service.getMaxAmount());
			error.setValid(false);
			return error;
		}

		if (service.getStatus().getValue().equalsIgnoreCase("Inactive")) {
			valid = false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}

		double transactionAmount = finalTransactionAmount;
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double virtualCardMonthlyLimit = configDatas.getVirtualCardLoadMonthlyLimit();
		double virtualCardDailyLimit = configDatas.getVirtualCardLoadDailyLimit();

		if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly, transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}

		if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit, transactionAmount)) {
			error.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
					+ " and you've already credited total of Rs." + totalDailyCredit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		if (virCard != null && virCard.getStatus().equalsIgnoreCase("Active")) {
			if (!CommonValidation.dailyCreditLimitCheck(virtualCardDailyLimit, totalDailyCredit, transactionAmount)) {
				error.setMessage("Daily Credit Limit Exceeded For Virtual Card. Your Daily limit is Rs."
						+ virtualCardDailyLimit + " and you've already credited total of Rs." + totalDailyCredit);
				valid = false;
				error.setValid(valid);
				return error;
			}

			if (!CommonValidation.monthlyCreditLimitCheck(virtualCardMonthlyLimit, totalCreditMonthly,
					transactionAmount)) {
				error.setMessage("Monthly Credit Limit Exceeded for your Virtual Card. Your monthly limit is Rs."
						+ virtualCardMonthlyLimit + " and you've already credited total of Rs." + totalCreditMonthly);
				valid = false;
				error.setValid(valid);
				return error;
			}
		}

		double amt = Double.parseDouble(amount);
		if (!CommonValidation.isNumeric(amount)) {
			error.setMessage("Please enter valid amount");
			valid = false;
		}

		if (amt < service.getMinAmount() || amt >= service.getMaxAmount()) {
			error.setMessage("Amount must be between " + service.getMinAmount() + " to " + service.getMaxAmount());
			valid = false;
		}

		if (phyCard != null && phyCard.getStatus().equalsIgnoreCase("Inactive")) {
			valid = false;
			error.setValid(valid);
			error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
			return error;
		} else if (virCard != null && virCard.getStatus().equalsIgnoreCase("Inactive")) {
			if (phyCard == null) {
				valid = false;
				error.setValid(valid);
				error.setMessage("Load Money for your account has been deactivated.Please contact customer care");
				return error;
			}
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail(), false);
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait");
				error.setValid(false);
				return error;
			}
		}

		int count;
		Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Success);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 2) {
				MTransaction tran = transactionApi.getLastTransactionDetails(reverseTransactionDate,
						senderUser.getAccountDetail(), false);
				double prevAmount = tran.getAmount();
				UserSession session = userSessionRepository.findByActiveUserSessions(senderUser);
				error.setMessage("Please try again after 2 mins");

				TwoMinBlock minBlock = twoMinBlockRepository.getBlockedUser(senderUser);
				if (minBlock == null) {
					if (prevAmount == finalTransactionAmount && session.getIpAddress().equalsIgnoreCase(address)) {
						minBlock = new TwoMinBlock();
						minBlock.setCounting(1);
						minBlock.setUser(senderUser);
						twoMinBlockRepository.save(minBlock);
					}
				} else {
					if (prevAmount == finalTransactionAmount && session.getIpAddress().equalsIgnoreCase(address)) {
						count = minBlock.getCounting() + 1;
						minBlock.setCounting(count);
						twoMinBlockRepository.save(minBlock);
					}
				}

				if (minBlock.getCounting() == 5) {
					senderUser.setAuthority("ROLE_USER,ROLE_BLOCKED");
					mUserRespository.save(senderUser);

					minBlock.setBlock(true);
					twoMinBlockRepository.save(minBlock);
				}
			}

			String activeCardId = null;
			List<MMCards> cardList = mMCardRepository.getCardsListByUser(senderUser);
			if (cardList != null && cardList.size() > 0) {
				for (MMCards mmCards : cardList) {
					if (mmCards.isHasPhysicalCard()) {
						if (mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()) {
							activeCardId = mmCards.getCardId();
						}
					} else {
						if (mmCards.getStatus().equalsIgnoreCase("Active") || !mmCards.isBlocked()) {
							activeCardId = mmCards.getCardId();
							mmCards.setStatus(Status.Inactive.getValue());
							mMCardRepository.save(mmCards);
						}
					}
				}
				MatchMoveCreateCardRequest request = new MatchMoveCreateCardRequest();
				request.setRequestType("suspend");
				request.setCardId(activeCardId);
				ResponseDTO res = matchMoveApi.deActivateCards(request);

				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(res.getCode())) {

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(res.getCode())) {
						error.setMessage(res.getMessage());
						error.setValid(false);
						return error;
					}
				} else {
					error.setMessage(ResponseStatus.FAILURE.getValue());
					error.setValid(false);
					return error;
				}
			}
		}
		error.setValid(false);
		return error;
	}

	public TransactionError validateMdexTimestampTransaction(double amount, String senderUsername) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);

		Date reverseTransactionDate = transactionApi.getLastTransactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Refunded);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 2) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 minute");

				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}

	public TransactionError validateLoadCardTransaction(String amount, String senderUsername, MService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(senderUsername);

		if (Status.Inactive.equals(senderUser.getMobileStatus())) {
			error.setMessage("Please verify User mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		if (Status.Inactive.equals(service.getStatus())) {
			valid = false;
			error.setValid(false);
			error.setMessage("Service is temporarily down");
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDailyCredit = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());

		if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly, transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. User monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		}

		if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit, transactionAmount)) {
			error.setMessage("Daily Credit Limit Exceeded. User Daily limit is Rs." + dailyTransactionLimit
					+ " and you've already credited total of Rs." + totalDailyCredit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		double amt = Double.parseDouble(amount);
		if (!CommonValidation.isNumeric(amount)) {
			error.setMessage("Please enter valid amount");
			valid = false;
		} else if (amt <= 0 || amt >= service.getMaxAmount()) {
			error.setMessage("Amount must be between 1 to" + service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

	public TransactionError validateP2PFundTransaction(String amount, String senderUsername, MService service,
			MCommission commission, double netCommissionValue) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		MUser senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		double transactionAmount = Double.parseDouble(amount);
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalDailyTransaction = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalMonthlyTransaction = transactionApi
				.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalMonthlyDebit = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		transactionAmount = transactionAmount + netCommissionValue;

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
			error.setMessage("Insufficient Balance. Your current balance is Rs."
					+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);

			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your monthly transaction limit is Rs."
					+ monthlyTransactionLimit + " and you already made total transaction of Rs." + totalMonthlyDebit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		error.setValid(valid);
		return error;
	}

	public String CorporateLoadCardMaxLimit(String amount, double loadCardMaxLimit,
			MPQAccountDetails mpqAccountDetails) {
		double totalDailyCredit = transactionApi.getDailyDebitTransationTotalAmount(mpqAccountDetails);
		if (amount != null) {
			if (loadCardMaxLimit >= totalDailyCredit + Double.parseDouble(amount)) {
				return null;
			}
			return "Max limit for load card has been exceed!";
		}
		return null;
	}

	public CommonError validateKYCCreditLimit(MUser user, double amount) {
		CommonError error = new CommonError();
		try {
			ClientResponse clientResponse = null;
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				System.out.println("MM User _Id===>" + wallet.getMmUserId());
				WebResource webResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/wallets");
				clientResponse = webResource.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", wallet.getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject jsonObject = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200) {
					error.setMessage("wallet not Found.");
					error.setValid(false);
				} else {
					Client client1 = Client.create();
					client1.addFilter(new LoggingFilter(System.out));
					System.out.println("MM User _Id===>" + wallet.getMmUserId());
					WebResource webResource1 = client1
							.resource(MatchMoveUtil.MATCHMOVE_HOST_URL + "/users/authentications/documents");
					ClientResponse clientResponse1 = webResource1
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-ID", wallet.getMmUserId()).get(ClientResponse.class);
					String accountType = clientResponse1.getEntity(String.class);
					JSONObject accountTypejson = new JSONObject(accountType);
					if (clientResponse1.getStatus() != 200) {
						error.setMessage("Account type not fetchd.");
						error.setValid(false);
					} else {
						String status = accountTypejson.getString("status");
						if (status.equalsIgnoreCase("approved")) {
							MPQAccountType kycAccountType = mPQAccountTypeRepository.findByCode("KYC");
							if (kycAccountType.equals(user.getAccountDetail().getAccountType())) {
								JSONObject kyclimits = jsonObject.getJSONObject("details").getJSONObject("topup_limits")
										.getJSONObject("post_kyc");
								double kycmonthlyLimit = kyclimits.getDouble("monthly_transactional_limit");
								double kycdailyLimit = kyclimits.getDouble("daily_transactional_limit");
								JSONObject kyccurrentlimit = jsonObject.getJSONObject("details")
										.getJSONObject("topup_limits").getJSONObject("current");
								double kycCurrentmonthlyLimit = kyccurrentlimit.getDouble("monthly_transactional");
								double kycCurrentdailyLimit = kyccurrentlimit.getDouble("daily_transactional");
								if (user.getAccountDetail().getAccountType().equals(kycAccountType)) {
									double remainingMonthlyBalanceLimit = kycmonthlyLimit - kycCurrentmonthlyLimit;
									double remainingDailyLimit = kycdailyLimit - kycCurrentdailyLimit;
									if (remainingDailyLimit >= amount) {
										if (remainingMonthlyBalanceLimit >= amount) {
											error.setMessage("Balance remaining.");
											error.setValid(true);
										} else {
											error.setMessage("wallet monthly limits exceeds.");
											error.setValid(false);
										}
									} else {
										error.setMessage("wallet limits exceeds.");
										error.setValid(false);
									}
								} 
							}
						} else {
							JSONObject nonkyclimits = jsonObject.getJSONObject("details").getJSONObject("topup_limits")
									.getJSONObject("pre_kyc");
							double monthlyLimit = nonkyclimits.getDouble("monthly_transactional_limit");
							double dailyLimit = nonkyclimits.getDouble("daily_transactional_limit");
							JSONObject currentlimit = jsonObject.getJSONObject("details").getJSONObject("topup_limits")
									.getJSONObject("current");
							double currentmonthlyLimit = currentlimit.getDouble("monthly_transactional");
							double currentdailyLimit = currentlimit.getDouble("daily_transactional");
							MPQAccountType nonkycAccountType = mPQAccountTypeRepository.findByCode("NONKYC");
							if (user.getAccountDetail().getAccountType().equals(nonkycAccountType)) {
								double remainingmonthlyBalanceLimit = monthlyLimit - currentmonthlyLimit;
								double remainingDailyBalanceLimit = dailyLimit - currentdailyLimit;
								if (remainingDailyBalanceLimit >= amount) {
									if (remainingmonthlyBalanceLimit >= amount) {
										error.setMessage("Balance remaining.");
										error.setValid(true);
									} else {
										error.setMessage("wallet monthly limits exceeds.");
										error.setValid(false);
									}
								} else {
									error.setMessage("wallet limits exceeds.");
									error.setValid(false);
								}
							} else {
								error.setMessage("invalid Account type");
								error.setValid(false);
							}
						}
					}
				}
			} else {
				error.setMessage("wallet not found.");
				error.setValid(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setMessage("exception occured while checking KYC limits. Please try after some time");
			error.setValid(false);
		}
		return error;
	}
}