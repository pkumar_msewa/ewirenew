package com.msscard.validation;


import org.codehaus.jettison.json.JSONObject;

import com.cashiercards.paramerterization.DataConfig;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.model.request.SendMoneyRequestDTO;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.model.Status;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.SendMoneyErrorDTO;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.util.MatchMoveUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class SendMoneyValidation {

	private final DataConfigRepository dataConfigRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MMCardRepository cardRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MPQAccountTypeRepository mPQAccountTypeRepository;

	public SendMoneyValidation(DataConfigRepository dataConfigRepository, IMatchMoveApi matchMoveApi,
			MMCardRepository cardRepository, MatchMoveWalletRepository matchMoveWalletRepository,
			MPQAccountTypeRepository mPQAccountTypeRepository) {
		super();
		this.dataConfigRepository = dataConfigRepository;
		this.matchMoveApi = matchMoveApi;
		this.cardRepository = cardRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mPQAccountTypeRepository = mPQAccountTypeRepository;
	}

	@SuppressWarnings("unused")
	public SendMoneyErrorDTO donationMoneyError(SendMoneyRequestDTO error,MUser sender){
		boolean valid=true;
		SendMoneyErrorDTO errorDTO=new SendMoneyErrorDTO();
		double reqAmt=Double.parseDouble(error.getAmount());		
		DataConfig datas=dataConfigRepository.findDatas();
		double minAmountLimit=Double.parseDouble(datas.getSendMoneyMinimum());
		double maxAmountLimit=Double.parseDouble(datas.getSendMoneyMaximum());
		double balance=matchMoveApi.getBalance(sender);
		
		MMCards senderVCard=cardRepository.getVirtualCardsByCardUser(sender);
		MMCards senderPCard=cardRepository.getPhysicalCardByUser(sender);
		MMCards activeCardSender=null;
				
		if(sender.getAccountDetail().getAccountType().getCode().trim().equalsIgnoreCase("NONKYC")){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Please upgrade your account to KYC to avail this feature");
			return errorDTO;
		}
		if(sender.getUsername().equalsIgnoreCase(error.getRecipientNo())){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You cannot send money to yourself..Please Stop trying..");
			return errorDTO;
		}
		
		if(reqAmt>maxAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The maximum limit for sending money is Rs "+maxAmountLimit);
			return errorDTO;
		}
		
		if(reqAmt<minAmountLimit){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("The minimum limit for sending money is Rs "+minAmountLimit);
			return errorDTO;
		}
		
		if(senderVCard==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;
		}
		
		if(senderPCard!=null){
		if(Status.Active.getValue().equalsIgnoreCase(senderPCard.getStatus())){
			activeCardSender=senderPCard;
		}else{
			activeCardSender=senderVCard;
		}
		}else{
			activeCardSender=senderVCard;
		}
		
		if(activeCardSender==null){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("Sender has no Active card.Transaction failed");
			return errorDTO;

		}
		if(reqAmt>balance){
			valid=false;
			errorDTO.setValid(valid);
			errorDTO.setMessage("You have insufficient funds.Transaction failed");
			return errorDTO;

		}
	
		errorDTO.setValid(valid);
		return errorDTO;
	}

	public CommonError validateKYCDebitLimit(MUser user, String amount) {
		CommonError error = new CommonError();
		try {
			error.setValid(true);
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				WebResource webResource = client.resource(MatchMoveUtil.MATCHMOVE_HOST_URL +"/users/wallets");
				ClientResponse clientResponse = webResource
						.header("Authorization", MatchMoveUtil.getBasicAuthorization())
						.header("X-Auth-User-ID", wallet.getMmUserId()).get(ClientResponse.class);
				String strResponse = clientResponse.getEntity(String.class);
				JSONObject jsonObject = new JSONObject(strResponse);
				if (clientResponse.getStatus() != 200) {
					error.setMessage("wallet not fetchd.");
					error.setValid(false);
				} else {
					Client client1 = Client.create();
					client1.addFilter(new LoggingFilter(System.out));
					WebResource webResource1 = client1.resource(MatchMoveUtil.MATCHMOVE_HOST_URL+"/users/authentications/documents");
					ClientResponse clientResponse1 = webResource1
							.header("Authorization", MatchMoveUtil.getBasicAuthorization())
							.header("X-Auth-User-ID", wallet.getMmUserId()).get(ClientResponse.class);
					String accountType = clientResponse1.getEntity(String.class);
					JSONObject accountTypejson = new JSONObject(accountType);
					if (clientResponse1.getStatus() != 200) {
						error.setMessage("Account type not fetchd.");
						error.setValid(false);
					} else {
						String status = accountTypejson.getString("status");
						if ("approved".equalsIgnoreCase(status)) {
							MPQAccountType kycAccountType = mPQAccountTypeRepository.findByCode("KYC");
								JSONObject kyclimits = jsonObject.getJSONObject("details")
										.getJSONObject("deduct_limits").getJSONObject("post_kyc");
								double kycmonthlyLimit = kyclimits.getDouble("monthly_transactional_limit");
								double kycdailyLimit = kyclimits.getDouble("daily_transactional_limit");
								JSONObject kyccurrentlimit = jsonObject.getJSONObject("details")
										.getJSONObject("deduct_limits").getJSONObject("current");
								double kycCurrentmonthlyLimit = kyccurrentlimit.getDouble("monthly_transactional");
								double kycCurrentdailyLimit = kyccurrentlimit.getDouble("daily_transactional");
								if (user.getAccountDetail().getAccountType().equals(kycAccountType)) {
									double remainingMonthlyBalanceLimit = kycmonthlyLimit - kycCurrentmonthlyLimit;
									double remainingDailyLimit = kycdailyLimit - kycCurrentdailyLimit;
									if (remainingDailyLimit >=  Double.parseDouble(amount)) {
										if (remainingMonthlyBalanceLimit >=  Double.parseDouble(amount)) {
											error.setMessage("Balance remaining.");
											error.setValid(true);
										} else {
											error.setMessage("wallet monthly limits exceeds.");
											error.setValid(false);
										}
									} else {
										error.setMessage("wallet limits exceeds.");
										error.setValid(false);
									}
								}else {
									error.setMessage("invalid Account type");
									error.setValid(false);
								}
						} else {
							JSONObject nonkyclimits = jsonObject.getJSONObject("details").getJSONObject("deduct_limits")
									.getJSONObject("pre_kyc");
							double monthlyLimit = nonkyclimits.getDouble("monthly_transactional_limit");
							double dailyLimit = nonkyclimits.getDouble("daily_transactional_limit");
							JSONObject currentlimit = jsonObject.getJSONObject("details").getJSONObject("deduct_limits")
									.getJSONObject("current");
							double currentmonthlyLimit = currentlimit.getDouble("monthly_transactional");
							double currentdailyLimit = currentlimit.getDouble("daily_transactional");
							MPQAccountType nonkycAccountType = mPQAccountTypeRepository.findByCode("NONKYC");
							if (user.getAccountDetail().getAccountType().equals(nonkycAccountType)) {
								double remainingmonthlyBalanceLimit = monthlyLimit - currentmonthlyLimit;
								double remainingDailyBalanceLimit = dailyLimit - currentdailyLimit;
								if (remainingDailyBalanceLimit >= Double.parseDouble(amount)) {
									if (remainingmonthlyBalanceLimit >=  Double.parseDouble(amount)) {
										error.setMessage("Balance remaining.");
										error.setValid(true);
									} else {
										error.setMessage("wallet monthly limits exceeds.");
										error.setValid(false);
									}
								} else {
									error.setMessage("wallet limits exceeds.");
									error.setValid(false);
								}
							} else {
								error.setMessage("invalid Account type");
								error.setValid(false);
							}
						}
					}
				}
			} else {
				error.setMessage("Error while fetching user wallet");
				error.setValid(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return error;
	}
}
