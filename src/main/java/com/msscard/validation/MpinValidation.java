package com.msscard.validation;

import java.text.SimpleDateFormat;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.msscard.entity.MUser;
import com.msscard.model.ChangeMpinError;
import com.msscard.model.ForgotMPinDTO;
import com.msscard.model.ForgotMPinError;
import com.msscard.model.MPinChangeDTO;
import com.msscard.model.MpinDTO;
import com.msscard.model.error.MpinError;



public class MpinValidation {

	private final PasswordEncoder passwordEncoder;
	public MpinValidation(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public MpinError validateNewMpin(MpinDTO dto) {
		MpinError error = new MpinError();
		boolean valid = true;
		if (CommonValidation.isNull(dto.getNewMpin())) {
			error.setMessage("MPIN must not be Empty");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmMpin())) {
			error.setMessage("Please Re-Enter your MPIN");
			valid = false;
		} else if (!(dto.getNewMpin().equals(dto.getConfirmMpin()))) {
			error.setMessage("Both MPIN must be equal");
			valid = false;
		} else if (!(CommonValidation.isNumeric(dto.getNewMpin()))) {
			error.setMessage("MPIN must be numeric");
			valid = false;
		} else if (!(CommonValidation.checkValidMpin(dto.getNewMpin()))) {
			error.setMessage("MPIN must be 4 characters long");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public ChangeMpinError validateChangeMpin(MPinChangeDTO dto) {
		ChangeMpinError error = new ChangeMpinError();
		boolean valid = true;
        if (CommonValidation.isNull(dto.getNewMpin())) {
			error.setNewMpin("enter your new MPIN");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmMpin())) {
			error.setConfirmMpin("Re Enter your new MPIN");
			valid = false;
		} else if (!CommonValidation.isNumeric(dto.getNewMpin())) {
			error.setNewMpin("MPIN must be in numeric form");
			valid = false;
		} else if (!CommonValidation.checkValidMpin(dto.getNewMpin())) {
			error.setNewMpin("MPIN but be 6 digits long");
			valid = false;
		} else if (!(dto.getNewMpin().equals(dto.getConfirmMpin()))) {
			error.setConfirmMpin("Both MPIN must be equal");
			valid = false;
		}
		error.setValid(valid);
		return error;

	}

	public ForgotMPinError checkError(ForgotMPinDTO dto,MUser u){
		ForgotMPinError error = new ForgotMPinError();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		boolean valid = true;
		String currentPassword = u.getPassword();

		String currentDateOfBirth = dateFormat.format(u.getUserDetail().getDateOfBirth());
		if(!(passwordEncoder.matches(dto.getPassword(),currentPassword))){
			valid = false;
			error.setPassword("Password doesn't match");
		}

		if(!(dto.getDateOfBirth().equals(currentDateOfBirth))){
			valid = false;
			error.setDateOfBirth("Date Of Birth doesn't match");
		}
		error.setValid(valid);
		return error;
	}
}
