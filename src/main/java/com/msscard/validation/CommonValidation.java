package com.msscard.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.multipart.MultipartFile;

import com.msscard.entity.MService;
import com.msscard.app.model.request.AssignPhysicalCardAgentDTO;
import com.msscard.app.model.request.BusAvailableTripListRequest;
import com.msscard.app.model.request.BusBookTicketRequest;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.ForgotPasswordDTO;
import com.msscard.app.model.request.TravellerDetailsDTO;
import com.msscard.model.AgentPrefundDTO;
import com.msscard.model.BusSeatDetailRequest;
import com.msscard.model.PromoCodeDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.TransactionIdDTO;
import com.msscard.model.UpgradeAccountDTO;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.ErrorResponse;

public class CommonValidation {

	/**
	 * Checks String with length greater than 6
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkLength6(String str) {

		String temp = str.trim();
		if (temp.length() >= 6) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks merchant password
	 *
	 * @param str
	 * @return boolean
	 */

	public static boolean checkMerchantPassword(String str) {
		String regex = "^[\\p{Digit}]{6}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateVBankAccountNumber(String str){
		String regex = "^[\\p{Digit}]{15}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}


	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMerchantVBankAccountNumber(String str){
		String regex = "^[M]{1}[\\p{Digit}]{15}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}



	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMobileNumber(String str){
		String regex = "^[7-9]{1}[0-9]{9}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	/**
	 *
	 * @param str
	 * @return boolean
	 */
	public static boolean validateMerchantMobileNumber(String str){
		String regex = "^[M]{1}[7-9]{1}[0-9]{9}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

//	public static void main(String... args){
//		System.err.println(validateMerchantMobileNumber("M8769986881"));
//	}


	/**
	 *
	 * @param str
	 * @return
	 */

	public static boolean validateUppercase(String str) {
		String regex = "[\\p{Upper}]{2,}";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}



	/**
	 * Checks String with length 10 and numeric
	 * @param str
	 * @return boolean
	 *
	 */

	public static boolean validateAdminPassword(String str){
		boolean valid = false;
		String regex = "^[\\p{Digit}]{10}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher passwordMatcher = pattern.matcher(str);
		if(passwordMatcher.matches()){
			valid = true;
		}
		return valid;
	}

	/**
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkValidMpin(String str) {
		String temp = str.trim();
		if (temp.length() == 4) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks String with length equal to 4
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean checkLength4(String str) {

		String temp = str.trim();
		if (temp.length() == 4) {
			return true;
		} else {
			return false;
		}

	}
	
	

	/**
	 * Checks whether a String is alphanumeric
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */
	public static boolean isAlphanumeric(String str) {
		String temp = str.trim();
		if (temp.matches("[A-Za-z0-9]+")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isValidNumber(String str) {
		String temp = str.trim();
		if (temp.matches("^(?=(?:[6-9]){1})(?=[0-9]{10}).*")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether the string is completely numeric
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isNumeric(String str) {
		String temp = str.trim();
		boolean isNumber = false;
		if (temp.matches("[0-9]+")) {
			isNumber = true;
		}
		return isNumber;
	}

	public static boolean isValidLoadMoneyTransaction(String str) {
		int amount = Integer.parseInt(str);
		if ((amount >= 1) && (amount <= 10000)) {
			return true;
		}
		return false;
	}

	/**
	 * checks the length of string is equal to 10
	 * 
	 * @param str
	 * @return
	 */
	public static boolean checkLength10(String str) {
		String temp = str.trim();
		int length = temp.length();
		boolean isValid = false;
		if (length == 10) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * for email validation
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isLoginEmail(String str) {
		String regexOfMail = "^[\\w]{2,}@[\\p{Alpha}]{3,}\\.[\\p{Alpha}]{3}$";
		Pattern mailPattern = Pattern.compile(regexOfMail);
		Matcher mailMatcher = mailPattern.matcher(str);
		return mailMatcher.matches();
	}

	/**
	 * @param password
	 * @return boolean
	 */

	public static boolean isSuperAdminPassword(String password){
		String regex = "[\\p{Digit}]{10}";
		Pattern passwordPattern = Pattern.compile(regex);
		Matcher passwordMatcher = passwordPattern.matcher(password);
		return passwordMatcher.matches();
	}

	/**
	 * @param otp
	 * @return boolean
	 */

	public static boolean isValidOTP(String otp){
		String regex = "[\\p{Digit}]{5,6}";
		Pattern otpPattern = Pattern.compile(regex);
		Matcher otpMatcher = otpPattern.matcher(otp);
		return otpMatcher.matches();
	}

	/**
	 * checks the number must be greater than 10
	 * 
	 * @param number
	 * @return boolean
	 */

	public static boolean isGreaterthan10(double number) {
		if (number >= 10)
			return true;
		else
			return false;
	}

	/**
	 * checks the validity of email
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */

	public static boolean isValidMail(String str) {
		boolean isValid = false;
		if (str.contains("@") || str.contains(".")) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * checks whether String is null or not
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */
	public static boolean isNull(String str) {
		if (str == null || str.isEmpty() || str == "") {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNullImage(MultipartFile str) {
		if (str == null || str.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether String contains only alphabets
	 * 
	 * @param str
	 * @return boolean
	 * 
	 */

	public static boolean containsAlphabets(String str) {
		if (str.matches("[A-Za-z]+")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * checks whether String contains valid image extension
	 * 
	 * @param str
	 * @return boolean
	 * 
	 * 
	 */
	public static boolean isValidImageExtension(String str) {
		String temp = str.trim();
		if (temp.contains(".jpg") || temp.contains(".png") || temp.contains(".tiff") || temp.contains(".gif"))
			return true;
		else
			return false;
	}

	/**
	 * Checks transaction amount is less than or equal to user balance
	 * 
	 * @param userBalance
	 * @param transactionAmount
	 * @return
	 */
	public static boolean balanceCheck(double userBalance, double transactionAmount) {
		if (transactionAmount <= userBalance) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if user daily transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param dailyTransactionLimit
	 * @param totalDailyTransaction
	 * @param transactionAmount
	 * @return
	 */
	public static boolean dailyLimitCheck(double dailyTransactionLimit, double totalDailyTransaction,
			double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean dailyLimitCheckBank(double dailyTransactionLimitBank, double totalDailyTransactionBank,
			double transactionAmount) {
		if (dailyTransactionLimitBank >= (totalDailyTransactionBank + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user daily debit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param dailyTransactionLimit
	 * @param totalDailyDebitTransactionAmount
	 * @return
	 */
	public static boolean dailyDebitLimitCheck(double dailyTransactionLimit, 
			double totalDailyDebitTransactionAmount, double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyDebitTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user daily credit transaction limit is to mark or not if the
	 * transaction is carried out
	 * @param transactionAmount
	 * @param dailyTransactionLimit
	 * @param totalDailyCreditTransactionAmount
	 * @return
	 */
	public static boolean dailyCreditLimitCheck(double dailyTransactionLimit, 
			double totalDailyCreditTransactionAmount, double transactionAmount) {
		if (dailyTransactionLimit >= (totalDailyCreditTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if user monthly transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyTransaction
	 * @param transactionAmount
	 * @return
	 */
	public static boolean monthlyLimitCheck(double monthlyTransactionLimit, double totalMonthlyTransaction,
			double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean monthlyLimitCheckBank(double monthlyTransactionLimitBank, double totalMonthlyTransactionBank,
			double transactionAmount) {
		if (monthlyTransactionLimitBank >= (totalMonthlyTransactionBank + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Checks if user monthly debit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyDebitTransactionAmount
	 * @return
	 */
	public static boolean monthlyDebitLimitCheck(double monthlyTransactionLimit, 
			double totalMonthlyDebitTransactionAmount, double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyDebitTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if user monthly credit transaction limit is to mark or not if the
	 * transaction is carried out
	 * 
	 * @param monthlyTransactionLimit
	 * @param totalMonthlyCreditTransactionAmount
	 * @return
	 */
	public static boolean monthlyCreditLimitCheck(double monthlyTransactionLimit, 
			double totalMonthlyCreditTransactionAmount, double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyCreditTransactionAmount + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean monthlyLoadMoneyLimitCheck(double monthlyTransactionLimit, double totalMonthlyTransaction,
			double transactionAmount) {
		if (monthlyTransactionLimit >= (totalMonthlyTransaction + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if receiver balance limit is to mark or not if the transaction is
	 * carried out
	 * 
	 * @param balanceLimit
	 * @param receiverBalance
	 * @param transactionAmount
	 * @return
	 */
	public static boolean receiverBalanceLimit(double balanceLimit, double receiverBalance, double transactionAmount) {
		if (balanceLimit >= (receiverBalance + transactionAmount)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isAmountInMinMaxRange(double minAmount, double maxAmount, String strAmount) {
		boolean valid = true;
		try {
			double amount = Double.parseDouble(strAmount);
			if (amount >= minAmount && amount <= maxAmount) {
				System.err.println("inside the amount ::" + amount);
				valid = true;
			} else {
				valid = false;
			}
		} catch (Exception e) {
			valid = false;
		}
		return valid;
	}

	public ErrorResponse checkKycRequest(UpgradeAccountDTO dto) {
		ErrorResponse err=new ErrorResponse();
		boolean valid = true;
		
		if (dto.getAddress1() == null || dto.getAddress1().isEmpty()) {
			err.setMessage("Address1 Required");
			valid = false;
		}
		
		if (dto.getAddres2() == null || dto.getAddres2().isEmpty()) {
			err.setMessage("Address2 Required");
			valid = false;
		}
		if (dto.getCity() == null || dto.getCity().isEmpty()) {
			err.setMessage("City Required");
			valid = false;
		}
		if (dto.getState() == null || dto.getState().isEmpty()) {
			err.setMessage("State Required");
			valid = false;
		}
		if (dto.getPincode() == null || dto.getPincode().isEmpty()) {
			err.setMessage("Pincode Required");
			valid = false;
		}
		if (dto.getCountry() == null || dto.getCountry().isEmpty()) {
			err.setMessage("Country Required");
			valid = false;
		}
		if (dto.getPincode() == null || dto.getPincode().isEmpty()) {
			err.setMessage("Pincode Required");
			valid = false;
		}
		if (dto.getIdType() == null || dto.getIdType().isEmpty()) {
			err.setMessage("Id Type Required");
			valid = false;
		}
		if (dto.getIdNumber() == null || dto.getIdNumber().isEmpty()) {
			err.setMessage("Id Number Required");
			valid = false;
		}
		if (dto.getImage() == null || dto.getImage().isEmpty()) {
			err.setMessage("Upload Image");
			valid = false;
		}
		err.setValid(valid);
		return err;
	}

	public CommonError checkAddPromo(PromoCodeDTO dto) {
		CommonError err = new CommonError();
		boolean valid=true;
		if ((!"".equalsIgnoreCase(dto.getPercentageValue())) && (!"".equalsIgnoreCase(dto.getAmount()))) {
			err.setMessage("Enter either Amount or amount");
			valid = false;
		}	
		if (dto.getPromoCode() == null || dto.getPromoCode().isEmpty()) {
			err.setMessage("Enter promo code");
			valid = false;
		}	
		if (dto.getEndDate() == null || dto.getEndDate().isEmpty()) {
			err.setMessage("Enter end date");
			valid = false;
		}	
		if (dto.getMaxCashBack() == null || dto.getMaxCashBack().isEmpty()) {
			err.setMessage("Enter max cash back");
			valid = false;
		}	
		if (dto.getMinTrxAmount() == null || dto.getMinTrxAmount().isEmpty()) {
			err.setMessage("Enter min trx amount");
			valid = false;
		}	
		if (("".equalsIgnoreCase(dto.getPercentageValue())) && ("".equalsIgnoreCase(dto.getAmount()))) {
			err.setMessage("Enter either Amount or amount");
			valid = false;
		}	
		if (dto.getStartDate() == null || dto.getStartDate().isEmpty()) {
			err.setMessage("Enter min trx amount");
			valid = false;
		}
		if (dto.getServiceCode() == null || dto.getServiceCode().isEmpty()) {
			err.setMessage("Please select service");
			valid = false;
		}
		err.setValid(valid);
		return err;
	}
	
	public CommonError checkAgentPrefundError(AgentPrefundDTO dto) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (dto.getAmount() <=  0.0) {
			valid = false;
			error.setMessage("Amount should be more than 0");
		}
	
		error.setValid(valid);
		return error;
	}
	
	public CommonError checkKitNo(AssignPhysicalCardAgentDTO dto) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (dto.getKitNo().length() != 12) {
			valid = false;
			error.setMessage("Please enter your 12 digit kit no.");
		}
	
		error.setValid(valid);
		return error;
	}
	
	public CommonError changePassword(ChangePasswordRequest dto) {
		CommonError error = new CommonError();
		boolean valid = true;
		if(dto.getPassword() == null && dto.getPassword().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your current password");
		}
		if(dto.getNewPassword() == null && dto.getNewPassword().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your new password");
		}
		if(dto.getConfirmPassword() == null && dto.getConfirmPassword().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your confirm password");
		}
		if(!dto.getNewPassword().equalsIgnoreCase(dto.getConfirmPassword())) {
			valid = false;
			error.setMessage("New password and confirm password does not match");
		}
		if (dto.getPassword().equalsIgnoreCase(dto.getNewPassword())) {
			valid = false;
			error.setMessage("Current password and new password can't be same.");
		}
		
		error.setValid(valid);
		return error;
	}
	
	public CommonError forgetPasswordRequest(ForgotPasswordDTO dto) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (dto.getUsername() == null && dto.getUsername().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your email id.");
		}
		error.setValid(valid);
		return error;
	}
	
	public CommonError forgetPasswordRequestWithOTP(ChangePasswordRequest dto) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (dto.getKey() == null && dto.getKey().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your OTP.");
		}
		
		if (dto.getNewPassword() == null && dto.getNewPassword().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your new password.");
		}
		if (dto.getConfirmPassword() == null && dto.getConfirmPassword().isEmpty()) {
			valid = false;
			error.setMessage("Please enter your confirm password.");
		}
		
		if (!dto.getNewPassword().equalsIgnoreCase(dto.getConfirmPassword())) {
			valid = false;
			error.setMessage("New password and confirm password does not match.");
		}
		error.setValid(valid);
		return error;
	}
	
	public static boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9]+(?:\\."+
							"[a-zA-Z0-9_+&*-]+)*@" +
							"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
							"A-Z]{2,7}$";
							
		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}
	
	public CommonError getAllAvailableTripsValidation(BusAvailableTripListRequest request) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (request.getSourceId() == null || request.getSourceId().isEmpty()) {
			error.setMessage("Please enter source");
			valid = false;
		} else if (request.getDestinationId() == null || request.getDestinationId().isEmpty()) {
			error.setMessage("Please enter destination");
			valid = false;
		} else if (request.getDate() == null || request.getDate().isEmpty()) {
			error.setMessage("Please enter date");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public CommonError getTransactionIdVal(TransactionIdDTO request)
	{
		CommonError error=new CommonError();
		boolean valid=true;
		if (request.getMobileNo()==null || request.getMobileNo().isEmpty()) {
			error.setMessage("Please enter mobile no");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getEmailId()==null || request.getMobileNo().isEmpty())&& valid==true) {
			error.setMessage("Please enter email Id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getAddress()==null || request.getAddress().isEmpty()) && valid==true) {
			error.setMessage("Please enter address");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getEngineId()==null || request.getEngineId().isEmpty()) && valid==true) {
			error.setMessage("Please enter engine id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBusid()==null || request.getBusid().isEmpty()) && valid==true) {
			error.setMessage("Please enter bus id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBusType()==null || request.getBusType().isEmpty()) && valid==true) {
			error.setMessage("Please enter bus type");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardId()==null || request.getBoardId().isEmpty()) && valid==true) {
			error.setMessage("Please enter board id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardName()==null || request.getBoardName().isEmpty()) && valid==true) {
			error.setMessage("Please enter board name");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getArrTime()==null || request.getArrTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter arrival time");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDepTime()==null || request.getDepTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter departure time");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getTravelName()==null || request.getTravelName().isEmpty()) && valid==true) {
			error.setMessage("Please enter travel name");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSource()==null || request.getSource().isEmpty()) && valid==true) {
			error.setMessage("Please enter source");		
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDestination()==null || request.getDestination().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSourceid()==null || request.getSourceid().isEmpty()) && valid==true) {
			error.setMessage("Please enter source id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDestinationId()==null || request.getDestinationId().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination id");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getJourneyDate()==null || request.getJourneyDate().isEmpty()) && valid==true) {
			error.setMessage("Please enter journey date");		
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardpoint()==null || request.getBoardpoint().isEmpty()) && valid==true) {
			error.setMessage("Please enter board point");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}if ((request.getBoardprime()==null || request.getBoardprime().isEmpty()) && valid==true) {
			error.setMessage("Please enter board prime");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardTime()==null || request.getBoardTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter board time");			
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropId()==null || request.getDropId().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop id");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropName()==null || request.getDropName().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop name");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropprime()==null || request.getDropprime().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop prime");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropTime()==null || request.getDropTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop time");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getRouteId()==null || request.getRouteId().isEmpty()) && valid==true) {
			error.setMessage("Please enter route id");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSeatDetail()==null || request.getSeatDetail().isEmpty()) && valid==true) {
			error.setMessage("Please enter seat detail");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getwLCode()==null || request.getwLCode().isEmpty()) && valid==true) {
			error.setMessage("Please enter wl code");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getIpAddress()==null || request.getIpAddress().isEmpty()) && valid==true) {
			error.setMessage("Please enter ip address");
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}

		if (valid) {
			if (request.getTravellers()!=null|| !request.getTravellers().isEmpty()) {
			for (int i = 0; i < request.getTravellers().size(); i++) {
				TravellerDetailsDTO tdDTO=request.getTravellers().get(i);

				if ((tdDTO.getTitle()==null || tdDTO.getTitle().isEmpty()) && valid==true) {
					error.setMessage("Please enter title");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getfName()==null || tdDTO.getfName().isEmpty()) && valid==true) {
					error.setMessage("Please enter first name");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getlName()==null || tdDTO.getlName().isEmpty()) && valid==true) {
					error.setMessage("Please enter last name");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getAge()==null || tdDTO.getAge().isEmpty()) && valid==true) {
					error.setMessage("Please enter age");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getGender()==null || tdDTO.getGender().isEmpty()) && valid==true) {
					error.setMessage("Please enter gender");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getSeatNo()==null || tdDTO.getSeatNo().isEmpty()) && valid==true) {
					error.setMessage("Please enter seat no");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getSeatType()==null || tdDTO.getSeatType().isEmpty()) && valid==true) {
					error.setMessage("Please enter seat type");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getFare()==null || tdDTO.getFare().isEmpty()) && valid==true) {
					error.setMessage("Please enter fare");
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				tdDTO=null;
			}
		}
			}
		
		if(valid) {			
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}	
	
	public CommonError busSeatDetailsValidation(BusSeatDetailRequest request) {
		CommonError error = new CommonError();
		boolean valid = true;
		if (request.getBusId() == null || request.getBusId().isEmpty()) {
			error.setMessage("Please enter Bus Id");
			valid = false;
		} else if (request.getEngineId() <= 0 && valid == true) {
			error.setMessage("Please enter Engine Id");
			valid = false;
		} else if (request.getJourneyDate() == null || request.getJourneyDate().isEmpty()) {
			error.setMessage("Please enter Journey Date");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public CommonError checkTravelValidation(MService service) {
		CommonError error = new CommonError();
		boolean valid = true;

		if (service == null || Status.Inactive.equals(service.getStatus())) {
			error.setMessage("Service is down for maintenance");
			valid = false;
		}

		error.setValid(valid);
		return error;
	}
	
	public CommonError checkBookTicketValidation(BusBookTicketRequest request) {
		CommonError error = new CommonError();
		boolean valid = true;

		if (request.getTransactionId() == null || request.getTransactionId().isEmpty()
				|| "null".equalsIgnoreCase(request.getTransactionId())) {
			error.setMessage("Please enter transaction id");
			valid = false;
		} else if (request.getAmount() == 0) {
			error.setMessage("Please enter valid amount");
			valid = false;
		} else if (request.getSeatHoldId() == null || request.getSeatHoldId().isEmpty()
				|| "null".equalsIgnoreCase(request.getSeatHoldId())) {
			error.setMessage("Please enter seat hold id");
			valid = false;
		}

		error.setValid(valid);
		return error;
	}


}
