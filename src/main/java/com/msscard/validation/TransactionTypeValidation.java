package com.msscard.validation;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.msscard.app.api.IMatchMoveApi;
import com.msscard.entity.MCommission;
import com.msscard.entity.MMCards;
import com.msscard.entity.MService;
import com.msscard.entity.MUser;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.entity.PhysicalCardDetails;
import com.msscard.model.ResponseStatus;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.TransactionValidationError;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MTransactionRepository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.PhysicalCardDetailRepository;

public class TransactionTypeValidation {

	private final PhysicalCardDetailRepository physicalCardDetailRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mmCardRepository;
	private final MTransactionRepository mTransactionRepository;
	private final IMatchMoveApi matchMoveApi;
	private final MCommissionRepository mCommissionRepository;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public TransactionTypeValidation(PhysicalCardDetailRepository physicalCardDetailRepository,
			MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository mmCardRepository,
			MTransactionRepository mTransactionRepository, IMatchMoveApi matchMoveApi, MCommissionRepository mCommissionRepository) {
		super();
		this.physicalCardDetailRepository = physicalCardDetailRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mmCardRepository = mmCardRepository;
		this.mTransactionRepository=mTransactionRepository;
		this.matchMoveApi = matchMoveApi;
		this.mCommissionRepository = mCommissionRepository;
	}
	
	public TransactionValidationError validateTransactionType(MUser user){
		
		TransactionValidationError validationError=new TransactionValidationError();
		boolean valid=true;
		MatchMoveWallet wallet=matchMoveWalletRepository.findByUser(user);
		MMCards cards=mmCardRepository.getPhysicalCardByUser(user);
		PhysicalCardDetails phyDetails=null;

		if(wallet!=null){
			valid=false;
			validationError.setValid(valid);
			validationError.setCode(ResponseStatus.WALLET_EXISTS);
			phyDetails=physicalCardDetailRepository.findByWallet(wallet);
			if(phyDetails!=null){
				valid=false;
				validationError.setValid(valid);
				validationError.setCode(ResponseStatus.PHYSICAL_CARD_REQUEST_EXIST);
			}
		}
		else if(cards!=null){
				valid=false;
				validationError.setValid(valid);
				validationError.setCode(ResponseStatus.PHYSICAL_CARD_EXIST);
			}
		
		else{
		validationError.setCode(ResponseStatus.SUCCESS);
		validationError.setValid(valid);
			}
		return validationError;
	}
	
	public TransactionValidationError checkPaymentForBus(MUser user, double txnAmount) {
		TransactionValidationError validationError=new TransactionValidationError();
		boolean valid=true;
		double balance = matchMoveApi.getBalanceApp(user);
		if (!CommonValidation.balanceCheck(balance, txnAmount)) {
			validationError.setMessage("You have insufficient balance. Please load money and continue.");
			valid = false;
			validationError.setValid(valid);
			validationError.setCode(ResponseStatus.INSUFFICIENT_FUNDS);			
			return validationError;
		}
		Date date = mTransactionRepository.getLastTranasactionTimeStampBus(user.getAccountDetail(), true);
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				validationError.setMessage("Please wait for current transaction to complete");
				validationError.setValid(false);
				validationError.setCode(ResponseStatus.FAILURE);
				return validationError;
			}
		}
		
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				validationError.setMessage("Please wait");
				validationError.setValid(false);
				validationError.setCode(ResponseStatus.FAILURE);
				return validationError;
			}
		}		
		validationError.setValid(valid);
		return validationError;
	}
	
	public CommonError validateCommissionCharges(MService service, CommonError error, double currentBal,
			double transactionAmount) {
		boolean valid = true;
		MCommission commission = findCommissionByService(service);
		double netCommissionValue = getCommissionValue(commission, transactionAmount);
		transactionAmount = transactionAmount + netCommissionValue;
		if (!CommonValidation.balanceCheck(currentBal, transactionAmount)) {
			error.setMessage("Insufficient Balance. Your current balance is Rs. " + currentBal);
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
		
	public MCommission findCommissionByService(MService service) {
		
		MCommission commission = mCommissionRepository.findCommissionByService(service);
		if (commission == null) {
			commission = new MCommission();
			commission.setMinAmount(100);
			commission.setMaxAmount(2000000);			
			commission.setValue(0.5);
			commission.setFixed(false);
			commission.setService(service);
		}
		return commission;
	}
	
	public double getCommissionValue(MCommission senderCommission, double amount) {
		double netCommissionValue = 0;
		if (senderCommission.isFixed()) {
			netCommissionValue = senderCommission.getValue();
		} else {
			netCommissionValue = (senderCommission.getValue() * amount) / 100;
		}
		String v = String.format("%.2f", netCommissionValue);

		netCommissionValue = Double.parseDouble(v);
		return netCommissionValue;
	}

	/*public ErrorResponse validateLoadMoneyTransaction(String amount, String username, MServiceType service) {

		ErrorResponse error = new ErrorResponse();
		boolean valid = true;
		MUser senderUser = userApi.findByUserName(username);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double balanceLimit = senderUser.getAccountDetail().getAccountType().getBalanceLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalMonthlyTransaction = userApi.monthlyLoadMoneyTransactionTotal(senderUser);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = mTransactionRepository.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
//			userApi.blockUser(username);
			error.setCode("F02");
			return error;
		}

		if (totalMonthlyTransaction < 0) {
			error.setMessage("Oops!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs. " + monthlyTransactionLimit
					+ " and you've already credited total of Rs. " + totalCreditMonthly);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.receiverBalanceLimit(balanceLimit, currentWalletBalance, transactionAmount)) {
			error.setMessage(
					"Balance Limit Exceeded. " + senderUser.getUsername() + " balance limit is Rs. " + balanceLimit);
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = mTransactionRepository.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		System.err.println(" DATE:::::::::::" + date);
		if (date != null) {
			Date currentDate = new Date();
			System.err.println("CURRENT DATE:::::::::::::::::::" + currentDate);
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				// userApi.blockUser(senderUsername);
				error.setValid(false);
				return error;
			}
		}

		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}

		Date reverseTransactionDate = mTransactionRepository.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 mins");
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	
	}*/
}
