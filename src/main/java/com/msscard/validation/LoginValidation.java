package com.msscard.validation;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.msscard.app.model.request.LoginDTO;
import com.msscard.app.model.request.SessionDTO;
import com.msscard.entity.MUser;
import com.msscard.model.error.LoginError;

public class LoginValidation {

	@SuppressWarnings("unused")
	private final PasswordEncoder passwordEncoder;

	public LoginValidation(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public LoginError checkLoginValidation(LoginDTO login) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getUsername())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Username");
		} else if (CommonValidation.isNull(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Password");
		} else if (CommonValidation.isNull(login.getIpAddress())) {
			loginError.setMessage("Not a valid device");
			loginError.setSuccess(false);
		}
		return loginError;
	}
	
	public LoginError checkMpinLoginValidation(LoginDTO login) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getmPin())) {
			loginError.setSuccess(false);
			loginError.setMessage("Enter Mpin");
		}else{
			loginError.setSuccess(true);
		}
		
		return loginError;
	}

	public LoginError checkLogoutValidation(SessionDTO dto) {
		LoginError error = new LoginError();
		error.setSuccess(true);
		if (CommonValidation.isNull(dto.getSessionId())) {
			error.setSuccess(false);
			error.setMessage("Enter Session ID");
		}
		return error;
	}

	public LoginError checkSuperAdminValidation(LoginDTO login, MUser user) {
		LoginError loginError = new LoginError();
		loginError.setSuccess(true);
		if (CommonValidation.isNull(login.getUsername())) {
			loginError.setMessage("Please Enter Email");
			loginError.setSuccess(false);
		} else if (!CommonValidation.isLoginEmail(login.getUsername())) {
			loginError.setSuccess(false);
			loginError.setMessage("Please enter valid mail");
		}
		if (CommonValidation.isNull(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Please Enter Password");
		} else if (!CommonValidation.isSuperAdminPassword(login.getPassword())) {
			loginError.setSuccess(false);
			loginError.setMessage("Password must be 10 digits long");
		}
		return loginError;
	}
}
