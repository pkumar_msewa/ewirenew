package com.msscard.validation;

import java.util.List;

import com.msscard.app.api.ITransactionApi;
import com.msscard.entity.BulkLoadMoney;
import com.msscard.entity.MMCards;
import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MUser;
import com.msscard.model.error.BulkLoadMoneyError;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserRespository;

public class BulkLoadMoneyValidation {

	private final MUserRespository userRespository;
	private final MMCardRepository cardRepository;
	private final ITransactionApi transactionApi;
	public BulkLoadMoneyValidation(MUserRespository userRespository, MMCardRepository cardRepository,ITransactionApi transactionApi) {
		super();
		this.userRespository = userRespository;
		this.cardRepository = cardRepository;
		this.transactionApi=transactionApi;
	}
	
	public BulkLoadMoneyError validateBulkLoad(BulkLoadMoney bulkLoad){
		
		boolean isValid=true;
		BulkLoadMoneyError bulkErr=new BulkLoadMoneyError();
		
		MUser user=userRespository.findByUsername(bulkLoad.getMobile());
		if(user==null){
			isValid=false;
			bulkErr.setValid(isValid);
			bulkErr.setMessage("UserDoesn'tExist");
			return bulkErr;
		}else if(user!=null && user.getMobileStatus().getValue().equalsIgnoreCase("Inactive")){
			isValid=false;
			bulkErr.setValid(isValid);
			bulkErr.setMessage("UserInactive");
			return bulkErr;
		}
		if(user!=null){
		List<MMCards> cards=cardRepository.findCardByUserId(user);
		if(cards==null){
			isValid=false;
			bulkErr.setValid(isValid);
			bulkErr.setMessage("NoCardsFound");
			return bulkErr;
		}/*else if(cards!=null){
			for (MMCards mmCards : cards) {
				if(mmCards.isHasPhysicalCard() && mmCards.isBlocked() && mmCards.getStatus().equalsIgnoreCase("Inactive")){
					isValid=false;
					bulkErr.setValid(isValid);
					bulkErr.setMessage("PhysicalcardInactiveStatus");
					return bulkErr;
				}
				if(mmCards.isHasPhysicalCard()==false){
					isValid=false;
					bulkErr.setValid(isValid);
					bulkErr.setMessage("NoPhysicalCardFound");
					return bulkErr;
				}
			}
		}*/
	}
	
		if(user!=null){
		MPQAccountDetails corpAccount=bulkLoad.getCorporate().getAccountDetail();
		double balance=corpAccount.getBalance();
		double amt=Double.parseDouble(bulkLoad.getAmount());
		if(amt>balance){
			isValid=false;
			bulkErr.setValid(false);
			bulkErr.setMessage("Prefunding Balance low");
			return bulkErr;
		}
		}
		
		double transactionAmount=Double.parseDouble(bulkLoad.getAmount());
		double monthlyTransactionLimit = user.getAccountDetail().getAccountType().getMonthlyLimit();
		double dailyTransactionLimit = user.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(user.getAccountDetail());
		double totalDailyCredit=transactionApi.getDailyCreditTransationTotalAmount(user.getAccountDetail());
		

		 if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			 bulkErr.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			isValid = false;
			bulkErr.setValid(isValid);
			return bulkErr;
		}
		 
		 if (!CommonValidation.dailyCreditLimitCheck(dailyTransactionLimit, totalDailyCredit,
					transactionAmount)) {
			 bulkErr.setMessage("Daily Credit Limit Exceeded. Your Daily limit is Rs." + dailyTransactionLimit
						+ " and you've already credited total of Rs." + totalDailyCredit);
			 isValid = false;
			 bulkErr.setValid(isValid);
				return bulkErr;
			}

		
		return bulkErr;
}

}
