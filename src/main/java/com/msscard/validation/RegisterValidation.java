package com.msscard.validation;

import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cashiercards.paramerterization.DataConfig;
import com.msscard.entity.MMCards;
import com.msscard.entity.MatchMoveWallet;
import com.msscard.app.api.IMatchMoveApi;
import com.msscard.app.api.IUserApi;
import com.msscard.app.model.request.ChangePasswordRequest;
import com.msscard.app.model.request.MatchMoveCreateCardRequest;
import com.msscard.app.model.request.ResponseDTO;
import com.msscard.app.model.response.ForgotPasswordError;
import com.msscard.entity.GroupBulkKyc;
import com.msscard.entity.GroupFileCatalogue;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.entity.MerchantDetails;
import com.msscard.model.AddMerchantDTO;
import com.msscard.model.RegisterDTO;
import com.msscard.model.ResponseStatus;
import com.msscard.model.Status;
import com.msscard.model.error.CommonError;
import com.msscard.model.error.RegisterError;
import com.msscard.repositories.DataConfigRepository;
import com.msscard.repositories.GroupBulkKycRepository;
import com.msscard.repositories.MMCardRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.repositories.MatchMoveWalletRepository;
import com.msscard.repositories.MerchantDetailsRepository;
import com.msscard.util.CommonUtil;
import com.msscard.util.SecurityUtil;

public class RegisterValidation {

	private final MUserRespository mUserRespository;
	private final PasswordEncoder passwordEncoder;
	private final MUserDetailRepository mUserDetailRepository;
	private final IUserApi userApiImpl;
	private final IMatchMoveApi matchMoveApi;
	private final MerchantDetailsRepository merchantDetailsRepository;
	private final DataConfigRepository dataConfigRepository;
	private final GroupBulkKycRepository groupBulkKycRepository;
	private final MatchMoveWalletRepository matchMoveWalletRepository;
	private final MMCardRepository mMCardsRepository;

	public RegisterValidation(MUserRespository mUserRespository, PasswordEncoder passwordEncoder,
			MUserDetailRepository mUserDetailRepository, IUserApi userApiImpl, IMatchMoveApi matchMoveApi, 
			MerchantDetailsRepository merchantDetailsRepository, DataConfigRepository dataConfigRepository, 
			GroupBulkKycRepository groupBulkKycRepository, MatchMoveWalletRepository matchMoveWalletRepository, MMCardRepository mMCardsRepository) {
		super();
		this.mUserRespository = mUserRespository;
		this.passwordEncoder = passwordEncoder;
		this.mUserDetailRepository = mUserDetailRepository;
		this.userApiImpl = userApiImpl;
		this.matchMoveApi = matchMoveApi;
		this.merchantDetailsRepository = merchantDetailsRepository;
		this.dataConfigRepository = dataConfigRepository;
		this.groupBulkKycRepository = groupBulkKycRepository;
		this.matchMoveWalletRepository = matchMoveWalletRepository;
		this.mMCardsRepository = mMCardsRepository;
	}

	public RegisterError validateNormalUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (dto.isHasAadhar()) {
			if (!CommonValidation.isNull(dto.getAadharNo())) {
			} else {
				error.setMessage("aadhar no is mandatory");
				valid = false;
			}
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			error.setMessage("Password Required");
			valid = false;
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (MUserDetails ud : userDetail) {
						MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
							error.setMessage("Email already exists");
						}
					}
				}
			}

			MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
				cardRequest.setUsername(dto.getContactNo());
				ResponseDTO responseDTO = matchMoveApi.fetchMMUser(cardRequest);
				if (responseDTO.getCode().equalsIgnoreCase("S00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage("Please use a different email id");
				} else if (responseDTO.getCode().equalsIgnoreCase("E01")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				} else if (responseDTO.getCode().equalsIgnoreCase("E00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
				valid = false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				return error;
			}
		}
		DataConfig dataConfig = dataConfigRepository.findDatas();

		if (dataConfig != null && !dataConfig.isAllowRegistration()) {
			valid = false;
			error.setValid(valid);
			error.setMessage("Services are temporarily down..Inconvenience is regretted");
			return error;
		}
		error.setValid(valid);
		return error;
	}

	public ForgotPasswordError forgotPassword(String username) {
		ForgotPasswordError error = new ForgotPasswordError();
		if (!CommonValidation.checkLength10(username)) {
			error.setErrorLength("Mobile number must be 10 digits long");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;

	}

	public ForgotPasswordError changePassword(ChangePasswordRequest dto) {

		ForgotPasswordError error = new ForgotPasswordError();

		MUser user = userApiImpl.findByUserName(dto.getUsername());
		String hashedPassword = user.getPassword();
		String pass = CommonUtil.base64Decode(dto.getPassword());
		String newPass = CommonUtil.base64Decode(dto.getNewPassword());
		String confirmPass = CommonUtil.base64Decode(dto.getConfirmPassword());

		System.err.println("password:: " + dto.getPassword());
		System.err.println("new pass:: " + dto.getNewPassword());
		System.err.println("confirmpass:: " + dto.getConfirmPassword());

		System.err.println("hashedPassword:: " + hashedPassword);
		System.err.println("pass:: " + pass);
		System.err.println("newPasss: " + newPass);
		System.err.println("confirmPass: " + confirmPass);
		if (!(passwordEncoder.matches(pass, hashedPassword))) {
			error.setMessage("You have entered wrong current password");
			error.setValid(false);
			return error;
		}

		if (dto.getPassword() == null) {
			error.setMessage("Please enter the Password");
			error.setValid(false);
			return error;
		}
		if (pass.equalsIgnoreCase(newPass)) {
			error.setMessage("Please use a different password other than previous one ");
			error.setValid(false);
			return error;
		}
		if (dto.getNewPassword() == null) {
			error.setMessage("Please enter the New Password");
			error.setValid(false);
			return error;
		}
		if (dto.getConfirmPassword() == null) {
			error.setMessage("Please enter the Confirm Password");
			error.setValid(false);
			return error;
		}
		if (!newPass.equalsIgnoreCase(confirmPass)) {
			error.setMessage("Password Missmatch");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;
	}

	public ForgotPasswordError forgotPassword(ChangePasswordRequest dto) {
		ForgotPasswordError error = new ForgotPasswordError();
		String pass = CommonUtil.base64Decode(dto.getPassword());
		String confirmPass = CommonUtil.base64Decode(dto.getConfirmPassword());
		if (dto.getPassword() == null) {
			error.setMessage("Please enter the Password");
			error.setValid(false);
			return error;
		}
		if (dto.getKey() == null) {
			error.setMessage("Please Enter the OTP");
			error.setValid(false);
			return error;
		}
		if (dto.getConfirmPassword() == null) {
			error.setMessage("Please enter the Confirm Password");
			error.setValid(false);
			return error;
		}
		if (!pass.equalsIgnoreCase(confirmPass)) {
			error.setMessage("Password Missmatch");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;
	}

	public RegisterError customRegisterError(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			error.setHasError(true);
			valid = false;
		} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				error.setMessage("Mobile number must be 10 digits long");
				error.setHasError(true);
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			error.setMessage("Please enter valid mobile number");
			error.setHasError(true);
			valid = false;
		}
		MUser user = mUserRespository.findByUsernameAndStatus(dto.getContactNo(), Status.Active);
		if (user != null) {
			error.setUsername("MobileNo Already Exist");
			error.setContactNo("MobileNo Already Exist");
			error.setMessage("MobileNo Already Exist");
			error.setFullyFilled(user.isFullyFilled());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

	public RegisterError validateFullDetails(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getDateOfBirth().isEmpty() || dto.getDateOfBirth() == null) {
			valid = false;
			error.setMessage("please enter date of birth");
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			error.setMessage("Password Required");
			valid = false;
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (!(userDetail == null || userDetail.isEmpty())) {
					valid = false;
					error.setMessage("Email id exists use a different one");
					error.setEmail("Email id exists use a different one");
				}
			}
			MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));

			} catch (Exception e) {
				e.printStackTrace();
				valid = false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}

	public RegisterError validateEmail(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (!(userDetail == null || userDetail.isEmpty())) {
					valid = false;
					error.setMessage("Email id exists use a different one");
					error.setEmail("Email id exists use a different one");
				}
			}
			MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
				cardRequest.setUsername(dto.getContactNo());
				ResponseDTO responseDTO = matchMoveApi.fetchMMUser(cardRequest);
				if (responseDTO.getCode().equalsIgnoreCase("S00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage("Please use a different email id");
				} else if (responseDTO.getCode().equalsIgnoreCase("E01")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				} else if (responseDTO.getCode().equalsIgnoreCase("E00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				}

			} catch (Exception e) {
				e.printStackTrace();
				valid = false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				return error;
			}
		}

		error.setValid(valid);
		return error;
	}

	public static void main(String[] args) {
		String jsonString = "{\r\n    \"status\": \"Success\",\r\n    \"code\": \"S00\",\r\n    \"message\": \"Login successful.\",\r\n    \"balance\": 0,\r\n    \"details\": {\r\n        \"accountDetail\": {\r\n            \"id\": 24,\r\n            \"created\": 1519452440000,\r\n            \"lastModified\": 1519452440000,\r\n            \"balance\": 0,\r\n            \"accountNumber\": 999900910000029,\r\n            \"points\": 0,\r\n            \"accountType\": {\r\n                \"id\": 3,\r\n                \"created\": 1519048495000,\r\n                \"lastModified\": 1519048495000,\r\n                \"name\": \"Non - KYC\",\r\n                \"description\": \"Unverified Account\",\r\n                \"code\": \"NONKYC\",\r\n                \"monthlyLimit\": 20000,\r\n                \"dailyLimit\": 20000,\r\n                \"balanceLimit\": 20000,\r\n                \"transactionLimit\": 5,\r\n                \"new\": false\r\n            },\r\n            \"branchCode\": null,\r\n            \"new\": false\r\n        },\r\n        \"userDetail\": {\r\n            \"sessionId\": null,\r\n            \"username\": \"8178003068\",\r\n            \"userId\": \"29\",\r\n            \"firstName\": \"kanchan\",\r\n            \"middleName\": \"\",\r\n            \"lastName\": \"bishst\",\r\n            \"address\": \"\",\r\n            \"contactNo\": \"8178003068\",\r\n            \"userType\": \"User\",\r\n            \"authority\": \"ROLE_USER,ROLE_AUTHENTICATED\",\r\n            \"emailStatus\": \"Active\",\r\n            \"mobileStatus\": \"Active\",\r\n            \"email\": \"kanchan.bishst@gmail.com\",\r\n            \"image\": \"\",\r\n            \"mpin\": \"\",\r\n            \"mpinPresent\": false,\r\n            \"dateOfBirth\": \"1992-02-02\",\r\n            \"gcmId\": \"\",\r\n            \"pinCode\": \"560095\",\r\n            \"circleName\": \"Karnataka\",\r\n            \"districtName\": \"Bangalore\",\r\n            \"locality\": \"Kormangala\",\r\n            \"encodedImage\": \"\",\r\n            \"hasMpin\": false,\r\n            \"deviceLocked\": false\r\n        },\r\n        \"sessionId\": \"4B8A548C34D06C8A54C78C3DF7509B6E\"\r\n    },\r\n    \"txnId\": null,\r\n    \"error\": null,\r\n    \"valid\": false,\r\n    \"info\": null,\r\n    \"hasRefer\": false,\r\n    \"userList\": null,\r\n    \"hasVirtualCard\": true,\r\n    \"hasPhysicalCard\": false,\r\n    \"cardDetails\": {\r\n        \"status\": null,\r\n        \"code\": \"S00\",\r\n        \"message\": \"card successfully assigned...\",\r\n        \"details\": null,\r\n        \"expiryDate\": \"2023-02\",\r\n        \"issueDate\": \"2018-02-24\",\r\n        \"walletNumber\": \"5386290707500161\",\r\n        \"walletId\": null,\r\n        \"withholdingAmt\": null,\r\n        \"availableAmt\": null,\r\n        \"card_status\": false,\r\n        \"mmUserId\": null,\r\n        \"cardId\": \"038c6a1f538cdb56d678e3c0be4c4f1b\",\r\n        \"cvv\": \"359\",\r\n        \"holderName\": \"kanchanbishst\",\r\n        \"cardCount\": 0,\r\n        \"countLimit\": false\r\n    }\r\n}";
		try {
			JSONObject jobject = new JSONObject(jsonString);
			System.err.println(jobject);
			String s = jobject.getString("message");
			System.err.println(s);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public RegisterError validateCorporateAgent(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getCorporateName() == null || dto.getCorporateName().isEmpty()) {
			error.setUsername("Corporate name Required");
			error.setMessage("Corporate name Required");
			valid = false;
		}
		MUser userC = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
		if (userC != null) {
			error.setUsername("Corporate Already Exist with this Email_id");
			error.setContactNo("User Already Exist");
			error.setMessage("Corporate Already Exist with this Email_id");
			valid = false;
		}
		if (!CommonValidation.isNullImage(dto.getCorporateLogo1())) {
		} else {
			error.setMessage("Please upload Corporate Logo");
			valid = false;
		}
		if (CommonValidation.isNullImage(dto.getCorporateLogo2())) {
			error.setMessage("Please upload Corporate Logo");
			valid = false;
		}

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			valid = false;
		} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				error.setMessage("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			error.setMessage("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (MUserDetails ud : userDetail) {
						MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getEmail(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
							error.setMessage("Email already exists");
						}
					}
				}
			}
		}

		error.setValid(valid);
		return error;
	}

	public RegisterError validateCorporateUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}
		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			error.setMessage("Password Required");
			valid = false;
		}

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			valid = false;
		}
		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (MUserDetails ud : userDetail) {
						MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
							error.setMessage("Email already exists");
						}
					}
				}
			}

			MatchMoveCreateCardRequest cardRequest = new MatchMoveCreateCardRequest();
			cardRequest.setEmail(dto.getEmail());
			try {
				cardRequest.setPassword(SecurityUtil.md5(dto.getContactNo()));
				cardRequest.setUsername(dto.getContactNo());
				ResponseDTO responseDTO = matchMoveApi.fetchMMUser(cardRequest);
				if (responseDTO.getCode().equalsIgnoreCase("S00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage("Please use a different email id");
				} else if (responseDTO.getCode().equalsIgnoreCase("E01")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				} else if (responseDTO.getCode().equalsIgnoreCase("E00")) {
					valid = false;
					error.setValid(valid);
					error.setMessage(responseDTO.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
				valid = false;
				error.setValid(valid);
				error.setMessage("Services are temporarily down..Please try after sometime");
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}

	/**
	 * RECHECK VALIDATION
	 */

	public RegisterError validateCorporateUserRecheck(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		}
		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			error.setMessage("Password Required");
			valid = false;
		}
		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			valid = false;
		}
		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	/**
	 * AGENT VALIDATION
	 */

	public RegisterError validateAgent(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (!CommonValidation.isNullImage(dto.getAadharImage1())) {
		} else {
			error.setMessage("Please upload your Aadhar card front side");
			valid = false;
		}

		if (!CommonValidation.isNullImage(dto.getAadharImage2())) {
		} else {
			error.setMessage("Please upload your Aadhar card back side");
			valid = false;
		}

		if (!CommonValidation.isNullImage(dto.getPanCardImage())) {

		} else {
			error.setMessage("Please upload your Pan card");
			valid = false;
		}

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			valid = false;
		} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				error.setMessage("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			error.setMessage("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			error.setFirstName("First Name Required");
			error.setMessage("First Name Required");
			valid = false;
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValid(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (MUserDetails ud : userDetail) {
						MUser user = mUserRespository.findByUsernameAndMobileStatusAndEmailStatus(ud.getEmail(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
							error.setMessage("Email already exists");
						}
					}
				}
			}
		}
		error.setValid(valid);
		return error;
	}

	public RegisterError validateUser(RegisterDTO dto) {
		RegisterError registerError = new RegisterError();
		boolean valid = true;
		if (dto.getFirstName() == null && dto.getFirstName().isEmpty()) {
			registerError.setMessage("First name Required");
			valid = false;
		} else {
			MUser user = mUserRespository.findByUsernameAndStatus((dto.getContactNo()), Status.Active);
			if (user != null) {
				registerError.setUsername("Username Already Exist");
				registerError.setContactNo("User Already Exist");
				registerError.setMessage("User Already Exist");
				valid = false;
			}
		}
		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			registerError.setContactNo("Contact No Required");
			registerError.setMessage("Contact No Required");
			valid = false;
		} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				registerError.setContactNo("Mobile number must be 10 digits long");
				registerError.setMessage("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			registerError.setContactNo("Please enter valid mobile number");
			registerError.setMessage("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			registerError.setEmail("Email Required");
			registerError.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			registerError.setEmail("Please enter valid mail address");
			registerError.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			registerError.setEmail("Please enter valid email address.");
			registerError.setMessage("Please enter valid email address.");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<MUserDetails> userDetail = mUserDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null && !userDetail.isEmpty()) {
					valid = false;
					registerError.setEmail("Email already exists");
					registerError.setMessage("Email already exists");
				}
			}
		}
		registerError.setValid(valid);
		return registerError;
	}

	/**
	 * ADD MERCHANT
	 */

	public RegisterError validateMerchant(AddMerchantDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getBusinessCorrespondentName() == null || dto.getBusinessCorrespondentName().isEmpty()) {
			error.setUsername("BC name Required");
			error.setMessage("BC name Required");
			valid = false;
		}

		if (dto.getMerchantName() != null) {
			MerchantDetails merchantDetails = merchantDetailsRepository
					.findByMerchantName(dto.getMerchantName().trim());
			if (merchantDetails != null) {
				valid = false;
				error.setMessage("Merchant with this name already exists");
				error.setValid(valid);
				return error;
			}

		} else {
			valid = false;
			error.setValid(valid);
			error.setMessage("Please enter Merchant Name");
		}
		if (dto.getBusinessCorrespondentNumber() == null || dto.getBusinessCorrespondentNumber().isEmpty()) {
			error.setContactNo("Contact No Required");
			error.setMessage("Contact No Required");
			valid = false;
		} else if (CommonValidation.isValidNumber(dto.getBusinessCorrespondentNumber())) {
			if (!CommonValidation.checkLength10(dto.getBusinessCorrespondentNumber())) {
				error.setContactNo("Mobile number must be 10 digits long");
				error.setMessage("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			error.setMessage("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getBusinessCorrespondentEmail() == null || dto.getBusinessCorrespondentEmail().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getBusinessCorrespondentEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else if (dto.getBusinessCorrespondentEmail().contains("@yopmail.com")
				|| dto.getBusinessCorrespondentEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
		} else {
		}
		error.setValid(valid);
		return error;
	}

		public ResponseDTO addKycData(RegisterDTO dto, GroupFileCatalogue catalogue) {
			ResponseDTO result = new ResponseDTO();
			try {
				Date d1 = CommonUtil.DATEFORMATTER.parse(dto.getDateOfBirth());
				GroupBulkKyc gbr = groupBulkKycRepository.getUserRowByContact(dto.getContactNo(), catalogue, true,
						Status.Success);
				if (gbr != null) {
					gbr = new GroupBulkKyc();
					if (dto.getAddress() != null || dto.getAddress().isEmpty()) {
						gbr.setAddress1(dto.getAddress());
					}
					if (dto.getServices() != null || dto.getServices().isEmpty()) {
						gbr.setAddress2(dto.getServices());
					}
					if (dto.getContactNo() != null || dto.getContactNo().isEmpty()) {
						gbr.setMobile(dto.getContactNo());
					}
					if (dto.getDateOfBirth() != null || dto.getDateOfBirth().isEmpty()) {
						gbr.setDob(d1);
					}
					if (dto.getEmail() != null || dto.getEmail().isEmpty()) {
						gbr.setEmail(dto.getEmail());
					}
					if (dto.getIdType() != null || dto.getIdType().isEmpty()) {
						gbr.setIdType(dto.getIdType());
					}
					if (dto.getIdNo() != null || dto.getIdNo().isEmpty()) {
						gbr.setIdNumber(dto.getIdNo());
					}
					if (dto.getFileName() != null || dto.getFileName().isEmpty()) {
						gbr.setPinCode(dto.getFileName());
					}
					if (dto.getAadharImagePath1() != null || dto.getAadharImagePath1().isEmpty()) {
						gbr.setDocumentUrl(dto.getAadharImagePath1());
					}
					if (dto.getAadharImagePath2() != null || dto.getAadharImagePath2().isEmpty()) {
						gbr.setDocumentUrl1(dto.getAadharImagePath2());
					}
					gbr.setCronStatus(true);
					gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
					gbr.setGroupFileCatalogue(catalogue);
					gbr.setSuccessStatus(false);
					gbr.setFailReason("Duplicate KYC Data");
					gbr.setStatus(Status.Failed);
					groupBulkKycRepository.save(gbr);
					result.setCode(ResponseStatus.FAILURE.getValue());
				} else {
					gbr = new GroupBulkKyc();
					if (dto.getAddress() != null || dto.getAddress().isEmpty()) {
						gbr.setAddress1(dto.getAddress());
					}
					if (dto.getServices() != null || dto.getServices().isEmpty()) {
						gbr.setAddress2(dto.getServices());
					}
					if (dto.getContactNo() != null || dto.getContactNo().isEmpty()) {
						gbr.setMobile(dto.getContactNo());
					}
					if (dto.getDateOfBirth() != null || dto.getDateOfBirth().isEmpty()) {
						gbr.setDob(d1);
					}
					if (dto.getEmail() != null || dto.getEmail().isEmpty()) {
						gbr.setEmail(dto.getEmail());
					}
					if (dto.getIdType() != null || dto.getIdType().isEmpty()) {
						gbr.setIdType(dto.getIdType());
					}
					if (dto.getIdNo() != null || dto.getIdNo().isEmpty()) {
						gbr.setIdNumber(dto.getIdNo());
					}
					if (dto.getFileName() != null || dto.getFileName().isEmpty()) {
						gbr.setPinCode(dto.getFileName());
					}
					if (dto.getAadharImagePath1() != null || dto.getAadharImagePath1().isEmpty()) {
						gbr.setDocumentUrl(dto.getAadharImagePath1());
					}
					if (dto.getAadharImagePath2() != null || dto.getAadharImagePath2().isEmpty()) {
						gbr.setDocumentUrl1(dto.getAadharImagePath2());
					}
					gbr.setCronStatus(true);
					gbr.setGroupDetail(catalogue.getUser().getGroupDetails());
					gbr.setGroupFileCatalogue(catalogue);
					gbr.setStatus(Status.Initiated);
					groupBulkKycRepository.save(gbr);
					result.setCode(ResponseStatus.SUCCESS.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		public RegisterError validateKyc(RegisterDTO dto, GroupFileCatalogue fileCatalogue, boolean status) {
		RegisterError error = new RegisterError();
		boolean valid = true;

		GroupBulkKyc gbr = groupBulkKycRepository.getUserRowByContact(dto.getContactNo(), fileCatalogue, status,
				Status.Initiated);

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			valid = false;
			error.setMessage("Contact No Required");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);

		} else if (CommonValidation.isValidNumber(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				error.setMessage("Mobile number must be 10 digits long");
				valid = false;
				error.setValid(valid);

				gbr.setFailReason(error.getMessage());
				gbr.setSuccessStatus(false);
				gbr.setStatus(Status.Failed);
				groupBulkKycRepository.save(gbr);
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			error.setMessage("Please enter valid mobile number");
			valid = false;
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			valid = false;
			error.setValid(valid);
			error.setEmail("email cannot be empty");
			error.setMessage("email cannot be empty");

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		} else if (dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")) {
			error.setEmail("Please enter valid email address.");
			error.setMessage("Please enter valid email address.");
			valid = false;
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		} /*
			 * else { if (!CommonValidation.isNull(dto.getEmail())) { List<MUserDetails>
			 * userDetail = mUserDetailRepository.checkMail(dto.getEmail()); if (userDetail
			 * != null && !userDetail.isEmpty()) { valid = false;
			 * error.setEmail("Email already exists");
			 * error.setMessage("Email already exists"); error.setValid(valid);
			 * 
			 * gbr.setFailReason(error.getMessage()); gbr.setSuccessStatus(false);
			 * gbr.setStatus(Status.Failed); groupBulkKycRepository.save(gbr); } } }
			 */

		if (dto.getDateOfBirth() == null || dto.getDateOfBirth().isEmpty()) {
			valid = false;
			error.setMessage("date of birth cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getAddress() == null || dto.getAddress().isEmpty()) {
			valid = false;
			error.setMessage("Address1 cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getServices() == null || dto.getServices().isEmpty()) {
			valid = false;
			error.setMessage("Address2 cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getIdType() == null || dto.getIdType().isEmpty()) {
			valid = false;
			error.setMessage("IdType cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getIdNo() == null || dto.getIdNo().isEmpty()) {
			valid = false;
			error.setMessage("IdNo cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getFileName() == null || dto.getFileName().isEmpty()) {
			valid = false;
			error.setMessage("Pincode cannot be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getAadharImagePath1() == null || dto.getAadharImagePath1().isEmpty()) {
			valid = false;
			error.setMessage("Id proof 1 should not be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}

		if (dto.getAadharImagePath2() == null || dto.getAadharImagePath2().isEmpty()) {
			valid = false;
			error.setMessage("Id proof 2 should not be empty");
			error.setValid(valid);

			gbr.setFailReason(error.getMessage());
			gbr.setSuccessStatus(false);
			gbr.setStatus(Status.Failed);
			groupBulkKycRepository.save(gbr);
		}
		error.setValid(valid);
		return error;
	}

	public CommonError validateCardCreation(String mobile) {

		boolean valid = true;
		CommonError error = new CommonError();
		MUser user = mUserRespository.findByUsername(mobile);

		if (user == null) {
			valid = false;
			error.setValid(valid);
			error.setMessage("User doesn't exist");
			return error;
		} else {
			MatchMoveWallet wallet = matchMoveWalletRepository.findByUser(user);
			if (wallet == null) {
				valid = false;
				error.setValid(valid);
				error.setMessage("Wallet is not created for this user");
				return error;
			} else {
				MMCards card = mMCardsRepository.getPhysicalCardByUser(user);
				if (card != null) {
					valid = false;
					error.setValid(valid);
					error.setMessage("Physical already assigned to this user");
					return error;
				}
			}
		}
		error.setValid(valid);
		return error;
	}
		
	}
