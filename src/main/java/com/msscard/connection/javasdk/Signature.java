package com.msscard.connection.javasdk;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

public class Signature {
	public static final String ALGO = "AES/CBC/PKCS5Padding";
	public static final String ALGO_KEY = "AES";
	public static final String HASH = "HmacSHA1";
	public static final String METHOD = "HMAC-SHA1";
	
	public static final String ALGO_KEY_HASH = "MD5";
	public static final String ALGO_PROVIDER = "BC";
	
	private String signature = "";
	
	public String getString() {
		return this.signature;
	}
	
	public String getURIEncoded()
			throws UnsupportedEncodingException {
		return MapUtil.URLRFC2986(this.signature);
	}
	
	public Signature (String signature, String method, String host, Map <String, String> payload)
			throws UnsupportedEncodingException,
				InvalidKeyException,
				NoSuchAlgorithmException,
				NoSuchProviderException {
		
		Map<String, String> sortedData = new TreeMap<String, String>(payload);
		
		String rawSignature = method + "&"
			+ MapUtil.URLRFC2986(host) + "&"
			+ MapUtil.URLRFC2986(MapUtil.mapToString(sortedData));
		
		 this.signature = Signature.hash(rawSignature, signature);
	}
	
	public static String hash(String data, String signature)
			throws NoSuchAlgorithmException,
				NoSuchProviderException,
				InvalidKeyException,
				UnsupportedEncodingException {
		
		String encoding = "UTF-8";
		
	    Mac mac = Mac.getInstance(Signature.HASH);

	    SecretKeySpec keySpec = new SecretKeySpec(signature.getBytes(encoding), Signature.HASH);
	    mac.init(keySpec);
	    byte[] result = mac.doFinal(data.getBytes(encoding));
	    
	    return Signature.getBase64(result);
	}
	
	public static String getBase64(byte[] data) {
		return new String(Base64.encode(data)).trim();
	}
	
	public static String md5String(byte[] data) {
        BigInteger hash = new BigInteger(1, data);
        String result = hash.toString(16);
        while(result.length() < 32) { //40 for SHA-1
            result = "0" + result;
        }
        return result;
	}
	
	public static byte[] arrayConcat(byte[] a1, byte[] a2) {
		int a1Size = a1.length;
		int a2Size = a2.length;
		byte[] result = new byte[a1Size + a2Size];
		System.arraycopy(a1, 0, result, 0, a1Size);
		System.arraycopy(a2, 0, result, a1Size, a2Size);
		
		return result;
	}
	
	public String encrypt(String secret, String data)
			throws NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				NoSuchPaddingException,
				InvalidKeyException,
				IllegalBlockSizeException,
				BadPaddingException {

		String encoding = "UTF-8";
		String signature = this.getString() + secret;
		
		MessageDigest hash = MessageDigest.getInstance(Signature.ALGO_KEY_HASH);
		SecretKeySpec keySpec = new SecretKeySpec(hash.digest(signature.getBytes(encoding)), Signature.ALGO_KEY);
		Cipher cipher = Cipher.getInstance(Signature.ALGO, Signature.ALGO_PROVIDER);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] result = cipher.doFinal(data.getBytes(encoding));
		
		result = Signature.arrayConcat(cipher.getIV(), result);
		
		return Signature.getBase64(result);
	}
}
