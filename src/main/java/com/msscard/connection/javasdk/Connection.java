package com.msscard.connection.javasdk;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import org.json.JSONException;
import org.json.JSONObject;

import com.msscard.connection.javasdk.HttpRequest.HttpRequestException;
import com.msscard.util.MatchMoveUtil;



public class Connection {

	private String host = MatchMoveUtil.MATCHMOVE_HOST_URL;
	private String key = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY;
	private String secret = MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
	private AccessToken accessToken = null;
	protected AccessSession session = null;
	
	public final static int OK = 200;
	
	/**
	 * Constructor to connect using SSL
	 * 
	 * @param host                  Complete connection host (protocol://server/product/version)
	 * @param key                   OAuth consumer key
	 * @param secret                OAuth consumer secret
	 * @param ssl_certificate_path  Path to the SSL certificate
	 */
	public Connection(String host, String key, String secret, String ssl_certificate_path) {
		this.initialize(host, key, secret);
	}

	/**
	 * Constructor to connect without SSL certificate
	 *
	 * @param host     Complete connection host (protocol://server/product/version)
	 * @param key      OAuth consumer key
	 * @param secret   OAuth consumer secret
	 */
	public Connection(String host, String key, String secret) {
		this.initialize(host, key, secret);
	}
	
	/**
	 * Initialize connection parameters
	 * 
	 * @param host     Complete connection host (protocol://server/product/version)
	 * @param key      OAuth consumer key
	 * @param secret   OAuth consumer secret
	 */
	private void initialize(String host, String key, String secret) {
		this.host   = host;
		this.key    = key;
		this.secret = secret;
		this.accessToken = new AccessToken(host, key, secret);
	}
	
	/**
	 * Authenticates the user's credentials to access secure information
	 * 
	 * @param user      user identification (email)
	 * @param password  user password
	 * @return Boolean  true for success otherwise, false.
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws URISyntaxException 
	 * @throws NoSuchProviderException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws ShortBufferException 
	 * @throws InvalidKeySpecException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws ResourceException 
	 * @throws HttpRequestException 
	 * @throws UnsupportedMethodException 
	 * @throws JSONException 
	 */
	public void authenticate(String user, String password)
			throws
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				URISyntaxException,
				InvalidKeyException,
				NoSuchProviderException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException,
				ShortBufferException,
				InvalidKeySpecException,
				InvalidAlgorithmParameterException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException, JSONException {
		
		if (null != this.session) {
			this.accessToken = new AccessToken(this.session, user);
		}
		
		if (null == this.accessToken.getResponse()) {
			RequestToken request = new RequestToken(this.host, this.key, this.secret, user, password);
			

			this.accessToken = new AccessToken(request);
			
			if (null != this.session) {
				this.session.set(user, this.accessToken.getResponse().toString());
			}
		}

	}
	
	public boolean authenticate(String user)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException, JSONException {
		if (null != this.session) {
			this.accessToken = new AccessToken(this.session, user);
		}
		
		return null != this.accessToken.getResponse();
	}
	
	public static String hash(byte[] data)
			throws NoSuchAlgorithmException {
		MessageDigest hash = MessageDigest.getInstance("MD5");
		// Change this to "UTF-16" if needed
		hash.update(data);
		byte[] digest = hash.digest();
		
		StringBuffer buffer = new StringBuffer();
		
		for (byte b : digest) {
			buffer.append(String.format("%02x", b & 0xff));
		}
		
		return buffer.toString();
	}
	
	public void setSession(ConnectionSession session) {
		this.session = new AccessSession(this.host, this.key, this.secret, session);
	}
	
	public JSONObject consume(String resource)
			throws InvalidKeyException,
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		return this.consume(resource, HttpRequest.METHOD_GET, null);
	}
	
	public JSONObject consume(String resource, String method)
			throws InvalidKeyException,
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		return this.consume(resource, method, null);
	}
	
	public JSONObject consume(String resource, String method, Map<String, String> data)
			throws InvalidKeyException,
				NoSuchAlgorithmException,
				UnsupportedEncodingException,
				NoSuchProviderException,
				JSONException,
				HttpRequestException,
				ResourceException,
				UnsupportedMethodException,
				NoSuchPaddingException,
				IllegalBlockSizeException,
				BadPaddingException {
		return this.accessToken.consume(resource, method, data);
	}
	
	public static JSONObject request(String method, String uri, Map<String, String> data) 
			throws UnsupportedMethodException,
				HttpRequestException,
				ResourceException, JSONException {
		HttpRequest request = null;
		
		if (method == HttpRequest.METHOD_GET) {
			request = data == null 
					? HttpRequest.get(uri)
					: HttpRequest.get(uri + "?" + MapUtil.mapToString(data));
		} else if (method == HttpRequest.METHOD_POST) {
			request = HttpRequest.post(uri);
			
			if (data != null) {
				request.send(MapUtil.mapToString(data));
			}
		} else if (method == HttpRequest.METHOD_PUT) {
			request = data == null 
					? HttpRequest.put(uri)
					: HttpRequest.put(uri + "?" + MapUtil.mapToString(data));
		} else if (method == HttpRequest.METHOD_DELETE) {
			request = data == null 
					? HttpRequest.delete(uri)
					: HttpRequest.delete(uri + "?" + MapUtil.mapToString(data));
		} else {
			throw new UnsupportedMethodException();
		}
		
		if (Connection.OK != request.code()) {
			return new JSONObject(request.body("UTF-8"));
		}
		
		return new JSONObject(request.body("UTF-8"));
	}
}
