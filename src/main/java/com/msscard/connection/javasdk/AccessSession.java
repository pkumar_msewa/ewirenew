package com.msscard.connection.javasdk;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AccessSession {
	
	private String host = "https://beta-api.mmvpay.com/indemo/v1";
	private String key = "UUbXdlrTzjlsPUdezEPvKtKGmKODz5nX";
	private String secret = "FRSxXp9S3e6atfVPed1iQ7iONHidkPNbU96wJBTnl1LcyfblN1xU29cK9LpT53rE";
	private ConnectionSession session = null;
	
	public AccessSession(String host, String key, String secret, ConnectionSession session) {
		this.host = host;
		this.key = key;
		this.secret = secret;
		this.session = session;
	}
	
	public String getHost() {
		return this.host;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public String getSecret() {
		return this.secret;
	}
	
	public ConnectionSession getSession() {
		return this.session;
	}
	
	public String getSessionKey(String user)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException {
		byte[] data = new String(this.getHost() + this.getKey() + user).getBytes("UTF-8"); 
		
		MessageDigest hash = MessageDigest.getInstance("MD5");
		// Change this to "UTF-16" if needed
		hash.update(data);
		byte[] digest = hash.digest();
		
		StringBuffer buffer = new StringBuffer();
		
		for (byte b : digest) {
			buffer.append(String.format("%02x", b & 0xff));
		}
		
		return buffer.toString();
	}
	
	public void set(String user, String value)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException {
		this.session.set(this.getSessionKey(user), value);
	}
	
	public String get(String user)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException {
		return this.session.get(this.getSessionKey(user));
	}
	
	public void unset(String user)
			throws UnsupportedEncodingException,
				NoSuchAlgorithmException {
		this.session.set(this.getSessionKey(user), null);
	}
}
