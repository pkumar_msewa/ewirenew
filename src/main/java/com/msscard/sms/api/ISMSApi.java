package com.msscard.sms.api;

import com.msscard.app.model.request.SMSRequest;
import com.msscard.app.model.response.SMSExecution;
import com.msscard.app.model.response.SMSResponse;

public interface ISMSApi {

	SMSResponse verifyDelivery(String messageId);

	SMSExecution sendSMS(SMSRequest dto);

}
