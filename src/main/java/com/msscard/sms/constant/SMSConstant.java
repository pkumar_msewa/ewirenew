package com.msscard.sms.constant;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;

import com.msscard.app.model.request.TransactionInitiateRequest;
import com.msscard.model.SetCustomPinDTO;
import com.msscard.util.AESEncryption;
import com.msscard.util.MatchMoveUtil;

public class SMSConstant {

	

		// public static final String URL =
		// "http://sms6.routesms.com:8443/bulksms/bulksms?"; //For FGM

		private static final String HOST = "http://api.infobip.com/";
		private static final String REST_API = "sms/";		
		private static final int MESSAGES = 1;
		public static String USERNAME = "MSEWAint";
		public static String PASSWORD = "Msewa@123";
		public static String SMS_URL = HOST + REST_API + MESSAGES + "/text/single";

		public static String FROM = "EEWIRE";
		public static String REPORT_URL = HOST + REST_API + MESSAGES + "/reports";
		
		public static String GETBALANCE = "https://api.mmvpay.com/inmss/v1/users/wallets/cards/";
		
		public static final String UPIKEY = "UPILOADE";

		public static String getAuthorizationHeader() {
			String result = "Basic ";
			String credentials = USERNAME + ":" + PASSWORD;
			String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
			result = result + encodedCredentials;
			return result;
		}
		
		public static String getBasicAuthorization() {
			String result = "Basic ";
			String credentials = MatchMoveUtil.MATCHMOVE_CONSUMER_KEY + ":" + MatchMoveUtil.MATCHMOVE_CONSUMER_SECRET;
			String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
			result = result + encodedCredentials;
			return result;
		}
		
		public static String decodeBase64String(String encodedString) throws IOException {       
			String decryptedData = null;
			try {
				decryptedData = AESEncryption.decrypt(encodedString);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return decryptedData;
	    }
		
		public static TransactionInitiateRequest prepareTopupRequest(String decdata) {
			System.err.println("decdata:: "+decdata);
			TransactionInitiateRequest request = new TransactionInitiateRequest();
	        try {
	            JSONObject obj = new JSONObject(decdata);
	            request.setSessionId(obj.getString("sessionId"));	          
	            request.setAmount(obj.getString("amount"));	                    
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return request;
	    }
		
		public static SetCustomPinDTO prepareCustomPinRequest(String decdata) {
			System.err.println("decdata:: "+decdata);
			SetCustomPinDTO request = new SetCustomPinDTO();
	        try {
	            JSONObject obj = new JSONObject(decdata);
	            request.setSessionId(obj.getString("sessionId"));	          
	            request.setDob(obj.getString("dob"));	
	            request.setPin(obj.getString("pin"));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return request;
	    }
		
		public static String getBasicAuthorizationUpi(String username, String password ) {
			String result = "Basic ";
			String credentials = username + ":" + password;
			String encodedCredentials = DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
			result = result + encodedCredentials;
			return result;
		}
}
