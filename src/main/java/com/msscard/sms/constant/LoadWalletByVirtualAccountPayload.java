package com.msscard.sms.constant;

public class LoadWalletByVirtualAccountPayload {

	private String id;

	private String amount;

	private String date_added;

	private String status;

	private String prefunding_type;

	private String product_code;

	private String agent_name;

	private String agent_code;

	private String virtual_account_number;

	private String transaction_comments;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDate_added() {
		return date_added;
	}

	public void setDate_added(String date_added) {
		this.date_added = date_added;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrefunding_type() {
		return prefunding_type;
	}

	public void setPrefunding_type(String prefunding_type) {
		this.prefunding_type = prefunding_type;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getVirtual_account_number() {
		return virtual_account_number;
	}

	public void setVirtual_account_number(String virtual_account_number) {
		this.virtual_account_number = virtual_account_number;
	}

	public String getTransaction_comments() {
		return transaction_comments;
	}

	public void setTransaction_comments(String transaction_comments) {
		this.transaction_comments = transaction_comments;
	}

	@Override
	public String toString() {
		return "LoadWalletByVirtualAccountPayload [id=" + id + ", amount=" + amount + ", date_added=" + date_added
				+ ", status=" + status + ", prefunding_type=" + prefunding_type + ", product_code=" + product_code
				+ ", agent_name=" + agent_name + ", agent_code=" + agent_code + ", virtual_account_number="
				+ virtual_account_number + ", transaction_comments=" + transaction_comments + "]";
	}

}