package com.msscard.startup;

import com.msscard.entity.MCommission;
import com.msscard.entity.MService;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MServiceRepository;

public class CommissionCreator {

	private final MServiceRepository serviceRepository;
	private final MCommissionRepository commissionRepository;
	public CommissionCreator(MServiceRepository serviceRepository, MCommissionRepository commissionRepository) {
		super();
		this.serviceRepository = serviceRepository;
		this.commissionRepository = commissionRepository;
	}
	
	public void create(){
		MService service=serviceRepository.findServiceByCode("LMS");
		MCommission comm=commissionRepository.findCommissionByService(service);
		if(comm==null){
		comm=new MCommission();
		comm.setFixed(false);
		comm.setMinAmount(0);
		comm.setMaxAmount(10000);
		comm.setValue(0);
		comm.setService(service);
		commissionRepository.save(comm);
	}
	}


}
