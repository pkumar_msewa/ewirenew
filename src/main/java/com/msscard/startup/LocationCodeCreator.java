package com.msscard.startup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.msscard.entity.MLocationDetails;
import com.msscard.repositories.MLocationDetailRepository;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class LocationCodeCreator {

	private final String URL="https://api.data.gov.in/resource/6176ee09-3d56-4a3b-8115-21841576b2f6";
	private final MLocationDetailRepository locationDetailRepository;
	
	
	public LocationCodeCreator(MLocationDetailRepository locationDetailRepository) {
		super();
		this.locationDetailRepository = locationDetailRepository;
	}


	public void create(){
		
		Client client=Client.create();
		WebResource webResource=client.resource(URL).queryParam("api-key", "579b464db66ec23bdd0000019fcf62dcac134b354e98cd9ebbb778ee").queryParam("format", "json").queryParam("offset", "40000").queryParam("limit", "50000");
		ClientResponse clientResponse=webResource.get(ClientResponse.class);
		String strResponse=clientResponse.getEntity(String.class);
		if(strResponse!=null){
			try {
				//System.err.println(strResponse);
				JSONObject pinCodeJson=new JSONObject(strResponse);
				JSONArray pinCodeJArray=pinCodeJson.getJSONArray("records");
				if(pinCodeJArray!=null){
					for (int i = 0; i < pinCodeJArray.length(); i++) {
						JSONObject jPinObject=pinCodeJArray.getJSONObject(i);
						if(jPinObject!=null){
							String locality=jPinObject.getString("officename");
							String pinCode=jPinObject.getString("pincode");
							String regionName=jPinObject.getString("regionname");
							String circleName=jPinObject.getString("circlename");
							String districtName=jPinObject.getString("districtname");
							String stateName=jPinObject.getString("statename");
							System.err.println(stateName);
							MLocationDetails locationDetails=new MLocationDetails();
							locationDetails.setLocality(locality);
							locationDetails.setPinCode(pinCode);
							locationDetails.setRegionName(regionName);
							locationDetails.setCircleName(circleName);
							locationDetails.setDistrictName(districtName);
							locationDetails.setStateName(stateName);
							MLocationDetails location=locationDetailRepository.findLocationByPin(pinCode);
							if(location==null){
							locationDetailRepository.save(locationDetails);
							}
						}

					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
	
}
