package com.msscard.startup;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.msscard.entity.MCommission;
import com.msscard.entity.MOperator;
import com.msscard.entity.MService;
import com.msscard.entity.MServiceType;
import com.msscard.model.Status;
import com.msscard.repositories.MCommissionRepository;
import com.msscard.repositories.MOperatorRepository;
import com.msscard.repositories.MServiceRepository;
import com.msscard.repositories.MServiceTypeRepository;
import com.msscard.util.StartUpUtil;
import com.razorpay.constants.MdexConstants;
import com.razorpay.constants.RazorPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class ServiceCreator {

	private final MServiceTypeRepository serviceTypeRepository;
	private final MOperatorRepository operatorRepository;
	private final MServiceRepository serviceRepository;
	private final MCommissionRepository commissionRepository;
	public ServiceCreator(MServiceTypeRepository serviceTypeRepository, MOperatorRepository operatorRepository,
			MServiceRepository serviceRepository,MCommissionRepository commissionRepository) {
		super();
		this.serviceTypeRepository = serviceTypeRepository;
		this.operatorRepository = operatorRepository;
		this.serviceRepository = serviceRepository;
		this.commissionRepository= commissionRepository;
	}
	
	
public void create() {
		
		MOperator WoohooOperator = operatorRepository.findOperatorByName("Load Money");
		if (WoohooOperator == null) {
			WoohooOperator = new MOperator();
			WoohooOperator.setName("Load Money");
			WoohooOperator.setStatus(Status.Active);
			operatorRepository.save(WoohooOperator);
		}
		
		MServiceType woohoGift = serviceTypeRepository.findServiceTypeByName("Load Money");
		if (woohoGift == null) {
			woohoGift = new MServiceType();
			woohoGift.setName("Load Money");
			woohoGift.setDescription("Load Money to card");
			woohoGift = serviceTypeRepository.save(woohoGift);
		}
		
		
		MService woohoo = serviceRepository.findServiceByCode(RazorPayConstants.SERVICE_CODE);
		 if (woohoo == null) {
			 woohoo = new MService();
			 woohoo.setName("Load Money");
			 woohoo.setDescription("Load money to card");
			 woohoo.setMinAmount(200);
			 woohoo.setMaxAmount(20000);
			 woohoo.setCode(RazorPayConstants.SERVICE_CODE);
			 woohoo.setOperator(WoohooOperator);
			 woohoo.setOperatorCode(RazorPayConstants.SERVICE_CODE);
			 woohoo.setStatus(Status.Active);
			 woohoo.setServiceType(woohoGift);
			 serviceRepository.save(woohoo);
			 System.err.println("service saved...."+woohoo.getCode());

				 }

		 
		 MOperator promoCode = operatorRepository.findOperatorByName("Promo Code");
			if (promoCode == null) {
				promoCode = new MOperator();
				promoCode.setName("Promo Code");
				promoCode.setStatus(Status.Active);
				operatorRepository.save(promoCode);
			}
			
			MServiceType promoService = serviceTypeRepository.findServiceTypeByName("Promo Service");
			if (promoService == null) {
				promoService = new MServiceType();
				promoService.setName("Promo Service");
				promoService.setDescription("Promo Services in Cashier");
				promoService = serviceTypeRepository.save(promoService);
			}
			
			
			MService promoServ = serviceRepository.findServiceByCode(RazorPayConstants.PROMO_SERVICES);
			 if (promoServ == null) {
				 promoServ = new MService();
				 promoServ.setName("Promo Services");
				 promoServ.setDescription("Promo Services for cashier");
				 promoServ.setMinAmount(10);
				 promoServ.setMaxAmount(500);
				 promoServ.setCode(RazorPayConstants.PROMO_SERVICES);
				 promoServ.setOperator(promoCode);
				 promoServ.setOperatorCode(RazorPayConstants.PROMO_SERVICES);
				 promoServ.setStatus(Status.Active);
				 promoServ.setServiceType(promoService);
				 serviceRepository.save(promoServ);

					 }

			 /**
			  * UPI SERVICE
			  * */
			 
			 MOperator upiOperator = operatorRepository.findOperatorByName("UPI");
				if (upiOperator == null) {
					upiOperator = new MOperator();
					upiOperator.setName("UPI");
					upiOperator.setStatus(Status.Active);
					operatorRepository.save(upiOperator);
				}
				
				MServiceType upiServiceType = serviceTypeRepository.findServiceTypeByName("UPI");
				if (upiServiceType == null) {
					upiServiceType = new MServiceType();
					upiServiceType.setName("UPI");
					upiServiceType.setDescription("UPI in Cashier");
					upiServiceType = serviceTypeRepository.save(upiServiceType);
				}
				
				
				MService upiService = serviceRepository.findServiceByCode(RazorPayConstants.UPI_SERVICE);
				 if (upiService == null) {
					 upiService = new MService();
					 upiService.setName("UPI");
					 upiService.setDescription("UPI for cashier");
					 upiService.setMinAmount(10);
					 upiService.setMaxAmount(500);
					 upiService.setCode(RazorPayConstants.UPI_SERVICE);
					 upiService.setOperator(upiOperator);
					 upiService.setOperatorCode(RazorPayConstants.UPI_SERVICE);
					 upiService.setStatus(Status.Active);
					 upiService.setServiceType(upiServiceType);
					 serviceRepository.save(upiService);
					 }
				 
			
				 /**
				  * SERVICE CREATOR FOR CORPORATE AGENT
				  * */
				 
				 MOperator corp_Operator = operatorRepository.findOperatorByName("CORP_AGENT");
					if (corp_Operator == null) {
						corp_Operator = new MOperator();
						corp_Operator.setName("CORP_AGENT");
						corp_Operator.setStatus(Status.Active);
						operatorRepository.save(corp_Operator);
					}
					
					MServiceType bulk = serviceTypeRepository.findServiceTypeByName("Bulk_Registration");
					if (bulk == null) {
						bulk = new MServiceType();
						bulk.setName("Bulk_Registration");
						bulk.setDescription("Bulk Registration in Cashier Corporate Agent");
						bulk = serviceTypeRepository.save(bulk);
					}
					
					
					MService bulkService = serviceRepository.findServiceByCode("BRC");
					 if (bulkService == null) {
						 bulkService = new MService();
						 bulkService.setName("BRC");
						 bulkService.setDescription("Bulk_Resgistration for Corporate Agent");
						 bulkService.setMinAmount(10);
						 bulkService.setMaxAmount(500);
						 bulkService.setCode("BRC");
						 bulkService.setOperator(corp_Operator);
						 bulkService.setOperatorCode("BRC");
						 bulkService.setStatus(Status.Active);
						 bulkService.setServiceType(bulk);
						 serviceRepository.save(bulkService);

						 }
		 
					 MServiceType bulkLoad = serviceTypeRepository.findServiceTypeByName("Bulk_Card_Load");
						if (bulkLoad == null) {
							bulkLoad = new MServiceType();
							bulkLoad.setName("Bulk_Card_Load");
							bulkLoad.setDescription("Bulk Card Load in Cashier Corporate Agent");
							bulkLoad = serviceTypeRepository.save(bulkLoad);
						}
						
						
						MService bulkCardLoadService = serviceRepository.findServiceByCode("BRCL");
						 if (bulkCardLoadService == null) {
							 bulkCardLoadService = new MService();
							 bulkCardLoadService.setName("BRCL");
							 bulkCardLoadService.setDescription("Bulk_Card_Load for Corporate Agent");
							 bulkCardLoadService.setMinAmount(10);
							 bulkCardLoadService.setMaxAmount(500);
							 bulkCardLoadService.setCode("BRCL");
							 bulkCardLoadService.setOperator(corp_Operator);
							 bulkCardLoadService.setOperatorCode("BRCL");
							 bulkCardLoadService.setStatus(Status.Active);
							 bulkCardLoadService.setServiceType(bulkLoad);
							 serviceRepository.save(bulkCardLoadService);

							 }
						 
						 
						 MServiceType singleCardAssignment = serviceTypeRepository.findServiceTypeByName("Single_Card_Issuance");
							if (singleCardAssignment == null) {
								singleCardAssignment = new MServiceType();
								singleCardAssignment.setName("Single_Card_Issuance");
								singleCardAssignment.setDescription("Single_Card_Issuance in Cashier Corporate Agent");
								singleCardAssignment = serviceTypeRepository.save(bulkLoad);
							}
							
							
							MService singleCardAssignmentService = serviceRepository.findServiceByCode("BRCS");
							 if (singleCardAssignmentService == null) {
								 singleCardAssignmentService = new MService();
								 singleCardAssignmentService.setName("BRCS");
								 singleCardAssignmentService.setDescription("Bulk_Card_Issuance_Service for Corporate Agent");
								 singleCardAssignmentService.setMinAmount(10);
								 singleCardAssignmentService.setMaxAmount(500);
								 singleCardAssignmentService.setCode("BRCS");
								 singleCardAssignmentService.setOperator(corp_Operator);
								 singleCardAssignmentService.setOperatorCode("BRCS");
								 singleCardAssignmentService.setStatus(Status.Active);
								 singleCardAssignmentService.setServiceType(singleCardAssignment);
								 serviceRepository.save(singleCardAssignmentService);

								 }
							 
							 MServiceType singleCardLoad = serviceTypeRepository.findServiceTypeByName("Single_Card_Load");
								if (singleCardLoad == null) {
									singleCardLoad = new MServiceType();
									singleCardLoad.setName("Single_Card_Load");
									singleCardLoad.setDescription("Single_Card_Load in Cashier Corporate Agent");
									singleCardLoad = serviceTypeRepository.save(singleCardLoad);
								}
								
								
								MService singleCardLoadServices = serviceRepository.findServiceByCode("BRCSL");
								 if (singleCardLoadServices == null) {
									 singleCardLoadServices = new MService();
									 singleCardLoadServices.setName("BRCSL");
									 singleCardLoadServices.setDescription("Single_Card_Load for Corporate Agent");
									 singleCardLoadServices.setMinAmount(10);
									 singleCardLoadServices.setMaxAmount(500);
									 singleCardLoadServices.setCode("BRCSL");
									 singleCardLoadServices.setOperator(corp_Operator);
									 singleCardLoadServices.setOperatorCode("BRCSL");
									 singleCardLoadServices.setStatus(Status.Active);
									 singleCardLoadServices.setServiceType(singleCardLoad);
									 serviceRepository.save(singleCardLoadServices);

									 }


								 MServiceType card_block_unblock_service_type = serviceTypeRepository.findServiceTypeByName("card_block_unblock");
									if (card_block_unblock_service_type == null) {
										card_block_unblock_service_type = new MServiceType();
										card_block_unblock_service_type.setName("card_block_unblock");
										card_block_unblock_service_type.setDescription("card_block_unblock in Cashier Corporate Agent");
										card_block_unblock_service_type = serviceTypeRepository.save(card_block_unblock_service_type);
									}
									
									
									MService card_block_unblock_service = serviceRepository.findServiceByCode("BRCBL");
									 if (card_block_unblock_service == null) {
										 card_block_unblock_service = new MService();
										 card_block_unblock_service.setName("BRCBL");
										 card_block_unblock_service.setDescription("card_block_unblock for Corporate Agent");
										 card_block_unblock_service.setMinAmount(10);
										 card_block_unblock_service.setMaxAmount(500);
										 card_block_unblock_service.setCode("BRCBL");
										 card_block_unblock_service.setOperator(corp_Operator);
										 card_block_unblock_service.setOperatorCode("BRCBL");
										 card_block_unblock_service.setStatus(Status.Active);
										 card_block_unblock_service.setServiceType(card_block_unblock_service_type);
										 serviceRepository.save(card_block_unblock_service);

										 }


									 //SINGLE CARD ASSIGNMENT
									 
									 
									 MServiceType singleCardAssign = serviceTypeRepository.findServiceTypeByName("Single_Card_Assignment");
										if (singleCardAssign == null) {
											singleCardAssign = new MServiceType();
											singleCardAssign.setName("Single_Card_Assignment");
											singleCardAssign.setDescription("Single_Card_Assignment in Cashier Corporate Agent");
											singleCardAssign = serviceTypeRepository.save(singleCardAssign);
										}
										
										
										MService singleCardAssignServices = serviceRepository.findServiceByCode("CORSA");
										 if (singleCardAssignServices == null) {
											 singleCardAssignServices = new MService();
											 singleCardAssignServices.setName("CORSA");
											 singleCardAssignServices.setDescription("Single_Card_Assignment");
											 singleCardAssignServices.setMinAmount(10);
											 singleCardAssignServices.setMaxAmount(500);
											 singleCardAssignServices.setCode("CORSA");
											 singleCardAssignServices.setOperator(corp_Operator);
											 singleCardAssignServices.setOperatorCode("CORSA");
											 singleCardAssignServices.setStatus(Status.Active);
											 singleCardAssignServices.setServiceType(singleCardAssign);
											 serviceRepository.save(singleCardAssignServices);

											 }

									 
									 
										// FOR RECHARGE  SERVICES 
												 MOperator mdex = operatorRepository.findOperatorByName("MDEX");
												  if (mdex == null) {
													mdex = new  MOperator();
													mdex.setName("MDEX");
													mdex.setStatus(Status.Active);
													operatorRepository.save(mdex);
												}
												  for(int j=0;j<8;j++){
												Client client = Client.create();
												client.addFilter(new LoggingFilter(System.out));
												WebResource webResource=null;
												if(j==0){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"pre");
												}
												if(j==1){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"post");

												}
												if(j==2){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"datacard");

												}
												if(j==3){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"dthbill");
												}
												if(j==4){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"elec");

												}
												if(j==5){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"gas");
												}
												if(j==6){
													webResource = client.resource(MdexConstants.MDEX_SERVICES+"insurance");

												}
												if(j==7){
													webResource=client.resource(MdexConstants.MDEX_SERVICES+"landline");
												}
												ClientResponse response = webResource.header("Authorization", "Basic YWJoaWppdDphYmhpMTIzNA==")
														.get(ClientResponse.class);
												String strResponse = response.getEntity(String.class);
												try {
													JSONObject obj = new JSONObject(strResponse);
													System.err.println(strResponse);
													JSONArray details = obj.getJSONArray("details");
													for (int i = 0; i < details.length(); i++) {
														JSONObject ser = details.getJSONObject(i);
														MServiceType ppfAccount = serviceTypeRepository.findServiceTypeByName(ser.getString("serviceType"));
														if (ppfAccount == null) {
															ppfAccount = new MServiceType();
															ppfAccount.setName(ser.getString("serviceType"));
															ppfAccount.setDescription(ser.getString("serviceType"));
															ppfAccount = serviceTypeRepository.save(ppfAccount);
														}

														MService housJoyService = serviceRepository.findServiceByCode(ser.getString("code"));
														if (housJoyService == null) {
															housJoyService = new MService();
															housJoyService.setName(ser.getString("name"));
															housJoyService.setDescription(ser.getString("description"));
															housJoyService.setMinAmount(Double.valueOf(ser.getString("minAmount")));
															housJoyService.setMaxAmount(Double.valueOf(ser.getString("maxAmount")));
															housJoyService.setCode(ser.getString("code"));
															housJoyService.setOperator(mdex);
															housJoyService.setOperatorCode(ser.getString("code"));
															housJoyService.setStatus(Status.Active);
															housJoyService.setServiceType(ppfAccount);
															housJoyService.setServiceImage(ser.getString("serviceLogo"));
															serviceRepository.save(housJoyService);
														}

													
														}
													
												} catch (JSONException e) {
												
													e.printStackTrace();
												}
												
											/*BUS SERVICES*/
												 MOperator busOp = operatorRepository.findOperatorByName("MDEX");
												  if (busOp == null) {
												   busOp = new MOperator();
												   busOp.setName("Bus");
												   busOp.setStatus(Status.Active);
												   operatorRepository.save(busOp);
												  }

												    
												  MServiceType busServiceType = serviceTypeRepository.findServiceTypeByName("BUS_CP");
												  if (busServiceType == null) {
												   busServiceType = new MServiceType();
												   busServiceType.setName("Bus_Cashiercard");
												   busServiceType.setDescription("Bus_CP payment in Cashiercard");
												   busServiceType = serviceTypeRepository.save(busServiceType);
												  }

												  MService busService= serviceRepository.findServiceByCode(StartUpUtil.BUS_SERVICECODE);
												  if (busService == null) {
												   busService = new MService();
												   busService.setName("Bus_Cashiercard");
												   busService.setDescription("Bus_CP Payment in Cashiercard");
												   busService.setMinAmount(100);
												   busService.setMaxAmount(20000);
												   busService.setCode(StartUpUtil.BUS_SERVICECODE);
												   busService.setOperator(busOp);
												   busService.setOperatorCode(StartUpUtil.BUS_SERVICECODE);
												   busService.setStatus(Status.Active);
												   busService.setServiceType(busServiceType);
												   serviceRepository.save(busService);
												  }
										
										  /*  END OF BUS SERVICES*/
												
												
}
			                               /*    Prefund service*/
       									 MServiceType prefundSerType = serviceTypeRepository.findServiceTypeByName("Corp_Prefund");
											if (prefundSerType == null) {
												prefundSerType = new MServiceType();
												prefundSerType.setName("Corp_Prefund");
												prefundSerType.setDescription("Corp_Prefund in Cashier");
												prefundSerType = serviceTypeRepository.save(prefundSerType);
											}
											
											
											MService corp_Prefund_Service = serviceRepository.findServiceByCode("PREFC");
											 if (corp_Prefund_Service == null) {
												 corp_Prefund_Service = new MService();
												 corp_Prefund_Service.setName("PREFC");
												 corp_Prefund_Service.setDescription("prefund in corporate");
												 corp_Prefund_Service.setMinAmount(10);
												 corp_Prefund_Service.setMaxAmount(50000);
												 corp_Prefund_Service.setCode("PREFC");
												 corp_Prefund_Service.setOperator(corp_Operator);
												 corp_Prefund_Service.setOperatorCode("PREFC");
												 corp_Prefund_Service.setStatus(Status.Active);
												 corp_Prefund_Service.setServiceType(prefundSerType);
												 serviceRepository.save(corp_Prefund_Service);

												 }
											 
											    
//										        IMPS
										        

											 MOperator imps = operatorRepository.findOperatorByName("Imps");
												if (imps == null) {
													imps = new MOperator();
													imps.setName("Imps");
													imps.setStatus(Status.Active);
													operatorRepository.save(imps);
												}
												
										        
												MServiceType serviceImps = serviceTypeRepository.findServiceTypeByName("IMPS FundTransfer");
												if (serviceImps == null) {
													serviceImps = new MServiceType();
													serviceImps.setName("IMPS FundTransfer");
													serviceImps.setDescription("IMPS P2A FundTransfer in Cashier Cards.");
													serviceImps = serviceTypeRepository.save(serviceImps);
												}
										        
										        MService impsService= serviceRepository.findServiceByCode(StartUpUtil.IMPS_SERVICECODE);
										        if(impsService==null){
										        	impsService = new MService();
										        	impsService.setName("IMPS FundTransfer");
										        	impsService.setDescription("IMPS P2A FundTransfer in Cashier Cards.");
										        	impsService.setMinAmount(1);
										        	impsService.setMaxAmount(10000000);
										        	impsService.setCode(StartUpUtil.IMPS_SERVICECODE);
										        	impsService.setOperator(imps);
										        	impsService.setOperatorCode(StartUpUtil.IMPS_SERVICECODE);
										        	impsService.setStatus(Status.Active);
										        	impsService.setServiceType(serviceImps);
										        	serviceRepository.save(impsService);
										        
											        MCommission impsCommision = new MCommission();
											        impsCommision.setMinAmount(1);
											        impsCommision.setMaxAmount(10000);
											        impsCommision.setValue(0);
											        impsCommision.setFixed(true);
											        impsCommision.setService(impsService);
											        commissionRepository.save(impsCommision);
											        
										        }
										        
										        
						/**
						 * CASHBACK
						 * */				        
										   	 MOperator cashBack = operatorRepository.findOperatorByName("CASHBACK");
												if (cashBack == null) {
													cashBack = new MOperator();
													cashBack.setName("CASHBACK");
													cashBack.setStatus(Status.Active);
													operatorRepository.save(cashBack);
												}
												
												MServiceType cBServiceType = serviceTypeRepository.findServiceTypeByName("CASHBACK");
												if (cBServiceType == null) {
													cBServiceType = new MServiceType();
													cBServiceType.setName("CASKBACK");
													cBServiceType.setDescription("CASHBACK in Cashier");
													cBServiceType = serviceTypeRepository.save(cBServiceType);
												}
												
												
												MService cBService = serviceRepository.findServiceByCode("CASH");
												 if (cBService == null) {
													 cBService = new MService();
													 cBService.setName("CASH");
													 cBService.setDescription("CASH for cashier");
													 cBService.setMinAmount(10);
													 cBService.setMaxAmount(500);
													 cBService.setCode("CASH");
													 cBService.setOperator(upiOperator);
													 cBService.setOperatorCode("CASH");
													 cBService.setStatus(Status.Active);
													 cBService.setServiceType(cBServiceType);
													 serviceRepository.save(cBService);
													 }
										        
									 
}
}
