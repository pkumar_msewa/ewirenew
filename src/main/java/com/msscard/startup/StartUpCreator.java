package com.msscard.startup;

import java.io.IOException;
import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.msscard.entity.MPQAccountDetails;
import com.msscard.entity.MPQAccountType;
import com.msscard.entity.MUser;
import com.msscard.entity.MUserDetails;
import com.msscard.model.Status;
import com.msscard.model.UserType;
import com.msscard.repositories.MPQAccountDetailRepository;
import com.msscard.repositories.MPQAccountTypeRepository;
import com.msscard.repositories.MUserDetailRepository;
import com.msscard.repositories.MUserRespository;
import com.msscard.util.Authorities;
import com.msscard.util.MssUtil;
import com.msscard.util.StartUpUtil;

public class StartUpCreator {

	private final MUserRespository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final MUserDetailRepository userDetailRepository;
	private final MPQAccountDetailRepository pqAccountDetailRepository;
	private final MPQAccountTypeRepository pqAccountTypeRepository;
	
	
	
	public StartUpCreator(MUserRespository userRepository, PasswordEncoder passwordEncoder,
			MUserDetailRepository userDetailRepository, MPQAccountDetailRepository pqAccountDetailRepository,
			MPQAccountTypeRepository pqAccountTypeRepository) {
		super();
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
	}



	public void create() throws IOException {
		
		MPQAccountType acTypeKYC = pqAccountTypeRepository.findByCode(StartUpUtil.KYC);
		if (acTypeKYC == null) {
			acTypeKYC = new MPQAccountType();
			acTypeKYC.setCode(StartUpUtil.KYC);
			acTypeKYC.setDailyLimit(50000);
			acTypeKYC.setMonthlyLimit(50000);
			acTypeKYC.setBalanceLimit(50000);
			acTypeKYC.setName("KYC");
			acTypeKYC.setDescription("Verified Account");
			acTypeKYC.setTransactionLimit(100);
			pqAccountTypeRepository.save(acTypeKYC);
		}
		
		

	

		MPQAccountType acTypeSuperKYC = pqAccountTypeRepository.findByCode(StartUpUtil.SUPER_KYC);
		if (acTypeSuperKYC == null) {
			acTypeSuperKYC = new MPQAccountType();
			acTypeSuperKYC.setCode(StartUpUtil.SUPER_KYC);
			acTypeSuperKYC.setDailyLimit(5000000);
			acTypeSuperKYC.setMonthlyLimit(5000000);
			acTypeSuperKYC.setBalanceLimit(5000000);
			acTypeSuperKYC.setName("KYC - MERCHANT");
			acTypeSuperKYC.setDescription("Verified Account");
			acTypeSuperKYC.setTransactionLimit(10000000);
			pqAccountTypeRepository.save(acTypeSuperKYC);
		}

		MPQAccountType acTypeNonKYC = pqAccountTypeRepository.findByCode(StartUpUtil.NON_KYC);
		if (acTypeNonKYC == null) {
			acTypeNonKYC = new MPQAccountType();
			acTypeNonKYC.setCode(StartUpUtil.NON_KYC);
			acTypeNonKYC.setDailyLimit(20000);
			acTypeNonKYC.setMonthlyLimit(20000);
			acTypeNonKYC.setBalanceLimit(20000);
			acTypeNonKYC.setName("Non - KYC");
			acTypeNonKYC.setDescription("Unverified Account");
			acTypeNonKYC.setTransactionLimit(5);
			pqAccountTypeRepository.save(acTypeNonKYC);
		}

		MUser admin = userRepository.findByUsername(StartUpUtil.ADMIN);
		if (admin == null) {
			MUserDetails detail = new MUserDetails();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartUpUtil.ADMIN);
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Admin");
			detail.setEmail(StartUpUtil.ADMIN);
			userDetailRepository.save(detail);

			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(MssUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			admin = new MUser();
			admin.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			admin.setPassword(passwordEncoder.encode("admin123456"));
			admin.setCreated(new Date());
			admin.setMobileStatus(Status.Active);
			admin.setEmailStatus(Status.Active);
			admin.setUserType(UserType.Admin);
			admin.setUsername(detail.getContactNo());
			admin.setUserDetail(detail);
			admin.setAccountDetail(pqAccountDetail);
			userRepository.save(admin);
		}



		

		MUser superAdmin = userRepository.findByUsername(StartUpUtil.SUPER_ADMIN);
		if (superAdmin == null) {
			MUserDetails detail = new MUserDetails();
			detail.setAddress("#106, 4th Cross, 2nd Block, Koramangala");
			detail.setContactNo("7022620747");
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Super Admin");
			detail.setEmail(StartUpUtil.SUPER_ADMIN);
			userDetailRepository.save(detail);

			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(MssUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			superAdmin = new MUser();
			superAdmin.setAuthority(Authorities.SUPER_ADMIN + "," + Authorities.AUTHENTICATED);
			System.err.println(superAdmin.getAuthority());
			superAdmin.setPassword(passwordEncoder.encode("7022620747"));
			superAdmin.setCreated(new Date());
			superAdmin.setMobileStatus(Status.Active);
			superAdmin.setEmailStatus(Status.Active);
			superAdmin.setUserType(UserType.SuperAdmin);
			superAdmin.setUsername(detail.getEmail());
			superAdmin.setUserDetail(detail);
			superAdmin.setAccountDetail(pqAccountDetail);
			userRepository.save(superAdmin);
		}

		
/*		 Super Agent Account Creator 
		MUser superAgent = userRepository.findByUsername(StartUpUtil.SUPER_AGENT);
		if (superAgent == null) {
			MUserDetails detail = new MUserDetails();
			detail.setAddress("Karnataka");
			detail.setContactNo("9850264933");
			detail.setFirstName("Super Agent");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartUpUtil.SUPER_AGENT);
			userDetailRepository.save(detail);

			MPQAccountDetails pqAccountDetail = new MPQAccountDetails();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(MssUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			superAgent = new MUser();
			superAgent.setAuthority(Authorities.SUPER_AGENT + "," + Authorities.AUTHENTICATED);
			superAgent.setPassword(passwordEncoder.encode("123456"));
			superAgent.setCreated(new Date());
			superAgent.setMobileStatus(Status.Active);
			superAgent.setEmailStatus(Status.Active);
			superAgent.setUserType(UserType.SuperAgent);
			superAgent.setUsername(detail.getEmail());
			superAgent.setUserDetail(detail);
			superAgent.setAccountDetail(pqAccountDetail);
			userRepository.save(superAgent);
		}
*/
		
		MUser mmCard = userRepository.findByUsername(StartUpUtil.MMCARD_USER);
		if (mmCard == null) {
			MUserDetails mmCardDetail = new MUserDetails();
			mmCardDetail.setAddress("Kormangala");
			mmCardDetail.setContactNo(StartUpUtil.MMCARD_USER);
			mmCardDetail.setFirstName("MatchMove");
			mmCardDetail.setMiddleName("_");
			mmCardDetail.setLastName("Mastercard");
			mmCardDetail.setEmail(StartUpUtil.MMCARD_USER);
			userDetailRepository.save(mmCardDetail);

			MPQAccountDetails cardAccDetail = new MPQAccountDetails();
			cardAccDetail.setBalance(0);
			cardAccDetail.setAccountNumber(MssUtil.BASE_ACCOUNT_NUMBER + mmCardDetail.getId());
			cardAccDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(cardAccDetail);

			mmCard = new MUser();
			mmCard.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			mmCard.setPassword(passwordEncoder.encode("paul12345678"));
			mmCard.setCreated(new Date());
			mmCard.setMobileStatus(Status.Active);
			mmCard.setEmailStatus(Status.Active);
			mmCard.setUserType(UserType.Locked);
			mmCard.setUsername(mmCardDetail.getContactNo());
			mmCard.setUserDetail(mmCardDetail);
			mmCard.setAccountDetail(cardAccDetail);
			userRepository.save(mmCard);
		}

//		IMPS
			

/*		MUser Imps = userRepository.findByUsername(StartUpUtil.IMPS);
		if (Imps == null) {
			MUserDetails mmCardDetail = new MUserDetails();
			mmCardDetail.setAddress("Kormangala");
			mmCardDetail.setContactNo("1234567890");
			mmCardDetail.setFirstName("MatchMove");
			mmCardDetail.setMiddleName("_");
			mmCardDetail.setLastName("Mastercard");
			mmCardDetail.setEmail(StartUpUtil.IMPS);
			userDetailRepository.save(mmCardDetail);

			MPQAccountDetails cardAccDetail = new MPQAccountDetails();
			cardAccDetail.setBalance(0);
			cardAccDetail.setAccountNumber(MssUtil.BASE_ACCOUNT_NUMBER + mmCardDetail.getId());
			cardAccDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(cardAccDetail);

			MUser mmCard = new MUser();
			mmCard.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			mmCard.setPassword(passwordEncoder.encode("imps123456"));
			mmCard.setCreated(new Date());
			mmCard.setMobileStatus(Status.Active);
			mmCard.setEmailStatus(Status.Active);
			mmCard.setUserType(UserType.Locked);
			mmCard.setUsername(StartUpUtil.IMPS);
			mmCard.setUserDetail(mmCardDetail);
			mmCard.setAccountDetail(cardAccDetail);
			userRepository.save(mmCard);
		}
		*/
		
	}

}
