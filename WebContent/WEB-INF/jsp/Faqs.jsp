<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>EWire | FAQs</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="keywords" content="Cashier Card - Enjoy great offers online & offline using Cashier Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
    <meta name="description" content="India&#039;s Digital Cash. Go cashless with Cashier Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">

    <!-- favicon -->
  	<link rel="icon" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png" type="image/gif" sizes="32x32">

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- slick slider -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets/slick/slick-theme.css">

</head>
<body>

<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

<nav class="navbar navbar-white">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
      	<img src="${pageContext.request.contextPath}/resources/assets/img/logo.png" class="img-responsive">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/About">about us</a></li>
        <li><a href="#section1">offers</a></li>
        <!-- <li><a href="#section2">testimonial</a></li> -->
        <li><a href="#section3">contact</a></li>
        <!-- <li><a href="#">refer &amp; earn</a></li> -->
      <!--   <li><a href="#" class="btn btn-sm btn-custom" style="margin-right: 6px;">login</a></li>
        <li><a href="#" class="btn btn-sm btn-custom">register</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="body-container">
	<div class="container" id="hero-image">

		<div class="aboutWrp">
			<div class="secHeading text-center">
				<span>About us</span>
			</div>
			<div class="aboutTxt">
				<p class="text-center">Customer gratification and business elevation are the foundations of our organization. EWire powered by Cashier is an international financial services technology company, established in 2017 with the aim to make transactions simpler and secure with our robust technical support.</p>

				<p class="text-center">We assist you in building a business network by connecting you with stakeholders and beneficiaries through a single platform to carry out smooth transactions.</p>

				<p class="text-center">Our support enables convergence of compliant domestic and cross-border payments, peer-to-peer, business-to-customer, business-to-business, merchant payments, shopping and other such transactions through a single platform.</p>
			</div>
		</div>

		<!-- Download App from store -->
		<div class="download_app">
			<div class="app_heading text-center">
				<span>In the palm of your hands</span>
			</div>
			<div class="app_subhead text-center">
				<span>Download our App Now</span>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/playS.png" class="img-responsive store-img right-align"></a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div>
							<a href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/assets/img/landing/appS.png" class="img-responsive store-img left-align"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer starts here -->
		<div class="footer">
			<div class="row">
				<div class="col-sm-6 col-xs-12" id="terms_link">
					<span><a href="${pageContext.request.contextPath}/PrivacyPolicy">privacy policy</a> | <a href="${pageContext.request.contextPath}/TermsnConditions">terms &amp; conditions</a> | <a href="${pageContext.request.contextPath}/faq">faq<small>s</small></a></span>
				</div>
				<div class="col-sm-6 col-xs-12 text-right" id="copyR">
					<div class="copyright">
						<span><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; copyright EWire.</span>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>




<!-- scripts starts here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/slick/slick.min.js"></script>

<!-- custom js for dashboard -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/js/dashboard.js"></script>

</body>

</html>