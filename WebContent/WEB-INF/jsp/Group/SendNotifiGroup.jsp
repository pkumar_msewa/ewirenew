<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css">

        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
		
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Send FCM Notification</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                          
                        <div class="row">
                        <div class="col-md-12 col-xl-12">
                             <div class="card m-b-30">
                                <div class="card-body">
                                <center><div><h6 style="font: bold;color: red;">${message}</h6></center>
                                    <div class="">
                                  
                                    
                                    <form action="${pageContext.request.contextPath}/Group/SendNotification" method="post" id="formId" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-6 offset-md-3 offset-sm-3">
                                                <fieldset>
                                                    <legend>Message Details</legend>
                                                    <div class="form-group bmd-form-group">
                                                        <label for="name" class="bmd-label-floating">Enter Message Title</label>
                                                        <input type="text" name="title" id="messageTitle" class="form-control" maxlength="50" required>
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div>
                                                    
                                                     <div class="form-group bmd-form-group">
                                                        <label for="email" class="bmd-label-floating">Enter Message Here</label>
                                                        <!-- <input type="text" name="message" placeholder="Enter Message Body" id="messageBody" class="form-control" maxlength="160"> -->
                                                        <textarea rows="3" class="form-control" maxlength="120" name="message"></textarea>
                                                   			<p class="error" id="error_bcName"></p>
                                                    </div>
                                                    
                                                    <div class="form-check form-check-inline checkbox mb-2">
                                                    <label>
                                                        <input type="checkbox" class="checkbox-decorator" name="withImage" id="fcmCheckbox" value="Yo">
                                                        With Image</label>
                                                    </div>
                                                       <div class="form-check form-check-inline">
                                                      <div class="radio">
                                                      <label>
                                                        <input type="radio" class="bmd-radio" name="allUser" id="singleUser" value="YoSingle">
                                                        Single User</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                    <div class="radio">
                                                    <label>
                                                        <input type="radio" class="bmd-radio" name="allUser" id="allUsers" value="YoAll" checked="checked">
                                                        All Users</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-check form-check-inline checkbox mb-2">
                                                     <label>
                                                        <input type="checkbox" class="checkbox-decorator" name="forIos" id="iosUser" value="ios">
                                                       For iOS</label>
                                                    </div>
                                                    
                                                    <div class="form-group" id="MyHide">
                                                        <label for="name">User's Mobile</label>
                                                        <input type="text" placeholder="Enter Mobile Number" name="userMobile" id="userMobile" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);">
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div>
                                                    <!-- <div id="checkUserMobile">
                                                     <div class="form-group" id="MyHide">
                                                        <label for="name">User's Mobile</label>
                                                        <input type="text" name="title" placeholder="Enter Mobile Number" id="userMobile" class="form-control" maxlength="50" onkeypress="return isAlphKey(event);">
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div>
                                                   </div> -->
                                                   
 </fieldset>
                                            </div>
                                            <div class="col-6 offset-md-3 offset-sm-3" id="imagingFcm">
                                              <p class="error" id="error_im"></p>
                                                                                    
                                          
                                            </div>
                                            </div>
                                            <center><button type="submit" id="addAgent" class="btn btn-primary mt-4" onclick="sendNoti()">Submit</button></center>
                                        </form>
                                       </div>
                                        
                                </div>
                            </div>
                            </div>
                        </div> <!-- end row -->

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->
        
           <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
        


        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script>

        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/4/core.js"></script>
        <script src="https://www.amcharts.com/lib/4/charts.js"></script>
        <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

        <!-- Datatable -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        

        <script>
            $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'pdf', 'csv' ]
                } );
             
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            } );
        </script>
        
           <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		<script>
	function getSingleUserDetails() {
		var username=$("#username").val();
		var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true) {
			$("#myForm").submit();
		}
	}

</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#fcmCheckbox').click(function(){
            if($(this).is(":checked") && $('#iosUser').is(":checked")){
            	  console.log("Checkbox is checked.");
                  $('#imagingFcm').empty();

                  var html='<fieldset>'+
                      '<legend>Upload Image</legend>'+
                      '<div class="row">'+
  						'<div class="col-sm-10">'+
  							'<div class="form-group">'+
                                  '<div class="fileUpload blue-btn btn width100">'+
                                      '<span id="spaning">Upload Image</span>'+
                                      '<input type="file" class="uploadlogo" name="aadhar_Image" id="aadhar_Image" onchange="imagUpload()"/>'+
                                    '</div>'+
                              '</div>'+															
  						'</div>'+
  						'<div class="col-sm-2">'+
  							'<div class="form-group">'+
  								'<a class="btn btn-warning" type="button" title="Upload Image to Server" href="javascript:uploadImage();"><i class="fa fa-cloud-upload"></i></a>'+																
  							'</div>'+															
  						'</div>'+                                                    	
                      '</div>'+
                      '<div class="form-group">'+
  						'<input type="text" class="form-control" id="urlRec" name="shortenedimageUrl">'+
                      '</div>'+
                      '<div>'+
                      	'<strong style="color: red;">Note: Copy this URL and go to the following <a target="_blank" href="https://bitly.com/">link</a> to shorten.</strong>'+
                      '</div>'+
                   
                  '</fieldset>'
              $('#imagingFcm').append(html);
            }
            else if($(this).is(":checked")){
            	 console.log("Checkbox is checked.");
                 $('#imagingFcm').empty();

                 var html='<fieldset>'+
                 '<legend>Upload Image</legend>'+
                 '<div class="form-group">'+
                     '<div class="fileUpload blue-btn btn width100">'+
                         '<span id="spaning">Upload Image</span>'+
                         
                         '<input type="file" class="uploadlogo" name="image" id="aadhar_Image" onchange="uploadImage()"/>'+
                      '</div>'+
                       '<p class="error" id="error_aaadharCardImg"></p>'+
                 '</div>'+
              
             '</fieldset>'
             $('#imagingFcm').append(html);

            }
            else if($(this).is(":not(:checked)")){
                console.log("Checkbox is unchecked.");
                $('#imagingFcm').empty();

            }
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
    	$('#MyHide').hide();
    	
    	$("#SingleUserN").hide();
        $('#singleUser').click(function(){
            if($(this).is(":checked")){
                console.log("Checkbox is checked.");
                $('#checkUserMobile').empty();
                $("#SingleUserN").hide();
                 /* var html='<div class="form-group bmd-form-group">'+
                '<label for="name" class="bmd-label-floating">Enter Mobile Number</label>'+
                '<input type="text" name="userMobile" id="userMobile" onkeypress="return isNumberKey(event);" class="form-control" maxlength="10">'+
           			'<p class="error" id="error_MobileNo"></p>'+
            '</div>' */
            //$('#myDiv').append(html);
            $('#MyHide').show();
            }
            else if($(this).is(":not(:checked)")){
                console.log("Checkbox is unchecked.");
                $('#checkUserMobile').empty();
                $('#MyHide').hide();
            }
        });
    });
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#iosUser').click(function(){
            if($(this).is(":checked") && $('#fcmCheckbox').is(":checked")){
                console.log("Checkbox is checked.");
                $('#imagingFcm').empty();

                var html='<fieldset>'+
                    '<legend>Upload Image</legend>'+
                    '<div class="row">'+
						'<div class="col-sm-10">'+
							'<div class="form-group">'+
                                '<div class="fileUpload blue-btn btn width100">'+
                                    '<span id="spaning">Upload Image</span>'+
                                    '<input type="file" class="uploadlogo" name="aadhar_Image" id="aadhar_Image" onchange="imagUpload()"/>'+
                                  '</div>'+
                            '</div>'+															
						'</div>'+
						'<div class="col-sm-2">'+
							'<div class="form-group">'+
								'<a class="btn btn-warning" type="button" title="Upload Image to Server" href="javascript:uploadImage();"><i class="fa fa-cloud-upload"></i></a>'+																
							'</div>'+															
						'</div>'+                                                    	
                    '</div>'+
                    '<div class="form-group">'+
						'<input type="text" class="form-control" id="urlRec" name="shortenedimageUrl">'+
                    '</div>'+
                    '<div>'+
                    	'<strong style="color: red;">Note: Copy this URL and go to the following <a target="_blank" href="https://bitly.com/">link</a> to shorten.</strong>'+
                    '</div>'+
                 
                '</fieldset>'
            $('#imagingFcm').append(html);
            
        }
            else if($(this).is(":not(:checked)") && $('#fcmCheckbox').is(":checked")){
                console.log("Checkbox is unchecked.");
                $('#imagingFcm').empty();
                var html='<fieldset>'+
                '<legend>Upload Image</legend>'+
                '<div class="form-group">'+
                    '<div class="fileUpload blue-btn btn width100">'+
                        '<span id="spaning">Upload Image</span>'+
                        
                        '<input type="file" class="uploadlogo" name="image" id="aadhar_Image" onchange="imagUpload()"/>'+
                     '</div>'+
                      '<p class="error" id="error_aaadharCardImg"></p>'+
                '</div>'+
             
            '</fieldset>'
            $('#imagingFcm').append(html);
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#allUsers').click(function(){
            if($(this).is(":checked")){
                console.log("Checkbox is checked.");
                $('#checkUserMobile').empty();

                           }
        });
    });
</script>

            <script>
            
            function imagUpload(){
            	   var filename = $('#aadhar_Image').val();
            	   console.log('filename'+filename);
            	 /*   var path = (window.URL || window.webkitURL).createObjectURL(filename);
            	    console.log('path', path);
 */
                   $('#spaning').html(filename);
                 }

                 // Read File and return value  
                 /* function readURL(input) {
                   var url = input.value;
                   var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                   if (input.files && input.files[0] && (
                     ext == "pdf"
                     )) {
                     var path = $(input).val();
                     var filename = path.replace(/^.*\\/, "");
                     // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                     return "Uploaded file : "+filename;
                   } else {
                     $(input).val("");
                     return "Only PDF format is allowed!";
                   }
                 }
 */
            </script>
            
            <script>
            function cleartext() {
            	$("#error_bcName").html("");
            }
            </script>
            
            
            
           <script type="text/javascript">
           function sendNoti(){
        		var valid=true;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		//var messageTitle=$('#messageTitle').val();
        		var messageBody = $('#message').val();
        		var singleUser=$("#singleUser").val();
        		var allUser=$('#allUsers').val();
        		//var aadharCardImg=$('#aadhar_Image').val();
        		//var mobileNo=$('#error_MobileNo').val();
        		var mob=$('#userMobile').val();        		
        		//console.log("Pancared image"+panCardImg);
        		
        		/* if(messageTitle=='') {
        			$("#error_fullName").html("Please enter Title");
        			valid = false;
        			console.log("valid: "+"fullName");
        		} */
        		if(messageBody == '') {
        			valid = false;
        			$('#error_bcName').html("please enter text");
        		}
        		if(!($('#singleUser').is(":checked") || $('#allUsers').is(":checked"))){
        			valid=false;
        			$('#error_email').html("Please select an option");
        		}
        		
        		if($('#singleUser').is(":checked")){
	        		if(mob=='') {
	        			$('#error_MobileNo').html("Please enter mobile number");
	        			valid = false;
	        			console.log("valid: "+"bankName");
	        		}
        		}
        	/* 	if(mob!=''){
        			if(mob.length!=10){
        			$('#error_MobileNo').html("Please enter valid contact no");
        			valid = false;
        		}
        		} */
        		/* if($('#fcmCheckbox').is(":checked")){
        		if(aadharCardImg==''){
        			valid=false;
        			$('#error_im').html("Please attach an image");
        		}
        		} */
        		console.log(valid);
        		/* if(valid==false){
             	   $('#"addAgent"').prop('disabled',true);
	
        		} else {        		
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
        		} */
        		if(valid) {
        		/* 	$("#addAgent").addClass("disabled");
        			$.ajax({
        				type : "POST",
        				contentType : "application/json",
        				url : "${pageContext.request.contextPath}/Group/SendGroupSms",
        				dataType : "json",
        				data : JSON.stringify({
        					"message" : ""+messageBody+"",      
        					"singleUser" : ""+singleUser+"",
        					"allUser" : ""+allUser+"",
        					"userMobile" : ""+mob+""
        				}),
        				success : function(response) {
        					if(response.code == "S00") {
        						$("#common_success_true").modal("show");
          						$("#common_success_msg").html(response.message);
          						console.log(response.message);
          						var timeout = setTimeout(function() {
          							$("#common_success_true").modal("hide");      								      							
          							$('#message').val("");
          							$('#userMobile').val("");              							
          						}, 2000);        						
        						$("#addAgent").removeClass("disabled");
        					}
        				}
        			}); */
        		}
        }

        	</script>
        	<script>
        	function uploadImage(){
        	        var file_data = $('#aadhar_Image').prop('file');   
        	        var form_data = new FormData();                  
        	        form_data.append("aadhar_Image", $('#aadhar_Image')[0].files[0]);
        	        form_data.append("brand","samsung")
        	        
        	        $.ajax({
    					type:"POST",
    					url:"${pageContext.request.contextPath}/Admin/UploadImage",
    					cache : false,
    				    processData: false,
    			        contentType : false,
    					data:form_data,
    				success:function(data){
    					console.log(data);
        	               console.log("url is:"+data.code);
        	               console.log("url is:"+data.cardDetails);
        	               //$('#urlRec').html(data.code);
        	              $('#urlRec').val(data.code);
        	               
        	               //document.getElementById("urlRec").innerHTML=data.code;

        	            }
        	        });
        	}
        	</script>
        	<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>



    </body>

</html>