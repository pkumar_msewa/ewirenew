<div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="mdi mdi-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo"><i class="mdi mdi-assistant"></i> Urora</a>-->
                        <a href="${pageContext.request.contextPath}/Group/Home" class="logo">
                            <img src="${pageContext.request.contextPath}/resources/assets-group/images/white.png" alt="" class="logo-large">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft" id="sidebar-main">

                    <div id="sidebar-menu">
                        <ul>
                           <!--  <li class="menu-title">Main</li> -->

                            <li>
                                <a href="${pageContext.request.contextPath}/Group/Home" class="waves-effect">
                                    <i class="mdi mdi-view-dashboard"></i>
                                    <span> Dashboard
                                        <span class="badge badge-pill badge-primary float-right"></span>
                                    </span>
                                </a>
                            </li>

                              <%--  <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-table"></i>
                                    <span>Upload Banners</span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/UploadBanner">Upload Banner</a>
                                    </li>                                    
                                </ul>
                            </li> --%>
                            
                            <li class="menu-title">Components</li>

                            <%-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-animation"></i>
                                    <span>Group Services</span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/GroupRequestList">Group Request List</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/AcceptedUserGroup">Approved User List</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/RejectedUserGroup">Rejected User List</a>
                                    </li>
                                    
                                </ul>
                            </li> --%>
                            
                            <li class="has_sub" id="groupServiceId"><a
					href="javascript:void(0);" class="waves-effect"> <i
						class="mdi mdi-animation"></i> <span>Group Services</span> <span
						class="float-right"> <i class="mdi mdi-chevron-right"></i>
					</span>
				</a>
					<ul class="list-unstyled">
						<li id="GSRL"><a
							href="${pageContext.request.contextPath}/Group/GroupRequestList">Group
								Request List</a></li>
						<li id="GSRL"><a
							href="${pageContext.request.contextPath}/Group/ChangeRequestList"
							title="Group Change Request List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Group
								Change Request List</a></li>
						<li id="GSAL"><a
							href="${pageContext.request.contextPath}/Group/AcceptedUserGroup">Approved
								User List</a></li>
						<li id="GSRJ"><a
							href="${pageContext.request.contextPath}/Group/RejectedUserGroup">Rejected
								User List</a></li>
						<li id="GBRF"><a
							href="${pageContext.request.contextPath}/Group/GrBulkRegFailList"
							title="Group Bulk Register Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Register Fail List</a></li>
						<li id="GBCA"><a
							href="${pageContext.request.contextPath}/Group/GrBulkCarFailList"
							title="Group Bulk Register Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Card Fail List</a></li>
						<li id="BKY"><a
							href="${pageContext.request.contextPath}/Group/GrBulkKycFailList"
							title="Group Bulk Kyc Fail List"
							style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Bulk
								Kyc Fail List</a></li>
					</ul></li>
                            
                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-table"></i>
                                    <span>Bulk Register</span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                                                     
                            </li> -->
				<li><a href="${pageContext.request.contextPath}/Group/BulkRegister" class="waves-effect"><i class="mdi mdi-table"></i> Group
						Bulk Register</a></li>
				<li id="GBCA"><a
					href="${pageContext.request.contextPath}/Group/BulkCardAssignment"
					class="waves-effect" title="Group Bulk Card Assignment"
					style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<i class="mdi mdi-table"></i> <span>Bulk Card Assignment <span
							class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>
				
				<li id="BKY"><a
					href="${pageContext.request.contextPath}/Group/BulkKYCApproval"
					class="waves-effect"> <i class="mdi mdi-dictionary"></i> <span>Group
							Bulk KYC <span class="badge badge-pill badge-primary float-right"></span>
					</span>
				</a></li>

				<li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-cards"></i>
                                    <span>Bulk SMS</span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/BulkSMS">Bulk SMS</a>
                                    </li>

                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/BulkUploadSmsList">Bulk SMS History</a>
                                         <a href="${pageContext.request.contextPath}/Group/SendGroupSms">Send SMS</a>
                                    </li>
                                    
                                </ul>
                            </li>

                            
                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-border-outside"></i>
                                    <span>Push Notification</span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/Group/SendNotification">Send Notification </a>
                                    </li>                                    
                                </ul>
                            </li> 
                            

                                </ul>
                            </li>

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- end sidebarinner -->
            </div>