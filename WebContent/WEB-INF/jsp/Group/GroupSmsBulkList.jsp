<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Group Bulk SMS Upload List | Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css">

        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
		
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
   
    <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
   
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Group Bulk SMS Upload List</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                               <!--  <div class="col-sm-12 col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                               
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                           <th>Sl No</th>
			                                                <th>Category</th>
			                                                <th>File URL</th>
			                                                <th>Requested Group</th>
			                                                <th>Request Date</th>
			                                                <th>Status</th>		
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach items="${CatalogueList}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.categoryType}" default="" escapeXml="true" /></td>
										<td><a href="${card.s3Path}" download>DOWNLOAD</a> </td>
										<td> <c:out value="${card.user.groupDetails.groupName}" default="" escapeXml="true" /></a></td>
										<td> <c:out value="${card.created}" default="" escapeXml="true" />
									</td>
									<td>
									<c:choose>
									<c:when test="${card.reviewStatus==true}">
										<c:out value="Reviewed/Done" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
		   								<c:out value="Pending" default="" escapeXml="true" /></td>
									</c:otherwise>
									</c:choose> 
								
								</tr>
							</c:forEach>                                                
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script>

        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/4/core.js"></script>
        <script src="https://www.amcharts.com/lib/4/charts.js"></script>
        <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

        <!-- Datatable -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        

        <script>
            $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'pdf', 'csv' ]
                } );
             
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            } );
        </script>
        
           <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		<script>
	function getSingleUserDetails() {
		var username=$("#username").val();
		var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true) {
			$("#myForm").submit();
		}
	}

</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

 <script>
 
 function clickMe(value){
	 $('#corp').val(value);
	 $('#corpSub').submit();
 }
 
 </script>

    </body>

</html>