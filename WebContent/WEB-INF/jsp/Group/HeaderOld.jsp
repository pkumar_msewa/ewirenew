<script src="${pageContext.request.contextPath}/resources/admin/assets/js/status.js"></script>

<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <a href="${pageContext.request.contextPath}/Group/Home" class="logo">
            <span>
                <img src="${pageContext.request.contextPath}/resources/admin/assets/images/logo.png" alt="" width="180" style="margin-top: -9px;">
            </span>
            <i>
                <img src="${pageContext.request.contextPath}/resources/admin/assets/images/favicon.png" alt="" height="28">
            </i>
        </a>
    </div>

    <nav class="navbar-custom">

        <ul class="list-unstyled topbar-right-menu float-right mb-0">
            <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <center>
                                    <img src="${pageContext.request.contextPath}/resources/admin/assets/images/bell.png" alt="Notification" style="width: 37px;">
                                </center>
                                <span class="badge badge-danger badge-pill noti-icon-badge" id="badgeNo"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg" style="position: absolute; transform: translate3d(50px, 70px, 0px); top: 0px; left: 0px; will-change: transform;" x-placement="bottom-end">

                                <!-- item-->
                                <div class="dropdown-item noti-title" >
                                    <h6 class="m-0"><span class="float-right"></span>Downloads</h6>
                                </div>

                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 255px !important; overflow-y: auto;">
                                    <!-- item-->
                                    <div id="kanchan">
                                    <!-- <div class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-file-document"></i></div>
                                        <p class="notify-details">Download File Name<small class="text-success">Downloaded</small></p>
                                    </div> -->

                                    </div>

                            </div>
                            </div>
                        </li>
            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                    <img src="${pageContext.request.contextPath}/resources/admin/assets/images/avatar.png" alt="user" class="rounded-circle"><span class="ml-1"><i class="mdi mdi-chevron-down"></i> </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    
                    <!-- item-->
                    <a href="${pageContext.request.contextPath}/Group/Logout" class="dropdown-item notify-item">
                        <i class="fi-power"></i> <span>Logout</span>
                    </a>

                </div>
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
        </ul>

    </nav>

</div>