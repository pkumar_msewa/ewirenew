<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Send Notification</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
 <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
		<script>var pageContext = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Send Notification</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                <center><div><h6 style="font: bold;color: red;" id="tmessage">${message}</h6></center>
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    
                                    <form action="#" method="post" id="formId">
                                            <div class="row">
                                                <div class="col-6 offset-md-3 offset-sm-3">
                                                <fieldset>
                                                    <legend>Message Details</legend>
                                                  <!--   <div class="form-group">
                                                        <label for="name">Message Title</label>
                                                        <input type="text" name="title" placeholder="Enter Message Title" id="messageTitle" class="form-control" maxlength="50" required>
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div> -->
                                                    
                                                     <div class="form-group">
                                                        <label for="email">Message Body</label>
                                                        <!-- <input type="text" name="message" placeholder="Enter Message Body" id="messageBody" class="form-control" maxlength="160"> -->
                                                        <textarea rows="3" class="form-control" placeholder="Enter Message Here" maxlength="120" id="message" name="message" onkeypress="cleartext();"></textarea>
                                                   			<p class="error" id="error_bcName"></p>
                                                    </div>
                                                    
                                                   <!--  <div class="form-check form-check-inline">
                                                        <input type="checkbox" name="withImage" id="fcmCheckbox" value="Yo">
                                                        <label for="image">With Image</label>
                                                    </div> -->
                                                       <div class="form-check form-check-inline">
                                                        <input type="radio" name="allUser" id="singleUser" value="YoSingle">
                                                        <label for="single-user">Single User</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" name="allUser" id="allUsers" value="YoAll" checked="checked">
                                                        <label for="all-user">All Users</label>
                                                    </div>
                                                   <!--  <div class="form-check form-check-inline">
                                                        <input type="checkbox" name="forIos" id="iosUser" value="ios">
                                                        <label for="ios">For iOS</label>
                                                    </div> -->
                                                    <div id="checkUserMobile">
                                                    <!--  <div class="form-group">
                                                        <label for="name">User's Mobile</label>
                                                        <input type="text" name="title" placeholder="Enter Mobile Number" id="userMobile" class="form-control" maxlength="50" onkeypress="return isAlphKey(event);">
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div> -->
                                                    </div>
                                                    <center><span class="error" id="error_email"></span></center>
                                                    
                                                  
 </fieldset>
                                            </div>
                                            <div class="col-6 offset-md-3 offset-sm-3" id="imagingFcm">
                                              <p class="error" id="error_im"></p>
                                            
                                               <!--  <fieldset>
                                                    <legend>Upload Image</legend>
                                                    <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Image</span>
                                                            <input type="file" class="uploadlogo" name="image" id="aadhar_Image"/>
                                                          </div>
                                                          <p class="error" id="error_aaadharCardImg"></p>
                                                    </div>
                                                 
                                                </fieldset> -->
                                                
                                                <!--  <fieldset>
                                                    <legend>Upload Image</legend>
                                                    <div class="row">
														<div class="col-sm-10">
															<div class="form-group">
		                                                        <div class="fileUpload blue-btn btn width100">
		                                                            <span>Upload Image</span>
		                                                            <input type="file" class="uploadlogo" name="image" id="aadhar_Image"/>
		                                                          </div>
		                                                          <p class="error" id="error_aaadharCardImg"></p>
		                                                    </div>															
														</div>
														<div class="col-sm-2">
															<div class="form-group">
																<a class="btn btn-warning" type="button" title="Upload Image to Server"><i class="fa fa-cloud-upload"></i></a>																
															</div>															
														</div>                                                    	
                                                    </div>
                                                    <div class="form-group">
														<input type="text" class="form-control">                                                    	
                                                    </div>
                                                    <div>
                                                    	<strong style="color: red;">Note: Copy this URL and go to the following <a target="_blank" href="https://bitly.com/">link</a> to shorten.</strong>
                                                    </div>
                                                 
                                                </fieldset> -->
                                            </div>
                                            </div>
                                            <center><button type="button" id="addAgent" class="btn btn-primary mt-4" onclick="sendNoti()">Submit</button></center>
                                        </form>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
          <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>



        <!-- jQuery  -->
  <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
           <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

        <!-- Daterange picker -->
        <script type="text/javascript">
            $(function() {

                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                
            });
            </script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#fcmCheckbox').click(function(){
            if($(this).is(":checked") && $('#iosUser').is(":checked")){
            	  console.log("Checkbox is checked.");
                  $('#imagingFcm').empty();

                  var html='<fieldset>'+
                      '<legend>Upload Image</legend>'+
                      '<div class="row">'+
  						'<div class="col-sm-10">'+
  							'<div class="form-group">'+
                                  '<div class="fileUpload blue-btn btn width100">'+
                                      '<span id="spaning">Upload Image</span>'+
                                      '<input type="file" class="uploadlogo" name="aadhar_Image" id="aadhar_Image" onchange="imagUpload()"/>'+
                                    '</div>'+
                              '</div>'+															
  						'</div>'+
  						'<div class="col-sm-2">'+
  							'<div class="form-group">'+
  								'<a class="btn btn-warning" type="button" title="Upload Image to Server" href="javascript:uploadImage();"><i class="fa fa-cloud-upload"></i></a>'+																
  							'</div>'+															
  						'</div>'+                                                    	
                      '</div>'+
                      '<div class="form-group">'+
  						'<input type="text" class="form-control" id="urlRec" name="shortenedimageUrl">'+
                      '</div>'+
                      '<div>'+
                      	'<strong style="color: red;">Note: Copy this URL and go to the following <a target="_blank" href="https://bitly.com/">link</a> to shorten.</strong>'+
                      '</div>'+
                   
                  '</fieldset>'
              $('#imagingFcm').append(html);
            }
            else if($(this).is(":checked")){
            	 console.log("Checkbox is checked.");
                 $('#imagingFcm').empty();

                 var html='<fieldset>'+
                 '<legend>Upload Image</legend>'+
                 '<div class="form-group">'+
                     '<div class="fileUpload blue-btn btn width100">'+
                         '<span id="spaning">Upload Image</span>'+
                         
                         '<input type="file" class="uploadlogo" name="image" id="aadhar_Image" onchange="imagUpload()"/>'+
                      '</div>'+
                       '<p class="error" id="error_aaadharCardImg"></p>'+
                 '</div>'+
              
             '</fieldset>'
             $('#imagingFcm').append(html);

            }
            else if($(this).is(":not(:checked)")){
                console.log("Checkbox is unchecked.");
                $('#imagingFcm').empty();

            }
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#singleUser').click(function(){
            if($(this).is(":checked")){
                console.log("Checkbox is checked.");
                $('#checkUserMobile').empty();

                var html='<div class="form-group">'+
                '<label for="name">User Mobile</label>'+
                '<input type="text" name="userMobile" placeholder="Enter Mobile Number" id="userMobile" onkeypress="return isNumberKey(event);" class="form-control" maxlength="10">'+
           			'<p class="error" id="error_MobileNo"></p>'+
            '</div>'
            $('#checkUserMobile').append(html);
            }
            else if($(this).is(":not(:checked)")){
                console.log("Checkbox is unchecked.");
                $('#checkUserMobile').empty();

            }
        });
    });
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#iosUser').click(function(){
            if($(this).is(":checked") && $('#fcmCheckbox').is(":checked")){
                console.log("Checkbox is checked.");
                $('#imagingFcm').empty();

                var html='<fieldset>'+
                    '<legend>Upload Image</legend>'+
                    '<div class="row">'+
						'<div class="col-sm-10">'+
							'<div class="form-group">'+
                                '<div class="fileUpload blue-btn btn width100">'+
                                    '<span id="spaning">Upload Image</span>'+
                                    '<input type="file" class="uploadlogo" name="aadhar_Image" id="aadhar_Image" onchange="imagUpload()"/>'+
                                  '</div>'+
                            '</div>'+															
						'</div>'+
						'<div class="col-sm-2">'+
							'<div class="form-group">'+
								'<a class="btn btn-warning" type="button" title="Upload Image to Server" href="javascript:uploadImage();"><i class="fa fa-cloud-upload"></i></a>'+																
							'</div>'+															
						'</div>'+                                                    	
                    '</div>'+
                    '<div class="form-group">'+
						'<input type="text" class="form-control" id="urlRec" name="shortenedimageUrl">'+
                    '</div>'+
                    '<div>'+
                    	'<strong style="color: red;">Note: Copy this URL and go to the following <a target="_blank" href="https://bitly.com/">link</a> to shorten.</strong>'+
                    '</div>'+
                 
                '</fieldset>'
            $('#imagingFcm').append(html);
            
        }
            else if($(this).is(":not(:checked)") && $('#fcmCheckbox').is(":checked")){
                console.log("Checkbox is unchecked.");
                $('#imagingFcm').empty();
                var html='<fieldset>'+
                '<legend>Upload Image</legend>'+
                '<div class="form-group">'+
                    '<div class="fileUpload blue-btn btn width100">'+
                        '<span id="spaning">Upload Image</span>'+
                        
                        '<input type="file" class="uploadlogo" name="image" id="aadhar_Image" onchange="imagUpload()"/>'+
                     '</div>'+
                      '<p class="error" id="error_aaadharCardImg"></p>'+
                '</div>'+
             
            '</fieldset>'
            $('#imagingFcm').append(html);
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#allUsers').click(function(){
            if($(this).is(":checked")){
                console.log("Checkbox is checked.");
                $('#checkUserMobile').empty();

                           }
        });
    });
</script>

<!--             <script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {

              // Upload btn on change call function
              $(".uploadlogo").change(function() {
            	  console.log('i am here');
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });

              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                        ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "gif"

                  )) {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only Image format is allowed!";
                }
              }
              // Upload btn end

            });
            </script> -->
            <script>
            
            function imagUpload(){
            	   var filename = $('#aadhar_Image').val();
            	   console.log('filename'+filename);
            	 /*   var path = (window.URL || window.webkitURL).createObjectURL(filename);
            	    console.log('path', path);
 */
                   $('#spaning').html(filename);
                 }

                 // Read File and return value  
                 /* function readURL(input) {
                   var url = input.value;
                   var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                   if (input.files && input.files[0] && (
                     ext == "pdf"
                     )) {
                     var path = $(input).val();
                     var filename = path.replace(/^.*\\/, "");
                     // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                     return "Uploaded file : "+filename;
                   } else {
                     $(input).val("");
                     return "Only PDF format is allowed!";
                   }
                 }
 */
            </script>
            
            <script>
            function cleartext() {
            	$("#error_bcName").html("");
            }
            </script>
            
            
            
           <script type="text/javascript">
           function sendNoti(){
        		var valid=true;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		//var messageTitle=$('#messageTitle').val();
        		var messageBody = $('#message').val();
        		var singleUser=$("#singleUser").val();
        		var allUser=$('#allUsers').val();
        		//var aadharCardImg=$('#aadhar_Image').val();
        		//var mobileNo=$('#error_MobileNo').val();
        		var mob=$('#userMobile').val();        		
        		//console.log("Pancared image"+panCardImg);
        		
        		/* if(messageTitle=='') {
        			$("#error_fullName").html("Please enter Title");
        			valid = false;
        			console.log("valid: "+"fullName");
        		} */
        		if(messageBody == '') {
        			valid = false;
        			$('#error_bcName').html("please enter text");
        		}
        		if(!($('#singleUser').is(":checked") || $('#allUsers').is(":checked"))){
        			valid=false;
        			$('#error_email').html("Please select an option");
        		}
        		
        		if($('#singleUser').is(":checked")){
	        		if(mob=='') {
	        			$('#error_MobileNo').html("Please enter mobile number");
	        			valid = false;
	        			console.log("valid: "+"bankName");
	        		}
        		}
        	/* 	if(mob!=''){
        			if(mob.length!=10){
        			$('#error_MobileNo').html("Please enter valid contact no");
        			valid = false;
        		}
        		} */
        		/* if($('#fcmCheckbox').is(":checked")){
        		if(aadharCardImg==''){
        			valid=false;
        			$('#error_im').html("Please attach an image");
        		}
        		} */
        		console.log(valid);
        		/* if(valid==false){
             	   $('#"addAgent"').prop('disabled',true);
	
        		} else {        		
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
        		} */
        		if(valid) {
        			$("#addAgent").addClass("disabled");
        			$.ajax({
        				type : "POST",
        				contentType : "application/json",
        				url : "${pageContext.request.contextPath}/Group/SendGroupSms",
        				dataType : "json",
        				data : JSON.stringify({
        					"message" : ""+messageBody+"",      
        					"singleUser" : ""+singleUser+"",
        					"allUser" : ""+allUser+"",
        					"userMobile" : ""+mob+""
        				}),
        				success : function(response) {
        					if(response.code == "S00") {
        						$("#common_success_true").modal("show");
          						$("#common_success_msg").html(response.message);
          						console.log(response.message);
          						var timeout = setTimeout(function() {
          							$("#common_success_true").modal("hide");      								      							
          							$('#message').val("");
          							$('#userMobile').val("");              							
          						}, 2000);        						
        						$("#addAgent").removeClass("disabled");
        					}
        				}
        			});
        		}
        }

        	</script>
        	<script>
        	function uploadImage(){
        	        var file_data = $('#aadhar_Image').prop('file');   
        	        var form_data = new FormData();                  
        	        form_data.append("aadhar_Image", $('#aadhar_Image')[0].files[0]);
        	        form_data.append("brand","samsung")
        	        
        	        $.ajax({
    					type:"POST",
    					url:"${pageContext.request.contextPath}/Admin/UploadImage",
    					cache : false,
    				    processData: false,
    			        contentType : false,
    					data:form_data,
    				success:function(data){
    					console.log(data);
        	               console.log("url is:"+data.code);
        	               console.log("url is:"+data.cardDetails);
        	               //$('#urlRec').html(data.code);
        	              $('#urlRec').val(data.code);
        	               
        	               //document.getElementById("urlRec").innerHTML=data.code;

        	            }
        	        });
        	}
        	</script>
        	<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    </body>

<!-- Mirrored from coderthemes.com/abstack/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jan 2018 11:48:32 GMT -->
</html>