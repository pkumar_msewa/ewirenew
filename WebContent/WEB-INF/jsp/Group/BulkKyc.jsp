<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Group | Bulk KYC</title>
<meta content="Admin Dashboard" name="description" />
<meta content="Mannatthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

<!-- CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/style.css"
	type="text/css">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css"> --%>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/corporate/assets/dropify/css/dropify.min.css">


<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/group/style.css">
<!-- endinject -->

<script
	src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
<script>
	var contextPath = "${pageContext.request.contextPath}";
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
</head>

<body class="fixed-left">
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />

				<div class="page-content-wrapper dashborad-v">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-12">
								<div class="page-title-box">
									<h4 class="page-title">Group Bulk KYC Upload</h4>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card m-b-30">
									<div class="card-body">
										<h6 align="right" style="color: green;">
											<a
												href="${pageContext.request.contextPath}/Group/download/bulkKyc">Click
												here to download format</a>
										</h6>

										<form id="formId"
											action="${pageContext.request.contextPath}/Group/BulkKYCApproval"
											method="Post" enctype="multipart/form-data">
											<input type="hidden" name="csrf_token"
												value="${user_csrf_token}">

											<div class="form-group">
												<input type="file" class="dropify" name="file" id="fileId" />
											</div>
											<div class="form-group" align="center">
												<button class="btn btn-primary" type="button" id="uploadDoc"
													disabled onclick="submitForm();">Submit</button>
											</div>
										</form>

										<div align="center">
											<h6 class="card-title" style="color: red;">${sucessMSG}</h6>
											<p style="color: red;" id="errorMsg"></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				&copy;
				<script type="text/javascript">document.write(new Date().getFullYear())</script>
				MSS Payments Pvt. Ltd.
			</footer>
			<input type="hidden" id="fileHiddenId" /> <input type="hidden"
				id="fileTypeHiddenId" />
		</div>
	</div>

	<!-- jQuery  -->

	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/assets/dropify/js/dropify.min.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropify").change(
									function() {
										$("#errorMsg").html("");
										var filename = readURL(this);
										$(this).parent().children('span').html(
												filename);
									});

							// Read File and return value  
							function readURL(input) {
								var url = input.value;
								var name = url.substring(
										url.lastIndexOf('\\') + 1)
										.toLowerCase();
								$("#fileHiddenId").val(name);
								$("#fileTypeHiddenId").val(input.files[0].type);

								var ext = name.substring(name.indexOf('.') + 1)
										.toLowerCase();

								if (input.files && input.files[0]
										&& (ext == "csv")) {
									var path = $(input).val();
									var filename = path.replace(/^.*\\/, "");
									$('#uploadDoc').attr('disabled', false);

									return "Uploaded file : " + filename;
								} else {
									document.getElementById('uploadDoc').disabled = 'disabled';
									$(input).val("");
									$("#errorMsg").html(
											"Only csv format is allowed.");
									return "";
								}
							}
						});
	</script>

	<script>
		function submitForm() {
			var valid = true;

			var fileName = $("#fileId").val().trim();
			var preFileName = $("#fileHiddenId").val();

			var preFileType = $("#fileTypeHiddenId").val();

			if (fileName != "" && fileName.includes("\\")) {
				fileName = fileName.substring(fileName.lastIndexOf('\\') + 1)
			}

			if (fileName != preFileName) {
				valid = false;
			}

			if (valid) {
				$("#formId").submit();
			}
		}
	</script>
</body>
</html>