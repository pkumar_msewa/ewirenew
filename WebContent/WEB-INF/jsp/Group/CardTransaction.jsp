<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Group | Card Transaction</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />

<script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>
</head>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="${pageContext.request.contextPath}/resources/corporate/images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="${pageContext.request.contextPath}/resources/corporate/images/logo.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav">
         
        </ul>
        <ul class="navbar-nav navbar-nav-right">
        
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img src="${pageContext.request.contextPath}/resources/corporate/images/faces/face1.jpg" alt="image">
              <span class="d-none d-lg-inline">Corporate</span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <!-- <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached mr-2 text-success"></i>
                Activity Log
              </a>
              <div class="dropdown-divider"></div> -->
              <a class="dropdown-item" href="${pageContext.request.contextPath}/Group/Logout">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Signout
              </a>
            </div>
          </li>
       
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
       <!-- partial -->
                  <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp"/>
      
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card text-black">
                <div class="card-body">
                  <div class="row">
                    <div class="col-8">
                      <h4 class="card-title">Card Transactions</h4>
                    </div>
                    <!-- <div class="col-4">
                      <form class="form-inline">
                        <div class="form-group">
                          <label>Show By:&nbsp;</label>
                          <select class="form-control">
                            <option selected="selected">Active User</option>
                            <option>Inactive User</option>
                            <option>Blocked User</option>
                            <option>Failed User(Onboard)</option>
                          </select>
                        </div>
                      </form>
                    </div> -->
                  </div>
                  <div class="tabble">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                           <tr>
                             <th>Sl No</th>
                            <th>Amount</th>
                            <th>TransactionType</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Status</th>
                           </tr>
                      </thead>
                      <tbody>
                      	<c:forEach items="${transactions}" var="card" varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.description}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.date}" default="" escapeXml="true" />
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>
							</c:forEach>
                                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="#" target="_blank">Mss Payments Pvt. Ltd.</a> All rights reserved.</span>
            <!-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span> -->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
  <!-- <script src="js/hoverable-collapse.js"></script> -->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
  <%-- <script src="${pageContext.request.contextPath}/resources/corporate/js/data-table.js"></script> --%>
  

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
  <!-- End custom js for this page-->
  
  
  
    
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
        	searching: true
        });
    } );
    </script>
</body>

</html>
