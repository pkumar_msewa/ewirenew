<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Group | Change Group Request</title>
<meta content="Admin Dashboard" name="description" />
<meta content="Mannatthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

<!-- CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/assets-group/css/style.css"
	type="text/css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css">

<!-- Datatables -->
<link
	href="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link
	href="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script
	src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
<script>var contextPath = "${pageContext.request.contextPath}";</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/GassignService.js"></script>
</head>

<body class="fixed-left">
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div>

	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
				<div class="page-content-wrapper dashborad-v">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-12">
								<div class="page-title-box">
									<h4 class="page-title">Change Group Request List</h4>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card m-b-30">
									<div class="card-body">

										<div class="row">
											<div class="col-md-6">
												<form action="<c:url value="/Group/ChangeRequestList"/>"
													method="post">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group bmd-form-group">
																<input type="text" class="form-control" id="reportrange"
																	name="reportrange" value="${dat}" readonly="readonly">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<button type="submit"
																	class="btn btn-primary btn-sm mt-4">Search</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<table id="datatable-buttons"
											class="table table-striped table-bordered w-100">
											<thead>
												<tr>
													<th>S. No.</th>
													<th>User Details</th>
													<th>Group Details</th>
													<th>Date</th>
													<th>Accept</th>
													<th>Reject</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${userData}" var="card"
													varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td><b>Name:</b> <c:out
																value="${card.userDetail.firstName} ${card.userDetail.lastName}"
																default="" escapeXml="true" /><br> <b>Contact
																No:</b> <c:out value="${card.username}" default=""
																escapeXml="true" /> <br /> <b>Email Id</b> <c:out
																value="${card.userDetail.email}" default=""
																escapeXml="true" /></td>
														<td><b>Group Name:</b> <c:out
																value="${card.groupDetails.groupName}" default=""
																escapeXml="true" /><br /> <b>Remark:</b> <c:choose>
																<c:when test="${card.userDetail.remark eq null}">
																	<c:if test="${card.userDetail.remark eq null}">
																		<c:out value="-"></c:out>
																	</c:if>
																</c:when>
																<c:otherwise>
																	<c:out value="${card.userDetail.remark}" default=""
																		escapeXml="true" />
																</c:otherwise>
															</c:choose></td>
														<td><fmt:formatDate pattern="dd-MM-yyyy"
																value="${card.created}" /></td>
														<td><input type="hidden" id="counting"
															value="${count.count}" /> <a
															onclick="accept('${card.username}');">
																<button class="btn btn-success" id="accept">Accept</button>
														</a></td>
														<td><input type="hidden" value="${slist.contactNo}"
															name="mobileNo" id="mobileNo"> <a
															onclick="rejectionReason('${card.username}');">
																<button class="btn btn-danger" id="reject">Reject</button>
														</a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				&copy;
				<script type="text/javascript">document.write(new Date().getFullYear())</script>
				MSS Payments Pvt. Ltd.
			</footer>
		</div>
	</div>

	<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_success_msg" class="alert alert-success"></center></B>
				</div>
			</div>
		</div>
	</div>

	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_error_msg" class="alert alert-danger"></center></B>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModalReject" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Rejection
						Reason</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="address3">Reason</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="address3"
									placeholder="Reason" name="address3" required /> <span
									class="error" style="color: red;" id="rejectreasonError2"></span>
							</div>
						</div>
						<input type="hidden" id="rejectContact" name="rejectContact" />
					</form>
				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
					<button type="button" class="btn btn-primary" onclick="reject()">Add
						Reason</button>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery  -->

	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script>


	<!-- Datatable -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.bootstrap4.min.js"></script>
	<!-- Buttons examples -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.bootstrap4.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/buttons.colVis.min.js"></script>
	<!-- Responsive examples -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/datatables/responsive.bootstrap4.min.js"></script>

	<!-- Datatable init js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/datatables.init.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script>

	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

	<script>
            $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'pdf', 'csv' ]
                } );
             
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            } );
        </script>

	<script>
		$(function() {
			var startD = '${startDate}';
			var endD = '${endDate}';

			var start = moment(startD);
			var end = moment(endD);
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>

	<script>
		function rejectionReason(username) {
			$("#rejectContact").val(username);
			$("#myModalReject").modal("show");
		}
		
	function accept(username) {
		$.ajax({
			type: "POST",
    		datatype : 'json',
    		contentType: "application/json",
			url: "${pageContext.request.contextPath}/Group/AcceptGroup/"+username,
			data: JSON.stringify({
				"contactNo" : username				
			}),
			success: function (response) {
				if(response.code.includes("S00")) {
					
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				} else {
					
					$("#common_error_true").modal("show");
		  			$("#common_error_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				}
			}
		});
	}
	
	function reject() {
		$("#rejectreasonError").html("");
		var username = $("#rejectContact").val();
		var reason = $("#address3").val();
		var valid = true;
		if(reason.length <= 0) {
			$("#rejectreasonError2").html("please enter rejection reason");
			valid = false;		
		}
		if(valid) {
		$.ajax({
			type : "POST",
			datatype : 'json',
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/Group/RejectGroup",
			data: JSON.stringify({
				"username" : username,
				"status" : reason
			}),
			success: function (response) {
				$("#myModalReject").modal("hide");
				if(response.code.includes("S00")) {
					
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				} else {
					
					$("#common_error_true").modal("show");
		  			$("#common_error_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				}
			}
		});
		}
	}
	</script>

	<script>
	function getSingleUserDetails() {
		var username=$("#username").val();
		var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true) {
			$("#myForm").submit();
		}
	}
</script>

	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

</body>
</html>