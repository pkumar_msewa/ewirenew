<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Group | EWire</title>
 <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/themify-icons/themify-icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            body {
                overflow: hidden;
            }
            footer {
                position: fixed;
                width: 100%;
                bottom: 0;
            }
            .input-group-prepend {
                margin-right: -1px;
            }
            .input-group-append, .input-group-prepend {
                display: flex;
            }
            .input-group>.input-group-append:last-child>.btn:not(:last-child):not(.dropdown-toggle), .input-group>.input-group-append:last-child>.input-group-text:not(:last-child), .input-group>.input-group-append:not(:last-child)>.btn, .input-group>.input-group-append:not(:last-child)>.input-group-text, .input-group>.input-group-prepend>.btn, .input-group>.input-group-prepend>.input-group-text {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0;
            }
            .input-group-text {
                display: flex;
                align-items: center;
                padding: .375rem .75rem;
                font-size: .875rem;
                font-weight: 300;
                line-height: 1.5;
                color: #4f5467;
                text-align: center;
                background-color: #f8f9fa;
                border: 1px solid #e9ecef;
                border-radius: 2px;
            }

            .btn-group-toggle>.btn, .btn-group-toggle>.btn-group>.btn, .custom-control-label, .custom-file, .dropdown-header, .input-group-text, .nav {
                margin-bottom: 0;
            }

            .badge, .btn, .dropdown-header, .dropdown-item, .input-group-text, .navbar-brand, .progress-bar {
                white-space: nowrap;
            }
            .input-group>.custom-file, .input-group>.custom-select, .input-group>.form-control {
                position: relative;
                flex: 1 1 auto;
                width: 1%;
                margin-bottom: 0;
            }
            .form-control-lg {
                padding: .5rem 1rem;
                font-size: 1.09375rem;
                line-height: 1.5;
                border-radius: 2px;
            }
            .custom-control {
                position: relative;
                display: block;
                min-height: 1.5rem;
                padding-left: 1.5rem;
            }
            .custom-control-input {
                position: absolute;
                z-index: -1;
                opacity: 0;
            }
            .custom-checkbox .custom-control-label::before {
                border-radius: 2px;
            }
            .custom-control-label::after, .custom-control-label::before {
                top: .15rem;
            }
            .custom-control-label::before {
                pointer-events: none;
                user-select: none;
                background-color: #dee2e6;
            }
            .custom-control-label::after, .custom-control-label::before {
                position: absolute;
                display: block;
                width: 1rem;
                height: 1rem;
                content: "";
                left: 0;
            }
            .custom-control-label::after {
                background-repeat: no-repeat;
                background-position: center center;
                background-size: 50% 50%;
            }
            .custom-checkbox .custom-control-input:checked~.custom-control-label::before, .custom-checkbox .custom-control-input:indeterminate~.custom-control-label::before {
                background-color: #4798e8;
            }
            .custom-control-input:checked~.custom-control-label::before {
                color: #fff;
                background-color: #4798e8;
            }
            .custom-checkbox .custom-control-input:checked~.custom-control-label::after {
                background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3E%3Cpath fill='%23fff' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26 2.974 7.25 8 2.193z'/%3E%3C/svg%3E");
            }
            .se-pre-con {
				position: fixed;
				left: 0px;
				top: 0px;
				width: 100%;
				height: 100%;
				z-index: 9999;
				background: url(${pageContext.request.contextPath}/resources/spinner.gif)
				center no-repeat #fff;
			}
        </style>
</head>
<body class="se-pre-con">
	<section>
	<div class="main-wrapper">
                <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background: url(${pageContext.request.contextPath}/resources/admin/assets/images/back.png) no-repeat center center;">
                    <div class="auth-box">
                        <div id="loginfrom">
                            <div class="logo">
                                <span class="db">
                                    <img src="${pageContext.request.contextPath}/resources/admin/assets/images/logo.png" alt="logo">
                                    <h6 class="font-medium mb-3">Sign In to Group</h6>
                                </span>
                            </div>
                            <!-- form -->
                            <div class="row">
                                <div class="col-12">
                                    <form class="form-horizontal mt-3" method="post" action="${pageContext.request.contextPath}/Group/Login/VerifyMobile">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-user"></i>
                                                </span>
                                            </div>
                                            <input type="email" class="form-control form-control-lg" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="username" required="required">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="ti-lock"></i>
                                                </span>
                                            </div>
                                            <input type="password" class="form-control form-control-lg" placeholder="Password" aria-label="Username" aria-describedby="basic-addon2" name="password" maxlength="10" required="required">
                                        </div>
                                         <c:if test="${error ne null}">
											<div class="alert alert-danger col-md-12" id="alertDanger" align="center">
												<c:out value="${error}" escapeXml="true" default="" />
											</div>
										</c:if>
										<c:if test="${msg ne null}">
											<div class="alert alert-success col-md-12" id="successAlert" align="center">
													<c:out value="${msg}" escapeXml="true" default="" />
											</div>
										</c:if>	
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <div class="col-xs-12 p-b-20">
                                                <button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</section>
	 <footer>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <div style="padding: 10px;">
                        <span>2018 &copy; Copyright | All Right Reserved</span>
                    </div>
                </div>
            </div>
        </footer>


        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
        <script type="text/javascript">
        	$(document).ready(function () {
        		
        	});
        </script>
	
</body>
</html>