<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Group | Bulk Register</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png" />
  
  <!-- App css -->
  
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
  
</head>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
   <%--  <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="${pageContext.request.contextPath}/resources/corporate/images/logo.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav">
                
        </ul>
        <ul class="navbar-nav navbar-nav-right">
         
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img src="images/faces/face1.jpg" alt="image">
              <span class="d-none d-lg-inline">Group</span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <!-- <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached mr-2 text-success"></i>
                Activity Log
              </a>
              <div class="dropdown-divider"></div> -->
              <a class="dropdown-item" href="${pageContext.request.contextPath}/Corporate/Logout">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Signout
              </a>
            </div>
          </li>
         
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav> --%>
    <!-- partial -->
    <div id="wrapper">
       <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
     <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
      
      <!-- partial -->
    <div class="content-page">
        <div class="content">
        <div class="container-fluid">
        
         <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Bulk Registration</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
        
          <div class="row">
            <div class="col-md-6 offset-md-3 grid-margin stretch-card">
              <div class="card text-black">
                <div class="card-body">
                  <h4 class="card-title">Bulk Registration</h4><h6 align="right" style="color: green;"><a href="${pageContext.request.contextPath}/Group/download/bulkregister">Click here to download format</a></h6>
                  
                  <form action="${pageContext.request.contextPath}/Group/BulkRegister" method="Post" enctype="multipart/form-data">
                    <div class="form-group">
                    	<input type="file" class="dropify" name="file"/>
                    </div>
                    <div class="form-group">
                      <center><button class="btn btn-primary" type="submit" id="uploadDoc" disabled>Submit</button></center>
                    </div>
                  </form>
                  
                  <center><h6 class="card-title" style="color: red;">${sucessMSG}</h6></center>
                </div>
              </div>
            </div>
          </div>
		</div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="#" target="_blank">Mss Payments Pvt. Ltd.</a> All rights reserved.</span>
            <!-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span> -->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
  <!-- <script src="js/hoverable-collapse.js"></script> -->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
  <!-- End custom js for this page-->
  
  
  <!-- responsive -->
  
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
  
  
  
  
  
  <script type="text/javascript">
    	$(document).ready(function(){

        	// demo.initChartist();
        	 $(".dropify").change(function() {
                 var filename = readURL(this);
                 $(this).parent().children('span').html(filename);
               });
 
               // Read File and return value  
               function readURL(input) {
                 var url = input.value;
                 var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                 console.log(ext);
                 if (input.files && input.files[0] && (
                   ext == "csv"
                   )) {
                   var path = $(input).val();
                   var filename = path.replace(/^.*\\/, "");
                   // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  // document.getElementById('uploadDoc').enabled = 'enabled';
                  $('button:submit').attr('disabled',false); 
                   
                   return "Uploaded file : "+filename;
                 } else {
                	  document.getElementById('uploadDoc').disabled = 'disabled';
                   $(input).val("");
                   return "Only csv format are allowed!";
                 }
               }

    	});
	</script>
  
</body>

</html>
