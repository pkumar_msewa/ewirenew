<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Ewire</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets-group/images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/icons.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets-group/css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/assets-group/css/custom.css">

        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
		
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <jsp:include page="/WEB-INF/jsp/Group/LeftMenu.jsp" />
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                        <jsp:include page="/WEB-INF/jsp/Group/Header.jsp" />
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper dashborad-v">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-title-box">
                                        <h4 class="page-title">Request List</h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <form class="" action="<c:url value="/Group/GroupRequestList"/>" method="post">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group bmd-form-group">
                                                                    <label for="reportrange" class="bmd-label-floating">Date</label>
                                                                    <input type="text" class="form-control" id="reportrange" name="reportrange" value="${dat}" readonly="readonly">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary btn-sm mt-4">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-6">
                                                    <form class="" action="<c:url value="/Group/SingleGroupRequest"/>" method="post" id="myForm">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group bmd-form-group">
                                                                    <label for="exampleInputEmail1" class="bmd-label-floating">Enter ContactNo</label>
                                                                   <!--  <input type="text" class="form-control" id="exampleInputEmail"> -->
                                                                    <input id="username" name="mobileNoId" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" />
				                           							<input id="remark" type="hidden" name="remark" value="request" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <button class="btn btn-primary btn-sm mt-4" onclick="getSingleUserDetails()" type="button">Search</button>
                                                                </div>                                                                 
                                                            </div>
                                                             <span id="err" style="color: red;"></span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                            <div class="">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                           <th>S. No.</th>
															<th>User details</th>												
															<th>Group Details</th>
															<th>Date</th>
															<th>Accept</th>
															<th>Reject</th>	
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                   <c:forEach items="${userData}" var="card" varStatus="loopCounter">
													<tr>
													<td>${loopCounter.count}</td>
													<td><b>Name:</b><c:out value="${card.userDetail.firstName} ${card.userDetail.lastName}" default="" escapeXml="true" /><br>
														<b>Contact No:</b> <c:out value="${card.username}" default="" escapeXml="true" /> <br/>
														<b>Email Id</b> <c:out value="${card.userDetail.email}" default="" escapeXml="true" /></td>
													<td><b>Group Name:</b><c:out value="${card.groupDetails.groupName}" default="" escapeXml="true" /><br/>
													<b>Remark:</b><c:choose><c:when test="${card.userDetail.remark eq null}">
													<c:if test="${card.userDetail.remark eq null}"><c:out value="-"></c:out></c:if></c:when>									
													<c:otherwise><c:out value="${card.userDetail.remark}" default="" escapeXml="true" /></c:otherwise>
													</c:choose></td>
													<td><fmt:formatDate pattern="dd-MM-yyyy" value="${card.created}" /></td>
													<td>
														<input type="hidden" id="counting" value="${count.count}" />
														<a onclick="accept('${card.username}');">
														<button class="btn btn-success" id="accept">Accept</button> </a>
													</td>									
													<td>
													<input type="hidden" value="${slist.contactNo}" name ="mobileNo" id ="mobileNo">
													<a onclick="rejectionReason('${card.username}');">
													<button class="btn btn-danger" id="reject">Reject</button></a></td>
												</tr>		 
												</c:forEach>                                               
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div> 

                        </div>
                        <!-- container -->

                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    &copy; <script type="text/javascript">document.write(new Date().getFullYear())</script> MSS Payments Pvt. Ltd.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->
        
         <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_success_msg" class="alert alert-success"></center></B>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_error_msg" class="alert alert-danger"></center></B>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="myModalReject" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel" >
                    Rejection Reason
                </h4>
            </div>
            
          
            <div class="modal-body">
                
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="address3">Reason</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="address3" placeholder="Reason" name="address3" required/>
                        <span class="error" style="color: red;" id="rejectreasonError2"></span>
                    </div>
                  </div>
                  <input type="hidden" id="rejectContact" name="rejectContact" />                 
                </form>
            </div>
            
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary" onclick="reject()">
                    Add Reason
                </button>
            </div>
        </div>
    </div>
</div>


        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/bootstrap-material-design.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/jquery.scrollTo.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/carousel/owl.carousel.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/plugins/peity/jquery.peity.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/pages/dashborad.js"></script>

        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/4/core.js"></script>
        <script src="https://www.amcharts.com/lib/4/charts.js"></script>
        <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

        <!-- Datatable -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/assets-group/js/custom.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        

        <script>
            $(document).ready(function() {
                var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'pdf', 'csv' ]
                } );
             
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            } );
        </script>
        
           <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		<script>
		function rejectionReason(username) {
			$("#rejectContact").val(username);
			$("#myModalReject").modal("show");
		}
		
		
		
	function accept(username) {
		console.log("username:: "+username);
		$.ajax({
			type: "POST",
    		datatype : 'json',
    		contentType: "application/json",
			url: "${pageContext.request.contextPath}/Group/AcceptGroup/"+username,
			data: JSON.stringify({
				"contactNo" : username				
			}),
			success: function (response) {
				console.log("success");
				if(response.code.includes("S00")) {
					
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				} else {
					
					$("#common_error_true").modal("show");
		  			$("#common_error_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				}
			}
		});
	}
	
	function reject() {
		$("#rejectreasonError").html("");
		var username = $("#rejectContact").val();
		var reason = $("#address3").val();
		var valid = true;
		if(reason.length <= 0) {
			$("#rejectreasonError2").html("please enter rejection reason");
			valid = false;		
		}
		if(valid) {
		$.ajax({
			type : "POST",
			datatype : 'json',
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/Group/RejectGroup",
			data: JSON.stringify({
				"username" : username,
				"status" : reason
			}),
			success: function (response) {
				console.log("success");
				$("#myModalReject").modal("hide");
				if(response.code.includes("S00")) {
					
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				} else {
					
					$("#common_error_true").modal("show");
		  			$("#common_error_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				}
			}
		});
		}
	}
	</script>
		
	<script>
	function getSingleUserDetails() {
		var username=$("#username").val();
		var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true) {
			$("#myForm").submit();
		}
	}

</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    </body>

</html>