<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">             
                <li class="menu-title">Navigation</li>
                <p class="menu-title">Welcome ${name}</p>
                <li>
                    <a href="${pageContext.request.contextPath}/Group/Home">
                        <i class="fa fa-home"></i> <span>Dashboard</span>
                    </a>
                </li>               
                
                <li>
                	<a href="javascript: void(0);"><i class="fa fa-user-secret"></i><span>Group Services</span><span class="menu-arrow"></span></a>
                	<ul class="nav-second-level" aria-expanded="false">                		
                		<li><a href="${pageContext.request.contextPath}/Group/GroupRequestList">Group Request List</a></li>
                		<li><a href="${pageContext.request.contextPath}/Group/AcceptedUserGroup">Approved User List</a></li>
                		<li><a href="${pageContext.request.contextPath}/Group/RejectedUserGroup">Rejected User List</a></li>
                	</ul>
                </li>
                
                  <li>
                	 <a href="javascript: void(0);"><i class="fa fa-building-o"></i><span>Bulk Register</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="${pageContext.request.contextPath}/Group/BulkRegister">Bulk Register</a></li>                       
                    </ul>
                </li>
                
                 <li>
                	 <a href="javascript: void(0);"><i class="fa fa-building-o"></i><span>Bulk SMS</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="${pageContext.request.contextPath}/Group/BulkSMS">Bulk SMS</a></li> 
                         <li><a href="${pageContext.request.contextPath}/Group/BulkUploadSmsList">Bulk SMS History</a></li>                       
                    </ul>
                </li>
                
                <li>
                <a href="javascript: void(0);"><i class="fa fa-user-secret"></i><span>Notification Center</span><span class="menu-arrow"></span></a>
                	<ul class="nav-second-level" aria-expanded="false">                		
                		<li><a href="${pageContext.request.contextPath}/Group/SendGroupSms">Send Notification</a></li>                		
                	</ul>
                </li>
               
                
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>