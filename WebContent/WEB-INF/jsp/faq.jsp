<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>EWire | FAQs</title>

    <!-- Favicon -->
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.png">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .wellcome_area {
            height: 74vh;
        }
        .faq-wrapper .card-header {
            padding: .75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgb(255, 255, 255);
            border-bottom: none;
            border-radius: 10px;
            color: #291859;
        }
        .faq-wrapper .card-body {
            color: #000;
        }
        .faq-wrapper .card {
            border: none;
            -webkit-box-shadow: 0 10px 90px rgba(0, 0, 0, 0.08);
            box-shadow: 0 10px 90px rgba(0, 0, 0, 0.08);
            margin-bottom: 10px;
            border-radius: 10px;
        }
        .faq-wrapper .card .card-header:after {
            font-family: 'FontAwesome';
            content: '\f01a';
            float: right;
            color: #291859;
        }
        .faq-wrapper .card .card-header .collapsed:after {
            content: '\f062';
        }
    </style>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Signup</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
               <%--  <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="${pageContext.request.contextPath}/User/SignUp">Sign Up</a>
                    </div>
                </div>
 --%>            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Special Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-center">
                        <h2>Frequently Asked Questions</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Special Description Area -->
        <div class="special_description_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-1 col-xl-10">
                        <div class="faq-wrapper">
                            <div id="accordion">
                              <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <span class="mb-0">
                                    How do I sign up for Cashier Card ?
                                  </span>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Download the <strong>CASHIER</strong> App and open the app on your mobile phone.</p>

                                    <p>Complete the sign up process by following steps:</p>
                                    <ol type="1">
                                        <li>Click on 'Register' button</li>
                                        <li>Enter in your mobile number</li>
                                        <li>Fill details i.e. full name, Date of Birth and Email. Click on next button.</li>
                                        <li>Set password and enter your  ID proof details. </li>
                                        <li>Click on 'Register'  button.</li>
                                        <li>You will get an OTP on the mobile number given.</li>
                                        <li>Enter the OTP in the app and then click on 'Submit', </li>
                                        <li>Generate your virtual card</li>
                                    </ol>
                                    <p>You're ready to use cashier app and virtual card link with cashier.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  <span class="mb-0">
                                    Where can I download the app and is it free ?
                                  </span>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>You can Download the app free of cost from google play store or iOS app store.</p>
                                  </div>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header collapsed" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  <span class="mb-0">
                                    Is my credit/debit card information completely safe on your App ?
                                  </span>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Yes, you can be rest assured about the safety of your credit/debit card information. We have the highest level of security measures on our platform which complies  to the latest technology on data security. We do not save any details of the users debit/credit card from our end.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                  <span class="mb-0">
                                    Why Cashier Card ?
                                  </span>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                  <div class="card-body">
                                    <ol type="1">
                                        <li>Cashier offers a simple  process to make  payments and while you do this, you will be rewarded with unique reward points system on every transaction.</li>
                                        <li>No separate setup/ installation or registration is required. Any customer with cashier app can create Virtual Card or request for Physical card.</li>
                                        <li>Can be used at any platform for making the payment online/offline.</li>
                                        <li>Virtual card allows user to make any kind of online payments.</li>
                                        <li>Loyalty points and cash backs will be accumulated in this 1 card itself.</li>
                                        <li>User can withdraw money from any ATM like any normal debit or credit card.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                  <span class="mb-0">
                                    I am not able to get the One Time Password (OTP), What should I do ?
                                  </span>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>You  can click on the resend  button . and in case if you still not able to then you can write to us at <a href="mailto:info@madmoveglobal.com">info@madmoveglobal.com</a> and our team will be glad to help you.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                  <span class="mb-0">
                                    I am not able to complete registration, What should I do ?
                                  </span>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Even after multiple attempts if you are not able to register, please write to us at <a href="mailto:info@madmoveglobal.com">info@madmoveglobal.com</a> with your number and email address and our team will get back to you.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                  <span class="mb-0">
                                    How can I load Money ?
                                  </span>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Following simple steps have to be taken to load money into your wallet:</p>
                                    <ol type="1">
                                        <li>Click on the 'load money tab' on the screen.</li>
                                        <li>Select the amount you want to load.</li>
                                        <ol type="i">
                                            <p>Select from any of the following options to load money into Cashier Card:</p>
                                            <li>Credit Card/ Debit card</li>
                                            <li>UPI</li>
                                            <li>Net Banking</li>
                                        </ol>
                                        <li>Follow the process that your bank / card issuer asks in order to complete the transaction.</li>
                                        <li>The amount will instantly reflect in your Cashier Balance.</li>
                                        <li>You can save the details for faster upload from the next time.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingEight" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                  <span class="mb-0">
                                    I have forgotten my password, What do I do ?
                                  </span>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>To reset the password, please follow the steps given below:</p>
                                    <ol type="1">
                                        <li>On the login screen, you need to click on the "Forgot Password" link.</li>
                                        <li>Enter your registered mobile number</li>
                                        <li>Enter the OTP received on registered mobile number</li>
                                        <li>Set a new password.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingNine" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                  <span class="mb-0">
                                    How can I apply for cashier physical card?
                                  </span>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>To apply for a physical Card, kindly follow the steps below:</p>
                                    <ol type="1">
                                        <li>Open your  Cashier App.</li>
                                        <li>Tap on the Navigation icon at the top left corner of your screen</li>
                                        <li>Click on the physical card</li>
                                        <li>Click on (+) icon on screen to generate your physical card.</li>
                                        <li>Pop up occurs asking to have minimum balance of Rs.500.</li>
                                        <li>Feed in your delivery address and click on 'Proceed to Pay'</li>
                                        <li>Your order will be placed and you will receive a confirmation message.</li>
                                    </ol>
                                    <strong>You will receive your card within 15 days or less depending upon your location.</strong>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingTen" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                  <span class="mb-0">
                                    How can I activate my Card?
                                  </span>
                                </div>
                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Once your Card has been delivered to you, it can be activated by Proxy number. Follow the steps below.</p>
                                    <ol type="1">
                                        <li>Launch your  Cashier App</li>
                                        <li>Tap on navigation icon at the top left corner of your screen.</li>
                                        <li>Click on the physical card</li>
                                        <li>Choose the option 'Activate Card' (In case your card is already activated, you will not see this option)</li>
                                        <li>You will be prompted to enter your proxy no. (Proxy Number)</li>
                                        <li>On entering your proxy no., you will be prompted to enter activation code received on your registered no.</li>
                                        <li>Enter the number, your card is ready to use now.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingEleven" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                  <span class="mb-0">
                                    I have forgotten my PIN. How do I reset it?
                                  </span>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Your PIN will be available to you only in case you have ordered a physical card. In case you would like to reset your PIN, please follow the steps below</p>
                                    <ol type="1">
                                        <li>Launch your Cashier App</li>
                                        <li>Tap on the Navigation icon at the top left corner of your screen</li>
                                        <li>Click on the physical card</li>
                                        <li>Choose the option 'Reset  PIN'.</li>
                                        <li>You will receive new pin on your registered number.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingTwelve" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                  <span class="mb-0">
                                    How do I block my card? 
                                  </span>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Please call customer care to get your card blocked.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingThirteen" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                  <span class="mb-0">
                                    How do I unblock my card? 
                                  </span>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Please call customer care to get your card unblocked</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingFourteen" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                                  <span class="mb-0">
                                    What are the charges for ordering a Card?
                                  </span>
                                </div>
                                <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>For the purpose of generating physical card you need to have a minimum balance of Rs.500 and while the time of applying for the card Rs.200 will be deducted as card charges</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingFifteen" data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                                  <span class="mb-0">
                                    Can I have more than 1 card at a time?
                                  </span>
                                </div>
                                <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>No, you can't have more than one card at a time. Once you generate physical card, virtual card gets deactivated.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingSixteen" data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                                  <span class="mb-0">
                                    What happens to my balance when I opt for Physical card?
                                  </span>
                                </div>
                                <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Your balance gets automatically transferred to physical card from virtual card.</p>
                                  </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header collapsed" id="headingSeventeen" data-toggle="collapse" data-target="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                                  <span class="mb-0">
                                    How do i view my card details?
                                  </span>
                                </div>
                                <div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordion">
                                  <div class="card-body">
                                    <p>Please follow the steps below to view your card details</p>
                                    <ol>
                                        <li>Log in into Cashier App.</li>
                                        <li>You can see your card details on home page itself.</li>
                                    </ol>
                                  </div>
                                </div>
                              </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Special Area End ***** -->

    <!-- ***** CTA Area Start ***** -->
    <section class="our-monthly-membership section_padding_50 clearfix">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm-12">
                    <div class="membership-description">
                        <h2>In the palm of your hands</h2>
                        <p>Download our App Now.</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <!-- <div class="get-started-button wow bounceInDown" data-wow-delay="0.5s">
                        <a href="#">Get Started</a>
                    </div> -->
                    <center>
                        <div class="app-download-area">
                            <div class="mss-app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                <!-- Google Store Btn -->
                                <a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/resources/User/NewUi/img/app.png" class="img-responsive store_btn"></a>
                            </div>
                            <div class="mss-app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                                <!-- Apple Store Btn -->
                                <a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/resources/User/NewUi/img/play.png" class="img-responsive store_btn"></a>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** CTA Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon footer-social text-center section_padding_70 clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <p><b>COMPANY</b></p>
                    <a href="${pageContext.request.contextPath}/Aboutus"><p>About Us</p></a>
                    <a href="${pageContext.request.contextPath}/TermsnConditions"><p>Terms and Conditions</p></a>
                    <a href="${pageContext.request.contextPath}/PrivacyPolicy"><p>Privacy Policy</p></a>
                </div>
                <div class="col-md-3">
                    <p><b>RESOURCES</b></p>
                    <a href="javascript:void(0);"><p>Help &amp; Support</p></a>
                    <a href="${pageContext.request.contextPath}/TermsnConditions"><p>FAQs</p></a>
                </div>
                <div class="col-md-6">
                    <div class="footer-text">
                        <a href="${pageContext.request.contextPath}/Home">
                            <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid" style="width: 180px;">
                        </a>
                    </div>
                    <div class="copyright-text">
                        <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
                        <p>2017 &copy; Copyright EWire.</p>
                    </div>
                    <!-- social icon-->
                    <div class="footer-social-icon">
                        <a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="javascript:void(0);"> <i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="${pageContext.request.contextPath}/resources/js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script>
</body>

</html>
