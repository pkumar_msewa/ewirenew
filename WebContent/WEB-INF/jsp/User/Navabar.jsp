<nav class="navbar navbar-white navbar-fixed-top">
  <div class="container" style="padding-left: 30px; padding-right: 30px;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/User/Login/Process">
      	<img src="${pageContext.request.contextPath}/resources/assets-newui/img/logo.png">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="javascript:void(0);" style="padding-top: 3px; padding-bottom: 1px;">
          <div class="usr-img row">
            <div class="col-sm-3 text-center" style="padding-left: 0;">
              <img src="${pageContext.request.contextPath}/resources/newui/assets/img/user.png">
            </div>
            <div class="col-sm-9">
              <strong class="usr-nme">${user.userDetail.firstName}</strong><br>
              <strong>Balance:</strong>&nbsp;<i class="fa fa-inr"></i>&nbsp;<span id="cardBalance" class="wall_mny"></span>&nbsp;&nbsp;
              <span tooltip="Load Money to Wallet" flow="down" data-toggle="modal" data-target="#loadMny">
                <img src="${pageContext.request.contextPath}/resources/assets-newui/img/load.png" style="width: 18px; position: absolute;">
              </span>
            </div>
          </div>
        </a></li>
        <!-- <li><a href="${pageContext.request.contextPath}/Aboutus">ABOUT US</a></li>
        <li><a href="${pageContext.request.contextPath}/Offers">OFFERS</a></li> -->
        <!-- <li><a href="#">REFER &amp; EARN</a></li>  -->
        <li class="dropdown">
        	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
        	<ul class="dropdown-menu">
        		<div class="drop_wrp">
        			<!-- <div class="row">
        				<div class="col-md-4 col-sm-4 col-xs-4">
        					<div class="usr-img">
        						<img src="${pageContext.request.contextPath}/resources/assets-newui/img/user.svg" class="img-responsive">
        					</div>
        				</div>
        				<div class="col-md-8 col-sm-8 col-xs-8" style="padding-left: 8px; padding-right: 8px;">
        					<div class="usr-dtls">
        						<span class="usr-nme"><b>${user.userDetail.firstName}</b></span><br>
        						<i class="fa fa-phone"></i>&nbsp;<small>${user.userDetail.contactNo}</small><br>
        						<i class="fa fa-envelope-o"></i>&nbsp;<small>${user.userDetail.email}</small><br>
        					</div>
        				</div>
        			</div> -->
        			<!-- <div class="row">
        				<div class="col-sm-12 load_wrap">
        					<div class="col-sm-6 pd-0 bd-r">
        						<small>Available Balance</small><br>
        						<i class="fa fa-inr"></i>&nbsp;<span id="cardBalance" class="wall_mny"></span>
        					</div>
        					<div class="col-sm-6 pd-0">
                    <button class="btn btn-primary" style="margin-top: 0%; margin-left: -1%;" data-toggle="modal" data-target="#loadMny">&nbsp; Load Money</button>
        					</div>
        				</div>
        			</div> -->
              <div class="row">
                <div class="col-sm-12 load_wrap">
                  <div class="col-sm-3 pd-0 bd-r">
                    <div class="usr-im/g">
                      <img src="${pageContext.request.contextPath}/resources/assets-newui/img/user.svg" class="img-responsive">
                    </div>
                  </div>
                  <div class="col-sm-9 pd-0">
                    <!-- <button class="btn btn-primary" style="margin-top: 0%; margin-left: -1%;" data-toggle="modal" data-target="#loadMny">&nbsp; Load Money</button> -->
                    <div class="usr-dtls">
                      <i class="fa fa-phone"></i>&nbsp;<small>${user.userDetail.contactNo}</small><br>
                      <i class="fa fa-envelope-o"></i>&nbsp;<small>${user.userDetail.email}</small><br>
                    </div>
                  </div>
                </div>
              </div>
        			<div class="row">
        				<div class="col-sm-12">
        					<!-- <div class="prof-btn">
        						<a href="${pageContext.request.contextPath}/User/MyProfile" class="btn btn-block btn-custom" type="button">View Profile</a>
        					</div> -->
        					<li><a href="${pageContext.request.contextPath}/User/Expenses"><i class="fa fa-file-text-o"></i> Transaction Statement</a></li>
<%--   		        			<li><a href="${pageContext.request.contextPath}/User/ImpsService"><i class="fa fa-university"></i> Bank Transfer</a></li>
 --%> 		        			<li><a target="_blank" href="${pageContext.request.contextPath}/Offers"><i class="fa fa-ticket"></i> Offers</a></li> 
			                  <li><a href="${pageContext.request.contextPath}/Aboutus"><i class="fa fa-info-circle"></i> About Us</a></li>
			                  <li><a href="${pageContext.request.contextPath}/User/MyProfile"><i class="fa fa-user"></i> View Profile</a></li>
			                  <li><a href="#" data-toggle="modal" data-target="#restpn"><i class="fa fa-key"></i> Reset PIN</a></li>
		        			<li><a href="${pageContext.request.contextPath}/User/Login/Logout"><i class="fa fa-power-off"></i> Sign out</a></li>
		        			<hr style="margin-top: 10px; margin-bottom: 10px;">
 		        			<a href="#" class="help" style="float: right; font-size: 11px;"><span class="fa fa-life-buoy"></span>&nbsp;Help &amp; Support</a>
        				</div>
        			</div>
        		</div>
        	</ul>
        </li>
      </ul>
    </div>
  </div>
</nav>



<!-- modal for load money -->
<div id="loadMny" class="modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Amount to Card</h4>
      </div>
      <div class="modal-body">
        <div class="row">
         <div class="col-sm-12 col-xs-12">
          <div class="add_mnyWrp">
           <form action="${pageContext.request.contextPath}/Api/v1/User/web/en/LoadMoney/InitiateRequestWeb" method="post" id="loadForm">
            <div class="form-group">
             <label for="">Enter Amount</label>
             <input type="text" id="amount" name="amount" class="form-control" placeholder="0.00" maxlength="4" onkeypress="return isNumberKey(event)" required="required">
           		<p id="errorAmount" style="color: red"></p>
           		
            </div>
            <div class="form-group">
              	<label class="radio-inline"><input type="radio" name="ua" checked="checked" value="false"><img src="${pageContext.request.contextPath}/resources/images/debitNet.png" class="img-fluid" style="width: 90%"></label><br><br><br>
				<label class="radio-inline"><input type="radio" name="ua" value="true"><img src="${pageContext.request.contextPath}/resources/images/UPIldpi.png" class="img-fluid" style="width: 40%"></label>
            </div>
            <center><button type="button" onclick="checkloadValidation()" class="btn btn-primary">Proceed</button></center>
           </form>
          </div>
         </div>
        </div>
      </div>
    </div>

  </div>
</div>

<script>
function checkloadValidation(){
	var valid = true;
	var amount=$("#amount").val();
	if(amount.length  <= 0){
		$("#errorAmount").html("Please enter the amount");
		valid = false;
	}
	if(valid == true){
		$("#loadForm").submit();
	}
	var timeout = setTimeout(function(){
		$("#errorAmount").html("");
	}, 3000);
}
</script>