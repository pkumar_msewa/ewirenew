<!DOCTYPE html>
<html>
<head>
	<title>Success</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">


	<style>
		body {
			background: #efefef;
			font-family: 'Open Sans', sans-serif;
		}
		.response_stats {
			border: 2px solid #efefef;
			background: #fff;
			box-shadow: 1px 1px 17px 5px #d4d2d2;
			border-radius: 5px;
			padding: 15px 20px;
			margin-top: 30%;
		}
		.response_head h4 {
			font-size: 30px;
			font-weight: 600;
			color: #488c0b;
		}
		.response_body img {
			width: 36%;
		}

		@media screen and (max-width: 768px) {
			.response_head h4 {
				font-size: 24px;
			}
		}
		@media screen and (max-width: 320px) {
			.response_head h4 {
				font-size: 20px;
			}
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
				<div class="response_stats text-center">
					<div class="response_head">
						<h4>Transaction Successful</h4>
					</div>
					<div class="response_body">
						<img src="${pageContext.request.contextPath}/resources/images/success.gif">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- script starts here -->

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>