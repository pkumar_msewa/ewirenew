<section>
		<div class="container">
			<div class="footer">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<span>All Rights Reserved. &copy; EWire Pvt. Ltd.</span>
						</div>
						<div class="col-md-6 text-right">
							<span class="footer-links"><a href="${pageContext.request.contextPath}/PrivacyPolicy" target="_blank">Privacy Policy</a><a href="#">FAQ</a><a href="${pageContext.request.contextPath}/TermsnConditions">Terms &amp; Conditions</a><a href="${pageContext.request.contextPath}/User/Login/Process">Home</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>