<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Cashier | Contact Us</title>

    <!-- Favicon -->
  <link rel="icon" href="${pageContext.request.contextPath}/resources/img/core-img/favicon.png">

    <!-- Core Stylesheet -->
    <link href="${pageContext.request.contextPath}/resources/css/Style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/custom.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">

    <style>
        .wellcome_area {
            height: 74vh;
        }
    </style>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
   <header class="header_area1 animated sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-12">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="${pageContext.request.contextPath}/Home">
                                <img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <!-- <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/Home">Home</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Aboutus">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/faq">FAQs</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/ContactUs">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/Login">Login</a></li>
                                    <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/User/SignUp">Signup</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <!-- <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="${pageContext.request.contextPath}/User/SignUp">Sign Up</a>
                    </div>
                </div> -->
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Special Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-center">
                        <h2>Need help? Let us know</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Special Description Area -->
        <div class="special_description_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5 ml-xl-auto">
                        <div class="single-special">
                            <div class="row">
                                <div class="col-3">
                                    <div class="special_description_content">
                                        <div class="support-email">
                                            <img src="${pageContext.request.contextPath}/resources/img/team-img/email.png" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-9">
                                    <h2 style="font-size: 25px;">Mail</h2>
                                    <p>For any support-related queries,email us at:</p>
                                    <p><a href="mailto:">care@madmoveglobal.com</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 ml-xl-auto">
                        <div class="single-special">
                            <div class="row">
                                <div class="col-3">
                                    <div class="special_description_content">
                                        <div class="support-email">
                                            <img src="${pageContext.request.contextPath}/resources/img/team-img/telephone.png" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-9">
                                    <h2 style="font-size: 25px;">Phone</h2>
                                    <p>Call us between 10 AM - 7 PM all days, except public holidays</p>
                                    <p><a href="tel:">+91-7259195449</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Special Area End ***** -->

    <!-- ***** CTA Area Start ***** -->
    <section class="our-monthly-membership section_padding_50 clearfix">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="membership-description">
                        <h2>In the palm of your hands</h2>
                        <p>Download our App Now.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- <div class="get-started-button wow bounceInDown" data-wow-delay="0.5s">
                        <a href="#">Get Started</a>
                    </div> -->
                    <center>
                        <div class="app-download-area">
                            <div class="mss-app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                <!-- Google Store Btn -->
                                <a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/resources/User/NewUi/img/app.png" class="img-responsive"></a>
                            </div>
                            <div class="mss-app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                                <!-- Apple Store Btn -->
                                <a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/resources/User/NewUi/img/play.png" class="img-responsive"></a>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** CTA Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon footer-social text-center section_padding_70 clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <p><b>COMPANY</b></p>
                    <a href="${pageContext.request.contextPath}/Aboutus"><p>About Us</p></a>
                    <a href="${pageContext.request.contextPath}/TermsnConditions"><p>Terms and Conditions</p></a>
                    <a href="${pageContext.request.contextPath}/PrivacyPolicy"><p>Privacy Policy</p></a>
                </div>
                <div class="col-md-3">
                    <p><b>RESOURCES</b></p>
                    <a href="javascript:void(0);"><p>Help &amp; Support</p></a>
                    <a href="${pageContext.request.contextPath}/faq"><p>FAQs</p></a>
                </div>
                <div class="col-md-6">
                    <div class="footer-text">
                        <a href="${pageContext.request.contextPath}/Home"><img src="${pageContext.request.contextPath}/resources/User/assets/images/logo1.png" class="img-fluid" style="width: 180px;"></a>
                    </div>
                    <div class="copyright-text">
                        <p>2017 &copy; Copyright EWire. </p>
                    </div>
                    <!-- social icon-->
                    <div class="footer-social-icon">
                        <a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="javascript:void(0);"> <i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <!-- <script src="js/footer-reveal.min.js"></script> -->
    <!-- Active JS -->
    <script src="${pageContext.request.contextPath}/resources/js/active.js"></script></body>

</html>
