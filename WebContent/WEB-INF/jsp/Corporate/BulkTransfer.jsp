
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Coporate Dashboard</title>
<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.png">
<!-- Bootstrap core CSS     -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css"
	rel="stylesheet" />
<!-- Animation library for notifications   -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css"
	rel="stylesheet" />
<!--  Paper Dashboard core CSS    -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css"
	rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css"
	rel="stylesheet" />
<!--  Fonts and icons     -->
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"
	rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300'
	rel='stylesheet' type='text/css'>
<link
	href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
	rel="stylesheet" type="text/css" />



<style>
.fileUpload {
	position: relative;
	overflow: hidden;
	height: 43px;
	margin-top: 0;
}

.fileUpload input.uploadlogo {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity = 0);
	width: 100%;
	height: 42px;
}
</style>
</head>
<body>

	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>

		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid"></div>
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div class="card ">
							<div class="header">
								<h3 class="title">
									<B>Bulk Amount Transfer</b>
								</h3>
							</div>
							<div class="content">
								<form id="bulkfom">
									<div class="fileUpload blue-btn btn width100">
										<span>Please Upload your Sheet</span> <input type="file"
											class="uploadlogo" name="aadharImage" />
									</div>
									<button type="button" id=" ">Submit</button>
								</form>
								<a
									href="${pageContext.request.contextPath}/Corporate/download/bulktransfer">Click
									here to download format</a>
							</div>
						</div>
					</div>

				</div>

				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<h3 class="page-title float-left">Upload History</h3>
						</div>
					</div>
					<!-- end row -->


					<div class="row">
						<div class="col-sm-12">
							<div class="card-box table-responsive">
								<hr>
								<table id="datatable"
									class="table table-striped table-bordered dt-responsive nowrap"
									cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sl No</th>
											<th>User Name</th>
											<th>User Email</th>
											<th>Amount</th>
											<th>Transfer Date</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cardTransList}" var="card"
											varStatus="loopCounter">
											<tr>
												<td>${loopCounter.count}</td>
												<td><c:out value="${card.name}" default=""
														escapeXml="true" /></td>
												<td><c:out value="${card.email}" default=""
														escapeXml="true" /></td>
												<td><c:out value="${card.amount}" default=""
														escapeXml="true" /></td>
												<td><c:out value="${card.transactionDate}" default=""
														escapeXml="true" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="Footer.jsp"></jsp:include>
	</div>

	<script>
		function submitform() {
			alert("your request for bulk amount transfer has been processed");

		}
	</script>
</body>

<!--   Core JS Files   -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/jquery-1.10.2.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap.min.js"
	type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script
	src="${pageContext.request.contextPath}/resources/corporate/assets/js/demo.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		demo.initChartist();
		$(".uploadlogo").change(function() {
			var filename = readURL(this);
			$(this).parent().children('span').html(filename);
		});

		// Read File and return value  
		function readURL(input) {
			var url = input.value;
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0] && (ext == "csv")) {
				var path = $(input).val();
				var filename = path.replace(/^.*\\/, "");
				// $('.fileUpload span').html('Uploaded Proof : ' + filename);
				document.getElementById('uploadDoc').enabled = 'enabled';
				return "Uploaded file : " + filename;
			} else {
				document.getElementById('uploadDoc').disabled = 'disabled';
				$(input).val("");
				return "Only csv format are allowed!";
			}
		}

	});
</script>

</html>

