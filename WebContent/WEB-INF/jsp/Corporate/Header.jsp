<nav
	class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success">
	<div
		class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo"
			href="${pageContext.request.contextPath}/Corporate/Home"><img
			src="${pageContext.request.contextPath}/resources/corporate/images/logo.png"
			alt="logo" /></a> <a class="navbar-brand brand-logo-mini"
			href="${pageContext.request.contextPath}/Corporate/Home"><img
			src="${pageContext.request.contextPath}/resources/corporate/images/logo.svg"
			alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-stretch">
		<button class="navbar-toggler navbar-toggler align-self-center"
			type="button" data-toggle="minimize">
			<span class="mdi mdi-menu"></span>
		</button>

		<ul class="navbar-nav navbar-nav-right">
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle nav-profile" id="profileDropdown"
				href="#" data-toggle="dropdown" aria-expanded="false"> <img
					src="images/faces/face1.jpg" alt="image"> <span
					class="d-none d-lg-inline">Corporate</span>
			</a>
				<div class="dropdown-menu navbar-dropdown w-100"
					aria-labelledby="profileDropdown">

					<a class="dropdown-item"
						href="${pageContext.request.contextPath}/Corporate/Logout"> <i
						class="mdi mdi-logout mr-2 text-primary"></i> Signout
					</a>
				</div></li>
		</ul>
		<button
			class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
			type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>