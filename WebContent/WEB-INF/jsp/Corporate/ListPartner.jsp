<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | List Partners</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />


<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
<style>
modal-body .form-horizontal .col-sm-2, .modal-body .form-horizontal .col-sm-10
	{
	width: 100%
}

.modal-body .form-horizontal .control-label {
	text-align: left;
}

.modal-body .form-horizontal .col-sm-offset-2 {
	margin-left: 15px;
}

.select2-container {
	width: 426.97917px !important
}
</style>
<script type="text/javascript">
	var context_path = "${pageContext.request.contextPath}";
</script>
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />
		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<p align="center" id="errorMesg" style="color: green;"></p>
									<div class="row">
										<div class="col-8">
											<h4 class="card-title">Partners</h4>
										</div>
									</div>
									<div class="tabble">
										<table id="example"
											class="table table-striped table-bordered dt-responsive nowrap"
											style="width: 100%">
											<thead>
												<tr>
													<th>Sl No.</th>
													<th>Partner Name</th>
													<th>email</th>
													<th>mobile</th>
													<th>Date Created</th>
													<th>Load Card Max Limit</th>
													<th>Services Assigned</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${partnerDetailsList}" var="partner"
													varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td><c:out value="${partner.partnerName} " default=""
																escapeXml="true" /></td>
														<td><a
															href="${pageContext.request.contextPath}/Corporate/getPartnerTransaction?partner=${partner.id}">
																${partner.partnerEmail}</a></td>
														<td><a href="#"><c:out
																	value="${partner.partnerMobile}" default=""
																	escapeXml="true" /></a></td>
														<td><c:out value="${partner.created}" default=""
																escapeXml="true" /></td>
														<td>${partner.loadCardMaxLimit}</td>
														<input type="hidden" id="hiddenId" name="hiddenId"
															value="" />
														<td style="color: purple;"><a href=""
															data-toggle="modal" data-target="#myModalHorizontal"
															data-book-id="${partner.id}"> <c:forEach
																	items="${partner.partnerServices}" var="partner1"
																	varStatus="loopCounter">

																	<c:out value="${partner1.description}" default=""
																		escapeXml="true" />

																</c:forEach></td>
														</a>
														<td><a
															href="${pageContext.request.contextPath}/Corporate/inactivePartner?partner=${partner.partnerUser.id}"><button
																	type="button" class="btn btn-primary">Delete</button></a></td>
													</tr>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- MODAL STARTS -->

	<div class="modal fade" id="myModalHorizontal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Edit Services</h4>
				</div>

				<!-- Modal Body -->
				<div class="modal-body">

					<form class="form-horizontal" role="form">
						<div class="">
							<div class="form-group">
								<!-- <label for="mobile">Multiple Select</label> -->
								<select class="multi-select" name="services" multiple="multiple"
									id="services">
									<c:forEach items="${serviceExcluded}" var="services"
										varStatus="loopCounter">
										<option value="${services.code}" selected="selected">${services.description}</option>

									</c:forEach>

									<c:forEach items="${serviceExcluded}" var="operators"
										varStatus="loopCounter">
										<option value="${operators.code}">${operators.description}</option>

									</c:forEach>

								</select>
							</div>
						</div>
					</form>
				</div>

				<!-- Modal Footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						Close</button>
					<button type="button" class="btn btn-primary" id="action">
						Update Services</button>
				</div>
			</div>
		</div>
	</div>


	<!-- MODAL ENDS -->


	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<%-- <script src="${pageContext.request.contextPath}/resources/corporate/js/data-table.js"></script> --%>


	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script>
	<!-- endinject -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>


	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->


	<script>
		$(document).ready(
				function() {
					var table = $('#example').DataTable({
						lengthChange : false,
						buttons : [ 'excel', 'csv' ]
					});

					table.buttons().container().appendTo(
							'#example_wrapper .col-md-6:eq(0)');
				});
	</script>
	<script>
		$(document).ready(function() {
			$('.multi-select').select2();
			var mesg = '${mesg}';
			$('#errorMesg').html(mesg);

			setTimeout(function() {
				$('#errorMesg').html('');
			}, 3000);
		});
	</script>

	<script type="text/javascript">
		var agentId;
		$('#myModalHorizontal').on('show.bs.modal', function(e) {
			console.log("i am here");
			var bookId = $(e.relatedTarget).data('book-id');
			console.log("this is the book Id:" + bookId);
			$(e.currentTarget).find('input[name="hiddenId"]').val(bookId);
			agentId = bookId;
		});
		$("#action")
				.click(
						function() {
							var servicesList = $("#services").val();
							var realvalues = new Array();//storing the selected values inside an array
							$('#services :selected').each(
									function(i, selected) {
										realvalues[i] = $(selected).val();
									});
							console.log("ServiceList:::" + servicesList);
							console.log("realValues:::" + realvalues);
							console.log("agent:::" + agentId);
							$
									.ajax({
										type : "POST",
										url : "${pageContext.request.contextPath}/Corporate/EditPartnerServices",
										dataType : "json",
										data : {
											'id' : agentId,
											'services' : "" + servicesList + ""
										},

										success : function(data) {
											console.log(data.code);
											setTimeout(
													"$('#myModalHorizontal').modal('hide');",
													30000);

											window.location.href = "${pageContext.request.contextPath}/Corporate/ListCorporatePartner";
										},
									});
						});
	</script>

</body>
</html>