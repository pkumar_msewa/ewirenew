<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Bulk Register</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />

		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-6 offset-md-3 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">Bulk Registration</h4>
									<h6 align="right" style="color: green;">
										<a
											href="${pageContext.request.contextPath}/Corporate/download/bulkregister">Click
											here to download format</a>
									</h6>

									<form
										action="${pageContext.request.contextPath}/Corporate/BulkRegister"
										method="Post" enctype="multipart/form-data">
										<div class="form-group">
											<input type="file" class="dropify" name="file" />
										</div>
										<div class="form-group">
											<center>
												<button class="btn btn-primary" type="submit" id="uploadDoc"
													disabled>Submit</button>
											</center>
										</div>
									</form>

									<center>
										<h6 class="card-title" style="color: red;">${sucessMSG}</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dropify.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->
	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							// demo.initChartist();
							$(".dropify").change(
									function() {
										var filename = readURL(this);
										$(this).parent().children('span').html(
												filename);
									});

							// Read File and return value  
							function readURL(input) {
								var url = input.value;
								var ext = url.substring(
										url.lastIndexOf('.') + 1).toLowerCase();
								console.log(ext);
								if (input.files && input.files[0]
										&& (ext == "csv")) {
									var path = $(input).val();
									var filename = path.replace(/^.*\\/, "");
									// $('.fileUpload span').html('Uploaded Proof : ' + filename);
									// document.getElementById('uploadDoc').enabled = 'enabled';
									$('button:submit').attr('disabled', false);

									return "Uploaded file : " + filename;
								} else {
									document.getElementById('uploadDoc').disabled = 'disabled';
									$(input).val("");
									return "Only csv format are allowed!";
								}
							}

						});
	</script>

</body>

</html>
