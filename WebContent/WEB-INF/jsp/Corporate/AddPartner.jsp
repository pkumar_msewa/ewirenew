<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Add Partner</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">

<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />

<style type="text/css">
.tgl {
	position: relative;
	display: inline-block;
	height: 30px;
	cursor: pointer;
}

.tgl>input {
	position: absolute;
	opacity: 0;
	z-index: -1;
	/* Put the input behind the label so it doesn't overlay text */
	visibility: hidden;
}

.tgl .tgl_body {
	width: 60px;
	height: 30px;
	background: white;
	border: 1px solid #dadde1;
	display: inline-block;
	position: relative;
	border-radius: 50px;
}

.tgl .tgl_switch {
	width: 30px;
	height: 30px;
	display: inline-block;
	background-color: white;
	position: absolute;
	left: -1px;
	top: -1px;
	border-radius: 50%;
	border: 1px solid #ccd0d6;
	-moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
	-webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
	box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
	-moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1),
		-moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
	-o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform
		250ms cubic-bezier(0.34, 1.61, 0.7, 1);
	-webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1),
		-webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
	transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms
		cubic-bezier(0.34, 1.61, 0.7, 1);
	z-index: 1;
}

.tgl .tgl_track {
	position: absolute;
	left: 0;
	top: 0;
	right: 0;
	bottom: 0;
	overflow: hidden;
	border-radius: 50px;
}

.tgl .tgl_bgd {
	position: absolute;
	right: -10px;
	top: 0;
	bottom: 0;
	width: 55px;
	-moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right
		250ms cubic-bezier(0.34, 1.61, 0.7, 1);
	-o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms
		cubic-bezier(0.34, 1.61, 0.7, 1);
	-webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right
		250ms cubic-bezier(0.34, 1.61, 0.7, 1);
	transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms
		cubic-bezier(0.34, 1.61, 0.7, 1);
	background: #439fd8
		url("http://petelada.com/images/toggle/tgl_check.png") center center
		no-repeat;
}

.tgl .tgl_bgd-negative {
	right: auto;
	left: -45px;
	background: white url("http://petelada.com/images/toggle/tgl_x.png")
		center center no-repeat;
}

.tgl:hover .tgl_switch {
	border-color: #b5bbc3;
	-moz-transform: scale(1.06);
	-ms-transform: scale(1.06);
	-webkit-transform: scale(1.06);
	transform: scale(1.06);
}

.tgl:active .tgl_switch {
	-moz-transform: scale(0.95);
	-ms-transform: scale(0.95);
	-webkit-transform: scale(0.95);
	transform: scale(0.95);
}

.tgl>:not (:checked ) ~ .tgl_body>.tgl_switch {
	left: 30px;
}

.tgl>:not (:checked ) ~ .tgl_body .tgl_bgd {
	right: -45px;
}

.tgl>:not (:checked ) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
	right: auto;
	left: -10px;
}

.select2-container {
	display: block;
	width: 100% !important;
}

.select2-container--default .select2-selection--multiple {
	border-radius: 0 !important;
}
</style>

</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp"></jsp:include>
		<div class="container-fluid page-body-wrapper">

			<!-- partial -->
			<!-- partial:partials/_sidebar.html -->
			<jsp:include page="LeftMenu.jsp" />
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">Add Partner</h4>
									<div style="color: green; text-align: center;">${message}</div>

									<form
										action="${pageContext.request.contextPath}/Corporate/AddCorporatePartner"
										id="formId" method="post">
										<fieldset>
											<legend>Partner Details</legend>
											<div class="row">
												<div class="col-6">
													<div class="form-group">
														<label for="fname">Partner Name</label> <input type="text"
															name="partnerName" id="firstName" class="form-control"
															onkeypress="return isAlphKey(event);">
														<p id="firstError" style="color: red"></p>
													</div>
												</div>

												<div class="col-6">
													<div class="form-group">
														<label for="email">Email</label> <input type="email"
															name="partnerEmail" id="email" class="form-control">
														<p id="mailError" style="color: red"></p>
													</div>
												</div>
												<div class="col-6">
													<div class="form-group">
														<label for="mobile">Mobile</label> <input type="text"
															name="partnerMobile" id="contactNo" maxlength="10"
															class="form-control"
															onkeypress="return isNumberKey(event);">
														<p id="ferror" style="color: red"></p>
													</div>
												</div>
												<div class="col-6">
													<div class="form-group">
														<label for="mobile">Load Card Max Limit</label> <input
															type="text" name="loadCardMaxLimit" id="maxLimitId"
															maxlength="8" class="form-control"
															onkeypress="return isNumberKey(event);">
														<p id="lcerror" style="color: red"></p>
													</div>
												</div>
												<div class="col-6">
													<div class="form-group">
														<label for="mobile">Multiple Select</label> <select
															class="multi-select" name="services" multiple="multiple">
															<c:forEach items="${serviceList}" var="services"
																varStatus="loopCounter">
																<option value="${services.code}">${services.description}</option>

															</c:forEach>
														</select>
													</div>
												</div>

											</div>
										</fieldset>
										<center>
											<button type="button" id="adduse"
												class="btn btn-primary mt-4" onclick="validateform()">Submit</button>
										</center>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>


	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js"
		type="text/javascript"></script>

	<script type="text/javascript">
            $(document).ready(function () {
                $('#option').change(function () {
                  $('#proxy').fadeToggle();
                });
            });

        </script>

	<script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                autoClose: true
            });
    </script>

	<script>
      $(document).ready(function() {
          $('.multi-select').select2();
      });
    </script>

	<script>
    	function validateform(){
    	var valid = true;
    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
    	var firstName=$("#firstName").val();
    	var contactNo  = $('#contactNo').val();
    	var email = $('#email').val() ;
    	var maxLimit= $('#maxLimitId').val();
    	var tetee=document.getElementsByName("physicalCard");
    
    	if(firstName.length <= 0){
    	$("#firstError").html("Please enter your first name");
    	valid = false;
    	}
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact no");
    		valid = false;
    	}
    	if(maxLimit.length <=0){
    		$("#lcerror").html("Please enter load card max limit");
    		valid = false;
    	}
    	if(email.length  <= 0){
    		$("#mailError").html("Please enter your email Id ");
    		valid = false; 
    	}else if(!email.match(pattern)) {
    		console.log("inside mail")
    		$("#mailError").html("Enter valid email Id");
    		valid = false;	
    	}

    if(valid == true) {
    	$("#formId").submit();
    	$("#adduse").addClass("disabled");
    } 
    
    var timeout = setTimeout(function(){
        $("#firstError").html("");
    	$("#lastError").html("");
    	$("#ferror").html("");
    	$("#mailError").html("");
    	$("#proxyNumbererror").html("");
    	$("#passError").html("");
    	$("#doberror").html("");
    	$("#lcerror").html("");
    }, 4000);
    }
    </script>


	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

	<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

	<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
</body>

</html>
