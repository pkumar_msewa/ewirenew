<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | upgrade account</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />

		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-8 offset-md-2 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">UPGRADE ACCOUNT</h4>
									<form
										action="${pageContext.request.contextPath}/Corporate/UpgradeAccount"
										method="Post" enctype="multipart/form-data">

										<div class="row">
											<div class="col-6">
												<div class="form-group">
													<label for="">Id Type</label> <select class="form-control"
														id="idType" name="idType">
														<option value="">Select Id Type</option>
														<option value="drivers_id">Driving License(DL)</option>
														<option value="passport">Passport</option>
														<option value="aadhaar">Aadhaar Card</option>
													</select>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="" class="sr-on/ly">Id Number</label> <input
														type="text" id="idNumber" name="idNumber"
														class="form-control" placeholder="eg.1288 8273 8123 1231"
														maxlength="15">
													<p id="error_idNumber" style="color: red"></p>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="" class="sr-on/ly">Upload Id(Front)</label> <input
														type="file" name="image" id="aadhar_Image"
														class="form-control">
													<p id="error_aaadharCardImg" style="color: red"></p>
												</div>
											</div>
											<div class="form-group">
												<label for="" class="sr-on/ly">Upload Id(Back)</label> <input
													type="file" name="image2" id="aadhar_Image2"
													class="form-control">
												<p id="error_aaadharCardImg2" style="color: red"></p>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="address1">Address1</label>
													<div>
														<input type="text" class="form-control" id="address1"
															placeholder="Address1" name="address1" />
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="dob">DOB</label>
													<div>
														<input type="text" class="form-control" id="dob"
															placeholder="DOB" name="dob" />
													</div>
												</div>
											</div>
											<input type="hidden" name="username" value="${userc}" />
											<div class="col-6">

												<div class="form-group">
													<label for="address2">Address2</label>
													<div>
														<input type="text" class="form-control" id="address2"
															placeholder="Address2" name="address2" />
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="city">City</label>
													<div>
														<input type="text" class="form-control" id="city"
															placeholder="City" name="city" />
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="state">State</label>
													<div>
														<input type="text" class="form-control" id="state"
															placeholder="State" name="state" />
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="country">Country</label>
													<div>
														<input type="text" class="form-control" id="country"
															placeholder="Country" name="Country" />
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="form-group">
													<label for="pincode">Pincode</label>
													<div>
														<input type="text" class="form-control" id="pincode"
															placeholder="Pincode" name="pincode" />
													</div>
												</div>
											</div>

										</div>
										<div class="form-group">
											<center>
												<button class="btn btn-primary" type="submit">Submit</button>
											</center>
										</div>
									</form>
									<center>
										<h6 class="card-title" style="color: red;">${message}</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->

</body>
</html>