<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Corporate Prefund</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->


<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />

<script type="text/javascript">
	var context_path = "${pageContext.request.contextPath}";
</script>
</head>
<body class="sidebar-fixed" oncontextmenu="return false">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />
		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<div class="row">
										<div class="col-8">
											<h4 class="card-title">Prefund History</h4>
										</div>
									</div>

									<div class="row">
										<div class="col-12">
											<div class="card-box">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
														<form
															action="${pageContext.request.contextPath}/Corporate/PrefundHistory"
															method="post">
															<div class="form-row">
																<div class="col-sm-8">
																	<div id="" class="pull-left" style="cursor: pointer;">
																		<label class="sr-only" for="filterBy">Filter
																			By:</label> <input id="reportrange" name="Daterange"
																			class="form-control" readonly="readonly" />
																	</div>
																</div>
																<div class="col-sm-3">
																	<button class="btn btn-primary" onclick="fetchlist()"
																		type="submit">Filter</button>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="tabble">
										<table id="example"
											class="table table-striped table-bordered dt-responsive nowrap"
											style="width: 100%">
											<thead>
												<tr>
													<th><b>Sl No.</b></th>
													<th><b>Date</b></th>
													<th><b>Transaction Ref No</b></th>
													<th><b>Amount</b></th>
													<th><b>Status</b></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${prefund}" var="partner"
													varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td><c:out value="${partner.created} " default=""
																escapeXml="true" /></td>
														<td><c:out value="${partner.transactionRefNo}"
																default="" escapeXml="true" /></td>
														<td><c:out value="${partner.amount}" default=""
																escapeXml="true" /></td>
														<c:choose>
															<c:when test="${partner.prefundStatus==true}">
																<td><c:out value="Success" default=""
																		escapeXml="true" /></td>
															</c:when>
															<c:otherwise>
																<td><c:out value="Failed" default=""
																		escapeXml="true" /></td>
															</c:otherwise>
														</c:choose>
													</tr>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<%-- <script src="${pageContext.request.contextPath}/resources/corporate/js/data-table.js"></script> --%>

	<!-- Include Date Range Picker -->
	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->

	<script>
		$(function() {

			var start = moment().subtract(29, 'days');
			var end = moment();

			function cb(start, end) {
				$('#reportrange').html(
						start.format('MM-dd-yyyy') + ' - '
								+ end.format('MM-dd-yyyy'));
			}

			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						locale : {
							format : 'YYYY-MM-DD'
						},
						dateLimit : {
							"days" : 30
						},
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);

			cb(start, end);

		});
	</script>

	<script>
		$(document).ready(function() {
			$('#example').DataTable({
				searching : true
			});
		});
	</script>

	<script>
		document.onkeydown = function(e) {
			if (event.keyCode == 123) {
				return false;
			}
			if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
				return false;
			}
			if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
				return false;
			}
			if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
				return false;
			}
		}
	</script>

</body>
</html>