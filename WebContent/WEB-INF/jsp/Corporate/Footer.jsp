<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<footer class="footer text-right">
	&copy;
	<script>
		document.write(new Date().getFullYear());
	</script>
	Copyright EWire
</footer>