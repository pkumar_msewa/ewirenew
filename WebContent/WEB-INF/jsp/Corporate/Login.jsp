<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Login</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/custom.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div
				class="content-wrapper d-flex align-items-center auth login-full-bg">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auth-form-dark text-left p-5">
							<center>
								<img
									src="${pageContext.request.contextPath}/resources/corporate/images/logo.png"
									class="img-fluid logLogo">
							</center>
							<!-- <h4 class="font-weight-light">Hello! let's get started</h4> -->
							<form class="pt-5"
								action="${pageContext.request.contextPath}/Corporate/Home"
								method="Post">
								<div class="form-group">
									<p id="loginMsg" style="color: red;" align="center"></p>
									<label for="exampleInputEmail1">Username</label> <input
										type="email" class="form-control" id="exampleInputEmail1"
										name="username" placeholder="Username"> <i
										class="mdi mdi-account"></i>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label> <input
										type="password" class="form-control"
										id="exampleInputPassword1" name="password"
										placeholder="Password"> <i class="mdi mdi-eye"></i>
								</div>
								<div class="mt-5">
									<button
										class="btn btn-block btn-warning btn-lg font-weight-medium"
										type="submit">Login</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/hoverable-collapse.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<!-- endinject -->

	<script type="text/javascript">
		$(document).ready(function() {
			var loginMsg = '${loginMsg}';
			$('#loginMsg').html(loginMsg);
			setTimeout(function() {
				$('#loginMsg').html('');
			}, 3000);
		});
	</script>

</body>
</html>