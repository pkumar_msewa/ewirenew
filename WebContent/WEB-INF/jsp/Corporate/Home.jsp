<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Dashboard</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />

<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />

		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-6 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">Amount Prefunded</h4>
									<canvas id="lineChart" style="height: 250px"></canvas>
								</div>
							</div>
						</div>
						<div class="col-md-6 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<!-- <h4 class="card-title">User Rating</h4> -->
									<!-- <h6 class="text-muted text-center mb-1">Average Rating 4.0</h6> -->
									<div class="row">
										<div class="col-md-6">
											<div class="wrapper d-flex align-items-center py-2 mb-3">
												<div class="wrapper ml-3">
													<h6 class="text-muted mb-2">Total Amount Prefunded</h6>
													<h4 class="mb-0">
														<strong>Rs. ${corporateBalance}</strong>
													</h4>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div id="container"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Recent Transaction</h4>
									<div
										class="wrapper d-flex align-items-center py-2 mb-4 table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<th><b>Sl No.</b></th>
													<th><b>Date</b></th>
													<th><b>Transaction Ref No</b></th>
													<th><b>Amount</b></th>
													<th><b>Status</b></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${prefundList}" var="partner"
													varStatus="loopCounter">
													<tr>
														<td>${loopCounter.count}</td>
														<td><c:out value="${partner.created} " default=""
																escapeXml="true" /></td>
														<td><c:out value="${partner.transactionRefNo}"
																default="" escapeXml="true" /></td>
														<td><c:out value="${partner.amount}" default=""
																escapeXml="true" /></td>
														<c:choose>
															<c:when test="${partner.prefundStatus==true}">
																<td><c:out value="Success" default=""
																		escapeXml="true" /></td>
															</c:when>
															<c:otherwise>
																<td><c:out value="Failed" default=""
																		escapeXml="true" /></td>
															</c:otherwise>
														</c:choose>
													</tr>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/highcharts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/exporting.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/export-data.js"></script>

	<!-- piechart -->
	<script>
		// Build the chart
		Highcharts.chart('container', {
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false,
				type : 'pie'
			},
			exporting : {
				enabled : false
			},
			credits : {
				enabled : false
			},
			title : {
				text : 'Card Analytics'
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : false
					},
					showInLegend : true
				}
			},
			series : [ {
				name : 'Brands',
				colorByPoint : true,
				data : [ {
					name : 'Virtual Cards',
					y : 20,
					sliced : true,
					selected : true
				}, {
					name : 'Physical Cards',
					y : 60
				} ]
			} ]
		});
	</script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/chart.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/c3.js"></script>
</body>
</html>