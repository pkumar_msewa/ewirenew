<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Card Details</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->

<!-- datatables -->
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
<!-- datatables -->

<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/custom.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp" />
		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />

			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row mb-3">
						<div class="col-md-12">
							<div class="card text-black">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<div class="avail-bal">
												<span class="upper">Available Balance :</span>&nbsp;<span
													class="fa fa-inr"></span>&nbsp;<span>${Ubalance}</span>
											</div>
										</div>
										<div class="col">
											<div class="total-cred">
												<span class="upper">Total Debit :</span>&nbsp;<span
													class="fa fa-inr"></span>&nbsp;<span>NA</span>
											</div>
										</div>
										<div class="col">
											<div class="total-deb">
												<span class="upper">Total Credit :</span>&nbsp;<span
													class="fa fa-inr"></span>&nbsp;<span>NA</span>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<!-- <h4 class="card-title">Recent Transaction</h4> -->
									<div class="row">
										<div class="col-sm-5">
											<div class="card-wrp">
												<img
													src="${pageContext.request.contextPath}/resources/corporate/images/card.png"
													class="img-fluid">
												<div class="card-txt">
													<div class="cvv">
														<span>CVV</span>&nbsp;<span>${cardDetail.cvv}</span>
													</div>
													<div class="card-num">
														<span>${cardDetail.walletNumber}</span>
													</div>
													<div class="expDt">
														<span>Valid Thru</span><br> <span>${cardDetail.expiryDate}</span>
													</div>
													<div class="holdNme">
														<span>${cardDetail.holderName}</span>
													</div>
												</div>
												<div class="usrBlock text-center mt-3">
													<c:choose>
														<c:when test="${cardstatus == 'false'}">
															<button class="btn btn-danger" type="button"
																data-toggle="modal" data-target="#myModal">BLOCK</button>
														</c:when>
														<c:otherwise>

															<button class="btn btn-success" type="button"
																id="myUnblock" onclick="unblk()">UNBLOCK</button>
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</div>
										<div class="col-sm-7">
											<div
												class="wrapper d-flex align-items-center py-2 mb-4 table-responsive">
												<table id="one_card"
													class="table table-striped table-bordered dt-responsive nowrap"
													style="width: 100%">
													<thead>
														<tr>
															<th>Sl No</th>
															<th>Amount</th>
															<th>TransactionType</th>
															<th>Description</th>
															<th>Date</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${transactions}" var="card"
															varStatus="loopCounter">
															<tr>
																<td>${loopCounter.count}</td>
																<td><c:out value="${card.amount}" default=""
																		escapeXml="true" /></td>
																<td><c:out value="${card.transactionType}"
																		default="" escapeXml="true" /></td>
																<td><c:out value="${card.description}" default=""
																		escapeXml="true" /></td>
																<td><c:out value="${card.date}" default=""
																		escapeXml="true" />
																<td><c:out value="${card.status}" default=""
																		escapeXml="true" /></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- content-wrapper ends -->
				<div class="modal fade" id="myModal" style="z-index: 9999;">
					<div class="modal-dialog modal-sm">
						<div class="modal-content"
							style="padding: 0; padding-left: 0; width: auto;">

							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title">Card Block</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<!-- Modal body -->
							<div class="modal-body">
								<div class="blck_resnm">
									<form>
										<div class="form-group">
											<label for="option">Reason</label> <select
												class="form-control" id="blockType" name="blockType">
												<option value="suspend">Suspend</option>
												<option value="Lost">Lost</option>
												<option value="Stolen">Stolen</option>
												<option value="Damaged">Damaged</option>
											</select>
										</div>
										<center>
											<button class="btn btn-primary" type="button" id="blockCard">Submit</button>
										</center>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<!-- endinject -->

	<!-- datatables -->
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script>
	<!-- Custom js for this page-->
	<script src="js/dashboard.js"></script>
	<!-- End custom js for this page-->

	<script>
		$(document).ready(function() {
			$('#one_card').DataTable({
				responsive : true,
				paging : false,
				info : false,
				searching : false
			});
		});
	</script>


	<script>
		$(document).ready(
				function() {
					$('#datatable').DataTable({
						responsive : true
					});

					//Buttons examples
					var table = $('#datatable-buttons').DataTable({
						lengthChange : false,
						buttons : [ 'copy', 'excel', 'pdf' ]
					});

					table.buttons().container().appendTo(
							'#datatable-buttons_wrapper .col-md-6:eq(0)');
				});
	</script>
	<!-- <script>

var cardId="${cardDetail.cardId}";
var cont_path="${pageContext.request.contextPath}";
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBlock");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

var blockBtn=document.getElementById('blockType').value;

console.log(blockBtn);

var onBlock=document.getElementById('blockCard');

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    onBlock.onclick=function(){
    	onBlock.disabled='disabled';
    	console.log(blockBtn);
    	$.ajax({
    	    type:"POST",
    	    contentType : "application/json",
    	    url: cont_path+"/Corporate/BlockCard",
    	    dataType : 'json',
			data : JSON.stringify({
				"cardId" : "" + cardId + "",
				"requestType" : ""+blockBtn+""
			}),
			success : function(response) {
    	         /*  your code here */
    	        if (response.code.includes("S00")) {
					swal("Success!!", response.message, "success");}
    	        else{
    	        	swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
    	        }
    	    }
    	   
    	});
    }
}
</script> -->
	<script type="text/javascript">
		function unblk() {
			$
					.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Corporate/UnblockCard",
						dataType : 'json',
						data : JSON.stringify({
							"cardId" : "" + cardId + "",
							"requestType" : "Active"
						}),
						success : function(response) {
							/*  your code here */
							if (response.code.includes("S00")) {
								$('#myModal').modal('hide');
								swal("Success!!", response.message, "success");
							} else {
								$('#myModal').modal('hide');
								swal({
									type : 'error',
									title : 'Sorry!!',
									text : response.message
								});
							}
						}

					});

		}
	</script>

	<script>
		var cardId = "${cardDetail.cardId}";
		var cont_path = "${pageContext.request.contextPath}";
		// Get the modal
		var modal = document.getElementById('myModal');
		var blockBtn = document.getElementById('blockType').value;

		$("#blockCard").click(function() {
			console.log('Hi....');
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "${pageContext.request.contextPath}/Corporate/BlockCard",
				dataType : 'json',
				data : JSON.stringify({
					"cardId" : "" + cardId + "",
					"requestType" : "" + blockBtn + ""
				}),
				success : function(response) {
					/*  your code here */
					if (response.code.includes("S00")) {
						$('#myModal').modal('hide');
						swal("Success!!", response.message, "success");
					} else {
						$('#myModal').modal('hide');
						swal({
							type : 'error',
							title : 'Sorry!!',
							text : response.message
						});
					}
				}

			});

		});
	</script>

</body>

</html>
