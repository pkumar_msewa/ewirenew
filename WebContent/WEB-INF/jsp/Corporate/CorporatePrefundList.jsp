<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Admin | Corporate Prefund Requests</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- App favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.png">

<!-- DataTables -->
<link
	href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link
	href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css"
	rel="stylesheet" type="text/css" />

<!-- App css -->
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>

<script type="text/javascript">
	var context_path = "${pageContext.request.contextPath}";
</script>

</head>


<body oncontextmenu="return false">
	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">List of Prefunds</h4>
								<div class="clearfix"></div>
								<span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card-box table-responsive">
								<!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

								<table id="datatable"
									class="table table-striped table-bordered dt-responsive nowrap"
									cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Sl No</th>
											<th>Date Created</th>
											<th>Corporate Name</th>
											<th>ScreenShot</th>
											<th>Requested Amount</th>
											<th>TransactionRefNo</th>
											<c:choose>
												<c:when test="${addUserBool eq true}">
													<th>Action</th>
												</c:when>
												<c:otherwise>
												</c:otherwise>
											</c:choose>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${prefundList}" var="card"
											varStatus="loopCounter">
											<tr>
												<td>${loopCounter.count}</td>
												<td><c:out value="${card.created}" default=""
														escapeXml="true" /></td>
												<td><c:out value="${card.corporateName}" default=""
														escapeXml="true" /></td>
												<td><a href="${card.filePath}" download target="_blank">DOWNLOAD</a></td>
												<td><c:out value="${card.amount}" default=""
														escapeXml="true" /></a></td>
												<td><c:out value="${card.transactionRefNo}" default=""
														escapeXml="true" /></td>
												<td><c:choose>
														<c:when
															test="${card.reviewStatus==true && card.prefundStatus==false}">
															<td><h6>Already Reviewed</h6></td>
														</c:when>
														<c:when
															test="${card.prefundStatus==true && card.reviewStatus==true}">
															<td><h6>Already Prefunded</h6></td>
														</c:when>
														<c:otherwise>
															<form
																action="${pageContext.request.contextPath}/Admin/ReviewCorporatePrefund"
																method="post" id="corpSub">
																<input type="hidden" value="${card.id}" name="fileId" />
																<input type="hidden" id="corp" name="action" />
																<c:choose>
																	<c:when test="${addUserBool eq true}">
																		<td><button type="button"
																				class="btn btn-sm btn-success" id="ublck1"
																				value="accept" onclick="clickMe('accept')">Accept</button>&nbsp;
																			<button type="button" class="btn btn-sm btn-success"
																				id="ublck1" value="reject"
																				onclick="clickMe('reject')">Reject</button></td>
																	</c:when>
																	<c:otherwise>
																	</c:otherwise>
																</c:choose>
															</form>
														</c:otherwise>
													</c:choose>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="Footer.jsp"></jsp:include>
		</div>
	</div>

	<!-- jQuery  -->

	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

	<!-- Required datatable js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
	<!-- Buttons examples -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
	<!-- Responsive examples -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

	<script type="text/javascript">
		$(document).ready(
				function() {
					$('#datatable').DataTable({
						responsive : true
					});

					//Buttons examples
					var table = $('#datatable-buttons').DataTable({
						lengthChange : false,
						buttons : [ 'copy', 'excel', 'pdf' ]
					});

					table.buttons().container().appendTo(
							'#datatable-buttons_wrapper .col-md-6:eq(0)');

					var timeout = setTimeout(function() {
						$("#stst").html("");
					}, 3000);

				});
	</script>

</body>

<script>
	function clickMe(value) {
		$('#corp').val(value);
		$('#corpSub').submit();
	}
</script>

<script>
	document.onkeydown = function(e) {
		if (event.keyCode == 123) {
			return false;
		}
		if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
			return false;
		}
		if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
			return false;
		}
		if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
			return false;
		}
	}
</script>
</html>