<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ewire Corporate | Change Password</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/css/vendor.bundle.addons.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/vendors/iconfonts/ti-icons/css/themify-icons.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/corporate/css/style.css">
<!-- endinject -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/corporate/images/favicon.png" />
</head>
<body class="sidebar-fixed">
	<div class="container-scroller">
		<jsp:include page="Header.jsp"></jsp:include>

		<div class="container-fluid page-body-wrapper">
			<jsp:include page="LeftMenu.jsp" />

			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card text-black">
								<div class="card-body">
									<h4 class="card-title">ChangePassword</h4>
									<div style="color: red; text-align: center;">${message}</div>
									<form
										action="${pageContext.request.contextPath}/Corporate/ChangePasssword"
										id="formId" method="post">
										<div class="row">
											<div class="col-6 offset-sm-3">
												<fieldset>
													<legend>Change Password</legend>
													<div class="form-group">
														<label for="password">Old Password</label> <input
															type="Password" name="oldPassword" id="Oldpassword"
															class="form-control" maxlength="16">
														<p id="ferror" style="color: red"></p>
													</div>
													<div class="form-group">
														<label for="newPassword">New Password</label> <input
															type="Password" name="password" id="newPassword"
															class="form-control" maxlength="16">
														<p id="derror" style="color: red"></p>
													</div>
													<div class="form-group">
														<label for="confirmPassword">Confirm Password</label> <input
															type="text" name="confirmPassword" id="confirmPassword"
															class="form-control" maxlength="16">
														<p id="amountError" style="color: red"></p>
													</div>
													<input type="hidden" name="username" value="${username}">
												</fieldset>
											</div>
										</div>
										<center>
											<button type="button" class="btn btn-primary mt-4"
												onclick="validateform()">Submit</button>
										</center>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="Footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- plugins:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.base.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/off-canvas.js"></script>
	<!-- <script src="js/hoverable-collapse.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/misc.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/settings.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/todolist.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/jquery-file-upload.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script
		src="${pageContext.request.contextPath}/resources/corporate/js/dashboard.js"></script>
	<!-- End custom js for this page-->

	<script>
    	
    	function validateform(){
    	var valid = true;
    	
    	var contactNo  = $('#Oldpassword').val();
    	var amount = $('#newPassword').val() ;
    	var driver= $('#confirmPassword').val();
    	console.log(contactNo);
    	console.log(amount);
    	console.log(driver);
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your Old Password");
    		valid = false;
    	}
    	if(amount.length<= 0){
    		$("#amountError").html("Please enter Your New Password ");
    		valid = false; 
    	}
    	if(driver.length<=0){
    		$("#derror").html("Please enter Confirm Password");
    	}

    if(valid == true) {
    	$("#formId").submit();
    } 
    
    var timeout = setTimeout(function(){
    	$("#ferror").html("");
    	$("#amountError").html("");
    }, 4000);
    }
    </script>

	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

	<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>
</body>

</html>
