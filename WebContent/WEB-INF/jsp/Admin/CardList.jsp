<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Cards</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
        <script	src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
        
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->
    
    <script type="text/javascript">
    var spinnerUrl = "<img src='${pageContext.request.contextPath}/resources/spinner2.gif' style='height:150px;width:150px;'>"
    $(document).ready(function() {
    	$("#spinner").html(spinnerUrl);	 
		 getAjaxData();
	 });

	function fetchMe(value){
	$(".testingg").empty();
	var reportRange=$("#reportrange").val();
	
	var status=$("#stst").val();
	console.log("FETCH"+reportRange);
	console.log("STATUS"+status);
	$("#spinner").html(spinnerUrl);	
	var paging=value;
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/CardListByPage",
	data:{page:paging,size:'10',Daterange:reportRange,cardStatus:status},
	dataType:"json",
	success:function(data){
		$("#spinner").html('');	 
		var trHTML='';
		if(trHTML==''){
		$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});
		$('#abhijit').append(trHTML);
		}
		else
		{
			$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});
			$('#abhijit').append(trHTML);
			
		}
		 
	}

	});
			 }
		 
	 function  getAjaxData(){
		 var paging='0';
		 var size='';
		 var reportRange=$("#reportrange").val();
			var status=$("#stst").val();
			console.log(reportRange);
			console.log(status);
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/CardListByPage",
				data:{
					page:paging,
					size:'10',
					Daterange:reportRange,
					cardStatus:status
				},
			dataType:"json",
			success:function(data){
				console.log(data);
			$("#abhijit").empty();
				$("#spinner").html('');	 
				var trHTML='';
				if(trHTML==''){
				$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});
				$('#abhijit').append(trHTML);
				}
				else
				{
					$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});
					$('#abhijit').append(trHTML);
					
				}
					 $(function () {
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 5,
				         onPageClick: function (event, page) {
				        	 //console.log(data.totalPages);
							console.log(page);				        	 
				        	 fetchMe(page-1);
								
				         }
						 });
						});
						}
					 });
				 }
</script>	
    

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                      <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">List of Virtual Cards</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        
                      <!--   for left content -->
                      <div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="form-row">
													<div class="col-sm-4">
														 <div class="form-group">
														 	<select id="stst" name="status" class="form-control mr-sm-2">
							                                    <option value="Active">Active</option>
							                                    <option value="Inactive">InActive</option>
						                                    </select>
														 </div>
														 
													</div>
													<div class="col-sm-4">
														<div id="" class="pull-left" style="cursor: pointer;">
															<label class="sr-only" for="filterBy">Filter By:</label>
														   	<input id="reportrange" name="toDate" class="form-control" readonly="readonly" onkeypress="return getAjaxData();"/>
														</div>
													</div>
													<div class="col-sm-2">
														<button class="btn btn-primary" id="btn2sds">Filter</button>
													</div>
												</div>
										
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<form action="${pageContext.request.contextPath}/Admin/SingleCardvirtualByPage" method="post" class="form-inline" style="float: right;" id="formId">
			                                    <div class="form-group">
				                                    <input id="username" name="username" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
				                                    <button class="btn btn-primary" onclick="fetchSincgleCard()" type="button" >Search</button>
			                                   	</div>
			                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
			                                </form>
										</div>
									</div>
								</div>
							</div>
						</div>
                     <!--  end -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                	<div class="row mb-2">
										<div class="col-sm-12">
											<!-- <button class="btn btn-primary">CSV</button> -->
											
											<input type="hidden" name="requestType" id="hiddenRequestType" value="virtualCards"/>
											
											<button type="button" id="downloadCsvButton">Download CSV</button>
											
										</div>                                		
                                	</div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User Details</th>
                                                <th>Card Id</th>
                                                <th>Balance</th>
                                                <th>Issue Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody id="abhijit">
                                    </tbody> 
                                    </table>
                                    <center><span id="spinner"> </span></center>
                                    <nav style="float: right;">
										<ul class="pagination" id="paginationn"></ul>
									</nav>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

<script>


</script>


        <!-- jQuery  -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
          <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
          <!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
 <script>
 function fetchSincgleCard(){
	var username=$("#username").val();
	var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true){
			$("#formId").submit();
		}
 }
 </script>
 <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script>
		$(function() {
		
		    var start = moment().subtract(30, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		<script>
		$("#btn2sds").click(function(){
			console.log('calling');
			$("#spinner").html(spinnerUrl);
			getAjaxData();
		});
		</script>
		
		<!-- DOWNLOAD AJAX -->
		<script>
		$("#downloadCsvButton").click(function(){

		var reportRange=$("#reportrange").val();
				var status=$("#stst").val();
				var requestType=$("#hiddenRequestType").val();
				console.log(requestType);
				console.log(reportRange);
			 	console.log(status);
			 console.log("under ready...");
			 $("#spinner").html(spinnerUrl);

			 $.ajax({
					type:"GET",
					url:"${pageContext.request.contextPath}/Admin/download/csv",
					data:{
						requestType:requestType,
						dateRange:reportRange,
						status:status
					},
					dataType:"json",

				success:function(data){
					console.log("SUCCESS");
					console.log(data.code);
					window.location.href="${pageContext.request.contextPath}"+data.code;
					 $("#spinner").empty();

					//window.open(data.code, '_blank');
		}
			 });
		});
		</script>
		
	 	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

    </body>
   

</html>