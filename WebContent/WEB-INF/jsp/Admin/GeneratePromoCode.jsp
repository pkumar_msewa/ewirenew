<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Add PromoCode</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

         <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            .mt-6 {
                margin-top: 2rem !important;
            }
            .gj-datepicker-bootstrap [role=right-icon] button {
                padding: 18px !important;
            }
        </style>

<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}";
</script>
    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">
		 <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Promocode</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <div style="color: green;text-align: center;">${sucMessage}</div>
                                    <div style="color: red;text-align: center;">${errMessage}</div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="col-sm-5 offset-sm-3">
                                    <div class="card-box">
                                        <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                        <p class="text-muted font-14 m-b-30">
                                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                        </p> -->
                                        <form action="${pageContext.request.contextPath}/Admin/AddPromo" method="post" id="promForm">
                                                        <div class="form-row">
                                                            <div class="form-group col-md-9">
                                                                <label for="">Promocode:</label>
                                                                <input type="text" class="form-control" name="promoCode" id="promoCode" onkeypress="return isAlphNumberKey(event);" maxlength="15">
                                                                <span id="errorPromoCode" style="color: red"></span>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <button type="button" class="btn btn-sm btn-primary mt-6" onclick="checkPromo()"><i class="fa fa-check-circle-o"></i> Validate</button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label for="">Starts Date:</label>
                                                                    <input type="text" class="form-control start_dt" id="startDate" name="startDate" readonly="readonly">
                                                                    <p id="errorstartDate" style="color: red">
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label for="">End Date:</label>
                                                                    <input type="text" class="form-control end_dt" name="endDate" id="endDate" readonly="readonly">
                                                                    <p id="errorendDate" style="color: red"> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label for="">Min Trx Amount:</label>
                                                                    <input type="text" name="minTrxAmount" class="form-control" id="minTrxAmount" onkeypress="return isNumberKey(event);" maxlength="4">
                                                               		<p id="errorminTrxAmount" style="color: red">
                                                                </div>
                                                                <div class="col">
                                                                    <label for="">Max Cash Back:</label>
                                                                    <input type="text" name="maxCashBack" class="form-control" id="maxCashBack" onkeypress="return isNumberKey(event);" maxlength="4">
                                                                	<p id="errormaxCashback" style="color: red">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label for="">Inpercent(Amount):</label>
                                                                    <input type="text" class="form-control" id="percentage" name="percentageValue" onkeypress="return isNumberKey(event);" maxlength="3">
                                                                </div>
                                                                <div class="col">
                                                                    <label for="">Value(Amount):</label>
                                                                    <input type="text" class="form-control" id="amount" name="amount" onkeypress="return isNumberKey(event);" maxlength="4">
                                                                </div>
                                                                <p id="erroramount" style="color: red">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                          <label for="sel1">Select Service:</label>
                                                          <select name="serviceCode" class="form-control" id="serviceCode">
                                                          <option value="">Select Service</option>
                                                          <c:forEach items="${services}" var="serv">
                                                            <option value="${serv.code}">${serv.name}</option>
                                                           </c:forEach> 
                                                            
                                                          </select>
                                                          <p id="errorservice" style="color: red">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col">
                                                                <center><button type="button" onclick="generatePrmcod()" class="btn btn-primary">Add Promocode</button></center>
                                                            </div>
                                                        </div>
                                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 Â© Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script>
            
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            $('.start_dt').datepicker({
                uiLibrary: 'bootstrap4',
                minDate: today,
                maxDate: function () {
                    return $('.end_dt').val();
                }
            });
            $('.end_dt').datepicker({
                uiLibrary: 'bootstrap4',
                minDate: function () {
                    return $('.start_dt').val();
                }
            });
        </script>

        <script type="text/javascript">
        function checkPromo(){
        	var promocode=$("#promoCode").val();
        	var valid=true;
        	if(promocode == ''){
        		valid=false;
        		$("#errorPromoCode").html("Please enter promo code")
        	}
        	
        	if(valid == true){
        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Admin/Promocode",
    			dataType : 'application/json',
    			data : JSON.stringify({
    				"promoCode" :"" +promocode+"",
    			}),
    			success : function(response) {
    				console.log("response:: -- "+response.code);
    				console.log("response:: "+response.message);
    				$("#errorPromoCode").html(response.message)
    			},
        	error : function(response) {
				var daad =JSON.parse(response.responseText);
				console.log("response:: "+daad.message);
				$("#errorPromoCode").html(daad.message)
			}
    		});}
        	var timeout = setTimeout(function(){
        		$("#errorPromoCode").html("");
        	}, 5000);
        }
        
        </script>


	<script type="text/javascript">
	
	function generatePrmcod(){
		var promocode=$("#promoCode").val();
		var startDate=$("#startDate").val();
		var endDate=$("#endDate").val();
		var amount=$("#amount").val();
		var percentage=$("#percentage").val();
		var maxCashback=$("#maxCashBack").val();
		var minTrxAmount=$("#minTrxAmount").val();
		var service=$("#serviceCode").val();
		var valid = true;
		if(promocode == ''){
			valid=false;
			$("#errorPromoCode").html("Please enter promo code");
		}
		if(startDate == ''){
			valid=false;
			$("#errorstartDate").html("Please enter start date");
		}
		if(endDate == ''){
			valid=false;
			$("#errorendDate").html("Please enter end date");
		}
		if(maxCashback == ''){
			valid=false;
			$("#errormaxCashback").html("Please enter max cash back");
		}
		if(minTrxAmount == ''){
			valid=false;
			$("#errorminTrxAmount").html("Please enter min trx amount");
		}
		if(service == ''){
			valid=false;
			$("#errorservice").html("Please select service");
		}
		if(amount != '' && percentage!='' ){
			valid=false;
			$("#erroramount").html("Please enter either amount or percentage");
		} else if(amount == '' && percentage==''){
			$("#erroramount").html("Please enter either amount or percentage");
		}
		
		if(valid == true){
			$("#promForm").submit();
		}
		
		var timeout = setTimeout(function(){
    		$("#errorPromoCode").html("");
    		$("#errorstartDate").html("");
    		$("#errorendDate").html("");
    		$("#errormaxCashback").html("");
    		$("#errorminTrxAmount").html("");
    		$("#errorservice").html("");
    		$("#erroramount").html("");
    	}, 3000);
		
	}
	</script>


	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

    </body>

</html>