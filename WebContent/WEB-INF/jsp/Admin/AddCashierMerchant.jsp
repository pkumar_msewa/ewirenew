<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Add Merchant</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
 <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Merchant</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                <center><div><h6 style="font: bold;color: red;">${msg}</h6></center>
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    
                                    <form action="${pageContext.request.contextPath}/Admin/AddMerchant" method="post" id="formId" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col">
                                                <fieldset>
                                                    <legend>Merchant Details</legend>
                                                    <div class="form-group">
                                                        <label for="name">Merchant Name</label>
                                                        <input type="text" name="merchantName" placeholder="Enter Merchant Name" id="f_Name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event);">
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div>
                                                    
                                                     <div class="form-group">
                                                        <label for="email">BC Name</label>
                                                        <input type="email" name="businessCorrespondentName" placeholder="Enter BC Name" id="bcName" class="form-control">
                                                   			<p class="error" id="error_bcName"></p>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="email">BC Email</label>
                                                        <input type="email" name="businessCorrespondentEmail" placeholder="Enter BC Email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control">
                                                   			<p class="error" id="error_email"></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mobile">BC Contact Number</label>
                                                        <input type="text" name="businessCorrespondentNumber" placeholder="Enter BC Mobile" id="mobile" class="form-control" minlength="10" maxlength="10" onkeypress="return isNumberKey(event);">
                                                  		<p class="error" id="error_mobile"></p>
                                                    </div>
                                                    
                                                      <%-- <div class="form-group">
                                                        <label for="ifsc">Services</label>
                                                        <c:forEach items="${serviceList}" var="card"
									   varStatus="loopCounter">
                                                        <input type="checkbox" name="services" class="form-control" value="${card.name}">
														</c:forEach>
                                                    </div>
 --%>                                                
 
 </fieldset>
                                            </div>
                                            <div class="col">
                                                <fieldset>
                                                    <legend>Upload Documents</legend>
                                                    <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Documents</span>
                                                            <input type="file" class="uploadlogo" name="image" id="aadhar_Image"/>
                                                          </div>
                                                          <p class="error" id="error_aaadharCardImg"></p>
                                                    </div>
                                                  <!--   <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Corporate Logo</span>
                                                            <input type="file" class="uploadlogo" name="corporateLogo2" id="pan_Image" />
                                                        </div>
                                                          <p class="error" id="error_panCardImg"></p>
                                                    </div>
  -->                                               </fieldset>
                                            </div>
                                            </div>
                                            <center><button type="button" id="addAgent" class="btn btn-primary mt-4" onclick="addCorporateAgent()">Submit</button></center>
                                        </form>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
  <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
           <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

        <!-- Daterange picker -->
        <script type="text/javascript">
            $(function() {

                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                
            });
            </script>

            <script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {

              // Upload btn on change call function
              $(".uploadlogo").change(function() {
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });

              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                  ext == "pdf"
                  )) {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only PDF format is allowed!";
                }
              }
              // Upload btn end

            });
            </script>
            
            
            
            
            
           <script type="text/javascript">
           function addCorporateAgent(){
        		var valid=true;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		var fullName=$('#f_Name').val();
        		var mobile=$("#mobile").val();
        		var email=$('#email').val();
        		var aadharCardImg=$('#aadhar_Image').val();
        		var bcName=$('#bcName').val();
        		
        		//console.log("Pancared image"+panCardImg);
        		
        		if(fullName=='') {
        			$("#error_fullName").html("Please enter Merchant Name");
        			valid = false;
        			console.log("valid: "+"fullName");
        		}
        		
        		
        		if(mobile=='') {
        			$("#error_mobile").html("Please enter corporate contact no");
        			valid = false;
        			console.log("valid: "+"bankName");
        		}
        		if(mobile!=''){
        			if(mobile.length!=10){
        			$("#error_mobile").html("Please enter valid contact no");
        			valid = false;
        		}
        		}
        		if(email=='') {
        			$("#error_email").html("Please enter BC email id");
        			valid = false;
        			console.log("valid: "+"bankName");
        		}
        		if(email!=''){
        			if(!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}")){
            			$("#error_email").html("Please enter valid email id");
							valid=false;
        			}
        		}
        		if (aadharCardImg!= "") {
        		    if (!(aadharCardImg.split(".")[1].toUpperCase() == "PDF")) {
        		     $("#error_aaadharCardImg").html("File with " + panCardImg.split(".")[1]
        		     + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   }
        		if(aadharCardImg=='') {
        			$("#error_aaadharCardImg").html("Please attach Doc");
        			valid = false;
        		}
        		if (valid) {
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
//         			$("#addAgent").html(spinnerUrl);
        		}
        	}

        	</script>
        	
        	<script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script>

    </body>

<!-- Mirrored from coderthemes.com/abstack/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jan 2018 11:48:32 GMT -->
</html>