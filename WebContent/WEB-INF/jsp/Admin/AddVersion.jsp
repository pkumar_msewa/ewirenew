<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Add Version</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	 <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
            .tgl {
              position: relative;
              display: inline-block;
              height: 30px;
              cursor: pointer;
            }
            .tgl > input {
              position: absolute;
              opacity: 0;
              z-index: -1;
              /* Put the input behind the label so it doesn't overlay text */
              visibility: hidden;
            }
            .tgl .tgl_body {
              width: 60px;
              height: 30px;
              background: white;
              border: 1px solid #dadde1;
              display: inline-block;
              position: relative;
              border-radius: 50px;
            }
            .tgl .tgl_switch {
              width: 30px;
              height: 30px;
              display: inline-block;
              background-color: white;
              position: absolute;
              left: -1px;
              top: -1px;
              border-radius: 50%;
              border: 1px solid #ccd0d6;
              -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              z-index: 1;
            }
            .tgl .tgl_track {
              position: absolute;
              left: 0;
              top: 0;
              right: 0;
              bottom: 0;
              overflow: hidden;
              border-radius: 50px;
            }
            .tgl .tgl_bgd {
              position: absolute;
              right: -10px;
              top: 0;
              bottom: 0;
              width: 55px;
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              background: #439fd8 url("http://petelada.com/images/toggle/tgl_check.png") center center no-repeat;
            }
            .tgl .tgl_bgd-negative {
              right: auto;
              left: -45px;
              background: white url("http://petelada.com/images/toggle/tgl_x.png") center center no-repeat;
            }
            .tgl:hover .tgl_switch {
              border-color: #b5bbc3;
              -moz-transform: scale(1.06);
              -ms-transform: scale(1.06);
              -webkit-transform: scale(1.06);
              transform: scale(1.06);
            }
            .tgl:active .tgl_switch {
              -moz-transform: scale(0.95);
              -ms-transform: scale(0.95);
              -webkit-transform: scale(0.95);
              transform: scale(0.95);
            }
            .tgl > :not(:checked) ~ .tgl_body > .tgl_switch {
              left: 30px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd {
              right: -45px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
              right: auto;
              left: -10px;
            }
            .options {
                font-size: 16px;
            }
            .gj-datepicker-bootstrap [role=right-icon] button {
            	padding: 18px 0;
            }
        </style>

    </head>


    <body oncontextmenu="return false">

		<!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Version</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <div style="color: green; text-align: center; ">${regmessage}</div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    
                                    <form action="${pageContext.request.contextPath}/Admin/AddUser" id="formId"  method="post">
                                            <div class="row">
                                                <div class="col-6 offset-md-3 offset-md-3t-sm-3">
                                                    <fieldset>
                                                        <legend>User Details</legend>
                                                        <div class="form-group">
                                                            <label for="fname">First Name</label>
                                                            <input type="text" name="firstName" id="firstName" class="form-control"  onkeypress="return isAlphKey(event);">
                                                        	<p id="firstError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="lname">Last Name</label>
                                                            <input type="text" name="lastName" id="lastName" class="form-control"  onkeypress="return isAlphKey(event);">
                                                       	<p id="lastError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="email" name="email" id="email" class="form-control">
                                                        <p id="mailError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="mobile">Mobile</label>
                                                            <input type="text" name="contactNo" id="contactNo" class="form-control"  onkeypress="return isNumberKey(event);">
                                                        <p id="ferror" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="dob">Date of Birth</label>
                                                            <input type="text" id="dob" name="dateOfBirth"  class="form-control datepicker" readonly="readonly">
                                                        <p id="doberror" style="color: red" ></p>
                                                        </div>
                                                         <div class="form-group">
                                                            <label for="password">Password</label>
                                                            <input type="text" name="password" id="password" maxlength="6" class="form-control">
                                                        <p id="passError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="options">Physical</span>
                                                            <label class="tgl">
                                                                <input type="checkbox" id="option" name="physicalCard" checked/>
                                                                <span class="tgl_body">
                                                                    <span class="tgl_switch"></span>
                                                                    <span class="tgl_track">
                                                                        <span class="tgl_bgd"></span>
                                                                        <span class="tgl_bgd tgl_bgd-negative"></span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                            <span class="options">Virtual</span>
                                                        </div>
                                                        <div class="form-group" id="proxy">
                                                            <label for="proxy">Proxy Number</label>
                                                            <input type="text" name="proxyNumber" id="proxyNumber" maxlength="16" min="16"  class="form-control"  onkeypress="return isNumberKey(event);">
                                                       	<p id="proxyNumbererror" style="color: red" ></p>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <center><button type="button" class="btn btn-primary mt-4" onclick="validateform()">Submit</button></center>
                                        </form>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


         <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#option').change(function () {
                  $('#proxy').fadeToggle();
                });
            });

        </script>
        
         <script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
    </script>
    
    
    <script>
    	
    	function validateform(){
    	var valid = true;
    	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|in)";
    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
    	var firstName=$("#firstName").val();
    	var lastName  = $('#lastName').val();
    	var contactNo  = $('#contactNo').val();
    	var email = $('#email').val() ;
    	var dateOfBirth  = $('#dob').val();
    	var password  = $('#password').val();
    	var option  = $('#option').val();
    	var proxyNumber  = $('#proxyNumber').val();
    	console.log("email:: "+email);
    	var tetee=document.getElementsByName("physicalCard");
    	if(firstName.length <= 0){
    	$("#firstError").html("Please enter your first name");

    	valid = false;
    	}
    	 if(lastName.length <= 0){
    		$("#lastError").html("Please enter your last name");
    		valid = false;
    	}
    	
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact no");
    		valid = false;
    	}
    	if(email.length  <= 0){
    		$("#mailError").html("Please enter your email Id ");
    		valid = false; 
    	}else if(!email.match(pattern)) {
    		console.log("inside mail")
    		$("#mailError").html("Enter valid email Id");
    		valid = false;	
    	}
    	if(password.length  <= 0){
    		$("#passError").html("Please enter password");
    		valid = false; 
    	}
    	
    	if(dateOfBirth.length  <= 0){
    		$("#doberror").html("Please select date of birth");
    		valid = false; 
    	}
    	
    	/* if(option == "on"){
    		if(proxyNumber.length  <= 0){
        		$("#proxyNumbererror").html("Please enter the proxy number");
        		valid = false; 
        	} 
    	} */
    	
    	
    	var today = new Date();
    	var dd = today.getDate();
    	var mm = today.getMonth()+1;
    	var yyyy = today.getFullYear();

    	today = yyyy+'-'+"0"+mm+'-'+dd;
    	console.log("current date:::::"+today);
    	if(new Date(dateOfBirth) >= new Date("1998-12-31")) {
    				$("#doberror").html("You must be at least 18 years old to sign up");
    				valid = false;
    	}
    	
    	console.log("valid: "+valid);

    if(valid == true) {
    	$("#formId").submit();
    } 
    
    var timeout = setTimeout(function(){
        $("#firstError").html("");
    	$("#lastError").html("");
    	$("#ferror").html("");
    	$("#mailError").html("");
    	$("#proxyNumbererror").html("");
    	$("#passError").html("");
    	$("#doberror").html("");
    }, 4000);
    }
    </script>
    
    
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>
    
	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
    
    </body>

</html>