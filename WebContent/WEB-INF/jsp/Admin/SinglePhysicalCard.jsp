<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Physical Card</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
        <script	src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
        
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->
    
    <script type="text/javascript">
    $(document).ready(function() {
// 		 getAjaxData();
		 /* var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf']
        });

        table.buttons().container()
                .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)'); */
		/*  $('#datatable').DataTable({
             responsive: true,
             paging: false,
             info: false
         }); */

         //Buttons examples
         /* var table = $('#datatable-buttons').DataTable({
             lengthChange: false,
             buttons: ['copy', 'excel', 'pdf']
         }); */

         /* table.buttons().container()
                 .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)'); */
	 });

	 function fetchMe(value){
		var paging=value;
		//var daterangeVal=$('#daterange').val();
		console.log(paging);
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/CardListPhyByPage",
	data:{page:paging,size:'10'},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
			
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});

		$('#abhijit').append(trHTML);
		 
		 

		}
		else
		{
			$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});

			$('#abhijit').append(trHTML);
			
		}
		 
	}

	});
			 }
		 
// 	 function getFilteredData(){
// 		 var category='${category}';
// 		 var daterangeVal=$('#daterange').val();
// 		 console.log("category"+category+"daterange"+daterangeVal);
// 		 fetchMe(0,category,daterangeVal);
// 	 }
	 
	 function  getAjaxData(){
		
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/CardListPhyByPage",
				data:{
					page:paging,
					size:'10'
					},
			dataType:"json",
			success:function(data){
				 console.log("Response get");
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});

					$('#abhijit').append(trHTML);
					
					}
					else
					{
						$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].name + '<br>Mobile No.:<a href="${pageContext.request.contextPath}/Admin/UserTransaction/'+data.jsonArray[i].contactNo+'/'+data.jsonArray[i].cardId+'">'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'</td><td><a href="${pageContext.request.contextPath}/Admin/SingleCard/'+data.jsonArray[i].email+'/'+data.jsonArray[i].cardId+'/'+data.jsonArray[i].contactNo+'">'+ data.jsonArray[i].cardId+'</a></td>'+'<td>'+ data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].created+'</td>'+'<td>'+data.jsonArray[i].status+'</td></tr>';});

						$('#abhijit').append(trHTML);
						
					}
					
					 $(function () {
							console.log("inside funt...and total pages:"+data.totalPages);
							
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
						
				         }
						 });
						});
						}
					 });
				 }
</script>	
    

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Physical Card</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <div class="row">
                                   		<div class="col-md-6 offset-md-6">
                                   			<form action="${pageContext.request.contextPath}/Admin/SingleCardPhyByPage" method="post" class="form-inline" style="float: right;" id="formId">
			                                    <div class="form-group">
				                                    <input id="username" name="username" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
				                                    <button class="btn btn-primary" onclick="fetchSincgleCard()" type="button" >Search</button>
			                                   	</div>
			                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
			                                </form>
                                   		</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User Details</th>
                                                <th>Card Id</th>
                                                <th>Balance</th>
                                                <th>Issue Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${cardList}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
								
									<td> Name:<c:out value="${card.name}" default="" escapeXml="true" /><br>
										 Email:<c:out value="${card.email}" default="" escapeXml="true" /><br>
										 DOB:<c:out value="${card.dob}" default="" escapeXml="true" /><br>
										 <a href="${pageContext.request.contextPath}/Admin/UserTransaction/${card.contactNo}/${card.cardId}">ContactNo:<c:out value="${card.contactNo}" default="" escapeXml="true" /></a></td>
										<td> <a href="${pageContext.request.contextPath}/Admin/SingleCard/${card.email}/${card.cardId}/${card.contactNo}"><c:out value="${card.cardId}" default="" escapeXml="true" /></a></td>
										<td><c:out value="${card.balance}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.created}" default="" escapeXml="true" />
									</td>
									<c:choose>
									<c:when test="${card.status == 'Active'}">
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
									</c:when>	
									<c:otherwise>
									<td style="color: red"> <c:out value="${card.status}" default="" escapeXml="true" /></td>
									</c:otherwise> 
									</c:choose>
								</tr>		 
							</c:forEach>
                                    </tbody> 
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <!-- <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true,
                    paging: false
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>
 -->
 
 <script>
 function fetchSincgleCard(){
	var username=$("#username").val();
	var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true){
			$("#formId").submit();
		}
 }
 </script>
 <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

    </body>

</html>