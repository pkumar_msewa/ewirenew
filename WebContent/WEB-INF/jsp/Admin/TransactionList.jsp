
<!doctype html>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> --%>
<html lang="en">
<head>
<title>Transaction List</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/linearicons/style.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/chartist/css/chartist-custom.css"/>">
<!-- MAIN CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/main.css"/>">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/demo.css"/>">
<!-- GOOGLE FONTS -->

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
	rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/resources/assets/img/apple-icon.png"/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value="/resources/assets/img/favicon.png"/>">
	<script>var contextPath = "${pageContext.request.contextPath}";</script>
		
<%-- 		 <!-- Select2  css_file -->
<script src="${pageContext.request.contextPath}/resources/admin/js/select/select2.css"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/select/select2.min.css"></script>
		
		  <!-- Select2  js_file -->
<script src="${pageContext.request.contextPath}/resources/admin/js/select/select2.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/select/select2.min.js"></script> --%>
		
<!-- 		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script> -->
<!-- 		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script> -->
<!-- 		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script> -->
<!-- 		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script> -->
<!-- 		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" /> -->
<!-- 		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" /> -->
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>

<!-- Daterange picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.css" />


<script type="text/javascript">
function fetchMe(value,startDate,endDate){
	var paging=value;
	console.log("inside function " + value);
		
	 $.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/TransactionList",
	data:{page:paging,
		size:'20',
			fromDate:startDate,
			toDate:endDate
	},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
			$(".testingg").empty();
			$(data).each(function(i,item){
				
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="clientname">' + data[i].username + '</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].created + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount  
				+'</td><td>'
				var status=data[i].status ;
				if( status=='Success'){
					//alert(status);
					trHTML+='<h5><span class="label label-success">' 
			}
				else if(data[i].status =='Failed'){
					trHTML+=' <h5><span class="label label-danger">'
				}else{
					trHTML+='<h5><span class="label label-warning">'
				}
				trHTML+= data[i].status +'</h5></td></tr>';});
			  $('#editedtable').append(trHTML);
			}
			else
			{
				$(data).each(function(i,item){
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="clientname">' + data[i].username + '</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].created + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td>' + data[i].status +'</td></tr>';});
				  $('#editedtable').append(trHTML);
			}
	}
	}); 
	}
	 $(document).ready(function() {
 	//datatableEnable();
 		populateDropDownList();
		getAllData();
		var check='${check}';
		console.log(check);
// 		if(check=="true"){
// 			 $("form#formId").submit();
// 		}
	 });
	 
	 // The bellow code is working fine in case of ajax call. As of now it is not in use.
	  function getAllData (){
		
		  var startDate='${startDate}';
			var endDate='${endDate}';
		  
		  console.log("all Data"+startDate+":"+endDate);
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/TransactionList",
				data:{page:paging,
					size:'20',
					fromDate:startDate,
					toDate:endDate,
				},
			dataType:"json",
			success:function(data){
				var total = 0;
//				alert(data[0].transactionRefNo);
				var trHTML='';
				 if(data!=''){
					if(trHTML==''){
					$(".testingg").empty();
					$(data).each(function(i,item){
						console.log("Ref No " + data[i].transactionRefNo)
						total = data[i].totalPages;
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].created + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + 
						'</td><td>' + data[i].status +'</td></tr>';});
					  $('#editedtable').append(trHTML);
					}
					else
					{
						$(data).each(function(i,item){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].created + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td>' + data[i].status +'</td></tr>';});
						  $('#editedtable').append(trHTML);
					}
					
					  $(function () {
							console.log("inside funt..."+total);
						 $('#paginationn').twbsPagination({
							 totalPages:total,
							 visiblePages: 20,
				         onPageClick: function (event, page) {
				        	 
				        	 page = page - 1;
				        	 console.log("Page size :: " + page);
				        	fetchMe(page,startDate,endDate);
				         }
						 });
						}); 
			}else{
				trHTML += '<tr class="odd"><td class="dataTables_empty" colspan="8" valign="top">No data available in table</td></tr>';
				$('#editedtable').append(trHTML);
			}
			}
		 });
	 }
	 
</script>
<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
				$(function() {
					$( "#toDate" ).datepicker({
						format:"yyyy-mm-dd"
					}).on('change', function() {
						   $('.datepicker').hide();
					  });
					$( "#fromDate" ).datepicker({
						format:"yyyy-mm-dd"
					}).on('change', function() {
						   $('.datepicker').hide();
					  });
				});
	</script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
</head>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">TRANSACTION LIST</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
					  <form id="formId" action="<c:url value="/Admin/filteredTransactionList"/>" method="post" onsubmit="return validate();" >
								<div class="row">
									<div class="col-md-12">
										<h4
											style="position: absolute; font-size: 14px; font-weight: 600; margin-top: 2px;">Select
											Date Range*</h4>
										<div class="col-md-4 col-sm-4 col-xs-3" id="reportrange"
											style="background: #fff; cursor: pointer; border: 1px solid #ccc; margin-top: 26px; padding: 5px; border-radius: 4px;">
											<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
											<span></span> <b class="caret"></b>
										</div>

										<input type="hidden" id="daterange" name="daterange" value=""
											class="form-control" readonly />

																			<div class="col-md-2" class="form-group">
																				<label>From</label> <input type="text" id="fromDate"
																					name="fromDate" value="${startDate }" class="form-control"
																					readonly />
																				<p id="fromDateMsg"></p>
																			</div>
																			<div class="col-md-2" class="form-group">
																				<label>To</label> <input type="text" id="toDate" name="toDate"
																					value="${endDate }" class="form-control" readonly />
																				<p id="toDateMsg"></p>
																			</div>
										<div class="col-md-3 col-sm-3 col-xs-3">
											<label>Clients*</label> <select id="userId" name="val"
												 class="form-control" >
												<option value="All">All Clients</option>
												<c:if
													test="${requestScope.allClients !=null && !empty allClients}">
													<c:forEach var="allClients"
														items="${requestScope.allClients}">
														<option value="${allClients.userId}"
															${fn:contains(userId,allClients.userId) ? 'selected="selected"' : ''}>
															<c:out value="${allClients.name}" /></option>
													</c:forEach>
												</c:if>
											</select>
											<p id="userMsg"></p>
										</div>
									<div class="col-md-3 col-sm-3 col-xs-3">
											<label>Services*</label> <select id="serviceId"
												name="service" class="form-control">
												<option value="All">All Services</option>
											</select>
											<p id="serviceMsg"></p>
										</div>
									<!-- 						  <input type="hidden" id="pageId" name="page" value="0" class="form-control" /> -->
										<!-- 						  <input type="hidden" id="sizeId" name="size" value="2" class="form-control" /> -->
										<!-- 						  <sec:csrfInput/> -->
										<div class="col-md-2 col-sm-2 col-xs-2">
											<button type="submit" class="btn btn-primary" title="Search"
												style="margin-top: 25px;">
												<span class="glyphicon glyphicon-filter"></span>
											</button>
										</div>
									</div>
								</div>
								<!-- 					 onclick="getAllData();"  -->
					  </form><p></p>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable" class="table table-striped table-bordered date_sorted">
									<thead>
										<tr >
										    <th>S.NO</th>
										    <th>Client Name</th>
											<th>Transaction ID</th>
											<th>Transaction Date</th>
											<th>Description</th>
											<th>Retrieval Ref No</th>
											<th>Amount</th>
											
											<th>Status</th>
										</tr>	</thead>
										</table>
									<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>	
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	
		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">Msewa
						Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
<!-- 	data table -->



<script src="<c:url value='/resources/js/modernizr.js'/>"></script>	
  <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>

<script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>
<script
            src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>
	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	
		 <script
            src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
            
            
            
            
	<script>
		$('#userId').change(function(){
			var userId=$('#userId').val();
			if(userId =='All'){
				$('#serviceId')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option id="optId2" value="All" selected>All Services</option>')
			    .val('');
			}else{
				$('#serviceId')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option id="optId2" value="" selected>Loading...</option>')
			    .val('');
			}
			console.log(userId);
			 $.ajax({
					type:"GET",
					url:"${pageContext.request.contextPath}/Admin/serviceByClient",
					data:{
						val:userId
					},
				dataType:"json",
				success:function(data){
					if(data!=''){
					$("#serviceId").removeAttr("disabled");
					 $("#optId2").remove();
					$('#serviceId')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option id="optId2" value="All" selected>All Services</option>')
							 $(data).each(function(i,item){
					            {
					             console.log(data[i].serviceId+":"+data[i].serviceName);
					             var div_data="<option value="+data[i].serviceId+">"+data[i].serviceName+"</option>";
					             console.log(div_data);
					            $(div_data).appendTo('#serviceId'); 
					            }
					            });  
				 }if(data==''){
						$("#optId2").remove();
						$('#serviceId')
					    .find('option')
					    .remove()
					    .end()
					    .append('<option id="optId2" value="" selected >No Data Found</option>')
					    $("#serviceId").prop("disabled", "disabled");
					}
				}
		});
	});
	</script>
	<script>
	 function validate(){
		 
			var user = $('#userId').val();
			var service = $('#serviceId').val();
// 			if (fDate == null || fDate == '') {
// 				$('#fromDateMsg').html('<p style="color:red;">Please select from date</p>');
// 				$('#fromDateMsg').show();
// 				return false;
// 			} if(toDate == null || toDate == ''){
// 				$('#fromDateMsg').hide();
// 				$('#toDateMsg').html('<p style="color:red;">Please select to date</p>');
// 				$('#toDateMsg').show();
// 				return false;
// 			}
			if(user == null || user == ''){
				//$('#fromDateMsg').hide();
				$('#userMsg').html('<p style="color:red;">Please select client</p>');
				$('#userMsg').show();
				return false;
			}if(service == null || service == ''){
				$('#userMsg').hide();
				$('#serviceMsg').html('<p style="color:red;">Please select service</p>');
				$('#serviceMsg').show();
				return false;
			} if(user!='' &&  service!=''){
				return true;
			}else {
				return false;
			}
		}
	 
	 
	 function populateDropDownList(){
		 var serviceList='${jsonString}';
		 var jsonObj = $.parseJSON( serviceList);
		 for (var key in jsonObj) {
			  if (jsonObj.hasOwnProperty(key)) {
		             var div_data="<option value="+jsonObj[key].serviceId+">"+jsonObj[key].serviceName+"</option>";
		             console.log(div_data);
		            $(div_data).appendTo('#serviceId'); 
			  }
			}
	 }
	 
	</script>
	
	<script type="text/javascript">
$(function() {
	var startDate='${startDate}';
	var endDate='${endDate}';
	//var date1=new Date(startDate);
	//startDate = startDate +" 00:00:00";
	//var start=parseInt(startDate);
	console.log("start:"+startDate);
 var start = moment(startDate);
    var end = moment(endDate);
    console.log("startDate:"+start+":"+end);
    function cb(start, end) {
    	console.log(start+":"+end);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#daterange').val(start + ' - ' + end)
        console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script>

<script>
// $(document).ready(function() {
// 	$('#userId').select2();
// });
</script>
<script>
// $(document).ready(function() {
//     $(".select2-menu").select2();
// });
</script>

</body>

</html>
