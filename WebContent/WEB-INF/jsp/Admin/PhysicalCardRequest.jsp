<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Physical Card Request</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

		
		
       	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
		

		 <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
        	 <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
	
<script>
var contextPath="${pageContext.request.contextPath}";
var spinnerUrl = "<img src='${pageContext.request.contextPath}/resources/spinner2.gif' style='height:150px;width:150px;'>"
</script>


 <script type="text/javascript">
    $(document).ready(function() {
    	$("#spinner").html(spinnerUrl);	 
		 getAjaxData();
		
	 });

	 function fetchMe(value){
		 $(".testingg").empty();
			$("#spinner").html(spinnerUrl);
		var paging=value;
		var daterangeVal=$('#reportrange').val();
		console.log(paging);
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/PhysicalCardRequestUserList",
	data:{page:paging,size:'10',Daterange:daterangeVal},
	dataType:"json",
	success:function(data){
		$("#spinner").html('');
	var trHTML='';
		if(trHTML==''){
		$(data.jsonArray).each(function(i,item){
			if(data.jsonArray[i].status === "Requested"){
				if(data.jsonArray[i].activationCode == "NA"){
		trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
		 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
		+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
				}else{
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
					 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
					+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

				}
				}else if(data.jsonArray[i].status === "Shipped"){
					if(data.jsonArray[i].activationCode == "NA"){
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
			 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
			+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';
					}else{
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
						 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

					}
		}else{
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
			+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
			
		}});
			
		
		$('#ashok').append(trHTML);

		}
		else
		{
			$(data.jsonArray).each(function(i,item){
				if(data.jsonArray[i].status === "Requested"){
					if(data.jsonArray[i].activationCode == "NA"){
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
			 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
			+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
					}else{
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
						 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

					}
					}else if(data.jsonArray[i].status === "Shipped"){
						if(data.jsonArray[i].activationCode == "NA"){
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
				 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
				+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';
						}else{
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
							 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

						}
			}else{
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
				+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
				
			}});
			$('#ashok').append(trHTML);
			
		}
		 
	}

	});
			 }
	 
	 function  getAjaxData(){
		
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/PhysicalCardRequestUserList",
				data:{
					page:paging,
					size:'10'
					},
			dataType:"json",
			success:function(data){
				$("#spinner").html('');
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						if(data.jsonArray[i].status === "Requested"){
							if(data.jsonArray[i].activationCode == "NA"){
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
					 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
					+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
							}else{
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
								 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
								+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

							}
							}else if(data.jsonArray[i].status === "Shipped"){
								if(data.jsonArray[i].activationCode == "NA"){
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
						 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
									 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

								}
					}else{
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
						
					}});
					$('#ashok').append(trHTML);
					
					}
					else
					{
						$(data.jsonArray).each(function(i,item){
							
							if(data.jsonArray[i].status === "Requested"){
								if(data.jsonArray[i].activationCode == "NA"){
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
						 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
									 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

								}
								}else if(data.jsonArray[i].status === "Shipped"){
									if(data.jsonArray[i].activationCode == "NA"){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
							 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'<td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></td></tr>';
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
										 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
										+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

									}
						}else{
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
							
						}});
						$('#ashok').append(trHTML);
						
					}
					
					  $(function () {
							console.log("inside funt...and total pages:"+data.totalPages);
							
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
						
				         }
						 });
						}); 
						}
					 });
				 }
</script>	
    




    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Physical Card Request</h4>
                                    <div class="clearfix"></div>
                                    <div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                        	<div class="col-12">
                        		<div class="card-box">
                        			<div class="row">
                        				<div class="col-6">
	                        				<form class="inline" action="">
												<div class="form-row">
													<div class="col-sm-3">
														<div class="form-group">
															<select name="status" id="status" class="form-control mr-sm-2">
									                            <option value="All">All</option>
									                            <option value="Requested">Requested</option>
									                            <option value="Shipped">Shipped</option>
									                            <option value="Declined">Declined</option>
									                            <option value="Active">Active</option>
									                            <option value="Received">Received</option>
								                            </select>
														</div>
													</div>
													<div class="col-sm-5">
														<div id="" class="pull-left" style="cursor: pointer;">
															<label class="sr-only" for="filterBy">Filter By:</label>
															<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
														</div>
													</div>
													<div class="col-sm-2">
														<button class="btn btn-primary" onclick="filterResult()" type="button">Filter</button>
													</div>
												</div>
											</form>
	                        			</div>

										<div class="col-6">
											<form class="form form-inline pull-right">
												<div class="form-group">
						                        	<input id="username" name="userName" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
					                            	<span id="err" style="color: red;"></span>
						                        </div>
						                        <div class="form-group">    
						                        	<button class="btn btn-primary" onclick="fetchSincgleCard()" type="button" >Search</button>
					                            </div>
											</form>
										</div>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
							<div class="col-12">
								<div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>UserDetails</th>
                                                <th>Address</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Comment</th>
                                                <th>Action1</th>
                                                <th>Action2</th>
                                            </tr>
                                        </thead>
                                       <tbody id="ashok">
                                    </tbody>
                                    </table>
                                     <center><span id="spinner"> </span></center>
                                      <nav style="float: right;">
										<ul class="pagination" id="paginationn"></ul>
									</nav>
                                </div>
							</div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 Â© Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
         <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
       
       <!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
       
       
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/User/assets/js/all.js"></script>

        
        <script>
        function changeStatus(valu,dId){
        	var dsds="status_"+dId;
        	var status=$('#'+dsds).val();
        	console.log("Status card"+status);
        	var id=valu;
        	console.log("id card"+id);
        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Admin/UpdatePhysicalCardStatus",
    			dataType : 'json',
    			data : JSON.stringify({
    				"id" :""+id+"",
    				"status" :""+status+"",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#restpn").modal('hide')
    				if (response.code.includes("S00")) {
    					swal("Congrats!!", response.message, "success");
    					window.location.href='${pageContext.request.contextPath}/Admin/Card/PhysicalCardRequest';
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Sorry!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
        	
        }
        </script>



 <script>
        function changeStat(valu,dId){
        	var dsds="stat_"+dId;
        	var cm="comm_"+dId;
        	var status=$('#'+dsds).val();
        	var comment=$('#'+cm).val();
        	console.log("Status card"+status);
        	var id=valu;
        	console.log("id card"+id);
        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Admin/UpdatePhysicalCardStatus",
    			dataType : 'json',
    			data : JSON.stringify({
    				"id" :""+id+"",
    				"status" :""+status+"",
    				"comment" :""+comment+"",
    				
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#restpn").modal('hide')
    				if (response.code.includes("S00")) {
    					swal("Congrats!!", response.message, "success");
    					window.location.href='${pageContext.request.contextPath}/Admin/Card/PhysicalCardRequest';
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Sorry!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
        	
        }
        </script>
        
<script>
        function activateCard(valu,dId){
        	var dsds="kitNo_"+dId;
        	var kitNo=$('#'+dsds).val();
        	console.log("Status card"+kitNo);
        	var id=valu;
        	console.log("id card"+id);
        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Admin/AssignPhyCard",
    			dataType : 'json',
    			data : JSON.stringify({
    				"id" :""+id+"",
    				"kitNo" :""+kitNo+"",
    			}),
    			
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				$("#restpn").modal('hide')
    				if (response.code.includes("S00")) {
    					swal("Congrats!!", response.message, "success");
    					window.location.href='${pageContext.request.contextPath}/Admin/Card/PhysicalCardRequest';
    					}else{
    						swal({
    							  type: 'error',
    							  title: 'Sorry!!',
    							  text: response.message
    							});
    				}			
    			}
    		});
        	
        }
        </script>


        
         <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		
		<script>
		function filterResult(){
			var status=$("#status").val();
			var daterange=$("#reportrange").val();
			 var paging='0';
			 var size='';
			 console.log("under ready...");
			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/Card/PhysicalCardRequestList",
					data:{
						page:paging,
						size:'10',
						status:status,
						Daterange:daterange
						},
				dataType:"json",
				success:function(data){
					$("#status").val(data.status);
					$("#reportrange").html(data.date);
					 console.log("Response get");
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].status === "Requested"){
								if(data.jsonArray[i].activationCode == "NA"){
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
						 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
						+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
									 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

								}
								}else if(data.jsonArray[i].status === "Shipped"){
									if(data.jsonArray[i].activationCode == "NA"){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
							 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'<td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></td></tr>';
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
										 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
										+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

									}
						}else{
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
							
						}});
						$('#ashok').append(trHTML);
						
						}
						else
						{
							$(data.jsonArray).each(function(i,item){
								
								if(data.jsonArray[i].status === "Requested"){
									if(data.jsonArray[i].activationCode == "NA"){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
							 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
							+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
										 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
										+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

									}
									}else if(data.jsonArray[i].status === "Shipped"){
										if(data.jsonArray[i].activationCode == "NA"){
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
								 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
								+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'<td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></td></tr>';
										}else{
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
											 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
											+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

										}
							}else{
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
								+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
								
							}});
							$('#ashok').append(trHTML);
							
						}
						
						  $(function () {
								console.log("inside funt...and total pages:"+data.totalPages);
								
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
								 visiblePages: 7,
					         onPageClick: function (event, page) {
					        	 fetchMe(page-1);
							
					         }
							 });
							}); 
							}
						 });
		}
		
		</script>
		
		
		
		<script>
		function fetchSincgleCard(){
			
			var username=$("#username").val();
			var valid=true;
				if(username == ''){
					valid=false;
					$("#err").html("Please enter the username")
				}
				
				else if(username.length !=10){
					valid=false;
					$("#err").html("Please enter the valid username")
				}
				
				if(valid == true){
					
					 console.log("under ready...");
					 $.ajax({
							type:"POST",
							url:"${pageContext.request.contextPath}/Admin/PhysicalCardRequestSingleUser",
							data:{
								username:username
								},
						dataType:"json",
						success:function(data){
							 console.log("Response get");
							var trHTML='';
								if(trHTML==''){
								$(".testingg").empty();
								$(data.jsonArray).each(function(i,item){
									if(data.jsonArray[i].status === "Requested"){
										if(data.jsonArray[i].activationCode == "NA"){
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
								 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
								+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
										}else{
											trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
											 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
											+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

										}
										}else if(data.jsonArray[i].status === "Shipped"){
											if(data.jsonArray[i].activationCode == "NA"){
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
									 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'<td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></td></tr>';
											}else{
												trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
												 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
												+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

											}
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
									
								}});
								$('#ashok').append(trHTML);
								
								}
								else
								{
									$(data.jsonArray).each(function(i,item){
										
										if(data.jsonArray[i].status === "Requested"){
											if(data.jsonArray[i].activationCode == "NA"){
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
									 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
									+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
											}else{
												trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
												 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Shipped">Shipped</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
												+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

											}
											}else if(data.jsonArray[i].status === "Shipped"){
												if(data.jsonArray[i].activationCode == "NA"){
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
										 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
										+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td><input type="text" placeholder="Enter Kit No." maxlength="12" autocomplete="off" id='+'kitNo_'+i+'></td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td><td><button type="button" class="btn btn-sm btn-danger" onclick="activateCard('+data.jsonArray[i].id+','+i+')">Assign</button></td></tr>';
												}else{
													trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+'<select class="form-control" name="status" id='+'stat_'+i+'>'
													 +'<option value="'+data.jsonArray[i].status+'" selected>'+data.jsonArray[i].status+'</option>'+'<option value="Declined">Declined</option>'+'</select></td>'
													+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td><td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" onclick="changeStat('+data.jsonArray[i].id+','+i+')">Submit</button>'+'</td></tr>';

												}
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'</td>'+'<td>Address:'+ data.jsonArray[i].address+'<br> City:'+ data.jsonArray[i].city+'<br> Pincode:'+ data.jsonArray[i].pincode+'</td>'+'<td>Requested Date:'+ data.jsonArray[i].issueDate+'<br> Updated date:'+ data.jsonArray[i].lastModified+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'
										+'<td>ActivationCode:'+data.jsonArray[i].activationCode+'</td<td>'+'<textarea type="text" placeholder="Enter Comment" id='+'comm_'+i+' class="form-control" maxlength="150">'+data.jsonArray[i].comment+'</textarea></td></tr>';
										
									}});
									$('#ashok').append(trHTML);
									
								}
								
								  $(function () {
										console.log("inside funt...and total pages:"+data.totalPages);
										
									 $('#paginationn').twbsPagination({
										 totalPages: data.totalPages,
										 visiblePages: 7,
							         onPageClick: function (event, page) {
							        	 fetchMe(page-1);
									
							         }
									 });
									}); 
									}
								 });
			
				}
		}
		
		</script>
		
		<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

    </body>

</html>