<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Approved Group Request | EWire</title>
<!-- App favicon -->
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

<!-- DataTables -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- App css -->
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
<style>
.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/resources/spinner.gif) center no-repeat #fff;
		}
</style>
</head>
<body oncontextmenu="return false">

<div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Approved Request</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
						
						<!-- datepicker strats here -->
						
						<div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
										
											<form action="<c:url value="/Admin/AcceptedUserGroup"/>" 
												method="post" class="form form-inline">
												<%-- <div class="form-group">
													<label>Select Group</label> <select
													 class="form-control" name="contact" id="operatorId"
													style="width: 100% !important; margin-left: -8px; margin-top: -2px;">	
														<option value="All" selected>Select</option>												
														<c:forEach var="operator" items="${groupList}">
															<option value="${operator.contactNo}">${operator.groupName}</option>
														</c:forEach>
													</select>
												</div>
												<input type="hidden" id="contact" name="contact" value="${operator.contactNo}"/> --%>
												<div class="form-group">
													<div id="" class="pull-left" style="cursor: pointer;">
														<label class="" for="filterBy" style="justify-content: left;">Filter By:</label>
														<input id="reportrange" name="reportrange" class="form-control"  value="${dat}" readonly="readonly"/>
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary"
														title="Search" style="margin-left: 10px; margin-top: 20px;">
														Search
													</button>
												</div>
											</form>
											<!-- <form class="inline" action="${pageContext.request.contextPath}/Admin/GroupRequestList" method="post">
												 <div class="form-row">														
													
													<div class="col-sm-3">
														
													</div>
													<div class="col-sm-3">
														<button class="btn btn-primary" id="">Filter</button>
													</div>
												 </div> 
											</form>		 -->									
										</div>
										
										 <div class="col-md-6 col-sm-6 col-xs-12 pt-4">
											<form action="<c:url value="/Admin/SingleUserGroupRequest"/>" 
												method="post" id="myForm" class="form form-inline">
			                            	<div class="form-group mr-2">
				                            	<input id="username" name="mobileNoId" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
				                           		<input id="remark" type="hidden" name="remark" value="accepted" class="form-control" />
				                            </div>
				                            <div class="form-group">    
				                            	<button class="btn btn-primary" onclick="getSingleUserDetails()" type="button">Search</button>
			                                </div>	
			                                <span id="err" style="color: red;"></span>
			                               </form>
										</div>  
										
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User details</th>
                                                <th>Registered Date</th>
                                                <th>Group Name</th>                                                                                               
                                                <th>Action</th>
                                                <th>Action</th>										
                                            </tr>
                                        </thead>
                                       <tbody>
                                       <c:forEach items="${userData}" var="card" varStatus="loopCounter">
									<tr>
									<td>${loopCounter.count}</td>
									<td> <b>Name:</b><c:out value="${card.firstName}" default="" escapeXml="true" /><br>
										<b>Mobile No:</b> <%-- <a href="${pageContext.request.contextPath}/Admin/userDetails/${card.contactNO}"> --%>${card.contactNO}<!-- </a> --><br/>
										<b>Email Id</b> <c:out value="${card.email}" default="" escapeXml="true" />
										<b>DOB:</b> <c:out value="${card.userDetail.dob}" default="" escapeXml="true" />
										<b>Account Type:</b><c:out value="${card.accountType}" default="" escapeXml="true" />
									</td>
									<td><fmt:formatDate pattern="dd-MM-yyyy" value="${card.issueDate}" /></td>
									<td><c:out value="${card.group}" default="" escapeXml="true" /></td>	
									<td>
										<c:choose>
										<c:when test="${card.changeGroupStatus eq false}">
											<button type="submit" class="btn btn-sm btn-danger" id="changeGroup_s" onclick="changeGroup('+data.jsonArray[i].contactNO+')" value="">Assign New Group</button>
										</c:when>
										<c:otherwise>
											<button type="submit" class="btn btn-sm btn-danger" id="changeGroup_s" onclick="changeGroup('+data.jsonArray[i].contactNO+')" value="" disabled>Assign New Group</button>
										</c:otherwise>
									</c:choose>
									</td>
									<td>
										<c:choose>
										<c:when test="${card.group eq 'None'}">
											<button type="button" class="btn btn-danger" onclick="deleteReason('+data.jsonArray[i].contactNO+');" id="deleting" disabled>Delete</button>
										</c:when>
										<c:otherwise>
											<button type="button" class="btn btn-danger" onclick="deleteReason('+data.jsonArray[i].contactNO+');" id="deleting">Delete</button>
										
										</c:otherwise>
										</c:choose>
									</td>								
									<%-- <td>
									<input type="hidden" value="${slist.contactNo}" name ="mobileNo" id ="mobileNo">
									<a onclick="deleteUser('${card.username}');">
									<button class="btn btn-danger" id="deleting">Delete</button></a></td> --%>
								</tr>		 
							</c:forEach>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_success_msg" class="alert alert-success"></center></B>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_error_msg" class="alert alert-danger"></center></B>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
	
	<!-- MODAL BEGINS -->
	
	<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
	     aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	           
	            <div class="modal-header">
	                <button type="button" class="close" 
	                   data-dismiss="modal">
	                       <span aria-hidden="true">&times;</span>
	                       <span class="sr-only">Close</span>
	                </button>
	                <h4 class="modal-title" id="myModalLabel">
	                    Change Group
	                </h4>
	            </div>
	          
	            <div class="modal-body">
	                <form class="form-horizontal" role="form">
	                  <div class="form-group">	                    
	                    <div class="col-sm-10">
	                        <select class="form-control" name="contactNo" id="groupop"
								style="width: 100% !important; margin-left: -8px; margin-top: -2px;">	
							<option value="All" selected>Select Group</option>												
							<c:forEach var="operator" items="${groupList}">
								<option value="${operator.contactNo}">${operator.groupName}</option>
							</c:forEach>
							</select>
							<input type="hidden" id="groupMob" name="groupMob" value="${operator.contactNo}"/>
							<input type="hidden" id="userContactNo" name="userContactNo" />
	                        <span class="error" style="color: red;" id="reasonErrorHorizontal"></span>
	                    </div>
	                  </div>	                  
	                </form>
	            </div>
	            
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default"
	                        data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary" onclick="changeGroupPost()">
	                    Add Group</button>
	            </div>
	        </div>
	    </div>
	</div>
	
	
	<!-- MODAL ENDS -->
	
	<div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Deletion Reason
                </h4>
            </div>
            
          
            <div class="modal-body">
                
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="address1">Reason</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="address1" placeholder="Reason" name="address1" required/>
                        <span class="error" style="color: red;" id="reasonError"></span>
                    </div>
                  </div>
                  <input type="hidden" id="deleteContact" name="deleteContact" />                 
                </form>
            </div>
            
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary" onclick="deleteUser()">
                    Add Reason
                </button>
            </div>
        </div>
    </div>
</div>



        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>
        
        <script>
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'excel', 'pdf', 'csv' ]
            } );
         
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
        
        </script>
        
         <script>
    function changeGroup(va) {
    	var contact = va;    	
    	$("#userContactNo").val(contact);
    	$("#myModalHorizontal").modal("show");
    }
    
    function changeGroupPost() {
    	var date=$("#reportrange").val();	
    	var contactNo = $("#groupop").val();
    	
    	var userContactNo = $("#userContactNo").val();
    	console.log("userContactNo :: "+userContactNo);
    	console.log("contact:: "+contactNo);
		 var paging='0';
		 var size='';
    	$.ajax({
    		type : "POST",
    		url : "${pageContext.request.contextPath}/Admin/ChangeUserGroup",
    		data : {
    			page:paging,
				size:'10',
				Daterange:date,
				contact : contactNo,
				userContactNo : userContactNo
    		},
    		dataType:"json",
    		success:function(data) {
    			$("#myModalHorizontal").modal("hide");
    			$("#common_success_true").modal("show");
    			$("#common_success_msg").html(data.message);
    			var timeout = setTimeout(function() {
    				$("#common_success_true").modal("hide");
    			}, 2000);
    		}
    	});
    }
    </script>
    
      <script>
        
        function deleteReason(username) {
        	$("#deleteContact").val(username);
        	$("#myModalDelete").modal("show");
        }

function deleteUser() {
	var username = $("#deleteContact").val();
	var deleteReason = $("#address1").val();
	var valid = true;
	console.log("username:: "+username);
	console.log("reason:: "+deleteReason)
	if(deleteReason.length <= 0) {
		$("#reasonError").html("please enter deletion reason");
		valid = false;		
	}
	if(valid) {
		$.ajax({
			type : "POST",
			datatype : 'json',
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/Admin/DeleteGroup",
			data: JSON.stringify({
				"username" : username,
				"status" : deleteReason
			}),
			success: function (response) {
				console.log("success");
				if(response.code.includes("S00")) {
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				} else {
					$("#common_error_true").modal("show");
		  			$("#common_error_msg").html(response.message);
		  			setTimeout(location.reload.bind(location), 2000);
				}
			}
		});
	}
}
</script>
    
        
        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
 	  <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 




	<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>


		
</body>
</html>