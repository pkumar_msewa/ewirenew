<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Services List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	<%-- <link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" /> --%>
	<link
	href="${pageContext.request.contextPath}/resources/admin/css/datatables/css/dataTables.bootstrap.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
</head>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">SERVICES LIST</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
						<div class="row">
						<form id="formId" class="form-auth-small" action="${pageContext.request.contextPath}/Admin/serviceList" method="get">
<%-- 							<input type="hidden" id="userId" name="userId" value="${userId}"></input> --%>
									<div class="col-md-12">
										<!-- 									<label for="signin-email" class="control-label sr-only">Email</label> -->
									<div class="col-md-3 col-sm-3 col-xs-3">
											<select id="userId" name="val" class="form-control" >
												<option value="">All Service</option>
												 <c:if test="${requestScope.allClients !=null && !empty allClients}">
													<c:forEach var="allClients" items="${requestScope.allClients}">
														<option value="${allClients.userId}"
															${fn:contains(userId,allClients.userId) ? 'selected="selected"' : ''}>
															<c:out value="${allClients.name}" /></option>
													</c:forEach>
												</c:if>
											</select>
										</div>
<!-- 											<div> -->
<!-- 												 <button type="submit" class="btn btn-primary" title="Search"><span class="glyphicon glyphicon-filter"></span></button> -->
<!-- 											</div> -->
										</div>

								</form>
						</div>
								<div class="row" id="table-wrapper" >
								 <div id="table-scroll">
									<c:choose>
										<c:when test="${userId !=null && userId!=''}">
											<table id="editedtable"
												class="table table-striped table-bordered date_sorted" width="auto">
												<thead>
													<tr>
														<th>S.NO</th>
														<th>Name</th>
														<th>Code</th>
														<th>Min Amount</th>
														<th>Max Amount</th>
														<th>Commission</th>
														<th>Commission Type</th>
														<th>Service Operator</th>
														<th>Service Type</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${totalServices}" var="totalServices"
														varStatus="loopCount">
														<tr>
															<td>${loopCount.count}</td>
															<td><c:out value="${totalServices.serviceName}" /></td>
															<td><c:out value="${totalServices.code}" /></td>
															<td><c:out value="${totalServices.minAmount}" /></td>
															<td><c:out value="${totalServices.maxAmount}" /></td>
															<td><c:out value="${totalServices.commission}" /></td>
															<td><c:out value="${totalServices.fixedVal}" /></td>
															<td><c:out value="${totalServices.operatorName}" /></td>
															<td><c:out value="${totalServices.serviceType}" /></td>
															<td><c:out value="${totalServices.status}" /></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:when>
										<c:otherwise>
											<table id="editedtable"
												class="table table-striped table-bordered date_sorted">
												<thead>
													<tr>
														<th>S.NO</th>
														<th>Name</th>
														<th>Code</th>
														<th>Description</th>
														<th>Service Operator</th>
														<th>Service Type</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${totalServices}" var="totalServices"
														varStatus="loopCount">
														<tr>
															<td>${loopCount.count}</td>
															<td><c:out value="${totalServices.serviceName}" /></td>
															<td><c:out value="${totalServices.code}" /></td>
															<td><c:out value="${totalServices.description}" /></td>
															<td><c:out value="${totalServices.operatorName}" /></td>
															<td><c:out value="${totalServices.serviceType}" /></td>
															<td><c:out value="${totalServices.status}" /></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:otherwise>
									</c:choose>

								</div>
								</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script>
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : true,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	<script>
		$('#userId').change(function(){
			$("#formId").submit();
		})
	</script>
	
	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
	
</body>

</html>
