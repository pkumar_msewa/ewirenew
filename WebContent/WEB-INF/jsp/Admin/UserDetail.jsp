<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | User Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
       <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- <link href="assets/font/styles.css" rel="stylesheet" type="text/css" /> -->

        <!-- App css -->
       <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>

        <style>
            @font-face {
                font-family: "OCRAExtended";
                src: url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.eot");
                src:
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.woff") format("ttf"), 
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.ttf") format("woff"),
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.svg#filename") format("svg");
            }
            .br-15 {
                border-radius: 15px;
            }
            .pd-10 {
                padding: 10px;
            }
            .usr_card {
                width: 78%;
                margin-left: 10%;
                margin-bottom: 50px;
            }
            .usr_card span {
                font-family: 'OCRAExtended', Arial, sans-serif;
                color: #fff;
                font-size: 13px;
                text-transform: uppercase;
            }
            .usr-wrp img {
                box-shadow: 1px 1px 5px 5px #efefef;
                border-radius: 18px;
            }
            .usrcard-dtls {
                margin-top: -184px;
                padding: 0 22px;
                line-height: 36px;
            }
            .cardNum span {
                font-size: 22px;
                letter-spacing: 2px;
            }
            .validity span {
                font-size: 13px;
            }
            .validity {
                margin-left: 43%;
            }
            .cardHold span {
                font-size: 20px;
            }
            .usrdtls {
                font-family: 'OCRAExtended', Arial, sans-serif;
                /*margin-top: -178px;*/
                padding: 0 15px;
                /*position: absolute;*/
                text-transform: uppercase;
                line-height: 24px;
            }
            .usr-name span {
                font-size: 18px;
            }
            .green-hed {
                background: #db1c28;
            }
            .amt-loaded {
                font-family: 'OCRAExtended', Arial, sans-serif;
                font-size: 16px;
            }
            .amt-loaded p {
                margin-bottom: 0;
                margin-top: 20px;
            }
            .red-hed {
                background: #331c56;
            }
            .txns-tabl .card-box {
                max-height: 300px;
                overflow: auto;
                overflow-x: hidden;
            }
            .txns-hed {
                border-radius: 5px;
                margin-bottom: 10px;
            }
            .txns-hed span {
                font-family: 'OCRAExtended', Arial, sans-serif;
                font-size: 22px;
                padding: 8px;
                color: #fff;
                font-weight: 100;
            }
            .txns-itms {
                border: 1px solid #efefef;
                border-radius: 5px;
                padding: 4px;
                margin-bottom: 8px;
            }
            .txt-gray {
                color: #908f91;
            }
            .txt-black {
                color: #000;
            }
            .fw-600 {
                font-weight: 600;
            }
            .fw-800 {
                font-weight: 800;
            }
            .text-green {
                color: #07a507;
            }
            .lodAmt span {
                font-size: 30px;
            }
            /* width */
            .txns-tabl .card-box::-webkit-scrollbar {
                width: 10px;
            }

            /* Track */
            .txns-tabl .card-box::-webkit-scrollbar-track {
                background: #f1f1f1; 
            }
             
            /* Handle */
            .txns-tabl .card-box::-webkit-scrollbar-thumb {
                background: #888; 
            }

            /* Handle on hover */
            .txns-tabl .card-box::-webkit-scrollbar-thumb:hover {
                background: #555; 
            }
        </style>

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">
			  <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">User Details</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="card-box usr-wrp br-15">
                                                        <!-- <img src="assets/images/blank-card.png" class="img-fluid"> -->
                                                        <div class="usrdtls">
                                                        <div class="row">
                                                                <div class="col-6">
                                                            <div class="usr-name text-left">
                                                                <span class="txt-black fw-600">${User.userDetail.firstName} ${User.userDetail.lastName}</span>
                                                            </div>
                                                            	</div>
                                                            	<div class="col-6">
                                                            	<div class="usr-name text-right">
                                                                <span class="txt-black fw-600"><i class="fa fa-inr"></i>&nbsp;${Ubalance}</span>
                                                            </div>
                                                            	</div>
                                                         </div>
                                                            <div class="usrdob">
                                                                <span class="dob txt-black fw-600">${User.userDetail.dateOfBirth}</span>
                                                            </div>
                                                            <div class="usradd">
                                                                <span>${User.userDetail.address}</span>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="auth-code text-left">
                                                                        <span>AUTHORITY <br>${User.authority}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="otpcode text-right">
                                                                        <span class="txt-black fw-600">otp</span><br><span class="txt-black fw-600">${User.mobileToken}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="amt-loaded">
                                                                <center>
                                                                    <p class="txt-black fw-600">Total Amount Loaded : <span>INR</span>&nbsp;<span>${Lamt}</span></p>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="usr_card">
                                                        <center><img src="${pageContext.request.contextPath}/resources/images/card.png" class="img-fluid"></center>
                                                        <div class="usrcard-dtls">
                                                            <div class="text-right">
                                                                <span>cvv</span>&nbsp;<span>${cardDetail.cvv}</span>
                                                            </div>
                                                            <div class="cardNum">
                                                                <span class="card-num">${cardDetail.walletNumber}</span>
                                                            </div>
                                                            <div class="validity">
                                                                <span>valid</span>&nbsp;<span>${cardDetail.expiryDate}</span>
                                                            </div>
                                                            <div class="cardHold">
                                                                <span>${cardDetail.holderName}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="txns-wrp">
                                                        <div class="txns-hed green-hed">
                                                            <span>CREDIT TRANSACTION</span>
                                                        </div>
                                                        <div class="txns-tabl">
                                                            <div class="card-box pd-10">
                                                            <c:forEach items="${credttrx}" var="card" varStatus="loopCounter">
                                                                <div class="txns-itms">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-9">
                                                                                <div class="row datId">
                                                                                    <div class="col-6">
                                                                                        <small class="txt-gray">${card.created}</small>
                                                                                    </div>
                                                                                    <div class="col-6">
                                                                                        <small class="txt-gray">Txn No.</small><small class="txt-gray">${card.transactionRefNo}</small>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="desc">
                                                                                    <small class="txt-black fw-600">Description:</small>
                                                                                </div>
                                                                                <div class="desc-txt">
                                                                                    <small class="txt-gray">${card.description}</small>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-3">
                                                                                <center>
                                                                                    <div class="lodstatus">
                                                                                        <small class="text-green fw-800">${card.status}</small>
                                                                                    </div>
                                                                                    <div class="lodAmt">
                                                                                        <span class="txt-gray fw-800">${card.amount}</span>
                                                                                    </div>
                                                                                </center>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
																</c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="txns-wrp">
                                                        <div class="txns-hed red-hed">
                                                            <span>CARD TRANSACTION</span>
                                                        </div>
                                                        <div class="txns-tabl">
                                                            <div class="card-box pd-10">
                           				 <c:forEach items="${transactions}" var="card">
				                           <div class="txns-itms">
				                               <div class="col-md-12">
				                                   <div class="row">
				                                       <div class="col-9">
				                                           <div class="row datId">
				                                               <div class="col-6">
				                                                   <small class="txt-gray">${card.date}</small>
				                                               </div>
				                                               <div class="col-6">
				                                                   <small class="txt-gray">Txn Type.</small><small class="txt-gray">${card.transactionType}</small>
				                                               </div>
				                                           </div>
				                                           <div class="desc">
				                                               <small class="txt-black fw-600">Description:</small>
				                                           </div>
				                                           <div class="desc-txt">
				                                               <small class="txt-gray">${card.description}</small>
				                                           </div>
				                                       </div>
				                                       <div class="col-3">
				                                           <center>
				                                               <div class="lodstatus">
				                                                   <small class="text-green fw-800">${card.status}</small>
				                                               </div>
				                                               <div class="lodAmt">
				                                                   <span class="txt-gray fw-800">${card.amount}</span>
				                                               </div>
				                                           </center>
				                                       </div>
				                                   </div>
				                               </div>
				                           </div>
						</c:forEach>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © MSS Payments Pvt. Ltd.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
       <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
         <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script>

    </body>

</html>