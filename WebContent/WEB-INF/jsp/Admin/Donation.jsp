<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Donation | EWire</title>
<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}";
</script>

<!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        
         <!-- jQuery  -->
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/blockKeys.js"></script>

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>
</head>
<body oncontextmenu="return false">

<!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Donatee</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                <center><div><h6 style="font: bold;color: red;">${msg}</h6></center>
                                                                       
                                    <form action="#" method="POST" id="formId">
                                            <div class="row">
                                                <div class="col">
                                                <fieldset>
                                                    <legend>Donation Details</legend>
                                                    <div class="form-group">
                                                        <label for="name">Donatee Name</label>
                                                   			<input type="text" name="name" placeholder="Enter Donatee Name" id="name" class="form-control" maxlength="60" onkeypress="return firstNameFunction(event)">
                                                   			<p class="error" id="firstNameErrorId"></p>
                                                    </div>         
                                                    
                                                     <div class="form-group">
                                                        <label>Email</label>
                                                   			<input type="email" name="emailAddressId" placeholder="Enter Email" id="emailAddressId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" onkeypress="return emailFunction(event)" class="form-control">
                                                   			<p class="error" id="emailAddressErrorId"></p>
                                                    </div>                           
                                                      <div class="form-group">
                                                        <label>Contact Number</label>
                                                  			<input type="text" name="mobileNoId" placeholder="Enter Mobile" id="mobileNoId" class="form-control" minlength="10" maxlength="10" onkeypress="return mobileFunction(event)">
                                                  		<p class="error" id="mobileNoErrorId"></p>
                                                    </div>                                              
	                                                <input type="hidden" id="countryCodeId" name="countryCode" />
	                                                
                                                    </div>
                                                    <div class="col">
                                                    <fieldset>
                                                   
                                                     <div class="form-group">
	                                                   <label>Organization Name</label>
	                                                 <input type="text" name="organization" placeholder="Enter organization" id="organization" onkeypress="return organizationFunction(event)" class="form-control">
	                                                 	<p class="error" id="organizationErrorId"></p>
	                                                </div> 
	                                                <div class="form-group">
	                                                   <label>Account Number</label>
	                                                  	<input type="text" name="account" placeholder="Enter Account Number" id="account" class="form-control" maxlength="60" onkeypress="return accountFunction(event)">
	                                                  	<p class="error" id="accountErrorId"></p>
	                                                </div>        
	                                                <div class="form-group">
	                                                   <label>Remark</label>
	                                                  	<input type="text" name="remark" placeholder="Enter remark" id="remark" class="form-control" maxlength="60" onkeypress="return remarkFunction(event)">
	                                                  	<p class="error" id="remarkErrorId"></p>
	                                                </div>
	                                                 
                                                    </fieldset>
                                                    
                                                         </div>
 												</fieldset>
                                            </div>                                          
                                            <center><button type="button" id="Agentregister" class="btn btn-primary mt-4">Add Donatee</button></center>
                                        </form>
                                         </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

<script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

        <!-- Daterange picker -->
        <script type="text/javascript">
            $(function() {

                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                
            });
            </script>

            <script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {

              // Upload btn on change call function
              $(".uploadlogo").change(function() {
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });
	
              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                  ext == "jpg"
                  ) || ext == "png" || ext == "jpeg") {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only jpg/png/jpeg format is allowed!";
                }
              }
              // Upload btn end

            });
            </script> 
            <script type="text/javascript">
		
			$("#Agentregister").click(function () {
				
			
			$("#firstNameErrorId").html("");
			$("#emailAddressErrorId").html("");				
			$("#mobileNoErrorId").html("");
			$("#organizationErrorId").html("");
			$("#accountErrorId").html("");
			$("#remarkErrorId").html("");
			
			var valid = true;
			var myRegEx = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i;
			var myRegEx2 = /^[0-9]+$/i;
			var myRegEx4 = /^[a-zA-Z ]+$/i;
			var myRegEx5 = /^[0-9.]+$/i;
			
			//var role = $('#roleId').val();			
			var firstName = $('#name').val();			
			var emailAddress = $('#emailAddressId').val();
			var mobileNo = $('#mobileNoId').val();
			var org = $('#organization').val();
			var acc = $('#account').val();
			var remark = $('#remark').val();
			var country = "India";			
						
					
			if (firstName.length <= 0) {
				$("#firstNameErrorId").html("Enter Donatee name");
				$('#name').val("");
				valid = false;
			} else if (!firstName.match(myRegEx4)) {
				$("#firstNameErrorId").html("Enter valid Donatee name");
				valid = false;
			}
			$("#name").val(firstName);
			
			if (emailAddress.length <= 0) {
				$("#emailAddressErrorId").html("Enter email id");
				valid = false;
			} else if (!emailAddress.match(myRegEx)) {
				$("#emailAddressErrorId").html("Enter valid email id");
				valid = false;
			}
						
			if (mobileNo.length <= 0) {
				$("#mobileNoErrorId").html("Enter contact no.");
				valid = false;
			} else if (mobileNo.length < 7) {
				$("#mobileNoErrorId").html("Enter valid contact no.");
				valid = false;
			} else if (!mobileNo.match(myRegEx2)) {
				$("#mobileNoErrorId").html("Enter valid contact no.");
				valid = false;
			}
			
			if(org.length <= 0) {
				$("#organizationErrorId").html("Enter organization name");
				valid = false;
			} else if(!org.match(myRegEx4)) {
				$("#organizationErrorId").html("Enter valid organization name");
				valid = false;
			}
			
			if(acc.length <= 0) {
				$("#accountErrorId").html("Enter account number");
				valid = false;
			} else if(!acc.match(myRegEx2)) {
				$("#accountErrorId").html("Enter valid account number");
				valid = false;
			}
			
			if(remark.length <= 0) {
				$("#remarkErrorId").html("Enter Remark");
				valid = false;
			} else if(!remark.match(myRegEx4)) {
				$("#remarkErrorId").html("Enter Valid text");
				valid = false;
			}
			
			if (valid == true) {				
			
				$.ajax({
					type: "POST",
      				contentType : "application/json",
      				url : "${pageContext.request.contextPath}/Admin/AddDonation",
      				dataType : 'json',  
      				data: JSON.stringify ({
      					"name" : firstName,
      					"emailAddressId" : emailAddress,
      					"mobileNoId" : mobileNo,
      					"organization" : org,  
      					"account" : acc,
      					"remark" : remark
      				}),
      				success : function(response) {
      					if(response.code == "S00") {
      						$("#common_success_true").modal("show");
      						$("#common_success_msg").html(response.message);
      						console.log(response.message);
      						var timeout = setTimeout(function() {
      							$("#common_success_true").modal("hide");      								      							
      							$("#name").val("");
      							$("#emailAddressId").val("");
      							$("#mobileNoId").val("");
      							$("#organization").val("");
      							$("#account").val("");
      							$('#remark').val("");
      						}, 2000);
      					} else {
      						$("#common_error_true").modal("show");
      						$("#common_error_msg").html(response.message);
      						var timeout = setTimeout(function() {
      							$("#common_error_true").modal("hide");      							
      							$("#name").val("");
      							$("#emailAddressId").val("");
      							$("#mobileNoId").val("");
      							$("#organization").val("");
      							$("#account").val("");
      							$('#remark').val("");
      						}, 3000);
      					}
      				}
				});
			}
			});
	</script>
            
            
            
            
           <script type="text/javascript">
           function addCorporateAgent(){
        		var valid=true;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		
        		$("#error_fullName").html("");
        		$("#error_email").html("");
        		$("#error_address").html("");
        		$("#error_state").html("");
        		$("#error_city").html("");
        		$("#error_mobile").html("");
        		$("#error_aaadharCardImg").html("");
        		
        		var groupName=$('#name').val();
        		var mobile=$("#mobileNoId").val();
        		var email=$('#emailAddressId').val();
        		var address=$('#address').val();
        		var city=$('#city').val();
        		var state = $('#state').val();
        		var aadharCardImg=$('#aadhar_Image').val();
        		var countryCode = "91";
    			
    			$("#countryCodeId").val(countryCode);
        		       		
        		if(groupName=='') {
        			$("#error_fullName").html("Please enter Group Name");
        			valid = false;
        			console.log("valid: "+"fullName");
        		}
        		if(email=='') {
        			$("#error_email").html("Please enter email id");
        			valid = false;
        			console.log("valid: "+"bankName");
        		}
        		if(email!=''){
        			if(!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}")){
            			$("#error_email").html("Please enter valid email id");
							valid=false;
        			}
        		}
        		if(address=='') {
        			$("#error_address").html("Please enter address");
        			valid = false; 
        			console.log("valid: "+"address");
        		}
        		if(state=='') {
        			$('#error_state').html("Please enter state");
        			valid = false;
        			console.log("valid: "+"state");
        		}
        		if(city=='') {
        			$('#error_city').html("Please enter city");
        			valid = false;
        			console.log("valid: "+"city");
        		}
        		
        		if(mobile=='') {
        			$("#error_mobile").html("Please enter contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        		if(mobile!=''){
        			if(mobile.length!=10){
        			$("#error_mobile").html("Please enter valid contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        	}
        		/* if (aadharCardImg!= "") {
        		    if (!((aadharCardImg.split(".")[1].toUpperCase() == "jpg") || (aadharCardImg.split(".")[1].toUpperCase() == "jpeg") || (aadharCardImg.split(".")[1].toUpperCase() == "png"))) {
        		    		
        		     $("#error_aaadharCardImg").html("File " + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   } */
        		if(aadharCardImg=='') {
        			$("#error_aaadharCardImg").html("Please attach Doc");
        			valid = false;
        		}
        		        		
        		if (valid) {
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
        			console.log("")
//         			$("#addAgent").html(spinnerUrl);
        		}
        	}

        	</script>
        	
        	<script type="text/javascript">
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function groupname_error(evt) {
			$("#error_fullName").html("");	
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function email_error(evt) {
			$("#error_email").html("");			
		}
		
		function address_error(evt) {
			$("#error_address").html("");			
		}
		
		function state_error(evt) {
			$("#error_state").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			 return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function city_error(evt) {
			$("#error_city").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function mobile_error(evt) {
			$("#error_mobile").html("");
			var charCode = (evt.which) ? evt.which : evt.keyCode
			return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}

	</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script>

var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(9); //Tab
specialKeys.push(46); //Delete
specialKeys.push(36); //Home
specialKeys.push(35); //End
specialKeys.push(37); //Left
specialKeys.push(39); //Right

function firstNameFunction(e) {   
 $('#firstNameErrorId').html(' ');
 var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
       var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
       if(!ret){
        $('#firstNameErrorId').html('Enter valid Donatee name');
       }
       return ret;
}

function emailFunction(e) {   
	 $('#emailAddressErrorId').html(' ');
	 var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	       var ret = (keyCode == 32);
	       if(ret){
	        $('#emailAddressErrorId').html('Enter valid email id');
	       }
	       return !ret;
	}
	
function mobileFunction(e) {   
	 $('#mobileNoErrorId').html(' ');
	 var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	       var ret = (keyCode >= 48 && keyCode <= 57);
	       if(!ret){
	        $('#mobileNoErrorId').html('Enter valid contact no');
	       }
	       return ret;
	}
	
	function organizationFunction(e) {
		$('#organizationErrorId').html(' ');
		var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	       var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
	       if(!ret){
	        $('#organizationErrorId').html('Enter valid organization name');
	       }
	       return ret;
	}
	
	function remarkFunction(e) {
		$('#remarkErrorId').html(' ');
		var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	       var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
	       if(!ret){
	        $('#remarkErrorId').html('Enter valid remark');
	       }
	       return ret;
	}
	
	function accountFunction(e) {
		$('#accountErrorId').html(' ');
		var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
		var ret = (keyCode >= 48 && keyCode <= 57);
	       if(!ret){
	        $('#accountErrorId').html('Enter valid account number');
	       }
	       return ret;
	}
	
function stateFunction(e) {
	var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var abc = (keyCode >= 48 && keyCode <= 57);
    var space = (keyCode == 32)
    
    if(space) {
    	$("#stateErrorId").html("");
    	return true;
    }
    if(abc) {    	
   		 return false;
   	}
    else {
    	$("#stateErrorId").html("");
   		var ret = ( (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
   		console.log(ret);
   		return ret;
    }   
}

function cityFunction(e) {
	var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var abc = (keyCode >= 48 && keyCode <= 57);
    var space = (keyCode == 32)   
    if(space) {
    	$("#cityErrorId").html("");
    	return true;
    }
    if(abc) {
   		 return false;
   	}
    else {
    	$("#cityErrorId").html("");
   		var ret = ( (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
   		console.log(ret);
   		return ret;
    }   
}

function addressFunction(e) {
	var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var abc = (keyCode >= 48 && keyCode <= 57);
    var space = (keyCode == 32)
    $("#addressErrorId").html("");
    if(space) {
    	$("#addressErrorId").html("");
    	return true;
    }
}

</script>
<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}


</script>


</body>
</html>