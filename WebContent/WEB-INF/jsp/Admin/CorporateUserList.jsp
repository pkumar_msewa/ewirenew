<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Corporate User List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- Table Export -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/table_export/css/tableexport.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
		
		
 <!-- <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script> -->
    
    <script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>

    </head>


    <body oncontextmenu="return false">
    
    
    
    <script>
    $(document).ready(function() {
// 		 getAjaxData();
		 var timeout = setTimeout(function(){
     		$("#stst").html("");
     	}, 3000); 
         
     

	 });
	</script>
	<!-- <script>
	 function fetchMe(value){
		var paging=value;
		var daterangeVal=$('#reportrange').val();
		console.log(paging);
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/UserList",
	data:{page:paging,size:'10',Daterange:daterangeVal},
	dataType:"json",
	success:function(data){
		//$("#reportrange").html(data.date);
	var trHTML='';
		if(trHTML==''){
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){
			console.log(data.jsonArray[i].role)
			if(data.jsonArray[i].role){
		trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
		}else{
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
		}});
		
		$('#ashok').append(trHTML);


		}
		else
		{
			$(data.jsonArray).each(function(i,item){
				console.log(data.jsonArray[i].role)
				if(data.jsonArray[i].role){
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
					}else{
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
					}});

			$('#ashok').append(trHTML);
			
		}
		 
	}

	});
			 }
	 </script>
	 <script>
	 function  getAjaxData(){
		
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/UserList",
				data:{
					page:paging,
					size:'10'
					},
			dataType:"json",
			success:function(data){
				 console.log("Response get");
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						if(data.jsonArray[i].role){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
							}else{
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
							}});

					$('#ashok').append(trHTML);
					
					}
					else
					{
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
								}});
						$('#ashok').append(trHTML);
						
					}
					
					  $(function () {
							console.log("inside funt...and total pages:"+data.totalPages);
							
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
						
				         }
						 });
						}); 
						}
					 });
				 }
</script>	
     -->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">List of Users</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        
                        <div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-8">
											<!-- <form action="" method="post">
												<div class="form-row">
													<div class="col-sm-4">
														<div id="" class="pull-left" style="cursor: pointer;">
															<label class="sr-only" for="filterBy">Filter By:</label>
														   	<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
														</div>
													</div>
													<div class="col-sm-3">
														<button class="btn btn-primary" onclick="" type="button">Filter</button>
													</div>
												</div>
											</form> -->
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
										<!-- <div class="row">
			                                    <div class="form-group">
				                                    <input id="username" name="userName" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Username"/>
				                                </div>
				                                <div class="form-group">    
				                                    <button class="btn btn-primary" onclick="" type="button" >Search</button>
			                                   	</div>	
			                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
			                                </div> -->
										</div>
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
									<!-- <div class="col-sm-3">
										<div id="" class="pull-left" style="cursor: pointer;">
											<label class="sr-only" for="filterBy">Filter By:</label>
											<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
										</div>
									</div>
									<div class="col-sm-3">
										<button class="btn btn-primary">Filter</button>
									</div> -->
                                    <table id="Cashier-userList" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User details</th>
                                                <th>Registered Date</th>
                                                <th>Mobile Token</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${userList}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td><b>Name:</b> <c:out value="${card.firstName}  ${card.lastName} " default="" escapeXml="true" /><br>
										<b>Email:</b> <c:out value="${card.email}" default="" escapeXml="true" /><br>
										<b>DOB:</b> <c:out value="${card.dob}" default="" escapeXml="true" /><br>
										<b>AccountType:</b> <c:out value="${card.accountType}" default="" escapeXml="true" /><br>
										<b>Contact No: <c:out value="${card.contactNO}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.issueDate}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.mobileToken}" default="" escapeXml="true" /></td>
									<td><c:out value="${card.status}" default="" escapeXml="true" /></td>
								<c:choose>
								<c:when test="${card.authority == 'ROLE_USER,ROLE_AUTHENTICATED'}">
<%-- 								<form action="${pageContext.request.contextPath}/Admin/Status/block/unblock" method="post"> --%>
<%-- 								<input type="hidden" value="${card.contactNO}" name="userName" /> --%>
<!-- 								<input type="hidden" value="ROLE_USER,ROLE_LOCKED" name="authority" /> -->
								<td> <button type="submit" class="btn btn-sm btn-danger" id="blck_${loopCounter.count}"  value="${card.contactNO}" onclick="blockUser('${card.contactNO}','${loopCounter.count}')" >Block</button></td>
<!-- 								</form> -->
								</c:when>
								<c:otherwise>
<%-- 								<form action="${pageContext.request.contextPath}/Admin/Status/block/unblock" method="post"> --%>
<%-- 								<input type="hidden" value="${card.contactNO}" name="userName" /> --%>
<!-- 								<input type="hidden" value="ROLE_USER,ROLE_AUTHENTICATED" name="authority" /> -->
								<td> <button type="submit" class="btn btn-sm btn-success" id="ublck_${loopCounter.count}" value="${card.contactNO}" onclick="unblockUser('${card.contactNO}','${loopCounter.count}')" >Unblock</button></td>
<!-- 								</form> -->
								</c:otherwise>
								</c:choose>
								</tr>
							</c:forEach>
							
							
                                    </tbody>
                                    </table>
                                     <nav style="float: right;">
										<ul class="pagination" id="paginationn"></ul>
									</nav>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
	
		 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
		
		<!-- Table Export js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/FileSaver.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/assets/table_export/js/tableexport.js"></script>
		
        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        

    </body>
    
    
    
    <script type="text/javascript">
     function blockUser(va,Bid){
    	 var contact=va;
    	 var auth="ROLE_USER,ROLE_LOCKED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :""+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
//     				 $("#statusUpdt").html(response.message);
    				/*  $("#blck_"+Bid).html("Unblock");
    				 $("#blck_"+Bid).css('color','green'); */
    				 window.location.href="${pageContext.request.contextPath}/Admin/${usern}/fetchUserList";
    			},
    		});
    	 
     }
    </script>
    
     <script type="text/javascript">
     function unblockUser(va,Bid){
    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :""+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
//     				 $("#statusUpdt").html(response.message);
    				/*  $("#ublck_"+Bid).html("Block");
    				 $("#ublck_"+Bid).css('color','red'); */
    				 window.location.href="${pageContext.request.contextPath}/Admin/${usern}/fetchUserList";
    			},
    		});
    	 
     }
    </script>
    
    
    
    
		<script>
		function fetchlist(){
				var date=$("#reportrange").val();
			 var paging='0';
			 var size='';
			 console.log("under ready...");
			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/UserList",
					data:{
						page:paging,
						size:'10',
						Daterange:date
						},
				dataType:"json",
				success:function(data){
					 console.log("Response get");
					 $("#reportrange").html(data.date);
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
								}});

						$('#ashok').append(trHTML);
						
						}
						else
						{
							$(data.jsonArray).each(function(i,item){
								if(data.jsonArray[i].role){
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
									}});
							$('#ashok').append(trHTML);
							
						}
						
						 $(function () {
								console.log("inside funt...and total pages:"+data.totalPages);
								
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
								 visiblePages: 7,
					         onPageClick: function (event, page) {
					        	 fetchMe(page-1);
							
					         }
							 });
							});
							}
						 });
		}
		</script>
		
		<script>
 function fetchSincgleCard(){
	var username=$("#username").val();
	var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true){

			 var paging='0';
			 var size='';
			 console.log("under ready...");
			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/SingleUser",
					data:{
						userName:username
						},
				dataType:"json",
				success:function(data){
					 console.log("Response get");
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(data.jsonArray[i].role){
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
								}else{
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
								}});

						$('#ashok').append(trHTML);
						
						}
						else
						{
							$(data.jsonArray).each(function(i,item){
								if(data.jsonArray[i].role){
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="" onclick="blockUser('+data.jsonArray[i].contactNO+')" value="">Block</button></td></tr>';
									}else{
										trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNO+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>'+ data.jsonArray[i].issueDate+'</td>'+'<td>'+ data.jsonArray[i].mobileToken+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-primary" id="" onclick="unblockUser('+data.jsonArray[i].contactNO+')" value="">Unblock</button></td></tr>';
									}});
							$('#ashok').append(trHTML);
							
						}
						
						/*  $(function () {
								console.log("inside funt...and total pages:"+data.totalPages);
								
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
								 visiblePages: 7,
					         onPageClick: function (event, page) {
					        	 fetchMe(page-1);
							
					         }
							 });
							}); */
							}
						 });
		}
 }
 </script>
    
    <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

<script type="text/javascript">
            
            $("#Cashier-userList").tableExport();

        </script>
        
         <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
</html>