<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | User</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        
    
    <script type="text/javascript">
    var context_path="${pageContext.request.contextPath}";
    </script>

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="clearfix"></div>
                                    <span id="stst" style="margin-left: 40%; color: #3c86d8fa;">${statusUpdt}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User details</th>
                                                <th>Contact No</th>
                                                <th>registered Date</th>
                                                <th>Mobile Token</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${userList}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td><b>Name:</b><a href="${pageContext.request.contextPath}/Admin/SingleUserDetails/${card.contactNO}"><c:out value="${card.firstName}  ${card.lastName} " default="" escapeXml="true" /></a> <br>
										<b>Email:</b> <c:out value="${card.email}" default="" escapeXml="true" /><br>
										<b>DOB:</b> <c:out value="${card.dob}" default="" escapeXml="true" /><br>
										<b>AccountType:</b> <c:out value="${card.accountType}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.contactNO}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.issueDate}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.mobileToken}" default="" escapeXml="true" /></td>
									<td><c:out value="${card.status}" default="" escapeXml="true" /></td>
								<c:choose>
								<c:when test="${card.authority == 'ROLE_USER,ROLE_AUTHENTICATED'}">
								<form action="${pageContext.request.contextPath}/Admin/Status/block/unblock" method="post">
								<input type="hidden" value="${card.contactNO}" name="userName" />
								<input type="hidden" value="ROLE_USER,ROLE_LOCKED" name="authority" />
								<td> <button type="submit" class="btn btn-sm btn-danger" id="${loopCounter.count}"  value="${card.contactNO}" >Block</button></td>
								</form>
								</c:when>
								<c:otherwise>
								<form action="${pageContext.request.contextPath}/Admin/Status/block/unblock" method="post">
								<input type="hidden" value="${card.contactNO}" name="userName" />
								<input type="hidden" value="ROLE_USER,ROLE_AUTHENTICATED" name="authority" />
								<td> <button type="submit" class="btn btn-sm btn-success" id="ublck" value="${card.contactNO}" >Unblock</button></td>
								</form>
								</c:otherwise>
								</c:choose>
								</tr>
							</c:forEach>
							
							<td><b>Name:</b> ${user.userDetail.firstName}  ${user.userDetail.lastName}<br>
							<b>Contact No:</b> ${user.userDetail.contactNo}<br>
							<b>Email:</b> ${user.userDetail.email}<br>
							<b>Dob:</b> ${user.userDetail.dateOfBirth}<br>
							<b>Account Type:</b> ${user.accountDetail.accountType.code}</td>
							<td>${user.created}</td>
							<td>${user.mobileToken}</td>
							<td>${user.mobileStatus}</td>
							<c:choose>
								<c:when test="${user.authority == 'ROLE_USER,ROLE_AUTHENTICATED'}">
								<td> <button type="submit" class="btn btn-sm btn-success" id="ublck" value="" onclick="blockUser('${user.username}')">block</button></td>
								</c:when>
							<c:otherwise>
							<td> <button type="submit" class="btn btn-sm btn-success" id="ublck" value="" onclick="unblockUser('${user.username}')">Unblock</button></td>
							</c:otherwise>
							</c:choose>
							<td></td>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            
            $(document).ready(function() {
                $('#datatable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                } );
                
                var timeout = setTimeout(function(){
            		$("#stst").html("");
            	}, 3000); 
            } );

        </script>

    </body>
    
    
    
    <script type="text/javascript">
     function blockUser(va){
    	 alert("ddd")
    	 var contact=va;
    	 var auth="ROLE_USER,ROLE_LOCKED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
    			},
    		});
    	 
     }
    </script>
    <!-- <script type="text/javascript">
            $(document).ready(function() {
                
                //Buttons examples
                var table = $('#datatable').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script> -->
    
     <script type="text/javascript">
     function unblockUser(va){
    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
    			},
    		});
    	 
     }
    </script>
    
    <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		
		
    
    <script type="text/javascript">
     function blockUser(va){
    	 var contact=va;
    	 var auth="ROLE_USER,ROLE_LOCKED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
//     				 window.location.href="${pageContext.request.contextPath}/Admin/UserList";
    			},
    		});
    	 
     }
    </script>
    
     <script type="text/javascript">
     function unblockUser(va){
    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
    		$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : context_path+"/Admin/Status/block/unblock",
    			dataType : 'json',
    			data : JSON.stringify({
    				"authority" :" "+auth+"",
    				"userName" :""+va+""
    			}),
    			success : function(response) {
    				 $("#statusUpdt").html(response.message);
//     				 window.location.href="${pageContext.request.contextPath}/Admin/UserList";
    			},
    		});
    	 
     }
    </script>
    
   <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

</html>