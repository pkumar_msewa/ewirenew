<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Add Merchant Services</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<%-- <link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" /> --%>
	<link
	href="${pageContext.request.contextPath}/resources/admin/css/datatables/css/dataTables.bootstrap.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css">
	<script src="${pageContext.request.contextPath}/resources/js/datepicker.js"></script>
	
	<%-- <script type="text/javascript" src="<c:url value="/resources/js/Admin/ClientServices.js"/>"></script> --%>
	
	<%-- <link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script> --%>
	
</head>

<!-- <script>
	$(function() {
		console.log("Calling date funtionaawsss")
		var dt;
		dt = new Date();
		dt.setFullYear(new Date().getFullYear() - 18);
		$("#dob").datepicker({
			viewMode : "years",
			endDate : dt,
			autoclose : 1,
			format : "yyyy-mm-dd"
		});
	});
</script> -->

<script type="text/javascript">
$(document).ready(function() {
	/*  $("#commonSpinner").modal("show"); */
         		var errorMsg=document.getElementById("errorMsg").value;
        		console.log("Alert ID :: " + errorMsg);
        		if(!errorMsg.length == 0){
        			$("#common_error_true").modal("show");
        			$("#common_error_msg").html(errorMsg);
        			var timeout = setTimeout(function(){
        				$("#common_error_true").modal("hide");
        				$("#errorMsg").val("");
        	          }, 3000);
        		}
        		
        		var successMsg=document.getElementById("successMsg").value;
        		console.log("successMsg :: " + successMsg);
        		if(!successMsg.length == 0){
        			$("#common_success_true").modal("show");
        			$("#common_success_msg").html(successMsg);
        			var timeout = setTimeout(function(){
        				$("#common_success_true").modal("hide");
        				$("#successMsg").val("");
        	          }, 2000); 
        		}
         	});
         	
   
  // Calling Api to get all Merchant    	
	$.ajax({
    type : "GET",
     url : "${pageContext.request.contextPath}/Admin/MerchantList",
    success : function(response) {
    	console.log("response of branch");
    	console.log(response);
        seljobs = response;
        var i = 0; 
        var html;
        
        response.forEach(function (i) {  
        	var service = i.userDetail.firstName +" "+ i.userDetail.lastName;
        	if(!(i.username == 'pnation@mdex.com') && !(i.username == 'airos@mdex.com')){
        		html += '<option value="' + i.username + '">' + service + '</option>';
                i++;
        	}
        });
        console.log("Option add");
        $('#mname').append(html);
   		 }
	});
	
	// Calling All Countrylist Api
	$.ajax({
    type : "GET",
     url : "${pageContext.request.contextPath}/Admin/CountryList",
   		success : function(response) {
    	console.log("response of branch");
    	console.log(response);
        seljobs = response;
        var i = 0; 
        var html;
        
        response.forEach(function (i) {                           
        	html += '<option value="' + i.code + '">' + i.name + '</option>';
            i++;
        });
        console.log("Option add");
        $('#m_countryCode').append(html);
   		}
	});
  
	// Calling Api for set the exiting ServiceType	
	$.ajax({
	    type : "GET",
	     url : "${pageContext.request.contextPath}/Admin/GetPQServiceList",
	    success : function(response) {
	    	console.log("GetPQServiceList");
	    	/*  $("#commonSpinner").modal("hide"); */
	    	console.log(response);
	    	console.log(response.length)
	        seljobs = response;
	        var i = 0; 
	        var html;
	        if(response.length == 0){
	        	$("#common_error_true").modal("show");
    			$("#common_error_msg").html("No Service found, Please add services before assigning to Merchant");
	        }else{
	        	  response.forEach(function (i) {                           
	  	        	html += '<option value="' + i.code + '">' + i.description + '</option>';
	  	            i++;
	  	        });
	  	        console.log(" ServiceTypeListOption add");
	  	       
	  	        $('#serviceCode').append(html);
	        }
	   		 }
		});
	
	// Call PQ Services based on Service Typw
         </script>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
		<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">Add Merchant Services</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-4 col-md-offset-4"><br>
									<!-- ADD MERCHANT FORM START -->
									<center>
										<form action="#" method="post">
											<center>
												<div class="form-group">
														<select class="form-control" name="mCode" id="m_countryCode" onchange="clearvalue('error_m_countryCode')">
															<option value="0">Select Merchant Country*</option>
														</select>
														<p class="error" id="error_m_countryCode"></p>
													</div>
													
												 <div class="form-group">
													<select class="form-control" name="mname" id="mname" onchange="clearvalue('error_mname')">
														<option value="0">Select Merchant*</option>
													</select>
													<p class="error" id="error_mname"></p>
												</div>
												 
												<!-- <div class="form-group">
													<select class="form-control" name="serviceType" id="serviceType" onchange="clearvalue('error_serviceType')">
														<option value="0">Select Service Type*</option>
													</select>
													<p class="error" id="error_serviceType"></p>
												</div> -->
												
												
												<div class="form-group">
													<select class="form-control" name="serviceCode" id="serviceCode" onchange="clearvalue('error_serviceCode')">
														<option value="0">Select Services*</option>
													</select>
													<p class="error" id="error_serviceCode"></p>
												</div>
												
												<div class="form-group">
													<select class="form-control" name="type" id="type" onchange="clearvalue('error_type')">
														<option value="0">Select Type*</option>
														<option value="PRE">PRE</option>
														<option value="POST">POST</option>
													</select>
													<p class="error" id="error_type"></p>
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Commission Value*" id="value" name="value" onkeypress="clearvalue('error_value')">
													<p class="error" id="error_value"></p>
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Min Amount*" id="min_amount" name="minAmount" onkeypress="clearvalue('error_min_amount')">
													<p class="error" id="error_min_amount"></p>
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control"  required="required" placeholder="Max Amount*" id="max_amount" name="maxAmount" onkeypress="clearvalue('error_max_amount')">
													<p class="error" id="error_max_amount"></p>
												</div>
												
												<input type="checkbox" class="check" value="fixed" id="fixed"><p >Fixed</p></br>
												
												<div class="form-group">
													<button type="button" id = "add_service_id"  name="singlebutton" class="btn btn-default">ADD MERCHANT SERVICES</button>
												</div>
											</center>
										</form>
									</center>
								<!-- ADD MERCHANT FORM END -->
								</div>
							</div>
						</div>
					</div> 
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			
		<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" data-backdrop="static"  class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		
	<!-- Common model for the transaction processing -->
	<div class="modal fade" role="dialog" id="commonSpinner" data-backdrop="static" style="margin-top:10%;">
		<div class="modal-dialog modal-sm">
			<div class="modal-body text-center" style="width: 120%; background:white; padding:15px;">
				<button type="button" data-dismiss="modal" class="close">&times;</button>			
					<center><img alt="" src="${pageContext.request.contextPath}/resources/admin/images/please-wait.gif" class="img-responsive"></center>
					<!-- <br>
					<h4><b>Please wait...</b></h4> -->
			</div>
		</div>
	</div>
		
		<!-- END WRAPPER -->
		<script type="text/javascript">
		function loadValidate() {
			
			var spinnerUrl = "Please wait <img src='/resources/admin/images/spinner.gif' height='20' width='20'>"
			var valid = true;
			var countryCode = $('#countryCode').val();
			console.log("operator : " + countryCode);
			
			 if(countryCode == "" || countryCode == "0"){
				valid = false;
				$("#error_countryCode").html("Select Country");	
			}
			if(valid == true) {
			     //$("#loadMId").attr("disabled","disabled");
				 $("#loadMId").html(spinnerUrl);  
				return valid;
			}
			return valid;
		}
		
		</script>
		<script type="text/javascript">
		function clearvalue(val){
			$("#"+val).text("");
		}
		</script>
		<script type="text/javascript">
		
		 $("#serviceType").change(function(){
			 console.log("Calling PQServices Based on Service Type");
		   var value = $("#serviceType").val();
		   var m_code = $("#m_countryCode").val();
			var mername = $("#mname").val();
		   $('#serviceCode').html("Select Services*");
//	    alert("Ajax Calling for Brand "+value );
	   $.ajax({
	       type : "GET",
	        contentType : "application/json",
	        url : "${pageContext.request.contextPath}/Admin/GetPQServiceList",
			dataType : 'json',
			data :{
				"service_type" : value,
				"county_code" : m_code,
				"mer_name" : mername,
			},
	       success : function(response) {
	       	console.log("response of branch");
	       	console.log(response);
	           
	           seljobs = response;
	           var i = 0; 
	           var html='<option value="' + '0' + '">'+ "Select Services*" +'</option>';
	           response.forEach(function (i) {                           
	           	html += '<option value="' + i.code + '">' + i.description + '</option>';
	               i++;
	           });
	           console.log("Option add");
//	           console.log($('#ecity_provider').html());
	           $('#serviceCode').append(html);
	       }
	   });
	   }); 
		</script>
		
		
	<!-- Javascript -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script>
	
	
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : true,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	<script>
		$('#userId').change(function(){
			$("#formId").submit();
		})
	</script>
	
	
<script type="text/javascript">
	$(document).ready(function(){
		var accountName = "";
		var servicesName = "";
	    var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var hash_key="hash";
		var default_hash="123456";
		var headers = {};
		headers[hash_key] = default_hash;
		headers[csrfHeader] = csrfToken;
		console.log("inside bill payment");
		
		
		//Add Client Services
		$("#add_service_id").click(function() {
			var valid = true;
			$("#error_mname").html("");
			$("#error_m_countryCode").html("");
			$("#error_serviceType").html("");
			$("#error_serviceCode").html("");
			
			var m_code = $("#m_countryCode").val();
			var mername = $("#mname").val();
			//var service_Type = $("#serviceType").val();
			var service_Code = $("#serviceCode").val();
			var s_type = $("#type").val();
			var s_value = $("#value").val();
			var s_min_amount = $("#min_amount").val();
			var s_max_amount = $("#max_amount").val();
			
			
			console.log("service_Code : :  " + service_Code);
			
			if(mername == "" || mername == "0"){
				valid = false;
				$("#error_mname").html("Select Merchant");
			}
			
			if(m_code == "" || m_code == "0"){
				valid = false;
				$("#error_m_countryCode").html("Select Merchant Country");
			}
			
			/* if(service_Type == "" || service_Type == "0"){
				valid = false;
				$("#error_serviceType").html("Select Service Type");
			} */
			
			if(service_Code == "" || service_Code == "0"){
				valid = false;
				$("#error_serviceCode").html("Select Services");
			}
			
			if(s_type == "" || s_type == "0"){
				valid = false;
				$("#error_type").html("Select Type");
			}
			
			if(s_value.length == 0){
				valid = false;
				$("#error_value").html("Enter Commission Value");
			}
			
			if(s_min_amount.length == 0){
				valid = false;
				$("#error_min_amount").html("Enter Min Amount");
			}
			
			if(s_max_amount.length == 0){
				valid = false;
				$("#error_max_amount").html("Enter Max Amount");
			}
			
			var is_fixed = document.getElementById("fixed").checked;
			
			console.log("Fixed : : " + is_fixed);

			if(valid == true){
				$("#commonSpinner").modal("show");
				var contextPath = "${pageContext.request.contextPath}"; 
				console.log("ContexPath : : " + contextPath)
				/*
	            $("#dth_submit").html(spinnerUrl);
	            $("#dth_submit").attr("disabled","disabled");*/

				$.ajax({
					type : "POST",
					contentType : "application/x-www-form-urlencoded",
					url : contextPath+"/Admin/AddMerchantServices",
					data : {
						mname : mername,
						mCode : m_code,
						//serviceType : service_Type,
						serviceCode : service_Code,
						type : s_type,
						value : s_value,
						minAmount : s_min_amount,
						maxAmount : s_max_amount,
						fixed : is_fixed
					},
					success : function(response) {
						$("#commonSpinner").modal("hide");
						console.log(response);
						if(response.code == "F00"){
							
						    $("#common_error_true").modal("show");
		        			$("#common_error_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_error_true").modal("hide");
		        				$("#errorMsg").val("");
		        	          }, 4000);
						}
						if(response.code == "S00"){
							$("#common_success_true").modal("show");
		        			$("#common_success_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_success_true").modal("hide");
		        	          }, 4000); 
					   }
					}
				});
			}
		});
	});
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 


</body>

</html>
