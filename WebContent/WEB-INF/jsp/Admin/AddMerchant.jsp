<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Add Merchant</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<%-- <link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" /> --%>
	<link
	href="${pageContext.request.contextPath}/resources/admin/css/datatables/css/dataTables.bootstrap.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css">
	<script src="${pageContext.request.contextPath}/resources/js/datepicker.js"></script>
	<%-- <link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script> --%>
	
</head>

<!-- <script>
	$(function() {
		console.log("Calling date funtionaawsss")
		var dt;
		dt = new Date();
		dt.setFullYear(new Date().getFullYear() - 18);
		$("#dob").datepicker({
			viewMode : "years",
			endDate : dt,
			autoclose : 1,
			format : "yyyy-mm-dd"
		});
	});
</script> -->

<script type="text/javascript">
$(document).ready(function() {
         		var errorMsg=document.getElementById("errorMsg").value;
        		console.log("Alert ID :: " + errorMsg);
        		if(!errorMsg.length == 0){
        			$("#common_error_true").modal("show");
        			$("#common_error_msg").html(errorMsg);
        			var timeout = setTimeout(function(){
        				$("#common_error_true").modal("hide");
        				$("#errorMsg").val("");
        	          }, 3000);
        		}
        		
        		var successMsg=document.getElementById("successMsg").value;
        		console.log("successMsg :: " + successMsg);
        		if(!successMsg.length == 0){
        			$("#common_success_true").modal("show");
        			$("#common_success_msg").html(successMsg);
        			var timeout = setTimeout(function(){
        				$("#common_success_true").modal("hide");
        				$("#successMsg").val("");
        	          }, 2000); 
        		}
         	});
         	
	// Calling Api for set the exiting ServiceType	
	$.ajax({
    type : "GET",
     url : "${pageContext.request.contextPath}/Admin/CountryList",
    success : function(response) {
    	console.log("response of branch");
    	console.log(response);
        seljobs = response;
        var i = 0; 
        var html;
        
        response.forEach(function (i) {                           
        	html += '<option value="' + i.code + '">' + i.name + '</option>';
            i++;
        });
        console.log("Option add");
        $('#countryCode').append(html);
   		 }
	});
	
	// Get all merchant categotry list api
	$.ajax({
	    type : "GET",
	     url : "${pageContext.request.contextPath}/Admin/GetMerchantCategoryList",
	    success : function(response) {
	    	console.log("response of branch");
	    	console.log(response);
	        seljobs = response;
	        var i = 0; 
	        var html;
	        
	        response.forEach(function (i) {                           
	        	html += '<option value="' + i.code + '">' + i.description + '</option>';
	            i++;
	        });
	        console.log("Option add");
	        $('#categoryCode').append(html);
	   		 }
		});
         </script>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
		<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">Add Merchant</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-4 col-md-offset-4"><br>
									<!-- ADD MERCHANT FORM START -->
									<center>
										<form action="${pageContext.request.contextPath}/Admin/AddMerchant" method="post">
											<center>
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="First Name*" id="firstName" name="firstName">
												</div>
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Last Name*" id="lastName" name="lastName">
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Contact No*" id="contact" name="contact">
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Email Id*" id="email" name="email">
												</div>
												
												<div class="form-group">
													<input type="text" class="form-control" required="required" placeholder="Address*" id="address" name="address">
												</div>
												
												 <div class="form-group">
													<select class="form-control" name="categoryCode" id="categoryCode" onchange="clearvalue('error_categoryCode')">
														<option value="0">Select Merchant Category*</option>
													</select>
													<p class="error" id="error_categoryCode"></p>
												</div>
												
												<div class="form-group">
													<select class="form-control" name="countryCode" id="countryCode" onchange="clearvalue('error_countryCode')">
														<option value="0">Select Country*</option>
													</select>
													<p class="error" id="error_countryCode"></p>
												</div>
												
												
												<div class="form-group">
													<button type="submit" id = "loadMId" onclick="return loadValidate();" name="singlebutton" class="btn btn-default">ADD MERCHANT	</button>
												</div>
											</center>
										</form>
									</center>
								<!-- ADD MERCHANT FORM END -->
								</div>
							</div>
						</div>
					</div> 
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			
			<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>x
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
		<script type="text/javascript">
		function loadValidate() {
			
			var spinnerUrl = "Please wait <img src='/resources/admin/images/spinner.gif' height='20' width='20'>"
			var valid = true;
			var countryCode = $('#countryCode').val();
			var categoryCode = $('#categoryCode').val();			
			
			if(countryCode == "" || countryCode == "0"){
				valid = false;
				$("#error_countryCode").html("Select Country");	
			}
			 
			if(categoryCode == "" || categoryCode == "0"){
					valid = false;
					$("#error_categoryCode").html("Select Merchant Category");	
				}
			if(valid == true) {
			     //$("#loadMId").attr("disabled","disabled");
				 //$("#loadMId").html(spinnerUrl);  
				return valid;
			}
			return valid;
		}
		
		</script>
		<script type="text/javascript">
		function clearvalue(val){
			$("#"+val).text("");
		}
		</script>
		
	<!-- Javascript -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script>
	
	
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : true,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	<script>
		$('#userId').change(function(){
			$("#formId").submit();
		})
	</script>
	
	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
	
</body>

</html>
