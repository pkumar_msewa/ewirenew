<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Clients</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
</head>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">CLIENT LIST</h3></div>
							
<!-- 							MODEL WINDOW -->

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 4%;">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-md-12" >
												<div class="col-md-8"  class="form-control">
											<h4 id="headingId"></h4>
											</div>
												<div class="col-md-4"  class="form-control" align="right">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span><i title ="Close"class="fa fa-times" aria-hidden="true"   style="color:red"></i></span>
											</button>	</div></div>
										</div>
									<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
													<!-- TABLE HOVER -->
													<!-- page content -->
													<table id="modelTable"
														class="table table-striped table-bordered date_sorted">
														<thead>
															<tr>
																<th>S.NO</th>
																<th>Account Number</th>
																<th>Country</th>
																<th>Prefunded Account Balance</th>
															</tr>
														</thead>
													</table>
<!-- 													<nav> -->
<!-- 														<ul class="pagination" id="paginationn"></ul> -->
<!-- 													</nav> -->
													<!-- End Page Content -->
													<!-- END TABLE HOVER -->
												</div>
											</div>

									</div>
								</div>
								</div>
							</div>
							
							
<!-- 							MODEL WINDOW -->
							

							<form id="formId" action="<c:url value="/Admin/filteredTransactionList"/>" method="post" >
							<input type="hidden" name="val" value="" id="usernameId">
							<input type="hidden" name="service" value="All" >
							</form>
							
							
						<div class="panel-body" style="background: #eaeaea;">
						<div class="row">
								<div class="col-md-12" >
								<table id="editedtable"
									class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>S.NO</th>
											<th>Name</th>
											<th>Contact</th>
											<th>Authority</th>
											<th>Last Transaction Date</th>
											<th>Last Transaction Amount</th>
											<th>Account Balance</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${allClients}" var="allClients"
											varStatus="loopCount">
											<tr>
												<td>${loopCount.count}</td>
												<td style="cursor: pointer; color: #d86b1d; text-decoration: underline;"
													onclick="fromSubmitFunction(${allClients.userId});"><c:out value="${allClients.name}" /></td>
												<td><c:out value="${allClients.contactNo}" /></td>
												<td><c:out value="${allClients.authority}" /></td>
												<td><c:out value="${allClients.maxDate}" /></td>
												<td><c:out value="${allClients.amount}" /></td>
												<td><i title="View Accounts" onmouseover=""
													style="cursor: pointer; color: #d86b1d;"
													onclick="getAccountDetails('${allClients.userId}','${allClients.name}')"
													class="fa fa-money" aria-hidden="true"></i></td>
												<td><a
													href="${pageContext.request.contextPath}/Admin/clientServices?val=${allClients.userId}"
													title="View Services"><i style="cursor: pointer; color: #d86b1d;" class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
												<a
													href="${pageContext.request.contextPath}/Admin/accountHistory?id=${allClients.userId}"
													title="Add Money"><i style="cursor: pointer; color: #d86b1d;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div></div>
								</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	 <script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
			// $("#editedtable").removeClass("dataTable");
		});
	</script>
	<script>
	function getAccountDetails(userId,userName){
		$('#headingId').html('<h4>'+userName+'&nbsp; Account Balance</h4>');
		$.ajax({
			type:"GET",
			url:"${pageContext.request.contextPath}/Admin/clientAccount",
			data:{
				val:userId
			},
		dataType:"json",
		success:function(data){
			console.log(data);
			var trHTML='';
			$(".testingg").empty();
			if(trHTML==''){
			$(data).each(function(i,item){
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="accountNumber">' + data[i].accountNumber+ 
				'</td><td>' + data[i].country + '</td><td>' + data[i].strBalance +'</td></tr>';	
				});
			  $('#modelTable').append(trHTML);
			  $('#myModal').modal('show'); 
	 }
		}
});
	console.log(userId);		
	}
	</script>
	<script>
	function fromSubmitFunction(userId){
		$('#usernameId').val(userId);
		$("#formId").submit();
	console.log(userId);
	}
	</script>
	
	 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
	
</body>

</html>
