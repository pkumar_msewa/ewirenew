<script>
	$(document).ready(function() {
		$.ajax({
			type : "GET",
			url : "${pageContext.request.contextPath}/Admin/AdminAccess",

			success : function(response) {
				console.log(response);
				console.log(response.details.addUser);
				console.log(!response.details.addUser);
				if (!(response.details.addUser)) {
					$("#addu").hide();
					//	$("#kycreq").hide();
					$("#addu").hide();
					$("#blockunblock").hide();
					$("#activeUserBlock").hide();
					$("#acceptreasonid").hide();
					$("#rejectreasonid").hide();
					$("#bulkuploadregid").hide();
					$("#bulksmsid").hide();
					$("#acceptgrouprequestid").hide();
					$("#rejectgrouprequestid").hide();
					$("#groupuserlistidassign").hide();
					$("#groupuserlistiddelete").hide();
					$("#agentidcustom").hide();
					$("#physicalcardrequestid").hide();
					$("#promoserviceid").hide();
					$("#impstransactionid").hide();
					$("#corporateagentid").hide();
					$("#adg").hide();
					$("#loadc").hide();
					$("#merchantagentid").hide();
					$("#bulkregid").hide();
					$("#bulksmsid").hide();
					$("#donateeid").hide();
					$("#userblocklistid").hide();
					$("#adminactivityid").hide();
					$("#noti").hide();
					$("#ver").hide();
					$("#kycuserlistid").hide();
				}
				if (!response.details.loadCard) {
					$("#loadc").hide();
				}
				if (!response.details.assignPhysicalCard) {
					$("#apc").hide();
				}
				if (!response.details.assignVirtualCard) {
					$("#avc").hide();
				}
				if (!response.details.addCorporate) {
					$("#aca").hide();
				}
				if (!response.details.addMerchant) {
					$("#amr").hide();
				}
				if (!response.details.addAgent) {
					$("#ada").hide();
				}
				if (!response.details.addDonation) {
					$("#addd").hide();
				}
				if (!response.details.addGroup) {
					$("#adg").hide();
				}
				if (!response.details.sendNotification) {
					$("#noti").hide();
				}
				if (!response.details.androidVersion) {
					$("#ver").hide();
				}
				if (!response.details.promoService) {
					$("#gpromo").hide();
					$("#cashb").hide();
				}
				if (!response.details.myUnblock) {
					$("#myUnblockbutton").hide();
				}
			}
		});
	});
</script>
<div class="left side-menu">
	<div class="slimscroll-menu" id="remove-scroll">

		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<!-- Left Menu Start -->
			<ul class="metismenu" id="side-menu">
				<li class="menu-title">Navigation</li>
				<li><a href="${pageContext.request.contextPath}/Admin/Home">
						<i class="fa fa-home"></i> <span>Dashboard</span>
				</a></li>
				<li><a href="javascript: void(0);"><i class="fa fa-users"></i>
						<span>User</span> <span class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/UserList">All
								User</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/ActiveUserList">Active
								User</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/KYCUserList">KYC
								User</a></li>
						<li id="kycreq"><a
							href="${pageContext.request.contextPath}/Admin/KycRequest">KYC
								Request</a></li>
						<li id="addu"><a
							href="${pageContext.request.contextPath}/Admin/AddUser">Add
								User</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/KycRequestRejected">Rejected
								Users(KYC)</a></li>
					</ul></li>

				<li><a href="javascript: void(0);"><i
						class="fa fa-credit-card"></i> <span>Cards</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/CardList">Virtual
								Cards</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/PhysicalCardList">Physical
								Cards</a></li>
						<li id="physicalcardrequestid"><a
							href="${pageContext.request.contextPath}/Admin/Card/PhysicalCardRequest">Physical
								Card Request</a></li>
						<%-- <li id="loadc"><a
							href="${pageContext.request.contextPath}/Admin/LoadCard">Load
								Card</a></li> --%>
						<li id="apc"><a
							href="${pageContext.request.contextPath}/Admin/AssignPhysicalCard">Assign
								Physical Card</a></li>
					</ul></li>

				<li><a href="javascript: void(0);"><i
						class="fa fa-file-text-o"></i> <span>Transactions</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/LoadMoney/Transactions">LoadMoney</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/LoadMoney/TransactionsUPI">LoadMoney
								UPI</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/SendMoneyDetails">SendMoney</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/LoadMoney/MdexTransaction">Mdex
								Transactions</a></li>
						<li id="impstransactionid"><a
							href="${pageContext.request.contextPath}/Admin/Imps/Transactions">Imps
								Transactions</a></li>
					</ul></li>

				<li id="promoserviceid"><a href="javascript: void(0);"><i
						class="fa fa-gift"></i> <span>Promo Service</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="gpromo"><a
							href="${pageContext.request.contextPath}/Admin/GeneratePromoCode">Generate
								PromoCode</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/PromoCodeList">List
								of PromoCode</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/PromoCode/RequestList">List
								of PromoCode Request</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/PromoCode/Transactions">Transactions</a></li>
						<li id="cashb"><a
							href="${pageContext.request.contextPath}/Admin/CashBack">CashBack</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/Cashback/Transactions">CashBack
								Transactions</a></li>

					</ul></li>

				<li id="corporateagentid"><a href="javascript: void(0);"><i
						class="fa fa-building-o"></i><span>Corporate Agent</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="aca"><a
							href="${pageContext.request.contextPath}/Admin/AddCorporateAgent">Add
								Corporate Agent</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/fetchCorporateAgent">Corporate
								Agent List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/fetchBulkUploadList">Bulk
								Upload List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/PrefundRequests">Corporate
								Prefund Request</a></li>
					</ul></li>

				<li id="merchantagentid"><a href="javascript: void(0);"><i
						class="fa fa-shopping-bag"></i><span>Merchant</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="amr"><a
							href="${pageContext.request.contextPath}/Admin/AddCMerchant">Add
								Merchant</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/fetchMerchantList">Merchant
								List</a></li>
					</ul></li>

				<li id="agentidcustom"><a href="javascript: void(0);"><i
						class="fa fa-user-secret"></i><span>Agent</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="ada"><a
							href="${pageContext.request.contextPath}/Admin/AddAgent">Add
								Agent</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/AgentList">Agent
								List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/AgentPrefundList">Agent
								Prefund Request</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/AgentTransactionList">Agent
								Transaction List</a></li>
					</ul></li>

				<li><a href="javascript: void(0);"><i
						class="fa fa-user-secret"></i><span>Group Services</span><span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="adg"><a
							href="${pageContext.request.contextPath}/Admin/AddGroup">Add
								Group</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/FetchGroupList">Group
								List</a></li>
						<li id="grl"><a
							href="${pageContext.request.contextPath}/Admin/GroupRequestList">Group
								Request List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/GroupUserListing">Group
								User List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/AcceptedUserGroup">Approved
								User List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/RejectedUserGroup">Rejected
								User List</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/DeletedUserGroup">Deleted
								User List</a></li>
						<li id="bulkregid"><a
							href="${pageContext.request.contextPath}/Admin/fetchBulkUploadListGroup">Bulk
								Registration Upload Request</a></li>
						<li id="bulksmsid"><a
							href="${pageContext.request.contextPath}/Admin/fetchBulkUploadListSms">Bulk
								SMS Upload Request</a></li>
					</ul></li>

				<li id="donateeid"><a href="javascript: void(0);"><i
						class="fa fa-building-o"></i><span>Donation Service</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li id="addd"><a
							href="${pageContext.request.contextPath}/Admin/AddDonation">Add
								Donatee</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Admin/DonationList">Donatee
								List</a></li>
					</ul></li>

				<li><a href="javascript: void(0);"><i
						class="fa fa-building-o"></i><span>Travel Service</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/BusDetailsList">Bus
								List</a></li>
					</ul></li>

				<li id="userblocklistid"><a href="javascript: void(0);"><i
						class="fa fa-building-o"></i><span>UserBlockList</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/UserBlockList">UserBlockList</a></li>
					</ul></li>

				<%--  <li>
                	 <a href="javascript: void(0);"><i class="fa fa-building-o"></i><span>Travel Service</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="${pageContext.request.contextPath}/Admin/BusDetailsList">Bus List</a></li>                       
                    </ul>
                </li>  --%>

				<li id="adminactivityid"><a href="javascript: void(0);"><i
						class="fa fa-bell-o"></i><span>Admin Activity</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/AdminActivity">Admin
								Activity List</a></li>
					</ul></li>

				<li id="noti"><a href="javascript: void(0);"><i
						class="fa fa-bell-o"></i><span>Notification Centre</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li><a
							href="${pageContext.request.contextPath}/Admin/SendNotification">Send
								Notification</a></li>
					</ul></li>

				<li id="ver"><a href="javascript: void(0);"><i
						class="fa fa-code-fork"></i> <span>Version</span> <span
						class="menu-arrow"></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<!-- <li><a href="${pageContext.request.contextPath}/Admin/AddVersion">Add Version</a></li> -->
						<li><a
							href="${pageContext.request.contextPath}/Admin/UpdateVersion">Update
								Version</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- Sidebar -->
		<div class="clearfix"></div>
	</div>
	<!-- Sidebar -left -->
</div>