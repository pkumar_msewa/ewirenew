<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<title>Verify | Ewire</title>
<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
		  type="image/png" />

	 <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script> 
	 
		
	<link rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
		  type='text/css'>
<%-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> --%>
	<script src="${pageContext.request.contextPath}/resources/assets/js/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
	
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/spinner/spinner.gif) center no-repeat #fff;
		}

		.short {
			font-weight: normal;
			color: #FF0000;
			font-size: 13px;
		}

		.weak {
			font-weight: normal;
			color: orange;
			font-size: 13px;
		}

		.good {
			font-weight: normal;
			color: #2D98F3;
			font-size: 13px;
		}

		.strong {
			font-weight: normal;
			color: limegreen;
			font-size: 13px;
		}
		fieldset {
			border: 1px solid #000;
			padding: inherit;
		}
		legend {
			text-align: left;
			width: auto;
		}
	</style>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

	 <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.js"></script>  --%>

	
	
	<script>
        $(document).ready(function() {
            var csrfHeader = $("meta[name='_csrf_header']").attr("content");
            var csrfToken = $("meta[name='_csrf']").attr("content");
            var headers = {};
            headers[csrfHeader] = csrfToken;
            $("#logMessage_success").html("");
    		$("#error_otp1").html("");
    		$('#verify_otp_key').val("");
    		
           /*  $("#login_resend_otp").click(function(){
    		
    			
            }); */
            
            var count = 0;
            $(".resend-otp").click(function () {
                if (count >= 5) {
                	 $("#common_error_div").modal("show");
                     $("#common_error_message").html("Please Login Again.");
                    
                } else {
                	count++;
                	var userNam = $('#username').val();
            		
            		console.log("username: "+userNam);
            		
               			 $.ajax({
                                type: "POST",
                                headers: headers,
                                contentType : "application/json",
                                url: "${pageContext.request.contextPath}/Admin/ResendDeviceBindingOTP",
                        		dataType : 'json',
                        		data : JSON.stringify({
                                    "username": "" + userNam + "",
                                }),
                                success: function (response) {
                                    if(response.code.includes("S00")){
                                    	$("#common_success_true").modal("show");
                        				$("#common_success_msg").html(response.message);
                        				 var timeout = setTimeout(function() {
                        					$("#common_success_true").modal("hide");
                        					$("#errorMsg").val("");						
                        				}, 1000); 
                 					} 
                                }
                            });
                }
            });
            
         
            
            var errorMsg = document.getElementById("errorMsg").value;
			console.log("Alert ID :: " + errorMsg);
			if (!errorMsg.length == 0) {
				$("#common_error_div").modal("show");
				$("#common_error_message").html(errorMsg);
				var timeout = setTimeout(function() {
					$("#common_error_div").modal("hide");
					$("#mobileToken").val("");
					$("#errorMsg").val("");						
				}, 3000);
			} 
			
			$('#common_error_div').on('hidden.bs.modal', function () {
				window.location = "${pageContext.request.contextPath}/Admin/Home";
            	})
			
            	
            
           /*   $('#common_error_div').on('hidden.bs.modal', function () {
            	window.location = "${pageContext.request.contextPath}/Admin/Home";
            	})   */
        });
    </script>
    
</head>
<body oncontextmenu="return false">

<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
<div class="se-pre-con"></div>
	<nav class="navbar navbar-default"
		style="min-height: 80px; margin-bottom: 0">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img
					src="<c:url value='/resources/admin/assets/images/logo.png'/>" alt="logo" style="width: 230px; margin-top: 8px;" ></a>
			</div>	 	
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="line" style="height: 4px; background: #17bcc8;"></div>
	<!-----------------end navbar---------------------->
	<div class="container">
		<div id="aboutus">
			<div class="row">
						<%-- <div class="alert alert-info"><center><b>${msg}</b></center></div> --%>
					<div class="col-md-4 offset-md-4 text-center mt-5">
					<fieldset class="pd-2">
						<legend>OTP Verification</legend>
						<spring:form modelAttribute="loginDTO" action="${pageContext.request.contextPath}/Admin/Login/Process" method="post" cssClass="form form-horizontal">
							<spring:hidden path="username" />
							<spring:hidden path="password" />
							
							<div class="group">
								<spring:input path="mobileToken" class="numeric form-control" placeholder="Enter OTP" maxlength="6" size="6" onkeypress="return isNumberKey(event);" required="required" autocomplete="off"/>
								<span class="highlight"></span>
								<span class="bar"></span> <!-- <label style="margin-left: 11px;">Enter OTP</label> -->
								<c:choose>
									<c:when test="">
									</c:when>
									<c:otherwise>									
									</c:otherwise>
								</c:choose>
							</div>
	<br>
	                         <div class="group_1">
								 <button type="button" class="btn btn-md btncu resend-otp" data-toggle="modal" data-target="resendOTP" id="login_resend_otp"
										style="margin-bottom: 5px;margin-left: 0px; background:#0095ff">Resend OTP</button>
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
	                        <button class="btn btn-md btncu"
								style="margin-bottom: 5px; margin-left: -10px; margin-right:10px; background:#0095ff" id="login_verify_mobile">Continue</button>
					</div>
								
						</spring:form>
					</fieldset>
                        </div>	
                        
                         <div id="common_error_div" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="common_error_message" class="alert alert-danger"></center>
								</div>
							</div>
						</div>
					</div>			

 <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
			
			</div>
		</div>
		

	</div>
	
	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
	
</body>
</html>