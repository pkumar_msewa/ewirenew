<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
<head>
        <meta charset="utf-8" />
        <title>Admin | Load Card</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	 <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<script type="text/javascript">var contextPath= "${pageContext.request.contextPath}";</script>
        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
            .tgl {
              position: relative;
              display: inline-block;
              height: 30px;
              cursor: pointer;
            }
            .tgl > input {
              position: absolute;
              opacity: 0;
              z-index: -1;
              /* Put the input behind the label so it doesn't overlay text */
              visibility: hidden;
            }
            .tgl .tgl_body {
              width: 60px;
              height: 30px;
              background: white;
              border: 1px solid #dadde1;
              display: inline-block;
              position: relative;
              border-radius: 50px;
            }
            .tgl .tgl_switch {
              width: 30px;
              height: 30px;
              display: inline-block;
              background-color: white;
              position: absolute;
              left: -1px;
              top: -1px;
              border-radius: 50%;
              border: 1px solid #ccd0d6;
              -moz-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              box-shadow: 0 2px 2px rgba(0, 0, 0, 0.13);
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -moz-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -o-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), -webkit-transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), transform 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              z-index: 1;
            }
            .tgl .tgl_track {
              position: absolute;
              left: 0;
              top: 0;
              right: 0;
              bottom: 0;
              overflow: hidden;
              border-radius: 50px;
            }
            .tgl .tgl_bgd {
              position: absolute;
              right: -10px;
              top: 0;
              bottom: 0;
              width: 55px;
              -moz-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -o-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              -webkit-transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              transition: left 250ms cubic-bezier(0.34, 1.61, 0.7, 1), right 250ms cubic-bezier(0.34, 1.61, 0.7, 1);
              background: #439fd8 url("http://petelada.com/images/toggle/tgl_check.png") center center no-repeat;
            }
            .tgl .tgl_bgd-negative {
              right: auto;
              left: -45px;
              background: white url("http://petelada.com/images/toggle/tgl_x.png") center center no-repeat;
            }
            .tgl:hover .tgl_switch {
              border-color: #b5bbc3;
              -moz-transform: scale(1.06);
              -ms-transform: scale(1.06);
              -webkit-transform: scale(1.06);
              transform: scale(1.06);
            }
            .tgl:active .tgl_switch {
              -moz-transform: scale(0.95);
              -ms-transform: scale(0.95);
              -webkit-transform: scale(0.95);
              transform: scale(0.95);
            }
            .tgl > :not(:checked) ~ .tgl_body > .tgl_switch {
              left: 30px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd {
              right: -45px;
            }
            .tgl > :not(:checked) ~ .tgl_body .tgl_bgd.tgl_bgd-negative {
              right: auto;
              left: -10px;
            }
            .options {
                font-size: 16px;
            }
            .gj-datepicker-bootstrap [role=right-icon] button {
            	padding: 18px 0;
            }
            
             input[name = 'captcha'] {
            	border: 0;
            	background: transparent;
            	text-align: center;
            	font-size: 28px;
            	letter-spacing: 5px;
            	font-style: italic;
            }
            
            .form-group .refresh {
            	position: absolute;
            	top: 76%;
            	right: 45%;
            	cursor: pointer;
            }
        </style>

    </head>


    <body oncontextmenu="return false" onload="generateCaptcha()">

		<!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Load Card</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                    <div style="color: green; text-align: center; ">${succMsg}</div>
                                    <div style="color: red; text-align: center; ">${errorMsg}</div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    
                                    <form action="${pageContext.request.contextPath}/Admin/LoadCard" id="formId"  method="post">
                                            <div class="row">
                                                <div class="col-6 offset-md-3 offset-md-3t-sm-3">
                                                    <fieldset>
                                                        <legend>User Details</legend>
                                                        <div class="form-group">
                                                            <label for="mobile">Mobile</label>
                                                            <input type="text" name="contactNo" id="contactNo" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="16">
                                                        <p id="ferror" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="amount">Amount</label>
                                                            <input type="text" name="amount" id="amount" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="4">
                                                        <p id="amountError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="lname">Comment(optional)</label>
<!--                                                             <input type="text" name="lastName" id="lastName" class="form-control"  onkeypress="return isAlphKey(event);"> -->
                                                       	<textarea rows="5" cols="46" maxlength="250" name="comment" id="comment"></textarea>
                                                       	<p id="lastError" style="color: red" ></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="options">User Request</span>
                                                            <label class="tgl">
                                                                <input type="checkbox" id="option" name="transactionType" checked/>
                                                                <span class="tgl_body">
                                                                    <span class="tgl_switch"></span>
                                                                    <span class="tgl_track">
                                                                        <span class="tgl_bgd"></span>
                                                                        <span class="tgl_bgd tgl_bgd-negative"></span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                            <span class="options">Admin Request</span>
                                                        </div>
                                                        <div class="form-group" id="proxy">
                                                            <label for="proxy">Transaction Ref No</label>
                                                            <input type="text" name="transactionrefNo" id="transactionrefNo" maxlength="16" min="16"  class="form-control"  onkeypress="return isNumberKey(event);">
                                                       	<p id="proxyNumbererror" style="color: red" ></p>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            	<div class="form-group" style="margin-bottom: 0;">
											<input type="text" id="captcha" name="captcha" disabled="disabled" style="margin-left: 269px;
   					 			margin-top: 5px;">&nbsp;<span class="fa fa-refresh refresh" onclick="generateCaptcha();" title="Refresh" ></span>
										</div>
  										<div class="form-group">
 											<input type="text" id="captchaText" name="captchaText" class="form-control" onkeypress="return isNumberKey(event);" maxlength="4" placeholder="Enter Captcha" style="width: 270px;
    							margin-left: 340px;" required> 					
    						<p id="captError" style="color: red; margin-left: 346px;" ></p>
  										</div>
                                            <center><button type="button" class="btn btn-primary mt-4" onclick="validateform()" disabled="disabled">Load</button></center>
                                        </form>
                                </div>
                            </div>384570
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
         <div id="common_error_div" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="common_error_message" class="alert alert-danger"></center>
								</div>
							</div>
						</div>
					</div>	
					
					<!-- MODAL BEGINS -->

<div class="modal fade" id="loadCardModal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" >
                     OTP 
                </h4>
            </div>
            
          
            <div class="modal-body">                
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label for="address1" id="myModalLabelOtp" style="color:green"></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="address1" placeholder="Enter OTP" name="address1" maxlength="6" onkeypress="return isNumberKey(event);" required/>
                        <input type="hidden" name="loadcardOtp" id="loadcardOtp"/>
                        <span class="error" style="color: red;" id="reasonError"></span>
                    </div>
                  </div>                             
                </form>
            </div>
            
            
            <div class="modal-footer">
                <button type="button" id="resendloadotp" class="btn btn-default">Resend</button>
                <button type="button" class="btn btn-primary" onclick="verifyOtp()">
                    Submit</button>
            </div>
        </div>
    </div>
</div>


<!-- MODAL ENDS -->        


         <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#option').change(function () {
                  $('#proxy').fadeToggle();
                });
            });

        </script>
        
     <script>
            $('.datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
    </script>
    
    
    <script>
    	
    	function validateform(){
    	var valid = true;
    	var input = document.getElementById('captchaText').value;
    	
    	 $("#myModalLabelOtp").html("");  
    	var contactNo  = $('#contactNo').val();
    	var amount = $('#amount').val() ;
    	
    	if(contactNo.length <=0){
    		$("#ferror").html("Please enter your contact no");
    		valid = false;
    	}
    	if(amount.length  <= 0){
    		$("#amountError").html("Please enter the amount ");
    		valid = false; 
    	}
    	
    	if(input.length <= 0) {
    		$("#captError").html("Please enter the captcha ");
    		valid = false;
    	}

    if(valid == true) {
    	 var input = document.getElementById('captchaText').value;
         
         if (captcha == input) {
    		//$("#formId").submit();
    		sendOtp();    		 		
         } else {
        	 $("#common_error_div").modal("show");
        	 $("#common_error_message").html("Invalid Captcha");
         }
    } 
    
    var timeout = setTimeout(function(){
    	$("#ferror").html("");
    	$("#amountError").html("");
    	$("#captError").html("");
    	 $("#address1").val("");  
    	 $("#reasonError").html("");
    }, 4000);
    }
    	
    	var count = 0;
    	 function sendOtp() {
    		 $("#myModalLabelOtp").html("");      		
    		$.ajax({
	    		type: "POST",
	    		contentType : "application/json",
	    		url : "${pageContext.request.contextPath}/Admin/LoadCardOtp",
	    		dataType : 'json',  
	    		data: JSON.stringify ({
	    						
	    		}),
	    		success : function(response) {
	    			if(response.code == "S00") {
	    				$("#loadCardModal").modal("show");   
	    				$("#loadcardOtp").val(response.status);	
	    				$("#myModalLabelOtp").html("OTP sent");
	    			}
	    		}
	    	});
    	}
    	 
    	 var count = 0;
    	 $("#resendloadotp").click(function () {
    		 count++;
    		 console.log("count:: "+count);
    		 $("#myModalLabelOtp").html("");
    		 if(count >= 5) {    			
    			 $("#resendloadotp").addClass("disabled");
    		 } else {    			 
    			 $("#resendloadotp").addClass("disabled");
    			 $.ajax({
    		    		type: "POST",
    		    		contentType : "application/json",
    		    		url : "${pageContext.request.contextPath}/Admin/LoadCardOtp",
    		    		dataType : 'json',  
    		    		data: JSON.stringify ({
    		    						
    		    		}),
    		    		success : function(response) {
    		    			if(response.code == "S00") {
    		    				 $("#resendloadotp").removeClass("disabled");
    		    				$("#loadCardModal").modal("show");   
    		    				$("#loadcardOtp").val(response.status);	
    		    				$("#myModalLabelOtp").html("OTP sent");
    		    			}
    		    		}
    		    	});
    		 }
    	 });
    	    
    	    function verifyOtp() {
    	    	$("#myModalLabelOtp").html(""); 
    	    	var valid = true;
    	    	var otp = $("#address1").val();
    	    	var sentOtp = $("#loadcardOtp").val();
    	    	if(otp.length <= 0) {
    	    		$("#reasonError").html("Please enter otp");
    	    		valid = false;
    	    	}
    	    	if(valid) {
	    	    	if(otp == sentOtp) {
	    	    		$("#loadCardModal").modal("hide");
	    	    		$("#formId").submit();
	    	    	} else {
	    	    		$("#loadCardModal").modal("hide");
	    	    		$("#common_error_div").modal("show");
	    	    		$("#common_error_message").html("Invalid OTP");
	    	    		$("#address1").val("");
	    	    		var timeout = setTimeout(function() {   
	    	    			$("#common_error_div").modal("hide");
	    	        	 	$("#captchaText").val("");
	    	    		}, 2000);
	    	    	}
    	    	}
    	    }
    </script>
    
    
  
    
    
    <script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
    
    <script type="text/javascript">
function isAlphNumberKey(evt){
    var k = (evt.which) ? evt.which : evt.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
}
</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script>
    
    // Captcha Script Starts here

    var captcha;

    function generateCaptcha() {
      var a = Math.floor((Math.random() * 10));
      var b = Math.floor((Math.random() * 10));
      var c = Math.floor((Math.random() * 10));
      var d = Math.floor((Math.random() * 10));

      captcha = a.toString() + b.toString() + c.toString() + d.toString();

      document.getElementById('captcha').value = captcha;     
      
    }

   
  </script>
    
    <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script>  


    
    </body>

</html>