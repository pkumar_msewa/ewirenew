<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Bus Details</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
<script>
var contextPath = "${pageContext.request.contextPath}";
</script>
<!-- DataTables -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- App css -->
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
<style>
.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/resources/spinner.gif) center no-repeat #fff;
		}
</style>
</head>
<body oncontextmenu="return false">

<div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Bus Details</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
						
						<!-- datepicker strats here -->
						
						<div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
										
											<form action="<c:url value="/Admin/BusDetailsList"/>" 
												method="post" class="form form-inline">
												
												<div class="form-group">
													<div id="" class="pull-left" style="cursor: pointer;">
														<label class="" for="filterBy" style="justify-content: left;">Filter By:</label>
														<input id="reportrange" name="reportrange" class="form-control"  value="${dat}" readonly="readonly"/>
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary"
														title="Search" style="margin-left: 10px; margin-top: 20px;">
														Search
													</button>
												</div>
											</form>
																		
										</div>
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->

                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th>S.No</th>
												<th>User Details</th>
												<th>User Contact Details</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>Ticket PNR No.</th>
												<th>Operator PNR No.</th>
												<th>Booking Status</th>
												<th>Transaction Status</th>
												<th>Source</th>
												<th>Destination</th>												
												<th>Transaction Amount</th>
												<th>Commission</th>
												<th>Ticket Details</th>																
                                            </tr>
                                        </thead>
                                       <tbody>
                                       <c:forEach items="${busList}" var="slist" varStatus="loopCounter">
									<tr>
									<td>${loopCounter.count}</td>																	
																	
									<td><c:out value="${slist.firstName} ${slist.lastName}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.userMobile}" default="" escapeXml="true"></c:out><br/>
										<c:out value="${slist.userEmail}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.transactionId}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.transactionDate}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.ticketPnr}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.operatorPnr}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.status}" default="" escapeXml="true"></c:out></td> 
									<td><c:out value="${slist.transactionStatus}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.source}" default="" escapeXml="true"></c:out></td>
									<td><c:out value="${slist.destination}" default="" escapeXml="true"></c:out></td>									
									<td><b>Total Txn Amount: </b><c:out value="${slist.totalFare}" default="" escapeXml="true"></c:out><br></td>
									<td><c:out value="${slist.commissionAmount}" default="" escapeXml="true"></c:out></td>
								 	<td>
										<button id="view_ticket" type="button" style="background-color: #5cb85c; color: #fff;" onclick="viewTicketDetails('${slist.emtTxnId}')">View</button>
									</td> 		
								</tr>		 
							</c:forEach> 
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_success_msg" class="alert alert-success"></center></B>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<B><center id="common_error_msg" class="alert alert-danger"></center></B>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

<!-- ========================= Modal For traveller Details ==================================== -->
	<div id="travellerDetails" class="modal fade" role="dialog"	>
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body">
					<button type="button" class="close close2" data-dismiss="modal"
						id="clsesits">&times;</button>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							 <img
								src="${pageContext.request.contextPath}/resources/admin/assets/images/Bus.png"
								style="width: 4%;"><span class="modal-title bookinghead"
								id="m_bus_nm">HMS Jabbar Travels</span> <br> &nbsp;&nbsp;<b>Journey
								Date :</b><span id="m_bus_doj">7/27/2017 </span> <span
								id="m_bus_toj">12:00:00 AM</span> &nbsp;<b>Source :</b><span
								id="src"></span> &nbsp;<b>Destination :</b><span id="dest"></span>

						</div>
					</div>
					<hr style="margin-top: 5px;">

					<br>

					<div class="row">
						<div class="row">
							<div class="col-md-12">
								<!-- TABLE HOVER -->
								<!-- page content -->
								<table id="editedtable" class="table table-striped ">
								<thead>
									<tr id="xyz">
										<th>Sl. No.</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Age</th>
										<th>Gender</th>
										<th>Seat No.</th>
										<th>Fare</th>
									</tr>
									</thead>
									<tbody id="editedtable">
									
									</tbody>
								</table>
								<nav>
									<ul class="pagination" id="paginationn">
									</ul>
								</nav>
								<!-- End Page Content -->
								<!-- END TABLE HOVER -->

								<span style="color: red; padding-left: 541px;">Refunded
									Amount(<i class="fa fa-inr"></i>): </span><span id="refAmt"></span><br> <span
									style="color: red; padding-left: 541px;">Cancellation
									Charge (<i class="fa fa-inr"></i>): </span><span id="cnclAmt"></span>

								<div align="center">
									<br> <input type="hidden" id="txnRefNo">
									<c:choose>
									<c:when test="${addUserBool eq true}">
									<button type="button" id="cnclTckt"
										style="background-color: #d43f3a; border-color: #d43f3a; color: #FFFFFF;">Cancel Ticket</button>
									</c:when>
									<c:otherwise>
									</c:otherwise>
									</c:choose>
									<p style="color: #d43f3a; font-size: large;" id="cncl_msg">Ticket is Cancelled.</p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	
	<!-- End of Modal for Cancel Ticket Confirmation -->
	<div id="cancel_confirm" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<div align="center">
						<h3 class="modal-title">Confirmation!</h3>
					</div>
				</div>
				<input type="hidden" name="prodId" class="form-control input-sm"
					id="rejProdId" required="required" />
				<div class="modal-body">
					<div class="row">
						<div align="center">
							 <img
								src="${pageContext.request.contextPath}/resources/admin/assets/images/delete.png"
								class="img-responsive" style="width: 20%;"> 
						</div>
					</div>
					<div class="row">
						<div align="center">
							<h2>Are you sure!</h2>
						</div>
					</div>
					<div class="row">
						<div align="center">
							<p>Are you sure that you want to cancel this ticket?</p>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-success" data-dismiss="modal"
								style="float: right;">No</button>
						</div>
						<div class="col-md-6">
							<button class="btn btn-danger" id="ticketConfCancel">Yes,
								Cancel it!</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" id="close3">&times;</button>
          <center>
        <div style="background: white; padding: 40px;">
          <h3 id="cnclMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
      </div>
    </div>
  </div>


        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );
		
        </script>
        
        <script>
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'excel', 'pdf', 'csv' ]
            } );
         
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
        
        </script>
        
               
        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		
		
	<script type="text/javascript">
		function viewTicketDetails(emtTxnId) {
			/* $("#axz").show(); */	
			$('#cnclTckt').hide(); 
			 $("#cncl_msg").hide();			
			
			/*   $('#editedtable')
					.html(
							'<tr class="testingg"><th>SL.NO</th><th>First Name</th><th>Last Name</th><th>Date Of Birth</th><th>Gender</th><th>Seat No.</th><th>Fare</th></tr>');  */ 
			var contextPath = "${pageContext.request.contextPath}";
			 var spinnerUrl = "Please wait <img src="+contextPath+"/resources/images/spinner.gif style='width:20px; hight:10px;'>"
			$("#view_ticket").addClass("disabled");
			$("#view_ticket").html(spinnerUrl); 
			$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Admin/getSingleTicketDetails",
						dataType : 'json',
						data : JSON.stringify({
							"emtTxnId" : emtTxnId,
						}),
						success : function(response) {
							$(".se-pre-con").fadeOut("slow");
							console.log("Response :: " + response.code);

							if (response.code.includes("S00")) {
								console.log("response:: "+response.details);
								console.log("Extra Info :: "
										+ response.extraInfo.busOperator);
							 	var trHTML = '';
							 	$(".testingg").empty();
								 for (var i = 0; i < response.details.length; i++) {
									var trHTML = '';
									trHTML += '<tr class="testingg"><td>'
											+ (i + 1) + '</td><td>'
											+ response.details[i].fName
											+ '</td><td>'
											+ response.details[i].lName
											+ '</td><td>'
											+ response.details[i].age
											+ '</td><td>'
											+ response.details[i].gender
											+ '</td><td>'
											+ response.details[i].seatNo
											+ '</td><td>'
											+ response.details[i].fare
											+ '</td></tr>';
									$('#editedtable').append(trHTML);
									console.log("refAmount: "+ response.refAmt);
									console.log("fname:: "+response.details[i].fName);
									console.log("lname:: "+response.details[i].lName);
									console.log("age:: "+response.details[i].age);
									console.log("gender:: "+response.details[i].gender);
									console.log("seatno:: "+response.details[i].seatNo);
									console.log("fare:: "+response.details[i].fare);
									console.log("refamount:: "+response.refAmt);
									console.log("cancel charges:: "+response.cancellationCharges);
									console.log("transaction ref no:: "+response.transactionRefNo);
									$('#refAmt').html(response.refAmt);
									$('#cnclAmt').html(
											response.cancellationCharges);
									$('#txnRefNo').val(
											response.transactionRefNo);

								}
								console.log("status:: "+response.extraInfo.status);
								console.log("bus opertaor:: "+response.extraInfo.busOperator);
								console.log("source:: "+response.extraInfo.source);
								console.log("destination:: "+response.extraInfo.destination);
								console.log("journey date:: "+response.extraInfo.journeyDate);
								console.log("arr time:: "+response.extraInfo.arrTime);
								
								if (response.extraInfo.status.includes("Booked")) {
									$("#cnclTckt").show();
								} else if (response.extraInfo.status.includes("Cancelled")) {
									$("#cncl_msg").show();
								} 
								$("#m_bus_nm").html(response.extraInfo.busOperator);
								$("#src").html(response.extraInfo.source);
								$("#dest").html(response.extraInfo.destination);
								$("#m_bus_doj").html("Date: "+ response.extraInfo.journeyDate);
								$("#m_bus_toj").html("Time: " + response.extraInfo.arrTime); 
								$("#view_ticket").addClass("removeClass");
								$("#view_ticket").html("View"); 
								$("#travellerDetails").modal("show");
							} else {
								location.reload();
							}

						}
					});
		}
	</script>
	<script>
	$("#cnclTckt").click(function() {
		$("#cancel_confirm").modal('show');
	});
	$("#ticketConfCancel").click(
			function() {
				var txnNo = $('#txnRefNo').val();
				var contextPath = "${pageContext.request.contextPath}";
				var spinnerUrl = "Please wait <img src="+contextPath+"/resources/images/spinner.gif style='width:20px; hight:10px;'>"
				$("#ticketConfCancel")
						.addClass("disabled");
				$("#ticketConfCancel").html(
						spinnerUrl);
				$.ajax({
							type : "POST",
							contentType : "application/json",
							url : "${pageContext.request.contextPath}/Admin/cancelBookedTicket",
							dataType : 'json',
							data : JSON.stringify({
										"vPqTxnId" : txnNo,
									}),
							success : function(response) {
								console.log("Response :: "+ response.code);
								if (response.code.includes("S00")) {
									$("#cnclMsgVal").html('Ticket Cancel Successfully');
								} else {
									$("#cnclMsgVal").html('Unable To Cancel Ticket');
								}
								$("#cancel_confirm").modal('hide');
								$("#travellerDetails").modal('hide');
								$("#myModal").modal('show');
								$("#myModal").on('hidden.bs.modal',	function() {
									location.reload();
								});
							}
						});
			});
	
	</script>
	
 	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
	
</body>
</html>