<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Admin | Add Group</title>
<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}";
</script>

<!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        
         <!-- jQuery  -->
  		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
  		

        <style>
            fieldset {
                min-width: 0;
                padding: 10px 30px;
                margin: 0;
                border: 1px solid #efefef;
            }
            fieldset legend {
                width: auto;
                font-size: 1.3rem;
            }
            .fileUpload {
              position: relative;
              overflow: hidden;
              height: 43px;
              margin-top: 0;
            }
            
            .error{
            color:red;
            }

            .fileUpload input.uploadlogo {
              position: absolute;
              top: 0;
              right: 0;
              margin: 0;
              padding: 0;
              font-size: 20px;
              cursor: pointer;
              opacity: 0;
              filter: alpha(opacity=0);
              width: 100%;
              height: 42px;
            }

            /*Chrome fix*/
            input::-webkit-file-upload-button {
              cursor: pointer !important;
              height: 42px;
              width: 100%;
            }
            .blue-btn:hover,
            .blue-btn:active,
            .blue-btn:focus,
            .blue-btn {
              background: transparent;
              border: solid 1px #27a9e0;
              border-radius: 3px;
              color: #27a9e0;
              font-size: 14px;
              margin-bottom: 20px;
              outline: none !important;
              padding: 10px 20px;
            }
        </style>
        
        <script type="text/javascript">
       /*  $(document).ready(function() { */
        	$( document ).ready(function() {
					
				var errorMsg = document.getElementById("errorMsg").value;
				console.log("Alert ID :: " + errorMsg);
				if (!errorMsg.length == 0) {
					$("#common_error_true").modal("show");
					$("#common_error_msg").html(errorMsg);
					var timeout = setTimeout(function() {
						$("#common_error_true").modal("hide");
						$("#errorMsg").val("");						
					}, 3000);
				}

				var successMsg = document.getElementById("successMsg").value;
				console.log("successMsg :: " + successMsg);
				if (!successMsg.length == 0) {
					$("#common_success_true").modal("show");
					$("#common_success_msg").html(successMsg);
					var timeout = setTimeout(function() {
						$("#common_success_true").modal("hide");
						$("#successMsg").val("");						
					}, 2000);
				}
						
		});
	</script>

</head>

<body oncontextmenu="return false">

<input type="hidden" name="successMsg" id="successMsg" value="${groupAddMsg}">
<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
 <!-- Begin page -->
        <div id="wrapper">
						   <!-- Top Bar Start -->
        <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Add Group</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                <center><div><h6 style="font: bold;color: red;">${msg}</h6></center>
                                                                       
                                    <form action="${pageContext.request.contextPath}/Admin/AddGroup" method="POST" id="formId" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col">
                                                <fieldset>
                                                    <legend>Group Details</legend>
                                                    <div class="form-group">
                                                        <label for="name">Group Name</label>
<!--                                                         <input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
                                                   			<input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return groupname_error(event); groupname_error(event);">
                                                   			<p class="error" id="error_fullName"></p>
                                                    </div>   
                                                   <div class="form-group">
                                                        <label for="name">Display Name</label>
<!--                                                         <input type="text" name="name" placeholder="Enter Group Name" id="name" class="form-control" maxlength="60" onkeypress="return isAlphKey(event); groupname_error(event);"> -->
                                                   			<input type="text" name="dname" oninput="this.value = this.value.toUpperCase()" placeholder="Display name (max 7 letters only)" id="dname" class="form-control" maxlength="7" onkeypress="return displayname_error(event);">
                                                   			<p class="error" id="error_displayName"></p>
                                                    </div>     
                                                    
                                                     <div class="form-group">
                                                        <label>Email</label>
<!--                                                         <input type="email" name="emailAddressId" placeholder="Enter Email" id="emailAddressId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control"> -->
                                                   			<input type="email" name="emailAddressId" placeholder="Enter Email" id="emailAddressId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" onkeypress="email_error(event);" class="form-control">
                                                   			<p class="error" id="error_email"></p>
                                                    </div>                           
                                                      <div class="form-group">
                                                        <label>Contact Number</label>
<!--                                                         <input type="text" name="mobileNoId" placeholder="Enter Mobile" id="mobileNoId" class="form-control" minlength="10" maxlength="10" onkeypress="return isNumberKey(event);"> -->
                                                  			<input type="text" name="mobileNoId" placeholder="Enter Mobile" id="mobileNoId" class="form-control" minlength="10" maxlength="10" onkeypress="return mobile_error(event);">
                                                  		<p class="error" id="error_mobile"></p>
                                                    </div>                                              
	                                                <input type="hidden" id="countryCodeId" name="countryCode" />
	                                                
                                                    </div>
                                                    <div class="col">
                                                    <fieldset>
                                                   
                                                     <div class="form-group">
	                                                   <label>Address</label>
<!-- 	                                                   <input type="text" name="address" placeholder="Enter Address" id="address" class="form-control"> -->
	                                                 <input type="text" name="address" placeholder="Enter Address" id="address" onkeypress="address_error(event);" class="form-control">
	                                                 	<p class="error" id="error_address"></p>
	                                                </div>       
	                                                <div class="form-group">
	                                                   <label>State</label>
<!-- 	                                                   <input type="text" name="state" placeholder="Enter State" id="state" class="form-control" maxlength="60" onkeypress="return isAlphKey(event);"> -->
	                                                  	<input type="text" name="state" placeholder="Enter State" id="state" class="form-control" maxlength="60" onkeypress="return state_error(event);">
	                                                  	<p class="error" id="error_state"></p>
	                                                </div>
	                                                												
                                                    <div class="form-group">
                                                        <label>City</label>
<!--                                                         <input type="text" name="city" placeholder="Enter City" id="city" class="form-control" maxlength="60" onkeypress="return isAlphKey(event);"> -->
                                                  		<input type="text" name="city" placeholder="Enter City" id="city" class="form-control" maxlength="60" onkeypress="return city_error(event);">
                                                  		<p class="error" id="error_city"></p>
                                                    </div>           
                                                                         
                                                    
                                                    
                                                    </fieldset>
                                                     <div class="col">
                                                <fieldset>
                                                    <legend>Upload Documents</legend>
                                                    <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Documents</span>                                                           
                                                            <input type="file" class="uploadlogo" name="image" id="aadhar_Image"/>
                                                         	
                                                          </div>
                                                          <p class="error" id="error_aaadharCardImg"></p>
                                                    </div>
                                                  <!--   <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Corporate Logo</span>
                                                            <input type="file" class="uploadlogo" name="corporateLogo2" id="pan_Image" />
                                                        </div>
                                                          <p class="error" id="error_panCardImg"></p>
                                                    </div>
  -->                                               </fieldset>
                                            </div>
                                                         </div>
 												</fieldset>
                                            </div>
                                           <!--  <div class="col">
                                                <fieldset>
                                                    <legend>Upload Documents</legend>
                                                    <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Documents</span>
                                                            <input type="file" class="uploadlogo" name="image" id="aadhar_Image"/>
                                                          </div>
                                                          <p class="error" id="error_aaadharCardImg"></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="fileUpload blue-btn btn width100">
                                                            <span>Upload Corporate Logo</span>
                                                            <input type="file" class="uploadlogo" name="corporateLogo2" id="pan_Image" />
                                                        </div>
                                                          <p class="error" id="error_panCardImg"></p>
                                                    </div>
                                                </fieldset>
                                            </div> -->
                                           
                                            <center><button type="button" id="addAgent" class="btn btn-primary mt-4" onclick="addCorporateAgent()">Add Group</button></center>
                                        </form>
                                         </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>

<script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

        <!-- Daterange picker -->
        <script type="text/javascript">
            $(function() {

                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                
            });
            </script>

            <script>
                // You can modify the upload files to pdf's, docs etc
            //Currently it will upload only images

            $(document).ready(function($) {

              // Upload btn on change call function
              $(".uploadlogo").change(function() {
                var filename = readURL(this);
                $(this).parent().children('span').html(filename);
              });
	
              // Read File and return value  
              function readURL(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (
                  ext == "jpg"
                  ) || ext == "png" || ext == "jpeg") {
                  var path = $(input).val();
                  var filename = path.replace(/^.*\\/, "");
                  // $('.fileUpload span').html('Uploaded Proof : ' + filename);
                  return "Uploaded file : "+filename;
                } else {
                  $(input).val("");
                  return "Only jpg/png/jpeg format is allowed!";
                }
              }
              // Upload btn end

            });
            </script> 
            
            
            
            
            
           <script type="text/javascript">
           function addCorporateAgent(){
        		var valid=true;
        		/* var ifscPattern= "[A-Za-z]{4}0[A-Z0-9a-z]{6}$";
        		var panPattern= "[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}"; */
        		
        		$("#error_fullName").html("");
        		$("#error_displayname").html("");
        		$("#error_email").html("");
        		$("#error_address").html("");
        		$("#error_state").html("");
        		$("#error_city").html("");
        		$("#error_mobile").html("");
        		$("#error_aaadharCardImg").html("");
        		
        		var groupName=$('#name').val();
        		var displayname = $("#dname").val();
        		var mobile=$("#mobileNoId").val();
        		var email=$('#emailAddressId').val();
        		var address=$('#address').val();
        		var city=$('#city').val();
        		var state = $('#state').val();
        		var aadharCardImg=$('#aadhar_Image').val();
        		var countryCode = "91";
    			
    			$("#countryCodeId").val(countryCode);
        		       		
        		if(groupName=='') {
        			$("#error_fullName").html("Please enter Group Name");
        			valid = false;
        			console.log("valid: "+"fullName");
        		}
        		if(displayname=='') {
        			$("#error_displayName").html("Please enter display name");
        			valid = false;
        			console.log("valid: "+"displayname");
        		}
        		if(email=='') {
        			$("#error_email").html("Please enter email id");
        			valid = false;
        			console.log("valid: "+"bankName");
        		}
        		if(email!=''){
        			if(!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}")){
            			$("#error_email").html("Please enter valid email id");
							valid=false;
        			}
        		}
        		if(address=='') {
        			$("#error_address").html("Please enter address");
        			valid = false; 
        			console.log("valid: "+"address");
        		}
        		if(state=='') {
        			$('#error_state').html("Please enter state");
        			valid = false;
        			console.log("valid: "+"state");
        		}
        		if(city=='') {
        			$('#error_city').html("Please enter city");
        			valid = false;
        			console.log("valid: "+"city");
        		}
        		
        		if(mobile=='') {
        			$("#error_mobile").html("Please enter contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        		if(mobile!=''){
        			if(mobile.length!=10){
        			$("#error_mobile").html("Please enter valid contact no");
        			valid = false;
        			console.log("valid: "+"mobile");
        		}
        	}
        		/* if (aadharCardImg!= "") {
        		    if (!((aadharCardImg.split(".")[1].toUpperCase() == "jpg") || (aadharCardImg.split(".")[1].toUpperCase() == "jpeg") || (aadharCardImg.split(".")[1].toUpperCase() == "png"))) {
        		    		
        		     $("#error_aaadharCardImg").html("File " + " is invalid. Upload a valid file");
        		     valid = false;
        		    }
        		   } */
        		if(aadharCardImg=='') {
        			$("#error_aaadharCardImg").html("Please attach Doc");
        			valid = false;
        		}
        		        		
        		if (valid) {
        			$('#formId').submit();
        			$("#addAgent").addClass("disabled");
        			document.forms["formId"].reset();
        			console.log("")
//         			$("#addAgent").html(spinnerUrl);
        		}
        	}

        	</script>
        	
        	<script type="text/javascript">
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function groupname_error(evt) {
			$("#error_fullName").html("");	
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
				
		}
		
		function displayname_error(evt) {
			$("#error_displayName").html("");	
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 && charCode == 32)
		}
		
		function email_error(evt) {
			$("#error_email").html("");			
		}
		
		function address_error(evt) {
			$("#error_address").html("");			
		}
		
		function state_error(evt) {
			$("#error_state").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			 return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function city_error(evt) {
			$("#error_city").html("");
			 var charCode = (evt.which) ? evt.which : evt.keyCode
			return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		function mobile_error(evt) {
			$("#error_mobile").html("");
			var charCode = (evt.which) ? evt.which : evt.keyCode
			return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}

	</script>

<script type="text/javascript">
function isAlphKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
}
</script>

<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
       
</body>
</html>