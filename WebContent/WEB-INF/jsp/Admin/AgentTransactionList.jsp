<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Transaction Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
        
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>

<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 70px; /* Location of the box */
    
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    padding-left: 60px;
    
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

</style>
</head>
<body oncontextmenu="return false">

	<div id="wrapper">
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
	</div>
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
	                <div class="col-12">
	                    <div class="page-title-box">
	                        <h4 class="page-title float-left">Agent Transaction Details</h4>
	                        <div class="clearfix"></div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col-12">
	            		<div class="card-box">
	            			<div class="col-md-12">
	            				<div class="tabble">
	            					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
	            					<thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Transaction Amount</th>
                                            <th>Date</th>
                                            <th>User Name</th>
                                            <th>User Email</th>
                                            <th>Physical card</th>
                                            <th>Agent Name</th>
                                            <th>Agent Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<c:forEach items="${agentTransactionList}" var="agentTransactionList" varStatus="loopCounter">
                                    		<tr>
                                    			<td>${loopCounter.count}</td>
                                    			<td>${agentTransactionList.amount}</td>
                                    			<td><fmt:formatDate pattern="dd-MM-yyyy" value="${agentTransactionList.created}" /></td>
                                    			<td>
                                    				${agentTransactionList.user.userDetail.firstName}
                                    				<c:if test="${not empty agentTransactionList.user.userDetail.lastName}">
                                    					${agentTransactionList.user.userDetail.lastName}
                                    				</c:if>
                                    			</td>
                                    			<td>
                                    				${agentTransactionList.user.userDetail.email}
                                    			</td>
                                    			<td>
                                    				<c:choose>
                                    					<c:when test="${agentTransactionList.physicalCard eq true}">Yes</c:when>
                                    					<c:otherwise>No</c:otherwise>
                                    				</c:choose>
                                    			</td>
                                    			<td>
                                    				${agentTransactionList.agent.userDetail.firstName}
                                    				<c:if test="${not empty agentTransactionList.agent.userDetail.lastName}">
                                    					${agentTransactionList.agent.userDetail.lastName}
                                    				</c:if>
                                    			</td>
                                    			<td>${agentTransactionList.agent.userDetail.email}</td>
                                    		</tr>
                                    	</c:forEach>
                                    </tbody>
	            				</table>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	<!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- Required datatable js -->
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap4.min.js"></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
         <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#example').DataTable( {
            	responsive: true,
                lengthChange: false,
                buttons: [ 'excel', 'pdf', 'csv' ]
            } );
         
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        });
        </script>
        
        <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
        
</body>
</html>