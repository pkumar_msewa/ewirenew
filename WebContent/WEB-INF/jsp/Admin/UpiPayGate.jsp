<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<h3>Please Wait while we're redirecting you to Payment Gateway...</h3>


	<form method="post" id="pay" action="http://106.51.8.246/cashier/ws/api/authValidate">
		<input type="hidden" class="form-control" name="id" value= "${fn:escapeXml(xxx.merchant_id)}"/>
		<input type="hidden" class="form-control" name="transactionID" value="${fn:escapeXml(xxx.transactionRefNo)}"  />
		<input type="hidden" class="form-control" name="amount" value="${fn:escapeXml(xxx.amount)}" />
		<input type="hidden" class="form-control" name="hash" value="${fn:escapeXml(xxx.hash)}" />
		<input type="hidden" class="form-control" name="additionalInfo" value="${fn:escapeXml(xxx.additionalInfo)}" />
	</form>
  	<script>document.getElementById('pay').submit();</script> 
</body>
</html>