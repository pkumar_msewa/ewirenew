<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> --%>
<html lang="en">
<head>
<title>Transaction List</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/linearicons/style.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/chartist/css/chartist-custom.css"/>">
<!-- MAIN CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/main.css"/>">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/demo.css"/>">
<!-- GOOGLE FONTS -->

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
	rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/resources/assets/img/apple-icon.png"/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value="/resources/assets/img/favicon.png"/>">
		
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>


<!-- DateRange -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />



<script type="text/javascript">
 	 $(document).ready(function() {
       datatableEnable();
       populateDropDownList();
   });
 	 
</script>
<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
// 				$(function() {
// 					$( "#toDate" ).datepicker({
// 						format:"yyyy-mm-dd"
// 					}).on('change', function() {
// 						   $('.datepicker').hide();
// 					  });
// 					$( "#fromDate" ).datepicker({
// 						format:"yyyy-mm-dd"
// 					}).on('change', function() {
// 						   $('.datepicker').hide();
// 					  });
// 				});
	</script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
</head>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">TRANSACTION LIST</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
					  <form id="formId" action="<c:url value="/Client/filteredTransactionlist"/>" method="post" onsubmit="return validate();" >
					 <div class="row">
					  <div class="col-md-12">
					<h4 style="position:absolute;font-size: 14px;font-weight: 600; margin-top: 2px;">Select Date Range*</h4>
						 	<div class="col-md-4 col-sm-4 col-xs-3" id="reportrange" 
									style="background: #fff; cursor: pointer;  border: 1px solid #ccc;margin-top: 26px; padding: 5px; border-radius: 4px; ">
									<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
									<span></span> <b class="caret"></b>
								</div>
						  <input type="hidden" id="daterange" name="daterange" value="" class="form-control" readonly/>

									<div class="col-md-3 col-sm-3 col-xs-3">
										<label>Services*</label> <select id="serviceId" name="service"
											class="form-control">
											<option value="All">All Services</option>
										</select>
										<p id="serviceMsg"></p>
									</div>



									<button type="submit" class="btn btn-primary" title="Search" style="margin-top: 25px;"><span class="glyphicon glyphicon-filter"></span></button>
<!-- 					 onclick="getAllData();"  -->
</div></div>
					  </form><p></p>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable" class="table table-striped table-bordered date_sorted">
									<thead>
										<tr >
										    <th>S.NO</th>
											<th>Transaction ID</th>
											<th>Transaction Date</th>
											<th>Transaction Type</th>
											<th>Retrieval Ref No</th>
											<th>Amount</th>
											<th>Status</th>
										</tr>	</thead>
									 	<tbody>
										<c:forEach items="${txnList}" var="txnList" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><c:out value="${txnList.transactionRefNo}"/></td>
											<td><c:out value="${txnList.created}"/></td>
										    <td><c:out value="${txnList.txnType	}"/></td>
											<td><c:out value="${txnList.retrivalReferenceNo	}"/></td>
											<td><c:out value="${txnList.amount	}"/></td>
													<c:choose>
														<c:when test="${txnList.status =='Success'}">
															<td ><h5><span class="label label-success"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:when test="${txnList.status =='Failed'}">
															<td><h5><span class="label label-danger"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:otherwise>
															<td><h5><span class="label label-warning"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:otherwise>
													</c:choose>
												</tr>	
											</c:forEach>
										</tbody>					
									</table>
<!-- 									<nav> -->
<!-- 										<ul class="pagination" id="paginationn"></ul> -->
<!-- 									</nav>	 -->
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	
		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">Msewa
						Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
<!-- 	data table -->



<script src="<c:url value='/resources/js/modernizr.js'/>"></script>	
  <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>

<script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>
<script
            src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>
	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	
		 <script
            src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
	function datatableEnable(){
		   $('#editedtable').DataTable({
			   "iDisplayLength": 20,
		    "bPaginate" : true,
		    "bFilter" : false,
		    "bInfo" : false,
		    "bSortable" : true,
		    "order" : [],
		    columnDefs : [ {
		     orderable : true,
		     targets : [ 0 ]
		    }, {
		     orderable : false,
		     targets : [ 2 ]
		    }, {
		     orderable : false,
		     targets : [ 3 ]
		    }, {
		     orderable : false,
		     targets : [ 4 ]
		    }, {
		     orderable : false,
		     targets : [ 6 ]
		    } ],
		    dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		   });
		  }

function validate(){
	
	var service = $('#serviceId').val();
	if(service == null || service == ''){
		$('#userMsg').hide();
		$('#serviceMsg').html('<p style="color:red;">Please select service</p>');
		$('#serviceMsg').show();
		return false;
	} if(user!='' &&  service!=''){
		return true;
	}else {
		return false;
	}
		}
		
		
function populateDropDownList(){
	 var serviceList='${jsonClientService}';
	 var serviceId='${serviceId}';
	 var jsonObj = $.parseJSON( serviceList);
	 var i=1;
	 var j=0;
	 for (var key in jsonObj) {
		  if (jsonObj.hasOwnProperty(key)) {
	             var div_data="<option value="+jsonObj[key].serviceId+">"+jsonObj[key].name+" ("+jsonObj[key].operatorName+")"+"</option>";
	             if(serviceId == jsonObj[key].serviceId){
	            	 console.log(jsonObj[key].serviceName);
	            	 j=i;
	             }
	             console.log(div_data);
	            $(div_data).appendTo('#serviceId'); 
		  }
		  i++;
		}
	 $('#serviceId option')[j].selected = true;
}
		

	</script>
	
		<script type="text/javascript">
$(function() {
	var startDate='${startDate}';
	var endDate='${endDate}';
	console.log("start:"+startDate);
 var start = moment(startDate);
    var end = moment(endDate);
    console.log("startDate:"+start+":"+end);
    function cb(start, end) {
    	console.log(start+":"+end);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#daterange').val(start + ' - ' + end)
        console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>


 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 
	
</body>

</html>
