<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
    <%
	   response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	   response.setDateHeader("Expires", 0);
	   response.setHeader("Pragma", "no-cache");
	%>
<head>
        <meta charset="utf-8" />
        <title>Admin | Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		    <jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Dashboard</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="card-group">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Card Issued</h4>
                                            <h5>${totalCards}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Total Active Users</h4>
                                            <h5>${totalActiveUser}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Total Physical Cards</h4>
                                            <h5>${totalPhysicalCards}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Total Credit</h4>
                                            <h5><i class="fa fa-inr"></i>&nbsp;${totalCredit}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block align-items-center">
                                        <a href="#"><i></i></a>
                                        <div class="m-l-15 m-t-10">
                                            <h4 class="font-medium m-b-0">Total Prefund Balance</h4>
                                            <h5><i class="fa fa-inr"></i>&nbsp;${totalPrefundAmount}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">
                        	<div class="col-4">
								<div class="card">
                            		<div class="card-body">
										 <div id="container"></div>
                            		</div>
                            	</div>                        		
                        	</div>
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">
                                         <div id="container2"></div>
                                    </div>
                                </div>                              
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                         <div id="container1"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <br>
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    &copy; <script>document.write(new Date().getFullYear());</script> Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/waypoints/lib/jquery.waypoints.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/counterup/jquery.counterup.min.js"></script>

        <!-- Chart JS -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/chart.js/chart.bundle.js"></script> --%>

        <!-- init dashboard -->
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/assets/pages/jquery.dashboard.init.js"></script> --%>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/highcharts.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/exporting.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/export-data.js"></script>
		
		<!-- piechart -->
		<script>
	// Build the chart
	Highcharts.chart('container', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie'
	    },
	    exporting: {
	    	enabled: false
	    },
	    credits: {
	    	enabled: false
	    },
	    title: {
	        text: 'Card Analytics'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: true
	        }
	    },
	    series: [{
	        name: 'Brands',
	        colorByPoint: true,
	        data: [{
	            name: 'Virtual Cards',
	            y: ${virCount},
	            sliced: true,
	            selected: true
	        }, {
	            name: 'Physical Cards',
	            y: ${phycount}
	        }, {
	            name: 'Physical Card Requests',
	            y: ${pendingReq}
	        }]
	    }]
	});
</script>

<!-- linechart -->
  <script>
    $(document).ready(function() {
		 getAjaxData();
		
     

	 });
	</script>
<!-- 	 <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> --> 
<script>
function  getAjaxData(){

$.ajax({
	type:"GET",
	url:"${pageContext.request.contextPath}/Admin/GetLineGraphData",
	dataType:"json",
success:function(data){

        Highcharts.chart('container1', {
            chart: {
                zoomType: 'x'
            },
            exporting: {
            	enabled: false
            },
            title: {
                text: 'Transaction rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            credits: {
            	enabled: false
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Amount'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Amount to Time',
                data: data
            }]
        });
    }
});
}
</script>

<script>
    
var chart = Highcharts.chart('container2', {

    chart: {
        type: 'column'
    },

    exporting: {
        enabled: false
    },

    credits: {
        enabled: false
    },

    title: {
        text: 'UPI and PG Transactions Bar chart'
    },

    legend: {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal'
    },

    xAxis: {
        categories: ${monthsList},
        labels: {
            x: -10
        }
    },

    yAxis: {
        allowDecimals: true,
        title: {
            text: 'Amount'
        }
    },

    series: [{
        name: 'UPI Transaction',
        data: ${monthsAmountUPI}
    }, {
        name: 'Razorpay Transaction',
        data: ${amountListRazorPay}
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    layout: 'horizontal'
                },
                yAxis: {
                    labels: {
                        align: 'left',
                        x: 0,
                        y: -5
                    },
                    title: {
                        text: null
                    }
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                }
            }
        }]
    }
});
</script>


    </body>

</html>