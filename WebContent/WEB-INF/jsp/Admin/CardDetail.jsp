
<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Card Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<script>var contextPath = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 70px; /* Location of the box */
    
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    padding-left: 60px;
    
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

</style>
    </head>

<!-- oncontextmenu="return false" -->
    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Card Details</h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="card-wrap">
                                                    <div class="card-body">
                                                        <div class="card1">
                                                         <c:choose>
                                                                 <c:when test="${cardstatus == 'false'}">
                                                            <center><img src="${pageContext.request.contextPath}/resources/images/card.png" class="img-responsive" style="width: 90%;"></center>
                                                           </c:when>
                                                           <c:otherwise>
                                                           <center><img src="${pageContext.request.contextPath}/resources/images/Bcard.png" class="img-responsive" style="width: 90%;"></center>
                                                           </c:otherwise>
                                                           </c:choose>
                                                            <div class="card-details custm_pos text-deco1">
                                                                <div class="card_num fs-20">${cardDetail.walletNumber}</div>
                                                                <div class="exp_dt">expiration<br>${cardDetail.expiryDate}</div>
                                                                <div class="cvv">cvv<br>${cardDetail.cvv}</div>
                                                                <div class="card_holdr fs-20">${cardDetail.holderName}</div>
                                                            </div>
                                                        </div>
                                                        <c:choose>
                                                        <c:when test="${cardBlockInfo == true}" >
                                                        <div class="card-info">
                                                            <div class="row">
                                                                 <c:choose>
                                                                 <c:when test="${cardstatus == 'false'}">
                                                                <div class="col">
                                                                    <center><button class="btn btn-danger" data-toggle="modal" data-target="#myModal" type="button" id="myBlock">Block</button></center>
                                                                </div>
                                                                </c:when>
                                                                <c:otherwise>
                                                                <div class="col">
                                                                    <center><button class="btn btn-success" id="myUnblockbutton"  onclick="unblk()"  type="button">Unblock</button></center>
                                                                </div>
                                                                </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                        </div>
                                                        </c:when>
                                                        <c:otherwise>
                                                        </c:otherwise>
                                                        </c:choose>
                                                        <div>
                                                        <span><b>Available Balance:</b>  ${Ubalance}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                        </div>
                                        
                                        <!-- The Modal -->
                                        
                                        <div class="col-md-7">
                                            <table id="one_card" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                               <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Amount</th>
                                                <th>TransactionType</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${transactions}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.transactionType}" default="" escapeXml="true" /></td>
										<td><c:out value="${card.description}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.date}" default="" escapeXml="true" />
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>
							</c:forEach>
                                    </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                     2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        
        <!-- Modal starts here -->
        <div class="modal fade" id="myModal" style="z-index: 9999;">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content" style="padding: 0; padding-left: 0; width: auto;">
		
		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Card Block</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		
		      <!-- Modal body -->
		      <div class="modal-body">
		        <div class="blck_resnm">
		        	<form>
		        		<div class="form-group">
		        			<label for="option">Reason</label>
							  <select class="form-control" id="blockType" name="blockType">
							    <option value="suspend">Suspend</option>
							    <option value="Lost">Lost</option>
							    <option value="Stolen">Stolen</option>
							    <option value="Damaged">Damaged</option>
							  </select>
		        		</div>
		        		<center><button class="btn btn-primary" type="button" id="blockCard">Submit</button></center>
		        	</form>
		        </div>
		      </div>
		
		    </div>
		  </div>
		</div>



        <!-- jQuery  -->
        
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
<%--         <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
       

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
         <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>
        
        <script>
	        $(document).ready(function() {
	            $('#one_card').DataTable({
	            	responsive: true,
	            	paging: false,
	            	info: false,
	            	searching: false
	            });
	        } );
        </script>

        <!-- Daterange picker -->
        <script type="text/javascript">
            $(function() {

                var start = moment().subtract(29, 'days');
                var end = moment();

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                
            });
            </script>
<script>
var cardId="${cardDetail.cardId}";
var cont_path="${pageContext.request.contextPath}";
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBlock");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

var blockBtn=document.getElementById('blockType').value;

console.log(blockBtn);

var onBlock=document.getElementById('blockCard');

// When the user clicks the button, open the modal 
 btn.onclick = function() {
    modal.style.display = "block";
} 

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    onBlock.onclick=function(){
    	onBlock.disabled='disabled';
    	console.log(blockBtn);
    	console.log("card id "+cardId);
    	$.ajax({
    	    type:"POST",
    	    contentType : "application/json",
    	    url: cont_path+"/Admin/BlockCard",
    	    dataType : 'json',
			data : JSON.stringify({
				"cardId" : "" + cardId + "",
				"requestType" : ""+blockBtn+""
			}),
			success : function(response) {
    	         /*  your code here */
    	        if (response.code.includes("S00")) {
					swal("Success!!", response.message, "success");
					var timeout = setTimeout(function() {
						window.location.reload();
					}, 2000);
				}
    	        else{
    	        	swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
    	        }
    	    }
    	   
    	});
    }
}
</script>

<script type="text/javascript">

function unblk(){
	$.ajax({
	    type:"POST",
	    contentType : "application/json",
	    url: cont_path+"/Admin/UnblockCard",
	    dataType : 'json',
		data : JSON.stringify({
			"cardId" : "" + cardId + "",
			"requestType" : "Active"
		}),
		success : function(response) {
	         /*  your code here */
	        if (response.code.includes("S00")) {
	        	$('#myModal').modal('hide');
				swal("Success!!", response.message, "success");
				window.location.reload();
				}
	        
	        else{
	        	$('#myModal').modal('hide');
	        	swal({
					  type: 'error',
					  title: 'Sorry!!',
					  text: response.message
					});
	        }
	    }
	   
	});
	
}

</script>

  <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 


    </body>

</html>