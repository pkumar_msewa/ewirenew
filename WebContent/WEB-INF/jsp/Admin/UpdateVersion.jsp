<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | Update Version </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
 
 <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<script>
var contextPath="${pageContext.request.contextPath}";
</script>

    </head>


    <body oncontextmenu="return false">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
          
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Versions</h4>

                                    <div class="clearfix"></div>
                                    <span id="updateError"></span>
                                    <span style="color: green; margin-left: 32%;">${versionMessage}</span>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-12">
                            	<div class="mb-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVersion" data-backdrop="static">Add Version</button>
                                </div>
                                <div class="card-box table-responsive">
                                    <!-- <h4 class="m-t-0 header-title"><b>Default Example</b></h4>
                                    <p class="text-muted font-14 m-b-30">
                                        DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                                    </p> -->
                                    

                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Name</th>
                                                <th>Created Date</th>
                                                <th>Updated Date</th>
                                                <th>Version</th>
                                                <th>Status</th>
                                                 <th>Action</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                        <c:forEach items="${versions}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
								
									<td> <c:out value="${card.name}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.created}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.updated}" default="" escapeXml="true" /></td>
										 <td><c:out value="${card.version}" default="" escapeXml="true" /></td>
										<td> <c:out value="${card.apiStatus}" default="" escapeXml="true" /></td>
<%-- 										<td> <button onclick="UpdateValue('${card.apiStatus}')">Update</button></td> --%>
								<c:choose>
								<c:when test="${card.apiStatus == 'Active'}">
								<td><label class="switch"> <input type="checkbox" checked="checked"  onchange="UpdateValue('${card.apiStatus}','${card.id}')"> <span class="slider round"></span></label></td>
								</c:when>
								<c:otherwise>
								<td><label class="switch"> <input type="checkbox"  onchange="UpdateValue('${card.apiStatus}','${card.id}')"> <span class="slider round"></span></label></td>
								</c:otherwise>
								</c:choose>
								</tr>		 
							</c:forEach>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
            
            <div class="modal fade" id="addVersion">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Version</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="${pageContext.request.contextPath}/Admin/AddVersion" method="post" id="addVrsnId">
        <div class="form-group">
        	<input type="text" name="version" id="version" class="form-control" placeholder="Enter Version"/>
        	<p id="error_version" style="color: red"></p>
        </div>
        <div class="form-group">
        	<select name="name" id="name" class="form-control">
        		<option selected="selected" value="#">Version Type</option>
		        <option value="Android">Android</option>
		        <option value="IOS">IOS</option>
	        </select>
	        <p id="error_name" style="color: red"></p>
        </div>
        <center><button type="button" class="btn btn-primary" id="addVrsn" onclick="validateAddVersion()">Add</button></center>
        </form>
      </div>

    </div>
  </div>
</div>


        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <%-- <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/jszip.min.js"></script> --%>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/pdfmake.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/vfs_fonts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.html5.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.print.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable({
                    responsive: true,
                    paging: false,
                    searching: false
                    
                });

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>
        
        <script>
        
        function UpdateValue(status,id){
        	var status=status;
        	var id=id;
        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url : contextPath+"/Admin/UpdateVersionStatus",
    			dataType : 'json',
    			data : JSON.stringify({
    				"status" :"" + status + "",
    				"id" :"" + id + "",
    			}),
    			success : function(response) {
    				console.log("response:: "+response.code);
    				console.log("response:: "+response.message);
    				if (response.code.includes("S00")) {
    					window.location.href="${pageContext.request.contextPath}/Admin/UpdateVersion";
    					}else{
    						$("#updateError").html(response.message);
    				}			
    			}
    		});
    	}
    	
        </script>
        <script>
        
        function validateAddVersion(){
        	var valid =true;
        	var name=$("#name").val();
        	var version=$("#version").val();
        	
        	if(name=='#'){
        		valid=false;
        		$("#error_name").html("Please select the version name")
        	}
        	
        	if(version.length <=0){
        		valid=false;
        		$("#error_version").html("Please enter the version")
        	}
        	
        	if (valid == true){
        		$("#addVrsnId").submit();
        	}
        }
        </script>
        
       <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

    </body>

</html>