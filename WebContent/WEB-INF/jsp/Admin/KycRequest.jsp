<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Admin | KYC Request</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />
	
		<script>var contextPath = "${pageContext.request.contextPath}";</script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		 <script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.twbsPagination.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
		<style>
		modal-body .form-horizontal .col-sm-2,
.modal-body .form-horizontal .col-sm-10 {
    width: 100%
}

.modal-body .form-horizontal .control-label {
    text-align: left;
}
.modal-body .form-horizontal .col-sm-offset-2 {
    margin-left: 15px;
}
		</style>
	<script>
	var context_path="${pageContext.request.contextPath}";
	var kycRequestURL = context_path+"/Admin/UpdatedKycList";
	var pageSize = '50';
	var spinnerUrl = "<img src='"+context_path+"/resources/spinner2.gif' style='height:150px;width:150px;'>"
	</script>





 <script type="text/javascript">
    $(document).ready(function() {
	$("#spinner").html(spinnerUrl);	 
    	getAjaxData();
	 });

	 function fetchMe(value){
		var paging=value;
		var daterangeVal=$('#reportrange').val();
		console.log(paging);
	$.ajax({
	type:"POST",
	url:kycRequestURL,
	data:{page:paging,size:pageSize,
		Daterange:daterangeVal},
	dataType:"json",
	success:function(data){
		$("#spinner").html('');	 
	var trHTML='';
		if(trHTML==''){
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){
			if(!data.jsonArray[i].addUserBoolean) {
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+ '</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

			} else {
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+ '</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

			}
		
		});
		
		$('#ashok').append(trHTML);

		}
		else
		{
			$(data.jsonArray).each(function(i,item){
				if(!data.jsonArray[i].addUserBoolean) {
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+ '</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

				} else {
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+ '</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

				}
				
			
			});
			$('#ashok').append(trHTML);
		}
		 
	}
	});
}
	 
	 function  getAjaxData(){
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:kycRequestURL,
				data:{
					page:paging,
					size:pageSize
					},
			dataType:"json",
			success:function(data){
				 console.log("Response get");
					$("#spinner").html('');	 

				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						if(!data.jsonArray[i].addUserBoolean) {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

						} else {
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" data-book-id="A_'+i+'" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

						}
					});
					$('#ashok').append(trHTML);
					}
					else
					{
						$(data.jsonArray).each(function(i,item){
							if(!data.jsonArray[i].addUserBoolean) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+ '<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="A_'+i+' data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

							}
							});
						$('#ashok').append(trHTML);
					}
					   $(function () {
							console.log("inside funt...and total pages:"+data.totalPages);
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 7,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
				         }
						 });
						}); 
						}
					 });
				 }
</script>	
    </head>
    <body oncontextmenu="return false">

        <div id="wrapper">
           <jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">KYC Request</h4>
									<div id="messId" style="font-size: medium; text-align: center; color: #4e77cc">${mess}</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
							  <div class="row">
							<div class="col-12">
								<div class="card-box">
									<div class="row">
										<div class="col-md-8 col-sm-8 col-xs-8">
											<form action="">
												<div class="form-row">
													<div class="col-sm-4">
														<div id="" class="pull-left" style="cursor: pointer;">
															<label class="sr-only" for="filterBy">Filter By:</label>
														   	<input id="reportrange" name="toDate" class="form-control" readonly="readonly"/>
														</div>
													</div>
													<div class="col-sm-3">
														<button class="btn btn-primary" type="button" onclick="fetchlist()">Filter</button>
													</div>
												</div>
											</form>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="row">
			                                    <div class="form-group">
				                                    <input id="username" name="userName" class="form-control" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="Enter Contact No"/>
				                                </div>
				                                <div class="form-group">
				                                    <button class="btn btn-primary" onclick="fetchSincgleCard()" type="button" >Search</button>
			                                   	</div>	
			                                   	<span id="err" style="color: red;position: fixed;margin-top: 27px;"></span>
			                                </div>
										</div>
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>User Details</th>
                                                <th>Document Details</th>
                                                <th>Image1</th>
                                                <th>Image2</th>
                                                <th id="acceptreasonid"></th>
                                                <th id="rejectreasonid"></th>
                                            </tr>
                                        </thead>
                                       <tbody id="ashok">
                                  	  </tbody>
                                    </table>
                                    <center><span id="spinner"> </span></center>
                                     <nav style="float: right;">
										<ul class="pagination" id="paginationn"></ul>
									</nav>
                                </div>
                            </div>
                        </div> <!-- end row -->
                        
                        

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2017 © Copyright EWire.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

<!-- MODAL -->

<!-- <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            Modal Header
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Residential Address
                </h4>
            </div>
            
            Modal Body
            <div class="modal-body">
                
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="address1">Address1</label>
                    <div class="col-sm-10">
                    
                        <input type="text" class="form-control" 
                        id="address1" placeholder="Address1" name="address1"/>
                    </div>
                  </div>
                  <input type="hidden" id="hiddenId" name="hiddenId" value=""/>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="address2">Address2</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="address2" placeholder="Address2" name="address2"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="city">City</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="city" placeholder="City" name="city"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="state">State</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="state" placeholder="State" name="state"/>
                    </div>
                  </div>
                                    <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="country">Country</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="country" placeholder="Country" name="Country"/>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="pincode">Pincode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="pincode" placeholder="Pincode" name="pincode"/>
                    </div>
                  </div>
                 
                </form>
                
            </div>
            
            Modal Footer
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary" id="action">
                    Update Address
                </button>
            </div>
        </div>
    </div>
</div> -->

 <div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>

<!-- MODAL BEGINS -->

<div class="modal fade" id="myModalHorizontalKyc" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Rejection Reason
                </h4>
            </div>
            
          <form class="form-horizontal" role="form">
            <div class="modal-body">
                
       		 <div class="row pl-3 pr-3">
 					<div class="col-md-6">
 						<div class="form-group">
 							<div class="form-check">
							  	<input class="form-check-input known_radio" type="radio" name="exampleRadios" id="matching1" value="Address not Matching" required>
							  	<label class="form-check-label" for="exampleRadios1">
							    	Address not Matching
							  	</label>
							</div>							
 						</div>					
 					</div>
 					<div class="col-md-6">
 						<div class="form-group">
 							<div class="form-check">
							  	<input class="form-check-input known_radio" type="radio" name="exampleRadios" id="dob2" value="DOB Not Matching" required>
							  	<label class="form-check-label" for="exampleRadios1">
							    	DOB Not Matching
							  	</label>
							</div>							
 						</div>							
 					</div>
 					<div class="col-md-6">
 						<div class="form-group">
 							<div class="form-check">
							  	<input class="form-check-input known_radio" type="radio" name="exampleRadios" id="image3" value="Image Not Proper" required>
							  	<label class="form-check-label" for="exampleRadios1">
							    	Image Not Proper
							  	</label>
							</div>							
 						</div>							
 					</div>
 					<div class="col-md-6">
 						<div class="form-group">
 							<div class="form-check">
							  	<input class="form-check-input other_radio" type="radio" name="exampleRadios" id="other4" value="Other" required>
							  	<label class="form-check-label" for="exampleRadios1">
							    	Other
							  	</label>
							</div>							
 						</div>							
 					</div> 
 					<div class="col-md-12 reason_txt" style="display: none;">
 						<div class="form-group">
 							<textarea rows="2" class="form-control " name="address1" id="address1" placeholder="Enter Reason.." required></textarea>						
 							<span class="error" style="color: red;" id="reasonErrorradio"></span>
 						</div>							
 					</div>  
 					  <span class="error" style="color: red;" id="radioError"></span> 	
                </div> 
                <input type="hidden" id="vaVal" name="vaVal" />
                <input type="hidden" id="bidVal" name="bidVal"/>    
                              
            </div>            
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>
                <button type="button" class="btn btn-primary" onclick="rejection()">
                    Add Reason
                </button>
            </div>
             </form> 
        </div>
    </div>
</div>


<!-- MODAL ENDS -->

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });
                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
                
                var timeout = setTimeout(function(){
                	$("#messId").html("");
                }, 3000);
            } );

        </script>

    </body>
    
    <script>
    	$(document).ready(function() {
    		$('.other_radio').click(function() {
        		if ($(this).is(':checked')) {
        			$('.reason_txt').show();
				} 
        	});
    		$('.known_radio').click(function() {
        		if ($(this).is(':checked')) {
        			$('.reason_txt').hide();
				} 
        	});
    	});
    </script>

     <script>
        $('input[id="selectall"]').on('change', function(e){
        	   if(e.target.checked){
        	     $('#myModal').modal();
        	     $("#AppB").show();
      		   $("#RejB").show();
        	   }else{
        		   $("#AppB").hide();
          		   $("#RejB").hide();
        	   }
        	});
        </script>
        
        <!--  <script>
       function showHide(e){
    	   if(e.target.checked){
    		   $("#AppB").show();
    		   $("#RejB").show();
      	   }else{
      		 $("#AppB").hide();
  		   $("#RejB").hide();
      	   }
       }
        </script> -->



<script type="text/javascript">
$('#myModalHorizontal').on('show.bs.modal', function(e) {
	console.log("i am here");
    var bookId = $(e.relatedTarget).data('book-id');
    console.log("this is the book Id:"+bookId);
    $(e.currentTarget).find('input[name="hiddenId"]').val(bookId);
});
$("#action").click(function(){
	$("action").prop('disabled', true);
	var hidd=$("#hiddenId").val();
	console.log(hidd);
	var userId=$('#'+hidd).val();
	console.log(userId);
	//var address1=$('#address1').val();
	//var address2=$('#address2').val();
	//var city=$('#city').val();
	//var state=$('#state').val();
	//var pincode=$('#pincode').val();
	//var country=$('#country').val();
	var res=hidd.split("_");
	//var app=approve(userId,res[1],address1,address2,city,state,pincode,country);
	var app = approve();
	if(app==true){
		console.log("here i am seee...");
		//$('#myModalHorizontal').modal('hide');
	}
});

</script>

<script type="text/javascript">
     function approve(va,bId){
    	 console.log(va+" : "+bId);
    	 var bookId = $(e.relatedTarget).data('book-id');
    	 console.log("this is the book Id:"+bookId);
    	 var date=$("#reportrange").val();
    	 var buttonId=bId;
    	 var action="Success";
    		$.ajax({
    			type : "POST",
    			url : context_path+"/Admin/updateKycRequest",
    			dataType : 'json',
    			data :{
    				action :action,
    				id :va    				
    			},
    			success : function(response) {
    				 $("#statusUpdt").html(response.code);
    				 console.log(response.code)
    				 if(response.code == 'S00'){
    					 $('#A_'+buttonId).html('Approved');
    					 $('#R_'+buttonId).html('Rejected');
    					 $('#A_'+buttonId).removeClass('btn-danger');
    					 $('#A_'+buttonId).addClass('btn-disabled');
    					 $('#R_'+buttonId).removeClass('btn-danger');
    					 $('#R_'+buttonId).addClass('btn-disabled');
    					 $('#A_'+buttonId).prop('disabled', true);
    					 $('#R_'+buttonId).prop('disabled', true);
    					 $('#myModalHorizontal').modal('hide');
    					 $("action").prop('disabled', false);
    					// $(e.currentTarget).find('input[name="hiddenId"]').val(bookId);

    					//return true;
    				 window.location="${pageContext.request.contextPath}/Admin/UpdatedKycList";
    				 }
    			},
    		});
    	 
     }
    </script>



    <script type="text/javascript">
     function approve(va,bId,address1,address2,city,state,pincode,country){
    	 var contact=va;
    	 var size=pageSize;
    	 var date=$("#reportrange").val();
    	 var buttonId=bId;
    	 var action="Success";
    		$.ajax({
    			type : "POST",
    			url : context_path+"/Admin/updateKycRequest",
    			dataType : 'json',
    			data :{
    				action :action,
    				id :va,
    				address1:address1,
    				address2:address2,
    				city:city,
    				state:state,
    				pincode:pincode,
    				country:country
    			},
    			success : function(response) {
    				 $("#statusUpdt").html(response.code);
    				 console.log(response.code)
    				 if(response.code == 'S00'){
    					 $('#A_'+buttonId).html('Approved');
    					 $('#R_'+buttonId).html('Rejected');
    					 $('#A_'+buttonId).removeClass('btn-danger');
    					 $('#A_'+buttonId).addClass('btn-disabled');
    					 $('#R_'+buttonId).removeClass('btn-danger');
    					 $('#R_'+buttonId).addClass('btn-disabled');
    					 $('#A_'+buttonId).prop('disabled', true);
    					 $('#R_'+buttonId).prop('disabled', true);
    					$('#myModalHorizontal').modal('hide');
    					$("action").prop('disabled', false);


    					 return true;
//     				 window.location.href="${pageContext.request.contextPath}/Admin/KycRequest";
    				 }
    			},
    		});
    	 
     }
    </script>
    
    
    
     <script type="text/javascript">
     function reject(va,bId) {
    	 
    	 $("#reasonErrorradio").html("");
    	 $("#radioError").html("");  
    	 console.log("va "+va);
    	 console.log("bid:: "+bId);
    	 $('#vaVal').val(va);
    	 $('#bidVal').val(bId);
    	 $("#myModalHorizontalKyc").modal("show"); 
    	 
     }
     
     
     
     function rejection() {    	
    	 $("#reasonErrorradio").html("");
    	 $("#radioError").html("");  
    	 var valid = true;    	 
    	 var buttonId = $("#bidVal").val();
    	 var va = $("#vaVal").val();
    	 
    	 
    	 console.log("buttonId:: "+buttonId);
    	 console.log("va:: "+va);
    	 var rejectReason = $('input[name=name_of_your_radiobutton]:checked').val();
    	 if($("input[name=exampleRadios]").is(':checked')) {
    		    var card_type = $('input[name=exampleRadios]:checked').val();
    		    if(card_type == 'Other') {
    		    	var rejectReason = $("#address1").val(); 
    		    	if(rejectReason.length <= 0) {      		    		
   		    			$("#reasonErrorradio").html("Please enter Reason");
    		    		valid = false;
   		    	 	} else {
   		    	 		var rejectReason = $("#address1").val(); 
   		    	 		console.log("rejection reason:: "+rejectReason);
   		    	 	}
    		    } else {
    		    	var rejectReason = $('input[name=exampleRadios]:checked').val();
    		    	console.log("rejection reason:: "+rejectReason);
    		    }
    	} else {
    		$("#radioError").html("please choose an option");    		
    		valid = false;
    	 }
    	
    	 console.log("rejectReason:: "+rejectReason);
    	 var action="Failure";
    	// var buttonId=bId;
    	 
    	 console.log("reason:: "+rejectReason);
    	 console.log("va:: "+va);
    	 console.log("bis:: "+buttonId);
    	 if(valid) {    		 
    		$.ajax({
    			type : "POST",
    			url : context_path+"/Admin/updateKycRequest",
    			dataType : 'json',
    			data :{
    				action :action,
    				id :va,
    				rejectionReason : rejectReason
    			},
    			success : function(response) {
    				 $("#myModalHorizontalKyc").modal("hide"); 
    				 $("#statusUpdt").html(response.message);
    				 if(response.code == 'S00'){
    					 $('.reason_txt').hide();
    					 $('#A_'+buttonId).html('Approved');
    					 $('#R_'+buttonId).html('Rejected');
    					 
    					 $('#A_'+buttonId).removeClass('btn-danger');
    					 $('#A_'+buttonId).addClass('btn-disabled');
    					 $('#R_'+buttonId).removeClass('btn-danger');
    					 $('#R_'+buttonId).addClass('btn-disabled');
    					 $('#A_'+buttonId).prop('disabled', true);
    					 $('#R_'+buttonId).prop('disabled', true);
    					 $("#reasonErrorradio").html("");
    			    	 $("#radioError").html("");  
    					 $("#myModalHorizontal").modal("hide");
    					 $("#common_success_true").modal("show");
    					 $("#common_success_msg").html("KYC Rejected");
    					 var timeout = setTimeout(function() {
	     					 window.location="${pageContext.request.contextPath}/Admin/UpdatedKycList";
    					 }, 2000);
    				 }
    			},
    		});
    	 }
    	 
     }
    </script>
    
    <script>
		function fetchlist(){
			var date=$("#reportrange").val();
			 var paging='0';
			 var size='';
			 console.log("under ready...");
			 $.ajax({
					type:"POST",
					url:kycRequestURL,
					data:{
						page:paging,
						size:pageSize,
						Daterange:date
						},
				dataType:"json",
				success:function(data){
					 console.log("Response get");
					 $("#reportrange").html(data.date);
					 
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(!data.jsonArray[i].addUserBoolean) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+');" value="">Reject</button></td></tr>';

							}
						});

						$('#ashok').append(trHTML);
						
						}
						else
						{
							$(data.jsonArray).each(function(i,item){
								if(!data.jsonArray[i].addUserBoolean) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

								}
							});
									
							$('#ashok').append(trHTML);
							
						}
						
						  $(function () {
								console.log("inside funt...and total pages:"+data.totalPages);
								
							 $('#paginationn').twbsPagination({
								 totalPages: data.totalPages,
								 visiblePages: 7,
					         onPageClick: function (event, page) {
					        	 fetchMe(page-1);
							
					         }
							 });
							}); 
							}
						 });

			
		}
    </script>
    
    
    <script>
 function fetchSincgleCard(){
	var username=$("#username").val();
	var valid=true;
		if(username == ''){
			valid=false;
			$("#err").html("Please enter the username")
		}
		
		else if(username.length !=10){
			valid=false;
			$("#err").html("Please enter the valid username")
		}
		
		if(valid == true){

			 var paging='0';
			 var size='';
			 console.log("under ready...");
			 $.ajax({
					type:"POST",
					url:"${pageContext.request.contextPath}/Admin/SingleKycRequest",
					data:{
						username:username
						},
				dataType:"json",
				success:function(data){
					 console.log("Response get");
					 $('#paginationn').twbsPagination({
						 totalPages: data.totalPages,
						 visiblePages: 1,
					 });
					var trHTML='';
						if(trHTML==''){
						$(".testingg").empty();
						$(data.jsonArray).each(function(i,item){
							if(!data.jsonArray[i].addUserBoolean) {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

							} else {
								trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

							}
						});


						$('#ashok').append(trHTML);
						
						}
						else
						{
							$(data.jsonArray).each(function(i,item){
								if(!data.jsonArray[i].addUserBoolean) {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'</tr>';

								} else {
									trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>Name:' + data.jsonArray[i].firstName + '<br>Mobile No:'+ data.jsonArray[i].contactNo+'</a><br>Email id:'+ data.jsonArray[i].email+'<br>DOB:'+ data.jsonArray[i].dob+'<br>Account Type:'+ data.jsonArray[i].accountType+'</td>'+'<td>Id Type:'+ data.jsonArray[i].idType+'<br> Id Number:'+ data.jsonArray[i].idNumber+'<br> Address1:'+data.jsonArray[i].address1+'<br> Address2:'+data.jsonArray[i].address2+'<br> City:'+data.jsonArray[i].city+'<br>state:'+data.jsonArray[i].state+'<br>pincode:'+data.jsonArray[i].pinCode+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<a href='+data.jsonArray[i].image2+' target="_blank">'+'<img border="0" src='+data.jsonArray[i].image2+' class="img-responsive" style="height: 100px">'+'</a>'+'</td>'+'<td>'+'<button type="button" class="btn btn-sm btn-danger" id="A_'+i+'" data-book-id="A_'+i+'" onclick="approve('+data.jsonArray[i].userId+','+i+')" value="'+data.jsonArray[i].userId+'">Approve</button></td>'+'<td>'+'<button type="submit" class="btn btn-sm btn-danger" id="R_'+i+'" onclick="reject('+data.jsonArray[i].userId+','+i+')" value="">Reject</button></td></tr>';

								}
							});

							$('#ashok').append(trHTML);
							
						}
						 
								console.log("inside funt...and total pages:"+data.totalPages);
						}
				});
		}
 }
 </script>
 
 <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>
		<script>
		
		</script>
 
 
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>

    <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

</html>