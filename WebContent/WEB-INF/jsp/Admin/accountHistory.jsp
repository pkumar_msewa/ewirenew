<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	
	<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	
	<!-- Daterange picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.css" />
	
</head>

<body oncontextmenu="return false">

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-body" ><div class="panel-heading">
							<h3 class="panel-title">Deposit Balance</h3></div>
							 <form id="formId" action="<c:url value="/Admin/updateBalance"/>" method="post" onsubmit="return validate();" >
							 <input type="hidden" name="id" value="${userId}">
							<div class="row">
								<div class="col-md-3" >
								<label>Country*</label> <select id="accountId" name="accountNumber" class="form-control" onchange="getAccountDetail(this.value);">
<!-- 													<option value="" hidden selected >Select Account</option> -->
												<c:if
													test="${requestScope.accountDetails !=null && !empty accountDetails}">
													<c:forEach var="accountDetails"
														items="${requestScope.accountDetails}">
														<option value="${accountDetails.accountNumber}"
															${fn:contains(accountNo,accountDetails.accountNumber) ? 'selected="selected"' : ''}>
															<c:out value="${accountDetails.country}" /></option>
													</c:forEach>
												</c:if>
											</select>
											<p id="userMsg"></p>
								</div>
								<div class="col-md-3" class="form-control">
									<label>Current Balance*</label> <input type="text"
										style="cursor: not-allowed" name="currentBalance"
										class="form-control" id="cbId" value="" placeholder="Balance"
										readonly required>
								</div>
								<!-- <div class="col-md-3">
								<label>Country*</label> <input type="text"
										style="cursor: not-allowed" name="currentBalance"
										class="form-control" id="countryId" value="" placeholder="Country"
										readonly required>
								</div> -->
								<div class="col-md-3">
								<label>Deposit Amount*</label> <input type="text"
										class="form-control" name="amount" id="depoId" value="" placeholder="Deposit Amount" maxlength="7"
										  onkeypress="return isNumberKey(event)"><p id="depoError"></p>
								</div>
									<div class="col-md-3">
										<button type="submit" class="btn btn-primary" title="Save"
											style="margin-top: 25px;">Save</button>
									</div>
								</div>
							</form>
							</div></div>
							
							
<!-- 							MODEL WINDOW -->
							
					
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Account History</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable" class="table table-striped table-bordered date_sorted">
									<thead>
										<tr >
										    <th>S.NO</th>
											<th>Transaction ID</th>
											<th>Transaction Date</th>
											<th>Description</th>
<!-- 											<th>Retrieval Ref No</th> -->
											<th>Amount</th>
											<th>Status</th>
										</tr>	</thead>
									 	<tbody>
										<c:forEach items="${accountHistory}" var="txnList" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><c:out value="${txnList.transactionRefNo}"/></td>
											<td><c:out value="${txnList.created}"/></td>
										    <td><c:out value="${txnList.description	}"/></td>
<%-- 											<td><c:out value="${txnList.retrivalReferenceNo	}"/></td> --%>
											<td><c:out value="${txnList.amount	}"/></td>
													<c:choose>
														<c:when test="${txnList.status =='Success'}">
															<td ><h5><span class="label label-success"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:when test="${txnList.status =='Failed'}">
															<td><h5><span class="label label-danger"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:otherwise>
															<td><h5><span class="label label-warning"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:otherwise>
													</c:choose>
												</tr>	
											</c:forEach>
										</tbody>					
									</table>
<!-- 									<nav> -->
<!-- 										<ul class="pagination" id="paginationn"></ul> -->
<!-- 									</nav>	 -->
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
 <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>

	<script>
		$(document).ready(function() {
			 $('#editedtable').DataTable({
				   "iDisplayLength": 20,
			    "bPaginate" : true,
			    "bFilter" : false,
			    "bInfo" : false,
			    "bSortable" : true,
			    "order" : [],
			    columnDefs : [ {
			     orderable : true,
			     targets : [ 0 ]
			    }, {
			     orderable : true,
			     targets : [ 1 ]
			    }, {
			     orderable : true,
			     targets : [ 2 ]
			    }, {
			     orderable : true,
			     targets : [ 3 ]
			    }, {
			     orderable : true,
			     targets : [ 4 ]
			    } ],
			    dom : "Bfrtip",
				buttons : [ {
					extend : "copy",
					className : "btn-sm"
				}, {
					extend : "csv",
					className : "btn-sm"
				}, {
					extend : "excel",
					className : "btn-sm"
				}, {
					extend : "pdf",
					className : "btn-sm"
				}, {
					extend : "print",
					className : "btn-sm"
				} ],
				responsive : !0
			   });
			 
			
			 var accBal='${accountNo}';
			 console.log(accBal)
			 if(!(accBal!=null && accBal!='') ){
				 accBal=$("#accountId option:first").val();
			 }
			 getAccountDetail(accBal)
			 console.log(accBal);
		});
	</script>
	
	<script>
	function getAccountDetail(value){
		var stringJson='${accountJson}';
		var jsonObj=JSON.parse(stringJson);
		for(var i=0; i<jsonObj.length; i++){
			if(value==jsonObj[i].accountNumber){
				console.log(jsonObj[i]);
				$('#cbId').val(jsonObj[i].strBalance);
				$('#countryId').val(jsonObj[i].country);
			}
		}
		console.log("value:"+value+",JsonObj"+jsonObj);
	}
	
	function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : evt.keyCode;
       if (charCode != 46 && charCode > 31 
         && (charCode < 48 || charCode > 57))
          return false;
       $('#errorMsg').hide();
       return true;
    }
	
	 function validate(){
  	   var val=$('#depoId').val();
  	   if(val !=null && val>0){
  		 if(val.length>7){
  	  		 $('#depoError').html('<p style="color:red;" id="depoError">Deposit amount should be smaller than 7 digits</p>');
  	  		 return false;
  	  	   }
  		   return true;
  	   }else{
  		  $('#depoError').html('<p style="color:red;" id="depoError">Enter deposit amount</p>');
  	   }
  	   return false;
     }
	
	</script>
	
 	<script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script>
	
		<script type="text/javascript">
// $(function() {
// 	var startDate='${startDate}';
// 	var endDate='${endDate}';
	
// 	console.log("start:"+startDate);
//  var start = moment(startDate);
//     var end = moment(endDate);
//     console.log("startDate:"+start+":"+end);
//     function cb(start, end) {
//     	console.log(start+":"+end);
//         $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//         $('#daterange').val(start + ' - ' + end)
//         console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//     }

//     $('#reportrange').daterangepicker({
//         startDate: start,
//         endDate: end,
//         ranges: {
//            'Today': [moment(), moment()],
//            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
//            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//            'This Month': [moment().startOf('month'), moment().endOf('month')],
//            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//         }
//     }, cb);

//     cb(start, end);
    
// });
</script>
	
</body>

</html>
