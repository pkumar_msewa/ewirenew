<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    
	<head>
        <meta charset="utf-8" />
        <title>Admin | User Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
       	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">

        <!-- App css -->
       	<link href="${pageContext.request.contextPath}/resources/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/assets/css/style.css" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Daterangepicker -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <!-- google fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
		<script type="text/javascript">var contextPath="${pageContext.request.contextPath}";</script>
		<script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.min.js"></script>
        <style>
            @font-face {
                font-family: "OCRAExtended";
                src: url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.eot");
                src:
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.woff") format("ttf"), 
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.ttf") format("woff"),
                url("${pageContext.request.contextPath}/resources/admin/assets/fonts/OCRAExtended.svg#filename") format("svg");
            }
            fieldset {
            	min-width: initial;
            	padding: 15px;
            	margin: initial;
            	border: 1px solid #e5e5e5;
            }

            fieldset legend {
            	width: auto;
            	font-size: 1.3rem;
            }

            .artboard {
			  	width: 140px;
			  	position: relative;
			  	margin: 10px 67px 0;
			}

			.switch {
			  	position: relative;
			  	display: inline-block;
			  	width: 95px;
    			height: 42px;
			  	margin: 0 auto;
			}
			.switch input {
			  	display: none;
			}

			.slider {
			  	position: absolute;
			  	cursor: pointer;
			  	top: 0;
			  	left: 0;
			  	right: 0;
			  	bottom: 0;
			  	background-color: white;
			  	transition: 0.4s;
			  	border-radius: 34px;
			  	box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.24);
			}
			.slider .btn {
			  	box-sizing: border-box;
			  	position: absolute;
			  	height: 36px;
			    width: 36px;
			    left: 6px;
			    bottom: 3px;
			  	background-color: #ffdde4;
			  	transition: 0.4s;
			  	border-radius: 50%;
			  	border: 2px solid #9293ee;
			}
			.slider .btn:after, .slider .btn:before {
			  	position: absolute;
			  	content: "";
			  	top: 21px;
			  	left: 8px;
			  	background: #9293ee;
			  	height: 7px;
			  	width: 7px;
			  	border-radius: 50%;
			}
			.slider .btn:before {
			  	top: 4px;
			}
			.slider .btn .mouth {
			  	position: absolute;
			  	top: 12px;
    			left: 19px;
			  	width: 7px;
			  	height: 7px;
			  	background: transparent;
			  	border-radius: 45%;
			  	transform: rotate(45deg);
			  	border-top: 2px solid #9293ee;
			  	border-right: 2px solid #9293ee;
			}
			.slider .btn img {
			  	opacity: 0;
			  	transition: all 0.5s;
			  	position: absolute;
			  	top: 1px;
			    left: 1px;
			    transform: rotate(-10deg);
			    width: 30px;
			    z-index: 999;
			}

			input:focus + .slider {
			  	box-shadow: 0 0 1px #2196F3;
			}
			input:checked + .slider > .btn {
			  	transform: translateX(48px) rotate(180deg);
			}
			input:checked + .slider img {
			 	opacity: 1;
			}
			.gender {
				margin: 0 78px;
				font-size: 14px;
			}
        </style>
        
        <script>
$(document).ready(function() {
$.ajax({
	type:"GET",
	url:"${pageContext.request.contextPath}/Admin/AdminAccess",
	
	success:function(response){
		console.log(response);
		console.log(response.details.addUser);
		console.log(!response.details.addUser);
		if(!(response.details.addUser)) {
			$("#addu").hide();
		//	$("#kycreq").hide();
			$("#addu").hide();			
			$("#blockunblock").hide();	
			$("#activeUserBlock").hide();
			$("#acceptreasonid").hide();
			$("#rejectreasonid").hide();
			$("#bulkuploadregid").hide();
			$("#bulksmsid").hide();
			$("#acceptgrouprequestid").hide();
			$("#rejectgrouprequestid").hide();
			$("#groupuserlistidassign").hide();
			$("#groupuserlistiddelete").hide();
			$("#agentidcustom").hide();
			$("#physicalcardrequestid").hide();
			$("#promoserviceid").hide();
			$("#impstransactionid").hide();
			$("#corporateagentid").hide();
			$("#adg").hide();
			$("#loadc").hide();
			$("#merchantagentid").hide();
			$("#bulkregid").hide();
			$("#bulksmsid").hide();
			$("#donateeid").hide();
			$("#userblocklistid").hide();
			$("#adminactivityid").hide();
			$("#noti").hide();
			$("#ver").hide();
			$("#kycuserlistid").hide();
		}
		if(!response.details.loadCard) {
			$("#loadc").hide();
		}
		if(!response.details.assignPhysicalCard) {
			$("#apc").hide();
		}
		if(!response.details.assignVirtualCard) {
			$("#avc").hide();
		}
		if(!response.details.addCorporate) {
			$("#aca").hide();
		}
		if(!response.details.addMerchant) {
			$("#amr").hide();
		}
		if(!response.details.addAgent) {
			$("#ada").hide();
		}
		if(!response.details.addDonation) {
			$("#addd").hide();
		}
		if(!response.details.addGroup) {
			$("#adg").hide();			
		}
		if(!response.details.sendNotification) {
			$("#noti").hide();
		}
		if(!response.details.androidVersion) {
			$("#ver").hide();
		}		
		if(!response.details.promoService) {
			$("#gpromo").hide();
			$("#cashb").hide();
		}
		if(!response.details.myUnblock) {
			$("#myUnblockbutton").hide();
		}
	}
});
});
</script>

    </head>

<!-- oncontextmenu="return false" -->
    <body >

        <!-- Begin page -->
        <div id="wrapper">
			<!-- Top Bar Start -->
           	<jsp:include page="/WEB-INF/jsp/Admin/Header.jsp" />
           	<!-- Top Bar End -->
			<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
            <!-- Left Sidebar End -->
			
			<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="row">
                                    	<div class="col-md-6">
	                                    	<h4 class="page-title float-left">User Details</h4>
	                                    </div>
	                                    <div class="col-md-6 text-right">
	                                    	<strong>Mobile Token: </strong><code>${userdetails.mobileToken}</code>
	                                    </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                        	<div class="col-12">
                        		<div class="usrOptions">
                        			<ul class="justify-content-center">
	                        			<li>
	                        				<a href="javascript:void(0);" class="active" id="usrBtn" title="User Info" data-toggle="tab"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/usr_info.png" class="">&nbsp;<span>User Info</span></a>
	                        			</li>
	                        			<li>
	                        				<a href="javascript:void(0);" class="" id="cardBtn" title="Card Info" data-toggle="tab"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/card_info.png" class="">&nbsp;<span>Card Info</span></a>
	                        			</li>
	                        		</ul>
                        		</div>
                        	</div>
                        </div>

                        <div class="row" id="UsrInfoWrap">
                        	<div class="col-12">
                        		<div class="card-box">
	                        		<div class="card-body">
	                        			<div class="row">
	                        				<div class="col-3">
				                        		<div class="imgDtls-wrp">
				                        			<div class="usrImg">
				                        				<img src="${pageContext.request.contextPath}/resources/admin/assets/images/avatar.png" class="img-fluid">
				                        			</div>
				                        			<c:choose>
				                        			<c:when test="${addUserBool eq true}">
				                        			<div class="artboard">
													  	<!-- <h1>GENDER</h1> -->
													  	<label class="switch">
													    	<input type="checkbox" onclick="check(this)" id="statusUpdt"/><span class="slider"><span class="btn"><span class="mouth"></span><img src="${pageContext.request.contextPath}/resources/admin/assets/images/block.svg"/></span></span>
													  	</label>
													</div>
													<strong class="gender" >UNBLOCK</strong>
													</c:when>
													<c:otherwise>
													</c:otherwise>
													</c:choose>
				                        		</div>
				                        	</div>
				                        	<div class="col-9">
				                        		<fieldset>
				                        			<legend>&nbsp;Basic Info&nbsp;</legend>
				                        			<div class="basicInfo">
					                        			<div class="row">
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Name:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.userDetail.firstName}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Mobile No.:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>+91 </span><span>${userdetails.userDetail.contactNo}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Email-Id:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.userDetail.email}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>DOB:</strong></p>
					                        						</div>
                                                                   <div class="col-8">					                        						
                                                                  <c:choose>
                                                                  <c:when test="${userdetails.userDetail.dateOfBirth!=null}">
                                                                  <p><span>${userdetails.userDetail.dateOfBirth}</span></p>
                                                                  </c:when>    
                                                                  <c:otherwise>
                                                                  <p><span>N/A</span></p>
                                                                  </c:otherwise>
                                                                  </c:choose>
                                                                  </div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Acc Type:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.accountDetail.accountType.code}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>DO-Regd:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.created}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>Status:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.mobileStatus}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>ID type:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.userDetail.idType}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        				<div class="col-6">
					                        					<div class="row">
					                        						<div class="col-4">
					                        							<p><strong>ID Number:</strong></p>
					                        						</div>
					                        						<div class="col-8">
					                        							<p><span>${userdetails.userDetail.idNo}</span></p>
					                        						</div>
					                        					</div>
					                        				</div>
					                        			</div>
					                        		
					                        		</div>
				                        		</fieldset>
				                        	</div>
	                        			</div> <!-- end row -->
	                        			<div class="row">
	                        				<div class="col-12">
	                        					<div class="text-right">
	                        						<small><a href="javascript:void(0);" id="showKyc">Show KYC Details</a></small>
	                        					</div>
	                        				</div>
	                        			</div>
	                        		</div>
	                        	</div>

	                        	<div class="collapse" id="collapseKyc">
								  	<div class="card card-body mb-3">
								  		<div class="row kycHeader">
								  			<button class="close" type="button" id="closeKyc"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/cancel.png"></button>
								  			<h4 class="kycTitle">KYC Details</h4>
								  		</div>
								    	<div class="row">
								    		<div class="col-md-4">
								    			<div class="col-12">
									    			<div class="docImg mb-2">
									    				<center>
									    					<img src="${kycDetails.idImage}" class="img-fluid">
									    				</center>
									    			</div>
									    		</div>
									    		<div class="col-12">
									    			<div class="docImg">
									    				<center>
									    					<img src="${kycDetails.idImage1}" class="img-fluid">
									    				</center>
									    			</div>
									    		</div>
								    		</div>
								    		<div class="col-md-6 offset-md-1">
								    			<div class="idWrp">
								    				<img src="${pageContext.request.contextPath}/resources/admin/assets/images/id.png" class="img-fluid">
								    				<div class="idDetails">
								    					<div class="idType mb-3">
								    						<h5>ID Type: </h5>
								    						<span>${kycDetails.idType}</span>
								    					</div>
								    					<div class="idNum">
								    						<h5>ID Number: </h5>
								    						<span>${kycDetails.idNumber}</span>
								    					</div>
								    				</div>
								    			</div>
								    		</div>
								    	</div>
								  	</div>
								</div>

	                        	<div class="card-box">
	                        		<div class="card-body">
	                        			<div class="row filter-row">
	                        				<div class="col-md-12 col-sm-12 col-xs-12">
												<form action="${pageContext.request.contextPath}/Admin/userDetails" method="post" onload="myFunction()">
													<div class="form-row">
														<div class="col-sm-3">
															<div id="" class="form-group pull-left" style="cursor: pointer;">
																<label class="sr-only" for="filterBy">Filter By:</label>
															   		<input id="reportrange" name="toDate" class="form-control"  value="${dat}" readonly="readonly" />
															</div>
														</div>
														<div class="col-sm-3">
															<c:choose>
														 <c:when test="${Lstatus == 'All'}">
														 <div class="form-group">
														 	<select name="status" class="form-control mr-sm-2">
							                                    <option id="stst" value="All">All</option>
							                                    <option value="Success">Success</option>
							                                    <option  value="Initiated">Initiated</option>
							                                    <option value="Failed">Failed</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:when test="${Lstatus == 'Initiated'}">
														 <div class="form-group">
														 	<select name="status" class="form-control mr-sm-2">
							                                     <option  value="Initiated">Initiated</option>
							                                     <option value="Success">Success</option>
							                                    <option value="Failed">Failed</option>
							                                    <option id="stst" value="All">All</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:when test="${Lstatus == 'Failed'}">
														 <div class="form-group">
														 	<select name="status" class="form-control mr-sm-2">
							                                     <option value="Failed">Failed</option>
							                                    <option value="Success">Success</option>
							                                    <option  value="Initiated">Initiated</option>
							                                    <option id="stst" value="All">All</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:otherwise>
														 <div class="form-group">
														 	<select name="status" class="form-control mr-sm-2">
							                                    <option value="Success">Success</option>
							                                     <option  value="Initiated">Initiated</option>
							                                    <option value="Failed">Failed</option>
							                                    <option value="All">All</option>
						                                    </select>
														 </div>
														 </c:otherwise>
														 </c:choose>
														</div>
														<div class="col-sm-3">
														
																<c:choose>
														 <c:when test="${Type == 'All'}">
														 <div class="form-group">
														 	<select name="serviceType" class="form-control mr-sm-2">
							                                    <option id="stst" value="All">All</option>
							                                    <option value="LMS">Load Money</option>
							                                    <option  value="UPS">UPI</option>
							                                    <option value="IMPS">IMPS</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:when test="${Type == 'UPI'}">
														 <div class="form-group">
														 	<select name="serviceType" class="form-control mr-sm-2">
							                                     <option  value="UPS">UPI</option>
							                                     <option value="LMS">Load Money</option>
							                                    <option value="IMPS">IMPS</option>
							                                    <option id="stst" value="All">All</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:when test="${Type == 'IMPS'}">
														 <div class="form-group">
														 	<select name="serviceType" class="form-control mr-sm-2">
							                                     <option value="IMPS">IMPS</option>
							                                    <option value="LMS">Load Money</option>
							                                    <option  value="UPS">UPI</option>
							                                    <option id="stst" value="All">All</option>
						                                    </select>
														 </div>
														 </c:when>
														 <c:otherwise>
														 <div class="form-group">
														 	<select name="service" class="form-control mr-sm-2">
							                                    <option value="LMS">Load Money</option>
							                                     <option  value="UPS">UPI</option>
							                                    <option value="IMPS">IMPS</option>
							                                    <option value="All">All</option>
						                                    </select>
														 </div>
														 </c:otherwise>
														 </c:choose>
														</div>
													<input type="hidden" name="contactNo" value="${userdetails.userDetail.contactNo}" id="number">
														<div class="col-sm-3">
															<button class="btn btn-primary" type="submit" id="k">Filter</button>
														</div>
													</div>
												</form>
											</div>
	                        			</div>
	                        			<div class="row">
	                        				<div class="col-12">
	                        					<div class="trxn_table">
	                        						
				                        <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                       <thead>
				                                            <tr>
				                                                <th>Sl No</th>
				                                                <th>Amount</th>
				                                                <th>Trxn_Type</th>
				                                                <th>TransactionRefNo</th>
				                                                <th>Date</th>
				                                                <th>RRN</th>
				                                                <th>AuthRefId</th>
				                                                <th>Status</th>
				                                            </tr>
				                                        </thead>
                                       <tbody id="kanchan">
                                        <c:forEach items="${transactions}" var="card"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
									 <c:choose>
                                     <c:when test="${card.transactionType==true}">
                                     <td> <c:out value="debit" default="" escapeXml="true" /></td>
                                     </c:when>    
                                     <c:otherwise>
                                     <td> <c:out value="credit" default="" escapeXml="true" /></td>
                                     </c:otherwise>
                                     </c:choose>
									<c:choose>
									<c:when test="${card.transactionRefNo!=null}">
									<td> <c:out value="${card.transactionRefNo}" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
									<td>N/A</td>
									</c:otherwise>
									</c:choose>
									<td> <c:out value="${card.created}" default="" escapeXml="true" /></td>
									<c:choose>
									<c:when test="${card.retrivalRefNo!=null}">
									<td> <c:out value="${card.retrivalRefNo}" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
									<td>N/A</td>
									</c:otherwise>
									</c:choose>
									<c:choose>
									<c:when test="${card.authRefNo!=null}">
									<td> <c:out value="${card.authRefNo}" default="" escapeXml="true" /></td>
									</c:when>
									<c:otherwise>
									<td>N/A</td>
									</c:otherwise>
									</c:choose>
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>		 
							</c:forEach>
                                    </tbody>
                                    </table>
	                        	     </div>
	                        				</div>
	                        			</div>
	                        		</div>
	                        	</div>
                        	</div>
                        </div>

                        <div class="row" id="CardInfoWrap">
                        	<div class="col-12">
                        		<div class="card">
                        			<div class="card-body">
                        				<div class="row">
                        					<div class="col-sm-3 pr-0">
										        <div class="brsw-desc left-brows">
										        	<div class="scrol-y">
										        		<!-- Loop starts here -->
										        		<a href="javascript:void(0);" class="active" id="Vtab">Virtual Card Details</a>
										        		<a href="javascript:void(0);" class="" id="Ptab">Physical Card Details</a>
										        		<!-- Loop ends here -->
										        	</div>
										        </div>
										    </div>
										    <div class="col-sm-9">
										        <div class="vSection">
										        	<div class="topSection mb-2">
											        	<fieldset>
												        	<legend>Virtual Card</legend>
												        	<div class="row">
													        	<div class="col-6">
													        		<div class="card-img">
													        			<img src="${pageContext.request.contextPath}/resources/assets-newui/img/card.png" class="img-fluid">
													        			<div class="card-dtls">
													        				<div class="cardCvv text-right">
													        					<span>
													        						<small>cvv</small>&nbsp;
													        						<span>${cardDetailsVir.cvv}</span>
													        					</span>
													        				</div>
													        				<div class="cardNum">
													        					<span>${cardDetailsVir.walletNumber}</span>
													        				</div>
													        				<div class="cardValid text-right">
													        					<span>
													        						<small>Valid Thru</small>&nbsp;
													        						<span>${cardDetailsVir.expiryDate}</span>
													        					</span>
													        				</div>
													        				<div class="cardNme">
													        					<span>${cardDetailsVir.holderName}</span>
													        				</div>
													        			</div>
													        		</div>
													        	</div>
													        	<div class="col-6">
													        		<%-- <div class="row">
													        			<div class="col-6">
													        				<h5>Current Balance:</h5>
													        			</div>
													        			<div class="col-6">
													        				<h5><span class="fa fa-inr"></span>&nbsp;${cardBalanceVir}</h5>
													        			</div>
													        		</div> --%>
													        		<div class="row">
													        			<div class="col-6 offset-md-6">
													        				<div class="text-center">
														        				<small><a href="javascript:void(0);" id="virtual_Trxn" >Show Virtual Card Trxns</a></small>
														        			</div>
													        			</div>
													        		</div>
													        	</div>
													        </div>
												        </fieldset>
											        </div>
											        <div class="btmSection" id="collapseTrxns">
											        	
											        	<div class="trxn_table">
											        		<div class="trxn_Header">
													  			<button class="close" type="button" id="closeVirtu"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/cancel.png"></button>
													  			<h4 class="trxnTitle">Transactions</h4>
													  		</div>
			                        						<table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						                                        <thead>
						                                            <tr>
						                                                <th>Sl No</th>
						                                                <th>Amount</th>
						                                                <th>Trxn_Type</th>
						                                                <th>Description</th>
						                                                <th>Date</th>
						                                                <th>Status</th>
						                                            </tr>
						                                        </thead>
						                                      
							                                         <tbody>
                                        <c:forEach items="${virtualCardTrans}" var="card"
									   varStatus="loopCounter">
								    <tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.indicator}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.message}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.date}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								
								</tr>		 
							</c:forEach>
                                                            </tbody>
						                                    </table>
						                                     <nav style="float: right;">
										                     <ul class="pagination" id="paginationn"></ul>
									                         </nav>
			                        					</div>
											        </div> <!-- bottom Section -->
										        </div>

										        <!-- --------------- Physical Card ------------------ -->
										        <div class="phySection">
										        	<div class="topSection mb-2">
											        	<fieldset>
												        	<legend>Physical Card</legend>
												        	<div class="row">
													        	<div class="col-6">
													        		<div class="card-img">
													        			<img src="${pageContext.request.contextPath}/resources/assets-newui/img/card.png" class="img-fluid">
													        			<div class="card-dtls">
													        				<div class="cardCvv text-right">
													        					<span>
													        						<small>cvv</small>&nbsp;
													        						<span>${physicalCard.cvv}</span>
													        					</span>
													        				</div>
													        				<div class="cardNum">
													        					<span>${physicalCard.walletNumber}</span>
													        				</div>
													        				<div class="cardValid text-right">
													        					<span>
													        						<small>Valid Thru</small>&nbsp;
													        						<span>${physicalCard.expiryDate}</span>
													        					</span>
													        				</div>
													        				<div class="cardNme">
													        					<span>${physicalCard.holderName}</span>
													        				</div>
													        			</div>
													        		</div>
													        	</div>
													        	<div class="col-6">
													        											        
                                                
                                                               
													        		<div class="row">
													        			<div class="col-6 offset-md-6 offset-sm-6">
													        				<div class="text-center">
														        				<small><a href="javascript:void(0);" id="physical_Trxn">Show Physical Card Trxns</a></small>
														        			</div>
													        			</div>
													        		</div>
													        	</div>
													        </div>
												        </fieldset>
											        </div>
											        <div class="btmSection" id="collapseTrxns1">
											        	
											        	<div class="trxn_table">
											        		<div class="trxn_Header">
													  			<button class="close" type="button" id="closePhy"><img src="${pageContext.request.contextPath}/resources/admin/assets/images/cancel.png"></button>
													  			<h4 class="trxnTitle">Transactions</h4>
													  		</div>
			                        						<table id="example1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						                                        <thead>
						                                            <tr>
						                                                <th>Sl No</th>
						                                                <th>Amount</th>
						                                                <th>Trxn_Type</th>
						                                                <th>Description</th>
						                                                <th>Date</th>
						                                                <th>Status</th>
						                                            </tr>
						                                        </thead>
						                                       	<tbody>
                                        <c:forEach items="${physicalCardTrans}" var="card"
									   varStatus="loopCounter">
								    <tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${card.amount}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.indicator}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.message}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.date}" default="" escapeXml="true" /></td>
									<td> <c:out value="${card.status}" default="" escapeXml="true" /></td>
								</tr>		 
							</c:forEach>
                                                            </tbody>
						                                    </table>
			                        					</div>
											        </div> <!-- bottom Section -->
										        </div>
										    </div>
                        				</div>
                        			</div>
                        		</div>
                        	</div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    <script type="text/javascript">var year = new Date(); document.write(year.getFullYear());</script> © EWire Pvt. Ltd.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
         <!-- Modal starts here -->
        <div class="modal fade" id="myModal" style="z-index: 9999;">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content" style="padding: 0; padding-left: 0; width: auto;">
		
		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title">Card Block</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		
		      <!-- Modal body -->
		      <div class="modal-body">
		        <div class="blck_resnm">
		        	<form>
		        		<div class="form-group">
		        			<label for="option">Reason</label>
							  <select class="form-control" id="request_Type">
							    <option value="suspend">Suspend</option>
							    <option value="Lost">Lost</option>
							    <option value="Stolen">Stolen</option>
							    <option value="Damaged">Damaged</option>
							  </select>
		        		</div>
		        		<center><button class="btn btn-primary" type="button" onclick="block()" id="blockCard">Submit</button></center>
		        	</form>
		        </div>
		      </div>
		
		    </div>
		  </div>
		</div>



        <!-- jQuery  -->
       
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/metisMenu.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.slimscroll.js"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

        <!-- Required datatable js -->
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        
        <!-- Buttons examples -->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/admin/assets/js/jquery.app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/all.js"></script>

        <script>
        	function check(obj){
			  if (obj.checked) {
			    $(".gender").html("BLOCK");
			    	 var va="${userdetails.userDetail.contactNo}";
			    	 var auth="ROLE_USER,ROLE_LOCKED";
			    		$.ajax({
			    			type : "POST",
			    			contentType : "application/json",
			    			url : "${pageContext.request.contextPath}/Admin/Status/block/unblock",
			    			dataType : 'json',
			    			data : JSON.stringify({
			    				"authority" :" "+auth+"",
			    				"userName" :""+va+""
			    			}),
			    			success : function(response) {
			    				 $("#statusUpdt").html(response.message);
			    			},
			    		});
			     
			  } else {
			    $(".gender").html("UNBLOCK");
			         var va="${userdetails.userDetail.contactNo}";
			    	 var auth="ROLE_USER,ROLE_AUTHENTICATED";
			    		$.ajax({
			    			type : "POST",
			    			contentType : "application/json",
			    			url : "${pageContext.request.contextPath}/Admin/Status/block/unblock",
			    			dataType : 'json',
			    			data : JSON.stringify({
			    				"authority" :" "+auth+"",
			    				"userName" :""+va+""
			    			}),
			    			success : function(response) {
			    				 $("#statusUpdt").html(response.message);
			    			},
			    		});
			  }
			}; 
        </script>

        <!-- datatablles -->
        <script type="text/javascript">
			$(document).ready(function() {
			    var table = $('#example').DataTable( {
			    	searching: false,
			        lengthChange: false,
			        buttons: [ 'csv', 'excel', 'pdf' ]
			    } );
			 
			    table.buttons().container()
			        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
			} );

        </script>

        <!-- datatablles -->
        <script type="text/javascript">
			$(document).ready(function() {
		        $('#pcard_tab').DataTable({
		          info: false,
		          searching: false,
		          responsive: true
		        });
		    });

		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		      $($.fn.dataTable.tables(true)).DataTable()
		        .columns.adjust()
		        .responsive.recalc();
		    }); 

        </script>

        <script>
		$(function() {
		
		    var start = moment().subtract(29, 'days');
		    var end = moment();
		
		    function cb(start, end) {
		        $('#reportrange').html(start.format('MM-dd-yyyy') + ' - ' + end.format('MM-dd-yyyy'));
		    }
		
		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        locale: {
		        	format: 'YYYY-MM-DD'
		        },
		        dateLimit: {
		            "days": 30
		        },
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, cb);
		
		    cb(start, end);
		    
		});
		</script>

		<!-- kyc collapse -->
		<script>
			$(document).ready(function() {
				$('#showKyc').click(function() {
					$("#collapseKyc").collapse('show');
				});
				$('#closeKyc').click(function() {
					$("#collapseKyc").collapse('hide');
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#virtual_Trxn').click(function() {
					$("#collapseTrxns").slideDown();
				});
				$('#closeVirtu').click(function() {
					$("#collapseTrxns").slideUp();
				});
				$('#physical_Trxn').click(function() {
					$("#collapseTrxns1").slideDown();
				});

				$('#closePhy').click(function() {
					$("#collapseTrxns1").slideUp();
				});
			});
		</script>

		<script>
			$('#cardBtn').click(function() {
				$('#CardInfoWrap').fadeIn();
				$('#UsrInfoWrap').fadeOut();
				$('#cardBtn').addClass('active');
				$('#usrBtn').removeClass('active');
			});
			$('#usrBtn').click(function() {
				$('#UsrInfoWrap').fadeIn();
				$('#CardInfoWrap').fadeOut();
				$('#usrBtn').addClass('active');
				$('#cardBtn').removeClass('active');
			});
		</script>

		<script>
			$('#Ptab').click(function() {
				$('.phySection').show();
				$('.vSection').hide();
				$('#Ptab').addClass('active');
				$('#Vtab').removeClass('active');
			});
			$('#Vtab').click(function() {
				$('.vSection').show();
				$('.phySection').hide();
				$('#Vtab').addClass('active');
				$('#Ptab').removeClass('active');
			});
		</script>
		
	<script type="text/javascript">
    $(document).ready(function(){
    	console.log('here...');
    	var auth="${userdetails.authority}";
    	console.log(auth);
            if (auth.trim()=="ROLE_USER,ROLE_LOCKED") {
            	console.log('true');  
            	$("#statusUpdt").attr('checked','checked')
            } 
        });
    
</script>

<script>
// When the user clicks anywhere outside of the modal, close it

    function block(){
    var cardId="${physicalCard.cardId}";
    var requestType=$("#request_Type").val();
    	$.ajax({
    	    type:"POST",
    	    contentType : "application/json",
    	    url: "${pageContext.request.contextPath}/Admin/BlockCard",
    	    dataType : 'json',
			data : JSON.stringify({
				"cardId" : "" + cardId + "",
				"requestType" : ""+requestType+""
			}),
			success : function(response) {
				$("#myModal").modal("hide");
    	        if (response.code.includes("S00")) {
					swal("Success!!", response.message, "success");}
    	        else{
    	        	swal({
						  type: 'error',
						  title: 'Sorry!!',
						  text: response.message
						});
    	        }
    	    }
    	});
    }
</script>

<script type="text/javascript">
function unblk(){
	var cardId="${physicalCard.cardId}";
	$.ajax({
	    type:"POST",
	    contentType : "application/json",
	    url: "${pageContext.request.contextPath}/Admin/UnblockCard",
	    dataType : 'json',
		data : JSON.stringify({
			"cardId" : "" + cardId + "",
			"requestType" : "Active"
		}),
		success : function(response) {
	        if (response.code.includes("S00")) {
	        	$('#myModal').modal('hide');
				swal("Success!!", response.message, "success");}
	        else{
	        	$('#myModal').modal('hide');
	        	swal({
					  type: 'error',
					  title: 'Sorry!!',
					  text: response.message
					});
	        }
	    }
	});
}
</script>
<script>
function myFunction(){
	var numberr= "${userdetails.userDetail.contactNo}";
	$("#k").click(function(){
		$("#number").html(numberr);
		});
	}

</script>

  <script>
document.onkeydown = function(e) {
	if(event.keyCode == 123) {
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
	return false;
	}
	if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
	return false;
	}
	}

</script> 

</body>
</html>