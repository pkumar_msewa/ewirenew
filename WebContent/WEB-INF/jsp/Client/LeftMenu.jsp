<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="<c:url value="/Client/Home"/>"><i
						class="fa fa-home"></i> <span>Dashboard</span></a></li>
				<li><a href="#subPages" data-toggle="collapse"
					class="collapsed"><i class="fa fa-credit-card-alt"></i> <span>Transactions</span>
						<i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subPages" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/Client/TransactionList"/>"> <span>Transaction List</span></a></li>
							<li><a href="<c:url value="/Client/CommissionList"/>"><span>Commission List</span></a></li>
						</ul>
					</div></li>
					<li><a href="#resourceTab" data-toggle="collapse"
					class="collapsed"><i class="fa fa-wrench"></i> <span>Resources</span>
						<i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="resourceTab" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/Client/clientServices"/>"> <span>Client Services</span></a></li>
						</ul>
					</div></li>
			</ul>
		</nav>
	</div>
</div>
