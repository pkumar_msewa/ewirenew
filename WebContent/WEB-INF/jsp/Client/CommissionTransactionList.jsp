<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> --%>
<html lang="en">
<head>
<title>Commission List</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/linearicons/style.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/chartist/css/chartist-custom.css"/>">
<!-- MAIN CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/main.css"/>">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/demo.css"/>">
<!-- GOOGLE FONTS -->

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
	rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76"
	href="<c:url value="/resources/assets/img/apple-icon.png"/>">
<link rel="icon" type="image/png" sizes="96x96"
	href="<c:url value="/resources/assets/img/favicon.png"/>">

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>


<!-- Daterange picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.css" />



<script type="text/javascript">
	function fetchMe(value, startDate, endDate) {
		var paging = value;
		console.log("inside function " + value);

		$
				.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/Client/CommissionList",
					data : {
						page : paging,
						size : '20',
						fromDate : startDate,
						toDate : endDate
					},
					dataType : "json",
					success : function(data) {
						var trHTML = '';
						if (trHTML == '') {

							$(".testingg").empty();
							$(data)
									.each(
											function(i, item) {
												trHTML += '<tr class="testingg"><td>'
														+ (i + 1)
														+ '</td><td id="transactionRefNo">'
														+ data[i].transactionRefNo
														+ '</td><td>'
														+ data[i].created
														+ '</td><td>'
														+ data[i].description
														+ '</td><td>'
														+ data[i].retrivalReferenceNo
														+ '</td><td>'
														+ data[i].amount
														+ '</td><td>'
												var status = data[i].status;
												if (status == 'Success') {
													//alert(status);
													trHTML += '<h5><span class="label label-success">'
												} else if (data[i].status == 'Failed') {
													alert(data[i].status);
													trHTML += ' <h5><span class="label label-danger">'
												} else {
													trHTML += '<h5><span class="label label-warning">'
												}
												trHTML += data[i].status
														+ '</h5></td></tr>';
											});
							$('#editedtable').append(trHTML);
						} else {
							$(data)
									.each(
											function(i, item) {
												trHTML += '<tr class="testingg"><td>'
														+ (i + 1)
														+ '</td><td id="transactionRefNo">'
														+ data[i].transactionRefNo
														+ '</td><td>'
														+ data[i].created
														+ '</td><td>'
														+ data[i].description
														+ '</td><td>'
														+ data[i].retrivalReferenceNo
														+ '</td><td>'
														+ data[i].amount
														+ '</td><td>'
														+ data[i].status
														+ '</td></tr>';
											});
							$('#editedtable').append(trHTML);
						}
					}
				});
	}
	$(document).ready(function() {
		populateDropDownList();
		getAllData();
	});

	function getAllData() {

		var startDate = '${startDate}';
		var endDate = '${endDate}';
		console.log("all Data" + startDate + ":" + endDate);
		var paging = '0';
		var size = '';
		console.log("under ready...");
		$
				.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/Client/CommissionList",
					data : {
						page : paging,
						size : '20',
						fromDate : startDate,
						toDate : endDate,
					},
					dataType : "json",
					success : function(data) {
						var total = 0;
						var trHTML = '';
						if (data != '') {
							if (trHTML == '') {
								$(".testingg").empty();
								$(data)
										.each(
												function(i, item) {
													console
															.log("Ref No "
																	+ data[i].transactionRefNo)
													total = data[i].totalPages;
													trHTML += '<tr class="testingg"><td>'
															+ (i + 1)
															+ '</td><td id="transactionRefNo">'
															+ data[i].transactionRefNo
															+ '</td><td>'
															+ data[i].created
															+ '</td><td>'
															+ data[i].description
															+ '</td><td>'
															+ data[i].retrivalReferenceNo
															+ '</td><td>'
															+ data[i].amount
															+ '</td><td>'
															+ data[i].status
															+ '</td></tr>';
												});
								$('#editedtable').append(trHTML);
							} else {
								$(data)
										.each(
												function(i, item) {
													trHTML += '<tr class="testingg"><td>'
															+ (i + 1)
															+ '</td><td id="transactionRefNo">'
															+ data[i].transactionRefNo
															+ '</td><td>'
															+ data[i].created
															+ '</td><td>'
															+ data[i].description
															+ '</td><td>'
															+ data[i].retrivalReferenceNo
															+ '</td><td>'
															+ data[i].amount
															+ '</td><td>'
															+ data[i].status
															+ '</td></tr>';
												});
								$('#editedtable').append(trHTML);
							}

							$(function() {
								console.log("inside funt..." + total);
								$('#paginationn').twbsPagination({
									totalPages : total,
									visiblePages : 10,
									onPageClick : function(event, page) {
										page = page - 1;
										console.log("Page size :: " + page);
										fetchMe(page, startDate, endDate);
									}
								});
							});

						} else {
							trHTML += '<tr class="odd"><td class="dataTables_empty" colspan="7" valign="top">No data available in table</td></tr>';
							$('#editedtable').append(trHTML);
						}
					}
				});
	}
</script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/datepicker.css"/>">
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script>
	$(function() {
		$("#toDate").datepicker({
			format : "yyyy-mm-dd"
		}).on('change', function() {
			$('.datepicker').hide();
		});
		$("#fromDate").datepicker({
			format : "yyyy-mm-dd"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});
</script>
<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Client/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Client/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">COMMISSION LIST</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
						<form id="formId"
							action="<c:url value="/Client/filteredCommissionList"/>"
							method="POST" onsubmit="return validate();">
							<div class="row">

								<div class="col-md-12">
									<h4
										style="position: absolute; font-size: 14px; font-weight: 600; margin-top: 2px;">Select
										Date Range*</h4>
									<div class="col-md-4 col-sm-4 col-xs-3" id="reportrange"
										style="background: #fff; cursor: pointer; border: 1px solid #ccc; margin-top: 26px; padding: 5px; border-radius: 4px;">
										<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
										<span></span> <b class="caret"></b>
									</div>
									<input type="hidden" id="daterange" name="daterange" value=""
										class="form-control" readonly />

									<div class="col-md-3 col-sm-3 col-xs-3">
										<label>Services*</label> <select id="serviceId" name="service"
											class="form-control">
											<option value="All">All Services</option>
										</select>
										<p id="serviceMsg"></p>
									</div>


									<button type="submit" class="btn btn-primary" title="Search"
										style="margin-top: 25px;">
										<span class="glyphicon glyphicon-filter"></span>
									</button>
									<!-- 					 onclick="getAllData();"  -->
								</div>

							</div>
						</form>
						<p></p>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable"
										class="table table-striped table-bordered date_sorted">
										<thead>
											<tr>
												<th>S.NO</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>Description</th>
												<th>Retrival Ref No</th>
												<th>Amount</th>
												<th>Status</th>
											</tr>
										</thead>
									</table>
									<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>


		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">Msewa
						Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<!-- 	data table -->



	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>
	<script
		src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>
	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		// 		var handleDataTableButtons = function() {
		//             console.log("inside datatable buttons");
		// 			"use strict";	
		// 			0 !== $("#editedtable").length
		// 					&& $("#editedtable").DataTable({
		// 						 "bPaginate" : true,
		// 				 		    "bFilter" : true,
		// 				 		    "bInfo" : true,
		// 				 		    "order" : [],
		// 				 		   columnDefs : [ {
		// 				 			     orderable : true,
		// 				 			     targets : [ 0 ]
		// 				 			    }, {
		// 				 			     orderable : true,
		// 				 			     targets : [ 2 ]
		// 				 			    }, {
		// 				 			     orderable : true,
		// 				 			     targets : [ 3 ]
		// 				 			    }, {
		// 				 			     orderable : true,
		// 				 			     targets : [ 4 ]
		// 				 			    }, {
		// 				 			     orderable : true,
		// 				 			     targets : [ 6 ]
		// 				 			    } ],
		// 						dom : "Bfrtip",
		// 						buttons : [ {
		// 							extend : "copy",
		// 							className : "btn-sm"
		// 						}, {
		// 							extend : "csv",
		// 							className : "btn-sm"
		// 						}, {
		// 							extend : "excel",
		// 							className : "btn-sm"
		// 						}, {
		// 							extend : "pdf",
		// 							className : "btn-sm"
		// 						}, {
		// 							extend : "print",
		// 							className : "btn-sm"
		// 						} ],
		// 						responsive : !0
		// 					})
		// 		}, TableManageButtons = function() {
		// 			"use strict";
		// 			return {
		// 				init : function() {
		// 					handleDataTableButtons()
		// 				}
		// 			}
		// 		}();
	</script>
	<script type="text/javascript">
		// 	function datatableEnable(){
		// 		   $('#editedtable').DataTable({
		// 			   "iDisplayLength": 10,
		// 		    "bPaginate" : false,
		// 		    "bFilter" : false,
		// 		    "bInfo" : false,
		// 		    "bSortable" : true,
		// 		    "order" : [],
		// 		    columnDefs : [ {
		// 		     orderable : true,
		// 		     targets : [ 0 ]
		// 		    }, {
		// 		     orderable : false,
		// 		     targets : [ 2 ]
		// 		    }, {
		// 		     orderable : false,
		// 		     targets : [ 3 ]
		// 		    }, {
		// 		     orderable : false,
		// 		     targets : [ 4 ]
		// 		    }, {
		// 		     orderable : false,
		// 		     targets : [ 6 ]
		// 		    } ],
		// 		    dom : "Bfrtip",
		// 			buttons : [ {
		// 				extend : "copy",
		// 				className : "btn-sm"
		// 			}, {
		// 				extend : "csv",
		// 				className : "btn-sm"
		// 			}, {
		// 				extend : "excel",
		// 				className : "btn-sm"
		// 			}, {
		// 				extend : "pdf",
		// 				className : "btn-sm"
		// 			}, {
		// 				extend : "print",
		// 				className : "btn-sm"
		// 			} ],
		// 			responsive : !0
		// 		   });
		// 		  }
		// 		$(document).ready(function() {
		// 			$('#datatable-keytable').DataTable({
		// 				keys : true
		// 			});
		// 			$('#datatable-responsive').DataTable();
		// 			$('#datatable-scroller').DataTable({
		// 				ajax : "js/datatables/json/scroller-demo.json",
		// 				deferRender : true,
		// 				scrollY : 380,
		// 				scrollCollapse : true,
		// 				scroller : true
		// 			});
		// 			var table = $('#datatable-fixed-header').DataTable({
		// 				fixedHeader : true
		// 			});
		// 		});
		// 		TableManageButtons.init();

		function validate() {
			return true;
			// 		 	var fDate = $('#fromDate').val();
			// 			var toDate = $('#toDate').val();
			// 			var user = $('#userId').val();
			// 			var service = $('#serviceId').val();
			// 			if (fDate == null || fDate == '') {
			// 				$('#fromDateMsg').html('<p style="color:red;">Please select from date</p>');
			// 				$('#fromDateMsg').show();
			// 				return false;
			// 			} if(toDate == null || toDate == ''){
			// 				$('#fromDateMsg').hide();
			// 				$('#toDateMsg').html('<p style="color:red;">Please select to date</p>');
			// 				$('#toDateMsg').show();
			// 				return false;
			// 			}if(service == null || service == ''){
			// 				$('#toDateMsg').hide();
			// 				$('#serviceMsg').html('<p style="color:red;">Please select service</p>');
			// 				$('#serviceMsg').show();
			// 				return false;
			// 			} if(fDate !='' &&  toDate !=''  && user!='' &&  service!=''){
			// 				return true;
			// 			}else {
			// 				return false;
			// 			}
		}
		
		
		  function populateDropDownList(){
				 var serviceList='${jsonClientService}';
				 var jsonObj = $.parseJSON( serviceList);
				 for (var key in jsonObj) {
					  if (jsonObj.hasOwnProperty(key)) {
				             var div_data="<option value="+jsonObj[key].serviceId+">"+jsonObj[key].name+" ("+jsonObj[key].operatorName+")"+"</option>";
				             console.log(div_data);
				            $(div_data).appendTo('#serviceId'); 
					  }
					}
			 }
		
	</script>

	<script type="text/javascript">
		$(function() {
			var startDate = '${startDate}';
			var endDate = '${endDate}';
			//var date1=new Date(startDate);
			//startDate = startDate +" 00:00:00";
			//var start=parseInt(startDate);
			console.log("start:" + startDate);
			var start = moment(startDate);
			var end = moment(endDate);
			console.log("startDate:" + start + ":" + end);
			function cb(start, end) {
				console.log(start + ":" + end);
				$('#reportrange span').html(
						start.format('MMMM D, YYYY') + ' - '
								+ end.format('MMMM D, YYYY'));
				$('#daterange').val(start + ' - ' + end)
				console.log(start.format('MMMM D, YYYY') + ' - '
						+ end.format('MMMM D, YYYY'));
			}

			$('#reportrange').daterangepicker(
					{
						startDate : start,
						endDate : end,
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						}
					}, cb);

			cb(start, end);

		});
	</script>

</body>

</html>
