<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> --%>
<html lang="en">
<head>
<title>Transaction List</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- VENDOR CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/linearicons/style.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/assets/vendor/chartist/css/chartist-custom.css"/>">
<!-- MAIN CSS -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/main.css"/>">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet"
	href="<c:url value="/resources/assets/css/demo.css"/>">
<!-- GOOGLE FONTS -->
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
	rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/resources/assets/img/apple-icon.png"/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value="/resources/assets/img/favicon.png"/>">
	
		
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>

<script type="text/javascript">
function fetchMe(value){
	var paging=value;
	console.log("inside function " + value);
		
	 $.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Client/TransactionList",
	data:{page:paging,size:'5'},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
			
			$(".testingg").empty();
			$(data).each(function(i,item){
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].dateOfTransaction + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td>' + data[i].status +'</td></tr>';});
			  $('#editedtable').append(trHTML);
			}
			else
			{
				$(data).each(function(i,item){
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].dateOfTransaction + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td><center><span class="label label-warning">'+ data[i].status +'</span></center></td><td>';});
				  $('#editedtable').append(trHTML);
			}
	}
	}); 
	}
	 $(document).ready(function() {
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Client/TransactionList",
				data:{page:paging,size:'5'},
			dataType:"json",
			success:function(data){
				var total = 0;
				
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data).each(function(i,item){
						console.log("Ref No " + data[i].transactionRefNo)
						total = data[i].totalPages;
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].dateOfTransaction + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td>' + data[i].status +'</td></tr>';});
					  $('#editedtable').append(trHTML);
					}
					else
					{
						$(data).each(function(i,item){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td id="transactionRefNo">' + data[i].transactionRefNo + '</td><td>' + data[i].dateOfTransaction + '</td><td>' + data[i].description + '</td><td>'+ data[i].retrivalReferenceNo +'</td><td>' + data[i].amount + '</td><td>' + data[i].status +'</td></tr>';});
						  $('#editedtable').append(trHTML);
					}
					
					  $(function () {
							console.log("inside funt...");
						 $('#paginationn').twbsPagination({
							 totalPages: total,
							 visiblePages: 10,
				         onPageClick: function (event, page) {
				        	 page = page - 1;
				        	 console.log("Page size :: " + page);
				        	fetchMe(page);
				         }
						 });
						}); 
					 
			}
		 });
	 });
</script>
	
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Client/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Client/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">TRANSACTION LIST</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable" class="table table-striped table-bordered">
										<tr id="xyz">
										    <th>S.NO</th>
											<th>Transaction ID</th>
											<th>Transaction Date</th>
											<th>Transaction Type</th>
											<th>Retrival Ref No</th>
											<th>Amount</th>
											<th>Status</th>
										</tr>						
									</table>
									<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>	
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">
					&copy; 2017 <a href="https://www.msewa.com" target="_blank">Msewa
						Software Solution Pvt. Ltd.</a>. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
</body>

</html>
