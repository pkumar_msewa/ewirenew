<!doctype html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/vendor/chartist/css/chartist-custom.css">
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
	<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/assets/img/favicon.png">
	
	<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	
	<!-- Daterange picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/jsdeliver/daterangepicker.css" />
	
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/jsp/Client/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Client/LeftMenu.jsp" />
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-body" ><div class="panel-heading">
							<h3 class="panel-title">DASHBOARD</h3></div>
							<div class="row">
								<div class="col-md-3" >
									<div class="metric" onmouseover="" style="cursor: pointer;" onclick="parent.location=('${pageContext.request.contextPath}/Client/TransactionList');">
										<span class="icon"><i class="fa fa-shopping-bag"></i></span>
										<p>
											<span class="number">${totalTxn}</span>
											<span class="title">Total Transaction</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric" style="cursor: pointer;" onclick="getAccountDetails(${clickable})">
										<p>
											<c:choose>
     											 <c:when test="${clickable!=true}">
     											 <span class="icon"><i class="fa fa-inr"></i></span>
     											 	<span class="number">${walletBal}</span>
     											 	<span class="title">Prefunded A/C Bal</span>
     											 </c:when>
											 	 <c:otherwise>
											 	  <span class="icon"><i class="fa fa-usd"></i></span>
											 		<span class="number"><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
											 		<span class="title">Accounts</span>
											 	 </c:otherwise>
											</c:choose>
										</p>
									</div>
								</div>
								
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-inr"></i></span>
										<p>
											<span class="number">${totalAmount}</span>
											<span class="title">Payable Amount</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric"  onmouseover="" style="cursor: pointer;" onclick="parent.location=('${pageContext.request.contextPath}/Client/CommissionList');">
										<span class="icon"><i class="fa fa-inr"></i></span>
										<p>
											<span class="number">${commBalance}</span>
											<span class="title">Commission Balance</span>
										</p>
									</div>
								</div>
								<!-- <div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number">35%</span>
											<span class="title">Total Commission</span>
										</p>
									</div>
								</div> -->
								</div>
							</div></div>
							
							
							<!-- 							MODEL WINDOW -->

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 4%;">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-md-12" >
												<div class="col-md-8"  class="form-control">
											<h4 id="headingId"></h4>
											</div>
												<div class="col-md-4"  class="form-control" align="right">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span><i title ="Close"class="fa fa-times" aria-hidden="true"   style="color:red"></i></span>
											</button>	</div></div>
										</div>
									<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
													<!-- TABLE HOVER -->
													<!-- page content -->
											<table id="modelTable"
												class="table table-striped table-bordered date_sorted">
												<thead>
													<tr>
														<th>S.NO</th>
														<th>Account Type</th>
														<th>Country</th>
														<th>Prefunded Account Balance</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${accountDetails}" var="accountDetails"
														varStatus="loopCount">
														<tr>
															<td>${loopCount.count}</td>
															<td><c:out value="${accountDetails.accountType}" /></td>
															<td><c:out value="${accountDetails.country}" /></td>
															<td><c:out value="${accountDetails.strBalance}" /></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
											</div>
									</div>
								</div>
								</div>
							</div>
<!-- 							MODEL WINDOW -->
							
							
							
							<%-- <div class="panel panel-headline">
							<div class="panel-heading">
							<h3 class="panel-title">CLIENT SERVICES</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
								<div class="row" id="table-wrapper" >
								 <div id="table-scroll">
								<table id="editedtable" class="table table-striped table-bordered date_sorted" >
									<thead>
										<tr >
										    <th>S.NO</th>
											<th>Name</th>
											<th>Code</th>
											<th>Amount Range</th>
											<th>Commission</th>
											<th>Commission Type</th>
											<th>Status</th>
										</tr>	</thead>
										<tbody>
										<c:forEach items="${clientServices}" var="clientServices" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><c:out value="${clientServices.name}"/></td>
											<td><c:out value="${clientServices.code}"/></td>
											<td><c:out value="${clientServices.minAmount}"/>&nbsp;-&nbsp;<c:out value="${clientServices.maxAmount}"/></td>
											<td><c:out value="${clientServices.commission}"/></td>
											<td><c:out value="${clientServices.fixedVal}"/></td>
											<td><c:out value="${clientServices.status}"/></td>
												</tr>	
											</c:forEach>
										</tbody>					
									</table>
								</div>
								</div>
						</div>
					</div> --%>
					
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">TRANSACTION LIST</h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
					  <form id="formId" action="<c:url value="/Client/filteredTransactionlist"/>" method="post" onsubmit="return validate();" >
					 <div class="row">
					  <div class="col-md-12">
					<h4 style="position:absolute;font-size: 14px;font-weight: 600; margin-top: 2px;">Select Date Range*</h4>
						 	<div class="col-md-4 col-sm-4 col-xs-3" id="reportrange" 
									style="background: #fff; cursor: pointer;  border: 1px solid #ccc;margin-top: 26px; padding: 5px; border-radius: 4px; ">
									<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
									<span></span> <b class="caret"></b>
								</div>
						  <input type="hidden" id="daterange" name="daterange" value="" class="form-control" readonly/>
						   <input type="hidden" id="dashboardId" name="dashboard" value=true class="form-control" readonly/>

									<div class="col-md-3 col-sm-3 col-xs-3">
										<label>Services*</label> <select id="serviceId" name="service"
											class="form-control">
											<option value="All">All Services</option>
										</select>
										<p id="serviceMsg"></p>
									</div>



									<button type="submit" class="btn btn-primary" title="Search" style="margin-top: 25px;"><span class="glyphicon glyphicon-filter"></span></button>
<!-- 					 onclick="getAllData();"  -->
</div></div>
					  </form><p></p>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable" class="table table-striped table-bordered date_sorted">
									<thead>
										<tr >
										    <th>S.NO</th>
											<th>Transaction ID</th>
											<th>Transaction Date</th>
											<th>Description</th>
											<th>Retrieval Ref No</th>
											<th>Amount</th>
											<th>Status</th>
										</tr>	</thead>
									 	<tbody>
										<c:forEach items="${transactionList}" var="txnList" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><c:out value="${txnList.transactionRefNo}"/></td>
											<td><c:out value="${txnList.created}"/></td>
										    <td><c:out value="${txnList.description	}"/></td>
											<td><c:out value="${txnList.retrivalReferenceNo	}"/></td>
											<td><c:out value="${txnList.amount	}"/></td>
													<c:choose>
														<c:when test="${txnList.status =='Success'}">
															<td ><h5><span class="label label-success"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:when test="${txnList.status =='Failed'}">
															<td><h5><span class="label label-danger"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:when>
														<c:otherwise>
															<td><h5><span class="label label-warning"><c:out
																		value="${txnList.status	}" /></span></h5></td>
														</c:otherwise>
													</c:choose>
												</tr>	
											</c:forEach>
										</tbody>					
									</table>
<!-- 									<nav> -->
<!-- 										<ul class="pagination" id="paginationn"></ul> -->
<!-- 									</nav>	 -->
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">
						&copy; 2017 <a href="https://www.msewa.com" target="_blank">MSewa
							Software Solution Pvt. Ltd.</a>. All Rights Reserved.
					</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/vendor/bootstrap/css/bootstrap.min.css"/>">
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/scripts/klorofil-common.js"></script>
	<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
 <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>

	<script>
		$(document).ready(function() {
			 $('#editedtable').DataTable({
				   "iDisplayLength": 20,
			    "bPaginate" : true,
			    "bFilter" : false,
			    "bInfo" : false,
			    "bSortable" : true,
			    "order" : [],
			    columnDefs : [ {
			     orderable : true,
			     targets : [ 0 ]
			    }, {
			     orderable : false,
			     targets : [ 2 ]
			    }, {
			     orderable : false,
			     targets : [ 3 ]
			    }, {
			     orderable : false,
			     targets : [ 4 ]
			    }, {
			     orderable : false,
			     targets : [ 6 ]
			    } ],
			    dom : "Bfrtip",
				buttons : [ {
					extend : "copy",
					className : "btn-sm"
				}, {
					extend : "csv",
					className : "btn-sm"
				}, {
					extend : "excel",
					className : "btn-sm"
				}, {
					extend : "pdf",
					className : "btn-sm"
				}, {
					extend : "print",
					className : "btn-sm"
				} ],
				responsive : !0
			   });
			
			populateDropDownList();
		});
	</script>
	
	<script>
	function getAccountDetails(clickable){
		console.log(clickable);
		if(clickable){
		 $('#myModal').modal('show'); 
		}
	}
	
	function populateDropDownList(){
		 var serviceList='${jsonClientService}';
		 var serviceId='${serviceId}';
		 var jsonObj = $.parseJSON( serviceList);
		 var i=1;
		 var j=0;
		 for (var key in jsonObj) {
			  if (jsonObj.hasOwnProperty(key)) {
		             var div_data="<option value="+jsonObj[key].serviceId+">"+jsonObj[key].name+" ("+jsonObj[key].operatorName+")"+"</option>";
		             if(serviceId == jsonObj[key].serviceId){
		            	 console.log(jsonObj[key].serviceName);
		            	 j=i;
		             }
		             console.log(div_data);
		            $(div_data).appendTo('#serviceId'); 
			  }
			  i++;
			}
		 $('#serviceId option')[j].selected = true;
	}
	
	</script>
	
		<script type="text/javascript">
$(function() {
	var startDate='${startDate}';
	var endDate='${endDate}';
	
	console.log("start:"+startDate);
 var start = moment(startDate);
    var end = moment(endDate);
    console.log("startDate:"+start+":"+end);
    function cb(start, end) {
    	console.log(start+":"+end);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#daterange').val(start + ' - ' + end)
        console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>
	
</body>

</html>
