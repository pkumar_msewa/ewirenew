<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Change Password</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.error{
		color: red;
		font-size: 12px;
	}
	.success{
		color: green;
		font-size: 12px;
	}
    </style>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-10">
                            <div class="page-title-box">
                                <div class="clearfix"></div>
                            </div>
                        </div>
					</div>
					<center><span id="errormsg" class="error">${error}</span></center>
					<center><span id="errormsg" class="success">${success}</span></center>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="panel panel-default">
								<div class="panel-body">
								<div class="col-md-6 col-md-offset-3">
									<form action="${pageContext.request.contextPath}/Agent/ChangePassword" method="post" id="formId">
									<fieldset>
                                                <legend>Change Password</legend>
                                                <div class="form-group">
						                            <label for="name">Current Password:</label>
						                            <input type="password" class="form-control" id="password" name="password" maxlength="6" placeholder="Enter Your Current Password" autocomplete="off">
						                            <span id="error_password" class="error"></span>
					                          	</div>
                                                <div class="form-group">
                            						<label for="name">New Password:</label>
						                            <input type="password" class="form-control" id="newPassword" name="newPassword" maxlength="6" placeholder="Enter Your New Password" autocomplete="off">
						                            <span id="error_newPassword" class="error"></span>
					                          	</div>
                                                <div class="form-group">
						                            <label for="email">Confirm Password:</label>
						                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" maxlength="6" placeholder="Enter Your Confirm Password" autocomplete="off" >
						                            <span id="error_confirmPassword" class="error"></span>
					                          	</div>
                                            </fieldset>
	                                        <div class="cta-actions text-center">
				                              	<button class="btn signLog_btn" type="button" onclick="validatePassword()" title="CREATE ACCOUNT" id="register">Change Password</button>
				                          	</div>
								</form>	
								</div>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script>
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}	
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		
		function validatePassword(){
	    	var valid = true;
	    	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
	    	var password = $('#password').val();
	    	var newPassword = $('#newPassword').val();
	    	var confirmPassword = $('#confirmPassword').val();
	    	
	    	if (password.length <= 0){
	    		valid = false;
	    		$('#error_password').html('Please enter your current password');
	    	}
	    	
	    	if (newPassword.length <= 0){
	    		valid = false;
	    		$('#error_newPassword').html('Please enter your new password');
	    	}  
	    	if (confirmPassword.length <= 0){
	    		valid = false;
	    		$('#error_confirmPassword').html('Please enter your confirm password');
	    	} else if (newPassword != confirmPassword) {
	    		valid = false;
	    		$('#error_confirmPassword').html('New password and confirm password does not match.');
	    	}
	    	
		    if(valid == true) {
		    	$("#formId").submit();
		    } 
	    
		    var timeout = setTimeout(function(){
		        $("#error_password").html("");
		    	$("#error_newPassword").html("");
		    	$("#error_confirmPassword").html("");
		    }, 3000);
	    }
	</script>
</body>
</html>