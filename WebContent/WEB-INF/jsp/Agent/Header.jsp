 <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href="javascript:void(0);" style="cursor: default; pointer-events: none;"><i class="ti-wallet"></i> <p>${agentBalance}</p></a></li>
                    <%-- <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light ti-settings" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                            </a>
                            <a href="${pageContext.request.contextPath}/Agent/Logout" class="dropdown-item notify-item">
                                    <i class="fi-power"></i> <span>Logout</span>
                                </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a href="${pageContext.request.contextPath}/Agent/Logout" class="dropdown-item notify-item">
                                    <i class="fi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li> --%>
                        <li><a href="${pageContext.request.contextPath}/Agent/Logout"><i class="ti-settings"></i>&nbsp;Logout</a></li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                        </li>
                    
                    
                    </ul>
                </div>
            </div>
        </nav>