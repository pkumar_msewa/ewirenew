<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agent Prefund</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/jquery-ui/jquery-ui.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/corporate/assets/jquery-ui/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    
    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.error{
    	color:red;
    	font-size: 11px;
    }
    
    .success{
    	color:green;
    	font-size: 11px;
    }
    </style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false" oncontextmenu="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-10">
                            <div class="page-title-box">
                                <!-- <h4 class="page-title float-left">Agent Prefund</h4> -->
                                <div class="clearfix"></div>
                            </div>
                        </div>
					</div>
					<center><strong id="errormsg" style="color:red;">${error}</strong></center>
					<center><strong id="errormsg" style="color:green; font-size:14px;">${success}</strong></center>
					
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<form action="${pageContext.request.contextPath}/Agent/AgentPrefund" method="post" id="formId">
										<fieldset>
						  	 				<legend>Agent Prefund</legend>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">Amount</label>
			                                        <input type="text" name="amount" placeholder="Enter Amount" id="amount" class="form-control" maxlength="8" onkeypress="return isNumberKey(event);">
			                                  		<span class="error" id="error_amount"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">From Bank</label>
			                                        <input type="text" name="fromBank" placeholder="Enter Bank Name" id="fromBank" class="form-control fromBank" maxlength="60" onkeypress="return isAlphKey(event);">
			                                  		<span class="error" id="error_fromBank"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">Account No</label>
			                                        <input type="text" name="accountNo" placeholder="Enter Account No" id="accountNo" class="form-control" maxlength="20" onkeypress="return isNumberKey(event);">
			                                  		<span class="error" id="error_accountNo"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">Reference No</label>
			                                        <input type="text" name="referenceNo" placeholder="Enter Reference No" id="referenceNo" class="form-control" maxlength="20" onkeypress="return isAlphNumberKey(event);">
			                                  		<span class="error" id="error_referenceNo"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">To Bank</label>
			                                        <input type="text" name="toBank" placeholder="Enter Bank Name " id="toBank" class="form-control toBank" maxlength="60" onkeypress="return isAlphKey(event);">
			                                  		<span class="error" id="error_toBank"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">Account No</label>
			                                        <input type="text" name="receiverAccountNo" placeholder="Enter Account No" id="receiverAccountNo" class="form-control" maxlength="20" onkeypress="return isNumberKey(event);">
			                                  		<span class="error" id="error_receiverAccountNo"></span>
			                                    </div>
		                                    </div>
		                                    <div class="col-sm-6">
		                                    	<div class="form-group">
			                                        <label for="mobile">Remarks</label>
			                                        <input type="text" name="remarks" placeholder="Enter Remarks" id="remarks" class="form-control" maxlength="50" onkeypress="return isAlphKey(event);">
			                                  		<span class="error" id="error_remarks"></span>
			                                    </div>
		                                    </div>
					  	 			</fieldset>
                                    <div class="cta-actions text-center">
	                              		<button class="btn signLog_btn" type="button" onclick="validateAgentPrefund()" id="register">SUBMIT</button>
	                          		</div>
								</form>						
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>Your request has been raised successfully.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	
	<script>
	  $(document).ready(function(){
		<c:set var="bankList" value="${banks}"/>
		var bankData= ${banks};
	    var availableTags = ${banks};
	    $( ".toBank" ).autocomplete({
	      source: availableTags
	    });
	    $( ".fromBank" ).autocomplete({
		      source: availableTags
	    });
	  });
	  </script>
	<script>
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}	
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		
		function validateAgentPrefund(){
			var valid = true;
			var amount = $('#amount').val();
			var fromBank = $('#fromBank').val();
			var accountNo = $('#accountNo').val();
			var referenceNo = $('#referenceNo').val();
			var toBank = $('#toBank').val();
			var receiverAccountNo = $('#receiverAccountNo').val();
			var remarks = $('#remarks').val();
			
			if (amount.length <= 0) {
				console.log("empty amount.");
				$("#error_amount").html("Please enter amount.");
				valid = false;
			} else if (amount <= 0.0) {
				console.log("more than 0.0");
				$("#error_amount").html("Amount should be more than 0");
				valid = false;
			} /* else if (!amount.match(pattern)) {
				console.log("error");
	    		$("#error_amount").html("Enter valid amount");
	    		valid = false;	
			} */
			
			if (fromBank.length <= 0) {
				$('#error_fromBank').html('Please enter from bank.');
				valid = false;
			}
			
			if (accountNo.length <= 0) {
				$('#error_accountNo').html('Please enter account no.');
				valid = false;
			} else if (accountNo.length < 11) {
				$('#error_accountNo').html('Please enter valid account no.');
				valid = false;
			}
			
			if (referenceNo.length <= 0) {
				$('#error_referenceNo').html('Please enter reference no.');
				valid = false;
			}
			
			if (toBank.length <= 0) {
				$('#error_toBank').html('Please enter to bank.');
				valid = false;
			}
			if (receiverAccountNo.length <= 0) {
				$('#error_receiverAccountNo').html('Please enter account no.');
				valid = false;
			} else if (receiverAccountNo.length < 11) {
				$('#error_receiverAccountNo').html('Please enter valid account no.');
				valid = false;
			}
			
			if(valid == true) {
		    	$("#formId").submit();
		    }
			
			var timeout = setTimeout(function(){
		    	$("#error_amount").html("");
		    	$("#error_fromBank").html("");
		    	$("#error_accountNo").html("");
		    	$("#error_referenceNo").html("");
		    	$("#error_toBank").html("");
		    	$("#error_receiverAccountNo").html("");
		    }, 3000);
	    }
	</script>
	
	
</body>
</html>