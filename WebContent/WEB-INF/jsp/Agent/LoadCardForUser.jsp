<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Load card | Agent</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
    fieldset {
	    padding: .35em 1.625em .75em;
	    margin: 0 2px;
	    border: 1px solid silver;
	    margin-bottom: 15px;
	}
	legend {
		width: auto;
	}
	.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
    </style>
</head>
<body oncopy="return false" oncut="return false" onpaste="return false">
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                    	<div class="col-12">
                             <div class="page-title-box">
                                 <h4 class="page-title float-left">Load Card</h4>
                                 <!-- <ol class="breadcrumb float-right">
                                     <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                     <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                     <li class="breadcrumb-item active">Datatable</li>
                                 </ol> -->
                                 <div class="clearfix"></div>
                                 <div style="color: green; text-align: center; ">${succsseMsg}</div>
                                 <div style="color: red; text-align: center; ">${errorMsg}</div>
                             </div>
                         </div>
                    </div>
                    <div class="row">
                    	<div class="col-12">
                    		<div class="panel panel-default">
                    			<div class="panel-body">
	                    			<form action="${pageContext.request.contextPath}/Agent/LoadCardForUser" id="formId"  method="post">
	                    				<div class="col-md-6 col-md-offset-3">
	                    					<fieldset>
	                 							<legend>User Details</legend>
	                 							<div class="form-group">
	                                             	<label for="mobile">Mobile</label>
	                                             	<input type="text" name="contactNo" id="contactNo" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="10">
	                                            	<span id="ferror" class="error" style="color: red" ></span>
	                                            </div>
	                                            <div class="form-group">
	                                               	<label for="amount">Amount</label>
	                                           		<input type="text" name="amount" id="amount" class="form-control"  onkeypress="return isNumberKey(event);" maxlength="5">
	                                           		<span id="amountError" class="error" style="color: red" ></span>
	                                            </div>
	                                            <div class="form-group">
	                                           		<label for="lname">Comment(optional)</label>
	                                               	<textarea rows="5" cols="46" maxlength="180" name="comment" id="comment" class="form-control"></textarea>
	                                               	<span id="lastError" class="error" style="color: red" ></span>
	                                           </div>
	                                           <!-- <div class="form-group">
	                                           		<span class="options">User Request</span>
	                                            <label class="tgl">
	                                                <input type="checkbox" id="option" name="transactionType" checked/>
	                                                <span class="tgl_body">
	                                                    <span class="tgl_switch"></span>
	                                                    <span class="tgl_track">
	                                                        <span class="tgl_bgd"></span>
	                                                        <span class="tgl_bgd tgl_bgd-negative"></span>
	                                                    </span>
	                                                </span>
	                                            </label>
	                                            Rounded switch
												<label class="switch">
												  	<input type="checkbox" id="check">
												  	<input type="hidden" id="isTransactionType" name="isTransactionType" value="false">
												  	<span class="slider round"></span>
												</label>
	                                        	<span class="options">Agent Request</span>
	                                    	</div>
	                                    	<div class="form-group" id="proxy" style="display:none;">
	                                  			<label for="proxy">Transaction Ref No</label>
	                                           	<input type="text" name="transactionrefNo" id="transactionrefNo" maxlength="16" min="16"  class="form-control"  onkeypress="return isNumberKey(event);">
	                                            <p id="proxyNumbererror" style="color: red" ></p>
	                                            <input type="hidden" id="isTransactionType" name="transactionType" value="false">
	                                    	</div> -->
	                                    	<center><button type="button" class="btn signLog_btn mt-4" onclick="validateform()">Load</button></center>
	                    				</fieldset>
	                    			</div>
	                    		</form>
                    		</div>
                    	</div>
                    </div>
                  </div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
		
		function isAlphNumberKey(evt){
		    var k = (evt.which) ? evt.which : evt.keyCode
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 190 || k == 188);
		}
		
		function isAlphKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 08 || charCode == 32)
		}
		
		$("#check").change(function() {
		    if(this.checked) {
		        $('#proxy').css('display','block');
		        $('#isTransactionType').val(true);
		    } else {
		    	$('#proxy').css('display','none');
		    	$('#isTransactionType').val(false);
			}
		});
		
		function validateform(){
	    	var valid = true;
	    	var checkBoxValue = $('#check').is(':checked');
	    	var contactNo  = $('#contactNo').val();
	    	var amount = $('#amount').val();
	    	var transactionrefNo = $('#transactionrefNo').val();
	    	var contactNoPattern = "[7-9]{1}[0-9]{9}";
	    	if(contactNo.length <=0 || contactNo.length !=10){
	    		$("#ferror").html("Please enter your 10 digit contact no");
	    		valid = false;
	    	} else if(!contactNo.match(contactNoPattern)) {
    			console.log("invalid contact no");
    			$("#ferror").html("Please enter valid contact number");
	    		valid = false; 
    		}
	    	
	    	
	    	
	    	if(amount.length<=0){
	    		$("#amountError").html("Please enter the amount.");
	    		valid = false; 
	    	} else if(amount==0){
	    		$("#amountError").html("Amount should be more than 0");
	    		valid = false; 
	    	}
	    	
	    	if(checkBoxValue) {
		    	if (transactionrefNo.length <= 0) {
		    		$('#proxyNumbererror').html("Please enter the reference no.");
		    		valid = false;
		    	}
		    }

		    if(valid == true) {
		    	$("#formId").submit();
		    } 
	    	
		    var timeout = setTimeout(function(){
		    	$("#ferror").html("");
		    	$("#amountError").html("");
		    	$('#proxyNumbererror').html('');
		    }, 4000);
	    }
	</script>
</body>
</html>