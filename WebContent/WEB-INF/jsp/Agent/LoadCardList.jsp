<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<%
	response.setHeader("Cache-Control","no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "no-cache");
%>
<head>
<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Load Card List | Agent</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.png">
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Paper Dashboard core CSS    -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/demo.css" rel="stylesheet" />
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/resources/corporate/assets/css/themify-icons.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/admin/assets/js/modernizr.min.js"></script>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="LeftMenu.jsp"></jsp:include>
		<div class="main-panel">
			<jsp:include page="Header.jsp"></jsp:include>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
			            <div class="col-12">
			              <div class="page-title-box">
			                <h4 class="page-title float-left">Load Card List</h4>
			                <div class="clearfix"></div>
			              </div>
			            </div>
					</div>
			          <div class="row mb-15">
			            <div class="col-12">
			              <form class="form-inline" action="<c:url value="/Agent/LoadCardList"/>" method="post">
			                <div class="form-group">
			                  <label>Select Date</label>
			                  <input type="text" name="datetimes" id="reportrange" class="form-control" />
			                  <input type="hidden" id="daterange" name="daterange" value="" class="form-control" readonly />
			                </div>
			                <button class="btn btn-primary" type="submit">Filter</button>
			              </form>
			            </div>
			          </div>
					<div class="row">
						<div class="col-12">
							<div class="card-box table-responsive">
								<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
									<thead>
                                       <tr>
                                           <th>Sl No</th>
                                           <th>User Data</th>
                                           <th>Contact No</th>
                                           <th>Email</th>
                                           <th>Amount</th>
                                           <th>Date</th>
                                           <th>Physical Card</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                   		<c:forEach items="${cardList}" var="cardList" varStatus="loopCounter">
                                   			<tr>
                                   				<td>${loopCounter.count}</td>
                                   				<td>${cardList.name}<br>
                                   					User type: 
                                   					<%-- <c:choose>
                                   						<c:when test="${cardList.KYC eq true}">
                                   							KYC
                                   						</c:when>
                                   						<c:otherwise>NON KYC</c:otherwise>
                                   					</c:choose> --%>
                                   					${cardList.userType}
                                   				</td>
                                   				<td>${cardList.contactNo}</td>
                                   				<td>${cardList.email}</td>
                                   				<td>${cardList.amount}</td>
                                   				<td><fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${cardList.date}" /></td>
                                   				<td>${cardList.physicalCard}</td>
                                   			</tr>
                                   		</c:forEach>
                                   </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <script src="${pageContext.request.contextPath}/resources/corporate/assets/js/bootstrap.min.js" type="text/javascript"></script>
	  <!-- DateRange picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    
    <!-- Datatables -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    
    <script>
      $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: ['excel', 'csv' ]
        } );
     
        table.buttons().container()
            .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
    } );
    </script>

    <script>
    $(function() {
		<c:set var="startDate" value="${fromDate}"/> 
		<c:set var="endDate" value="${toDate}"/> 
		var startDate= '<c:out value="${startDate}"/>';
		var endDate= '<c:out value="${endDate}"/>';
	 	var start = moment(startDate);
	    var end = moment(endDate);
	    function cb(start, end) {
	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	        $('#daterange').val(start + ' - ' + end)
	        console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	    }
	
	    $('#reportrange').daterangepicker({
	        startDate: start,
	        endDate: end,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, cb);
	    cb(start, end);
	    
	});
    </script>
</body>
</html>