<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords"
	content="Ewire Prepaid Card - Enjoy great offers online & offline using Ewire Prepaid card. Fast & Easy Recharge. Pay Utility Bills. Swipe & Save.">
<meta name="description"
	content="India&#039;s Digital Cash. Go cashless with Ewire Prepaid Card. Experience a seamless, safe payments platform secured with Mastercard.">
<title>Home | EWire</title>
<!-- favicon -->
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon.png"
	type="image/gif" sizes="32x32">
<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/css/responsive.css">

<!-- google fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800"
	rel="stylesheet">

<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/slick/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/assets/slick/slick-theme.css">
</head>
<body>
	<a href="javascript:void(0);" id="top"><i class="fa fa-chevron-up"></i></a>

	<nav class="navbar navbar-white">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="${pageContext.request.contextPath}/Home"> <img
					src="${pageContext.request.contextPath}/resources/assets/img/logo.png"
					class="img-responsive">
				</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">About Us</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="#">offers</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
				<ul class="nav navbar-nav navbar-right">
					<li><a href="${pageContext.request.contextPath}/About">about
							us</a></li>
					<li><a href="#section1">offers</a></li>
					<!-- <li><a href="#section2">testimonial</a></li> -->
					<li><a href="#section3">contact</a></li>
					<!-- <li><a href="#">refer &amp; earn</a></li> -->
					<!-- <li><a href="#" class="btn btn-sm btn-custom" style="margin-right: 6px; color: #fff;">login</a></li>
        <li><a href="#" class="btn btn-sm btn-custom" style="color: #fff;">register</a></li> -->
				</ul>
			</div>
		</div>
	</nav>

	<div class="body-container">
		<div class="container" id="hero-image">



			<!-- offer carousel -->
			<div class="slider-wrp">
				<div class="offers_banners">
					<div>
						<img
							src="${pageContext.request.contextPath}/resources/assets/img/landing/slider/slider1.png"
							class="img-responsive">
					</div>
					<div>
						<img
							src="${pageContext.request.contextPath}/resources/assets/img/landing/slider/slider2.png"
							class="img-responsive">
					</div>
					<div>
						<img
							src="${pageContext.request.contextPath}/resources/assets/img/landing/slider/slider3.png"
							class="img-responsive">
					</div>
					<div>
						<img
							src="${pageContext.request.contextPath}/resources/assets/img/landing/slider/slider4.png"
							class="img-responsive">
					</div>
				</div>
			</div>

			<!-- single offers -->
			<div class="singlOffrs" id="section1">
				<div class="singlOffrs_head text-center">
					<span>exciting offers with Ewire Prepaid Cards</span>
				</div>
				<div class="singlOffrs_body">
					<div class="innersingl_offrs">
						<ul class="row">
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o1.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o2.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o3.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o4.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o5.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o6.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o7.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o8.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o9.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o10.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o11.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o12.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o13.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o14.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o15.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o16.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o17.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o18.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o19.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o20.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o21.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o22.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o23.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o24.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o25.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o26.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o27.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o28.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o29.png"
											class="img-responsive">
									</center>
								</div>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-6 col-xs-6 singl_item">
								<div class="singl_offfers">
									<center>
										<img
											src="${pageContext.request.contextPath}/resources/assets/img/landing/offers/o30.png"
											class="img-responsive">
									</center>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="singlOffrs_footer">
					<div class="load_more text-center">
						<a href="#" class="custm_btn" id="loadMore">Load More</a>
					</div>
				</div>
			</div>

			<!-- testimonial slider -->
			<!-- <div class="testi_wrap" id="section2">
			<div class="testi_hed text-center">
				<span>testimonial</span>
			</div>
			<div class="testi_body">
				<div class="row slide">
					<div class="col-sm-3">
						<div class="thumbnail">
					        <img src="../assets/img/landing/testimonial/t1.png" alt="..." class="img-responsive">
					        <div class="caption">
					        	<p>loremipsum loremipsumlorem ipsumloremip sumloremipsumlorem ipsum loremipsuml oremipsum</p>
					        </div>
					        <div class="row">
					        	<div class="col-sm-12 thumb-foot">
					        		<div class="col-sm-6">
						        		<center><img src="../assets/img/landing/sign/yuv.png" class="img-responsive" style="width: 50%;"></center>
						        	</div>
						        	<div class="col-sm-6">
						        		<div class="testi_person">
						        			<span>Yuvraj Singh</span><br>
						        			<small>Player(Kings XI Punjab)</small>
						        		</div>
						        	</div>
					        	</div>
					        </div>
					    </div>
					</div>
					<div class="col-sm-3">
						<div class="thumbnail">
					        <img src="../assets/img/landing/testimonial/t2.png" alt="..." class="img-responsive">
					        <div class="caption">
					        	<p>loremipsuml oremipsu mloremipsuml oremipsumlor emipsumlore mipsumlore mipsum loremipsum</p>
					        </div>
					        <div class="row">
					        	<div class="col-sm-12 thumb-foot">
					        		<div class="col-sm-6">
						        		<center><img src="../assets/img/landing/sign/asw.png" class="img-responsive" style="width: 50%;"></center>
						        	</div>
						        	<div class="col-sm-6">
						        		<div class="testi_person">
						        			<span>R Ashwin</span><br>
						        			<small>Captain(Kings XI Punjab)</small>
						        		</div>
						        	</div>
					        	</div>
					        </div>
					    </div>
					</div>
					<div class="col-sm-3">
						<div class="thumbnail">
					        <img src="../assets/img/landing/testimonial/t3.png" alt="..." class="img-responsive">
					        <div class="caption">
					        	<p>loremips umloremipsumloremipsuml oremipsumlor emipsumlo remipsu mlorem ipsumlorem ipsum</p>
					        </div>
					        <div class="row">
					        	<div class="col-sm-12 thumb-foot">
					        		<div class="col-sm-6">
						        		<center><img src="../assets/img/landing/sign/gale.png" class="img-responsive" style="width: 50%;"></center>
						        	</div>
						        	<div class="col-sm-6">
						        		<div class="testi_person">
						        			<span>Chris Gale</span><br>
						        			<small>Player(Kings XI Punjab)</small>
						        		</div>
						        	</div>
					        	</div>
					        </div>
					    </div>
					</div>
					<div class="col-sm-3">
						<div class="thumbnail">
					        <img src="../assets/img/landing/testimonial/t4.png" alt="..." class="img-responsive">
					        <div class="caption">
					        	<p>loremip sumloremipsumlo remipsumloremi psumlorem ipsu mloremipsuml oremips umloremipsum</p>
					        </div>
					        <div class="row">
					        	<div class="col-sm-12 thumb-foot">
					        		<div class="col-sm-6">
						        		<center><img src="../assets/img/landing/sign/brad.png" class="img-responsive" style="width: 50%;"></center>
						        	</div>
						        	<div class="col-sm-6">
						        		<div class="testi_person">
						        			<span>Brad Hodge</span><br>
						        			<small>Coach(Kings XI Punjab)</small>
						        		</div>
						        	</div>
					        	</div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div> -->

			<!-- Download App from store -->
			<div class="download_app">
				<div class="app_heading text-center">
					<span>In the palm of your hands</span>
				</div>
				<div class="app_subhead text-center">
					<span>Download our App Now</span>
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="col-sm-6 col-xs-6">
							<div>
								<a
									href="https://play.google.com/store/apps/details?id=in.MadMoveGlobal.CashierCard"
									target="_blank"><img
									src="${pageContext.request.contextPath}/resources/assets/img/landing/playS.png"
									class="img-responsive store-img right-align"></a>
							</div>
						</div>
						<div class="col-sm-6 col-xs-6">
							<div>
								<a
									href="https://itunes.apple.com/in/app/cashier-prepaid-cards/id1374882309?mt=8"
									target="_blank"><img
									src="${pageContext.request.contextPath}/resources/assets/img/landing/appS.png"
									class="img-responsive store-img left-align"></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- contact us -->
			<div class="cntct_wrap" id="section3">
				<div class="cntct_hed text-center">
					<span>contact us</span>
				</div>
				<div class="cntct_body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="contact_frm_hed">
									<h2>Reach out to us!</h2>
									<span>Got a question about Ewire Prepaid Cards. Are you
										interested in partnering with us? Have some suggestions or
										just want to say hi? contact us:</span>
								</div>
								<div class="contact_frm">
									<form>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="firName" class="form-control"
													placeholder="Enter First Name">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="lasName" class="form-control"
													placeholder="Enter Last Name">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="contatNum"
													class="form-control" placeholder="Enter Mobile Number">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" name="" id="emai" class="form-control"
													placeholder="Enter E-mail">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<textarea class="form-control" id="messg"
													placeholder="Message..." rows="4"></textarea>
											</div>
										</div>
										<div class="form-group text-right">
											<a class="custm_btn" type="button" id="email_request"
												onclick="sendMail()">Submit</a>
										</div>
									</form>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="customcare_contain">
									<div class="contact_hed">
										<h2>Customer Care</h2>
										<span>Not sure where to start? Need help? Just visit
											our <span>help center</span> or get in touch with us:
										</span>
									</div>
									<div class="contact_bdy">
										<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
											<div class="col-sm-3 col-xs-3">
												<div>
													<img
														src="${pageContext.request.contextPath}/resources/assets/img/landing/send.svg"
														class="img-responsive">
												</div>
											</div>
											<div class="col-sm-9 col-xs-9">
												<div>
													<strong>Mail us at:</strong><br> <span><a
														href="mailto:service@liveewire.com">service@liveewire.com</a></span>
												</div>
											</div>
										</div>

										<div class="col-md-12 col-sm-6 col-xs-12 singl_contac">
											<div class="col-sm-3 col-xs-3">
												<div>
													<img
														src="${pageContext.request.contextPath}/resources/assets/img/landing/tel.svg"
														class="img-responsive">
												</div>
											</div>
											<div class="col-sm-9 col-xs-9">
												<div>
													<strong>Call us at:</strong><br> <span><a
														href="tel:+91-7259195449">+91-7259195449</a><br>
													<em>(10 AM - 7 PM all days. Except Holidays)</em></span>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="social_wrapper">
									<div class="social_hed">
										<h4>Follow us at:</h4>
									</div>
									<div class="social_links">
										<ul class="row">
											<li><a href="https://www.facebook.com/CashierCard/"
												target="_blank">
													<center>
														<img
															src="${pageContext.request.contextPath}/resources/assets/img/landing/social/facebook.svg"
															class="img-responsive">
													</center>
											</a></li>
											<li><a href="https://twitter.com/cashiercard?lang=en"
												target="_blank">
													<center>
														<img
															src="${pageContext.request.contextPath}/resources/assets/img/landing/social/twitter.svg"
															class="img-responsive">
													</center>
											</a></li>
											<li><a href="https://www.instagram.com/cashiercard/"
												target="_blank">
													<center>
														<img
															src="${pageContext.request.contextPath}/resources/assets/img/landing/social/instagram.svg"
															class="img-responsive">
													</center>
											</a></li>
											<li><a
												href="https://www.youtube.com/channel/UCCt9wky_iLlTBU28_EpGSTw?pbjreload=10"
												target="_blank">
													<center>
														<img
															src="${pageContext.request.contextPath}/resources/assets/img/landing/social/youtube.svg"
															class="img-responsive">
													</center>
											</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- footer starts here -->
			<div class="footer">
				<div class="row">
					<div class="col-sm-6 col-xs-12" id="terms_link">
						<span><a
							href="${pageContext.request.contextPath}/PrivacyPolicy">privacy
								policy</a> | <a
							href="${pageContext.request.contextPath}/TermsnConditions">terms
								&amp; conditions</a> | <a
							href="${pageContext.request.contextPath}/faq">faq<small>s</small></a>
							| <a href="${pageContext.request.contextPath}/GrievancePolicy">Grievance
								Policy<small>s</small>
						</a> | <a href="${pageContext.request.contextPath}/RefundPolicy">Refund
								Policy<small>s</small>
						</a></span>
					</div>
					<div class="col-sm-6 col-xs-12 text-right" id="copyR">
						<div class="copyright">
							<span><script type="text/javascript">
								document.write(new Date().getFullYear());
							</script>
								&copy; copyright Ewire. | Powered by Cashier.</span>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- <div class="get-app hidden-xs">
	<span  class="pull-right" id="app_get">Get App</span>
	<div class="get-app-inner pull-right">
		<input type="text" name="getApp" id="get_app" maxlength="10" class="form-control" placeholder="Enter 10 Digit Mobile Number">
	</div>
	<span class="pull-right">
		<i class="glyphicon glyphicon-menu-left trans-03-sec"></i>
	</span>
</div> -->



	<!-- scripts starts here -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/assets/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- slick slider -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/assets/slick/slick.min.js"></script>

	<!-- custom js for dashboard -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/assets/js/dashboard.js"></script>

	<script type="text/javascript">
		function sendMail() {
			var valid = true;
			var contact = $('#contatNum').val();
			var email = $('#emai').val();
			var firstname = $('#firName').val();
			var lastName = $('#lasName').val();
			var message = $('#messg').val();

			if (contact.length <= 0) {
				$("#error_contact").html("Please enter the contact number");
				valid = false;
			}

			if (email.length <= 0) {
				$("#error_imail").html("Please enter the email id");
				valid = false;
			}
			if (firstname.length <= 0) {
				$("#error_firstname").html("Please enter the first name");
				valid = false;
			}
			if (lastName.length <= 0) {
				$("#error_lastName").html("Please enter the lastName");
				valid = false;
			}

			if (message.length <= 0) {
				$("#error_message").html("Please enter the message");
				valid = false;
			}

			if (valid == true) {
				$("#email_request").addClass("disabled");
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : context_path + "/Send/Mail",
					dataType : 'json',
					data : JSON.stringify({
						"contactNo" : "" + contact + "",
						"email" : "" + email + "",
						"firstName" : "" + firstname + "",
						"lastName" : "" + lastName + "",
						"message" : "" + message + ""
					}),
					success : function(response) {
						$("#email_request").removeClass("disabled");
						$("#email_request").html("Sent");
						console.log(response);
						// 				if (response.code.includes("S00")) {
						// 					swal("Congrats!!", response.message, "success");
						// 					$("#imps_success").html(response.message);
						// 					$("#imps_success").css("color", "green");
						$('#contatNum').val("");
						$('#emai').val("");
						$('#firName').val("");
						$('#lasName').val("");
						$('#messg').val("");
						// 				}
						// 				else if (response.code.includes("F00")) {
						// // 					$("#imps_success").html(response.message);
						// // 					$("#imps_success").css("color", "red");
						// // 					swal({
						// // 						  type: 'error',
						// // 						  title: 'Sorry!!',
						// // 						  text: response.message
						// // 						});
						// 					$('#contatNum').val("");
						// 					 $('#emai').val("");
						// 					$('#firName').val("") ;
						// 					$('#lasName').val("");
						// 					$('#messg').val("") ;
						// 				}
					}
				});
			}
			var timeout = setTimeout(function() {
				$("#error_contact").html("");
				$("#error_imail").html("");
				$("#error_firstname").html("");
				$("#error_lastName").html("");
				$("#error_message").html("");
			}, 3000);
		}
	</script>
</body>
</html>