/**
 * 
 */
var eStatus = "";

$(document).ready(function(){
	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
	var spinner = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var headers = {};
	headers[csrfHeader]=csrfToken;

	$(".numeric").keydown(function(event){
        if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
            return;
        }else{
            if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

	$.ajax({
		
		type : "POST",
		contentType : "application/json",
		url : context_path+"/Api/v1/User/Windows/en/GetBalanceWebJS",
		dataType : 'json',
		data : JSON.stringify({
			"sessionId" :" ",
		}),
		success : function(response) {
			console.log("card balance"+response.balance)
			 $("#cardBalance").html(response.balance);
			$("#cardBalance").val(response.balance);
			 $("#crdBalance").html(response.balance);
			$("#crdBalance").val(response.balance);
		},
	});
	
	
	
	
function renderReceiptTable(data) {
	var $table= $('#editedtable');
	if(($table).find('tbody').length) $table.find('tbody').empty();
	$tbody = $table.append('<tbody></tbody>');
	if(data){
		for(var i=0; i < data.length; i++) {
			$tr = $('<tr></tr>');
			var statusClass = '';
			switch(data[i].status) {
				case 'Initiated' :  statusClass = 'label label-info'; break;
				case 'Success' : statusClass = 'label label-success'; break;
				case 'Reversed' : statusClass = 'label label-warning'; break;
				case 'Processing' : statusClass = 'label label-active'; aabreak;
				case 'Failed' : statusClass = 'label label-danger'; break;
			}
			$tr.append($('<td></td>').text(data[i].date));
			$tr.append($('<td></td>').text(data[i].description));
			$tr.append($('<td></td>').text(data[i].transactionRefNo));
			$tr.append($('<td></td>').text(data[i].amount));
			$tr.append($('<td></td>').append($('<span style="font-size:13px;display:block;max-width:80px;margin-right:20px"></span>').text(data[i].status).addClass(statusClass)));
			$tbody.append($tr);
		}
	}
}
function createPaging(data) {
 $('#rcptPagination').twbsPagination({
	 totalPages:data.totalPages,
	 visiblePages: 5,
	 onPageClick: function (event, page) 
	 {
		  transactionlist(page-1);
	
     }
		
 });
}

});