
!function($) {
    "use strict";

    var Dashboard1 = function() {
    	this.$realData = []
    };

    //creates Bar chart
    Dashboard1.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barSizeRatio: 0.7,
            barColors: lineColors
        });
    },

    //creates line chart
    Dashboard1.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Area({
          element: element,
          data: data,
          xkey: xkey,
          ykeys: ykeys,
          labels: labels,
          fillOpacity: opacity,
          pointFillColors: Pfillcolor,
          pointStrokeColors: Pstockcolor,
          behaveLikeLine: true,
          gridLineColor: '#eef0f2',
          hideHover: 'auto',
          resize: true, //defaulted to true
          pointSize: 0,
          lineColors: lineColors
        });
    },

    //creates Donut chart
    Dashboard1.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors
        });
    },
    
    
    Dashboard1.prototype.init = function() {

        //creating bar chart
        var $barData  = [
            { y: 'January', a: 25, b: 25 },
            { y: 'February', a: 37, b: 38 },
            { y: 'March', a: 40, b: 49 },
            { y: 'April', a: 60, b: 55 },
            { y: 'May', a: 66, b: 60 },
            { y: 'June', a: 93, b: 80 }
        ];
        this.createBarChart('morris-bar-example', $barData, 'y', ['a','b'], ['Statistics'], ['#188ae2','#188ae2']);

        //create line chart
        var $data  = [
              { y: '2014', a: 0, b: 9},
              { y: '2015', a: 10,  b: 15},
              { y: '2016', a: 15,  b: 20},
              { y: '2017', a: 20,  b: 25},
              { y: '2018', a: 25,  b: 30},
              { y: '2019', a: 35,  b: 40},
              { y: '2020', a: 40, b: 60}
          ];
        this.createLineChart('morris-line-example', $data, 'y', ['a','b'], ['Series A','Series B'],['0.9'],['#ffffff'],['#999999'], ['#10c469','#188ae2']);

        //creating donut chart
        var $donutData = [
                {label: "Download Sales", value: 15},
                {label: "In-Store Sales", value: 10},
                {label: "Mail-Order Sales", value: 21}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#0074e4', '#3DCC79', "#FFC42E"]);
    },
    //init
    $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard1.init();
}(window.jQuery);