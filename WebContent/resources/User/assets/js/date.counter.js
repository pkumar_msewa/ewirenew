$(document).ready(function () {

    "use strict";

    //Set your date
    $('#count-down').countDown({
        targetDate: {
            'day': 17,
            'month': 1,
            'year': 2019,
            'hour': 0,
            'min': 0,
            'sec': 0
        },
        omitWeeks: true
    });

});